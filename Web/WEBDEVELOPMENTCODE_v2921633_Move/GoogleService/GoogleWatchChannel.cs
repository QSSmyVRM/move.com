﻿//ZD 100147 Start
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Web;
using Google.Apis.Util;
using Google.Apis.Authentication.OAuth2;
using Google.Apis.Authentication.OAuth2.DotNetOpenAuth;
using DotNetOpenAuth.Messaging;
using DotNetOpenAuth.OAuth2;
using Google.Apis.Calendar.v3;
using Google.Apis.Calendar.v3.Data;
using Google.Apis.Requests;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using Google.Apis.Auth.OAuth2;
using System.Threading;
using System.Net;
using Newtonsoft.Json.Linq;
using System.Security.Cryptography.X509Certificates;
using Google.Apis.Auth.OAuth2.Responses;
using Google.Apis.Auth.OAuth2.Flows;
using Google.Apis.Auth.OAuth2.Web;
using System.Xml.Linq;
using System.Globalization;
using System.Data;
using myVRM.DataLayer;

namespace GoogleService
{   
    class GoogleWatchChannel
    {
        static String dirPth = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
        static ASPIL.VRMServer myvrmCom = null;
        static VRMRTC.VRMRTC objRTC = null;
        static String MyVRMServer_ConfigPath = dirPth + "\\VRMSchemas\\";
        CalendarService calendarConnection;
        static System.Globalization.CultureInfo globalCultureInfo = new System.Globalization.CultureInfo("en-US", true);
        
        XmlDocument _Xdoc = null;
        string _OutXML = "", _ClientID = "", _SecretID = "", _ChannelAddrss = "", _RefreshToken = "", _UserEmail = "", _UserID = "";
        int PushtoGoogle = 0, Pollcount = 0, _GoogleIntegration = 0, _CompanyId = 11, _usertimezone = 0;
        StringBuilder _inXML = null;
        
        NS_LOGGER.Log log = null;
        static NS_CONFIG.Config config = null;
        static NS_MESSENGER.ConfigParams configParams = null;
        static string configPath = dirPth + "\\VRMMaintServiceConfig.xml";
        static string errMsg = null;
        static String RTC_ConfigPath = dirPth + "\\VRMSchemas\\VRMRTCConfig.xml";
        static bool ret = false;

        DateTime strtDate = DateTime.UtcNow;
        DateTime endDate = DateTime.UtcNow;

        List<RoomEWSDetails> RoomDetails = new List<RoomEWSDetails>();
        XDocument xdoc = null;

        internal GoogleWatchChannel()
        {
            myvrmCom = new ASPIL.VRMServer();
            config = new NS_CONFIG.Config();
            configParams = new NS_MESSENGER.ConfigParams();
            ret = config.Initialize(configPath, ref configParams, ref errMsg, MyVRMServer_ConfigPath,RTC_ConfigPath); //FB 2944
            log = new NS_LOGGER.Log(configParams);            
        }

        #region GoogleChannelupdate
        /// <summary>
        /// GoogleChannelupdate
        /// </summary>
        internal void GoogleChannelupdate()
        {
            try
            {
                if (myvrmCom == null)
                    myvrmCom = new ASPIL.VRMServer();

                _inXML = new StringBuilder();
                _OutXML = myvrmCom.Operations(MyVRMServer_ConfigPath, "GetGoogleChannelExpiredList", _inXML.ToString());
                log.Trace("OutXML: GetGoogleChannelExpiredList:" + _OutXML);

                if (_OutXML.IndexOf("<error>") >= 0)
                    log.Trace("Error in GetGoogleChannelExpiredList");
                else
                {
                    _Xdoc = new XmlDocument();
                    _Xdoc.LoadXml(_OutXML);

                    _ClientID = "";
                    if (_Xdoc.SelectSingleNode("//GetGoogleChannelExpiredList/GoogleClientID") != null)
                        _ClientID = _Xdoc.SelectSingleNode("//GetGoogleChannelExpiredList/GoogleClientID").InnerText;

                    _SecretID = "";
                    if (_Xdoc.SelectSingleNode("//GetGoogleChannelExpiredList/GoogleSecretID") != null)
                        _SecretID = _Xdoc.SelectSingleNode("//GetGoogleChannelExpiredList/GoogleSecretID").InnerText;

                    _ChannelAddrss = "";
                    if (_Xdoc.SelectSingleNode("//GetGoogleChannelExpiredList/ChannelAddress") != null)
                        _ChannelAddrss = _Xdoc.SelectSingleNode("//GetGoogleChannelExpiredList/ChannelAddress").InnerText;

                    if (_Xdoc.SelectSingleNode("//GetGoogleChannelExpiredList/GoogleIntegration") != null)
                        int.TryParse(_Xdoc.SelectSingleNode("//GetGoogleChannelExpiredList/GoogleIntegration").InnerText, out _GoogleIntegration);

                    if (_GoogleIntegration == 1)
                    {
                        XmlNodeList _NodeList = _Xdoc.GetElementsByTagName("User");
                        XmlElement _Element = null;

                        for (int _i = 0; _i < _NodeList.Count; _i++)
                        {
                            _Element = (XmlElement)_NodeList[_i];
                            _RefreshToken = ""; _UserEmail = ""; _UserID = "";
                            _RefreshToken = _Element.GetElementsByTagName("RefreshToken")[0].InnerText;
                            _UserID = _Element.GetElementsByTagName("UserID")[0].InnerText;
                            _UserEmail = _Element.GetElementsByTagName("UserEmail")[0].InnerText;

                            GetAuthentication();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + ex.ToString());
            }
        }
        #endregion

        #region GetAuthentication
        /// <summary>
        /// GetAuthentication
        /// </summary>
        private void GetAuthentication()
        {
            Google.Apis.Calendar.v3.Data.Channel _channel = null;
            Google.Apis.Calendar.v3.Data.Channel _ChannelRes = null;
            try
            {
                UserAuthentication(_UserEmail);

                _channel = new Google.Apis.Calendar.v3.Data.Channel();
                _channel.Id = Guid.NewGuid().ToString();

                _channel.Type = "web_hook";
                _channel.Address = _ChannelAddrss + @"/en/GoogleReceiver.aspx";
                //_channel.Address = "https://demo1.myvrm.com/en/GoogleReceiver.aspx";
                _channel.Token = _UserID;

                _ChannelRes = new Google.Apis.Calendar.v3.Data.Channel();
                _ChannelRes = calendarConnection.Events.Watch(_channel, _UserEmail).Execute();
              
                if (_ChannelRes == null)
                    log.Trace("Channel response failed");
                else
                {
                    _inXML = new StringBuilder();
                    _inXML.Append("<UserToken>");
                    _inXML.Append("<UserId>" + _UserID + "</UserId>");
                    _inXML.Append("<ChannelID>" + _ChannelRes.Id + "</ChannelID>");
                    _inXML.Append("<ChannelEtag>" + _ChannelRes.ETag + "</ChannelEtag>");

                    double _Expiration = 0;
                    double.TryParse(_ChannelRes.Expiration.ToString(), out _Expiration);

                    DateTime _dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0);
                    _dtDateTime = _dtDateTime.AddMilliseconds(_Expiration).ToUniversalTime();

                    _inXML.Append("<ChannelExpiration>" + _dtDateTime.ToString() + "</ChannelExpiration>");
                    _inXML.Append("<ChannelResourceId>" + _ChannelRes.ResourceId + "</ChannelResourceId>");
                    _inXML.Append("<ChannelResourceUri>" + _ChannelRes.ResourceUri + "</ChannelResourceUri>");
                    _inXML.Append("</UserToken>");

                    if (myvrmCom == null)
                        myvrmCom = new ASPIL.VRMServer();
                    _OutXML = myvrmCom.Operations(MyVRMServer_ConfigPath, "SetUserToken", _inXML.ToString());

                    if (_OutXML.IndexOf("<error>") >= 0)
                        log.Trace("Error in SetUserToken");
                }
            }
            catch (System.Threading.ThreadAbortException ex)
            {
                log.Trace("Thread abort GetAuthentication Block:" + ex.Message);
            }
            catch (System.Exception exp)
            {
                log.Trace("GetAuthentication Block:" + exp.Message);
            }
        }
        #endregion

        #region GoogleConferenceupdate
        /// <summary>
        /// GoogleConferenceupdate
        /// </summary>
        internal void GoogleConferenceupdate()
        {
            List<RoomEWSDetails> RoomDetails = null;
            DataSet ConfTable = null;
            DataTable ConferenceTable = null;
            XmlDocument xmlDoc = null;
            DataRow[] rows;
            try
            {
                if (myvrmCom == null)
                    myvrmCom = new ASPIL.VRMServer();

                RoomDetails = new List<RoomEWSDetails>();

                _inXML = new StringBuilder();
                _OutXML = myvrmCom.Operations(MyVRMServer_ConfigPath, "FetchGoogleDetails", _inXML.ToString());
                log.Trace("OutXML: FetchGoogleDetails:" + _OutXML);

                if (_OutXML.IndexOf("<error>") >= 0)
                    log.Trace("Error in FetchGoogleDetails");
                else
                {
                    _Xdoc = new XmlDocument();
                    _Xdoc.LoadXml(_OutXML);

                    if (_Xdoc.SelectSingleNode("//GoogleInfo/GoogleClientID") != null)
                        _ClientID = _Xdoc.SelectSingleNode("//GoogleInfo/GoogleClientID").InnerText;

                    if (_Xdoc.SelectSingleNode("//GoogleInfo/GoogleSecretID") != null)
                        _SecretID = _Xdoc.SelectSingleNode("//GoogleInfo/GoogleSecretID").InnerText;

                    if (_Xdoc.SelectSingleNode("//GoogleInfo/AdminEmailaddress") != null)
                        _UserEmail = _Xdoc.SelectSingleNode("//GoogleInfo/AdminEmailaddress").InnerText;

                    if (_Xdoc.SelectSingleNode("//GoogleInfo/PollCount") != null)
                        int.TryParse(_Xdoc.SelectSingleNode("//GoogleInfo/PollCount").InnerText, out Pollcount);

                    if (_Xdoc.SelectSingleNode("//GoogleInfo/GoogleIntegration") != null)
                        int.TryParse(_Xdoc.SelectSingleNode("//GoogleInfo/GoogleIntegration").InnerText, out _GoogleIntegration);

                    if (_Xdoc.SelectSingleNode("//GoogleInfo/UserId") != null)
                        _UserID = _Xdoc.SelectSingleNode("//GoogleInfo/UserId").InnerText;

                    if (_Xdoc.SelectSingleNode("//GoogleInfo/OrgID") != null)
                        int.TryParse(_Xdoc.SelectSingleNode("//GoogleInfo/OrgID").InnerText, out _CompanyId);

                    if (_Xdoc.SelectSingleNode("//GoogleInfo/RefreshToken") != null)
                        _RefreshToken = _Xdoc.SelectSingleNode("//GoogleInfo/RefreshToken").InnerText;

                    if (_Xdoc.SelectSingleNode("//GoogleInfo/usertimezone") != null)
                        int.TryParse(_Xdoc.SelectSingleNode("//GoogleInfo/usertimezone").InnerText, out _usertimezone);
                }

                if (_GoogleIntegration == 0)
                {
                    UserAuthentication(_UserEmail);

                    if (RetreiveRoomQueue(ref RoomDetails))
                    {
                        if (Pollcount > 0)
                            endDate = strtDate.AddDays(Pollcount);
                        else
                            endDate = strtDate.AddDays(30); //Default 30 days

                        strtDate = strtDate.Date;
                        endDate = endDate.Date.AddSeconds(86399);

                        strtDate = strtDate.Subtract(new TimeSpan(strtDate.Hour, strtDate.Minute, strtDate.Second));

                        objRTC = new VRMRTC.VRMRTC();
                        ConfTable = new DataSet();
                        ConferenceTable = new DataTable();
                        xmlDoc = new XmlDocument();

                        _inXML = new StringBuilder();
                        _inXML.Append("<GetGoogleConference><userid>" + _UserID + "</userid><GoogleGUID></GoogleGUID><LastrunDatetime>" + strtDate + "</LastrunDatetime></GetGoogleConference>");
                        _OutXML = objRTC.Operations(RTC_ConfigPath, "GetGoogleConference", _inXML.ToString());

                        log.Trace("GetGoogleConference OutXML:" + _OutXML);

                        if (_OutXML.IndexOf("<error>") >= 0)
                            log.Trace("Error in GetGoogleChannelExpiredList");
                        else
                        {
                            xmlDoc.LoadXml(_OutXML);
                            ConfTable.ReadXml(new XmlNodeReader(xmlDoc));
                            if (ConfTable.Tables.Count > 0)
                                ConferenceTable = ConfTable.Tables[0];
                            if (!ConferenceTable.Columns.Contains("confid"))
                                ConferenceTable.Columns.Add("confid");
                            if (!ConferenceTable.Columns.Contains("InstanceID"))
                                ConferenceTable.Columns.Add("InstanceID");
                            if (!ConferenceTable.Columns.Contains("orgId"))
                                ConferenceTable.Columns.Add("orgId");
                            if (!ConferenceTable.Columns.Contains("Recurring"))
                                ConferenceTable.Columns.Add("Recurring");
                            if (!ConferenceTable.Columns.Contains("GoogleConfLastUpdated"))
                                ConferenceTable.Columns.Add("GoogleConfLastUpdated");
                            if (!ConferenceTable.Columns.Contains("GoogleGuid"))
                                ConferenceTable.Columns.Add("GoogleGuid");
                            if (!ConferenceTable.Columns.Contains("Status"))
                                ConferenceTable.Columns.Add("Status");
                        }

                        for (int r = 0; r < RoomDetails.Count; r++)
                        {
                            if (RoomDetails[r].email.Trim() != "")
                            {
                                List<Event> _evt = new List<Google.Apis.Calendar.v3.Data.Event>();
                                EventsResource.ListRequest _calRequest = calendarConnection.Events.List(RoomDetails[r].email.Trim());
                                _calRequest.TimeZone = "UTC";
                                _calRequest.TimeMax = endDate;
                                _calRequest.TimeMin = strtDate;
                                //_calRequest.ShowDeleted = true;
                                _calRequest.SingleEvents = true;

                                Events response = _calRequest.Execute();
                                if (response.Items.Count > 0)
                                {
                                    MeetingEvents(ref response, ref ConferenceTable, RoomDetails[r].email.Trim()); //ALLOPS-79
                                }
                            }
                        }

                        rows = ConferenceTable.Select("Status = '1'");
                        foreach (DataRow row in rows)
                            ConferenceTable.Rows.Remove(row);

                        if (ConferenceTable.Rows.Count > 0)
                            DeleteEvents(ConferenceTable);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + ex.ToString());
            }
        }
        #endregion

        #region MeetingInsert/Update/Delete
        /// <summary>
        /// MeetingEvents
        /// </summary>
        /// <param name="response"></param>
        /// <param name="ConferenceTable"></param>
        /// <returns></returns>
        public bool MeetingEvents(ref Events response, ref DataTable ConferenceTable, string email) //ALLOPS-79
        {
            //XmlDocument _xdoc = null;
            string _EventXML = "", _EventOutXML = "", _Confid = "", _InstanceID = "", _Recur = "", _company = "", _newConfid = "", Subject = ""; //ALLOPS-79
            DateTime _GoogleConfLastUpdated = DateTime.UtcNow, _GoogleReceivedDateTime = DateTime.UtcNow, _StartDateTime = DateTime.UtcNow, _EndDateTime = DateTime.UtcNow;
            bool _AllowEdit = true;
            bool _doNotSync = false;
            TimeSpan _span = new TimeSpan();
            DataRow[] rows;
            try
            {
                for (int _i = 0; _i < response.Items.Count; _i++)
                {
                    try
                    {
                        objRTC = new VRMRTC.VRMRTC();
                        _span = new TimeSpan();
                        _Confid = "new";
                        _InstanceID = "";
                        _AllowEdit = true;
                        _newConfid = "";
                        Subject = ""; //ALLOPS-79
                        _doNotSync = false;
                        #region commented
                        //_xdoc = new XmlDocument();
                        //_EventXML = "<GetGoogleConference><userid>11</userid><GoogleGUID>" + response.Items[_i].Id + "</GoogleGUID></GetGoogleConference>";
                        //_EventOutXML = objRTC.Operations(RTC_ConfigPath, "GetGoogleConference", _EventXML);

                        //log.Trace("GetGoogleConference OutXML:" + _EventOutXML);

                        //if (_EventOutXML.IndexOf("<error>") >= 0)
                        //{
                        //    log.Trace("Error in Getting the GetGoogleConference");
                        //    continue;
                        //}
                        //else
                        //{
                        //    _xdoc.LoadXml(_EventOutXML);
                        //    if (_xdoc.SelectSingleNode("GetGoogleConference/confid") != null)
                        //        _Confid = _xdoc.SelectSingleNode("GetGoogleConference/confid").InnerText;
                        //    if (_xdoc.SelectSingleNode("GetGoogleConference/InstanceID") != null)
                        //        _InstanceID = _xdoc.SelectSingleNode("GetGoogleConference/InstanceID").InnerText;
                        //    if (_xdoc.SelectSingleNode("GetGoogleConference/Recurring") != null)
                        //        _Recur = _xdoc.SelectSingleNode("GetGoogleConference/Recurring").InnerText;
                        //    if (_xdoc.SelectSingleNode("GetGoogleConference/GoogleConfLastUpdated") != null)
                        //    {
                        //        DateTime.TryParse(_xdoc.SelectSingleNode("GetGoogleConference/GoogleConfLastUpdated").InnerText, out _GoogleConfLastUpdated);
                        //        DateTime.TryParse(response.Items[_i].Updated.ToString(), out _GoogleReceivedDateTime);
                        //        log.Trace("_GoogleReceivedDateTime : " + _GoogleReceivedDateTime.ToString("MM/dd/yyyy hh:mm"));
                        //        log.Trace("_GoogleConfLastUpdated : " + _GoogleConfLastUpdated.ToString("MM/dd/yyyy hh:mm"));
                        //        if (_GoogleReceivedDateTime.ToString("MM/dd/yyyy hh:mm") == _GoogleConfLastUpdated.ToString("MM/dd/yyyy hh:mm"))
                        //            _AllowEdit = false;
                        //        else
                        //            _AllowEdit = true;
                        //    }
                        //}
                        #endregion

                        rows = ConferenceTable.Select("GoogleGuid = '" + response.Items[_i].Id + "'");
                        if (rows.Count() > 0)
                        {
                            _Confid = rows[0]["confid"].ToString();
                            _InstanceID = rows[0]["InstanceID"].ToString();
                            _Recur = rows[0]["Recurring"].ToString();
                            _company = rows[0]["orgId"].ToString();
                            if (rows[0]["GoogleConfLastUpdated"] != null)
                            {
                                DateTime.TryParse(rows[0]["GoogleConfLastUpdated"].ToString(), out _GoogleConfLastUpdated);
                                DateTime.TryParse(response.Items[_i].Updated.ToString(), out _GoogleReceivedDateTime);
                                log.Trace("_GoogleReceivedDateTime : " + _GoogleReceivedDateTime.ToString("MM/dd/yyyy hh:mm"));
                                log.Trace("_GoogleConfLastUpdated : " + _GoogleConfLastUpdated.ToString("MM/dd/yyyy hh:mm"));
                                if (_GoogleReceivedDateTime.ToString("MM/dd/yyyy hh:mm") == _GoogleConfLastUpdated.ToString("MM/dd/yyyy hh:mm") || (rows[0]["Status"].ToString() == "1"))
                                    _AllowEdit = false;
                                //else if (rows[0]["Status"].ToString() != "1")
                                //    _AllowEdit = true;
                                else
                                    _AllowEdit = true;
                            }
                        }

                        if (response.Items[_i].Status == "cancelled" && _Confid != "new")
                        {
                            _EventXML = "<login>";
                            _EventXML += "<organizationID>" + _company + "</organizationID>";
                            _EventXML += "<userID>" + _UserID + "</userID>";
                            _EventXML += "<delconference>";
                            _EventXML += "<conference>";
                            //if (!response.Items[_i].Id.Contains('_') && _Recur == "1")
                            //    _EventXML += "<confID>" + _Confid + "</confID>";
                            //else
                            _EventXML += "<confID>" + _Confid + "," + _InstanceID + "</confID>";
                            _EventXML += "<reason></reason>";
                            _EventXML += "<deny>Delete</deny>";
                            _EventXML += "<fromAPI>1</fromAPI>";
                            _EventXML += "</conference>";
                            _EventXML += "</delconference>";
                            _EventXML += "</login>";

                            _EventOutXML = myvrmCom.Operations(MyVRMServer_ConfigPath, "DeleteConference", _EventXML);
                            log.Trace("OutXML: DeleteConference:" + _EventOutXML);
                        }
                        else if (response.Items[_i].Status.ToLower() == "confirmed" && _AllowEdit == true)
                        {
                            //ALLOPS-79 Start
                            //if (response.Items[_i].Attendees != null)
                            {
                                List<string> _RoomList = new List<string>();
                                IList<EventAttendee> Roomattendees = null;
                                if (response.Items[_i].Attendees != null)
                                    Roomattendees = response.Items[_i].Attendees.Where(EmailC => EmailC.Email.Contains("@resource.calendar.google.com") && EmailC.ResponseStatus != "declined").ToList();

                                //Zendesk - 105238 by SONU - Dated - 09 Sep, 2016
                                bool haveAttendee = false;
                                if (Roomattendees != null)
                                {
                                    _RoomList = Roomattendees.Select(Email => Email.Email.Trim()).ToList();
                                    if (Roomattendees.Where(ra => ra.Email.Equals(email) && ra.ResponseStatus != "declined").Count() > 0)
                                        haveAttendee = true;
                                }

                                if (response.Items[_i].Organizer.Email.Trim().Contains("@resource.calendar.google.com"))
                                    if (!_RoomList.Contains(response.Items[_i].Organizer.Email.Trim()))
                                        _RoomList.Add(response.Items[_i].Organizer.Email.Trim());

                                if (!_RoomList.Contains(email) && haveAttendee) //ALLOPS-79 Adding Rooms if room is not added in resource list
                                    _RoomList.Add(email);
                                //Zendesk - 105238 by SONU - Dated - 09 Sep, 2016  //ALLOPS-79 End

                                if (_RoomList.Count > 0)
                                {
                                    _EventXML = "<conference>";

                                    if (_company != "")
                                        _EventXML += "<organizationID>" + _company + "</organizationID>";
                                    else
                                        _EventXML += "<organizationID>" + _CompanyId + "</organizationID>";

                                    _EventXML += "<userEMail>" + response.Items[_i].Organizer.Email.Trim() + "</userEMail>";
                                    _EventXML += "<confInfo>";

                                    if (_Confid == "new")
                                        _EventXML += "<confID>" + _Confid + "</confID>";
                                    else
                                        _EventXML += "<confID>" + _Confid + "," + _InstanceID + "</confID>";
                                    //ALLOPS-79 Start
                                    if (response.Items[_i].Summary != null)
                                    {
                                        if (response.Items[_i].Summary.Trim() != "")
                                            Subject = response.Items[_i].Summary.Replace("'", "").Replace("&", "and").Replace("<", "").Replace(">", "").Replace("-", "");
                                    }
                                    else
                                        Subject = "(No title)";
                                    _EventXML += "<confName>" + Subject + "</confName>";
                                    _EventXML += "<confHostEmail>" + response.Items[_i].Organizer.Email.Trim() + "</confHostEmail>";
                                    if (response.Items[_i].Organizer.Email != null)
                                        _EventXML += "<confHostEmail>" + response.Items[_i].Organizer.Email.Trim() + "</confHostEmail>";
                                    else if (response.Items[_i].Organizer.Email == null || response.Items[_i].Organizer.Email == "")
                                        _EventXML += "<confHostEmail>" + _UserEmail.Trim() + "</confHostEmail>";
                                    //ALLOPS-79 End

                                    _EventXML += "<confOrigin>15</confOrigin>";

                                    log.Trace("Conf Start Time : " + response.Items[_i].Start.DateTime.ToString());

                                    if (string.IsNullOrEmpty(response.Items[_i].Start.DateTime.ToString()) && !(string.IsNullOrEmpty(response.Items[_i].Start.Date)))
                                        _StartDateTime = DateTime.Parse(response.Items[_i].Start.Date);
                                    else
                                        _StartDateTime = DateTime.Parse(response.Items[_i].Start.DateTimeRaw.ToString()).ToUniversalTime();// Changed for timezone issue

                                    //timeZone.changeToGMTTime(_usertimezone, ref _StartDateTime);

                                    log.Trace("Conf UTC Start Time : " + _StartDateTime.ToString());

                                    _EventXML += "<startDate>" + _StartDateTime.ToString("MM/dd/yyyy") + "</startDate>";
                                    _EventXML += "<startHour>" + _StartDateTime.ToString("hh") + "</startHour>";
                                    _EventXML += "<startMin>" + _StartDateTime.ToString("mm") + "</startMin>";
                                    _EventXML += "<startSet>" + _StartDateTime.ToString("tt") + "</startSet>";
                                    //_EventXML += "<standardTimeZone>" + response.Items[_i].Start.TimeZone + "</standardTimeZone>";//Based on this tag, XMLRefactoring command will fetch the timezone.

                                    _EventXML += "<timeZone>33</timeZone>";// Based on above tag, timezone will be fetched and replaced in this tag in XMLRefactoring method.
                                    _EventXML += "<setupDuration></setupDuration>";
                                    _EventXML += "<createBy>7</createBy>";


                                    if (string.IsNullOrEmpty(response.Items[_i].Start.DateTime.ToString()) && !(string.IsNullOrEmpty(response.Items[_i].Start.Date)))
                                        _EndDateTime = DateTime.Parse(response.Items[_i].End.Date).AddMinutes(-1);
                                    else
                                        _EndDateTime = DateTime.Parse(response.Items[_i].End.DateTimeRaw.ToString()).ToUniversalTime();// Changed for timezone issue

                                    //timeZone.changeToGMTTime(_usertimezone, ref _EndDateTime);

                                    log.Trace("Conf End Time Allday : " + _EndDateTime.ToString());

                                    _span = _EndDateTime - _StartDateTime;
                                    _EventXML += "<durationMin>" + (int)Math.Round(_span.TotalMinutes) + "</durationMin>";
                                    //ALLOPS-79 START
                                    if (response.Items[_i].Description != null)
                                        response.Items[_i].Description = response.Items[_i].Description.Replace("&", "σ").Replace("<", "õ").Replace(">", "æ");
                                    _EventXML += "<description>" + response.Items[_i].Description + "</description>";
                                    //ALLOPS-79 END

                                    //if (response.Items[_i].Id.Contains('_'))
                                    //    _EventXML += "<GoogleGUID>" + response.Items[_i].Id.Split('_')[0] + "</GoogleGUID>";
                                    //else
                                    _EventXML += "<GoogleGUID>" + response.Items[_i].Id + "</GoogleGUID>";

                                    _EventXML += "<GoogleSequence>" + response.Items[_i].Sequence + "</GoogleSequence>";

                                    //Zendesk - 105238 by SONU - Dated - 30 Aug, 2016                                    
                                    //if (_Confid == "new")
                                    //    _EventXML += "<GoogleConfLastUpdated>" + response.Items[_i].Created.ToString() + "</GoogleConfLastUpdated>";
                                    //else
                                    //    _EventXML += "<GoogleConfLastUpdated>" + response.Items[_i].Updated.ToString() + "</GoogleConfLastUpdated>";

                                    //Change to fix google sync issue
                                    if (_Confid == "new")
                                        _EventXML += "<GoogleConfLastUpdated>" + DateTime.Parse(response.Items[_i].CreatedRaw.ToString()).ToUniversalTime().ToString() + "</GoogleConfLastUpdated>";
                                    else
                                        _EventXML += "<GoogleConfLastUpdated>" + DateTime.Parse(response.Items[_i].UpdatedRaw.ToString()).ToUniversalTime().ToString() + "</GoogleConfLastUpdated>";
                                    //Zendesk - 105238 by SONU - Dated - 30 Aug, 2016

                                    _EventXML += "<locationList>";

                                    for (int _Roomcnt = 0; _Roomcnt < _RoomList.Count; _Roomcnt++)
                                    {
                                        _EventXML += "<selected>";
                                        _EventXML += "<roomQueue>" + _RoomList[_Roomcnt] + "</roomQueue>";
                                        _EventXML += "</selected>";
                                    }
                                    _EventXML += "</locationList>";


                                    _EventXML += "<partys>";
									//ALLOPS-79 Start
                                    if (response.Items[_i].Attendees != null)//ALLOPS-79
                                    {
                                        var _PartyList = response.Items[_i].Attendees.Where(EmailC => !EmailC.Email.Contains("@resource.calendar.google.com")).Select(Email => Email).ToList();
                                        for (int _PartyCnt = 0; _PartyCnt < _PartyList.Count; _PartyCnt++)
                                        {
                                            _EventXML += "<party>";
                                            _EventXML += "<partyID>new</partyID>";
                                            if (!string.IsNullOrEmpty(_PartyList[_PartyCnt].DisplayName))
                                            {
                                                _EventXML += "<partyFirstName>" + _PartyList[_PartyCnt].DisplayName.Trim() + "</partyFirstName>";
                                                _EventXML += "<partyLastName>" + _PartyList[_PartyCnt].DisplayName.Trim() + "</partyLastName>";
                                            }
                                            else
                                            {
                                                _EventXML += "<partyFirstName>" + _PartyList[_PartyCnt].Email.Trim().Split('@')[0].ToString() + "</partyFirstName>";
                                                _EventXML += "<partyLastName>Guest</partyLastName>";
                                            }

                                            _EventXML += "<partyEmail>" + _PartyList[_PartyCnt].Email.Trim() + "</partyEmail>";
                                            _EventXML += "<partyInvite>2</partyInvite>";
                                            _EventXML += "<partyNotify>1</partyNotify>";
                                            _EventXML += "<partyAudVid>1</partyAudVid>";
                                            _EventXML += "<notifyOnEdit>1</notifyOnEdit>";
                                            _EventXML += "</party>";
                                        }
                                    }
									//ALLOPS-79 End
                                    _EventXML += "</partys>";
                                    _EventXML += "</confInfo>";
                                    _EventXML += "</conference>";

                                    log.Trace("INXML: SetConferenceDetails:" + _EventXML);
                                    _EventOutXML = myvrmCom.Operations(MyVRMServer_ConfigPath, "SetConferenceDetails", _EventXML);
                                    log.Trace("OutXML: SetConferenceDetails:" + _EventOutXML);
                                    if (_EventOutXML.IndexOf("<error>") < 0)
                                    {
                                        DataRow dr = ConferenceTable.NewRow();
                                        _Xdoc = new XmlDocument();
                                        _Xdoc.LoadXml(_EventOutXML);

                                        if (_Xdoc.SelectSingleNode("//conferences/conference/confID") != null)
                                            _newConfid = _Xdoc.SelectSingleNode("//conferences/conference/confID").InnerText;

                                        dr["confid"] = _newConfid.Split(',')[0];
                                        dr["InstanceID"] = _newConfid.Split(',')[1];
                                        dr["orgId"] = "";
                                        dr["Recurring"] = "0";
                                        if (_Confid == "new")
                                            dr["GoogleConfLastUpdated"] = response.Items[_i].Created.ToString();
                                        else
                                            dr["GoogleConfLastUpdated"] = response.Items[_i].Updated.ToString();

                                        dr["GoogleGuid"] = response.Items[_i].Id;
                                        dr["Status"] = "1";

                                        ConferenceTable.Rows.Add(dr);
                                    }
                                }else {
                                    //Zendesk - 105238 by SONU - Dated - 10 Sep, 2016
                                    if (!haveAttendee)
                                        _doNotSync = true;
                                }
                            }
                        }

                        //Zendesk - 105238 by SONU - Dated - 10 Sep, 2016
                        if (!_doNotSync) {
                            foreach (DataRow row in rows)
                                row.SetField("Status", "1");
                        }                        
                    }
                    catch (Exception ex)
                    {

                        log.Trace("Error in creating updating conference in myVRM from google" + ex.Message);
                        log.Trace("Error in creating updating conference in myVRM from google Stack" + ex.StackTrace);
                        log.Trace("Error in creating updating conference in myVRM from google inner" + ex.InnerException);
                        log.Trace("Error in creating updating conference in myVRM from google Data" + ex.Data);
                        continue;
                    }
                }
            }
            catch (System.Exception ex)
            {
                errMsg = ex.Message + "_CURRENTDATETIME:" + DateTime.Now.ToLongDateString();
                log.Trace("Error in Retreive Room Queue : " + errMsg);
            }
            return true;
        }

        #endregion

        #region Retreive room queue
        /// <summary>
        /// RetreiveRoomQueue
        /// </summary>
        /// <param name="RoomDetails"></param>
        /// <returns></returns>
        public bool RetreiveRoomQueue(ref List<RoomEWSDetails> RoomDetails)
        {
            string errMsg = "";
            XDocument xdoc = null;
            try
            {
                log.Trace("Retreiving Room information Starts...");

                if (myvrmCom == null)
                    myvrmCom = new ASPIL.VRMServer();

                _inXML = new StringBuilder();
                _inXML.Append("<GetRoomEWSDetails>");
                _inXML.Append("<organizationID>" + _CompanyId + "</organizationID>");
                _inXML.Append("</GetRoomEWSDetails>");
                _OutXML = myvrmCom.Operations(MyVRMServer_ConfigPath, "GetRoomEWSDetails", _inXML.ToString());
                log.Trace("OutXML: GetRoomEWSDetails:" + _OutXML);

                if (_OutXML.IndexOf("<error>") >= 0)
                    log.Trace("Error in GetRoomEWSDetails");
                else
                {
                    if (_OutXML != "")
                    {
                        _Xdoc = new XmlDocument();
                        _Xdoc.LoadXml(_OutXML);

                        xdoc = XDocument.Parse(_OutXML);
                        RoomDetails = (from temp in xdoc.Descendants("RoomDetails").OrderBy(x => x.Element("RoomEWSURL").Value)
                                       select new RoomEWSDetails
                                       {
                                           RoomId = Convert.ToInt32(temp.Element("RoomId").Value),
                                           email = ((string.IsNullOrEmpty(temp.Element("RoomEmail").Value) == true) ? "" : Convert.ToString(temp.Element("RoomEmail").Value)),
                                           domain = ((string.IsNullOrEmpty(temp.Element("RoomDomain").Value) == true) ? "" : Convert.ToString(temp.Element("RoomDomain").Value)),
                                           EWSurl = ((string.IsNullOrEmpty(temp.Element("RoomEWSURL").Value) == true) ? "" : Convert.ToString(temp.Element("RoomEWSURL").Value)),
                                       }).ToList();
                    }
                }

                log.Trace("Retreiving Room information Ends...");
            }
            catch (System.Exception ex)
            {
                errMsg = ex.Message + "_CURRENTDATETIME:" + DateTime.Now.ToLongDateString();
                log.Trace("Error in Retreive Room Queue : " + errMsg);
            }
            return true;
        }

        #endregion

        #region RoomEWSDetails
        public class RoomEWSDetails
        {
            internal int RoomId { get; set; }
            internal string email { get; set; }
            internal string domain { get; set; }
            internal string EWSurl { get; set; }
        }
        #endregion

        #region UserAuthentication
        /// <summary>
        /// GetAuthentication
        /// </summary>
        /// <param name="useremail"></param>
        private void UserAuthentication(string useremail)
        {
            calendarConnection = new CalendarService();
            try
            {
                ClientSecrets secrets = new ClientSecrets
                {
                    ClientId = _ClientID,
                    ClientSecret = _SecretID
                };

                var token = new TokenResponse { RefreshToken = _RefreshToken };
                var credentials = new UserCredential(new GoogleAuthorizationCodeFlow(
                    new GoogleAuthorizationCodeFlow.Initializer
                    {
                        ClientSecrets = secrets
                    }), useremail, token);
                credentials.RefreshTokenAsync(CancellationToken.None);

                calendarConnection = new CalendarService(new BaseClientService.Initializer
                {
                    ApplicationName = "myVRM Google Sync",
                    HttpClientInitializer = credentials

                });
            }
            catch (System.Threading.ThreadAbortException ex)
            {
                log.Trace("Thread abort GetAuthentication Block:" + ex.Message);
            }
            catch (System.Exception exp)
            {
                log.Trace("GetAuthentication Block:" + exp.Message);
            }
        }
        #endregion

        #region DeleteEvents
        /// <summary>
        /// DeleteEvents
        /// </summary>
        /// <param name="ConferenceTable"></param>
        /// <returns></returns>
        public bool DeleteEvents(DataTable ConferenceTable)
        {
            string _EventXML = "", _EventOutXML = "";
            DateTime _GoogleConfLastUpdated = DateTime.UtcNow;
             DateTime Currenttime = DateTime.UtcNow;
            try
            {
                for (int i = 0; i < ConferenceTable.Rows.Count; i++)
                {
                    try
                    {
                        DateTime.TryParse(ConferenceTable.Rows[i]["GoogleConfLastUpdated"].ToString(), out _GoogleConfLastUpdated);
                        if (_GoogleConfLastUpdated.Subtract(Currenttime).TotalMinutes > 0)
                            continue;

                        objRTC = new VRMRTC.VRMRTC();
                        _EventXML = "<login>";
                        _EventXML += "<organizationID>" + ConferenceTable.Rows[i]["orgId"].ToString() + "</organizationID>";
                        _EventXML += "<userID>" + _UserID + "</userID>";
                        _EventXML += "<delconference>";
                        _EventXML += "<conference>";
                        //if (!response.Items[_i].Id.Contains('_') && _Recur == "1")
                        //    _EventXML += "<confID>" + _Confid + "</confID>";
                        //else
                        _EventXML += "<confID>" + ConferenceTable.Rows[i]["confid"] + "," + ConferenceTable.Rows[i]["InstanceID"] + "</confID>";
                        _EventXML += "<reason></reason>";
                        _EventXML += "<deny>Delete</deny>";
                        _EventXML += "<fromAPI>1</fromAPI>";
                        _EventXML += "</conference>";
                        _EventXML += "</delconference>";
                        _EventXML += "</login>";

                        _EventOutXML = myvrmCom.Operations(MyVRMServer_ConfigPath, "DeleteConference", _EventXML);
                        log.Trace("OutXML: DeleteConference:" + _EventOutXML);

                    }
                    catch (Exception ex)
                    {

                        log.Trace("Error in Deleting conference in myVRM from google" + ex.Message);
                        log.Trace("Error in Deleting conference in myVRM from google Stack" + ex.StackTrace);
                        log.Trace("Error in Deleting conference in myVRM from google inner" + ex.InnerException);
                        log.Trace("Error in Deleting conference in myVRM from google Data" + ex.Data);
                        continue;
                    }
                }
            }
            catch (System.Exception ex)
            {
                errMsg = ex.Message + "_CURRENTDATETIME:" + DateTime.Now.ToLongDateString();
                log.Trace("Error in Deleting conference : " + errMsg);
            }
            return true;
        }

        #endregion

        //ALLDEV-856 End
    }
}
