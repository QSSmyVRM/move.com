//ZD 100147 Start
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886 End
using System;
using System.Globalization;
using System.Collections;
using System.Xml;
using System.CodeDom;

namespace myVRM.ExcelXmlWriter
{
	public sealed class CrnCollection : CollectionBase, IWriter, ICodeWriter
	{
		// Fields
		internal static int GlobalCounter;

		// Methods
		internal CrnCollection()
		{
		}

		public Crn Add()
		{
			Crn item = new Crn();
			this.Add(item);
			return item;
		}

		public int Add(Crn item)
		{
			return base.InnerList.Add(item);
		}

		void ICodeWriter.WriteTo(CodeTypeDeclaration type, CodeMemberMethod method, CodeExpression targetObject)
		{
			for (int i = 0; i < base.InnerList.Count; i++)
			{
				Crn crn = this[i];
				string name = "Crn" + GlobalCounter++.ToString(CultureInfo.InvariantCulture);
				CodeVariableDeclarationStatement statement = new CodeVariableDeclarationStatement(typeof(Crn), name, new CodeMethodInvokeExpression(new CodePropertyReferenceExpression(targetObject, "Operands"), "Add", new CodeExpression[0]));
				method.Statements.Add(statement);
				((ICodeWriter) crn).WriteTo(type, method, new CodeVariableReferenceExpression(name));
			}
		}

		void IWriter.WriteXml(XmlWriter writer)
		{
			for (int i = 0; i < base.InnerList.Count; i++)
			{
				((IWriter) base.InnerList[i]).WriteXml(writer);
			}
		}

		public bool Contains(Crn link)
		{
			return base.InnerList.Contains(link);
		}

		public void CopyTo(Crn[] array, int index)
		{
			base.InnerList.CopyTo(array, index);
		}

		public int IndexOf(Crn item)
		{
			return base.InnerList.IndexOf(item);
		}

		public void Insert(int index, Crn item)
		{
			base.InnerList.Insert(index, item);
		}

		public void Remove(string item)
		{
			base.InnerList.Remove(item);
		}

		// Properties
		public Crn this[int index]
		{
			get
			{
				return (Crn) base.InnerList[index];
			}
		}
	}
}
