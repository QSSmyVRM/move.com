﻿/* Copyright (C) 2015 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.		
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Data;
using System.Xml;
using System.Text;
using System.IO;
using System.Collections;
using System.ComponentModel;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Xml.Schema;
using System.Collections.Generic;

namespace ns_RoomEndpointImport
{
    public partial class RoomEndpointImport : System.Web.UI.Page
    {

        #region protected data members
        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.DataGrid dgEndpoints;
        protected System.Web.UI.WebControls.DataGrid dgRooms;        
        private myVRMNet.NETFunctions obj;
        private ns_Logger.Logger log;
        protected System.Web.UI.WebControls.Button btnImport;
        protected System.Web.UI.HtmlControls.HtmlButton btnGoBack;
        protected System.Web.UI.HtmlControls.HtmlTableRow trEndpoints;
        protected System.Web.UI.HtmlControls.HtmlTableRow trRooms;
        protected System.Web.UI.WebControls.Label lblNoItems;
        protected System.Web.UI.WebControls.Table tblNoRecords;
        protected System.Web.UI.WebControls.Table tblPage;


        protected DataSet ds = null;
        XmlWriter _xWriter = null;
        XmlWriterSettings _xSettings = null;
        static int totalPages;
        static int pageNo;
        #endregion

        #region Constructor
        public RoomEndpointImport()
        {
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
        }
        #endregion

        #region UserFilterType

        private class UserFilterType
        {
            public const int Endpoints = 1;
            public const int Rooms = 2;
            
        }
        #endregion

        #region InitializeCulture
        protected override void InitializeCulture()
        {
            if (Session["UserCulture"] != null)
            {
                UICulture = Session["UserCulture"].ToString();
                Culture = Session["UserCulture"].ToString();
                base.InitializeCulture();
            }
        }

        #endregion

        #region Page_Load
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                
                btnImport.Visible = true;
                if (Session["admin"].ToString() == "3")
                    btnImport.Visible = false;
                if (!IsPostBack)
                    BindData();
            }
            catch (Exception ex)
            {
                log.Trace("BindData" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;
            }
        }
        #endregion

        #region BindData
        private void BindData()
        {
            StringBuilder _InXML = new StringBuilder();
            XmlDocument xmlDoc = new XmlDocument();
            int SelectedOptions = 1;
            string cmd = "";
            XmlTextReader xtr;
            ds = new DataSet();
            Session["totalPages"] = "";
            Session["pageNo"] = "";
            Session["tblPage"] = "";
            try
            {
                string pageNoS = "1";
                if (Request.QueryString["pageNo"] != null)
                    pageNoS = Request.QueryString["pageNo"].ToString().Trim();
                                
                if (Session["SelectedOptions"] != null)
                    SelectedOptions = Convert.ToInt32(Session["SelectedOptions"].ToString());

                _xSettings = new XmlWriterSettings();
                _xSettings.OmitXmlDeclaration = true;
                using (_xWriter = XmlWriter.Create(_InXML, _xSettings))
                {
                    if (SelectedOptions == UserFilterType.Endpoints)
                    {
                        _xWriter.WriteStartElement("GetSyncEndpoints");
                        cmd = "GetSyncEndpoints";

                    }
                    else if (SelectedOptions == UserFilterType.Rooms)
                    {
                        _xWriter.WriteStartElement("GetSyncRooms");
                        cmd = "GetSyncRooms";
                    }
                    
                    _xWriter.WriteString(obj.OrgXMLElement());

                    if (pageNoS != null)
                        _xWriter.WriteElementString("PageNo", pageNoS);
                    else
                        _xWriter.WriteElementString("PageNo", string.Empty);

                    _xWriter.WriteFullEndElement();
                    _xWriter.Flush();
                }
                _InXML = _InXML.Replace("&lt;", "<")
                                      .Replace("&gt;", ">");

                string outXML = obj.CallMyVRMServer(cmd, _InXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());

                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                    return;
                }
                else
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    XmlNodeList nodes = null;

                    if (SelectedOptions == UserFilterType.Endpoints)                    
                        nodes = xmldoc.SelectNodes("//GetSyncEndpoints/SyncEndpoint");
                    else if (SelectedOptions == UserFilterType.Rooms)
                        nodes = xmldoc.SelectNodes("//GetSyncRooms/SyncRoom");

                    

                    foreach (XmlNode node in nodes)
                    {
                        xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                        ds.ReadXml(xtr, XmlReadMode.InferSchema);
                    }

                    trEndpoints.Visible = false;
                    trRooms.Visible = false;
                    tblNoRecords.Visible = true;

                    switch (SelectedOptions)
                    {
                        case UserFilterType.Endpoints:
                            
                            nodes = xmldoc.SelectNodes("//GetSyncEndpoints/SyncEndpoint");

                            if (nodes.Count > 0)
                            {
                                totalPages = Convert.ToInt32(xmldoc.SelectSingleNode("//GetSyncEndpoints/TotalPages").InnerText);
                                pageNo = Convert.ToInt32(xmldoc.SelectSingleNode("//GetSyncEndpoints/PageNo").InnerText);

                                if (totalPages > 1)
                                {
                                    obj.DisplayPaging(totalPages, pageNo, tblPage, "RoomEndpointImport.aspx?");
                                    Session["totalPages"] = totalPages;
                                    Session["pageNo"] = pageNo;
                                    Session["tblPage"] = tblPage;
                                }

                                tblNoRecords.Visible = false;
                                trEndpoints.Visible = true;
                                dgEndpoints.DataSource = ds;
                                dgEndpoints.DataBind();
                            }
                            break;
                        case UserFilterType.Rooms:
                            
                            nodes = xmldoc.SelectNodes("//GetSyncRooms/SyncRoom");

                            if (nodes.Count > 0)
                            {
                                totalPages = Convert.ToInt32(xmldoc.SelectSingleNode("//GetSyncRooms/TotalPages").InnerText);
                                pageNo = Convert.ToInt32(xmldoc.SelectSingleNode("//GetSyncRooms/PageNo").InnerText);

                                if (totalPages > 1)
                                {
                                    obj.DisplayPaging(totalPages, pageNo, tblPage, "RoomEndpointImport.aspx?");
                                    Session["totalPages"] = totalPages;
                                    Session["pageNo"] = pageNo;
                                    Session["tblPage"] = tblPage;
                                }

                                tblNoRecords.Visible = false;
                                trRooms.Visible = true;
                                dgRooms.DataSource = ds;
                                dgRooms.DataBind();
                            }
                            break;
                        default:
                            break;
                    }                   
                   
                }

            }        
            catch (Exception ex)
            {
                log.Trace("BindData" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;
            }
        }
        #endregion       

        #region ImportData
        /// <summary>
        /// ImportData
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImportData(object sender, EventArgs e)
        {
            StringBuilder _InXML = new StringBuilder();
            XmlDocument xmlDoc = new XmlDocument();
            CheckBox chkSelect = null;
            bool isUserSelected = false;
            int selectedoption = 1, totalpage = 0, PageNO = 0;
            string cmd = "";
            Button btn = sender as Button;
            bool isrecords = false;
                
            try
            {
                if (Session["SelectedOptions"] != null)
                    selectedoption = Convert.ToInt32(Session["SelectedOptions"].ToString());

                _xSettings = new XmlWriterSettings();
                _xSettings.OmitXmlDeclaration = true;
                using (_xWriter = XmlWriter.Create(_InXML, _xSettings))
                {
                    if (selectedoption == UserFilterType.Endpoints)
                    {
                        _xWriter.WriteStartElement("SetSyncEndpoints");
                        cmd = "SetSyncEndpoints";

                    }
                    else if (selectedoption == UserFilterType.Rooms)
                    {
                        _xWriter.WriteStartElement("SetSyncRooms");
                        cmd = "SetSyncRooms";
                    }
                    
                    _xWriter.WriteElementString("userID", Session["userID"].ToString());
                    _xWriter.WriteString(obj.OrgXMLElement());


                    if (selectedoption == UserFilterType.Endpoints)                    
                        _xWriter.WriteStartElement("EndpointIDs");  
                    else if (selectedoption == UserFilterType.Rooms)
                        _xWriter.WriteStartElement("RoomIDs");


                    switch (selectedoption)
                    {
                        case UserFilterType.Endpoints:
                            for (int i = 0; i < dgEndpoints.Items.Count; i++)
                            {
                                isrecords = true;
                                chkSelect = (CheckBox)dgEndpoints.Items[i].FindControl("ChkSelect");
                                if (chkSelect != null)
                                {
                                    if (chkSelect.Checked)
                                    {
                                        _xWriter.WriteElementString("EndpointID", chkSelect.Attributes["EndpointID"]);
                                        isUserSelected = true;
                                    }
                                }
                            }
                            if (Session["totalPages"] != null)
                                int.TryParse(Session["totalPages"].ToString(), out totalpage);

                            if (Session["pageNo"] != null)
                                int.TryParse(Session["pageNo"].ToString(), out PageNO);

                            if (totalPages > 1)
                                obj.DisplayPaging(totalPages, pageNo, tblPage, "RoomEndpointImport.aspx?");

                            tblNoRecords.Visible = false;
                            trEndpoints.Visible = true;
                            break;
                        case UserFilterType.Rooms:
                            for (int i = 0; i < dgRooms.Items.Count; i++)
                            {
                                isrecords = true;
                                chkSelect = (CheckBox)dgRooms.Items[i].FindControl("ChkSelect");
                                if (chkSelect != null)
                                {
                                    if (chkSelect.Checked)
                                    {
                                        _xWriter.WriteElementString("RoomID", chkSelect.Attributes["RoomID"]);
                                        isUserSelected = true;
                                    }
                                }
                            }
                            if (Session["totalPages"] != null)
                                int.TryParse(Session["totalPages"].ToString(), out totalpage);

                            if (Session["pageNo"] != null)
                                int.TryParse(Session["pageNo"].ToString(), out PageNO);


                            if (totalPages > 1)
                                obj.DisplayPaging(totalPages, pageNo, tblPage, "RoomEndpointImport.aspx?");

                            tblNoRecords.Visible = false;
                            trRooms.Visible = true;
                            break;
                    }

                    _xWriter.WriteFullEndElement();
                    _xWriter.WriteFullEndElement();
                    _xWriter.Flush();
                }
                if (!isUserSelected)
                {
                    if(isrecords)
                        errLabel.Text = obj.GetTranslatedText("Please select a record for import.");
                    else
                        errLabel.Text = obj.GetTranslatedText("No records found");
                    errLabel.Visible = true;
                    return;
                }
                _InXML = _InXML.Replace("&lt;", "<")
                                      .Replace("&gt;", ">");

                string outXML = obj.CallMyVRMServer(cmd, _InXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") >= 0)
                {
                    BindData();
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                    return;
                }
                else
                {
                    errLabel.Text = obj.ShowSuccessMessage();
                    errLabel.Visible = true;
                    BindData();
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message + " : " + ex.StackTrace);
            }
        }
        #endregion

        #region DataGrid Item DataBound Event Handler

        protected void dgEndpointList_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    DataRowView row = e.Item.DataItem as DataRowView;

                    if (row != null)
                    {
                        CheckBox select = (CheckBox)e.Item.FindControl("ChkSelect");
                        if (select != null)
                        {
                            if (Session["SelectedOptions"] != null)
                            {
                                if (Session["SelectedOptions"].ToString() == "1")                                
                                    select.Attributes.Add("EndpointID", row["EndpointID"].ToString().Trim());
                                else if (Session["SelectedOptions"].ToString() == "2")
                                    select.Attributes.Add("RoomID", row["RoomID"].ToString().Trim());                                
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region GoBack
        /// <summary>
        /// GoBack
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void GoBack(Object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("mainadministrator.aspx");
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message + " : " + ex.StackTrace);
            }
        }
        #endregion

    }
}