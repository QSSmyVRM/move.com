﻿/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Xml;
using System.Web.UI;
using System.Collections;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;


namespace MyVRMNet
{
    public partial class LobbyManagement : System.Web.UI.Page
    {
        #region Protected Members

        ns_Logger.Logger log;
        myVRMNet.NETFunctions obj;

        protected System.Web.UI.HtmlControls.HtmlTableCell tdLobby;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdCalender;
        protected System.Web.UI.HtmlControls.HtmlTableCell tddashboard;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdNewConference;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdMypreferences;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdReports;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdOrgoptions;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdUserEmailQueue;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdOrgSettings;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdSysDiagnostic;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdSiteSettings;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdOrgUI;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdSysAudit;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdRooms;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdGroups;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdTemplate;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdInventory;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdHouseKeeping;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdCatering;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdtiers;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdEndpoint;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdMCU;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdInventoryWO;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdHouseKeepingWO;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdCateringWO;
        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.Button saveLink;
        protected System.Web.UI.WebControls.HiddenField selectedValues;
        protected System.Web.UI.HtmlControls.HtmlInputHidden iconCount;
        protected System.Web.UI.HtmlControls.HtmlInputHidden cancelPage;
        protected System.Web.UI.HtmlControls.HtmlInputHidden themeColor; // ZD 101006

        #endregion

        #region Private Data Members

        bool hasCalendar, hasCallMonitor;
        bool hasConference;
        bool hasExpConference; // ZD 101233
        bool hasTemplates;
        bool hasHistory;
        bool hasAVWO;
        bool hasCATWO;
        bool hasHKWO;
        bool hasAVManagement;
        bool hasCATManagement;
        bool hasHKManagement;
        bool hasSearch;
        bool isGeneral;

        bool hasPreferences;
        bool hasReports;
        bool hasGroups;

        bool hasEndpoints;
        bool hasDiagnostics;
        bool hasMCUs;
        bool hasMCUsLoadBalance; //ZD 100040


        bool hasRooms;
        bool hasTiers;
        bool hasFLPlans; //ZD 102123

        bool hasActiveUsers;
        bool hasBulkTool;
        bool hasDepartments;
        bool hasGuests;
        bool hasInactiveUsers;
        bool hasLDAPdirectoryImport, hasLDAPGroup; //ALLBUGS-100
        bool hasRoles;
        bool hasAdminTemplates;

        bool hasOptions;
        bool hasAdminSettings;

        bool hasSiteSettings;

        bool hasAudit;

        string selectedOptions = string.Empty;
        String userid = "11";
        bool isVNOCAdmin = false; //FB 2670
        bool hasEM7; //FB 2885
        bool hasAudioBridge; //FB 2885
        string themeType="", curThemeColor=""; // ZD 101006
        #endregion

        #region LobbyManagement

        public LobbyManagement()
        {
            log = new ns_Logger.Logger();
            obj = new myVRMNet.NETFunctions();
        }

        #endregion

        //ZD 101022
        #region InitializeCulture
        protected override void InitializeCulture()
        {
            if (Session["UserCulture"] != null)
            {
                UICulture = Session["UserCulture"].ToString();
                Culture = Session["UserCulture"].ToString();
                base.InitializeCulture();
            }
        }
        #endregion

        #region Page_Load

        private void Page_Load(object sender, System.EventArgs e)
        {
            // ZD 100263 Starts
            string confChkArg = "";
            if (obj == null)
                obj = new myVRMNet.NETFunctions();
            obj.URLConformityCheck(Request.Url.AbsoluteUri.ToLower());

            if (Request.QueryString["t"] != null)
                confChkArg = "?t=" + Request.QueryString["t"].ToString();

            obj.AccessConformityCheck("lobbymanagement.aspx" + confChkArg);

            // ZD 100263 Ends

            if (Session["userID"] == null) //FB 2303
                Response.Redirect("genlogin.aspx");

            themeType = Session["ThemeType"].ToString(); // ZD 101006
            string[] themeColors = { "#888888", "#4277B3", "#CD2522" };
            curThemeColor = themeColors[Int32.Parse(themeType)];
            themeColor.Value = curThemeColor;

            userid = Session["userID"].ToString();
            if (Request.QueryString["t"] != null)
                if (Request.QueryString["t"].Trim() == "au")
                    if (Session["UserToEdit"] != null)
                        userid = Session["UserToEdit"].ToString();

            if (Request.QueryString["P"] != null && Request.QueryString["t"] != null)
                cancelPage.Value = "ManageUserProfile.aspx?t=1";
            else if (Request.QueryString["P"] != null)
                cancelPage.Value = "ManageUserProfile.aspx";
            else
                cancelPage.Value = "SettingSelect2.aspx";

            //FB 2670 Starts 
            if (Session["admin"].ToString() == "3")
                isVNOCAdmin = true;
            //FB 2670 Ends
			
            if (!IsPostBack)
            {
                try
                {
                    XmlDocument xd = new XmlDocument();
                    ArrayList colNames = new ArrayList();
                    StringBuilder inXML = new StringBuilder();
                    XmlNodeList nodes = null;
                    String outXML = "";

                    string[] mary = Session["sMenuMask"].ToString().Split('-');
                    string[] mmary = mary[1].Split('+');
                    string[] ccary = mary[0].Split('*');
                    int topMenu = Convert.ToInt32(ccary[1]);
                    int adminMenu = Convert.ToInt32(mmary[10].Split('*')[1]); // Convert.ToInt32(mmary[1].Split('*')[1]); ZD 101233
                    int subMenu;
                    int orgMenu = Convert.ToInt32(mmary[11].Split('*')[1]); // Convert.ToInt32(mmary[2].Split('*')[1]); ZD 101233

                    int calenderMenu = Convert.ToInt32(mmary[0].Split('*')[1]);
                    //hasCalendar = Convert.ToBoolean(topMenu & 64);
                    hasExpConference = Convert.ToBoolean(Int32.Parse(mmary[9].Split('*')[1]) & 2); // ZD 101233
                    hasConference = Convert.ToBoolean(Int32.Parse(mmary[9].Split('*')[1]) & 1); // Convert.ToBoolean(topMenu & 32); ZD 101233
                    Boolean hasFeedback = Convert.ToBoolean(Int32.Parse(mary[2].Split('*')[1]) & 16); // Convert.ToBoolean(Int32.Parse(mary[2].Split('*')[1]) & 8); ZD 101233
                    Session.Add("hasFeedback", hasFeedback);
                    Boolean hasHelp = Convert.ToBoolean(Int32.Parse(mary[2].Split('*')[1]) & 8); // Convert.ToBoolean(Int32.Parse(mary[2].Split('*')[1]) & 4); ZD 101233
                    Session.Add("hasHelp", hasHelp);
                    hasHistory = hasConference;
                    hasSearch = Convert.ToBoolean(Int32.Parse(mary[2].Split('*')[1]) & 64); // Convert.ToBoolean(topMenu & 32); ZD 101233


                    if (Convert.ToBoolean(topMenu & 64))
                    {
                        hasCalendar = Convert.ToBoolean(calenderMenu & 8); // Convert.ToBoolean(calenderMenu & 2); ZD 101233
                        hasCallMonitor = Convert.ToBoolean(calenderMenu & 4); // Convert.ToBoolean(calenderMenu & 1); ZD 101233
                    }

                    if (Convert.ToBoolean(topMenu & 16))
                    {
                        hasPreferences = Convert.ToBoolean(adminMenu & 8);
                        hasReports = Convert.ToBoolean(adminMenu & 4);
                        hasTemplates = Convert.ToBoolean(adminMenu & 2);
                        hasGroups = Convert.ToBoolean(adminMenu & 1);
                    }

                    if (Convert.ToBoolean(orgMenu & 128)) //FB 2593 //FB 2885 Starts
                    {
                        subMenu = Convert.ToInt32(mmary[12].Split('*')[1]); // Convert.ToInt32(mmary[3].Split('*')[1]); ZD 101233
                        hasEndpoints = Convert.ToBoolean(subMenu & 32); //ZD 100040 start
                        hasDiagnostics = Convert.ToBoolean(subMenu & 16);
                        hasMCUs = Convert.ToBoolean(subMenu & 8);
                        hasMCUsLoadBalance = Convert.ToBoolean(subMenu & 4); //ZD 100040 end
                        hasAudioBridge = Convert.ToBoolean(subMenu & 2);
                        hasEM7 = Convert.ToBoolean(subMenu & 1);
                    } //FB 2885 Ends

                    if (Convert.ToBoolean(orgMenu & 64)) //FB 2593 //FB 2885
                    {
                        subMenu = Convert.ToInt32(mmary[13].Split('*')[1]); // Convert.ToInt32(mmary[4].Split('*')[1]); ZD 101233
                        hasRooms = Convert.ToBoolean(subMenu & 4);
                        hasTiers = Convert.ToBoolean(subMenu & 2);
                        hasFLPlans = Convert.ToBoolean(subMenu & 1); //ZD 102123                        
                    }

                    if (Convert.ToBoolean(orgMenu & 32))  //FB 2593 //FB 2885
                    {
                        subMenu = Convert.ToInt32(mmary[14].Split('*')[1]); // Convert.ToInt32(mmary[5].Split('*')[1]); ZD 101233
                        //ALLBUS-100 Starts

                        hasActiveUsers = Convert.ToBoolean(subMenu & 256);
                        hasBulkTool = Convert.ToBoolean(subMenu & 128);
                        hasDepartments = Convert.ToBoolean(subMenu & 64);
                        hasGuests = Convert.ToBoolean(subMenu & 32);
                        hasInactiveUsers = Convert.ToBoolean(subMenu & 16);
                        hasLDAPdirectoryImport = Convert.ToBoolean(subMenu & 8);
                        hasLDAPGroup = Convert.ToBoolean(subMenu & 4);
                        hasRoles = Convert.ToBoolean(subMenu & 2);
                        hasAdminTemplates = Convert.ToBoolean(subMenu & 1);

                        //ALLBUS-100 Ends
                    }

                    if (Convert.ToBoolean(orgMenu & 16)) //FB 2593 //FB 2885
                        hasOptions = true;

                    if (Convert.ToBoolean(orgMenu & 8)) //FB 2593 //FB 2885
                        hasAdminSettings = true;

                    if (Convert.ToBoolean(orgMenu & 4))
                    {
                        subMenu = Convert.ToInt32(mmary[15].Split('*')[1]); // Convert.ToInt32(mmary[6].Split('*')[1]); ZD 101233
                        hasAVManagement = Convert.ToBoolean(subMenu & 2);
                        hasAVWO = Convert.ToBoolean(subMenu & 1);
                    }

                    if (Convert.ToBoolean(orgMenu & 2))
                    {
                        subMenu = Convert.ToInt32(mmary[16].Split('*')[1]); // Convert.ToInt32(mmary[7].Split('*')[1]); ZD 101233
                        hasCATManagement = Convert.ToBoolean(subMenu & 2);
                        hasCATWO = Convert.ToBoolean(subMenu & 1);
                    }

                    if (Convert.ToBoolean(orgMenu & 1))
                    {
                        subMenu = Convert.ToInt32(mmary[17].Split('*')[1]); // Convert.ToInt32(mmary[8].Split('*')[1]); ZD 101233
                        hasHKManagement = Convert.ToBoolean(subMenu & 2);
                        hasHKWO = Convert.ToBoolean(subMenu & 1);
                    }

                    if (Session["UsrCrossAccess"].ToString().Equals("1") && Session["admin"].ToString().Trim().Equals("2"))  //FB 2670 
                        hasSiteSettings = true;

                    string useridses = Session["userID"].ToString(); // two digit number

                    //string userLevel2 = Session["UsrLevel2"].ToString(); // value 2 for user level 2 - audit
                    //string xpressUser = Session["isExpressUser"].ToString(); // value 1 for xpress user
                    Session["multisiloOrganizationID"] = null; //FB 2274

                    DataTable table = new DataTable();
                    String language = "en";
                    if (Session["language"] != null)
                        language = Session["language"].ToString();

                    inXML.Append("<GetIconReference>");
                    inXML.Append(obj.OrgXMLElement());
                    inXML.Append("<userid>" + userid + "</userid>");
                    inXML.Append("<userLang>" + language + "</userLang>");
                    inXML.Append("</GetIconReference>");

                    outXML = obj.CallMyVRMServer("GetIconsReference", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                    outXML = outXML.Replace("&", "&amp;");
                    xd.LoadXml(outXML);

                    if (xd.SelectNodes("//IconsReference/Icons/Icon") != null)
                    {
                        colNames.Add("userID");
                        colNames.Add("iconID");
                        colNames.Add("iconUri");
                        colNames.Add("SelectedColor"); // ZD 101006
                        colNames.Add("SelectedImage"); 
                        colNames.Add("defaultLabel");
                        colNames.Add("userLabel");
                        colNames.Add("iconTarget");
                        colNames.Add("gridPosition");

                        nodes = xd.SelectNodes("//IconsReference/Icons/Icon");
                        table = obj.LoadDataTable(nodes, colNames);
                    }


                    if (Session["roomModule"].ToString().Equals("0"))
                    {
                        hasAVManagement = false;
                        hasAVWO = false;
                    }
                    if (Session["foodModule"].ToString().Equals("0"))
                    {
                        hasCATManagement = false;
                        hasCATWO = false;
                    }
                    if (Session["hkModule"].ToString().Equals("0"))
                    {
                        hasHKManagement = false;
                        hasHKWO = false;
                    }


                    if (Session["isExpressUser"].ToString().Equals("1"))
                    {
                        selectedOptions = "";
                    }
                    else
                    {

                        if (hasBulkTool == false) table.Rows[0][0] = "0";
						//ZD 101388 Starts
                        /*
                        if (hasCalendar == false)
                        {
                            table.Rows[1][0] = "0";
                            table.Rows[2][0] = "0";
                            table.Rows[3][0] = "0";
                            table.Rows[4][0] = "0";
                            table.Rows[5][0] = "0";
                            table.Rows[47][0] = "0"; // ZD 101233
                        }
                        */
                        if (!Session["hasOngoing"].ToString().Equals("1"))
                            table.Rows[1][0] = "0";
                        if (!Session["hasReservations"].ToString().Equals("1"))
                            table.Rows[2][0] = "0";
                        if (!Session["hasPublic"].ToString().Equals("1"))
                            table.Rows[3][0] = "0";
                        if (!Session["hasPending"].ToString().Equals("1"))
                            table.Rows[4][0] = "0";
                        if (!Session["hasApproval"].ToString().Equals("1"))
                            table.Rows[5][0] = "0";
                        if (!Session["hasOnMCU"].ToString().Equals("1"))
                            table.Rows[47][0] = "0";
                        //ZD 101388 Ends
                        //ZD 102532 starts
                        if (!Session["hasWaitList"].ToString().Equals("1"))
                            table.Rows[50][0] = "0";
                        // ZD 102532 Ends

                        if (Session["EnablePublicRooms"] != null) //FB 2594
                        {
                            if (Session["EnablePublicRooms"].ToString() == "1")
                                table.Rows[5][0] = "0";
                        }

                        if (hasAVWO == false) table.Rows[6][0] = "0";
                        if (hasCATWO == false) table.Rows[7][0] = "0";
                        if (hasHKWO == false) table.Rows[8][0] = "0";

                        if (hasAVManagement == false) table.Rows[9][0] = "0";
                        if (hasCATManagement == false) table.Rows[10][0] = "0";
                        if (hasHKManagement == false) table.Rows[11][0] = "0";

                        if (hasConference == false)
                        {
                            table.Rows[12][0] = "0";
                        }
						//ZD 101388 Starts
                        if (!Convert.ToBoolean(Convert.ToInt32(mmary[0].Split('*')[1]) & 2))
                        {
                            table.Rows[13][0] = "0";
                        }
						//ZD 101388 End
                        if (hasExpConference == false) // ZD 101233
                        {
                            table.Rows[18][0] = "0";
                        }
                        //FB 2670 Starts 
                        if (isVNOCAdmin)
                        {
                            hasRoles = false;
                            hasSiteSettings = false;
                        }
                        //FB 2670 Ends

                        if (hasAdminSettings == false) table.Rows[14][0] = "0";
                        if (hasEndpoints == false) table.Rows[16][0] = "0";
                        if (hasDiagnostics == false) table.Rows[17][0] = "0";
                        if (hasMCUsLoadBalance == false) table.Rows[51][0] = "0"; //ZD 100040
                        //18 //Express
                        //ZD 101525
                        if (hasLDAPdirectoryImport == false)
                        {
                            table.Rows[21][0] = "0";
                            table.Rows[47][0] = "0"; // ZD 101948
                        }
                        if (hasLDAPGroup == false) table.Rows[48][0] = "0"; //ALLBUGS-100
                        //22 //Lobby
                        if (hasOptions == false) table.Rows[23][0] = "0";
                        if (hasMCUs == false) table.Rows[24][0] = "0";
                        if (hasDepartments == false) table.Rows[25][0] = "0";
                        if (hasGroups == false) table.Rows[26][0] = "0";
                        if (hasSiteSettings == false)
                        {
                            table.Rows[27][0] = "0";
                            table.Rows[28][0] = "0";
                        }

                        if (hasRooms == false) table.Rows[29][0] = "0";
                        if (hasTiers == false) table.Rows[30][0] = "0";
                        if (hasFLPlans == false) table.Rows[49][0] = "0";//ZD 102123
                        if (hasTemplates == false) table.Rows[31][0] = "0";

                        if (hasActiveUsers == false) table.Rows[32][0] = "0";
                        if (hasGuests == false) table.Rows[33][0] = "0";
                        if (hasInactiveUsers == false) table.Rows[34][0] = "0";
                        if (hasPreferences == false)
                        {
                            table.Rows[15][0] = "0"; // CustomizeEmail
                            table.Rows[35][0] = "0";
                        }

                        if (hasRoles == false) table.Rows[36][0] = "0";
                        if (hasAdminSettings == false)
                        {
                            table.Rows[37][0] = "0";
                            table.Rows[38][0] = "0";
                            table.Rows[39][0] = "0";
                            table.Rows[40][0] = "0";
                            table.Rows[41][0] = "0";  //FB 2303
                        }
                        else
                            table.Rows[38][0] = "0"; // FB 3047

                        if (hasCalendar == false)
                        {
                            table.Rows[42][0] = "0";
                            table.Rows[43][0] = "0";
                        }

                        //FB 2598 Starts jan 07
                        //if (Session["EnableCallmonitor"].ToString() == "0" || (Session["admin"].ToString() != "2" && Session["admin"].ToString() != "3")) //FB 2670
                        if (Session["EnableCallmonitor"].ToString() == "0" || hasCallMonitor == false)
                        {
                            table.Rows[44][0] = "0";
                            table.Rows[45][0] = "0";
                        }

                        //FB 2598 Ends

                        //FB 2633 Starts //FB 2885
                        if (!hasEM7 || (Session["EnableEM7"].ToString() == "0") || (Session["admin"].ToString() != "2" && Session["admin"].ToString() != "3"))
                        {
                            table.Rows[46][0] = "0";
                        }
                        //FB 2633 Ends

                        //FB 2593 Starts
                        if (hasReports == false)
                        {
                            table.Rows[19][0] = "0";
                            table.Rows[20][0] = "0";
                        }
                        if (Session["EnableAdvancedReport"].ToString() == "0") //FB 2593
                            table.Rows[20][0] = "0"; //Advanced Reports
                        //FB 2593 End
                    }

                    // FB 2643 Starts
                    DataRow[] results = table.Select("gridPosition <> '0'");
                    if (results.Length < 1)
                    {
                        table.Rows[22][6] = "11";
                        table.Rows[22][5] = table.Rows[22][3];
                        int j = 0;
                        for (int i = 0; i < table.Rows.Count; i++)
                        {
                            if (i == 22) continue;
                            if (table.Rows[i][0].ToString() != "0")
                            {
                                table.Rows[i][6] = (j + 12).ToString();
                                table.Rows[i][5] = table.Rows[i][3];
                                j++;
                                if (j == 24) break;
                            }
                        }
                    }
                    // FB 2643 Ends

                    iconCount.Value = (table.Rows.Count + 1).ToString();
                    BindIcons(table);


                    Session.Add("hasCalendar", hasCalendar);

                    /*if (!hasCalendar)
                        tdCalender.Attributes.Add("style", "display:none");
                
                    if (!hasTemplates)
                        tdTemplate.Attributes.Add("style", "display:none");

                    if (!hasConference)
                    {
                        tdNewConference.Attributes.Add("style", "display:none");
                        tddashboard.Attributes.Add("style", "display:none");
                    }

                    if (!hasAVManagement)
                        tdInventory.Attributes.Add("style", "display:none");

                    if (!hasHKManagement)
                        tdHouseKeeping.Attributes.Add("style", "display:none");

                    if (!hasCATManagement)
                        tdCatering.Attributes.Add("style", "display:none");

                    if (!hasAVWO)
                        tdInventoryWO.Attributes.Add("style", "display:none");

                    if (!hasCATWO)
                        tdCateringWO.Attributes.Add("style", "display:none");

                    if (!hasHKWO)
                        tdHouseKeepingWO.Attributes.Add("style", "display:none");*/
                    //FB 2670
                    if (Session["admin"].ToString() == "3")
                    {
                        if (Request.QueryString["t"] != null)
                        {
                            if (Request.QueryString["t"].Trim() == "au")
                            {
                                
                                //saveLink.ForeColor = System.Drawing.Color.Gray;
                                //saveLink.Attributes.Add("Class", "btndisable");// FB 2796
                                //saveLink.Attributes.Add("disabled", "disabled");
                                //ZD 100263
                                saveLink.Visible = false;
                            }
                        }
                        else
                            saveLink.Attributes.Add("Class", "altLongBlueButtonFormat");
                    }
                    else
                        saveLink.Attributes.Add("Class", "altLongBlueButtonFormat");// FB 2796
                }
                catch (Exception ex)
                {
                    errLabel.Visible = true;
                    log.Trace(ex.StackTrace);
                }
            }
        }

        #endregion

        #region Bind Icons

        private void BindIcons(DataTable table)
        {
            try
            {
                // FB 2863 Starts
                DataView dv = table.DefaultView;
                dv.Sort = "defaultLabel";
                table = dv.ToTable();
                // FB 2863 Ends

                string iconurl = "", iconid = "", htmlContent = "", htmlContentx = "", htmlContent2 = "", posid = "", ancID = "", imgid = "", chk = "", iconSelColor="", iconSelImage=""; // FB 2779 // ZD 101006
                int i = 11;
                foreach (DataRow dt in table.Rows)
                {
                    if (dt["userID"].ToString() != "0")
                    {
                        iconid = dt["iconID"].ToString();
                        ancID = "a" + dt["iconID"].ToString();
                        imgid = "i" + dt["iconID"].ToString();
                        //chk = "chk" + dt["iconID"].ToString();
                        chk = "chk" + i.ToString();

                        //This is sample working block commented
                        //string htmlContent = "<a href='../en/personalCalendar.aspx?v=1&r=1&hf=&m=&pub=&d=&comp=&f=v' ><img src='../en/img/calendar.jpg' alt='Calendar' /></a><br /><span id='p1' ondblclick='changeName(this.id)'>Calendar</span>";
                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "ControlKey", "<script>document.getElementById('pane2').innerHTML=\"" + htmlContent + "\"</script>", false);
                        iconurl = dt["iconUri"].ToString(); // FB 2779 // ZD 101006
                        iconSelColor = fnThemeImageColor(dt["SelectedColor"].ToString());
                        iconSelImage = fnThemeImageURL(dt["SelectedImage"].ToString(), dt["iconUri"].ToString());

                        htmlContent = "<input id='" + chk + "' type='checkbox' onclick='fnMoveIcon(this.id)'><a tabindex='-1' id='" + ancID + "' href='" + dt["iconTarget"].ToString() + "' onclick='return false' onkeydown='if(event.keyCode == 32){changeName(this.parentNode.childNodes[3].id,0)}'><img id='" + imgid // ZD 100243
                            + "' src='" + iconurl + "' style='background-color:" + curThemeColor + "' alt='" + obj.GetTranslatedText(dt["defaultLabel"].ToString())
                            + "' onmouseover='imgOver(this.id)' onmouseout='imgOut(this.id)' ondblclick='fnRemoveIcon(0)' /></a><br /><span class='"
                            + obj.GetTranslatedText(dt["defaultLabel"].ToString()) + "' id='xcont" + iconid + "' ondblclick='changeName(this.id,1)'>" + obj.GetTranslatedText(dt["defaultLabel"].ToString()) //FB 2272
                            + "</span>"; // FB 2779 ZD 100423 // ZD 101006

                        htmlContentx = "<input checked id='" + chk + "' type='checkbox' onclick='fnMoveIcon(this.id)'><a tabindex='-1' id='" + ancID + "' href='" + dt["iconTarget"].ToString() + "' onclick='return false' onkeydown='if(event.keyCode == 32){changeName(this.parentNode.childNodes[3].id,0)}'><img id='" + imgid // ZD 100243
                            + "' src='" + iconurl + "' style='background-color:" + curThemeColor + "' alt='" + obj.GetTranslatedText(dt["defaultLabel"].ToString())
                            + "' onmouseover='imgOver(this.id)' onmouseout='imgOut(this.id)' ondblclick='fnRemoveIcon(0)' /></a><br /><span class='"
                            + obj.GetTranslatedText(dt["defaultLabel"].ToString()) + "' id='xcont" + iconid + "' ondblclick='changeName(this.id,1)'>" + obj.GetTranslatedText(dt["defaultLabel"].ToString()) //FB 2272
                            + "</span>"; // FB 2779 ZD 100423 // ZD 101006

                        htmlContent2 = "<input checked id='" + chk + "' type='checkbox' onclick='fnMoveIcon(this.id)' style='display:none'><a id='" + ancID + "' href='" + dt["iconTarget"].ToString() + "' onclick='return false' onkeydown='if(event.keyCode == 32){changeName(this.parentNode.childNodes[3].id,0)}'><img id='" + imgid // ZD 100243
                            + "' src='" + iconSelImage + "' style='background-color:" + iconSelColor + "' alt='" + obj.GetTranslatedText(dt["userLabel"].ToString())
                            + "' onmouseover='imgOver(this.id)' onmouseout='imgOut(this.id)' ondblclick='fnRemoveIcon(this.parentNode.parentNode.childNodes[0].id)' /></a><br /><span lang='" + dt["SelectedColor"].ToString() + "' accesskey='" + dt["SelectedImage"].ToString() + "' class='"
                            + obj.GetTranslatedText(dt["defaultLabel"].ToString()) + "' id='xpane" + iconid + "' ondblclick='changeName(this.id,1)'>" + obj.GetTranslatedText(dt["userLabel"].ToString())  //FB 2272 //ZD 101913
                            + "</span>"; // FB 2779 ZD 100423 // ZD 101006

                        posid = "cont" + i.ToString();
                        if (dt["gridPosition"].ToString() == "0")
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), dt["iconID"].ToString(), "<script>document.getElementById('" + posid + "').innerHTML=\"" + htmlContent + "\"</script>", false);
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), dt["iconID"].ToString(), "<script>document.getElementById('" + posid + "').innerHTML=\"" + htmlContentx + "\"</script>", false);
                            posid = "pane" + dt["gridPosition"].ToString();
                            ScriptManager.RegisterStartupScript(this, this.GetType(), dt["iconID"].ToString() + i.ToString(), "<script>document.getElementById('" + posid + "').innerHTML=\"" + htmlContent2 + "\"</script>", false);
                        }
                        i++;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace("BindIcons: " + ex.Message);
            }
        }

        #endregion

        #region saveLink_Click

        protected void saveLink_Click(object sender, EventArgs e)
        {
            try
            {
                if (selectedValues.Value.Trim() != "") //FB 2303
                {
                    log.Trace(selectedValues.Value.ToString());
                    string[] rows = selectedValues.Value.ToString().Split('~');
                    string[] columns;

                    userid = Session["userID"].ToString();
                    if (Request.QueryString["t"] != null)
                        if (Request.QueryString["t"].Trim() == "au")
                            if (Session["UserToEdit"] != null)
                                userid = Session["UserToEdit"].ToString();

                    XmlDocument doc = new XmlDocument();
                    StringBuilder inXML = new StringBuilder();
                    string outXML = "";

                    inXML.Append("<SetIconReference>");
                    inXML.Append("<LobbyIcons>");
                    foreach (string row in rows)
                    {
                        columns = row.Split('`');
                        inXML.Append("<Icon>");
                        inXML.Append("<userID>" + userid + "</userID>");
                        inXML.Append("<iconID>" + columns[0].Replace("xpane", "") + "</iconID>");
                        inXML.Append("<defaultLabel>" + columns[1] + "</defaultLabel>");
                        inXML.Append("<iconUri>" + columns[2] + "</iconUri>");
                        inXML.Append("<SelectedColor>" + columns[3] + "</SelectedColor>"); // ZD 101006
                        inXML.Append("<SelectedImage>" + columns[4] + "</SelectedImage>");
                        inXML.Append("</Icon>");
                    }
                    inXML.Append("</LobbyIcons>");
                    inXML.Append("</SetIconReference>");

                    outXML = obj.CallMyVRMServer("SetUserLobbyIcons", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());

                    Response.Redirect("SettingSelect2.aspx");//FB 2644
                }
            }
            catch (System.Threading.ThreadAbortException)
            {
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                log.Trace(ex.StackTrace);
            }
        }
        #endregion

		// ZD 101006 Starts
        #region fnThemeImageColor
        // FB 2779 Starts
        protected string fnThemeImageColor(string color)
        {
            
            if (color != "")
                return color;
            else
            {
                string[] themeColors = { "#888888", "#4277B3", "#CD2522" };
                return themeColors[Int32.Parse(themeType)];
            }
            //int ind = path.LastIndexOf('.');
            //return (path.Substring(0, ind) + Session["ThemeType"].ToString() + path.Substring(ind, path.Length - ind));
        }
        // FB 2779 Ends
        #endregion


        #region fnThemeImageURL
        // FB 2779 Starts
        protected string fnThemeImageURL(string Selected, string Default)
        {

            if (Selected != "")
                return Selected;
            else
                return Default;
        }
        #endregion
		// ZD 101006 Ends
    }
}
