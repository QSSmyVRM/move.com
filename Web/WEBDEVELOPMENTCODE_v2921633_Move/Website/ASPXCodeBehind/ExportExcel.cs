﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Runtime.InteropServices;
using OfficeOpenXml;
using OfficeOpenXml.Drawing;
using OfficeOpenXml.Style;
using System.IO;
using System.Diagnostics;
using System.Drawing;


    public class ExportExcel : System.Web.UI.Page
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Form1"/> class.
        /// </summary>
        
        protected string exportFrom = "";
        protected myVRMNet.NETFunctions objNew; //ALLDEV-498
        public ExportExcel()
        {
        }
      
        /// <summary>
        /// Generates the report.
        /// </summary>
        public void GenerateReport(DataSet ds, string[] sheetNames, string strNote, string heading, string filePath, string eFrom,myVRMNet.NETFunctions obj) //ALLDEV-821
        {
            try
            {
                exportFrom  = eFrom;
                objNew = obj;
				//ALLDEV-821 - start
                using (ExcelPackage p = new ExcelPackage())
                {
                    switch (exportFrom)
                    {
                        case "":
                            for (Int32 t = 0; t < ds.Tables.Count; t++)
                            {
                                DataTable dt = ds.Tables[t];
                                //set the workbook properties and add a default sheet in it
                                //SetWorkbookProperties(p);
                                //Create a sheet
                                ExcelWorksheet ws = CreateSheet(p, sheetNames[t], t + 1);

                                //Merging cells and create a center heading for out table
                                int rowIndex = 1;
                                if (heading != "")
                                {
                                    ws.Cells[1, 1].Value = heading;
                                    ws.Cells[1, 1, 1, dt.Columns.Count].Merge = true;
                                    ws.Cells[1, 1, 1, dt.Columns.Count].Style.Font.Bold = true;
                                    ws.Cells[1, 1, 1, dt.Columns.Count].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    rowIndex++;
                                }

                                if (strNote != "")
                                    CreateNoteHeaderORFooter(ws, ref rowIndex, dt, strNote);

                                CreateHeader(ws, ref rowIndex, dt);
                                CreateData(ws, ref rowIndex, dt);

                                //AddComment(ws, 5, 10, "Zeeshan Umar's Comments", "Zeeshan Umar");
                                //AddCustomShape(ws, 10, 7, eShapeStyle.Ellipse, "Text inside Ellipse.");                       

                                ////These lines will open it in Excel
                                //ProcessStartInfo pi = new ProcessStartInfo(filePath);
                                //Process.Start(pi);
                            }
                            break;
                        case "users":
                            Int32 s = 0;
                            ExcelWorksheet ws1 = null;
                            Int32 rowIndex1 = 1;
                            Int32 shtno = 1;
                            Boolean isSecondSheet =false;
                            for (Int32 t = 0; t < ds.Tables.Count; t++)
                            {
                                if (t > 0 && s == 0)
                                    s = 1;
                                DataTable dt = ds.Tables[t];
                                if (dt.Rows.Count > 1 && (t == 0 || (!isSecondSheet && t >= 1)))
                                {
                                    if (t >= 1)
                                        isSecondSheet = true;

                                    ws1 = CreateSheet(p, sheetNames[s], shtno);
                                    s++;
                                    shtno++;
                                    rowIndex1 = 1;
                                }

                                if (dt.Rows.Count > 1)
                                {
                                    strNote = dt.Rows[0][1].ToString();
                                    if (strNote.Trim() != "")
                                        strNote = objNew.GetTranslatedText(strNote);
                                    CreateNoteHeaderORFooter(ws1, ref rowIndex1, dt, strNote);
                                    CreateHeader(ws1, ref rowIndex1, dt);
                                    CreateData(ws1, ref rowIndex1, dt);

                                    rowIndex1 = rowIndex1 + 2;
                                }
                            }
                            break;
                    }

                    //Generate A File with Random name
                    Byte[] bin = p.GetAsByteArray();
                    //string file = Request.MapPath(".") +"\\Image\\"+ fileName + ".xlsx";
                    File.WriteAllBytes(filePath, bin);
                }
				//ALLDEV-821 - End
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static ExcelWorksheet CreateSheet(ExcelPackage p, string sheetName, Int32 shtNumber)//ALLDEV-821 
        {
            p.Workbook.Worksheets.Add(sheetName);
            ExcelWorksheet ws = p.Workbook.Worksheets[shtNumber]; //ALLDEV-821
            ws.Name = sheetName; //Setting Sheet's name
            ws.Cells.Style.Font.Size = 11; //Default font size for whole sheet
            ws.Cells.Style.Font.Name = "Calibri"; //Default Font name for whole sheet

            return ws;
        }

        /// <summary>
        /// Sets the workbook properties and adds a default sheet.
        /// </summary>
        /// <param name="p">The p.</param>
        /// <returns></returns>
        private static void SetWorkbookProperties(ExcelPackage p)
        {
            //Here setting some document properties
            p.Workbook.Properties.Author = "Zeeshan Umar";
            p.Workbook.Properties.Title = "EPPlus Sample";

            
        }

        private void CreateHeader(ExcelWorksheet ws, ref int rowIndex, DataTable dt)
        {            
            int colIndex = 1;
            rowIndex++;
            foreach (DataColumn dc in dt.Columns) //Creating Headings
            {
                var cell = ws.Cells[rowIndex, colIndex];

                //Setting the background color of header cells to Gray
                var fill = cell.Style.Fill;
                fill.PatternType = ExcelFillStyle.Solid;
                fill.BackgroundColor.SetColor(Color.Gray);

                //Setting Top/left,right/bottom borders.
                var border = cell.Style.Border;
                border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                //Setting Value in cell
                cell.Value = objNew.GetTranslatedText(dc.ColumnName); //ALLDEV-498

                double cellSize = ws.Cells[colIndex, colIndex].Worksheet.Column(colIndex).Width;
                double proposedCellSize = cell.Value.ToString().Length * 1.3;
                if (cellSize <= proposedCellSize)
                {
                    ws.Cells[rowIndex, colIndex].Worksheet.Column(colIndex).Width = proposedCellSize;
                }

                colIndex++;
            }
           
        }

        private void CreateData(ExcelWorksheet ws, ref int rowIndex, DataTable dt)
        {
            int colIndex=0;
            int i = 0;
            string dateformat = "MM/dd/yyyy";
            dateformat = Session["FormatDateType"].ToString();
            String tformat = "hh:mm tt";
            if (Session["timeFormat"].ToString().Equals("0"))
                tformat = "HH:mm";
            else if (Session["timeFormat"].ToString().Equals("2"))
                tformat = "HHmmZ";

            foreach (DataRow dr in dt.Rows) // Adding Data into rows
            {
                i++;
                if (exportFrom == "users" && i == 1) //First Row carrying Heading
                    continue;

                colIndex = 1;
                rowIndex++;

                foreach (DataColumn dc in dt.Columns)
                {
                    var cell = ws.Cells[rowIndex, colIndex];

                    //Setting Value in cell
                    if (dc.ColumnName.ToLower() == "conference date")
                    {
                        if(dr[dc.ColumnName].ToString() != "")
                            cell.Value = Convert.ToDateTime(dr[dc.ColumnName]).ToString(dateformat) + " " + Convert.ToDateTime(dr[dc.ColumnName]).ToString(tformat);
                    }
                    else if(dc.ColumnName.ToLower() == "inventory type")
                        cell.Value = objNew.GetTranslatedText(dr[dc.ColumnName].ToString());
                    else
                        cell.Value = dr[dc.ColumnName];

                    if (cell.Value.ToString().Trim().ToLower() == "yes")
                        cell.Value = objNew.GetTranslatedText(cell.Value.ToString());
                    //Setting borders of cell
                    var border = cell.Style.Border;
                    border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;
                    if(dt.Rows.Count == i)
                        border.Bottom.Style = ExcelBorderStyle.Thin;

                    if(dc.ColumnName.ToLower() == "time zone" || dc.ColumnName.ToLower() == "email")
                        ws.Cells[rowIndex, colIndex].Worksheet.Column(colIndex).Width = cell.Value.ToString().Length * 1.3;
                    colIndex++;
                }
            }
        }

        private void CreateNoteHeaderORFooter(ExcelWorksheet ws, ref int rowIndex, DataTable dt, string strNote)
        {
            //rowIndex++;
            ws.Cells[rowIndex, 1].Value = strNote;
            //ws.Cells[rowIndex, 1, rowIndex, dt.Columns.Count].Merge = true;
            ws.Cells[rowIndex, 1, rowIndex, dt.Columns.Count].Style.Font.Bold = true;
            ws.Cells[rowIndex, 1, rowIndex, dt.Columns.Count].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
            ws.Cells[rowIndex, 1, rowIndex, dt.Columns.Count].Style.VerticalAlignment = ExcelVerticalAlignment.Justify;
            if(exportFrom == "users")
                ws.Cells[rowIndex, 1, rowIndex + 1, dt.Columns.Count].Merge = true;
            else
                ws.Cells[rowIndex, 1, rowIndex + 4, dt.Columns.Count].Merge = true;

            if (exportFrom == "users")
                rowIndex = rowIndex + 1;
            else
                rowIndex = rowIndex + 4;

            //int colIndex = 0;
            //foreach (DataColumn dc in dt.Columns) //Creating Formula in footers
            //{
            //    if (colIndex > 0)
            //        break;
            //    colIndex++;
            //    var cell = ws.Cells[rowIndex, colIndex];

            //    //Setting Sum Formula
            //    //cell.Formula = "Sum(" + ws.Cells[3, colIndex].Address + ":" + ws.Cells[rowIndex - 1, colIndex].Address + ")";
            //    cell.Value = strFoorter;
            //    //Setting Background fill color to Gray
            //    cell.Style.Fill.PatternType = ExcelFillStyle.Solid;
            //    cell.Style.Fill.BackgroundColor.SetColor(Color.Gray);
            //}
        }

        /// <summary>
        /// Adds the custom shape.
        /// </summary>
        /// <param name="ws">Worksheet</param>
        /// <param name="colIndex">Column Index</param>
        /// <param name="rowIndex">Row Index</param>
        /// <param name="shapeStyle">Shape style</param>
        /// <param name="text">Text for the shape</param>
        private static void AddCustomShape(ExcelWorksheet ws, int colIndex, int rowIndex, eShapeStyle shapeStyle, string text)
        {
            ExcelShape shape = ws.Drawings.AddShape("cs" + rowIndex.ToString() + colIndex.ToString(), shapeStyle);
            shape.From.Column = colIndex;
            shape.From.Row = rowIndex;
            shape.From.ColumnOff = Pixel2MTU(5);
            shape.SetSize(100, 100);
            shape.RichText.Add(text);
        }

        /// <summary>
        /// Adds the image in excel sheet.
        /// </summary>
        /// <param name="ws">Worksheet</param>
        /// <param name="colIndex">Column Index</param>
        /// <param name="rowIndex">Row Index</param>
        /// <param name="filePath">The file path</param>
        private static void AddImage(ExcelWorksheet ws, int columnIndex, int rowIndex, string filePath)
        {
            //How to Add a Image using EP Plus
            Bitmap image = new Bitmap(filePath);
            ExcelPicture picture = null;
            if (image != null)
            {
                picture = ws.Drawings.AddPicture("pic" + rowIndex.ToString() + columnIndex.ToString(), image);
                picture.From.Column = columnIndex;
                picture.From.Row = rowIndex;
                picture.From.ColumnOff = Pixel2MTU(2); //Two pixel space for better alignment
                picture.From.RowOff = Pixel2MTU(2);//Two pixel space for better alignment
                picture.SetSize(100, 100);
            }
        }

        /// <summary>
        /// Adds the comment in excel sheet.
        /// </summary>
        /// <param name="ws">Worksheet</param>
        /// <param name="colIndex">Column Index</param>
        /// <param name="rowIndex">Row Index</param>
        /// <param name="comments">Comment text</param>
        /// <param name="author">Author Name</param>
        private static void AddComment(ExcelWorksheet ws, int colIndex, int rowIndex, string comment, string author)
        {
            //Adding a comment to a Cell
            var commentCell = ws.Cells[rowIndex, colIndex];
            commentCell.AddComment(comment, author);
        }

        /// <summary>
        /// Pixel2s the MTU.
        /// </summary>
        /// <param name="pixels">The pixels.</param>
        /// <returns></returns>
        public static int Pixel2MTU(int pixels)
        {
            int mtus = pixels * 9525;
            return mtus;
        }
       
    }

