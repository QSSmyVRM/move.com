﻿/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
// ZD 103954 
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Drawing;

public partial class en_PasswordReset : System.Web.UI.Page
{
    #region Protected Data members
    protected System.Web.UI.WebControls.Label LblError;
    protected System.Web.UI.WebControls.Label LblMessage;
    protected System.Web.UI.WebControls.TextBox txtOldPassword;
    protected System.Web.UI.WebControls.TextBox txtPassword1;
    protected System.Web.UI.WebControls.TextBox txtPassword2;
    protected System.Web.UI.WebControls.Button btnSubmit;
    protected System.Web.UI.HtmlControls.HtmlButton btnCancel;
    protected System.Web.UI.HtmlControls.HtmlGenericControl spanpassword;
    protected String enablePass = "0";
    #endregion

    #region Private Data Members

    private string inXML = "";
    private ns_Logger.Logger log = null;
    private myVRMNet.NETFunctions obj;
    MyVRMNet.LoginManagement loginMgmt = null;

    public en_PasswordReset()
    {
        //
        // TODO: Add constructor logic here
        //
        obj = new myVRMNet.NETFunctions();
        loginMgmt = new MyVRMNet.LoginManagement();
    }

    #endregion

    #region InitializeCulture
    protected override void InitializeCulture()
    {
        if (Session["UserCulture"] != null)
        {
            UICulture = Session["UserCulture"].ToString();
            Culture = Session["UserCulture"].ToString();
        }
        else
        {
            if (Request.QueryString["lang"] != null)
            {
                string browserlang = Request.QueryString["lang"];
                Session["browserlang"] = Request.QueryString["lang"];
                if (browserlang.Contains("en-US") || browserlang.Contains("en"))
                {
                    UICulture = "en";
                    Culture = "en";
                    HttpContext.Current.Session.Add("languageID", "1");
                    Session["TranslationText"] = Cache["EnglishTransText"];
                }
                if (browserlang.Contains("fr-CA") || browserlang.Contains("fr"))
                {
                    UICulture = "fr-CA";
                    Culture = "fr-CA";
                    HttpContext.Current.Session.Add("languageID", "5");
                    Session["TranslationText"] = Cache["FrenchTransText"];
                }
                if (browserlang.Contains("es") || browserlang.Contains("es-US"))
                {
                    UICulture = "es-US";
                    Culture = "es-US";
                    HttpContext.Current.Session.Add("languageID", "3");
                    Session["TranslationText"] = Cache["SpanishTransText"];
                }
            }
            else
            {
                UICulture = "en";
                Culture = "en";
                HttpContext.Current.Session.Add("languageID", "1");
                Session["TranslationText"] = Cache["EnglishTransText"];
            }
        }
        base.InitializeCulture();
    }
    #endregion

    #region Page Load

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (obj == null)
                obj = new myVRMNet.NETFunctions();
            obj.AccessandURLConformityCheck("PasswordReset.aspx", Request.Url.AbsoluteUri.ToLower());
           
            if (!IsPostBack)
            {
                txtOldPassword.Text = "";
                txtPassword1.Text = "";
                txtPassword2.Text = "";
                LblError.Text = obj.GetTranslatedText("Your account password has been expired.");

                if (Session["EnablePasswordRule"] != null)
                {
                    if (Session["EnablePasswordRule"].ToString() != "")
                        enablePass = Session["EnablePasswordRule"].ToString();
                    if (enablePass == "1")
                        spanpassword.Attributes.Add("style", "display:block");
                    else
                    {
                        spanpassword.Attributes.Add("style", "display:none");
                        spanpassword.Parent.Visible = false;
                    }
                }
            } 
        }
        catch (Exception ex)
        {
            log = new ns_Logger.Logger();
            LblError.Visible = true;
            LblError.Text = obj.ShowSystemMessage();
            log.Trace(ex.StackTrace + " : " + ex.Message);
            log = null;
        }
    }
    #endregion

    #region Event Handlers Methods
    protected void BtnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            BuildInXML();
            GetOutXML(inXML);
        }
        catch (System.Threading.ThreadAbortException)
        {
        }
       catch (Exception ex)
        {
           LblError.Text = obj.ShowSystemMessage();
           log.Trace("BtnSubmit_Click:" + ex.Message);
           LblError.Visible = true;
        }
    }
    #endregion

    #region User Defind Methods

    #region BuildInXML
    private void BuildInXML()
    {
        inXML += "<SetExpirePassword>";
        if (Session["OrganizationID"] == null)
            Session["OrganizationID"] = "11";  
        inXML += obj.OrgXMLElement();
        inXML += "<PasswordReset>";
        if (Session["LoginUserName"] != null)
        {
            inXML += "<email>" + Session["LoginUserName"] + "</email>";
            inXML += "<oldpassword>" + txtOldPassword.Text.ToString() + "</oldpassword>";
            inXML += "<newpassword>" + txtPassword1.Text.ToString() + "</newpassword>";
            inXML += "</PasswordReset>";
            inXML += "</SetExpirePassword>";
        }
        else
        {
            inXML += "</PasswordReset>";
            inXML += "</SetExpirePassword>";
        }
    }
    #endregion

    #region GetOutXML

    private void GetOutXML(String inXML)
    {
        string cmd = "SetExpirePassword";   
        string outXML = obj.CallCommand(cmd, inXML);
        XmlDocument errDoc = null; 
        LblError.Text = "";
        LblError.Visible = false;
        if (outXML.IndexOf("<error>") < 0)
        {
           Session["outXML"] = "";
           LblError.Text = obj.GetTranslatedText("Operation Successful.");
           LblError.Visible = true;
           return;
         }
        else if (outXML.IndexOf("error") >= 0)
        {
            Session["outXML"] = "";
            errDoc = new XmlDocument();
            errDoc.LoadXml(outXML);
            if (errDoc.SelectSingleNode("error/message") != null)
            {
                LblError.Text = errDoc.SelectSingleNode("error/message").InnerText.Trim();
                LblError.Visible = true;
                return;
            }
        }
    }
    #endregion
    #endregion
}

