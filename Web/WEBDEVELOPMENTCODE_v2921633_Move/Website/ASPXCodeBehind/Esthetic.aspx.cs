/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886
using System;
using System.Data;
using System.IO;
using System.Collections;
using System.ComponentModel;
using System.Xml;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using CustomizationUtil;
using System.Text;

namespace myVRMAdmin.Web.en
{
    /// <summary>
    /// Summary description for Esthetic.
    /// </summary>
    public partial class Esthetic : System.Web.UI.Page
    {
        #region protected Members

        protected System.Web.UI.WebControls.RadioButton NamedColor;
        protected System.Web.UI.WebControls.RadioButton RGBColor;
        protected System.Web.UI.WebControls.TextBox TxtRGBColor;
        protected System.Web.UI.WebControls.TextBox CurrentFont;
        protected System.Web.UI.WebControls.TextBox m_mouseofffont;
        protected System.Web.UI.WebControls.Button btnCancel;
        protected System.Web.UI.WebControls.DropDownList C;
        protected System.Web.UI.HtmlControls.HtmlForm frmSample;
        protected System.Web.UI.WebControls.DropDownList NamedCol;
        protected System.Web.UI.HtmlControls.HtmlInputFile Bannerfile1024;
        protected System.Web.UI.HtmlControls.HtmlInputFile Bannerfile1600;
        protected System.Web.UI.HtmlControls.HtmlInputFile companylogo;
        //protected System.Web.UI.WebControls.Button original;
        protected System.Web.UI.WebControls.Button lastsaved;
        protected System.Web.UI.WebControls.DropDownList cmbtitlefont;
        protected System.Web.UI.WebControls.DropDownList cmbsubtitlefont;
        protected System.Web.UI.WebControls.DropDownList cmbbtnfont;
        protected System.Web.UI.WebControls.TextBox txtbtnimg;
        protected System.Web.UI.WebControls.DropDownList cmbsubtitlesize;
        protected System.Web.UI.WebControls.TextBox txtsubtitleimg;
        protected System.Web.UI.WebControls.DropDownList cmbtitlesize;
        protected System.Web.UI.WebControls.TextBox txttitleimg;
        protected System.Web.UI.WebControls.DropDownList cmbbtnsize;
        protected System.Web.UI.WebControls.TextBox txtbackimg;
        protected System.Web.UI.WebControls.Label lblgenname;
        protected System.Web.UI.WebControls.DropDownList drpgenfont;
        protected System.Web.UI.WebControls.Label lblgensize;
        protected System.Web.UI.WebControls.DropDownList drpgensize;
        protected System.Web.UI.WebControls.Label lblgencolor;
        protected System.Web.UI.WebControls.TextBox txtgencolor;
        protected System.Web.UI.WebControls.DropDownList drptitlefont;
        protected System.Web.UI.WebControls.Label lbltitlename;
        protected System.Web.UI.WebControls.Label lbltitlesize;
        protected System.Web.UI.WebControls.DropDownList drptitlesize;
        protected System.Web.UI.WebControls.Label lbltitlecolor;
        protected System.Web.UI.WebControls.TextBox txttitlecolor;
        protected System.Web.UI.WebControls.Label lblsubtitlename;
        protected System.Web.UI.WebControls.DropDownList drpsubtitlefont;
        protected System.Web.UI.WebControls.Label lblsubtitlesize;
        protected System.Web.UI.WebControls.DropDownList drpsubtitlesize;
        protected System.Web.UI.WebControls.Label lblsubtitlecolor;
        protected System.Web.UI.WebControls.TextBox txtsubtitlecolor;
        protected System.Web.UI.WebControls.Label lblbttnfontname;
        protected System.Web.UI.WebControls.DropDownList dropbttnfont;
        protected System.Web.UI.WebControls.Label lblbuttonsize;
        protected System.Web.UI.WebControls.DropDownList drpbuttonsize;
        protected System.Web.UI.WebControls.Label lblbttnfontcolor;
        protected System.Web.UI.WebControls.TextBox txtbuttonfontcolor;
        protected System.Web.UI.WebControls.Label lblbuttonbgcolor;
        protected System.Web.UI.WebControls.Label lbltableheaderforecolor;
        protected System.Web.UI.WebControls.Label lbltableheaderforecolor1;
        protected System.Web.UI.WebControls.Label lbltableheaderbackcolor;
        protected System.Web.UI.WebControls.Label lbltableheaderbackcolor1;
        protected System.Web.UI.WebControls.TextBox txtbuttonbgcolor;
        protected System.Web.UI.WebControls.Label lblerrormessagebackcolor;
        protected System.Web.UI.WebControls.Label lblerrormessagebackcolor1;
        protected System.Web.UI.WebControls.Label lblerrormessageforecolor;
        protected System.Web.UI.WebControls.Label lblerrormessageforecolor1;
        protected System.Web.UI.WebControls.Label lblinputtextfontsize;
        protected System.Web.UI.WebControls.Label lblinputtextbackcolor;
        protected System.Web.UI.WebControls.Label lblinputtextbackcolor1;
        protected System.Web.UI.WebControls.Label lblinputtextbordercolor;
        protected System.Web.UI.WebControls.Label lblinputtextbordercolor1;
        protected System.Web.UI.WebControls.Label lblinputtextforecolor;
        protected System.Web.UI.WebControls.Label lblinputtextforecolor1;
        protected System.Web.UI.WebControls.Label lblinputtextfontname;
        protected System.Web.UI.WebControls.Label lblerrormessagefontsize;
        protected System.Web.UI.WebControls.Button btnSave;
        protected System.Web.UI.WebControls.Button btnOriginal;
        protected System.Web.UI.WebControls.Label lblbodybgcolor;
        protected System.Web.UI.WebControls.Label lblbodybgcolor1;
        protected System.Web.UI.WebControls.Label lblgencolor1;
        protected System.Web.UI.WebControls.Label lbltitlecolor1;
        protected System.Web.UI.WebControls.Label lblsubtitlecolor1;
        protected System.Web.UI.WebControls.Label lblbttnfontcolor1;
        protected System.Web.UI.WebControls.Label lblbuttonbgcolor1;
        protected System.Web.UI.WebControls.Label lbld_offfont1;
        protected System.Web.UI.WebControls.Label lblm_Offback1;
        protected System.Web.UI.WebControls.Label lbld_Offback1;
        protected System.Web.UI.WebControls.Label lblm_Onfont1;
        protected System.Web.UI.WebControls.Label lbld_Onfont1;
        protected System.Web.UI.WebControls.Label lblm_Onback1;
        protected System.Web.UI.WebControls.Label lbld_Onback1;
        protected System.Web.UI.WebControls.Label lblm_offfont;
        protected System.Web.UI.WebControls.Label lblm_offfont1;
        protected System.Web.UI.WebControls.Label lbld_offfont;
        protected System.Web.UI.WebControls.TextBox d_mouseofffont;
        protected System.Web.UI.WebControls.Label lblm_Offback;
        protected System.Web.UI.WebControls.TextBox m_mouseOffback;
        protected System.Web.UI.WebControls.Label lbld_Offback;
        protected System.Web.UI.WebControls.TextBox d_mouseOffback;
        protected System.Web.UI.WebControls.Label lblm_Onfont;
        protected System.Web.UI.WebControls.TextBox m_mouseOnfont;
        protected System.Web.UI.WebControls.Label lbld_Onfont;
        protected System.Web.UI.WebControls.TextBox d_mouseOnfont;
        protected System.Web.UI.WebControls.Label lblm_Onback;
        protected System.Web.UI.WebControls.TextBox m_mouseOnback;
        protected System.Web.UI.WebControls.Label lbld_Onback;
        protected System.Web.UI.WebControls.TextBox d_mouseOnback;
        protected System.Web.UI.WebControls.Label lblm_mborder1;
        protected System.Web.UI.WebControls.Label lbld_mborder1;
        protected System.Web.UI.WebControls.Label lblm_hfontcol1;
        protected System.Web.UI.WebControls.Label lbld_hfontcol1;
        protected System.Web.UI.WebControls.Label lblm_hbackcol1;
        protected System.Web.UI.WebControls.Label lbld_hbackcol1;
        protected System.Web.UI.WebControls.Label lblm_mborder;
        protected System.Web.UI.WebControls.TextBox m_menuborder;
        protected System.Web.UI.WebControls.Label lbld_mborder;
        protected System.Web.UI.WebControls.TextBox d_menuborder;
        protected System.Web.UI.WebControls.Label lblm_hfontcol;
        protected System.Web.UI.WebControls.TextBox m_headfontcol;
        protected System.Web.UI.WebControls.Label lbld_hfontcol;
        protected System.Web.UI.WebControls.TextBox d_headfontcol;
        protected System.Web.UI.WebControls.Label lblm_hbackcol;
        protected System.Web.UI.WebControls.TextBox m_headbackcol;
        protected System.Web.UI.WebControls.Label lbld_hbackcol;
        protected System.Web.UI.WebControls.TextBox d_headbackcol;
        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.Button btnPreview;

        protected System.Web.UI.WebControls.TextBox txttableheaderbackcolor;
        protected System.Web.UI.WebControls.TextBox txttableheaderfontcolor;

        protected System.Web.UI.WebControls.TextBox txterrormessagebackcolor;
        protected System.Web.UI.WebControls.TextBox txterrormessagefontcolor;

        protected System.Web.UI.WebControls.TextBox txtinputtextbackcolor;
        protected System.Web.UI.WebControls.TextBox txtinputtextfontcolor;
        protected System.Web.UI.WebControls.TextBox txtinputtextbordercolor;
        protected System.Web.UI.WebControls.DropDownList drpinputtextfontname;
        protected System.Web.UI.WebControls.DropDownList drperrormessagefontsize;
        protected System.Web.UI.WebControls.DropDownList drpinputtextfontsize;

        //code added for FB 1473
        protected System.Web.UI.HtmlControls.HtmlTableRow DefaultThemeRow;
        protected System.Web.UI.HtmlControls.HtmlTableRow CustomizeRow;
        //protected System.Web.UI.WebControls.RadioButtonList ThemeType;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnValue;
        protected System.Web.UI.WebControls.RadioButton RThemeCustom;
        protected System.Web.UI.WebControls.RadioButton RTheme1;
        protected System.Web.UI.WebControls.RadioButton RTheme2;
        protected System.Web.UI.WebControls.RadioButton RTheme3;
        protected System.Web.UI.WebControls.RadioButton RTheme4;
        protected System.Web.UI.WebControls.RadioButton RTheme5;
        protected System.Web.UI.WebControls.RadioButton RDefault;
        protected System.Web.UI.WebControls.ImageButton RImg1;
        protected System.Web.UI.WebControls.ImageButton RImg2;
        protected System.Web.UI.WebControls.ImageButton RImg3;
        
        protected System.Web.UI.WebControls.TextBox txttablebodybackcolor;
        protected System.Web.UI.WebControls.TextBox txttablebodyfontcolor;
        protected System.Web.UI.WebControls.Label lbltablebodybackcolor;
        protected System.Web.UI.WebControls.Label lbltablebodyforecolor;
        protected System.Web.UI.WebControls.Label lbltablebodybackcolor1;
        protected System.Web.UI.WebControls.Label lbltablebodyforecolor1;

        protected System.Web.UI.WebControls.Label lblsitelogo;
        protected System.Web.UI.WebControls.Label lblstdres;
        protected System.Web.UI.WebControls.Label lblHighres;
        //protected System.Web.UI.WebControls.Button btnBanner; //FB 2739 //ZD 100420
        protected System.Web.UI.HtmlControls.HtmlButton btnBanner; //FB 2739 //ZD 100420
        //ZD 104387 - Start
        protected System.Web.UI.HtmlControls.HtmlInputHidden Map1ImageDt;
        protected System.Web.UI.HtmlControls.HtmlInputHidden Map2ImageDt;
        protected myVRMWebControls.ImageControl Map1ImageCtrl;
        protected myVRMWebControls.ImageControl Map2ImageCtrl;
        //ZD 104387 - End
        #endregion

        #region Private Members

        private CSSReplacementUtility cssUtil = null;
        private String newBGColor = "";
        private String newFontName = "";
        private String newFontSize = "";
        private String newFontColor = "";
        private String titleFontName = "";
        private String titleFontSize = "";
        private String titleFontColor = "";
        private String subTitleFontName = "";
        private String subTitleFontSize = "";
        private String subTitleFontColor = "";
        private String buttonFontName = "";
        private String buttonFontSize = "";
        private String buttonFontColor = "";
        private String buttonBackColor = "";
        private String tableHeaderBackColor = "";
        private String tableHeaderForeColor = "";
        private String inputTextForeColor = "";
        private String inputTextBackColor = "";
        private String inputTextFontName = "";
        private String inputTextFontSize = "";
        private String inputTextBorderColor = "";
        private String errorMessageBackColor = "";
        private String errorMessageForeColor = "";
        private String errorMessageFontSize = "";
        private String MMouseOffFont = "";
        private String MMouseoffbackground = "";
        private String MMouseonfont = "";
        private String MMouseonbackground = "";
        private String MMenuborder = "";
        private String MMenuheaderfontcolor = "";
        private String MMenuheaderbackgroundcolor = "";
        private String DMouseOffFont = "";
        private String DMouseoffbackground = "";
        private String DMouseonfont = "";
        private String DMouseonbackground = "";
        private String DMenuborder = "";
        private String DMenuheaderfontcolor = "";
        private String DMenuheaderbackgroundcolor = "";

        //Window Dressing
        private String tableBodyBackColor = "";
        private String tableBodyForeColor = "";
        private String themeName = "";

        private myVRMNet.NETFunctions obj;
        private myVRMNet.ImageUtil imgUtilObj = null; //Image Project
        
        private bool imgRemoved = false; //FB 1633
        private string bannerImage = "";
        protected String language = "";//FB 1830
        private bool ImgRemCompBackground = false;//ZD 104387
        ns_Logger.Logger log = new ns_Logger.Logger();//ZD 100263
        #endregion

        //ZD 101022
        #region InitializeCulture
        protected override void InitializeCulture()
        {
            if (Session["UserCulture"] != null)
            {
                UICulture = Session["UserCulture"].ToString();
                Culture = Session["UserCulture"].ToString();
                base.InitializeCulture();
            }
        }
        #endregion

        #region Methods Executed on Page Load

        private void Page_Load(object sender, System.EventArgs e)
        {

            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.AccessandURLConformityCheck("esthetic.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
                if (Session["language"] == null)//FB 1830
                    Session["language"] = "en";
                if (Session["language"].ToString() != "")
                    language = Session["language"].ToString();

                obj = new myVRMNet.NETFunctions();
                imgUtilObj = new myVRMNet.ImageUtil(); //Image Project

                /*if (Request.ServerVariables.Count > 0) //FB 2203
                {
                    if (Request.ServerVariables[0].IndexOf("HTTP_REFERER") < 1)
                    {
                        Response.Redirect(ConfigurationSettings.AppSettings["StartPage"].ToString(), true);
                    }
                }*/
                Session["ApplicationPath"] = Server.MapPath(".");//FB 1830 - Translation Menu
                //FB 2670
                if (Session["admin"].ToString() == "3")
                {
                    
                    //btnSave.ForeColor = System.Drawing.Color.Gray; // FB 2976
                    //btnSave.Attributes.Add("Class", "btndisable");// FB 2796
                    //ZD 100263
                    btnSave.Visible = false;
                }
                else
                    btnSave.Attributes.Add("Class", "altLongBlueButtonFormat");// FB 2796
                if (!IsPostBack)
                {
                    BindData();
                }
                //MakeReadonly();

               

            }
            catch (System.Threading.ThreadAbortException)
            {
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("Page_Load" + ex.ToString() + " : " + ex.Source.ToString() + " 1: " + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
                //errLabel.Text = ex.ToString() + " : " + ex.Source.ToString() + " 1: " + ex.Message;
            }
        }

        private void BindData()
        {
            try
            {
                Hashtable htStyles = null;

                cssUtil = new CSSReplacementUtility();
                cssUtil.ApplicationPath = Session["ApplicationPath"].ToString();
                DisplayCompanyImages();//ZD 104387
                //moving files to Temp location
                bool ret = cssUtil.MirrorToTempLocation();
                if (!ret)
                {
                    errLabel.Text = obj.GetTranslatedText("Operation failed: Invalid security permission. Please contact your VRM Administrator");
                    errLabel.Visible = true;
                }
                else
                {
                    htStyles = cssUtil.RetrieveMirrorStyles();

                    if (htStyles != null)
                    {
                        BindData(htStyles);
                        htStyles = null;
                    }

                    //Code added for FB 1473 - start
                    if (themeName == "")
                    {
                        if (!IsPostBack || hdnValue.Value == "")
                        {
                            CustomizeRow.Attributes.Add("Style", "Display:Block");
                            RThemeCustom.Checked = true;
                        }
                        else
                            CustomizeRow.Attributes.Add("Style", "Display:None");
                    }                  
                    else if (themeName != "")
                    {
                        CustomizeRow.Attributes.Add("Style", "Display:None");
                        btnSave.Text = obj.GetTranslatedText("Apply Theme");//FB 1830 - Translation
                        btnPreview.Attributes.Add("style","display:None");
                        hdnValue.Value = themeName;
                        switch (themeName.ToUpper())
                        {
                            case "THEME1":
                                RTheme1.Checked = true;
                                break;
                            case "THEME2":
                                RTheme2.Checked = true;
                                break;
                            case "THEME3":
                                RTheme3.Checked = true;
                                break;
                            case "THEME4":
                                RTheme4.Checked = true;
                                break;
                            case "THEME5":
                                RTheme5.Checked = true;
                                break;
                            case "DEFAULT":
                                RDefault.Checked = true;
                                break;
                            case "CUSTOM":
                                CustomizeRow.Attributes.Add("Style", "display:");//Edited for FF
                                btnSave.Text = obj.GetTranslatedText("Submit"); //FB 1830 - Translation
                                btnPreview.Attributes.Add("style", "display:");//Edited for FF
                                hdnValue.Value = "";
                                RThemeCustom.Checked = true;
                                break;
                        }                        
                    }
                    //Code added for FB 1473 - end

                    this.lblbodybgcolor.Text = newBGColor;
                    this.lblbodybgcolor1.Text = "&nbsp;&nbsp;&nbsp;&nbsp;";
                    this.lblbodybgcolor1.Style.Add("background-color", newBGColor);
                    this.lblgenname.Text = newFontName;
                    this.lblgensize.Text = newFontSize;
                    this.lblgencolor.Text = "&nbsp;&nbsp;&nbsp;&nbsp;";
                    this.lblgencolor.Style.Add("background-color", newFontColor);
                    this.lblgencolor1.Text = newFontColor;

                    this.lbltitlename.Text = titleFontName;
                    this.lbltitlesize.Text = titleFontSize;

                    this.lbltitlecolor.Text = "&nbsp;&nbsp;&nbsp;&nbsp;";
                    this.lbltitlecolor.Style.Add("background-color", titleFontColor);
                    this.lbltitlecolor1.Text = titleFontColor;

                    this.lblsubtitlename.Text = subTitleFontName;
                    this.lblsubtitlesize.Text = subTitleFontSize;
                    this.lblsubtitlecolor.Text = "&nbsp;&nbsp;&nbsp;&nbsp;";
                    this.lblsubtitlecolor.Style.Add("background-color", subTitleFontColor);
                    this.lblsubtitlecolor1.Text = subTitleFontColor;

                    this.lblbttnfontname.Text = buttonFontName;
                    this.lblbuttonsize.Text = buttonFontSize;
                    this.lblbttnfontcolor.Text = "&nbsp;&nbsp;&nbsp;&nbsp;";
                    this.lblbttnfontcolor.Style.Add("background-color", buttonFontColor);
                    this.lblbttnfontcolor1.Text = buttonFontColor;

                    this.lblbuttonbgcolor.Text = "&nbsp;&nbsp;&nbsp;&nbsp;";
                    this.lblbuttonbgcolor.Style.Add("background-color", buttonBackColor);
                    this.lblbuttonbgcolor1.Text = buttonBackColor;

                    this.lbltableheaderbackcolor.Text = "&nbsp;&nbsp;&nbsp;&nbsp;";
                    this.lbltableheaderbackcolor.Style.Add("background-color", tableHeaderBackColor);
                    this.lbltableheaderbackcolor1.Text = tableHeaderBackColor;

                    this.lbltableheaderforecolor.Text = "&nbsp;&nbsp;&nbsp;&nbsp;";
                    this.lbltableheaderforecolor.Style.Add("background-color", tableHeaderForeColor);
                    this.lbltableheaderforecolor1.Text = tableHeaderForeColor;

                    this.lblerrormessageforecolor.Text = "&nbsp;&nbsp;&nbsp;&nbsp;";
                    this.lblerrormessageforecolor.Style.Add("background-color", errorMessageForeColor);
                    this.lblerrormessageforecolor1.Text = errorMessageForeColor;

                    this.lblerrormessagebackcolor.Text = "&nbsp;&nbsp;&nbsp;&nbsp;";
                    this.lblerrormessagebackcolor.Style.Add("background-color", errorMessageBackColor);
                    this.lblerrormessagebackcolor1.Text = errorMessageBackColor;

                    this.lblinputtextbackcolor.Text = "&nbsp;&nbsp;&nbsp;&nbsp;";
                    this.lblinputtextbackcolor.Style.Add("background-color", inputTextBackColor);
                    this.lblinputtextbackcolor1.Text = inputTextBackColor;

                    this.lblinputtextforecolor.Text = "&nbsp;&nbsp;&nbsp;&nbsp;";
                    this.lblinputtextforecolor.Style.Add("background-color", inputTextForeColor);
                    this.lblinputtextforecolor1.Text = inputTextForeColor;

                    this.lblinputtextbordercolor.Text = "&nbsp;&nbsp;&nbsp;&nbsp;";
                    this.lblinputtextbordercolor.Style.Add("background-color", inputTextBorderColor);
                    this.lblinputtextbordercolor1.Text = inputTextBorderColor;

                    this.lblerrormessagefontsize.Text = errorMessageFontSize;

                    this.lblinputtextfontsize.Text = inputTextFontSize;
                    this.lblinputtextfontname.Text = inputTextFontName;

                    this.lblm_offfont.Text = "&nbsp;&nbsp;&nbsp;&nbsp;";
                    this.lblm_offfont.Style.Add("background-color", MMouseOffFont);
                    this.lblm_offfont1.Text = MMouseOffFont;

                    this.lblm_Offback.Text = "&nbsp;&nbsp;&nbsp;&nbsp;";
                    this.lblm_Offback.Style.Add("background-color", MMouseoffbackground);
                    this.lblm_Offback1.Text = MMouseoffbackground;

                    this.lblm_Onfont.Text = "&nbsp;&nbsp;&nbsp;&nbsp;";
                    this.lblm_Onfont.Style.Add("background-color", MMouseonfont);
                    this.lblm_Onfont1.Text = MMouseonfont;

                    this.lblm_Onback.Text = "&nbsp;&nbsp;&nbsp;&nbsp;";
                    this.lblm_Onback.Style.Add("background-color", MMouseonbackground);
                    this.lblm_Onback1.Text = MMouseonbackground;

                    this.lblm_mborder.Text = "&nbsp;&nbsp;&nbsp;&nbsp;";
                    this.lblm_mborder.Style.Add("background-color", MMenuborder);
                    this.lblm_mborder1.Text = MMenuborder;

                    this.lblm_hfontcol.Text = "&nbsp;&nbsp;&nbsp;&nbsp;";
                    this.lblm_hfontcol.Style.Add("background-color", MMenuheaderfontcolor);
                    this.lblm_hfontcol1.Text = MMenuheaderfontcolor;

                    this.lblm_hbackcol.Text = "&nbsp;&nbsp;&nbsp;&nbsp;";
                    this.lblm_hbackcol.Style.Add("background-color", MMenuheaderbackgroundcolor);
                    this.lblm_hbackcol1.Text = MMenuheaderbackgroundcolor;

                    this.lbld_offfont.Text = "&nbsp;&nbsp;&nbsp;&nbsp;";
                    this.lbld_offfont.Style.Add("background-color", DMouseOffFont);
                    this.lbld_offfont1.Text = DMouseOffFont;

                    this.lbld_Offback.Text = "&nbsp;&nbsp;&nbsp;&nbsp;";
                    this.lbld_Offback.Style.Add("background-color", DMouseoffbackground);
                    this.lbld_Offback1.Text = DMouseoffbackground;

                    this.lbld_Onfont.Text = "&nbsp;&nbsp;&nbsp;&nbsp;";
                    this.lbld_Onfont.Style.Add("background-color", DMouseonfont);
                    this.lbld_Onfont1.Text = DMouseonfont;

                    this.lbld_Onback.Text = "&nbsp;&nbsp;&nbsp;&nbsp;";
                    this.lbld_Onback.Style.Add("background-color", DMouseonbackground);
                    this.lbld_Onback1.Text = DMouseonbackground;

                    this.lbld_mborder.Text = "&nbsp;&nbsp;&nbsp;&nbsp;";
                    this.lbld_mborder.Style.Add("background-color", DMenuborder);
                    this.lbld_mborder1.Text = DMenuborder;

                    this.lbld_hfontcol.Text = "&nbsp;&nbsp;&nbsp;&nbsp;";
                    this.lbld_hfontcol.Style.Add("background-color", DMenuheaderfontcolor);
                    this.lbld_hfontcol1.Text = DMenuheaderfontcolor;

                    this.lbld_hbackcol.Text = "&nbsp;&nbsp;&nbsp;&nbsp;";
                    this.lbld_hbackcol.Style.Add("background-color", DMenuheaderbackgroundcolor);
                    this.lbld_hbackcol1.Text = DMenuheaderbackgroundcolor;

                    //Window Dressing
                    this.lbltablebodybackcolor.Text = "&nbsp;&nbsp;&nbsp;&nbsp;";
                    this.lbltablebodybackcolor.Style.Add("background-color", tableBodyBackColor);
                    this.lbltablebodybackcolor1.Text = tableBodyBackColor;

                    this.lbltablebodyforecolor.Text = "&nbsp;&nbsp;&nbsp;&nbsp;";
                    this.lbltablebodyforecolor.Style.Add("background-color", tableBodyForeColor);
                    this.lbltablebodyforecolor1.Text = tableBodyForeColor;

                    //Retrieving temp values to new controls
                    BindNewValues();
                }

            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("BindData" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;
                //errLabel.Text = ex.StackTrace;
            }
        }

        private void BindTempData()
        {
            try
            {
                Hashtable htStyles = null;

                cssUtil = new CSSReplacementUtility();
                cssUtil.ApplicationPath = Session["ApplicationPath"].ToString();

                int errid=0;//ZD 100263
                htStyles = cssUtil.RetrieveTempStyles(ref errid);
                if (errid == 1)
                {
                    errLabel.Text = obj.GetTranslatedText("Operation failed: Invalid security permission. Please contact your VRM Administrator");
                    errLabel.Visible = true;
                }

                if (htStyles != null)
                {
                    BindData(htStyles);
                    htStyles = null;
                }

                //Assigning temp values to new controls
                BindNewValues();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void BindData(Hashtable htStyles)
        {
            try
            {
                newBGColor = htStyles["NewBGColor"].ToString();
                newFontName = htStyles["NewFontName"].ToString();
                newFontSize = htStyles["NewFontSize"].ToString();
                newFontColor = htStyles["NewFontColor"].ToString();
                titleFontName = htStyles["TitleFontName"].ToString();
                titleFontSize = htStyles["TitleFontSize"].ToString();
                titleFontColor = htStyles["TitleFontColor"].ToString();
                subTitleFontName = htStyles["SubTitleFontName"].ToString();
                subTitleFontSize = htStyles["SubTitleFontSize"].ToString();
                subTitleFontColor = htStyles["SubTitleFontColor"].ToString();
                buttonFontName = htStyles["ButtonFontName"].ToString();
                buttonFontSize = htStyles["ButtonFontSize"].ToString();
                buttonFontColor = htStyles["ButtonFontColor"].ToString();
                buttonBackColor = htStyles["ButtonBackColor"].ToString();
                tableHeaderForeColor = htStyles["TableHeaderForeColor"].ToString();
                tableHeaderBackColor = htStyles["TableHeaderBackColor"].ToString();

                errorMessageBackColor = htStyles["ErrorMessageBackColor"].ToString();
                errorMessageForeColor = htStyles["ErrorMessageForeColor"].ToString();
                errorMessageFontSize = htStyles["ErrorMessageFontSize"].ToString();

                inputTextBackColor = htStyles["InputTextBackColor"].ToString();
                inputTextForeColor = htStyles["InputTextForeColor"].ToString();
                inputTextFontSize = htStyles["InputTextFontSize"].ToString();
                inputTextFontName = htStyles["InputTextFontName"].ToString();
                inputTextBorderColor = htStyles["InputTextBorderColor"].ToString();

                MMouseOffFont = htStyles["MMouseOffFont"].ToString();
                MMouseoffbackground = htStyles["MMouseoffbackground"].ToString();
                MMouseonfont = htStyles["MMouseonfont"].ToString();
                MMouseonbackground = htStyles["MMouseonbackground"].ToString();
                MMenuborder = htStyles["MMenuborder"].ToString();
                MMenuheaderfontcolor = htStyles["MMenuheaderfontcolor"].ToString();
                MMenuheaderbackgroundcolor = htStyles["MMenuheaderbackgroundcolor"].ToString();
                DMouseOffFont = htStyles["DMouseOffFont"].ToString();
                DMouseoffbackground = htStyles["DMouseoffbackground"].ToString();
                DMouseonfont = htStyles["DMouseonfont"].ToString();
                DMouseonbackground = htStyles["DMouseonbackground"].ToString();
                DMenuborder = htStyles["DMenuborder"].ToString();
                DMenuheaderfontcolor = htStyles["DMenuheaderfontcolor"].ToString();
                DMenuheaderbackgroundcolor = htStyles["DMenuheaderbackgroundcolor"].ToString();
                //window dressing
                tableBodyForeColor = htStyles["TableBodyForeColor"].ToString();
                tableBodyBackColor = htStyles["TableBodyBackColor"].ToString();
                themeName = htStyles["ThemeName"].ToString();
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("BindData" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;
                //errLabel.Text = ex.StackTrace;
            }
        }


        private void BindNewValues()
        {
            if (newBGColor.StartsWith("#"))
            {
                this.RGBColor.Checked = true;
                this.TxtRGBColor.Text = newBGColor;
            }
            else
            {
                this.NamedColor.Checked = true;
                this.RGBColor.Checked = false;

                this.NamedCol.ClearSelection();
                foreach (ListItem item in this.NamedCol.Items)
                {
                    if (item.Value.ToUpper() == newBGColor.ToUpper())
                    {
                        item.Selected = true;
                        break;
                    }
                }
            }

            String[] singleFont = newFontName.Split(',');
            newFontName = singleFont[0];

            this.drpgenfont.ClearSelection();
            foreach (ListItem item in this.drpgenfont.Items)
            {
                if (item.Value.ToUpper() == newFontName.ToUpper())
                {
                    item.Selected = true;
                    break;
                }
            }

            newFontSize = newFontSize.Substring(0, newFontSize.Length - 2);
            this.drpgensize.ClearSelection();
            foreach (ListItem item in this.drpgensize.Items)
            {
                if (item.Value.ToUpper() == newFontSize.ToUpper())
                {
                    item.Selected = true;
                    break;
                }
            }

            this.txtgencolor.Text = newFontColor;

            String[] singleTFont = titleFontName.Split(',');
            titleFontName = singleTFont[0];

            drptitlefont.ClearSelection();
            foreach (ListItem item in this.drptitlefont.Items)
            {
                if (item.Value.ToUpper() == titleFontName.ToUpper())
                {
                    item.Selected = true;
                    break;
                }
            }

            titleFontSize = titleFontSize.Substring(0, titleFontSize.Length - 2);

            this.drptitlesize.ClearSelection();
            foreach (ListItem item in this.drptitlesize.Items)
            {
                if (item.Value.ToUpper() == titleFontSize.ToUpper())
                {
                    item.Selected = true;
                    break;
                }
            }

            this.txttitlecolor.Text = titleFontColor;

            String[] singleSTFont = subTitleFontName.Split(',');
            subTitleFontName = singleSTFont[0];

            this.drpsubtitlefont.ClearSelection();
            foreach (ListItem item in this.drpsubtitlefont.Items)
            {
                if (item.Value.ToUpper() == subTitleFontName.ToUpper())
                {
                    item.Selected = true;
                    break;
                }
            }

            subTitleFontSize = subTitleFontSize.Substring(0, subTitleFontSize.Length - 2);

            this.drpsubtitlesize.ClearSelection();
            foreach (ListItem item in this.drpsubtitlesize.Items)
            {
                if (item.Value.ToUpper() == subTitleFontSize.ToUpper())
                {
                    item.Selected = true;
                    break;
                }
            }
            this.txtsubtitlecolor.Text = subTitleFontColor;

            String[] singleBFont = buttonFontName.Split(',');
            buttonFontName = singleBFont[0];

            this.dropbttnfont.ClearSelection();
            foreach (ListItem item in this.dropbttnfont.Items)
            {
                if (item.Value.ToUpper() == buttonFontName.ToUpper())
                {
                    item.Selected = true;
                    break;
                }
            }

            buttonFontSize = buttonFontSize.Substring(0, buttonFontSize.Length - 2);

            this.drpbuttonsize.ClearSelection();
            foreach (ListItem item in this.drpbuttonsize.Items)
            {
                if (item.Value.ToUpper() == buttonFontSize.ToUpper())
                {
                    item.Selected = true;
                    break;
                }
            }
            this.txtbuttonfontcolor.Text = buttonFontColor;
            this.txtbuttonbgcolor.Text = buttonBackColor;

            this.txttableheaderbackcolor.Text = tableHeaderBackColor;
            this.txttableheaderfontcolor.Text = tableHeaderForeColor;

            //window Dressing
            this.txttablebodybackcolor.Text = tableBodyBackColor;
            this.txttablebodyfontcolor.Text = tableBodyForeColor;

            this.txterrormessagebackcolor.Text = errorMessageBackColor;
            this.txterrormessagefontcolor.Text = errorMessageForeColor;

            this.drperrormessagefontsize.ClearSelection();
            foreach (ListItem item in this.drperrormessagefontsize.Items)
            {
                if (item.Value.ToUpper() == errorMessageFontSize.ToUpper())
                {
                    item.Selected = true;
                    break;
                }
            }

            this.txtinputtextbackcolor.Text = inputTextBackColor;
            this.txtinputtextbordercolor.Text = inputTextBorderColor;
            this.txtinputtextfontcolor.Text = inputTextForeColor;

            this.drpinputtextfontname.ClearSelection();
            foreach (ListItem item in this.drpinputtextfontname.Items)
            {
                if (item.Value.ToUpper() == inputTextFontName.ToUpper())
                {
                    item.Selected = true;
                    break;
                }
            }

            this.drpinputtextfontsize.ClearSelection();
            foreach (ListItem item in this.drpinputtextfontsize.Items)
            {
                if (item.Value.ToUpper() == inputTextFontSize.ToUpper())
                {
                    item.Selected = true;
                    break;
                }
            }


            this.m_mouseofffont.Text = MMouseOffFont;
            this.m_mouseOffback.Text = MMouseoffbackground;
            this.m_mouseOnfont.Text = MMouseonfont;
            this.m_mouseOnback.Text = MMouseonbackground;
            this.m_menuborder.Text = MMenuborder;
            this.m_headfontcol.Text = MMenuheaderfontcolor;
            this.m_headbackcol.Text = MMenuheaderbackgroundcolor;

            this.d_mouseofffont.Text = DMouseOffFont;
            this.d_mouseOffback.Text = DMouseoffbackground;
            this.d_mouseOnfont.Text = DMouseonfont;
            this.d_mouseOnback.Text = DMouseonbackground;
            this.d_menuborder.Text = DMenuborder;
            this.d_headfontcol.Text = DMenuheaderfontcolor;
            this.d_headbackcol.Text = DMenuheaderbackgroundcolor;
        }


        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            InitializeUIComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);

        }

        private void InitializeUIComponent()
        {
            this.btnPreview.Click += new EventHandler(btnPreview_Click);
            this.btnOriginal.Click += new EventHandler(btnOriginal_Click);
            this.btnSave.Click += new EventHandler(btnSave_Click);

            this.btnPreview.Attributes.Add("onclick", "javascript:return FnValidateForm();");
            this.btnSave.Attributes.Add("onclick", "javascript:return FnConfirm();");
            this.NamedColor.Attributes.Add("onclick", "javascript:return FnHideShow();");
            this.RGBColor.Attributes.Add("onclick", "javascript:return FnHideShow();");

            //Code added for FB 1473 - start
            RImg1.Click += new ImageClickEventHandler(RImg1_Click);
            RImg2.Click += new ImageClickEventHandler(RImg2_Click);
            RImg3.Click += new ImageClickEventHandler(RImg3_Click);
            //Code added for FB 1473 - end
        }

       
        #endregion

        #region Event Handler Methods To Prieview Button

        private void btnPreview_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["ApplicationPath"].ToString() == "")
                    throw new Exception("Application Path does not exist");

                SetUserPreferences();

                Page.RegisterClientScriptBlock("Preview", "<script>window.open('Preview.aspx','Preview','resizable=yes,scrollbars=yes,toolbar=no, width=900,height=600,top=40,left=40')</script>");

                btnPreview.Attributes.Add("Style", "Display:Block"); // FB 2603
                CustomizeRow.Attributes.Add("Style", "Display:Block");
                RThemeCustom.Checked = true;
                hdnValue.Value = "";
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("Page_Load" + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
                //errLabel.Text = ex.Message;
            }
        }

        private void SetUserPreferences()
        {
            Hashtable htStyles = null;
            bool ret = false;//ZD 100263
            try
            {

                htStyles = AssignCSSToHashtable(htStyles);

                cssUtil = new CSSReplacementUtility();
                //Response.Write("<br>before replacestyles");

                cssUtil.ApplicationPath = Session["ApplicationPath"].ToString();
                if (htStyles.Count > 0)//ZD 100263
                {
                    int errid=0;
                    cssUtil.ReplaceStyles(htStyles, ref errid);
                    if (errid == 1)
                    {
                        errLabel.Text = obj.GetTranslatedText("Operation failed: Invalid security permission. Please contact your VRM Administrator");
                        errLabel.Visible = true;
                    }
                }
                    
                htStyles = null;
                //Response.Write("<br>after replacestyles");
                htStyles = AssignMenuToHashtable(htStyles);
                //Response.Write("<br>after AssignMenuToHashtable");

                if (htStyles.Count > 0)//ZD 100263
                {
                    int errid = 0;
                    cssUtil.ReplaceMenuStyle(htStyles, ref errid);
                    if (errid == 1)
                    {
                        errLabel.Text = obj.GetTranslatedText("Operation failed: Invalid security permission. Please contact your VRM Administrator");
                        errLabel.Visible = true;
                    }
                }
                    
                //Response.Write("<br>after ReplaceMenuStyle");

                htStyles = null;
				//FB 2739 Starts
                //if (this.Bannerfile1024.Value.ToString() != "")
                //    cssUtil.PostArtifactsBanner(Bannerfile1024);

                //if (this.Bannerfile1600.Value.ToString() != "")
                //    cssUtil.PostArtifactsBanner(Bannerfile1600);

                //if (this.companylogo.Value != "")
                //    cssUtil.PostArtifactsLogo(companylogo);
				//FB 2739 End
            }
            catch (Exception ex)
            {
                //                Response.End();
                throw ex;
            }
        }

        private void MakeReadonly()
        {
            txtbuttonbgcolor.ReadOnly = true;
            txtbuttonfontcolor.ReadOnly = true;
            txttableheaderbackcolor.ReadOnly = true;
            txttableheaderfontcolor.ReadOnly = true;
            txtgencolor.ReadOnly = true;
            TxtRGBColor.ReadOnly = true;
            txtsubtitlecolor.ReadOnly = true;
            txttitlecolor.ReadOnly = true;
            txterrormessagebackcolor.ReadOnly = true;
            txterrormessagefontcolor.ReadOnly = true;
            txtinputtextbackcolor.ReadOnly = true;
            txtinputtextbordercolor.ReadOnly = true;
            txtinputtextfontcolor.ReadOnly = true;
            d_headbackcol.ReadOnly = true;
            d_headfontcol.ReadOnly = true;
            d_menuborder.ReadOnly = true;
            d_mouseOffback.ReadOnly = true;
            d_mouseofffont.ReadOnly = true;
            d_mouseOnback.ReadOnly = true;
            d_mouseOnfont.ReadOnly = true;
            m_headbackcol.ReadOnly = true;
            m_headfontcol.ReadOnly = true;
            m_menuborder.ReadOnly = true;
            m_mouseOffback.ReadOnly = true;
            m_mouseofffont.ReadOnly = true;
            m_mouseOnback.ReadOnly = true;
            m_mouseOnfont.ReadOnly = true;
           
        }

        private Hashtable AssignCSSToHashtable(Hashtable htStyles)
        {
            if (htStyles == null)
                htStyles = new Hashtable();
            try
            {
                if (this.NamedColor.Checked == true)
                    htStyles.Add("NewBGColor", this.NamedCol.SelectedValue);
                else
                    htStyles.Add("NewBGColor", this.TxtRGBColor.Text);

                if (this.drpgenfont.SelectedItem.ToString() != "Select the Font")
                    htStyles.Add("NewFontName", this.drpgenfont.SelectedItem.ToString());
                else
                    htStyles.Add("NewFontName", this.lblgenname.Text);

                if (this.drpgensize.SelectedItem.ToString() != "Size")
                    htStyles.Add("NewFontSize", this.drpgensize.SelectedItem.ToString());
                else
                    htStyles.Add("NewFontSize", this.lblgensize.Text);

                if (this.txtgencolor.Text != "")
                    htStyles.Add("NewFontColor", this.txtgencolor.Text);
                else
                    htStyles.Add("NewFontColor", this.lblgencolor.Text);

                if (this.drptitlefont.SelectedItem.ToString() != "Select the Font")
                    htStyles.Add("TitleFontName", this.drptitlefont.SelectedItem.ToString());
                else
                    htStyles.Add("TitleFontName", this.lbltitlename.Text);

                if (this.drptitlesize.SelectedItem.ToString() != "Size")
                    htStyles.Add("TitleFontSize", this.drptitlesize.SelectedItem.ToString());
                else
                    htStyles.Add("TitleFontSize", this.lbltitlesize.Text);

                if (this.txttitlecolor.Text != "")
                    htStyles.Add("TitleFontColor", this.txttitlecolor.Text);
                else
                    htStyles.Add("TitleFontColor", this.lbltitlecolor.Text);

                if (this.drpsubtitlefont.SelectedItem.ToString() != "Select the Font")
                    htStyles.Add("SubTitleFontName", this.drpsubtitlefont.SelectedItem.ToString());
                else
                    htStyles.Add("SubTitleFontName", this.lblsubtitlename.Text);

                if (this.drpsubtitlesize.SelectedItem.ToString() != "Size")
                    htStyles.Add("SubTitleFontSize", this.drpsubtitlesize.SelectedItem.ToString());
                else
                    htStyles.Add("SubTitleFontSize", this.lblsubtitlesize.Text);

                if (this.txtsubtitlecolor.Text != "")
                    htStyles.Add("SubTitleFontColor", this.txtsubtitlecolor.Text);
                else
                    htStyles.Add("SubTitleFontColor", this.lblsubtitlecolor.Text);

                if (this.dropbttnfont.SelectedItem.ToString() != "Select the Font")
                    htStyles.Add("ButtonFontName", this.dropbttnfont.SelectedItem.ToString());
                else
                    htStyles.Add("ButtonFontName", this.lblbttnfontname.Text);

                if (this.drpbuttonsize.SelectedItem.ToString() != "Size")
                    htStyles.Add("ButtonFontSize", this.drpbuttonsize.SelectedItem.ToString());
                else
                    htStyles.Add("ButtonFontSize", this.lblbuttonsize.Text);

                if (this.txtbuttonfontcolor.Text != "")
                    htStyles.Add("ButtonFontColor", this.txtbuttonfontcolor.Text);
                else
                    htStyles.Add("ButtonFontColor", this.lblbttnfontcolor.Text);

                if (this.txtbuttonbgcolor.Text != "")
                    htStyles.Add("ButtonBackColor", this.txtbuttonbgcolor.Text);
                else
                    htStyles.Add("ButtonBackColor", this.lblbuttonbgcolor.Text);

                if (this.txttableheaderbackcolor.Text != "")
                    htStyles.Add("TableHeaderBackColor", this.txttableheaderbackcolor.Text);
                else
                    htStyles.Add("TableHeaderBackColor", this.lbltableheaderbackcolor.Text);

                if (this.txttableheaderfontcolor.Text != "")
                    htStyles.Add("TableHeaderForeColor", this.txttableheaderfontcolor.Text);
                else
                    htStyles.Add("TableHeaderForeColor", this.lbltableheaderforecolor.Text);

                //window Dressing - start
                if (this.txttablebodybackcolor.Text != "")
                    htStyles.Add("TableBodyBackColor", this.txttablebodybackcolor.Text);
                else
                    htStyles.Add("TableBodyBackColor", this.lbltablebodybackcolor.Text);

                if (this.txttablebodyfontcolor.Text != "")
                    htStyles.Add("TableBodyForeColor", this.txttablebodyfontcolor.Text);
                else
                    htStyles.Add("TableBodyForeColor", this.lbltablebodyforecolor.Text);

                if (this.txttablebodyfontcolor.Text != "")
                    htStyles.Add("ThemeName", themeName);
                else
                    htStyles.Add("ThemeName", themeName);

                //window Dressing - end

                if (this.txterrormessagefontcolor.Text != "")
                    htStyles.Add("ErrorMessageForeColor", this.txterrormessagefontcolor.Text);
                else
                    htStyles.Add("ErrorMessageForeColor", this.lblerrormessageforecolor.Text);

                if (this.txterrormessagebackcolor.Text != "")
                    htStyles.Add("ErrorMessageBackColor", this.txterrormessagebackcolor.Text);
                else
                    htStyles.Add("ErrorMessageBackColor", this.lblerrormessagebackcolor.Text);

                if (this.drperrormessagefontsize.SelectedItem.ToString() != "Size")
                    htStyles.Add("ErrorMessageFontSize", this.drperrormessagefontsize.SelectedItem.ToString());
                else
                    htStyles.Add("ErrorMessageFontSize", this.lblerrormessagefontsize.Text);

                if (this.txtinputtextbackcolor.Text != "")
                    htStyles.Add("InputTextBackColor", this.txtinputtextbackcolor.Text);
                else
                    htStyles.Add("InputTextBackColor", this.lblinputtextbackcolor.Text);

                if (this.txtinputtextbordercolor.Text != "")
                    htStyles.Add("InputTextBorderColor", this.txtinputtextbordercolor.Text);
                else
                    htStyles.Add("InputTextBorderColor", this.lblinputtextbordercolor.Text);

                if (this.txtinputtextfontcolor.Text != "")
                    htStyles.Add("InputTextForeColor", this.txtinputtextfontcolor.Text);
                else
                    htStyles.Add("InputTextForeColor", this.lblinputtextforecolor.Text);

                if (this.drpinputtextfontsize.SelectedItem.ToString() != "Size")
                    htStyles.Add("InputTextFontSize", this.drpinputtextfontsize.SelectedItem.ToString());
                else
                    htStyles.Add("InputTextFontSize", this.lblinputtextfontsize.Text);

                if (this.drpinputtextfontname.SelectedItem.ToString() != "Select the Font")
                    htStyles.Add("InputTextFontName", this.drpinputtextfontname.SelectedItem.ToString());
                else
                    htStyles.Add("InputTextFontName", this.lblinputtextfontname.Text);

                return htStyles;
            }
            catch (Exception ex)
            {
                //throw ex;
                //ZD 100263
                log.Trace("Page_Load" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();
                //errLabel.Text = ex.StackTrace;
                errLabel.Visible = true;
                return htStyles;
            }
        }

        private Hashtable AssignMenuToHashtable(Hashtable hashTable)
        {
            try
            {
                if (hashTable == null)
                    hashTable = new Hashtable();

                if (this.m_mouseofffont.Text != "")
                    hashTable.Add("MMouseOffFont", this.m_mouseofffont.Text);

                if (this.d_mouseofffont.Text != "")
                    hashTable.Add("DMouseOffFont", this.d_mouseofffont.Text);

                if (this.m_mouseOffback.Text != "")
                    hashTable.Add("MMouseoffbackground", this.m_mouseOffback.Text);

                if (this.d_mouseOffback.Text != "")
                    hashTable.Add("DMouseoffbackground", this.d_mouseOffback.Text);

                if (this.m_mouseOnfont.Text != "")
                    hashTable.Add("MMouseonfont", this.m_mouseOnfont.Text);

                if (this.d_mouseOnfont.Text != "")
                    hashTable.Add("DMouseonfont", this.d_mouseOnfont.Text);

                if (this.m_mouseOnback.Text != "")
                    hashTable.Add("MMouseonbackground", this.m_mouseOnback.Text);

                if (this.d_mouseOnback.Text != "")
                    hashTable.Add("DMouseonbackground", this.d_mouseOnback.Text);

                if (this.m_menuborder.Text != "")
                    hashTable.Add("MMenuborder", this.m_menuborder.Text);

                if (this.d_menuborder.Text != "")
                    hashTable.Add("DMenuborder", this.d_menuborder.Text);

                if (this.m_headfontcol.Text != "")
                    hashTable.Add("MMenuheaderfontcolor", this.m_headfontcol.Text);

                if (this.d_headfontcol.Text != "")
                    hashTable.Add("DMenuheaderfontcolor", this.d_headfontcol.Text);

                if (this.m_headbackcol.Text != "")
                    hashTable.Add("MMenuheaderbackgroundcolor", this.m_headbackcol.Text);

                if (this.d_headbackcol.Text != "")
                    hashTable.Add("DMenuheaderbackgroundcolor", this.d_headbackcol.Text);
                //Response.Write(hashTable.Keys.Count);
                return hashTable;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Event Handler Methods To btnSave Button

        private void btnSave_Click(object sender, EventArgs e)
        {
            //code added for FB 1473
            Hashtable htStyles = null;
            bool ret = false;//ZD 100263
            try
            {
                if (Session["ApplicationPath"].ToString() == "")
                    throw new Exception("Application Path does not exist");

                //FB 2719 Starts
                StringBuilder inXml = new StringBuilder();
                String outXML = "";
                inXml.Append("<SetTheme>");
                inXml.Append(obj.OrgXMLElement());
                //inXml.Append("<organizationID>" + Session["orgID"].ToString() + "</organizationID>");
                inXml.Append("<ThemeType>" + (RDefault.Checked ? "0" : (RTheme1.Checked ? "1" : "2")) + "</ThemeType>");
                inXml.Append("</SetTheme>");
                outXML = obj.CallMyVRMServer("SetTheme", inXml.ToString(), Application["RTC_ConfigPath"].ToString());
                //FB 2719 Ends

                //if (ValidateImageSize()) //Organization CSS Project //FB 2739
                {
                    //code added for FB 1473 - start
                    if (hdnValue.Value != "")
                    {
                        cssUtil = new CSSReplacementUtility();

                        cssUtil.ApplicationPath = Session["ApplicationPath"].ToString();

                        if (hdnValue.Value == "Default")
                        {//ZD 100263
                            ret = cssUtil.DefaultAsPreview();
                            if (!ret)
                            {
                                errLabel.Text = obj.GetTranslatedText("Operation failed: Invalid security permission. Please contact your VRM Administrator");
                                errLabel.Visible = true;
                            }
                            Session["ThemeType"] = "0"; // FB 2719
                        }
                        else
                        {//ZD 100263
                            cssUtil.ThemeName = "Themes\\" + hdnValue.Value;                            //Moving files from the Orginal to Temp location

                            ret = cssUtil.ThemeAsPreview();
                            if (!ret)
                            {
                                errLabel.Text = obj.GetTranslatedText("Operation failed: Invalid security permission. Please contact your VRM Administrator");
                                errLabel.Visible = true;
                            }
                        }

                        // FB 2719 Starts
                        if (hdnValue.Value == "Theme1")
                        {
                            Session["ThemeType"] = "1";
                        }
                        else if (hdnValue.Value == "Theme2")
                        {
                            Session["ThemeType"] = "2";
                        }
                        // FB 2719 Ends
                        //Retrieve values from temp location to populate new control values
                        int errid = 0;//ZD 100263
                        htStyles = cssUtil.RetrieveTempStyles(ref errid);
                        if (errid == 1)
                        {
                            errLabel.Text = obj.GetTranslatedText("Operation failed: Invalid security permission. Please contact your VRM Administrator");
                            errLabel.Visible = true;
                        }

                        if (htStyles != null)
                        {
                            BindData(htStyles);
                            htStyles = null;
                        }

                        BindNewValues();
                    }
                    //code added for FB 1473 - end

                    if (hdnValue.Value != "")
                        themeName = hdnValue.Value;
                    else
                        themeName = "Custom";

                    SetUserPreferences();
                    //SetCompanyImages(); //Image Project//FB 2739

                    if (cssUtil == null)
                    {
                        cssUtil = new CSSReplacementUtility();
                        cssUtil.ApplicationPath = Session["ApplicationPath"].ToString();
                    }
                    //Moving files from the temp to Mirror location
                    ret = cssUtil.TempToMirrorLocation();//ZD 100263
                    if (!ret)
                    {
                        errLabel.Text = obj.GetTranslatedText("Operation failed: Invalid security permission. Please contact your VRM Administrator");
                        errLabel.Visible = true;
                    }    
                    this.errLabel.Text = obj.GetTranslatedText("Operation Successful! Please logout and re-login to see the changes.");//FB 1830 - Translation
                    BindData();
                }
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("Page_Load" + ex.Message);
                this.errLabel.Text = obj.ShowSystemMessage();
                //this.errLabel.Text = ex.Message;
            }
        }
        #endregion

        #region ValidateImageSize
        /// <summary>
        /// ValidateImageSize()
        /// </summary>
        private bool ValidateImageSize()
        {   
            HttpPostedFile myFile;
            int nFileLen;
            byte[] myData = null;
            String errMsg = "";
            string fileExtn = "", filextn = "", fName = "";//ZD 104387
            try
            {
                if (this.companylogo.Value != "")
                {
                    //ZD 104387 - Start
                    fName = Path.GetFileName(this.companylogo.Value.ToString());
                    if (fName != "")
                        filextn = fName.Substring(fName.LastIndexOf(".") + 1);

                    filextn = Path.GetExtension(fName);
                    //ZD 104387 - End    

                    myFile = companylogo.PostedFile;
                    nFileLen = myFile.ContentLength;
                    myData = new byte[nFileLen];
                    myFile.InputStream.Read(myData, 0, nFileLen);

                    if (nFileLen <= 3000000) //FB Icon
                    {
                        MemoryStream ms1 = new MemoryStream(myData, 0, myData.Length);
                        ms1.Write(myData, 0, myData.Length);
                        System.Drawing.Image SiteLogo = (System.Drawing.Image)System.Drawing.Image.FromStream(ms1);
                        string SiteWidth = SiteLogo.Width.ToString();
                        string Siteheight = SiteLogo.Height.ToString();
                        int width = 0;
                        int height = 0;
                        int.TryParse(SiteWidth, out width);
                        int.TryParse(Siteheight, out height);
						//ZD 104387 - Start
                        if (width == 1600 && height == 524)
                        {
                            Map2ImageDt.Value = imgUtilObj.ConvertByteArrToBase64(myData);

                            if (filextn.ToLower() == "gif")
                                Map2ImageCtrl.ImageType = myVRMWebControls.ImageType.Gif;
                            else
                                Map2ImageCtrl.ImageType = myVRMWebControls.ImageType.Jpeg;

                            Map2ImageCtrl.PersistenceType = myVRMWebControls.Persistence.Cache;
                            Map2ImageCtrl.Bitmap = (System.Drawing.Bitmap)System.Drawing.Bitmap.FromStream(ms1);
                            Map2ImageCtrl.Visible = true;
                        }
                        else
                            errMsg = obj.GetTranslatedText("Company Logo Banner exceeds the Resolution Limit");
                    }
                    else
                        errMsg = obj.GetTranslatedText("Company logo attachment is greater than 100KB. File has not been uploaded.");
                    //ZD 104387 - End

                    if (errMsg != "")
                    {
                        errLabel.Text = errMsg;
                        errLabel.Visible = true;
                        return false;
                    }
                }

                if (this.Bannerfile1024.Value.ToString() != "")
                {
                    //ZD 104387 - Start
                    fName = Path.GetFileName(this.Bannerfile1024.Value.ToString());
                    if (fName != "")
                        fileExtn = fName.Substring(fName.LastIndexOf(".") + 1);

                    fileExtn = Path.GetExtension(fName);
                    //ZD 104387 - End

                    myFile = Bannerfile1024.PostedFile;
                    nFileLen = myFile.ContentLength;
                    myData = new byte[nFileLen];
                    myFile.InputStream.Read(myData, 0, nFileLen);
                    if (nFileLen <= 500000)
                    {
                        MemoryStream ms1 = new MemoryStream(myData, 0, myData.Length);
                        ms1.Write(myData, 0, myData.Length);
                        System.Drawing.Image SiteLogo = (System.Drawing.Image)System.Drawing.Image.FromStream(ms1);
                        string SiteWidth = SiteLogo.Width.ToString();
                        string SiteHeight = SiteLogo.Height.ToString();
                        int width = 0;
                        int height = 0;
                        int.TryParse(SiteWidth, out width);
                        int.TryParse(SiteHeight, out height);
                        if (width > 200 || height > 75) // FB 2779
                        {
                            errMsg = obj.GetTranslatedText("Standard Resolution Banner exceeds the Resolution Limit");
                        }
                        else
                        {
							//ZD 104387 - Start
                            Map1ImageDt.Value = imgUtilObj.ConvertByteArrToBase64(myData); //FB 1633 moved
                            
                            if (fileExtn.ToLower() == "gif")
                                Map1ImageCtrl.ImageType = myVRMWebControls.ImageType.Gif;
                            else
                                Map1ImageCtrl.ImageType = myVRMWebControls.ImageType.Jpeg;

                            Map1ImageCtrl.PersistenceType = myVRMWebControls.Persistence.Cache;
                            Map1ImageCtrl.Bitmap = (System.Drawing.Bitmap)System.Drawing.Bitmap.FromStream(ms1);
                            Map1ImageCtrl.Visible = true;
                            //ZD 104387 - End
                        }
                    }
                    else
                        errMsg = obj.GetTranslatedText("Banner attachment is greater than 500KB. File has not been uploaded."); //ZD 101714

                    if (errMsg != "")
                    {
                        errLabel.Text = errMsg;
                        errLabel.Visible = true;
                        return false;
                    }
                }

                if (this.Bannerfile1600.Value.ToString() != "")
                {
                    myFile = Bannerfile1600.PostedFile;
                    nFileLen = myFile.ContentLength;
                    myData = new byte[nFileLen];
                    myFile.InputStream.Read(myData, 0, nFileLen);
                    if (nFileLen <= 500000)
                    {
                        MemoryStream ms1 = new MemoryStream(myData, 0, myData.Length);
                        ms1.Write(myData, 0, myData.Length);
                        System.Drawing.Image SiteLogo = (System.Drawing.Image)System.Drawing.Image.FromStream(ms1);
                        string SiteWidth = SiteLogo.Width.ToString();
                        string SiteHeight = SiteLogo.Height.ToString();
                        int width = 0;
                        int height = 0;
                        int.TryParse(SiteWidth, out width);
                        int.TryParse(SiteHeight, out height);
                        if (width > 1380 || height>72)
                        {
                            errMsg = "High Resolution Banner exceeds the Resolution Limit";
                        }
                    }
                    else
                        errMsg = "Banner attachment for high resolution is greater than 500KB. File has not been uploaded.";

                    if (errMsg != "")
                    {
                        errLabel.Text = errMsg;
                        errLabel.Visible = true;
                        return false;
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                //ZD 100263
                log.Trace("ValidateImageSize" + e.Message);
                this.errLabel.Text = obj.ShowSystemMessage();
                //this.errLabel.Text = e.Message;
                this.errLabel.Visible = true;
                return false;
            }
        }
        #endregion

        #region Event Handler Methods To Default Configuration Button

        private void btnOriginal_Click(object sender, EventArgs e)
        {
            Hashtable htStyles = null;

            try
            {
                //Response.Write("in function");
                //code added for FB 1473 - start
                CustomizeRow.Attributes.Add("Style", "Display:Block");
                //DefaultThemeRow.Attributes.Add("Style", "Display:None");
                RThemeCustom.Checked = true;
                hdnValue.Value = "";

                //ThemeType.SelectedValue = "1";
                //code added for FB 1473 - end

                CSSReplacementUtility cssUtil = new CSSReplacementUtility();

                cssUtil.ApplicationPath = Session["ApplicationPath"].ToString();

                //Moving files from the Orginal to Temp location
                bool ret = cssUtil.DefaultAsPreview();//ZD 100263
                if (!ret)
                {
                    errLabel.Text = obj.GetTranslatedText("Operation failed: Invalid security permission. Please contact your VRM Administrator");
                    errLabel.Visible = true;
                }

                //Retrieve values from temp location to populate new control values
                int errid = 0;
                htStyles = cssUtil.RetrieveTempStyles(ref errid);
                if (errid == 1)
                {
                    errLabel.Text = obj.GetTranslatedText("Operation failed: Invalid security permission. Please contact your VRM Administrator");
                    errLabel.Visible = true;
                }

                if (htStyles != null)
                {
                    BindData(htStyles);
                    htStyles = null;
                }
                BindNewValues();
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("Page_Load" + ex.Message);
                this.errLabel.Text = obj.ShowSystemMessage();
                //this.errLabel.Text = ex.Message;
            }
        }
        #endregion

        //Code added for FB 1473 - start

        #region Remove Image1 Click Event Handler
        void RImg1_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                cssUtil = new CSSReplacementUtility();
                cssUtil.ApplicationPath = Session["ApplicationPath"].ToString();
                cssUtil.RemoveImages("lobbytop1024.jpg");
                //FB 1633 start
                imgRemoved = true;
                cssUtil.PostBlankBanner(Session["BlankBannerPath"].ToString());
                SetCompanyImages();
                bool ret = cssUtil.TempToMirrorLocation();//ZD 100263
                if (!ret)
                {
                    errLabel.Text = obj.GetTranslatedText("Operation failed: Invalid security permission. Please contact your VRM Administrator");
                    errLabel.Visible = true;
                }                
                BindData();
                //FB 1633 end
                this.errLabel.Text = obj.GetTranslatedText("Operation Successful! Please logout and re-login to see the changes.");//FB 1830 - Translation
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Remove Image2 Click Event Handler

        void RImg2_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                cssUtil = new CSSReplacementUtility();
                cssUtil.ApplicationPath = Session["ApplicationPath"].ToString();
                cssUtil.RemoveImages("lobbytop1600.jpg");
                this.errLabel.Text = obj.GetTranslatedText("Operation Successful! Please logout and re-login to see the changes.");//FB 1830 - Translation
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Remove Image3 Click Event Handler
        void RImg3_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                cssUtil = new CSSReplacementUtility();
                cssUtil.ApplicationPath = Session["ApplicationPath"].ToString();
                cssUtil.RemoveImages("lobby_logo.jpg");
				//ZD 104387 - Start
                ImgRemCompBackground = true;
                cssUtil.PostBlankCompanyLogo(Session["BlankBannerPath"].ToString());
                SetCompanyImages();
                BindData();
				//ZD 104387 - End
                this.errLabel.Text = obj.GetTranslatedText("Operation Successful! Please logout and re-login to see the changes.");//FB 1830 - Translation
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        //Code added for FB 1473 - end

        //Image Project

        #region Set Organization Image
        /// <summary>
        /// Set Organization Image
        /// </summary>
        /// <returns></returns>
        private bool SetCompanyImages()
        {
            string logoImage = "";
            
            String logofName;
            String lobyfName;
            HttpPostedFile myFile;
            int nFileLen;
            byte[] myData = null;
            String errMsg = "";
            string inXML = "";
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();

                inXML = "<UpdateOrgImages>";
                inXML += obj.OrgXMLElement();//Organization Module 

                //ZD 104387 - Start
                //inXML += "<logoimagename></logoimagename>";
                //inXML += "<logoimage></logoimage>";                

                if (imgRemoved == true)
                {
                    Map1ImageDt.Value = imgUtilObj.ConvertImageToBase64(Session["BlankBannerPath"].ToString());
                    inXML += "<lobytopimagename>StdBanner.jpg</lobytopimagename>";
                    inXML += "<lobytopimage>" + Map1ImageDt.Value + "</lobytopimage>";
                    imgRemoved = false;
                }
                else if (ImgRemCompBackground == true)
                {
                    Map2ImageDt.Value = imgUtilObj.ConvertImageToBase64(Session["BlankBannerPath"].ToString());
                    inXML += "<logoimagename>CompanyLogo.jpg</logoimagename>";
                    inXML += "<logoimage>" + Map2ImageDt.Value + "</logoimage>";
                    ImgRemCompBackground = false;
                }
                //ZD 104387 - End
                else  //Added for FB 1633 end
                { 
                    if (this.companylogo.Value != "")
                    {
                        logofName = Path.GetFileName(companylogo.Value);
                        myFile = companylogo.PostedFile;
                        nFileLen = myFile.ContentLength;
                        myData = new byte[nFileLen];
                        myFile.InputStream.Read(myData, 0, nFileLen);

                        //logoImage = imgUtilObj.ConvertByteArrToBase64(myData);//ZD 104387

                        inXML += "<logoimagename>" + logofName + "</logoimagename>";
                        inXML += "<logoimage>" + Map2ImageDt.Value + "</logoimage>";//ZD 104387
                    }
                    else
                    {
                        inXML += "<logoimagename></logoimagename>";
                        inXML += "<logoimage></logoimage>";
                    }
                        
                    if (this.Bannerfile1024.Value.ToString() != "")
                    {
                        lobyfName = Path.GetFileName(Bannerfile1024.Value);
                        myFile = Bannerfile1024.PostedFile;
                        nFileLen = myFile.ContentLength;
                        myData = new byte[nFileLen];
                        myFile.InputStream.Read(myData, 0, nFileLen);
                        
                        inXML += "<lobytopimagename>" + lobyfName + "</lobytopimagename>";
                        inXML += "<lobytopimage>" + Map1ImageDt.Value + "</lobytopimage>";//ZD 104387
                        inXML += "<lobyhighimgname>" + lobyfName + "</lobyhighimgname>"; //FB 1633
                        inXML += "<lobytophighimage>" + Map1ImageDt.Value + "</lobytophighimage>"; //FB 1633//ZD 104387
                    }
                    else
                    {
                        inXML += "<lobytopimagename></lobytopimagename>";
                        inXML += "<lobytopimage></lobytopimage>";
                        inXML += "<lobyhighimgname></lobyhighimgname>"; //FB 1633
                        inXML += "<lobytophighimage></lobytophighimage>"; //FB 1633
                    }

                    /*commented for FB 1633
                    if (this.Bannerfile1600.Value.ToString() != "")
                    {
                        lobyhighfName = Path.GetFileName(Bannerfile1600.Value);
                        myFile = Bannerfile1600.PostedFile;
                        nFileLen = myFile.ContentLength;
                        myData = new byte[nFileLen];
                        myFile.InputStream.Read(myData, 0, nFileLen);
                        
                        bannerhighImage = imgUtilObj.ConvertByteArrToBase64(myData);

                        inXML += "<lobyhighimgname>" + lobyhighfName + "</lobyhighimgname>";
                        inXML += "<lobytophighimage>" + bannerhighImage + "</lobytophighimage>";
                    }
                    else
                    {
                        inXML += "<lobyhighimgname></lobyhighimgname>";
                        inXML += "<lobytophighimage></lobytophighimage>";
                    }*/
                }

                //Css Module starts...
                String cssXML = cssUtil.GetCSSXML();
                inXML += cssXML;
                //Css Module ends...

                inXML += "</UpdateOrgImages>";

                if (errMsg != "")
                {
                    this.errLabel.Text = errMsg;
                    this.errLabel.Visible = true;
                    return false;
                }
                
                String outXML = obj.CallMyVRMServer("UpdateOrgImages", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") >= 0)
                {
                    this.errLabel.Text = obj.ShowErrorMessage(outXML);
                    this.errLabel.Visible = true;
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                //ZD 100263
                log.Trace("SetCompanyImages" + e.Message);
                this.errLabel.Text = obj.ShowSystemMessage();
                //this.errLabel.Text = e.Message;
                this.errLabel.Visible = true;
                return false;
            }
        }
        #endregion

	    //FB 2739 Starts
        #region Event Handler Method To btnBanner Button
        /// <summary>
        /// btnBanner_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnBanner_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["ApplicationPath"].ToString() == "")
                    throw new Exception("Application Path does not exist");

                if (ValidateImageSize())
                {
                    cssUtil = new CSSReplacementUtility();
                    cssUtil.ApplicationPath = Session["ApplicationPath"].ToString();

                    if (this.Bannerfile1024.Value.ToString() != "")
                        cssUtil.PostArtifactsBanner(Bannerfile1024);

                    if (this.Bannerfile1600.Value.ToString() != "")
                        cssUtil.PostArtifactsBanner(Bannerfile1600);

                    if (this.companylogo.Value.ToString() != "")//ZD 104387
                        cssUtil.PostArtifactsLogo(companylogo);

                    SetCompanyImages();
                    //cssUtil.TempToMirrorLocation(); 
					//ZD 100263
                    bool ret = cssUtil.TempBannerToMirrorLocation(); //Only we need to move the Banner images. So created a new method in cssUtil.
                    if (!ret)
                    {
                        errLabel.Text = obj.GetTranslatedText("Operation failed: Invalid security permission. Please contact your VRM Administrator");
                        errLabel.Visible = true;
                    }  
                    

                    this.errLabel.Text = obj.GetTranslatedText("Operation successful! Please logout and re-login to see the changes.");
                }//ZD 104387
                else
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("btnBanner_Click" + ex.Message);
                this.errLabel.Text = obj.ShowSystemMessage();
                //this.errLabel.Text = ex.Message;
            }
        }
        #endregion
	    //FB 2739 End

        //ZD 104387 - Start
        #region Display Company Logo
        /// <summary>
        /// Display Company Logo
        /// </summary>
        /// <param name="imgNode"></param>        
        private void DisplayCompanyImages()
        {
            myVRMNet.ImageUtil imageObj = new myVRMNet.ImageUtil();
            string OrgBanner1024Path = "";
            string CompanyLogo = "";
            byte[] imageData = null;
            string inXml = "", outxml = "", fileext = "";
            XmlDocument xd = null;
            XmlNode node = null;
            try
            {
                inXml = "<GetOrgImages>";
                inXml += obj.OrgXMLElement();
                inXml += "</GetOrgImages>";

                outxml = obj.CallCommand("GetOrgImages", inXml);
                if (outxml.IndexOf("<error>") < 0)
                {
                    xd = new XmlDocument();
                    xd.LoadXml(outxml);

                    node = xd.SelectSingleNode("//GetOrgImages/LobyTop");
                    if (node != null)
                        Map1ImageDt.Value = node.InnerText;

                    node = xd.SelectSingleNode("//GetOrgImages/Logo");
                    if (node != null)
                        Map2ImageDt.Value = node.InnerText;

                    if (Session["orgStdBannerPath"] != null)
                    {
                        if (Session["orgStdBannerPath"].ToString() != "")
                        {
                            OrgBanner1024Path = Session["orgStdBannerPath"].ToString();

                            if (OrgBanner1024Path != "")
                                fileext = OrgBanner1024Path.Substring(OrgBanner1024Path.LastIndexOf(".") + 1);

                            if (fileext.ToLower() == "gif")
                                Map1ImageCtrl.ImageType = myVRMWebControls.ImageType.Gif;
                            else
                                Map1ImageCtrl.ImageType = myVRMWebControls.ImageType.Jpeg;

                            Map1ImageCtrl.PersistenceType = myVRMWebControls.Persistence.Cache;

                            if (!string.IsNullOrEmpty(Map1ImageDt.Value) && !string.IsNullOrEmpty(OrgBanner1024Path))
                            {
                                imageData = imageObj.ConvertBase64ToByteArray(Map1ImageDt.Value);
                                MemoryStream ms1 = new MemoryStream(imageData, 0, imageData.Length);
                                Map1ImageCtrl.Bitmap = (System.Drawing.Bitmap)System.Drawing.Bitmap.FromStream(ms1);
                                imageData = null;
                                Map1ImageCtrl.Visible = true;
                            }
                        }
                    }
                    if (Session["CompanyLogo"] != null)
                    {
                        if (Session["CompanyLogo"].ToString() != "")
                        {
                            CompanyLogo = Session["CompanyLogo"].ToString();

                            if (CompanyLogo != "")
                                fileext = CompanyLogo.Substring(CompanyLogo.LastIndexOf(".") + 1);

                            if (fileext.ToLower() == "gif")
                                Map2ImageCtrl.ImageType = myVRMWebControls.ImageType.Gif;
                            else
                                Map2ImageCtrl.ImageType = myVRMWebControls.ImageType.Jpeg;

                            Map2ImageCtrl.PersistenceType = myVRMWebControls.Persistence.Cache;

                            if (!string.IsNullOrEmpty(Map2ImageDt.Value) && !string.IsNullOrEmpty(CompanyLogo))
                            {
                                imageData = imageObj.ConvertBase64ToByteArray(Map2ImageDt.Value);
                                MemoryStream ms1 = new MemoryStream(imageData, 0, imageData.Length);
                                Map2ImageCtrl.Bitmap = (System.Drawing.Bitmap)System.Drawing.Bitmap.FromStream(ms1);
                                imageData = null;
                                Map2ImageCtrl.Visible = true;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion
        //ZD 104387 - End
    }
}

