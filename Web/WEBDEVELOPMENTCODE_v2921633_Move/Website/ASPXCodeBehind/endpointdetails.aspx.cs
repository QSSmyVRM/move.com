/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Text;
///<summary>
/// Summary description for endpointdetails
///</summary>

public partial class en_endpointdetails : System.Web.UI.Page
{
    #region protected Data Members
    protected System.Web.UI.WebControls.Label multicodec; //FB 2400
    protected System.Web.UI.WebControls.Label ErrLabel;
    protected System.Web.UI.WebControls.Label LblName;
    protected System.Web.UI.WebControls.Label LblPass;
    protected System.Web.UI.WebControls.Label LblAddrType;
    protected System.Web.UI.WebControls.Label LblAddr;
    protected System.Web.UI.WebControls.Label LblMdl;
    protected System.Web.UI.WebControls.Label LblPreDialingOpn;
    protected System.Web.UI.WebControls.Label LblPreBandWidth;
    protected System.Web.UI.WebControls.Label LblAssToMcu;
    protected System.Web.UI.WebControls.Label LblLocatedOutsideNet;
    protected System.Web.UI.WebControls.Label LblWebAccURL;
    //Code Added For FB 1422
    protected System.Web.UI.WebControls.Label LblTelnet;
    protected System.Web.UI.WebControls.Label LblExchange;

    #endregion

        #region Variables Declaction

        private myVRMNet.NETFunctions obj=null;
        private ns_Logger.Logger log;
        
        #endregion

        //ZD 101022
        #region InitializeCulture
        protected override void InitializeCulture()
        {
            if (Session["UserCulture"] != null)
            {
                UICulture = Session["UserCulture"].ToString();
                Culture = Session["UserCulture"].ToString();
                base.InitializeCulture();
            }
        }
        #endregion

        #region Page Load Event Hander

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.AccessandURLConformityCheck("endpointdetails.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
                obj = new myVRMNet.NETFunctions();
                log = new ns_Logger.Logger();

                BindData();
            }
            catch (Exception ex) 
            {
                ErrLabel.Visible=true;
                //ZD 100263
                ErrLabel.Text = obj.ShowSystemMessage();
                //ErrLabel.Text="Page Load: " + ex.Message;
                WriteIntoLog(ex.StackTrace);
            }
        }

        #endregion

        #region BindData
        /// <summary>
        /// Construct inXML. Get the outXML from Com (Command : GetEndpoint) and bind it to the controls
        /// Application["COM_ConfigPath"] refers ComConfig.xml file path
        /// </summary>
        
        private void BindData()
        {
            XmlNode node=null;
            
            try{
                String inXML= BuildInXML();
                String typeaddrType = "";
                String typevideo = "";
                String typerate = "";
                String typebridge = "";

                //FB 2027
                //String outXML = obj.CallCOM("GetEndpoint", inXML, Application["COM_ConfigPath"].ToString());
                String outXML = obj.CallMyVRMServer("GetEndpoint", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                
                if(outXML!=" ")
                {
                    XmlDocument xmlDoc=new XmlDocument();
                    xmlDoc.LoadXml(outXML);

                    //To bind Endpoint Details description in the header using endpoint node
                    node = xmlDoc.DocumentElement.SelectSingleNode("endpoint");
                    
                    this.LblName.Text = node.SelectSingleNode("name").InnerText.Trim();
                    this.LblPass.Text=node.SelectSingleNode("password").InnerText.Trim();
                    this.LblAddr.Text = node.SelectSingleNode("address").InnerText.Trim();
                    XmlNodeList addListType = xmlDoc.DocumentElement.SelectNodes("//getEndpoint/addressType");
                    typeaddrType = node.SelectSingleNode("//endpoint/addressType").InnerText.Trim();

                    if (addListType != null)
                    {
                        XmlNodeList addListCountType= addListType[0].SelectNodes("type");
                        foreach (XmlNode nodeType in addListCountType)
                        {
                            if (nodeType.SelectSingleNode("ID").InnerText.Trim() == typeaddrType)
                            {
                                this.LblAddrType.Text = obj.GetTranslatedText(nodeType.SelectSingleNode("name").InnerText.Trim());
                                break;
                            }
                           
                        }
                        addListType = null;
                    }
                    //FB 2400 start
                    String adds = "";
                    XmlNodeList nodes = null;
                    nodes = node.SelectNodes("//getEndpoint/endpoint/MultiCodec");
                    foreach (XmlNode node1 in nodes)
                    {
                        adds = "<table>";
                        XmlNodeList nodess = node1.SelectNodes("Address");
                        foreach (XmlNode snode in nodess)
                            adds += "<tr><td>" + snode.InnerText + "</td></tr>";

                        adds += "</table>";
                    }
                    
                    if (node.SelectSingleNode("isTelePresence").InnerText.Trim() == "1")
                    {
                        LblAddr.Text += "<a style='color:Blue' target='_blank' onMouseOver=\"toggleDiv('multiCodecPopUp',1);\" onMouseOut=\"toggleDiv('multiCodecPopUp',0);\"> "+   obj.GetTranslatedText("More")+" ...</a>";
                        multicodec.Text += "<div class='hdiv' id='multiCodecAdd'>" + adds + "</div>";
                    }
                    //FB 2400 End
                    //FB 2027 - Starts
                    XmlNodeList addListVideo = xmlDoc.DocumentElement.SelectNodes("//getEndpoint/VideoEquipment");
                    typevideo = node.SelectSingleNode("//endpoint/videoEquipment").InnerText.Trim();

                    if (addListVideo != null)
                    {
                        XmlNodeList addListCountVideo = addListVideo[0].SelectNodes("Equipment");
                        foreach (XmlNode nodeVideo in addListCountVideo)
                        {
                            if (nodeVideo.SelectSingleNode("VideoEquipmentID").InnerText.Trim() == typevideo)
                            {
                                this.LblMdl.Text = nodeVideo.SelectSingleNode("VideoEquipmentName").InnerText.Trim();
                                break;
                            }
                        }
                        addListVideo = null;
                    }

                    XmlNodeList addListRate = xmlDoc.DocumentElement.SelectNodes("//getEndpoint/LineRate");
                    typerate = node.SelectSingleNode("//endpoint/lineRate").InnerText.Trim();

                    if (addListRate != null)
                    {
                        XmlNodeList addListCountRate = addListRate[0].SelectNodes("Rate");
                        foreach (XmlNode nodeRate in addListCountRate)
                        {
                            if (nodeRate.SelectSingleNode("LineRateID").InnerText.Trim() == typerate)
                            {
                                this.LblPreBandWidth.Text = nodeRate.SelectSingleNode("LineRateName").InnerText.Trim();
                                break;
                            }//FB 2027 - End
                        }
                        addListRate = null;
                    }


                    XmlNodeList addListBridge = xmlDoc.DocumentElement.SelectNodes("//getEndpoint/bridges");
                    typebridge = node.SelectSingleNode("//endpoint/bridge").InnerText.Trim();

                    if (addListBridge != null)
                    {
                        XmlNodeList addListCountBridge = addListBridge[0].SelectNodes("bridge");
                        foreach (XmlNode nodeBridge in addListCountBridge)
                        {
                            if (nodeBridge.SelectSingleNode("ID").InnerText.Trim() == typebridge)
                            {
                                this.LblAssToMcu.Text = nodeBridge.SelectSingleNode("name").InnerText.Trim();
                                break;
                            }
                        }
                        addListBridge = null;
                    }

                    this.LblPreDialingOpn.Text = (node.SelectSingleNode("connectionType").InnerText == "1") ? obj.GetTranslatedText("Dial-in to Conference") : obj.GetTranslatedText("Dial-out to Location"); //ZD 100619 
                    this.LblLocatedOutsideNet.Text=(node.SelectSingleNode("isOutside").InnerText== "1")? obj.GetTranslatedText("Yes") : obj.GetTranslatedText("No");
                    this.LblWebAccURL.Text=node.SelectSingleNode("URL").InnerText.Trim();
                    //Code Added For FB 1422
                    this.LblTelnet.Text = (node.SelectSingleNode("TelnetAPI").InnerText == "1") ? obj.GetTranslatedText("Yes") : obj.GetTranslatedText("No");
                    this.LblExchange.Text = node.SelectSingleNode("ExchangeID").InnerText;

                    
                }
                 else
                 {
                    ErrLabel.Visible = true;
                    ErrLabel.Text = obj.ShowErrorMessage(outXML);
                 }
            }
            catch(Exception ex)
            {
                throw ex;
            }
            finally
            {
                obj=null;
            }
        }

       #endregion

       #region BuildInXML
       /// <summary>
       /// To construct the inputXML for fetching the data
       /// </summary>
       /// <returns></returns>

       private String BuildInXML()
       {   //FB 2027 - Starts
           StringBuilder inXML = new StringBuilder();
           inXML.Append("<login>");
           inXML.Append(obj.OrgXMLElement());//Organization Module Fixes
           inXML.Append("<userID>" + Session["userID"].ToString() + "</userID>");
           inXML.Append("<endpointID>" + Request.QueryString["eid"].ToString() + "</endpointID>");
           inXML.Append("</login>");
           log.Trace(inXML.ToString());
           
           return inXML.ToString();
           //FB 2027 - End
       }

     #endregion

     #region WriteIntoLog

     private void WriteIntoLog(String stackTrace)
     {
        log = new ns_Logger.Logger();
        log.Trace(stackTrace);
        log = null;
     }

     #endregion

        
}