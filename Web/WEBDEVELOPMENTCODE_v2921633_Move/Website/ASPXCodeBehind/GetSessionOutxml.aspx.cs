/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;

public partial class en_GetSessionOutxml : System.Web.UI.Page
{

    #region private data members  

    protected String confType = "";
    protected String outXML = "";
    protected String cfnm = "";
    protected String userEmail = "";
    protected String recurpattern = "";
    protected String confstart = "";
    protected String confend = "";
    protected String cfuid = "";
    protected String descriptionstr = "";
    protected String curtime = "";
    protected String STDSTART = "";
    protected String DLTSTART = "";
    protected String locationstr = "";
    protected String tp = "";
    protected String confID = "";

    ns_Logger.Logger log;
    myVRMNet.NETFunctions obj;
    protected Boolean isrecurconf = false;

    #endregion

    #region Page Load Event Handler  

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (obj == null)
                obj = new myVRMNet.NETFunctions();
            obj.AccessandURLConformityCheck("GetSessionOutxml.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
            log = new ns_Logger.Logger();
            obj = new myVRMNet.NETFunctions();
                       

            if (Request.QueryString["f"] != "")
                confType = Request.QueryString["f"];

            if (Request.QueryString["ii"] != "")
                confID = Request.QueryString["ii"];

            if (Session["outXML"] != null)
            {
                if (Session["outXML"].ToString() != "")
                    outXML = Session["outXML"].ToString();
            }

            BindData();
        }
        catch (Exception ex)
        {
            log.Trace("PageLoad: " + ex.StackTrace + " : " + ex.Message);
        }
    }

    #endregion

    #region BindData  

    protected void BindData()
    {
        try
        {

            if (confType == "1" || confType == "2" || confType == "3")
            {
                string inXML = "<login>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><confID>" + confID + "</confID></login>";//Organization Module Fixes

                //String outXML1 = obj.CallCOM("GetConfGMTInfo", inXML, Application["COM_ConfigPath"].ToString());
                String outXML1 = obj.CallMyVRMServer("GetConfGMTInfo", inXML, Application["MyVRMServer_ConfigPath"].ToString()); // FB 2027

                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML1);

                //Code Modified for FB 1676 -- Start
                String startDT = xmldoc.DocumentElement.SelectSingleNode("start").InnerText;
                String tzName = xmldoc.DocumentElement.SelectSingleNode("timezoneName").InnerText;
                String tzNameID = xmldoc.DocumentElement.SelectSingleNode("timezoneID").InnerText;
                inXML = "<ConvertFromGMT><DateTime>" + startDT + "</DateTime><TimeZone>" + tzNameID + "</TimeZone></ConvertFromGMT>";
                String outxml = obj.CallMyVRMServer("ConvertFromGMT", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                XmlDocument xmldoc1 = new XmlDocument();
                xmldoc1.LoadXml(outxml);
                startDT = xmldoc1.SelectSingleNode("//ConvertFromGMT/DateTime").InnerText;

                String endDT = xmldoc.DocumentElement.SelectSingleNode("end").InnerText;
                inXML = "<ConvertFromGMT><DateTime>" + endDT + "</DateTime><TimeZone>" + tzNameID + "</TimeZone></ConvertFromGMT>";
                outxml = obj.CallMyVRMServer("ConvertFromGMT", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                xmldoc1 = new XmlDocument();
                xmldoc1.LoadXml(outxml);
                endDT = xmldoc1.SelectSingleNode("//ConvertFromGMT/DateTime").InnerText;
                //Code Modified for FB 1676 -- End
                String curDT = xmldoc.DocumentElement.SelectSingleNode("current").InnerText;
               // String tzName = xmldoc.DocumentElement.SelectSingleNode("timezoneName").InnerText;

                String startDLT = xmldoc.DocumentElement.SelectSingleNode("daylightSaving/start").InnerText;
                String startSTD = xmldoc.DocumentElement.SelectSingleNode("daylightSaving/end").InnerText;

                DLTSTART = Convert.ToDateTime(startDLT).ToUniversalTime().ToString(DateFormat);
                STDSTART = Convert.ToDateTime(startSTD).ToUniversalTime().ToString(DateFormat);

                confstart = Convert.ToDateTime(startDT).ToUniversalTime().ToString(DateFormat);
                confend = Convert.ToDateTime(endDT).ToUniversalTime().ToString(DateFormat);
                curtime = Convert.ToDateTime(curDT).ToUniversalTime().ToString(DateFormat);

                xmldoc = new XmlDocument();
                outXML = outXML.Replace("& ", "&amp; ");
                xmldoc.LoadXml(outXML);

                string recurring = "0";
                cfuid = "";
                if (xmldoc.SelectSingleNode("/conference/confInfo/recurring") != null)
                    recurring = obj.GetProperValue(xmldoc.SelectSingleNode("/conference/confInfo/recurring").InnerText);

                descriptionstr = obj.GetProperValue(xmldoc.SelectSingleNode("/conference/confInfo/description").InnerText);
                cfnm = obj.GetProperValue(xmldoc.SelectSingleNode("/conference/confInfo/confName").InnerText);

                XmlNodeList nodes = xmldoc.SelectNodes("/conference/confInfo/locationList/selected/level1ID");
                Int32[] selectedRooms = new Int32[nodes.Count]; 
                int index = 0;
                foreach (XmlNode selNode in nodes)
                {
                    selectedRooms[index] = new Int32();
                    selectedRooms[index] = Convert.ToInt32(selNode.InnerText);
                    index++;
                }

                locationstr = "";
                nodes = xmldoc.SelectNodes("/conference/confInfo/locationList/level3List/level3");
                foreach (XmlNode node in nodes)
                {
                    XmlNodeList nodes2 = node.SelectNodes("level2List/level2");
                    foreach (XmlNode node2 in nodes2)
                    {
                        XmlNodeList nodes1 = node2.SelectNodes("level1List/level1");
                        foreach (XmlNode node1 in nodes1)
                        {
                            if (IsRoomSelected(Convert.ToInt32(node1.SelectSingleNode("level1ID").InnerText), selectedRooms))
                            {
                                if (locationstr == "")
                                    locationstr = node1.SelectSingleNode("level1Name").InnerText;
                                else
                                    locationstr = locationstr + "," + node1.SelectSingleNode("level1Name").InnerText;
                            }
                        }
                    }
                }

                if (recurring.Equals("1"))
                {
                    isrecurconf = true;
                    string recurstr = xmldoc.SelectSingleNode("/conference/confInfo/appointmentTime").InnerXml;
                    recurstr += xmldoc.SelectSingleNode("/conference/confInfo/recurrencePattern").InnerXml;
                    if (xmldoc.SelectNodes("/conference/confInfo/recurrenceRange").Count != 0)
                        recurstr += xmldoc.SelectSingleNode("/conference/confInfo/recurrenceRange").InnerXml;
                    recurstr = "<recurstr>" + recurstr + "</recurstr>";


                    string tzstr = "<TimeZone>" + xmldoc.SelectSingleNode("/conference/confInfo/timezones").InnerXml + "</TimeZone>";
                    string rst = obj.AssembleRecur(recurstr, tzstr);
                    string[] rst_array = rst.Split('|');
                    string recur = rst_array[0];

                    String[] recDateArr = recur.Split('#');

                    recur = "";

                    if (recDateArr.Length > 0)
                    {
                        String[] recArr = recDateArr[1].Split('&');
                        String[] recArr1 = recDateArr[2].Split('&');

                        switch (recArr[0])
                        {
                            case "1":
                                recurpattern = recurpattern + "FREQ=DAILY;";
                                switch (recArr[1])
                                {
                                    case "1":
                                        recurpattern = recurpattern + "INTERVAL=" + recArr[2] + ";";
                                        break;
                                    case "2":
                                        recurpattern = recurpattern + "INTERVAL=1;BYDAY=MO,TU,WE,TH,FR;";
                                        break;
                                    default:
                                        Response.Redirect("underconstruction.aspx?wintype=pop");
                                        Response.End();
                                        break;
                                }
                                break;
                            case "2":
                                recurpattern = recurpattern + "FREQ=WEEKLY;";
                                String[] wdary = recArr[4].Split(',');
                                String wkDay = "";
                                for (Int32 i = 0; i < wdary.Length; i++)
                                    wkDay = wkDay + getWeekDay(Convert.ToInt32(wdary[i])) + ",";

                                wkDay = obj.DelStringRev(wkDay, ",");
                                recurpattern = recurpattern + "INTERVAL=" + recArr[3] + ";BYDAY=" + wkDay + ";";
                                break;
                            case "3":
                                recurpattern = recurpattern + "FREQ=MONTHLY;";
                                switch (recArr[5])
                                {
                                    case "1":
                                        recurpattern = recurpattern + "INTERVAL=" + recArr[7] + ";" + "BYMONTHDAY=" + recArr[6] + ";";
                                        break;
                                    case "2":
                                        recurpattern = recurpattern + "INTERVAL=" + recArr[10] + ";" + "BYDAY=" + getMonthWeekDay(Convert.ToInt32(recArr[9])) + ";" + "BYSETPOS=" + getMonthWeekSet(Convert.ToInt32(recArr[8])) + ";";
                                        break;
                                    default:
                                        Response.Redirect("underconstruction.aspx?wintype=pop");
                                        Response.End();
                                        break;
                                }
                                break;
                            case "4":
                                recurpattern = recurpattern + "FREQ=YEARLY;";
                                switch (recArr[11])
                                {
                                    case "1":
                                        recurpattern = recurpattern + "INTERVAL=1;BYMONTHDAY=" + recArr[13] + ";BYMONTH=" + recArr[12] + ";";
                                        break;
                                    case "2":
                                        recurpattern = recurpattern + "INTERVAL=1;BYDAY=" + getMonthWeekDay(Convert.ToInt32(recArr[15])) + ";BYMONTH=" + recArr[16] + ";BYSETPOS=" + getMonthWeekSet(Convert.ToInt32(recArr[14])) + ";";
                                        break;
                                    default:
                                        Response.Redirect("underconstruction.aspx?wintype=pop");
                                        Response.End();
                                        break;
                                }
                                break;
                            default:
                                Response.Redirect("underconstruction.aspx?wintype=pop");
                                Response.End();
                                break;

                        }

                        switch (recArr1[1])
                        {
                            case "1":
                            case "2":
                                recurpattern = recurpattern + "COUNT=" + recArr1[2] + ";";
                                break;
                            case "3":
                                recurpattern = recurpattern + "UNTIL=" + Convert.ToDateTime(recArr1[0]).ToString(DateFormat).ToString() + ";";
                                break;
                            default:
                                Response.Redirect("underconstruction.aspx?wintype=pop");
                                Response.End();
                                break;
                        }

                        recurpattern = recurpattern + "WKST=SU";

                    }
                }
            }

            BindFile();

        }
        catch (Exception ex)
        {
            log.Trace("BindData: " + ex.StackTrace + " : " + ex.Message);
        }
    }

    #endregion

    #region BindFile  

    protected void BindFile()
    {
        try
        {
            HttpContext ctx = null;

            ctx = HttpContext.Current;

            curtime = DateTime.Now.ToString(DateFormat).ToString();

            ctx.Response.ContentType = "text/x-vCalendar";
            String filename = cfnm.Replace("/", "").Replace("\\", "").Replace(":", "").Replace("?", "").Replace("<", "").Replace(">", "").Replace("|", "");
            ctx.Response.AddHeader("content-disposition", "attachment; filename=" + filename + ".ics");

            ctx.Response.Write("BEGIN:VCALENDAR");
            ctx.Response.Write("\nVERSION:2.0");
            ctx.Response.Write("\nMETHOD:PUBLISH");

            if (isrecurconf)
            {
                Response.Write("\nBEGIN:VTIMEZONE");
                Response.Write("\nTZID:Eastern Time (US + Canada)");
                Response.Write("\nBEGIN:STANDARD");
                //Response.Write("\nDTSTART:" + STDSTART); //Commented for FB 1676
                Response.Write("\nRRULE:FREQ=YEARLY;INTERVAL=1;BYDAY=-1SU;BYMONTH=10");
                Response.Write("\nTZOFFSETFROM:-0400");
                Response.Write("\nTZOFFSETTO:-0500");
                Response.Write("\nTZNAME:Standard Time");
                Response.Write("\nEND:STANDARD");
                //Response.Write("\nBEGIN:DAYLIGHT"); //Commented for FB 1676
                //Response.Write("\nDTSTART:" + DLTSTART);
                //Response.Write("\nRRULE:FREQ=YEARLY;INTERVAL=1;BYDAY=1SU;BYMONTH=4");
                //Response.Write("\nTZOFFSETFROM:-0500");
                //Response.Write("\nTZOFFSETTO:-0400");
                //Response.Write("\nTZNAME:Daylight Savings Time");
                //Response.Write("\nEND:DAYLIGHT");
                Response.Write("\nEND:VTIMEZONE");
            }

            ctx.Response.Write("\nBEGIN:VEVENT");
            ctx.Response.Write("\nORGANIZER:MAILTO:" + userEmail);
            if (confType == "2" && isrecurconf)
            {
                Response.Write("\nDTSTART;TZID=" + (Char)34 + "Eastern Time (US + Canada)" + (Char)34 + ":" + confstart);
                Response.Write("\nDTEND;TZID=" + (Char)34 + "Eastern Time (US + Canada)" + (Char)34 + ":" + confend);
            }
            else
            {
                ctx.Response.Write("\nDTSTART:" + confstart);
                ctx.Response.Write("\nDTEND:" + confend);
            }

            if (confType == "2" && isrecurconf)
                Response.Write("\nRRULE:" + recurpattern);

            ctx.Response.Write("\nLocation;ENCODING=QUOTED-PRINTABLE:" + locationstr);
            ctx.Response.Write("\nTRANSP:OPAQUE");
            ctx.Response.Write("\nSEQUENCE:0");
            ctx.Response.Write("\nUID:" + cfuid + confstart);
            ctx.Response.Write("\nDTSTAMP:" + curtime);
            ctx.Response.Write("\nDESCRIPTION:" + descriptionstr);
            ctx.Response.Write("\nSUMMARY;ENCODING=QUOTED-PRINTABLE:" + cfnm);
            ctx.Response.Write("\nPRIORITY:3");
            ctx.Response.Write("\nCLASS:PUBLIC");
            ctx.Response.Write("\nBEGIN:VALARM");
            ctx.Response.Write("\nTRIGGER:PT15M");
            ctx.Response.Write("\nACTION:DISPLAY");
            ctx.Response.Write("\nDESCRIPTION:Reminder");
            ctx.Response.Write("\nEND:VALARM");
            ctx.Response.Write("\nEND:VEVENT");
            ctx.Response.Write("\nEND:VCALENDAR");
            ctx.Response.End();
        }
        catch (Exception ex)
        {
            log.Trace("BindFile: " + ex.StackTrace + " : " + ex.Message);
        }
    }

    #endregion

    #region Helper Methods  

    #region getWeekDay 

    protected String getWeekDay(Int32 weekdayno)
    {
        String getWkDay = "";
        switch (weekdayno)
        {
            case 1:
                getWkDay = "SU";
                break;
            case 2:
                getWkDay = "MO";
                break;
            case 3:
                getWkDay = "TU";
                break;
            case 4:
                getWkDay = "WE";
                break;
            case 5:
                getWkDay = "TH";
                break;
            case 6:
                getWkDay = "FR";
                break;
            case 7:
                getWkDay = "SA";
                break;
            default:
                Response.Redirect("underconstruction.aspx?wintype=pop");
                break;
        }

        return getWkDay;
    }

    #endregion

    #region getMonthWeekSet 

    protected Int32 getMonthWeekSet(Int32 monthweeksetno)
    {
        Int32 monthwksetno = 0;
        switch (monthweeksetno)
        {
            case 1:
                monthwksetno = 1;
                break;
            case 2:
                monthwksetno = 2;
                break;
            case 3:
                monthwksetno = 3;
                break;
            case 4:
                monthwksetno = 4;
                break;
            case 5:
                monthwksetno = -1;
                break;
            default:
                Response.Redirect("underconstruction.aspx?wintype=pop");
                break;
        }

        return monthwksetno;
    }

    #endregion

    #region getMonthWeekDay 

    protected string getMonthWeekDay(Int32 monthweekdayno)
    {
        String monthwkdayno = "";
        switch (monthweekdayno)
        {
            case 1:
                monthwkdayno = "SU,MO,TU,WE,TH,FR,SA";
                break;
            case 2:
                monthwkdayno = "MO,TU,WE,TH,FR";
                break;
            case 3:
                monthwkdayno = "SU,SA";
                break;
            default:
                monthwkdayno = getWeekDay(monthweekdayno - 3).ToString();
                break;
        }

        return monthwkdayno;
    }

    #endregion

    #region IsRoomSelected 

    protected bool IsRoomSelected(Int32 level1ID, Int32[] roomID)
    {
        try
        {
            for (int i = 0; i < roomID.Length; i++)
                if (roomID[i].Equals(level1ID))
                    return true;
            return false;
        }
        catch (Exception ex)
        {
            log.Trace("IsRoomSelected: " + ex.StackTrace + " : " + ex.Message);
            return false;
        }
    }

    #endregion

    #region DateFormat 

    string DateFormat
    {
        get
        {
            return "yyyyMMddTHHmmssZ"; // 20060215T092000Z
        }

    }

    #endregion

    #endregion    

}
