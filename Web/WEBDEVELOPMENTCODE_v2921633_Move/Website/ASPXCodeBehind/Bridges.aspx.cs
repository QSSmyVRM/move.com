/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886

using System;
using System.Data;
using System.Xml;
using System.Web.UI.WebControls;
using System.Text;

/// <summary>
/// Summary description for ConferenceOrders
/// </summary>
/// 
namespace ns_Bridges
{
    public partial class Bridges : System.Web.UI.Page
    {
        myVRMNet.NETFunctions obj;
        
        protected System.Web.UI.WebControls.Label lblHeader;
        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.DropDownList lstBridgeType;
        protected System.Web.UI.WebControls.DropDownList lstBridgeStatus;
        protected System.Web.UI.WebControls.Table tblNoMCUs;
        protected System.Web.UI.WebControls.DataGrid dgMCUs;
        protected System.Web.UI.WebControls.TextBox txtBridges;
        protected System.Web.UI.WebControls.Label lblMCUmsg; // FB 1920
        protected ns_Logger.Logger log = null;//FB 2027
        protected int enableCloudInstallation = 0; //FB 2659

        private Int32 mculmt = 0;
        private Int32 existingMculmt = 0;//organization module
        //FB 2599 Start
        protected System.Web.UI.WebControls.Button btnNewMCU;//FB 2262
        protected int Cloud = 0; //Fb 2262
        //FB 2599 End
        public Bridges()
        {
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();//FB 2027
            //
            // TODO: Add constructor logic here
            //
        }

        //ZD 101022
        #region InitializeCulture
        protected override void InitializeCulture()
        {
            if (Session["UserCulture"] != null)
            {
                UICulture = Session["UserCulture"].ToString();
                Culture = Session["UserCulture"].ToString();
                base.InitializeCulture();
            }
        }
        #endregion

        #region Methods Executed on Page Load

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.AccessandURLConformityCheck("managebridge.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
                //FB 1920 Starts
                if (Session["organizationID"].ToString() == "11")
                    lblMCUmsg.Attributes.Add("style", "display:none");
                //FB 1920 Ends
                if (Session["McuLimit"] != null)
                {
                    if (Session["McuLimit"].ToString() != "")
                        Int32.TryParse(Session["McuLimit"].ToString(), out mculmt);
                }

                //FB 2659
                if (Session["EnableCloudInstallation"] != null)
                    int.TryParse(Session["EnableCloudInstallation"].ToString().Trim(), out enableCloudInstallation);

                lblHeader.Text = obj.GetTranslatedText("Manage MCUs");//FB 1830 - Translation
                errLabel.Text = "";
                if (Request.QueryString["m"] != null)
                    if (Request.QueryString["m"].ToString().Equals("1"))
                    {
                        errLabel.Text = obj.GetTranslatedText("Operation Successful!");//FB 1830 - Translation
                        errLabel.Visible = true;
                    }
                //FB 2599 Start //FB 2717 Vidyo Integration Start
                /*if (Session["Cloud"] != null) //FB 2262
                {
                    if (Session["Cloud"].ToString() != "")
                        Int32.TryParse(Session["Cloud"].ToString(), out Cloud);
                }

                if (Cloud == 1) //FB 2262


                {
                    btnNewMCU.Visible = false;
                }*/
				//FB 2717 Vidyo Integration End
                //FB 2670
                if (Session["admin"].ToString().Trim().Equals("3"))
                {
                    
                    // btnNewMCU.ForeColor = System.Drawing.Color.Gray;// FB 2796
                    //btnNewMCU.Attributes.Add("Class", "btndisable");// FB 2796
                    btnNewMCU.Visible = false;//ZD 100263
                }
                else
                    btnNewMCU.Attributes.Add("Class", "altLongBlueButtonFormat");// FB 2796

                //FB 2599 End
                if (!IsPostBack)
                    BindData();
                else
                    if (Request.Params.Get("__EVENTTARGET").ToString().ToUpper().Equals("MANAGEORDER"))
                        ManageOrder();
            }
            catch (Exception ex)
            {
                //                Response.Write(ex.Message);
                errLabel.Visible = true;
                //errLabel.Text = "PageLoad: " + ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("Page_Load:" + ex.Message);//ZD 100263
            }

        }

        private void BindData()
        {
            try
            {
                Session["multisiloOrganizationID"] = null; //FB 2274
                String inXML = "";
                inXML += "<login>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                inXML += "</login>";
                String outXML = obj.CallMyVRMServer("GetBridgeList", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                //String outXML = obj.CallCOM("GetBridgeList", inXML, Application["COM_ConfigPath"].ToString());
                //String outXML = "<bridgeInfo><bridgeTypes><type><ID>1</ID><name>Polycom MGC 25</name><interfaceType>1</interfaceType></type><type><ID>2</ID><name>Polycom MGC 50</name><interfaceType>1</interfaceType></type><type><ID>3</ID><name>Polycom MGC 100</name><interfaceType>1</interfaceType></type><type><ID>4</ID><name>Codian MCU 4200</name><interfaceType>3</interfaceType></type><type><ID>5</ID><name>Codian MCU 4500</name><interfaceType>3</interfaceType></type><type><ID>6</ID><name> MSE 8000 Series</name><interfaceType>3</interfaceType></type><type><ID>7</ID><name>Tandberg MPS 800 Series</name><interfaceType>4</interfaceType></type></bridgeTypes><bridgeStatuses><status><ID>1</ID><name>Active</name></status><status><ID>2</ID><name>Maintenance</name></status><status><ID>3</ID><name>Disabled</name></status></bridgeStatuses><bridges><bridge><ID>1</ID><name>Test Bridge</name><interfaceType>3</interfaceType><administrator>VRM Administrator</administrator><exist>1</exist><status>1</status><order></order></bridge></bridges><totalNumber>1</totalNumber><licensesRemain>9</licensesRemain></bridgeInfo>";
                //Response.Write("<br>" + obj.Transfer(outXML));
                if (outXML.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    XmlNodeList nodes = xmldoc.SelectNodes("//bridgeInfo/bridges/bridge");
                    if (nodes.Count > 0)
                    {
                        XmlNodeList nodesTemp = xmldoc.SelectNodes("//bridgeInfo/bridgeTypes/type");
                        LoadList(lstBridgeType, nodesTemp);
                        nodesTemp = xmldoc.SelectNodes("//bridgeInfo/bridgeStatuses/status");
                        //1``Test Bridge||4``Test 4||
                        LoadList(lstBridgeStatus, nodesTemp);
                        LoadMCUGrid(nodes);
                        Label lblTemp = new Label();
                        DataGridItem dgFooter = (DataGridItem)dgMCUs.Controls[0].Controls[dgMCUs.Controls[0].Controls.Count - 1];
                        lblTemp = (Label)dgFooter.FindControl("lblTotalRecords");
                        lblTemp.Text = xmldoc.SelectSingleNode("//bridgeInfo/totalNumber").InnerText;


                        Int32.TryParse(lblTemp.Text, out existingMculmt);

                        lblTemp = (Label)dgFooter.FindControl("lblRemaining");
                        //lblTemp.Text = xmldoc.SelectSingleNode("//bridgeInfo/licensesRemain").InnerText;

                        lblTemp.Text = (mculmt - existingMculmt).ToString();//Organization Module

                        dgMCUs.Visible = true;
                        tblNoMCUs.Visible = false;
                        foreach (XmlNode node in nodes)
                            txtBridges.Text += node.SelectSingleNode("ID").InnerText + "``" + node.SelectSingleNode("name").InnerText + "||";

                    }
                    else
                    {
                        dgMCUs.Visible = false;
                        tblNoMCUs.Visible = true;
                    }
                }
                else
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                //errLabel.Text = "BindData: " + ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("BindData:" + ex.Message);//ZD 100263
            }
        }

        protected void LoadList(DropDownList lstTemp, XmlNodeList nodes)
        {
            try
            {
                XmlTextReader xtr;
                DataSet ds = new DataSet();

                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                DataTable dt = new DataTable();

                if (ds.Tables.Count > 0)
                    dt = ds.Tables[0];
                lstTemp.DataSource = dt;
                lstTemp.DataBind();
            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("LoadList:" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }

        protected void LoadMCUGrid(XmlNodeList nodes)
        {
            try
            {
                XmlTextReader xtr;
                DataSet ds = new DataSet();

                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                DataTable dt = new DataTable();

                if (ds.Tables.Count > 0)
                    dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    if (dr["exist"].ToString().Equals("1"))
                        dr["exist"] = obj.GetTranslatedText("Existing"); //FB 2272
                    else
                        dr["exist"] = obj.GetTranslatedText("Virtual");//FB 2272
                    dr["status"] = obj.GetTranslatedText(lstBridgeStatus.Items.FindByValue(dr["status"].ToString()).Text);//FB 2272
                    dr["interfaceType"] = lstBridgeType.Items.FindByValue(dr["interfaceType"].ToString()).Text;
                    
                    //FB 1462 start
                    if (dr["userstate"].ToString() == "I")
                    {
                        dr["administrator"] = "<font color=red>"+ dr["administrator"].ToString() + " (In-Active)</font>";
                    }
                    else if (dr["userstate"].ToString() == "D")
                    {
                        dr["administrator"] = "<font color=red>" + dr["administrator"].ToString() + "</font>";
                    }
                    //FB 1462 end
                }

                dgMCUs.DataSource = dt;
                dgMCUs.DataBind();
            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("LoadMCUGrid:" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }

        #endregion
        protected void DeleteMCU(object sender, DataGridCommandEventArgs e)
        {
            try
            {   //FB 2027 - Starts
                StringBuilder inXML = new StringBuilder();
                inXML.Append("<login>");
                inXML.Append(obj.OrgXMLElement());//Organization Module Fixes
                inXML.Append("<userID>" + Session["userID"].ToString() + "</userID>");
                inXML.Append("<bridge>");
                inXML.Append("<bridgeID>" + e.Item.Cells[0].Text + "</bridgeID>");
                inXML.Append("</bridge>");
                inXML.Append("</login>");
                log.Trace(inXML.ToString());
                //Response.Write(obj.Transfer(inXML));
                //String outXML = obj.CallCOM("DeleteBridge", inXML, Application["COM_ConfigPath"].ToString());
                String outXML = obj.CallMyVRMServer("DeleteBridge", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                //FB 2027 - End
                //Response.Write(obj.Transfer(outXML));
                //Response.End();
                if (outXML.IndexOf("<error>") < 0)
                {
                    Response.Redirect("ManageBridge.aspx?m=1");
                }
                else
                {
                    errLabel.Visible = true;
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                }
            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("DeleteMCU:" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }
        protected void EditMCU(object sender, DataGridCommandEventArgs e)
        {
            try
            {
                Session.Add("BridgeID", e.Item.Cells[0].Text);
                Response.Redirect("BridgeDetails.aspx");
            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("EditMCU:" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }
        protected void BindRowsDeleteMessage(Object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType.Equals(ListItemType.Item) || e.Item.ItemType.Equals(ListItemType.AlternatingItem))
                {
                    LinkButton btnTemp = (LinkButton)e.Item.FindControl("btnDelete");
                    // FB 1920 Starts
                    DataRowView row = e.Item.DataItem as DataRowView;
                    //ZD 103826 Start
                    btnTemp.Attributes.Add("onclick", "if (confirm('" + obj.GetTranslatedText("Are you sure you want to delete this MCU?") + "')) {DataLoading(1); return true;} else {DataLoading('0'); return false;}");
                    //ZD 103826 End
                }
            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("BindRowsDeleteMessage:" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }
        protected void CreateNewMCU(Object sender, EventArgs e)
        {
            Session.Add("BridgeID", "new");
            Response.Redirect("BridgeDetails.aspx");
        }
        protected void GenerateReport(Object sender, EventArgs e)
        {
            Response.Redirect("MCUResourceReport.aspx");
        }
        protected void ManageOrder()
        {
            try
            {
                String BridgeOrder = Request.Params["Bridges"].ToString();
                String inXML = "<login>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                inXML += "  <bridgeOrder>";
                for (int i = 0; i < BridgeOrder.Split(';').Length - 1; i++)
                {
                    inXML += "      <bridge>";
                    inXML += "          <order>" + (i + 1).ToString() + "</order>";
                    inXML += "          <bridgeID>" + BridgeOrder.Split(';')[i].ToString() + "</bridgeID>";
                    inXML += "      </bridge>";
                }
                inXML += "  </bridgeOrder>";
                inXML += "</login>";
                //Response.Write(obj.Transfer(inXML));
                //Response.End();
                //String outXML = obj.CallMyVRMServer("SetBridge", inXML, Application["MyVRMServer_ConfigPath"].ToString());//Wrong Command used
                //String outXML = obj.CallCOM("SetBridgeList", inXML, Application["COM_ConfigPath"].ToString());
                String outXML = obj.CallMyVRMServer("SetBridgeList", inXML, Application["MyVRMServer_ConfigPath"].ToString());//FB 2027
                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
                else
                    Response.Redirect("ManageBridge.aspx?m=1");
            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("BindRowsToGrid:" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }
        // Method Added for FB 1920  
        #region BindRowsToGrid
        protected void BindRowsToGrid(Object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType.Equals(ListItemType.Item) || e.Item.ItemType.Equals(ListItemType.AlternatingItem))
                {
                    DataRowView row = e.Item.DataItem as DataRowView;
					//FB 2599 Start
                    LinkButton btnTemp = (LinkButton)e.Item.FindControl("btnDelete");
                    LinkButton btnTemp2 = (LinkButton)e.Item.FindControl("btnEdit");
					//FB 2599 End
                   // string MCUispublic = node.SelectSingleNode("isPublic").InnerText;
                    //FB 2599 Start
                    if (Cloud == 1) //FB 2262
                    {
                        btnTemp.Enabled = false;
                        btnTemp2.Enabled = false;
                    }

                    if (Session["organizationID"].ToString() != "11" && enableCloudInstallation == 0)//FB 2659
                    {
                        if (row["isPublic"].ToString() != "0" && row["Orgid"].ToString() == "11")
                        {
                            //LinkButton btnTemp = (LinkButton)e.Item.FindControl("btnDelete");//FB 2262
                            //LinkButton btnTemp2 = (LinkButton)e.Item.FindControl("btnEdit");//FB 2262
                            btnTemp.Enabled = false;
                            btnTemp2.Enabled = false;
                        }
                    }
                    //FB 2599 End
                    //FB 2670
                    if (Session["admin"].ToString() == "3") 
                    {
                        btnTemp2.Text = obj.GetTranslatedText("View");
                        
                        //btnTemp.Attributes.Remove("onClick");
                        
                        //btnTemp.Style.Add("cursor", "default");
                        //btnTemp.ForeColor = System.Drawing.Color.Gray;
                        btnTemp.Visible = false;
                        //ZD 100263
                    }
					//FB 2659 Starts
                     if (enableCloudInstallation == 1)
                    {
                        if (row["isPublic"].ToString() != "0" && row["Orgid"].ToString() == "11" && Session["organizationID"].ToString() != "11")
                        {
                            btnTemp.Enabled = false;
                        }
                    }
					//FB 2659 End

                }
            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.Message;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("BindRowsToGrid:" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }
        #endregion

        //FB 1920
        protected void ManageMCUOrder(Object sender, EventArgs e)
        {
            try
            {
                ManageOrder();

            }
            catch (Exception ex)
            {
                
                 //errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("ManageMCUOrder:" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }
        
    }
}
