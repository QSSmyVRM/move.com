﻿/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using DevExpress.Web.ASPxHtmlEditor;


namespace ns_MyVRM
{
    public partial class CustomizeLicenseAgreement : System.Web.UI.Page
    {
        #region Private Data Members

        myVRMNet.NETFunctions obj;
        ns_Logger.Logger log;

        #endregion


        protected System.Web.UI.WebControls.Label lblHeader;
        protected System.Web.UI.WebControls.Label lblError;
        protected DevExpress.Web.ASPxHtmlEditor.ASPxHtmlEditor dxHTMLEndUsrLicAgr;
        protected System.Web.UI.WebControls.Button btnsubmit;//FB 2670

        #region CustomizeLicenseAgreement
        /// <summary>
        /// CustomizeLicenseAgreement
        /// </summary>
        public CustomizeLicenseAgreement()
        {
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
            //
            // TODO: Add constructor logic here
            //
        }
        #endregion

        #region Page_Load
        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.AccessandURLConformityCheck("customizelicenseagreement.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
                //FB 2670
                if (Session["admin"].ToString().Equals("3"))
                {
                    btnsubmit.Enabled = false;
                    //btnsubmit.ForeColor = System.Drawing.Color.Gray; // FB 2796
                    btnsubmit.Attributes.Add("Class", "btndisable");// FB 2796
                }
                else
                    btnsubmit.Attributes.Add("Class", "altMedium0BlueButtonFormat");// FB 2796
                if (!IsPostBack)
                {
                    lblHeader.Text = obj.GetTranslatedText(lblHeader.Text);
                    LoadLicenseAgreement();
                }
            }
            catch (Exception ex)
            {
                log.Trace("Page_Load" + ex.Message);
                lblError.Text = obj.ShowSystemMessage();
            }
        }
        #endregion

        #region LoadLicenseAgreement
        /// <summary>
        /// LoadLicenseAgreement
        /// </summary>
        private void LoadLicenseAgreement()
        {
            try
            {
                String inXML = "", outXML = "";

                inXML = "<GetOrgLicenseAgreement>"
                      + obj.OrgXMLElement()
                      + "<userid>" + Session["userID"].ToString() + "</userid>"
                      + "</GetOrgLicenseAgreement>";
                outXML = obj.CallMyVRMServer("GetOrgLicenseAgreement", inXML, Application["MyVRMServer_ConfigPath"].ToString());

                if (outXML.IndexOf("<error>") < 0)
                {
                    XmlDocument xd = new XmlDocument();
                    xd.LoadXml(outXML);

                    if (xd.SelectSingleNode("//OrgLicenseAgreement/LicenseAgreement") != null)
                        dxHTMLEndUsrLicAgr.Html = xd.SelectSingleNode("//OrgLicenseAgreement/LicenseAgreement").InnerXml.Trim().Replace("§","&");
                }
                else if (outXML.IndexOf("<error>") >= 0)
                {
                    lblError.Text = obj.ShowErrorMessage(outXML);
                }

            }
            catch (Exception ex)
            {
                log.Trace("LoadLicenseAgreement" + ex.Message);
                lblError.Text = obj.ShowSystemMessage();
            }
        }
        #endregion

        #region SetOrgLicenseAgreement
        /// <summary>
        /// SetOrgLicenseAgreement
        /// </summary>
        protected void SetOrgLicenseAgreement(object sender, EventArgs e)
        {
            try
            {
                String inXML = "", outXML = "";

                inXML = "<SetOrgLicenseAgreement>"
                      + obj.OrgXMLElement()
                      + "<userid>" + Session["userID"].ToString() + "</userid>"
                      + "<EndUserLicAgrmnt>" 
                        + dxHTMLEndUsrLicAgr.Html.Replace("&", "§").Replace("<?xml:namespace prefix = o ?>","") 
                      + "</EndUserLicAgrmnt>"
                      + "</SetOrgLicenseAgreement>";

                outXML = obj.CallMyVRMServer("SetOrgLicenseAgreement", inXML, Application["MyVRMServer_ConfigPath"].ToString());

                if (outXML.IndexOf("<error>") >= 0)
                {
                    lblError.Text = obj.ShowErrorMessage(outXML);
                }
                else
                    GoBack(null, null);
            }
            catch (Exception ex)
            {
                log.Trace("SetOrgLicenseAgreement" + ex.Message);
                lblError.Text = obj.ShowSystemMessage();
            }
        }
        #endregion

        #region GoBack
        /// <summary>
        /// GoBack
        /// </summary>
        protected void GoBack(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("OrganisationSettings.aspx");
            }
            catch (Exception ex)
            {
                log.Trace("GoBack" + ex.Message);
            }
        }
        #endregion
    }
}
