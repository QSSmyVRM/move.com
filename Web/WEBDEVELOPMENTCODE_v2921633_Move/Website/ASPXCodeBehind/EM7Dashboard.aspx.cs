﻿/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;

using System.Xml;

public partial class en_EM7Dashboard : System.Web.UI.Page
{

    #region GeneralDeclaration

    protected System.Web.UI.WebControls.Label LblError;
    protected System.Web.UI.HtmlControls.HtmlControl Ifrm_EM7;
    protected System.Web.UI.WebControls.Image Image1;




    XmlDocument xmlDOC;
    myVRMNet.NETFunctions obj;
    ns_Logger.Logger log;
    StringBuilder INXML, OUTXML;
    string Username = "";
    string Password = "";
    string URL = "";

    #endregion


    # region Constructor
    /// <summary>
    /// Public Constructor 
    /// </summary>
    public en_EM7Dashboard()
    {
        obj = new myVRMNet.NETFunctions();
        log = new ns_Logger.Logger();
    }
    #endregion

    //ZD 101022
    #region InitializeCulture
    protected override void InitializeCulture()
    {
        if (Session["UserCulture"] != null)
        {
            UICulture = Session["UserCulture"].ToString();
            Culture = Session["UserCulture"].ToString();
            base.InitializeCulture();
        }
    }
    #endregion

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {

        try
        {
            if (obj == null)
                obj = new myVRMNet.NETFunctions();
            obj.AccessandURLConformityCheck("em7dashboard.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263


            xmlDOC = new XmlDocument();
            INXML = new StringBuilder();
            OUTXML = new StringBuilder();

            GenerateXML(ref INXML);
            log.Trace("GetSuperAdmin INXML: " + INXML.ToString());

            OUTXML.Append(obj.CallCommand("GetSuperAdmin", INXML.ToString()));
            log.Trace("OUTXML: " + OUTXML.ToString());

            xmlDOC.LoadXml(OUTXML.ToString());

            Username = "";
            Password = "";
            URL = "";

            if (OUTXML.ToString().IndexOf("<error>") > 0)
                LblError.Text = obj.ShowErrorMessage(OUTXML.ToString());


            else
            {
                if (xmlDOC.SelectSingleNode("//EM7Connectivity/EM7Username") != null)
                    Username = xmlDOC.SelectSingleNode("//EM7Connectivity/EM7Username").InnerText;

                if (xmlDOC.SelectSingleNode("//EM7Connectivity/EM7Password") != null)
                    Password = xmlDOC.SelectSingleNode("//EM7Connectivity/EM7Password").InnerText;

                if (xmlDOC.SelectSingleNode("//EM7Connectivity/EM7URI") != null && xmlDOC.SelectSingleNode("//EM7Connectivity/EM7URI").InnerText != "") //DD2
                {
                    if (xmlDOC.SelectSingleNode("//EM7Connectivity/EM7Port") != null)
                        URL = xmlDOC.SelectSingleNode("//EM7Connectivity/EM7Port").InnerText == "443" ? ("https://" + xmlDOC.SelectSingleNode("//EM7Connectivity/EM7URI").InnerText) : ("http://" + xmlDOC.SelectSingleNode("//EM7Connectivity/EM7URI").InnerText);
                }
                else
                    URL = "";
                Ifrm_EM7.Attributes.Add("src", URL);
                //ZD 102361
                //Image1.ToolTip = obj.GetTranslatedText("URL") + "=" + URL + "  " + obj.GetTranslatedText("Username") + "=" + Username + "  " + obj.GetTranslatedText("Password") + "=" + Password;
                Image1.ToolTip = obj.GetTranslatedText("URL") + "=" + URL + "  " + obj.GetTranslatedText("Username") + "=" + Username + "  ";
            }
        }
        catch (Exception ex)
        {
            log.Trace("GetHomeCommand: " + ex.Message);
        }
    }
    #endregion

    private void GenerateXML(ref StringBuilder INXML)
    {
        try
        {
            INXML.Append("<login>");
            INXML.Append(obj.OrgXMLElement());
            INXML.Append("<userID>" + Session["userID"].ToString() + "</userID>");
            INXML.Append("</login>");
        }
        catch (Exception ex)
        {

            log.Trace(ex.Message);

        }
    }

}
