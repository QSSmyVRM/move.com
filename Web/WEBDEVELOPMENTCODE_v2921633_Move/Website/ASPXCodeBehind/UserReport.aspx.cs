/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 //ZD 100886
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;

public partial class en_UserReport : System.Web.UI.Page
{
    #region  Variables Declaration

    private myVRMNet.NETFunctions obj = null;
    private ns_Logger.Logger log;
    String comConfigPath = "";
    String xmlstr = "";
    String outXML = "";


    XmlNodeList nodes = null;
    XmlNode node = null;
    XmlDocument XmlDoc = null;

    protected string[] userID = null;
    protected string[] login = null;
    protected string[] strfirstName = null;
    protected string[] strlastName = null;
    protected string[] email = null;
    protected string xmlj = "";
    Int32 length = 0;

    #endregion

    #region Protected Data members

    protected System.Web.UI.HtmlControls.HtmlInputHidden sortBy;
    protected System.Web.UI.HtmlControls.HtmlInputHidden alphabet;
    protected System.Web.UI.HtmlControls.HtmlInputHidden pageNo;
    protected System.Web.UI.HtmlControls.HtmlInputHidden totalPages;
    protected System.Web.UI.HtmlControls.HtmlInputHidden xmljs;
    protected System.Web.UI.WebControls.Label LblError;

    #endregion        

    #region Page Load  Event Hander

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            GetQueryStrings();
            BindData();
        }
        catch (Exception ex)
        {
            LblError.Visible = true;
            //LblError.Text = "PageLoad: " + ex.Message;ZD 100263
            LblError.Text = obj.ShowSystemMessage();
            log.Trace("PageLoad:" + ex.Message);//ZD 100263

        }

    }
    #endregion

    #region GetQueryStrings

    /// <summary>
    /// User Defined Method - Building GetQueryStrings Structure
    /// </summary>

    private void GetQueryStrings()
    {

        try
        {
            if (Request.QueryString["sb"] != null)
            {
                if (Request.QueryString["sb"].ToString().Trim() != "")
                    sortBy.Value = Request.QueryString["sb"].ToString().Trim();
                else
                    sortBy.Value = "2";

            }
            else
                sortBy.Value = "2";


            if (Request.QueryString["al"] != null)
            {
                if (Request.QueryString["al"].ToString().Trim() != "")
                {

                    alphabet.Value = Request.QueryString["al"].ToString().Trim();
                }
                else
                    alphabet.Value = "a";
            }
            else
                alphabet.Value = "a";

            if (Request.QueryString["pg"] != null)
            {
                if (Request.QueryString["pg"].ToString().Trim() != "")
                {

                    pageNo.Value = Request.QueryString["pg"].ToString().Trim();
                }
                else
                    pageNo.Value = "1";
            }
            else
                pageNo.Value = "1";
           
        }
        catch (Exception ex)
        {
            LblError.Visible = true;
           // LblError.Text = "QueryString: " + ex.Message;ZD 100263
            LblError.Text = obj.ShowSystemMessage();
            log.Trace("QueryString:" + ex.Message);//ZD 100263

        }
    }
    #endregion

    #region BindData
    /// <summary>
    /// Construct inXML. Get the outXML from Com (Command : GetTemplate) and bind it to the controls
    /// Application["COM_ConfigPath"] refers ComConfig.xml file path
    /// </summary>

    private void BindData()
    {
        XmlNode node = null;

        try
        {
            String inXML = BuildInXML();

            if (Request.Form["clientName"] != null)
            {
                if (Request.Form["clientName"].ToString() == "")
                    comConfigPath = Request.QueryString["cname"].ToString();
                else
                    comConfigPath = Request.Form["clientName"].ToString();

                if (comConfigPath != "")
                    Application["COM_ConfigPath"] = comConfigPath;
            }

            obj = new myVRMNet.NETFunctions();
            outXML = obj.CallCOM("UserReport", inXML, Application["COM_ConfigPath"].ToString());

            if (outXML != "")
            {
                XmlDoc = new XmlDocument();
                XmlDoc.LoadXml(outXML);

               UserReportIncDetails();
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            obj = null;
        }
    }

    #endregion

    #region BuildInXML
    /// <summary>
    /// To construct the inputXML for fetching the data
    /// </summary>
    /// <returns></returns>

    private String BuildInXML()
    {
        try
        {
            String inXML = "";
            inXML += "<login>";
            inXML += obj.OrgXMLElement();//Organization Module Fixes
            inXML += "<userID>" + Session["userID"].ToString() + "</userID>";
            inXML += "  <alphabet>" + alphabet.Value + "</alphabet>";
            inXML += "  <pageNo>" + pageNo.Value + "</pageNo>";
            inXML += "  <sortBy>" + sortBy.Value + "</sortBy>";
            inXML += "</login>";
            return inXML;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    #endregion

    #region UserReportIncDetails
    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>

    private void UserReportIncDetails()
    {
        String errorMessage = "";

        try
        {
            xmlstr = outXML;
            Session["outXML"] = outXML;
            XmlDoc = new XmlDocument();
            XmlDoc.LoadXml(xmlstr);

            if (XmlDoc == null)
            {
                if (Session["who_use"].ToString() == "VRM")
                {
                    //errorMessage = "Outcoming XML document is illegal" + "\r\n" + "\r\n";
                    //errorMessage = errorMessage + xmlstr;
                    log.Trace("Outcoming XML document is illegal" + "\r\n" + "\r\n" + xmlstr);
                    errorMessage  = obj.ShowSystemMessage();//FB 1881
                    Response.Write(errorMessage);
                }
                else
                {
                    //Response.Write("<br><br><p align='center'><font size=4><b>Outcoming XML document is illegal<b></font></p>");
                    //Response.Write("<br><br><p align='left'><font size=2><b>" + xmlstr + "<b></font></p>");
                    log.Trace("Outcoming XML document is illegal" + "\r\n" + "\r\n" + xmlstr);
                    errorMessage  = obj.ShowSystemMessage();//FB 1881
                    Response.Write(errorMessage);
                }
                XmlDoc = null;
                Response.End();
            }

            if (XmlDoc.DocumentElement.SelectNodes("sortBy").Count > 0)
            {
                sortBy.Value = XmlDoc.DocumentElement.SelectSingleNode("sortBy").InnerText;
            }
            if (XmlDoc.DocumentElement.SelectNodes("alphabet").Count > 0)
            {
                alphabet.Value = XmlDoc.DocumentElement.SelectSingleNode("alphabet").InnerText;
            }
            if (XmlDoc.DocumentElement.SelectNodes("pageNo").Count > 0)
            {
                pageNo.Value = XmlDoc.DocumentElement.SelectSingleNode("pageNo").InnerText;
            }
            if (XmlDoc.DocumentElement.SelectNodes("totalPages").Count > 0)
            {
                totalPages.Value = XmlDoc.DocumentElement.SelectSingleNode("totalPages").InnerText;
            }

            nodes = XmlDoc.DocumentElement.SelectNodes("user");
            length = nodes.Count;
            userID = new string[length - 1 + 1];
            login = new string[length - 1 + 1];
            strfirstName = new string[length - 1 + 1];
            strlastName = new string[length - 1 + 1];
            email = new string[length - 1 + 1];

            Int32 j = 0;
                           
            foreach (XmlNode nod in nodes)
            {
            
                userID[j] = nod.SelectSingleNode("userID").InnerText;
               
                login[j] = nod.SelectSingleNode("login").InnerText;
               
                strfirstName[j] = nod.SelectSingleNode("firstName").InnerText;
                strlastName[j] = nod.SelectSingleNode("lastName").InnerText;
                email[j] = nod.SelectSingleNode("email").InnerText;
                xmlj = xmlj + userID[j] + "%" + strfirstName[j] + "%" + strlastName[j] + "%" + email[j] + "%" + login[j] + ";";
                j = j + 1;
           }
                xmljs.Value = xmlj;
                nodes = null;
                XmlDoc = null;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    #endregion

}
