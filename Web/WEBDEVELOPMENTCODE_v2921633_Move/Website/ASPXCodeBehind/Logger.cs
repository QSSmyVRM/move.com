/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886
using System;
using System.Web;
using System.IO;
using System.Collections;
using System.Xml;

/// <summary>
/// Summary description for Tier1
/// </summary>
/// 
namespace ns_Logger
{
    public class Logger
    {
        String fPath;
        Boolean isDebug;
        String configPath;
        public Logger()
        {
            try
            {
                isDebug = false;
                //HttpContext.Current.Response.Write(HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                if (HttpContext.Current.Application["MyVRMServer_ConfigPath"] != null)
                {
                    fPath = HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString() + "ASPXCodeBehind.log";
                    //HttpContext.Current.Response.Write(fPath);
                    if (HttpContext.Current.Application["COM_ConfigPath"] != null)
                        configPath = HttpContext.Current.Application["COM_ConfigPath"].ToString();
                    else
                        configPath = "";

                    //HttpContext.Current.Response.Write(configPath);
                    if (configPath.Trim().Equals(""))
                        isDebug = false;
                    else
                    {
                        XmlDocument xmldoc = new XmlDocument();
                        xmldoc.Load(configPath);
                        //HttpContext.Current.Response.Write(xmldoc.InnerXml);
                        if (xmldoc.SelectSingleNode("//COMConfig/TraceLogging").InnerText.ToUpper().Equals("YES"))
                            isDebug = true;
                        else
                            isDebug = false;
                    }
                    //HttpContext.Current.Response.Write(isDebug);
                }
            }
            catch (Exception ex)
            {
                isDebug = false;
                Trace(ex.Message + " : " + ex.StackTrace);
                //HttpContext.Current.Response.Write(ex.Message + " : " + ex.StackTrace);
            }
        }
        public void Trace(String logRecord)
        {

            Console.WriteLine(logRecord);
            StreamWriter sw;
            String logFilePath =  fPath;
            try
            {
                if (isDebug == true)
                {
                    if (!File.Exists(logFilePath))
                    {
                        // file doesnot exist . hence, create a new log file.
                        sw = File.CreateText(logFilePath);
                        sw.Flush();
                        sw.Close();
                    }
                    else
                    {
                        // check if exisiting log file size is greater than 50 MB
                        FileInfo fi = new FileInfo(logFilePath);
                        if (fi.Length > 100000000)
                        {
                            // delete the log file						
                            File.Delete(logFilePath);

                            // create a new log file 
                            sw = File.CreateText(logFilePath);
                            sw.Flush();
                            sw.Close();
                        }
                    }

                    // write the log record.
                    sw = File.AppendText(logFilePath);
                    logRecord = "\r" + DateTime.Now + ": " + logRecord;
                    sw.WriteLine(logRecord);
                    sw.Flush();
                    sw.Close();
                }
            }
            catch (Exception)
            {
                // do nothing
            }
        }
    }
}