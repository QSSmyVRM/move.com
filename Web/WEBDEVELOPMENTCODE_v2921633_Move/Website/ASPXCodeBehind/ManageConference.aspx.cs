/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886
using System.IO;
using System;
using System.Xml;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using ExpertPdf.HtmlToPdf;
using System.Collections.Generic;
using System.Text;
using System.Collections;

/// <summary>
/// Summary description for ConferenceOrders
/// </summary>
/// 
namespace ns_MyVRM  //RSS
{
    public partial class ManageConference : System.Web.UI.Page
    {
        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.Label lblStatus;
        protected System.Web.UI.WebControls.Label lblConfName;
        protected System.Web.UI.WebControls.TextBox lblConfID;
        protected System.Web.UI.WebControls.Label lblConfUniqueID;
        protected System.Web.UI.WebControls.Label lblConfTime;
        protected System.Web.UI.WebControls.Label lblConfDate;
        protected System.Web.UI.WebControls.Label lblConfType;
        protected System.Web.UI.WebControls.Label lblConfDuration;
        protected System.Web.UI.WebControls.Label lblDescription;
        //FB 2359 Start //FB 2377
        //protected System.Web.UI.HtmlControls.HtmlInputHidden hdnConceirgeSupp;
        //FB 2359 End
        protected System.Web.UI.WebControls.Label lblPublic;
        protected System.Web.UI.WebControls.Label lblRegistration;
        protected System.Web.UI.WebControls.Label lblFiles;
        protected System.Web.UI.WebControls.Label lblConfHost;
        protected System.Web.UI.WebControls.Label hdnConfHost;

        // Code added for the Bug # 74- mpujari
        protected System.Web.UI.WebControls.Label lblLastModifiedBy;
        protected System.Web.UI.WebControls.Label hdnLastModifiedBy;
        protected System.Web.UI.WebControls.Label lblLocCount;
        protected System.Web.UI.WebControls.Label lblLocation;
        protected System.Web.UI.WebControls.Label lblPartyCount;
        protected System.Web.UI.WebControls.Label lblNWAccess;
        protected System.Web.UI.WebControls.Label lblRestrictUsage;
        protected System.Web.UI.WebControls.Label lblMaxAudioPorts;
        protected System.Web.UI.WebControls.Label lblMaxVideoPorts;
        protected System.Web.UI.WebControls.Label lblVideoCodecs;
        protected System.Web.UI.WebControls.Label lblAudioCodecs;
        protected System.Web.UI.WebControls.Label lblConfOnPort;
        protected System.Web.UI.WebControls.Label lblDualStreamMode;
        protected System.Web.UI.WebControls.Label lblEncryption;
        protected System.Web.UI.WebControls.Label lblMaxLineRate;
        protected System.Web.UI.WebControls.Label lblAVWO;
        protected System.Web.UI.WebControls.Label lblCATWO;
        protected System.Web.UI.WebControls.Label lblHKWO;
        protected System.Web.UI.WebControls.Label lblPending;
        protected System.Web.UI.WebControls.Label lblLectureMode;
        protected System.Web.UI.WebControls.Label lblPassword;
        protected System.Web.UI.WebControls.Label lblAlert;
        protected System.Web.UI.WebControls.Label lblNoAlerts;
        protected System.Web.UI.WebControls.Label hdnConfDuration;
        protected System.Web.UI.WebControls.Label lblTimezone;
        protected System.Web.UI.WebControls.Label lblSingleDialin;
        protected System.Web.UI.WebControls.Label lblFECCMode;

        protected System.Web.UI.WebControls.Table lblAVNoWorkOrder;
        protected System.Web.UI.WebControls.Table lblCATNoWorkOrder;
        protected System.Web.UI.WebControls.Table lblHKNoWorkOrder;
        protected System.Web.UI.WebControls.Table tblAVWO;
        protected System.Web.UI.WebControls.Table tblCATWO;
        protected System.Web.UI.WebControls.Table tblHKWO;
        protected System.Web.UI.WebControls.Table tblNoEndpoints;
        protected System.Web.UI.WebControls.Table tblEndpoints;
        protected System.Web.UI.WebControls.Table tblActions;
        protected System.Web.UI.WebControls.Table tblAV;
        protected System.Web.UI.WebControls.Table tblForceTerminate;
        protected System.Web.UI.WebControls.Table tblTerminalControl;
        protected System.Web.UI.WebControls.Table tblP2PEndpoints;
        protected System.Web.UI.WebControls.Table tblAlerts;

        protected System.Web.UI.WebControls.Image imgVideoDisplay;
        
        //ZD 101869 Starts
        protected System.Web.UI.WebControls.Image imgVideoLayout;
        protected System.Web.UI.WebControls.Image imgLayoutMapping6EP;
        protected System.Web.UI.WebControls.Image imgLayoutMapping7EP;
        protected System.Web.UI.WebControls.Image imgLayoutMapping8EP;
        protected System.Web.UI.WebControls.Label lblCodianLOEP;
        //protected System.Web.UI.WebControls.Image imgLayoutMapping6;
        //protected System.Web.UI.WebControls.Image imgLayoutMapping7;
        //protected System.Web.UI.WebControls.Image imgLayoutMapping8;
        protected System.Web.UI.WebControls.Label lblCodianLO;
        //ZD 101869 End
        
        protected System.Web.UI.WebControls.DataGrid dgAVWO;
        protected System.Web.UI.WebControls.DataGrid dgCATWO;
        protected System.Web.UI.WebControls.DataGrid dgHKWO;
        protected System.Web.UI.WebControls.DataGrid partyGrid;
        protected System.Web.UI.WebControls.DataGrid dgEndpoints;
        protected System.Web.UI.WebControls.DataGrid dgP2PEndpoints;
        protected System.Web.UI.WebControls.DataGrid dgAlerts;
        protected System.Web.UI.WebControls.DataGrid dgBridgeResources;

        protected System.Web.UI.WebControls.Table tblNoParty;
        protected System.Web.UI.WebControls.Label lblHeader;
        protected System.Web.UI.HtmlControls.HtmlGenericControl Field15;//Added FB 1428
        protected System.Web.UI.HtmlControls.HtmlTableRow trPrt; //Added for FB 1425 QA Bug

        protected System.Web.UI.WebControls.TableRow trAcceptReject;
        protected System.Web.UI.WebControls.TableRow trClone;
        protected System.Web.UI.WebControls.TableRow trCancel;
        protected System.Web.UI.WebControls.TableRow trEdit;
        protected System.Web.UI.WebControls.TableRow trMCU;

        protected System.Web.UI.WebControls.TextBox cmd;
        protected System.Web.UI.WebControls.TextBox Recur;
        protected System.Web.UI.WebControls.TextBox txtTimeDifference;
        protected System.Web.UI.WebControls.TextBox tempText;
        protected System.Web.UI.WebControls.TextBox txtSelectedImage;
        protected System.Web.UI.WebControls.TextBox ImagesPath;
        protected System.Web.UI.WebControls.TextBox ImageFiles;
        protected System.Web.UI.WebControls.TextBox ImageFilesBT;
        protected System.Web.UI.WebControls.TextBox txtExtendedTime;
        protected System.Web.UI.WebControls.TextBox txtEndpointType;
        protected System.Web.UI.WebControls.TextBox txtTempImage;
        protected System.Web.UI.WebControls.TextBox txtSelectedImageEP;

        protected System.Web.UI.WebControls.LinkButton btnClone;
        protected System.Web.UI.WebControls.LinkButton btnEdit;
        protected System.Web.UI.WebControls.LinkButton btnDeleteConf;

        protected System.Web.UI.WebControls.Menu Menu22; // FB 2050
        protected System.Web.UI.WebControls.MultiView MultiView1;

        protected System.Web.UI.WebControls.DropDownList lstProtocol;
        protected System.Web.UI.WebControls.DropDownList lstBridges;
        protected System.Web.UI.WebControls.DropDownList lstBridgeCountV;
        protected System.Web.UI.WebControls.DropDownList lstBridgeCountA;
        protected System.Web.UI.WebControls.DropDownList lstAddressType;

        protected System.Web.UI.WebControls.HiddenField hdnConfType, hdnIsHDBusy; //ALLDEV-807
        protected System.Web.UI.WebControls.HiddenField hdnConfStatus;
        protected System.Web.UI.WebControls.HiddenField hdnConfLockStatus; //FB 2501 2012-12-07
        //For PSU Fix FB 1354
        protected System.Web.UI.WebControls.Button btnAddNewEndpoint;
        //Code Added For  FB 1400 - Start
        protected System.Web.UI.WebControls.Label LblExtendedTimeMsg;
        //Code Added For  FB 1400 - End
        protected System.Web.UI.WebControls.Label LblPExtTimeMsg; //FB 1562
        protected System.Web.UI.WebControls.TextBox txtPExtTime;    //FB 1562

        protected enum ConferenceType { Room = 7, AudioOnly = 6, AudioVideo = 2, P2P = 4 };
        protected enum ConferenceStatus { Scheduled = 0, Pending = 1, Terminated = 3, Ongoing = 5, OnMCU = 6, Completed = 7, Deleted = 9, WaitList = 2 }; //ZD 102532

        /* *** code added for buffer zone *** -- Start */
        protected System.Web.UI.WebControls.Label lblSetupDur;
        protected System.Web.UI.WebControls.Label lblTearDownDur;
        //ZD 100085 Starts
        protected System.Web.UI.WebControls.Label lblSetupTimeinMin;
        protected System.Web.UI.WebControls.Label lblTearDownTimeinMin;
        protected System.Web.UI.WebControls.Label lblMCUPreStart;
        protected System.Web.UI.WebControls.Label lblMCUPreEnd;
        //ZD 100085 End
        protected System.Web.UI.HtmlControls.HtmlTableRow bufferTableCell;
        /* *** code added for buffer zone *** -- End */

        //Added for MOJ Phase 2 QA --Start 
        protected System.Web.UI.HtmlControls.HtmlTableRow trPuPw;
        protected System.Web.UI.HtmlControls.HtmlTableRow trFle;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdType;
        //Added for MOJ Phase 2 QA -- End

        protected System.Web.UI.WebControls.Table tblCustOpt; //custom attribute fixes
        protected System.Web.UI.HtmlControls.HtmlTableCell tdCustOpt;
        myVRMNet.CustomAttributes CAObj = null;
        protected System.Web.UI.WebControls.Table lblNoCustOption;
        protected System.Web.UI.WebControls.Label lbl_CustOpt;
        protected System.Web.UI.HtmlControls.HtmlTableRow trEnableCustom; //custom attribute fixes

        //ZD 101098 START
        protected System.Web.UI.WebControls.DataGrid partyattendancelistGrid;
        protected System.Web.UI.HtmlControls.HtmlTable tblAttendancelist;
        //ZD 101098 END

        MyVRMNet.Util utilObj; //FB 2236
        XmlNodeList pnodes;
        myVRMNet.NETFunctions obj;
        ns_Logger.Logger log;
        ns_InXML.InXML objInXML;
        Boolean isParticipant;
        //Code Added For FB 1371  - Start
        String strEndPointID = "";
        String strType = "";
        //Code Added For FB 1371 - End
        string CascadeLink = ""; //FB 2528

        protected String isCustomEdit = ""; //Code added for FB 1391
        protected int isAcceptDecline = 0;//FB 2438
        protected String p2pStatus = "";//Code added for p2p Status
        protected System.Web.UI.WebControls.CheckBox P2pAutoRef;//Code added for p2p Status
        protected System.Web.UI.WebControls.TableCell refreshCell;//Code added for p2p Status
        protected System.Web.UI.HtmlControls.HtmlInputText TxtMessageBoxAll;//Code added for p2p Status
        protected System.Web.UI.WebControls.CheckBox Refreshchk;//Code added for p2p Status
        Boolean initload = false;//Code added for p2p Status
        myVRMNet.RSSXMLGenerator RSSObj = null; //Ticker
        CustomizationUtil.CSSReplacementUtility cssUtil; //Organization/CSS Module 

        protected System.Web.UI.HtmlControls.HtmlTableRow trAVCommonSettings;//Code added for Disney
        protected System.Web.UI.HtmlControls.HtmlTableRow trp2pLinerate;//Code added for Disney
        protected System.Web.UI.WebControls.Label LblLineRate;//Code added for Disney
        /* *** Code added for Audio-addon *** */
        protected String enableConferenceCode = "0";
        protected String enableLeaderPin = "0";
        protected String enableAdvAvParams = "0";
        protected String enableAudioParams = "0";

        /* *** Code added for Audio-addon *** */ 
        // WO Bug Fix
        protected String tformat = "hh:mm tt";

        //FB 2446 - Start
        protected String enableconferencepassword = "0";
        protected String enablepublicconference = "0";
        //FB 2446 - End
        public String timezne = "";//Fb 1728
        protected String language = "";//FB 1830
        bool isViewUser = false;//FB 1522
        protected System.Web.UI.WebControls.Label lblReminders;//FB 1926
        protected System.Web.UI.HtmlControls.HtmlInputHidden timezone;//FB 1948
        protected System.Web.UI.WebControls.PlaceHolder HostDetailHolder;//FB 1958
        protected System.Web.UI.WebControls.LinkButton LnkAVExpand; //FB 1985
        protected System.Web.UI.WebControls.View NormalView;//FB 1985
        //FB 2274 Starts
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossEnableBufferZone;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossfoodModule;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnCrosshkModule;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossroomModule;
        //FB 2446 - Start
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossEnableConfPassword;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossEnablePublicConf;
        //FB 2446 - End
        public string foodModule;
        public string hkModule;
        public string roomModule;
        public int enableBufferZone = 0; //ZD 100085
        //FB 2274 Ends

        protected System.Web.UI.HtmlControls.HtmlTableRow trEnableConcierge;
        //FB 2446 - Start
        protected System.Web.UI.HtmlControls.HtmlTableCell tdpw;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdpw1;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdpwd;//FB 2565
        protected System.Web.UI.HtmlControls.HtmlTableCell tdpu;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdpu1;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdpub;//FB 2565
        //FB 2446 - End
        // FB 2457 Exchange Round trip -starts
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnconfOriginID;
        public string conferenceOrigin = "";  
        // FB 2457 Exchange Round trip -ends

        //FB 2501 Starts
        protected System.Web.UI.WebControls.Label lblConfVNOC;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdStartMode;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdStartMode1;//FB 2565
        protected System.Web.UI.HtmlControls.HtmlTableCell tdStartModeSelection;
		protected System.Web.UI.WebControls.Label lblStartMode;
        public bool VMRConf;
        //int isVMR = 0; //FB 2819 //ZD 100602
        //FB 2501 Ends
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnEnableEM7;//FB 2598
        public string enableEM7;//FB 2598
		//FB 2632 - Starts
        protected System.Web.UI.HtmlControls.HtmlGenericControl lblmeet;
        protected System.Web.UI.HtmlControls.HtmlGenericControl lblVNOC;
        protected System.Web.UI.HtmlControls.HtmlGenericControl lblOnsiteAV;
        protected System.Web.UI.HtmlControls.HtmlGenericControl lblConciergeMonitoring;
        protected System.Web.UI.HtmlControls.HtmlTableRow trDedicatedVNOC;
		//FB 2632 - End
        protected System.Web.UI.WebControls.Label lblSecured;//FB 2595
        protected System.Web.UI.HtmlControls.HtmlTableCell tdSecured;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdSecured1;//FB 2565
        protected System.Web.UI.HtmlControls.HtmlTableCell tdSecuredSelection;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnNetworkSwitching;//FB 2598 //FB 2993
        int Secured = 0;
        //FB 2441 Starts
        protected System.Web.UI.WebControls.DataGrid dgMuteALL; 
        protected System.Web.UI.WebControls.LinkButton lnkMuteAllExcept; 
        protected System.Web.UI.WebControls.LinkButton lnkUnMuteAllParties; 
        protected System.Web.UI.HtmlControls.HtmlInputButton ConfLayoutSubmit;
        protected System.Web.UI.WebControls.Button btnExtendEndtime;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnMCUSynchronous;
        //FB 2441 Ends
        //FB 2670 START
        protected System.Web.UI.HtmlControls.HtmlTableRow trOnSiteAVSupport;
        protected System.Web.UI.HtmlControls.HtmlTableRow trMeetandGreet;
        protected System.Web.UI.HtmlControls.HtmlTableRow trConciergeMonitoring;
        protected System.Web.UI.WebControls.Table tblConcierge;
        protected System.Web.UI.HtmlControls.HtmlTableRow trDedicateVNOC;
        //FB 2670 END
		//FB 2694 Start
        protected System.Web.UI.HtmlControls.HtmlTableCell tdRemainder;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdRemainder1;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdRemainderselection;
        //FB 2694 End
        public bool isPCConf; //FB 2819

        protected System.Web.UI.HtmlControls.HtmlTableRow trBuffer; //ZD 100085
        protected System.Web.UI.HtmlControls.HtmlTableRow trMUCPreTime; //ZD 100085
        protected System.Web.UI.WebControls.DropDownList lstVideoCodecs;
        protected System.Web.UI.WebControls.DropDownList lstAudioCodecs;
        protected System.Web.UI.WebControls.DropDownList lstLineRate;

        //ZD 100221 Starts
        protected System.Web.UI.WebControls.Label WebEXMeetingStatus;
        protected System.Web.UI.WebControls.Label WebEXstartdate;
        protected System.Web.UI.WebControls.Label WebEXStartingTime;
        protected System.Web.UI.WebControls.Label WebEXDuration;
        protected System.Web.UI.WebControls.Label WebEXHost;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnWebExHostURL;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnWebExPartyURL;
        protected System.Web.UI.WebControls.LinkButton btnwebexconf;


        //ZD 100221 Ends
		//ZD 100602 Starts
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnTimeZoneId;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnCloudConf;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnServiceType;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnIsVMR;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnConfStart;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnConfEnd;
		//ZD 100602 End

        protected System.Web.UI.HtmlControls.HtmlTableCell tdmessage; //ZD 101133
        protected System.Web.UI.HtmlControls.HtmlTableCell tdsendmessage; //ZD 101133
        protected System.Web.UI.WebControls.Button SendMsgAll; //ZD 101133
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnPermanentConf; //ZD 100522
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnExpressConf; //ZD 101233
        //ZD 101233 Starts
        //ZD 101233 End
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnEditForm; //ZD 101597     

        //ZD 101755 start
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnSetuptimeDisplay;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnTeardowntimeDisplay;
        protected int EnableSetupTimeDisplay = 0, EnableTeardownTimeDisplay = 0;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdSetup;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdsetupcol;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdlblSetup;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdteardowncol;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdlblteardowncol;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdlblteardown;
        //ZD 101755 End
        //ZD 101931 start
        protected System.Web.UI.HtmlControls.HtmlTableCell tdVideoDisplay;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdimgVideoDisplay;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdDisplay;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdEVideoLayout;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdVideoLayout;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdEndpointLayout;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnCodian; //ZD 101971
        //ZD 101931 End
        protected System.Web.UI.HtmlControls.HtmlTableCell tdExndTmlbl; //ZD 102997
        protected System.Web.UI.HtmlControls.HtmlTableCell tdExndTm;//ZD 102997
        protected System.Web.UI.WebControls.DataGrid dgRoomHost;//ZD 103216
        //ALLDEV-826 Starts
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossEnableHostGuestPwd;
        protected String enableHostGuestPwd = "0";
        //ALLDEV-826 Ends
        protected System.Web.UI.WebControls.Label lblRequester;    //ALLDEV-857    
        protected System.Web.UI.WebControls.PlaceHolder SchedulerDetailHolder;//ALLDEV_857

        public ManageConference()
        {
            //
            // TODO: Add constructor logic here
            //
            isParticipant = false;
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
            objInXML = new ns_InXML.InXML();
            utilObj = new MyVRMNet.Util(); //FB 2236
        }

        //FB 2616 Start
        int encrypted = 0;
        MyVRMNet.LoginManagement loginMgmt = null;
        string userdetails = "";
        StringBuilder a = new StringBuilder();//FB 2694
        string Transtxt = ""; //ZD 100288
        //FB 2616 End
		string VMConfType = "0";//ZD 100522 
        int videoLayout = 1;//ZD 101931

        //ZD 101022
        #region InitializeCulture
        protected override void InitializeCulture()
        {
            if (Session["UserCulture"] != null)
            {
                UICulture = Session["UserCulture"].ToString();
                Culture = Session["UserCulture"].ToString();
                //ZD 101714
                if (Session["UserCulture"].ToString() == "fr-CA")
                    Culture = "en-US";
                base.InitializeCulture();
            }
        }
        #endregion

        // FB 2050 Start
        #region Page PreInit Method
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Request.ServerVariables["http_user_agent"].IndexOf("Safari", StringComparison.CurrentCultureIgnoreCase) != -1)
                Page.ClientTarget = "uplevel";
            //if (Request.UserAgent.IndexOf("AppleWebKit") > 0)
            //Request.Browser.Adapters.Clear();
        }
        #endregion
        // FB 2050 Ends

        private void Page_Load(object sender, System.EventArgs e)
        {
            string confChkArg = string.Empty;
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();

                obj.URLConformityCheck(Request.Url.AbsoluteUri.ToLower());

                if (Request.QueryString["t"] != null)
                    if(Request.QueryString["t"].ToString().Equals("hf"))
                        confChkArg = "?t=hf";

                obj.AccessConformityCheck("manageconference.aspx" + confChkArg);
                Session["VMRBridge"] = "0"; //ZD 100522
                // ZD 100170
                //string path = Request.Url.AbsoluteUri.ToLower();
                //if (path.IndexOf("%3c") > -1 || path.IndexOf("%3e") > -1)
                //    Response.Redirect("ShowError.aspx");
                //FB 118 start

                //if (hdnConfStatus.Value != ns_MyVRMNet.vrmConfStatus.Ongoing) //FB 1552
                //{
                //    btnAddNewEndpoint.Visible = false;
                //    dgEndpoints.ItemCreated += new DataGridItemEventHandler(dgEndpoints_ItemCreated);
                //}
                //else
                //{
                //    btnAddNewEndpoint.Visible = true;
                //    if (hdnConfLockStatus.Value == "1") //FB 2501 2012-12-07
                //    {
                //        btnAddNewEndpoint.Visible = false;
                //        dgEndpoints.ItemCreated += new DataGridItemEventHandler(dgEndpoints_ItemCreated);
                //    }
                //}

                //FB 118 End

                //WO Bug Fix
                if(Session["timeFormat"] == null)   //FB 1821 
                    Session["timeFormat"] = "1";

               // tformat = (Session["timeFormat"].ToString().Equals("0") ? "HH:mm" : "hh:mm tt");
               //FB 2588 Starts
                if (Session["timeFormat"].ToString().Equals("0"))
                    tformat = "HH:mm";
                else if (Session["timeFormat"].ToString().Equals("1"))
                    tformat = "hh:mm tt";
                else if (Session["timeFormat"].ToString().Equals("2"))
                    tformat = "HHmmZ";
                //FB 2588 Ends
				//FB 1830 - Translation - Start
                if (Session["isMultiLingual"] != null)
                {
                    if (Session["isMultiLingual"].ToString() != "1") //FB 2570
                    {

                        //Organization/CSS Module - Create folder for UI Settings --- Strat
                        if (Request.QueryString["hf"] == null)
                        {
                            String fieldText = "";
                            cssUtil = new CustomizationUtil.CSSReplacementUtility();

                            fieldText = cssUtil.GetUITextForControl("ManageConference.aspx", "Field15");
                            Field15.InnerText = obj.GetTranslatedText(fieldText); // FB 2272
                        }
                        //Organization/CSS Module - Create folder for UI Settings --- End
                    }
                }
				//FB 1830 - Translation - End

                //Code Added For  FB 1400 - Start
                LblExtendedTimeMsg.Text = "";
                LblPExtTimeMsg.Text = "";   //FB 1562
                //Code Added For  FB 1400 - End
                errLabel.Visible = false; 
                /* *** Code added for FB 1425 QA Bug -Start *** */

                //code added for rss feed
                if (Session["roomModule"] == null)
                    Session["roomModule"] = "0";

                if (Session["hkModule"] == null) //organization module
                    Session["hkModule"] = "0";

                if (Session["foodModule"] == null)
                    Session["foodModule"] = "0";

                if (Application["Client"] == null)
                    Application["Client"] = "";

                if (Session["systemTimezone"] == null)
                    Session.Add("systemTimezone", "0");

                if (Session["admin"] == null)
                    Session.Add("admin", "0");

                if (Session["systemDate"] == null)
                {
                    String curDate = DateTime.Now.ToShortDateString();
                    Session.Add("systemDate", curDate);
                }
                if (Session["systemTime"] == null)
                {
                    String curDateTime = DateTime.Now.ToShortTimeString();
                    Session.Add("systemTime", curDateTime);
                }
                //RSS Fix -- Start
                if (Request.QueryString["OrgID"] != null)
                {
                    if (Request.QueryString["OrgID"].ToString() != "")
                    {
                        Session.Add("organizationID", Request.QueryString["OrgID"].ToString());
                    }
                }
                //RSS Fix -- End

                //FB 1830 - Starts
                if (Session["language"] == null)
                    Session["language"] = "en";

                if (Session["language"].ToString() != "")
                    language = Session["language"].ToString();
                //FB 1830 - End

                if (Request.QueryString["hf"] != null)
                    if (Request.QueryString["hf"].ToString().Equals("1"))
                    {
                        
                        Session.Add("userID", "11");
                        lblTimezone.Text = (String)Application["systemTimeZone"];

                        if (RSSObj == null)
                            RSSObj = new myVRMNet.RSSXMLGenerator();

                        lblHeader.Text = obj.GetTranslatedText("View Conference");//FB 1830 - Translation
                        Field15.Visible = false;

                        Application.Add("COM_ConfigPath", "C:\\VRMSchemas_v1.8.3\\ComConfig.xml");
                        Application.Add("MyVRMServer_ConfigPath", "C:\\VRMSchemas_v1.8.3\\");
                        Application.Add("RTC_ConfigPath", "C:\\VRMSchemas_v1.8.3\\VRMRTCConfig.xml");

                        //obj.GetSystemDateTime(Application["COM_ConfigPath"].ToString());
                        obj.GetSystemDateTime(Application["MyVRMServer_ConfigPath"].ToString()); //FB 2027

                        String myvrmser = Application["MyVRMServer_ConfigPath"].ToString();

                        String webURL = RSSObj.GetSysSettingDetails(myvrmser);
                        Session.Add("ImageURL", webURL);

                        String inXML = "<login><userID>" + Session["userID"].ToString() + "</userID><user><userID>" + Session["userID"].ToString() + "</userID></user></login>";
                        //FB 2027 Start
                        String outXML = obj.CallMyVRMServer("GetOldUser", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                        //String outXML = obj.CallCOM("GetOldUser", inXML, Application["COM_ConfigPath"].ToString());
                        //FB 2027 End
                        if (outXML.IndexOf("<error>") < 0)
                        {
                            XmlDocument xmldoc = new XmlDocument();
                            xmldoc.LoadXml(outXML);
                            String dateformat = xmldoc.SelectSingleNode("//oldUser/dateFormat").InnerText;
                            String timeFormat = xmldoc.SelectSingleNode("//oldUser/timeFormat").InnerText;
                            String timeZoneDisplay = "";

                            if (xmldoc.SelectSingleNode("//oldUser/timezoneDisplay").InnerText != null)
                                timeZoneDisplay = xmldoc.SelectSingleNode("//oldUser/timezoneDisplay").InnerText;

                            Session.Add("timeFormat", timeFormat);
                            Session.Add("timezoneDisplay", timeZoneDisplay);
                            Session.Add("FormatDateType", dateformat);
                        }

                        trEdit.Visible = false;
                        trCancel.Visible = false;
                        trClone.Visible = false;
                        trAcceptReject.Visible = false;
                        trMCU.Visible = false;
                    }
                //rss feed end

                if (Session["isExpressUser"] != null)//FB 1779
                {
                    if (Session["isExpressUser"].Equals("1"))
                    {
                        trEdit.Visible = false;
                        trCancel.Visible = false;//ZDLatest
                        trClone.Visible = false;
                        trAcceptReject.Visible = false;
                        trMCU.Visible = false;//ZDLatest
                    }
                }

                if (Application["Client"].ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.MOJ))
                {
                    trPrt.Visible = false;
                    Menu22.Items[0].Text = "&nbsp;&nbsp;&nbsp;&nbsp;Locations&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                    Menu22.Items[0].Value = "0";
                    Menu22.ItemWrap = true;
                    trPuPw.Visible = false;
                    trFle.Visible = false;
                }

                //RSS Feed - start
                if (Request.QueryString["confid"] != null)
                {
                    if (Request.QueryString["confid"] != "")
                    {
                        String confID = Request.QueryString["confid"].ToString();
                        Session.Add("ConfID", confID);
                    }
                }
                //RSS Feed - end

                if (Session["EnableBufferZone"] == null)//Organization Module Fixes
                {
                    Session["EnableBufferZone"] = "0";//Organization Module Fixes
                }

                //ZD 100085
                //if (enableBufferZone == 0)//Organization Module Fixes //FB 2274
                //    bufferTableCell.Visible = false;

                /* *** Code added for Audio-addon *** */
                if (Session["ConferenceCode"] != null)
                    enableConferenceCode = Session["ConferenceCode"].ToString();
                if (Session["LeaderPin"] != null)
                    enableLeaderPin = Session["LeaderPin"].ToString();
                if (Session["AdvAvParams"] != null)
                    enableAdvAvParams = Session["AdvAvParams"].ToString();
                if (Session["AudioParams"] != null)
                    enableAudioParams = Session["AudioParams"].ToString();


                if (enableAdvAvParams == "0")
                	tblAV.Attributes.Add("style", "display:none");
                //else //Commented For FB 1982
                //    tblAV.Attributes.Add("style", "display:block");


                isViewUser = obj.ViewUserRole(); //FB 1522
                /* *** Code added for Audio-addon *** */

                /* *** Code added for FB 1425 QA Bug -End *** */
                if (IsPostBack)
                {
                    //Response.Write("Params: " + Request.Params.Get("__EVENTTARGET").ToString());
                    if (cmd.Text.Equals("1"))
                        btnSetupAtMCU_Click();
                    //else if (cmd.Text.Equals("2"))
                    //    btnDeleteConference_Click();
                    else if (cmd.Text.Equals("5") || cmd.Text.Equals("6"))
                    {
                        ChangeVideoDisplay();
                        int.TryParse(txtSelectedImage.Text, out videoLayout);//ZD 101931
                        DisplayvideoLayout(videoLayout);//ZD 101931
                    }
                    cmd.Text = "";

                    //code added for Ticker -- Start
                    myVRMNet.RSSXMLGenerator RSSObj = null;

                    if (RSSObj == null)
                        RSSObj = new myVRMNet.RSSXMLGenerator();

                    String tickermarq = "";
                    if (Session["tickerStatus"] != null)
                    {
                        if (Session["tickerStatus"].ToString().Trim() == "0")
                        {
                            if (Session["tickerDisplay"].ToString().Trim() == "0")
                            {
                                tickermarq = RSSObj.TickerGenerator(Session["userID"].ToString(), Session["tickerDisplay"].ToString(), Session["FormatDateType"].ToString(), Session["timeFormat"].ToString());
                            }
                            else
                            {
                                string url = Session["rssFeedLink"].ToString();
                                tickermarq = RSSObj.FeedTickerGenerator(url, Session["userID"].ToString(), "0");
                            }
                        }
                    }


                    


                    //feedTicker
                    Session.Add("ticker", tickermarq);

                    if (Session["tickerStatus1"] != null)
                    {
                        if (Session["tickerStatus1"].ToString().Trim() == "0")
                        {
                            if (Session["tickerDisplay1"].ToString().Trim() == "0")
                            {
                                tickermarq = RSSObj.TickerGenerator(Session["userID"].ToString(), Session["tickerDisplay1"].ToString(), Session["FormatDateType"].ToString(), Session["timeFormat"].ToString());
                            }
                            else
                            {
                                string url = Session["rssFeedLink1"].ToString();
                                tickermarq = RSSObj.FeedTickerGenerator(url, Session["userID"].ToString(), "1");
                            }
                        }
                    }

                    //feedTicker
                    Session.Add("ticker1", tickermarq);
                    //code added for Ticker -- End
                    ShowHostDetails(); //FB 1958
                    ShowSchedulerDetails(); //ALLDEV-857
                }
                else
                {
                    if (Session["admin"] != null)
                        if (Session["admin"].ToString().Equals("2"))
                            trMCU.Visible = true;
                        else
                            trMCU.Visible = false;

                    lblConfDate.Text = "";

                    //FB 2616 Start
                    if (Request.QueryString["tp"] == "M")
                    {
                        encrypted = 1;
                        loginMgmt = new MyVRMNet.LoginManagement();

                        if (Request.QueryString["req"] != null)
                          userdetails = Request.QueryString["req"];

                        loginMgmt.simpleDecrypt(ref userdetails);
                        string[] id = userdetails.Split(',');

                        loginMgmt.queyStraVal = id[0].ToString();
                        loginMgmt.qStrPVal = id[1].ToString();

                        String url = loginMgmt.GetHomeCommand();
                        if (Request.QueryString["confid"] != null)
                        {
                            if (Request.QueryString["confid"] != "")
                            {
                                String confID = Request.QueryString["confid"].ToString();
                                Session.Add("ConfID", confID);
                            }
                        }
                    }
                    //FB 2616 End

                    //ZD 101022
                    Transtxt = obj.GetTranslatedText("Endpoints");
                    String menuText = "<div align='center' valign='middle' style='width:123;' onclick='javascript:DataLoading(1);'>" + Transtxt + "</div>";
                    Menu22.Items[1].Text = menuText;

                    Transtxt = obj.GetTranslatedText("Resource br Availability").Replace("br", "<br />");
                    menuText = "<div align='center' valign='middle' style='width:123;' onclick='javascript:DataLoading(1);'>" + Transtxt + "</div>";
                    Menu22.Items[2].Text = menuText;

                    Transtxt = obj.GetTranslatedText("Point-To-Point");
                    menuText = "<div align='center' valign='middle' style='width:123;' onclick='javascript:DataLoading(1);'>" + Transtxt + "</div>";
                    Menu22.Items[3].Text = menuText;
                    Transtxt = "";

                    DisplayConferenceDetails();
                    //ALLDEV-842 Starts
                    if (Session["EM7URI"] != null && Session["EM7URI"].ToString() != "")
                    {
                        RefreshEM7Endpoints();  
                    }
                    //ALLDEV-842 Ends
                    //FB 2274 Starts
                    crossSilo();
                    if (hdnCrossroomModule != null && hdnCrossroomModule.Value != "")
                        roomModule = hdnCrossroomModule.Value;
                    else if (Session["roomModule"] != null)
                        roomModule = Session["roomModule"].ToString();

                    if (hdnCrosshkModule != null && hdnCrosshkModule.Value != "")
                        hkModule = hdnCrosshkModule.Value;
                    else if (Session["hkModule"] != null)
                        hkModule = Session["hkModule"].ToString();

                    if (hdnCrossfoodModule != null && hdnCrossfoodModule.Value != "")
                        foodModule = hdnCrossfoodModule.Value;
                    else if (Session["foodModule"] != null)
                        foodModule = Session["foodModule"].ToString();

                    //FB 2274 Ends
                    //FB 2446 - Start                    
                    if (hdnCrossEnableConfPassword != null && hdnCrossEnableConfPassword.Value != "")
                        enableconferencepassword = hdnCrossEnableConfPassword.Value;
                    else if (Session["EnableConfPassword"] != null)
                        enableconferencepassword = Session["EnableConfPassword"].ToString();

                    //ALLDEV-826 Starts
                    if (hdnCrossEnableHostGuestPwd != null && hdnCrossEnableHostGuestPwd.Value != "")
                        enableHostGuestPwd = hdnCrossEnableHostGuestPwd.Value;
                    else if (Session["EnableHostGuestPwd"] != null)
                        enableHostGuestPwd = Session["EnableHostGuestPwd"].ToString();
                    //ALLDEV-826 Ends

                    if (enableconferencepassword != null)
                        if (enableconferencepassword == "0")
                        {
                            //ALLDEV-826 Starts
                            if (enableHostGuestPwd != null)
                                if (enableHostGuestPwd == "0")
                                {//ALLDEV-826 Ends
                                    tdpw.Visible = false;
                                    tdpw1.Visible = false;
                                    tdpwd.Visible = false;//FB 2565
                                }//ALLDEV-826
                        }
                        else
                        {
                            tdpw.Visible = true;
                            tdpw1.Visible = true;
                            tdpwd.Visible = true;//FB 2565
                        }  
                    //ALLDEV-826 Starts
                    if (enableHostGuestPwd != null)
                        if (enableHostGuestPwd == "0")
                        {
                            if (enableconferencepassword != null)
                                if (enableconferencepassword == "0")
                                {
                                    tdpw.Visible = false;
                                    tdpw1.Visible = false;
                                    tdpwd.Visible = false;
                                }
                        }
                        else
                        {
                            tdpw.Visible = true;
                            tdpw1.Visible = true;
                            tdpwd.Visible = true;
                        }
                    //ALLDEV-826 Ends                                                   
                    if (hdnCrossEnablePublicConf != null && hdnCrossEnablePublicConf.Value != "")
                        enablepublicconference = hdnCrossEnablePublicConf.Value;
                    else if (Session["EnablePublicConf"] != null)
                        enablepublicconference = Session["EnablePublicConf"].ToString();
                    if (enablepublicconference != null)
                        if (enablepublicconference == "0")
                        {
                            tdpu.Visible = false;
                            tdpu1.Visible = false;
                            tdpub.Visible = false;//FB 2565
                        }
                        else
                        {
                            tdpu.Visible = true;
                            tdpu1.Visible = true;
                            tdpub.Visible = true;//FB 2565    
                        }
                    //FB 2446 - End
                    //FB 2595 Starts
                    if (hdnNetworkSwitching != null && hdnNetworkSwitching.Value != "")
                        int.TryParse(hdnNetworkSwitching.Value, out Secured);
                    else if (Session["NetworkSwitching"] != null)
                        int.TryParse(Session["NetworkSwitching"].ToString(), out Secured);

                    if (Secured != 2 || hdnConfType.Value == "8") //FB 2694
                    {
                        tdSecured.Visible = false;
                        tdSecured1.Visible = false;//FB 2565
                        tdSecuredSelection.Visible = false;
                    }
                    else
                    {
                        tdSecured.Visible = true;
                        tdSecured1.Visible = true;//FB 2565
                        tdSecuredSelection.Visible = true;
                    }
                    //FB 2595 Ends

                    //ZD 100085 //ZD 101755 start
                    if (enableBufferZone == 1)
                    {
                         trBuffer.Attributes.Add("Style", "Display:");
                         if (EnableSetupTimeDisplay == 1)
                         {
                             tdSetup.Attributes.Add("Style", "Display:");
                             tdsetupcol.Attributes.Add("Style", "Display:");
                             tdlblSetup.Attributes.Add("Style", "Display:");
                         }
                         else
                         {
                             tdSetup.Attributes.Add("Style", "Display:None");
                             tdsetupcol.Attributes.Add("Style", "Display:None");
                             tdlblSetup.Attributes.Add("Style", "Display:None");
                         }
                         if (EnableTeardownTimeDisplay == 1)
                         { 
                             tdteardowncol.Attributes.Add("Style", "Display:");
                             tdlblteardowncol.Attributes.Add("Style", "Display:");
                             tdlblteardown.Attributes.Add("Style", "Display:");
                         }
                         else
                         {
                             tdteardowncol.Attributes.Add("Style", "Display:None");
                             tdlblteardowncol.Attributes.Add("Style", "Display:None");
                             tdlblteardown.Attributes.Add("Style", "Display:None");
                         }
                        
                    }
                    else
                        trBuffer.Attributes.Add("Style", "Display:None");
                    //ZD 101755 End

                    //ZD 101931 start
                    if (Session["ShowVideoLayout"] != null && (Session["ShowVideoLayout"].ToString() == "0" || Session["ShowVideoLayout"].ToString() == "1"))
                    {
                        tdVideoDisplay.Attributes.Add("style", "Display:;");
                        tdDisplay.Attributes.Add("style", "Display:;");
                        tdimgVideoDisplay.Attributes.Add("style", "Display:;");
                        tdEVideoLayout.Attributes.Add("style", "Display:;");
                        tdVideoLayout.Attributes.Add("style", "Display:;");
                        tdEndpointLayout.Attributes.Add("style", "Display:;");
                    }
                    else
                    {
                        tdVideoDisplay.Attributes.Add("style", "Display:None;");
                        tdDisplay.Attributes.Add("style", "Display:None;");
                        tdimgVideoDisplay.Attributes.Add("style", "Display:None;");
                        tdEVideoLayout.Attributes.Add("style", "Display:None;");
                        tdVideoLayout.Attributes.Add("style", "Display:None;");
                        tdEndpointLayout.Attributes.Add("style", "Display:None;");
                    }
                    //ZD 101931 End

                    if (roomModule.Equals("1")) //FB 2274
                    {
                        LoadWorkOrders(dgAVWO, "1", lblAVWO, lblAVNoWorkOrder);
                    }
                    else
                    {
                        tblAVWO.Visible = false;
                    }
                    if (hkModule.Equals("1")) //organization module FB 2274
                    {
                        LoadWorkOrders(dgHKWO, "3", lblHKWO, lblHKNoWorkOrder);
                    }
                    else
                    {
                        tblHKWO.Visible = false;
                    }
                    if (foodModule.Equals("1")) //FB 2274
                    {
                        LoadWorkOrders(dgCATWO, "2", lblCATWO, lblCATNoWorkOrder);
                    }
                    else
                        tblCATWO.Visible = false;
                    if (Request.QueryString["confirm"] != null)
                    {
                        if (Request.QueryString["confirm"].ToString().Equals("1"))
                            SetConfirmationMessage();
                    }
                    if (Request.QueryString["m"] != null)
                        if (Request.QueryString["m"].ToString().Equals("1"))
                        {
                            errLabel.Text = obj.GetTranslatedText("Operation Successful!");//FB 1830 - Translation
                            errLabel.Visible = true;
                        }
                    //Response.Write(Menu22.Items.Count);
                    //Response.Write(Menu22.Items.Count);
                    // Code Modified -  Changed Equals(3) to 4 - FB 1422
                    //if (Menu22.Items.Count.Equals(3))
                    if (Menu22.Items.Count.Equals(3)) //Code added for Disney
                    {
                        MultiView1.ActiveViewIndex = 2;
                        CheckResourceAvailability();
                    }
                    else
                    {
                        MultiView1.ActiveViewIndex = 0;
                    }

                    if (hdnConfType.Value.Equals(ns_MyVRMNet.vrmConfType.P2P))//COde added for P2P
                    {
                        MultiView1.ActiveViewIndex = 2;
                        LoadEndpoints();

                        trp2pLinerate.Attributes.Add("style", "display:block");//Code modififed for disney
                        trAVCommonSettings.Attributes.Add("style", "display:none");

                    }
                    //FB 1985 - Starts
                    if (Application["Client"].ToString().ToUpper() == "DISNEY")
                    {
                        if (Menu22.Items.Count > 1)
                        {
                            if (Menu22.Items[1] != null)
                            {
                                Menu22.Items[1].Text = String.Empty;
                                Menu22.Items[1].Enabled = false;
                                dgEndpoints.Enabled = false;
                            }
                            CheckResourceAvailability();

                            if (NormalView != null)
                                MultiView1.SetActiveView(NormalView);

                            if (Menu22.Items[2] != null)
                            {
                                Menu22.Items[2].Selected = true;
                            }
                        }
                    }
                    //FB 1985 - End
                }
                //FB 2694 - Start
                if (hdnConfType.Value == "8") //FB 2769
                {
                    Transtxt = obj.GetTranslatedText("Locations"); //ZD 100288
                    a.Append("<div align='center' valign='middle' style='width:123;' onclick='javascript:DataLoading(1);'>" + Transtxt + "</div>"); //ZD 100429
                }
                else
                {
                    Transtxt = obj.GetTranslatedText("Locations"); //ZD 100288
                    a.Append("<div align='center' valign='middle' style='width:123;' onclick='javascript:DataLoading(1);'>" + Transtxt + " &"); //ZD 100429
                    Transtxt = obj.GetTranslatedText("Participants"); //ZD 100288
                    a.Append("<br>" + Transtxt + "</div>");
                }
                Menu22.Items[0].Text = a.ToString();
                //FB 2694 - End

                trEnableCustom.Visible = true;
                FillCustomAttributeTable();
                
                //FB 2288 Starts
                ButtonColumn butRem = partyGrid.Columns[06] as ButtonColumn; 
                if (butRem != null)
                {
                    butRem.Visible = false;
                    if (hdnConfStatus.Value == ns_MyVRMNet.vrmConfStatus.Pending || hdnConfStatus.Value == ns_MyVRMNet.vrmConfStatus.Scheduled)
                        butRem.Visible = true;
                }
                //FB 2288 Ends

                //ALLDEV-841 start
                if (Session["SendConfirmationEmail"].ToString() == "3")
                {
                    if (butRem != null)
                        butRem.Visible = false;
                    if (dgAVWO != null) 
                      dgAVWO.Columns[11].Visible = false;
                    if(dgCATWO != null) 
                      dgCATWO.Columns[9].Visible = false;
                    if (dgHKWO != null) 
                      dgHKWO.Columns[10].Visible = false;
                }
                //ALLDEV-841 End
                
                //FB 2598 Starts EnableEM7
                if (hdnEnableEM7 != null && hdnEnableEM7.Value != "")
                    enableEM7 = hdnEnableEM7.Value;
                else if (Session["EnableEM7"] != null)
                    enableEM7 = Session["EnableEM7"].ToString();
                if (enableEM7 != null)
                {
                    if (enableEM7 == "0")
                    {
                        dgEndpoints.Columns[15].Visible = false;
                        dgP2PEndpoints.Columns[14].Visible = false; 
                    }
                    else
                    {
                        dgEndpoints.Columns[15].Visible = true;
                        dgP2PEndpoints.Columns[14].Visible = true; 
                    }
                }
                //FB 2598 Ends
                string EnableOnsiteAV = Session["EnableOnsiteAV"].ToString();
                string EnableMeetandGreet = Session["EnableMeetandGreet"].ToString();
                string EnableConciergeMonitoring = Session["EnableConciergeMonitoring"].ToString();
                string EnableDedicatedVNOC = Session["EnableDedicatedVNOC"].ToString();
                string EnableVNOCselection = Session["EnableVNOCselection"].ToString();
                if (EnableOnsiteAV == "0" && EnableMeetandGreet == "0" && EnableConciergeMonitoring == "0" && EnableDedicatedVNOC == "1" && EnableVNOCselection == "0" || EnableOnsiteAV == "0" && EnableMeetandGreet == "0" && EnableConciergeMonitoring == "0" && EnableDedicatedVNOC == "0" && EnableVNOCselection == "1" || EnableOnsiteAV == "0" && EnableMeetandGreet == "0" && EnableConciergeMonitoring == "0" && EnableDedicatedVNOC == "0" && EnableVNOCselection == "0" || hdnConfType.Value.Equals(ns_MyVRMNet.vrmConfType.HotDesking))//FB 3007
                    trEnableConcierge.Visible = false;
                else
                    trEnableConcierge.Visible = true;
                //tblConcierge.Attributes.Add("Style", "Display:Block;");

                //FB 118 Start
				//ZD 100602 Starts
                if (hdnConfStatus.Value != ns_MyVRMNet.vrmConfStatus.Ongoing || hdnConfLockStatus.Value == "1" || hdnIsVMR.Value != "0" || hdnPermanentConf.Value == "1") //FB 1552  //ZD 100602 //ZD 100522
                {
                    btnAddNewEndpoint.Visible = false;
                    dgEndpoints.ItemCreated += new DataGridItemEventHandler(dgEndpoints_ItemCreated);
                }
                else
                    btnAddNewEndpoint.Visible = true;
				//ZD 100602 End             
                if (Request.QueryString["ep"] != null)
                {
                    if (Request.QueryString["ep"].ToString().Equals("1"))
                    {
						//ZD 101978
                        if (Application["Client"].ToString().ToUpper() != "DISNEY")
                        {
                            MultiView1.ActiveViewIndex = 1;
                            Menu22.Items[1].Enabled = true;
                            Menu22.Items[1].Selectable = true;
                            Menu22.Items[1].Selected = true;
                            dgEndpoints.Enabled = true;
                        }
                        if (hdnConfStatus.Value == ns_MyVRMNet.vrmConfStatus.Ongoing)
                        {
                            tblTerminalControl.Visible = true;
                            btnAddNewEndpoint.Visible = true;
                        }
                        else
                        {
                            tblTerminalControl.Visible = false;
                            btnAddNewEndpoint.Visible = false;
                        }
                        GetVideoLayouts();
                        DisplayvideoLayout(videoLayout);//ZD 101931
                        LoadEndpoints();                        
                    }   
                }
                //FB 118 End

                //ZD 101233 starts
                if (ViewState["hasEdit"].ToString() == "0")
                    trEdit.Visible = false;
                if (ViewState["hasClone"].ToString() == "0")
                    trClone.Visible = false;
                if (ViewState["hasDelete"].ToString() == "0")
                    trCancel.Visible = false;
                if (ViewState["hasExtendTime"].ToString() == "0")
                {
                    btnExtendEndtime.Visible = false;
                    txtExtendedTime.Visible = false;
                }
                //ZD 101233 End
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace("page_load" + ex.Message);//ZD 100263
            }

        }
        //FB 2274
        private void crossSilo()
        {
            try
            {
                string xml = "";
                int multiOrgId = 11;
                XmlDocument xmldocu = null;
                string multisiloOrgID = Session["organizationID"].ToString();
                if (Session["multisiloOrganizationID"] != null)
                    multisiloOrgID = Session["multisiloOrganizationID"].ToString();
                Int32.TryParse(multisiloOrgID, out multiOrgId);
                if (multiOrgId >= 12)
                {
                    xml = obj.CallMyVRMServer("GetAllOrgSettings", "<GetAllOrgSettings><organizationID>" + multisiloOrgID + "</organizationID></GetAllOrgSettings>", Application["MyVRMServer_ConfigPath"].ToString());
                    if (xml.IndexOf("<error>") <= 0)
                    {
                        xmldocu = new XmlDocument();
                        xmldocu.LoadXml(xml);

                        hdnCrossroomModule.Value = xmldocu.SelectSingleNode("//GetAllOrgSettings/EnableFacilites").InnerText;
                        hdnCrossfoodModule.Value = xmldocu.SelectSingleNode("//GetAllOrgSettings/EnableCatering").InnerText;
                        hdnCrosshkModule.Value = xmldocu.SelectSingleNode("//GetAllOrgSettings/EnableHouseKeeping").InnerText;
                        hdnCrossEnableBufferZone.Value = xmldocu.SelectSingleNode("//GetAllOrgSettings/EnableBufferZone").InnerText;
                        //FB 2446 - Start
                        hdnCrossEnableConfPassword.Value = xmldocu.SelectSingleNode("//GetAllOrgSettings/EnableConferencePassword").InnerText;
                        hdnCrossEnablePublicConf.Value = xmldocu.SelectSingleNode("//GetAllOrgSettings/EnablePublicConference").InnerText;
                        //FB 2446 - End
                        hdnCrossEnableHostGuestPwd.Value = xmldocu.SelectSingleNode("//GetAllOrgSettings/EnableHostGuestPwd").InnerText;   //ALLDEV-826
                        hdnEnableEM7.Value = xmldocu.SelectSingleNode("//GetAllOrgSettings/EnableEM7").InnerText;//FB 2598
                        hdnNetworkSwitching.Value = xmldocu.SelectSingleNode("//GetAllOrgSettings/NetworkSwitching").InnerText;//FB 2595

                        hdnSetuptimeDisplay.Value = xmldocu.SelectSingleNode("//GetAllOrgSettings/EnableSetupTimeDisplay").InnerText; //ZD 101755
                        hdnTeardowntimeDisplay.Value = xmldocu.SelectSingleNode("//GetAllOrgSettings/EnableTeardownTimeDisplay").InnerText; //ZD 101755
                    }
                }

                if (hdnCrossroomModule != null && hdnCrossroomModule.Value != "")
                    roomModule = hdnCrossroomModule.Value;
                else if (Session["roomModule"] != null)
                    roomModule = Session["roomModule"].ToString();

                if (hdnCrosshkModule != null && hdnCrosshkModule.Value != "")
                    hkModule = hdnCrosshkModule.Value;
                else if (Session["hkModule"] != null)
                    hkModule = Session["hkModule"].ToString();

                if (hdnCrossfoodModule != null && hdnCrossfoodModule.Value != "")
                    foodModule = hdnCrossfoodModule.Value;
                else if (Session["foodModule"] != null)
                    foodModule = Session["foodModule"].ToString();

                //ZD 100085
                if (hdnCrossEnableBufferZone != null && hdnCrossEnableBufferZone.Value != "")
                    int.TryParse(hdnCrossEnableBufferZone.Value, out enableBufferZone);
                else if (Session["EnableBufferZone"] != null)
                    int.TryParse(Session["EnableBufferZone"].ToString(), out enableBufferZone);
                //FB 2446 - Start
                if (hdnCrossEnableConfPassword != null && hdnCrossEnableConfPassword.Value != "")
                    enableconferencepassword = hdnCrossEnableConfPassword.Value;
                else if (Session["EnableConfPassword"] != null)
                    enableconferencepassword = Session["EnableConfPassword"].ToString();
                if (hdnCrossEnablePublicConf != null && hdnCrossEnablePublicConf.Value != "")
                    enablepublicconference = hdnCrossEnablePublicConf.Value;
                else if (Session["EnablePublicConf"] != null)
                    enablepublicconference = Session["EnablePublicConf"].ToString();
                //FB 2446 - End

                //ALLDEV-826 Starts
                if (hdnCrossEnableHostGuestPwd != null && hdnCrossEnableHostGuestPwd.Value != "")
                    enableHostGuestPwd = hdnCrossEnableHostGuestPwd.Value;
                else if (Session["EnableHostGuestPwd"] != null)
                    enableHostGuestPwd = Session["EnableHostGuestPwd"].ToString();
                //ALLDEV-826 Ends

                //FB 2598 Start
                if (hdnEnableEM7 != null && hdnEnableEM7.Value != "")
                    enableEM7 = hdnEnableEM7.Value;
                else if (Session["EnableEM7"] != null)
                    enableEM7 = Session["EnableEM7"].ToString();
                //FB 2598 End

                if (hdnNetworkSwitching != null && hdnNetworkSwitching.Value != "") //FB 2595
                    int.TryParse(hdnNetworkSwitching.Value, out Secured);
                else if (Session["NetworkSwitching"] != null)
                    int.TryParse(Session["NetworkSwitching"].ToString(), out Secured);
                //ZD 101755 start
                if (hdnSetuptimeDisplay != null && hdnSetuptimeDisplay.Value != "")
                    int.TryParse(hdnSetuptimeDisplay.Value, out EnableSetupTimeDisplay);
                else if (Session["EnableSetupTimeDisplay"] != null)
                    int.TryParse(Session["EnableSetupTimeDisplay"].ToString(), out EnableSetupTimeDisplay);

                if (hdnTeardowntimeDisplay != null && hdnTeardowntimeDisplay.Value != "")
                    int.TryParse(hdnTeardowntimeDisplay.Value, out EnableTeardownTimeDisplay);
                else if (Session["EnableTeardownTimeDisplay"] != null)
                    int.TryParse(Session["EnableTeardownTimeDisplay"].ToString(), out EnableTeardownTimeDisplay);
                //ZD 101755 End

            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace("crosssilo" + ex.Message);//ZD 100263
            }

        }
        private bool SetConfirmationMessage()
        {
            //ZD 101971 Starts
            string LayoutAlertText = "";
            if (Request.QueryString["LOA"] != null) 
            {
                if (Request.QueryString["LOA"].ToString().Equals("1"))
                    LayoutAlertText = "Note: The selected video display layout is not supported by the MCU in conference.";
            }
            //ZD 101971 Ends
            //ZD 102909 - Start
            //ZD 103265  start

            //string EntityCodeText = "";
            //if (Session["WrongEntityCode"] != null)
            //{
            //    if (Session["WrongEntityCode"].ToString() == "1")
            //        EntityCodeText = "<br/>" + obj.GetTranslatedText("Mentioned entity code could not be linked with conference as it does not available.");
            //}

            //ZD 103265  End
            //Added for FB 1428 START
            if (Application["Client"].ToString().ToUpper() == "MOJ")//Added for MOJ
                errLabel.Text = "Your Hearing has been scheduled successfully.";
            else
                //Added for FB 1428 End
                errLabel.Text = obj.GetTranslatedText("Your Conference has been scheduled successfully.") + "<br/>" + obj.GetTranslatedText(LayoutAlertText); // +EntityCodeText;//FB 1830 - Translation //ZD 101971 ZD 103265
            switch (hdnConfStatus.Value)
            {
                case ns_MyVRMNet.vrmConfStatus.Scheduled:
                    //Added for FB 1428 START
                    if (Application["Client"].ToString().ToUpper() == "MOJ")//Added for MOJ
                        errLabel.Text = "Your Hearing has been scheduled successfully. Email notifications have been sent to participants.";
                    else
                    { //ALLDEV-841 start
                        //Added for FB 1428 End
                        if (Session["SendConfirmationEmail"].ToString() == "3") 
                            errLabel.Text = obj.GetTranslatedText("Your conference has been scheduled successfully.");
                        else
                            errLabel.Text = obj.GetTranslatedText("Your conference has been scheduled successfully. Email notifications have been sent to participants.") + "<br/>" + obj.GetTranslatedText(LayoutAlertText); // + EntityCodeText;//FB 1830 - Translation //ZD 101971 ZD 103265 
                    } //ALLDEV-841 End
                    break;
                case ns_MyVRMNet.vrmConfStatus.Pending:
                    //Added for FB 1428 START
                    if (Application["Client"].ToString().ToUpper() == "MOJ")//Added for MOJ
                        errLabel.Text = "Your Hearing has been submitted successfully and is currently in pending status awaiting administrative approval.";
                    else
                        //Added for FB 1428 End
                        errLabel.Text = obj.GetTranslatedText("Your conference has been submitted successfully and is currently in pending status awaiting administrative approval.") + "<br/>" + obj.GetTranslatedText(LayoutAlertText);  //+ EntityCodeText;//FB 1830 - Translation //ZD 101971 ZD 103265 
                    break;
                case ns_MyVRMNet.vrmConfStatus.WaitList://ZD 102532
                    errLabel.Text = obj.GetTranslatedText("Your conference has been submitted successfully and is currently in wait list status.") + "<br/>" + obj.GetTranslatedText(LayoutAlertText); //+ EntityCodeText; ZD 103265
                    break;
            }

            if (hdnIsHDBusy != null && hdnIsHDBusy.Value == "1") //ALLDEV-807
                errLabel.Text = obj.GetTranslatedText("Your Conference has been scheduled successfully.");

            errLabel.Visible = true;
           // Session.Remove("WrongEntityCode"); //ZD 103265
            return true;
            //ZD 102909 - End
        }

        private void DisplayConferenceDetails()
        {
            try
            {
                XmlNodeList ConfBridges = null; //ZD 100522
                //Response.Write("in details");
                string inXML = "<login><userID>" + Session["userID"].ToString() + "</userID>" + obj.OrgXMLElement() + "<selectType>1</selectType><selectID>" + Session["ConfID"].ToString() + "</selectID><Encrypted>" + encrypted + "</Encrypted></login>";//Organization Module Fixes //FB 2616 // ALLBUGS-57
                //Response.Write(obj.Transfer(inXML));
                string outXML;
                string Schedular = "";//ZD 100221
                if (Request.QueryString["t"] != null) //Rss Change
                {
                    if (Request.QueryString["t"].Equals("hf"))
                    {
                        //Added for FB 1428 START
                        if (Application["Client"].ToString().ToUpper() == "MOJ")//Added for MOJ
                        {
                            lblHeader.Text = "Hearing Details";
                            Field15.Visible = false;
                            tblActions.Visible = false;
                        }
                        else
                        {
                            //Added for FB 1428 END
                            lblHeader.Text = obj.GetTranslatedText("Conference Details");//FB 1830 - Translation
                            Field15.Visible = false; //Added for FB 1428
                            tblActions.Visible = false;
                        }//Added for FB 1428
                    }
                }
                log.Trace("ManageConference GetOldConference InXML- " + inXML);
                outXML = obj.CallMyVRMServer("GetOldConference", inXML, Application["MyVRMServer_ConfigPath"].ToString()); //FB 2027


                log.Trace("ManageConference GetOldConference OutXML- " + outXML);
                Session.Add("outXML", outXML);
                //Response.Write(obj.Transfer(outXML));
                XmlDocument xmldoc = new XmlDocument();
                outXML = outXML.Replace("& ", "&amp; ");
                xmldoc.LoadXml(outXML);
                //ZD 100263_T Start
                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Visible = true;
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    Response.Redirect("ShowError.aspx");
                }
                //ZD 100263_T End
                // buffer zone Start
        		//ZD 100085 Starts
                //string setupDur = "0";  
                //string tearDownDur = "0";
                int setupDuration = 0;
                int tearDuration = 0;
        		//ZD 100085 End
                String tformat = "hh:mm tt";
                //tformat = (Session["timeFormat"].ToString().Equals("0") ? "HH:mm" : "hh:mm tt");
                //FB 2588 Starts
                if (Session["timeFormat"].ToString().Equals("0"))
                    tformat = "HH:mm";
                else if (Session["timeFormat"].ToString().Equals("1"))
                    tformat = "hh:mm tt";
                else if (Session["timeFormat"].ToString().Equals("2"))
                    tformat = "HHmmZ";
                //FB 2588 Ends

                // buffer zone End
                //FB 2274 Starts
                string multiOrgID = Session["organizationID"].ToString();
                if (xmldoc.SelectSingleNode("/conference/confInfo/ConfOrgID") != null) 
                {
                    if (multiOrgID != xmldoc.SelectSingleNode("/conference/confInfo/ConfOrgID").InnerText.Trim())
                    {
                        if (Session["multisiloOrganizationID"] == null)
                            Session.Add("multisiloOrganizationID", xmldoc.SelectSingleNode("/conference/confInfo/ConfOrgID").InnerText.Trim());
                        else
                            Session["multisiloOrganizationID"] = xmldoc.SelectSingleNode("/conference/confInfo/ConfOrgID").InnerText.Trim();
                    }
                    else
                        Session["multisiloOrganizationID"] = null;
                }
                //FB 2274 Ends
                //FB 2616 Start
                if (xmldoc.SelectSingleNode("/conference/confInfo/confID") != null)
                {
                    Session["ConfID"] = xmldoc.SelectSingleNode("/conference/confInfo/confID").InnerText.Trim();
                }

                //FB 2616 End

                //ZD 100085 Starts
                int MCUSetupTime = 0;
                if (xmldoc.SelectSingleNode("//conference/confInfo/McuSetupTime") != null && xmldoc.SelectSingleNode("//conference/confInfo/McuSetupTime").InnerText.Trim() != "")
                    int.TryParse(xmldoc.SelectSingleNode("//conference/confInfo/McuSetupTime").InnerText.Trim(), out MCUSetupTime);
                lblMCUPreStart.Text = MCUSetupTime.ToString();

                int MCUTearDownTime = 0;
                if (xmldoc.SelectSingleNode("//conference/confInfo/MCUTeardonwnTime") != null && xmldoc.SelectSingleNode("//conference/confInfo/MCUTeardonwnTime").InnerText.Trim() != "")
                    int.TryParse(xmldoc.SelectSingleNode("//conference/confInfo/MCUTeardonwnTime").InnerText.Trim(), out MCUTearDownTime);
                lblMCUPreEnd.Text = MCUTearDownTime.ToString();
                //ZD 100085 End

                string recurring = "0";
                if (xmldoc.SelectSingleNode("/conference/confInfo/recurring") != null)
                    recurring = obj.GetProperValue(xmldoc.SelectSingleNode("/conference/confInfo/recurring").InnerText);
                
                if (recurring.Equals("1"))
                {

                    XmlNode usrnode = xmldoc.SelectSingleNode("conference/userInfo");

                    if (usrnode != null)
                        usrnode.InnerXml += "<userId>" + Session["userID"].ToString() + "</userId>";

                    string recOutxml = obj.CallMyVRMServer("GetIfDirtyorPast", xmldoc.InnerXml, Application["MyVRMServer_ConfigPath"].ToString());

                    if (recOutxml != "")
                    {
                        if (recOutxml.IndexOf("<error>") < 0)
                        {
                            xmldoc.LoadXml(recOutxml);
                        }
                    }
                }

                // ZD 100263
                if (Session["roomCascadingControl"] != null && Session["roomCascadingControl"].ToString().Equals("1"))
                    if (xmldoc.SelectSingleNode("//conference/confInfo/timeZone") == null)
                    {
                        log.Trace("Page: ManageConference.aspx.cs, Function: DisplayConferenceDetails, Reason: Node timeZone is null");
                        Response.Redirect("ShowError.aspx");
                    }
                // ZD 100263
 
                timezne = obj.aGetTimeZoneName(1, xmldoc.SelectSingleNode("//conference/confInfo/timeZone").InnerText, "<TimeZone>" + xmldoc.SelectSingleNode("/conference/confInfo/timezones").InnerXml + "</TimeZone>");//Code added for Fb 1728
                timezone.Value = timezne; //FB 1948
                /* *** Recurring Fixes for Editing dirty conference 1391 - start **** */

                if (xmldoc.SelectSingleNode("//conference/confInfo/appointmentTime/customInstance") != null)
                {
                    if (xmldoc.SelectSingleNode("//conference/confInfo/appointmentTime/customInstance").InnerText == "1")
                        isCustomEdit = "Y";
                }
                /* *** Recurring Fixes for Editing dirty conference 1391 - end **** */

                //Recurrence Fixes - Edit With Some instances in past (FB 1131) - end
				//FB 2438 - Start
                if (xmldoc.SelectSingleNode("//conference/confInfo/locationList/selected/RoomInstance") != null)
                {
                    if (xmldoc.SelectSingleNode("//conference/confInfo/locationList/selected/RoomInstance").InnerText == "1")
                        isAcceptDecline = 1;
                }
				//FB 2438 - End

                /* *** code added for buffer zone *** -- Start */
                //ZD 100085 Starts
                if (xmldoc.SelectSingleNode("//conference/confInfo/bufferZone/SetupDur") != null)
                    int.TryParse(xmldoc.SelectSingleNode("//conference/confInfo/bufferZone/SetupDur").InnerText, out setupDuration);

                if (xmldoc.SelectSingleNode("//conference/confInfo/bufferZone/TearDownDur") != null)
                    int.TryParse(xmldoc.SelectSingleNode("//conference/confInfo/bufferZone/TearDownDur").InnerText, out tearDuration); ;

                lblSetupTimeinMin.Text = setupDuration.ToString();
                lblTearDownTimeinMin.Text = tearDuration.ToString();
        		//ZD 100085 End
                /* *** code added for buffer zone *** -- End */

                lblConfName.Text = obj.GetProperValue(xmldoc.SelectSingleNode("/conference/confInfo/confName").InnerText);
                lblPassword.Text = obj.GetProperValue(xmldoc.SelectSingleNode("/conference/confInfo/confPassword").InnerText);
                lblConfID.Text = obj.GetProperValue(xmldoc.SelectSingleNode("/conference/confInfo/confID").InnerText);
                lblConfUniqueID.Text = obj.GetProperValue(xmldoc.SelectSingleNode("/conference/confInfo/confUniqueID").InnerText);
                lblStatus.Text = obj.GetProperValue(xmldoc.SelectSingleNode("/conference/confInfo/Status").InnerText);
                lblConfType.Text = obj.GetProperValue(xmldoc.SelectSingleNode("/conference/confInfo/createBy").InnerText);
                hdnConfType.Value = obj.GetProperValue(xmldoc.SelectSingleNode("/conference/confInfo/createBy").InnerText);
                hdnIsHDBusy.Value = obj.GetProperValue(xmldoc.SelectSingleNode("/conference/confInfo/IsHDBusy").InnerText); //ALLDEV-807
                hdnConfStatus.Value = lblStatus.Text;
                //ZD 100513 Starts
                string OBTPconf = "0";
                if (xmldoc.SelectSingleNode("//conference/confInfo/isOBTP") != null)
                    OBTPconf = xmldoc.SelectSingleNode("//conference/confInfo/isOBTP").InnerText;
                if (lblConfType.Text == "2" && OBTPconf == "1")
                    lblConfType.Text = "9";
                //ZD 100513 Ends
                if (xmldoc.SelectSingleNode("/conference/confInfo/advAVParam/lockStatus") != null) //FB 2501 2012-12-07
                    hdnConfLockStatus.Value = xmldoc.SelectSingleNode("/conference/confInfo/advAVParam/lockStatus").InnerText.ToString();
                else
                    hdnConfLockStatus.Value = "0";

				//ZD 101233 Start
                int isPublicConf = 0;
                if (xmldoc.SelectSingleNode("/conference/confInfo/publicConf").InnerText.Equals("0"))
                {
                    isPublicConf = 0;
                    //hdnIsPublicConf.Value = "0";//ZD 101233
                    lblPublic.Text = obj.GetTranslatedText("No");//FB 1830 - Translation
                }
                else
                {
                    isPublicConf = 1;
                    //hdnIsPublicConf.Value = "1";//ZD 101233
                    lblPublic.Text = obj.GetTranslatedText("Yes");//FB 1830 - Translation
                    if (xmldoc.SelectSingleNode("/conference/confInfo/dynamicInvite").InnerText.Equals("1"))
                        lblRegistration.Text = obj.GetTranslatedText(" (Open for Registration)");//FB 1830 - Translation
                }

                //FB 2441 Starts
                if (xmldoc.SelectSingleNode("//conference/confInfo/IsSynchronous") != null)
                    hdnMCUSynchronous.Value = xmldoc.SelectSingleNode("//conference/confInfo/IsSynchronous").InnerText.ToString();
                //FB 2441 Ends

                //ZD 101233 Starts
                int confStatus = 0;
                int.TryParse(hdnConfStatus.Value, out confStatus);
                int hasView = 0;
                int hasEdit = 0;
                int hasDelete = 0;
                int hasManage = 0;
                int hasClone = 0;
                int hasMCUInfo = 0;
                int hasExtendTime = 0;
                int filterType = 0;

                int hasReservations = 0;
                int hasPublic = 0;

                if (Session["hasReservations"] != null && Session["hasReservations"].ToString() == "1")
                    hasReservations = 1;

                if (Session["hasPublic"] != null && Session["hasPublic"].ToString() == "1")
                    hasPublic = 1;

                //ZD 102783
                if (hasReservations == 1 && confStatus == 0)
                    filterType = 3;
                else if (hasPublic == 1 && confStatus == 0 && isPublicConf == 1)
                    filterType = 4;
                else if (confStatus == 1) //Pending
                    filterType = 5;
                else if (confStatus == 6) //On MCU
                    filterType = 8;
                else if (confStatus == 5) //Ongoing
                    filterType = 2;
                else if (confStatus == 2) //Wait List //ZD 102532
                    filterType = 11;
                else if (confStatus == 3 || confStatus == 9) //Terminated or Deleted
                    filterType = 9;
                else if (confStatus == 7) //Completed
                    filterType = 10;
                else
                    filterType = 3; //if (confStatus == 0 && isPublicConf == 0)//Reservation with Puclic or ConfSupport or without both

                obj.CheckConferenceRights(filterType, ref hasView, ref hasManage, ref hasExtendTime, ref hasMCUInfo, ref hasEdit, ref hasDelete, ref hasClone);

                ViewState["hasView"] = hasView.ToString();
                ViewState["hasManage"] = hasManage.ToString();
                //ZD 101884
                if (hdnMCUSynchronous.Value == "1")
                    hasEdit = 1;
                ViewState["hasEdit"] = hasEdit.ToString();
                ViewState["hasDelete"] = hasDelete.ToString();
                ViewState["hasClone"] = hasClone.ToString();
                ViewState["hasExtendTime"] = hasExtendTime.ToString();
                if (hasManage == 0)
                {
                    lblHeader.Text = obj.GetTranslatedText("Conference Details");
                    Field15.Visible = false;
                    tblActions.Visible = false;
                }
                //ZD 101233 End

                //FB 2501 Starts
                //ZD 100602 Starts
                VMRConf = false;
                hdnIsVMR.Value = "0";
                if (xmldoc.SelectSingleNode("//conference/confInfo/isVMR") != null)
                {
                    if (xmldoc.SelectSingleNode("//conference/confInfo/isVMR").InnerText.Equals("1"))
                    {
                        hdnIsVMR.Value = "1";
                        VMRConf = true;
                    }
                    VMConfType = xmldoc.SelectSingleNode("//conference/confInfo/isVMR").InnerText; //ZD 100522
                }

                hdnPermanentConf.Value = "0";
                if (xmldoc.SelectSingleNode("//conference/confInfo/IsPermanentConference") != null)
                {
                    if (xmldoc.SelectSingleNode("//conference/confInfo/IsPermanentConference").InnerText.Equals("1"))
                        hdnPermanentConf.Value = "1";
                }

                //ZD 101233 START
                hdnExpressConf.Value = "0";
                if (xmldoc.SelectSingleNode("//conference/confInfo/isExpressConference") != null)
                {
                    if (xmldoc.SelectSingleNode("//conference/confInfo/isExpressConference").InnerText.Equals("1"))
                        hdnExpressConf.Value = "1";
                }
                //ZD 101233 END
				
                hdnCloudConf.Value = "0";
                if (xmldoc.SelectSingleNode("//conference/confInfo/CloudConferencing") != null)
                {
                    if (xmldoc.SelectSingleNode("//conference/confInfo/CloudConferencing").InnerText.Equals("1"))
                        hdnCloudConf.Value = xmldoc.SelectSingleNode("//conference/confInfo/CloudConferencing").InnerText;
                }

                hdnServiceType.Value = "0";
                if (xmldoc.SelectSingleNode("//conference/confInfo/ServiceType") != null)
                {
                    if (xmldoc.SelectSingleNode("//conference/confInfo/ServiceType").InnerText.Equals("1"))
                        hdnServiceType.Value = xmldoc.SelectSingleNode("//conference/confInfo/ServiceType").InnerText;
                }

                //ZD 100602 Starts
                if (xmldoc.SelectSingleNode("//conference/confInfo/ConfBufferStartTime") != null)
                {
                    hdnConfStart.Value = xmldoc.SelectSingleNode("//conference/confInfo/ConfBufferStartTime").InnerText.Trim();
                }
                if (xmldoc.SelectSingleNode("//conference/confInfo/ConfBufferEndTime") != null)
                {
                    hdnConfEnd.Value = xmldoc.SelectSingleNode("//conference/confInfo/ConfBufferEndTime").InnerText.Trim();
                }
                //ZD 100602 Ends
				//ZD 101971 Starts
                int Bridetype = 0; 
                if (xmldoc.SelectSingleNode("//conference/confInfo/ConfSlaveBridgeType") != null)
                {
                    if (xmldoc.SelectSingleNode("//conference/confInfo/ConfSlaveBridgeType").InnerText != "")
                    {
                        int.TryParse(xmldoc.SelectSingleNode("//conference/confInfo/ConfSlaveBridgeType").InnerText, out Bridetype);
                    }
                }
                if (Bridetype == ns_MyVRMNet.MCUType.CodianMCU4200 || Bridetype == ns_MyVRMNet.MCUType.CodianMCU4500 || Bridetype == ns_MyVRMNet.MCUType.CodianMSE8000Series)
                    hdnCodian.Value = "1";
                else
                    hdnCodian.Value = "0";
				//ZD 101971 End                

                //ZD 100221 Starts
                hdnConfHost.Text = xmldoc.SelectSingleNode("conference/confInfo/hostId").InnerText;
                if (xmldoc.SelectSingleNode("conference/confInfo/schedularID") != null)
                    Schedular = xmldoc.SelectSingleNode("conference/confInfo/schedularID").InnerText;

                if (xmldoc.SelectSingleNode("//conference/confInfo/WebExHostURL") != null)
                    hdnWebExHostURL.Value = xmldoc.SelectSingleNode("/conference/confInfo/WebExHostURL").InnerText.ToString();
                if (xmldoc.SelectSingleNode("//conference/confInfo/WebEXPartyURL") != null)
                    hdnWebExPartyURL.Value = xmldoc.SelectSingleNode("//conference/confInfo/WebEXPartyURL").InnerText.ToString();

                
                if (string.IsNullOrEmpty(hdnWebExHostURL.Value) && string.IsNullOrEmpty(hdnWebExPartyURL.Value))
                    btnwebexconf.Visible = false;
                else
                {
                    if (hdnConfHost.Text == Session["userID"].ToString() || Schedular == Session["userID"].ToString() || !(string.IsNullOrEmpty(hdnWebExPartyURL.Value)))
                        btnwebexconf.Visible = true;
                    else 
                        btnwebexconf.Visible = false;
                }
                //if (hdnWebExHostURL.Value != "" && hdnWebExPartyURL.Value != "" && (hdnConfHost.Text == Session["userID"].ToString() || Schedular == Session["userID"].ToString()))
                //    btnwebexconf.Visible = true;
                //else
                //    btnwebexconf.Visible = false;

                //ZD 100221 Ends

                //FB 2501 Ends
                //FB 2819 Starts
                isPCConf = false;
                if (xmldoc.SelectSingleNode("//conference/confInfo/isPCconference") != null)
                {
                    if (xmldoc.SelectSingleNode("//conference/confInfo/isPCconference").InnerText.Equals("1"))
                        isPCConf = true;
                }
                //FB 2819 Ends
                

                //ZD 100522 Start
                Session["VMRConfType"] = VMConfType;
                if (VMConfType == "2")
                {
                    ConfBridges = xmldoc.SelectNodes("//conference/confInfo/ConfBridges/ConfBridge");
                    if (ConfBridges != null && ConfBridges.Count >0)
                        Session["RoomVMRBridge"] = ConfBridges[0].InnerText;
                }   
                //ZD 100522 Ends

                /* *** Recurrence Fixes - commented for edit With Some instances in past (FB 1131) - start *** */
                //string recurring = "0";
                //if (xmldoc.SelectSingleNode("/conference/confInfo/recurring") != null)
                //    recurring = obj.GetProperValue(xmldoc.SelectSingleNode("/conference/confInfo/recurring").InnerText);
                /* *** Recurrence Fixes - commented for edit With Some instances in past (FB 1131) - end *** */

                switch (lblStatus.Text.ToString())
                {
                    case ns_MyVRMNet.vrmConfStatus.Scheduled:
                        lblStatus.Text = obj.GetTranslatedText("Scheduled");//FB 1830 - Translation
                        tblNoEndpoints.Visible = false;
                        tblEndpoints.Visible = true;
                        WebEXMeetingStatus.Text = obj.GetTranslatedText("Not started"); //ZD 100221
                        break;
                    case ns_MyVRMNet.vrmConfStatus.Pending:
                        lblStatus.Text = obj.GetTranslatedText("Pending");//FB 1830 - Translation
                        trAcceptReject.Visible = false;
                        trMCU.Visible = false;
                        WebEXMeetingStatus.Text = obj.GetTranslatedText("Not started"); //ZD 100221
                        //if (Menu22.Items.Count > 1) fogbugz case 184
                        //{
                        //    Menu22.Items.RemoveAt(1);
                        //    Menu22.Items.RemoveAt(1);
                        //}
                        break;
                    case ns_MyVRMNet.vrmConfStatus.Terminated:
                        lblStatus.Text = obj.GetTranslatedText("Terminated");//FB 1830 - Translation
                        trEdit.Visible = false;
                        trCancel.Visible = false;
                        trAcceptReject.Visible = false;
                        trMCU.Visible = false;
                        WebEXMeetingStatus.Text = obj.GetTranslatedText("Terminated"); //ZD 100221
                        //Code Commented For FB 1422 
                        //if ((Menu22.Items.Count > 1) && (!recurring.Equals("1")))//FB 1133
                        //{
                        //    Menu22.Items.RemoveAt(1);
                        //    Menu22.Items.RemoveAt(1);
                        //}
                        break;
                    case ns_MyVRMNet.vrmConfStatus.Ongoing:
                        lblStatus.Text = obj.GetTranslatedText("Ongoing");//FB 1830 - Translation
                        trEdit.Visible = false;
                        tblNoEndpoints.Visible = false;
                        tblEndpoints.Visible = true;
                        tblTerminalControl.Visible = true;
                        btnDeleteConf.Text = obj.GetTranslatedText("Terminate");//FB 1830 - Translation
                        trAcceptReject.Visible = false;
                        refreshCell.Visible = true;
                        trClone.Visible = false; //FB 2274
                        WebEXMeetingStatus.Text = obj.GetTranslatedText("Started"); //ZD 100221
                        break;
                    case ns_MyVRMNet.vrmConfStatus.OnMCU:
                        lblStatus.Text = obj.GetTranslatedText("On MCU");//FB 1830 - Translation
                        if (hdnMCUSynchronous.Value == "1" && ViewState["hasEdit"].ToString()  == "1") //If Synchronous conf-Edit and Delete = True
                            trEdit.Visible = true;//ZD 100036
                        else
                            trEdit.Visible = false;//ZD 100036

        				//ZD 100085 Starts
                        tblNoEndpoints.Visible = false;
                        tblTerminalControl.Visible = true;
                        btnDeleteConf.Text = obj.GetTranslatedText("Delete"); //ZD 100036
                        refreshCell.Visible = true;
        				//ZD 100085 End
                        trMCU.Visible = false;
                        trClone.Visible = false; //ZD 100036
                        WebEXMeetingStatus.Text = obj.GetTranslatedText("Terminated"); //ZD 100221
                        break;
                    case ns_MyVRMNet.vrmConfStatus.Completed:
                        lblStatus.Text = obj.GetTranslatedText("Completed");//FB 1830 - Translation
                        trEdit.Visible = false;
                        trCancel.Visible = false;
                        trClone.Visible = true;    //FB 1469 - Enabled Now(Recurring Fixes - Clone is disabled for past normal and single instance conference)
                        trAcceptReject.Visible = false;
                        trMCU.Visible = false;
                        WebEXMeetingStatus.Text = obj.GetTranslatedText("Completed"); //ZD 100221
                        //Code Commented For FB 1422 
                        //if ((Menu22.Items.Count > 1) && (!recurring.Equals("1")))//FB 1133
                        //{
                        //    Menu22.Items.RemoveAt(1);
                        //    Menu22.Items.RemoveAt(1);

                        //}
                        break;
                    case ns_MyVRMNet.vrmConfStatus.Deleted:
                        lblStatus.Text = obj.GetTranslatedText("Deleted");//FB 1830 - Translation
                        trEdit.Visible = false;
                        trCancel.Visible = false;
                        trAcceptReject.Visible = false;
                        trMCU.Visible = false;
                        WebEXMeetingStatus.Text = obj.GetTranslatedText("Deleted"); //ZD 100221
                        //Code Commented For FB 1422 
                        //if (Menu22.Items.Count > 1)
                        //{
                        //    Menu22.Items.RemoveAt(1);
                        //    Menu22.Items.RemoveAt(1);
                        //}
                        break;
					//ZD 102532 starts
                    case ns_MyVRMNet.vrmConfStatus.WaitList:
                        lblStatus.Text = obj.GetTranslatedText("Wait List");
                        tblNoEndpoints.Visible = false;
                        tblEndpoints.Visible = true;
                        WebEXMeetingStatus.Text = obj.GetTranslatedText("Not started");
                        break;
					//ZD 102532 End
                    default:
                        lblStatus.Text = "Undefined";
                        WebEXMeetingStatus.Text = obj.GetTranslatedText("Not started"); //ZD 100221

                        break;
                }

                switch (lblConfType.Text.ToString())
                {
                    case ns_MyVRMNet.vrmConfType.RoomOnly:
                        //Added for FB 1428 Start
                        if (Application["Client"].ToString().ToUpper() == "MOJ")
                            lblConfType.Text = "Room Hearing";
                        else
                            //Added for FB 1428 End
                        lblConfType.Text = obj.GetTranslatedText("Room Conference");//FB 1830 - Translation
                        trMCU.Visible = false;
                        tblAV.Visible = false;
                        btnDeleteConf.Text = obj.GetTranslatedText("Delete");//FB 1830 - Translation
                        //FB 2501 Starts
                        tdStartMode.Visible = false;
                        tdStartMode1.Visible = false;
                        tdStartModeSelection.Visible = false;
                        //FB 2501 Ends
                        trMUCPreTime.Visible = false; //ZD 100085
                        if (Menu22.Items.Count > 1)
                        {
                            Menu22.Items.RemoveAt(1);
                            Menu22.Items.RemoveAt(1);
                            //Code Added For FB 1422 
                            Menu22.Items.RemoveAt(1);
                        }
                        
                        break;
                    case ns_MyVRMNet.vrmConfType.AudioOnly:
                        //Added for FB 1428 Start
                        if (Application["Client"].ToString().ToUpper() == "MOJ")
                            lblConfType.Text = "Audio Only Hearing";
                        else
                            //Added for FB 1428 End
                            lblConfType.Text = obj.GetTranslatedText("Audio Only Conference");//FB 1830 - Translation
                        //Code Added For FB 1422
                        //FB 2501 Starts
                        if (VMRConf || isPCConf || hdnPermanentConf.Value == "1") //FB 2819 //ZD 100522
                        {
                            tdStartMode.Visible = false;
                            tdStartMode1.Visible = false;
                            tdStartModeSelection.Visible = false;
                            trMUCPreTime.Visible = false; //ZD 100085
                        }
                        else
                        {
                            tdStartMode.Visible = true;
                            tdStartMode1.Visible = true;
                            tdStartModeSelection.Visible = true;
                            trMUCPreTime.Visible = true; //ZD 100085
                        }
                        //FB 2501 Ends
                        if (Menu22.Items.Count > 1)
                        {
                            Menu22.Items.RemoveAt(3);
                        }
                        break;
                    case ns_MyVRMNet.vrmConfType.AudioVideo:
                        //Added for FB 1428 Start
                        if (Application["Client"].ToString().ToUpper() == "MOJ")
                            lblConfType.Text = "Audio/Video Hearing";
                        else
                            //Added for FB 1428 End
                            lblConfType.Text = obj.GetTranslatedText("Audio/Video Conference");//FB 1830 - Translation
                        //CodeAdded For FB 1422 
                        //FB 2501 Starts
                        if (VMRConf || isPCConf || hdnPermanentConf.Value == "1") //FB 2819 //ZD 100522
                        {
                            tdStartMode.Visible = false;
                            tdStartMode1.Visible = false;
                            tdStartModeSelection.Visible = false;
                            trMUCPreTime.Visible = false; //ZD 100085
                        }
                        else
                        {
                            tdStartMode.Visible = true;
                            tdStartMode1.Visible = true;
                            tdStartModeSelection.Visible = true;
                            trMUCPreTime.Visible = true; //ZD 100085
                        }
                        //FB 2501 Ends
                        if (Menu22.Items.Count > 1)
                        {
                            Menu22.Items.RemoveAt(3);
                        }
                        break;
                    case ns_MyVRMNet.vrmConfType.P2P:
                        //Added for FB 1428 Start
                        if (Application["Client"].ToString().ToUpper() == "MOJ")
                            lblConfType.Text = "Point-To-Point Hearing";
                        else
                            //Added for FB 1428 End
                        lblConfType.Text = obj.GetTranslatedText("Point-To-Point Conference");//FB 1830 - Translation

                        tblTerminalControl.Visible = false;
                        tblEndpoints.Visible = false;
                        tblP2PEndpoints.Visible = true;
                        //Code Added For FB 1422
                        tblAV.Visible = false;
                        trMCU.Visible = false;
                        //FB 2501 Starts
                        tdStartMode.Visible = false;
                        tdStartMode1.Visible = false;
                        tdStartModeSelection.Visible = false;
                        //FB 2501 Ends
                        trMUCPreTime.Visible = false; //ZD 100085
                        if (Menu22.Items.Count > 1)
                        {
                            //Menu22.Items.RemoveAt(1);//Code commented for disney
                            Menu22.Items.RemoveAt(1);
                        }
                        break;
                        //FB 2694 Start
                    case ns_MyVRMNet.vrmConfType.HotDesking:
                        lblConfType.Text = obj.GetTranslatedText("Hotdesking Conference");//ZD 103095
                        tdStartMode.Visible = false;
                        tdStartMode1.Visible = false;
                        tdStartModeSelection.Visible = false;
                        trPuPw.Visible = false;
                        tdSecured.Visible = false;
                        tdSecured1.Visible = false;
                        tdSecuredSelection.Visible = false;
                        tdRemainder.Visible = false;
                        tdRemainder1.Visible = false;
                        tdRemainderselection.Visible = false;  	
                        trMCU.Visible = false;
                        tblAV.Visible = false;
                        trPrt.Visible = false; //FB 2694
                        tblHKWO.Visible = false;
                        trMUCPreTime.Visible = false; //ZD 100085
                        trBuffer.Visible = false; //ZD 102394
                        btnDeleteConf.Text = obj.GetTranslatedText("Delete");
                        if (Menu22.Items.Count > 1)
                        {
                            Menu22.Items.RemoveAt(1);
                            Menu22.Items.RemoveAt(1);
                            Menu22.Items.RemoveAt(1);
                        }
                        break;
                        //FB 2694 End
                    case ns_MyVRMNet.vrmConfType.OBTP: //ZD 100513
                        if (Application["Client"].ToString().ToUpper() == "MOJ")
                            lblConfType.Text = "OBTP Hearing";
                        else
                            lblConfType.Text = obj.GetTranslatedText("OBTP Conference");
                        if (VMRConf || isPCConf || hdnPermanentConf.Value == "1") 
                        {
                            tdStartMode.Visible = false;
                            tdStartMode1.Visible = false;
                            tdStartModeSelection.Visible = false;
                            trMUCPreTime.Visible = false; 
                        }
                        else
                        {
                            tdStartMode.Visible = true;
                            tdStartMode1.Visible = true;
                            tdStartModeSelection.Visible = true;
                            trMUCPreTime.Visible = true; 
                        }
                        if (Menu22.Items.Count > 1)
                            Menu22.Items.RemoveAt(3);
                        break;
                    default:
                        lblConfType.Text = "Undefined";
                        trMCU.Visible = false;
                        break;

                }


                //Response.Write(obj.Transfer(outXML));
               //code modified for buffer zone -- Start
                if (xmldoc.SelectSingleNode("/conference/confInfo/immediate").InnerText.Equals("1"))
                {
                    //blConfDate.Text = DateTime.Now.ToString("MM/dd/yyyy") ;
                    //lblConfDate.Text = "Immediate.";

                    //FB 1774 - Start

                    //DateTime startDate = DateTime.MinValue;
                    /*
                    //FB 2588 Ends
                    startDate = Convert.ToDateTime(xmldoc.SelectSingleNode("/conference/confInfo/startDate").InnerText);
                    int syear = startDate.Year;
                    int smonth = startDate.Month;
                    int sday = startDate.Day;

                    int sHour = Convert.ToInt16(xmldoc.SelectSingleNode("/conference/confInfo/startHour").InnerText);
                    int sMin = Convert.ToInt16(xmldoc.SelectSingleNode("/conference/confInfo/startMin").InnerText);
                    string sSet = xmldoc.SelectSingleNode("/conference/confInfo/startSet").InnerText;
                    if ((sSet.ToUpper().Equals("PM")) && (sHour != 12))
                        sHour += 12;

                    DateTime sDate = DateTime.Parse(smonth + "/" + sday + "/" + syear + " " + sHour + ":" + sMin + " " + sSet);  //buffer zone
                    */
                    //ZD 100718 Starts
                    //FB 2588 Starts
                    DateTime immdStartDate = DateTime.MinValue;

                    immdStartDate = Convert.ToDateTime(xmldoc.SelectSingleNode("/conference/confInfo/ImmediateConfDate").InnerText);
                    int sIHour = Convert.ToInt16(xmldoc.SelectSingleNode("/conference/confInfo/ImmediateConfHour").InnerText);
                    int sIMin = Convert.ToInt16(xmldoc.SelectSingleNode("/conference/confInfo/ImmediateConfMin").InnerText);
                    string Iset = xmldoc.SelectSingleNode("/conference/confInfo/ImmediateConfSet").InnerText;

                    if ((Iset.ToUpper().Equals("PM")) && (sIHour != 12))
                        sIHour += 12;

                    int sIYear = immdStartDate.Year;
                    int sIMonth = immdStartDate.Month;
                    int sIDay = immdStartDate.Day;

                    //FB 1774 - End
                    int dur = 0;
                    if (xmldoc.SelectSingleNode("/conference/confInfo/durationMin") != null)
                        int.TryParse(xmldoc.SelectSingleNode("/conference/confInfo/durationMin").InnerText, out dur);

                    DateTime sIDate = DateTime.Parse(sIMonth + "/" + sIDay + "/" + sIYear + " " + sIHour + ":" + sIMin + " " + Iset);  //FB 2588
                    DateTime confendDateTime = sIDate.AddMinutes(dur);

                    lblConfDate.Text = myVRMNet.NETFunctions.GetFormattedDate(sIDate);
                    WebEXstartdate.Text = lblConfDate.Text;// myVRMNet.NETFunctions.GetFormattedDate(smonth + "/" + sday + "/" + syear);//ZD 100221
                    lblConfTime.Text = myVRMNet.NETFunctions.GetFormattedTime(sIDate.ToShortTimeString(), Session["timeFormat"].ToString()); //FB 2588
                    //FB 2960 Start
                    lblSetupDur.Text = myVRMNet.NETFunctions.GetFormattedDate(sIDate) + " " + myVRMNet.NETFunctions.GetFormattedTime(sIDate.ToShortTimeString(), Session["timeFormat"].ToString());
                    if (hdnPermanentConf.Value == "1") //ZD 100522
                        lblTearDownDur.Text = obj.GetTranslatedText("Permanent");
                    else
                        lblTearDownDur.Text = myVRMNet.NETFunctions.GetFormattedDate(confendDateTime) + " " + myVRMNet.NETFunctions.GetFormattedTime(confendDateTime.ToShortTimeString(), Session["timeFormat"].ToString());

                    WebEXStartingTime.Text = lblConfTime.Text + " " + lblTimezone.Text; //ZD 100221
                    //FB 2960 End

                    hdnConfDuration.Text = dur.ToString();
                    //ZD 100522 Starts
                    if (hdnPermanentConf.Value == "1")
                        lblConfDuration.Text = obj.GetTranslatedText("Forever");
                    else
                        lblConfDuration.Text = obj.GetProperValue((dur / 60) + " " + obj.GetTranslatedText("hrs") + " " + (dur % 60) + " " + obj.GetTranslatedText("mins"));//ZD 100288 //ZD 100528
                    //ZD 100522 End
                    WebEXDuration.Text = lblConfDuration.Text;//ZD 100221
                    trClone.Visible = false;                    

                }
                else
                {
                    if (recurring.Equals("1"))
                    {
                        string recurstr = xmldoc.SelectSingleNode("/conference/confInfo/appointmentTime").InnerXml;
                        recurstr += xmldoc.SelectSingleNode("/conference/confInfo/recurrencePattern").InnerXml;
                        if (xmldoc.SelectNodes("/conference/confInfo/recurrenceRange").Count != 0)
                            recurstr += xmldoc.SelectSingleNode("/conference/confInfo/recurrenceRange").InnerXml;
                        recurstr = "<recurstr>" + recurstr + "</recurstr>";
                        //Response.Write(obj.Transfer(recurstr));
                        string tzstr = "<TimeZone>" + xmldoc.SelectSingleNode("/conference/confInfo/timezones").InnerXml + "</TimeZone>";
                        string rst = obj.AssembleRecur(recurstr, tzstr);
                        string[] rst_array = rst.Split('|');
                        string recur = rst_array[0];
                        /* *** Code added by Offshore for fb Issue 1073 DateFormat -Start **** */

                        string recDtString = "";
                        string tempRec = "";

                        String[] recDateArr = recur.Split('#');

                        recur = "";
                        if (recDateArr.Length > 0)
                        {
                            tempRec = recDateArr[recDateArr.Length - 1];
                            if (tempRec != "")
                            {
                                String[] dtsArr = tempRec.Split('&');

                                if (dtsArr.Length > 0)
                                {
                                    for (int lp = 0; lp < dtsArr.Length; lp++)
                                    {
                                        if (dtsArr[lp].IndexOf("/") > 0)
                                        {
                                            dtsArr[lp] = myVRMNet.NETFunctions.GetFormattedDate(dtsArr[lp]);
                                        }
                                        if (recDtString == "")
                                            recDtString = dtsArr[lp];
                                        else
                                            recDtString += "&" + dtsArr[lp];
                                    }
                                }
                            }
                            for (int lp = 0; lp < recDateArr.Length - 1; lp++)
                            {
                                if (recur == "")
                                    recur = recDateArr[lp];
                                else
                                    recur += "#" + recDateArr[lp];
                            }
                            recur += "#" + recDtString;
                        }
                        /* *** Code added by Offshore for fb Issue 1073 DateFormat - End **** */

                        Recur.Text = recur;
                        string SelectedTimeZoneName = rst_array[1];
                        if (xmldoc.SelectNodes("recurrenceRange").Count != 0)
                            //Code changed by Offshore for FB Issue 1073 -- Start
                            //lblConfDate.Text = xmldoc.SelectSingleNode("/conference/confInfo/recurrenceRange/startDate").InnerText;
                            lblConfDate.Text = myVRMNet.NETFunctions.GetFormattedDate(xmldoc.SelectSingleNode("/conference/confInfo/recurrenceRange/startDate").InnerText);
                        //Code changed by Offshore for FB Issue 1073 -- End
                        lblConfDuration.Text = obj.GetTranslatedText("N/A"); //FB 1133
                        btnClone.Text = obj.GetTranslatedText("Clone All"); //FB 1133 //FB 1830 - Translation
                        btnDeleteConf.Text = obj.GetTranslatedText("Delete All");//FB 1133 //FB 1830 - Translation
                        /* *** Code changed for Recurrance fixes -- Edit with past instances - Start *** */

                        btnEdit.Text = obj.GetTranslatedText("Edit All");  //FB 1133 //FB JAPAN
                        if (Int32.Parse(Session["admin"].ToString()) > 0) //ZD 101597
                            btnEdit.Attributes.Add("onclick", "javascript:return CustomEditAlert(); ");// FB 1391
                        

                        trClone.Visible = false; //FB 1770
                        if (lblStatus.Text == obj.GetTranslatedText("Pending") || lblStatus.Text == obj.GetTranslatedText("Scheduled"))//FB 1770 //FB JAPAN
                        {
							//ZD 101233 Start
                            if (ViewState["hasEdit"].ToString() == "1")
                                trEdit.Visible = true;
                            if (ViewState["hasDelete"].ToString() == "1")
                                trCancel.Visible = true;//FB 1133
                            if (ViewState["hasClone"].ToString() == "1")
                                trClone.Visible = true;//FB 1133
							//ZD 101233 End
                        }
                        //FB 2274 Starts
                        if (lblStatus.Text == obj.GetTranslatedText("Scheduled") && Session["UsrCrossAccess"].ToString() == "1" && Session["organizationID"].ToString() == "11")
                        {
                            if (Session["organizationID"] != null)
                            {
                                if (Session["multisiloOrganizationID"] != null)
                                {
                                    if (Session["organizationID"].ToString() == Session["multisiloOrganizationID"].ToString())
                                        trClone.Visible = true;
                                    else
                                        trClone.Visible = false;
                                }
                            }
                        }
                        //FB 2274 Ends
                        /*
                        if (hdnConfStatus.Value == "7")//FB 1133
                        {//FB 1133
                            trEdit.Visible = true;//FB 1133
                            btnEdit.Text = "Delete & Clone All";//FB 1133
                        }//FB 1133
                        else//FB 1133
                        {//FB 1133
                         btnEdit.Text = "Edit All";//FB 1133
                        }//FB 1133
                        */

                        /* *** Code changed for Recurrance fixes -- Edit with past instances - End *** */

                        /* *** code added/changed for buffer zone *** -- Start */

                        string startHour = "0", startMin = "0", startSet = "AM", durationMin = "0";
                        double duration = 0;

                        if (xmldoc.SelectSingleNode("//conference/confInfo/appointmentTime/startHour") != null)
                        {
                            if (xmldoc.SelectSingleNode("//conference/confInfo/appointmentTime/startHour").InnerText != "")
                            {
                                startHour = xmldoc.SelectSingleNode("//conference/confInfo/appointmentTime/startHour").InnerText;
                            }
                        }
                        if (xmldoc.SelectSingleNode("//conference/confInfo/appointmentTime/startMin") != null)
                        {
                            if (xmldoc.SelectSingleNode("//conference/confInfo/appointmentTime/startMin").InnerText != "")
                            {
                                startMin = xmldoc.SelectSingleNode("//conference/confInfo/appointmentTime/startMin").InnerText;
                            }
                        }
                        if (xmldoc.SelectSingleNode("//conference/confInfo/appointmentTime/startSet") != null)
                        {
                            if (xmldoc.SelectSingleNode("//conference/confInfo/appointmentTime/startSet").InnerText != "")
                            {
                                startSet = xmldoc.SelectSingleNode("//conference/confInfo/appointmentTime/startSet").InnerText;
                            }
                        }

                        durationMin = xmldoc.SelectSingleNode("//conference/confInfo/appointmentTime/durationMin").InnerText; //buffer zone
                       

                        Double.TryParse(durationMin, out duration);
                        //ZD 100085 Starts
                        DateTime ConfStart = Convert.ToDateTime(startHour + ":" + startMin + " " + startSet);

                         
                        //DateTime setupTime = ConfStart.AddMinutes(setupDuration);
                        string CTime = ConfStart.ToString(tformat);

                        DateTime ConfEndTime = Convert.ToDateTime(startHour + ":" + startMin + " " + startSet);
                        ConfEndTime = ConfEndTime.AddMinutes(duration);
                        //endTime = endTime.AddMinutes(-tearDuration);
                        string ETime = ConfEndTime.ToString(tformat);

                        lblSetupDur.Text = CTime;
                        lblTearDownDur.Text = ETime;
                        
                        //ZD 100085 End                        

                        //ZD 100221 Starts
                        WebEXStartingTime.Text = lblConfTime.Text + " " + lblTimezone.Text;
                        WebEXDuration.Text = lblConfDuration.Text;
                        WebEXHost.Text = xmldoc.SelectSingleNode("conference/confInfo/hostName").InnerText;
                        //ZD 100221 Ends

                        /* *** code added for buffer zone *** -- End */

                    }
                    else
                    {
                        //ZD 101597
                        if (Int32.Parse(Session["admin"].ToString()) > 0)
                        {
                            btnEdit.Attributes.Add("onclick", "javascript:return fnSelectForm();");
                        }
                        else
                        {
                            btnEdit.Attributes.Add("onclick", "javascript:DataLoading(1);");
                        }

                        //FB 1774 - Start
                        DateTime startDate = DateTime.MinValue;
                        startDate = Convert.ToDateTime(xmldoc.SelectSingleNode("/conference/confInfo/startDate").InnerText);
                        int syear = startDate.Year;
                        int smonth = startDate.Month;
                        int sday = startDate.Day;
                        
                        //int syear = Convert.ToInt16(xmldoc.SelectSingleNode("/conference/confInfo/startDate").InnerText.Split('/')[2]);
                        //int smonth = Convert.ToInt16(xmldoc.SelectSingleNode("/conference/confInfo/startDate").InnerText.Split('/')[0]);
                        //int sday = Convert.ToInt16(xmldoc.SelectSingleNode("/conference/confInfo/startDate").InnerText.Split('/')[1]);
                        //FB 1774 - End

                        int sHour = Convert.ToInt16(xmldoc.SelectSingleNode("/conference/confInfo/startHour").InnerText);
                        int sMin = Convert.ToInt16(xmldoc.SelectSingleNode("/conference/confInfo/startMin").InnerText);
                        string sSet = xmldoc.SelectSingleNode("/conference/confInfo/startSet").InnerText;
                        if ((sSet.ToUpper().Equals("PM")) && (sHour != 12))
                            sHour += 12;
                        //DateTime sDate = new DateTime(syear, smonth, sday, sHour, sMin, 0);
                         DateTime sDate = DateTime.Parse(smonth + "/" + sday + "/" + syear + " " + sHour + ":" + sMin + " " + sSet);  //buffer zone
                        //Code changed by Offshore for FB Issue 1073 -- Start
                        //lblConfDate.Text = sDate.ToString("MM/dd/yyyy"); //.Split(' ')[0];
                         //sDate = sDate.to(Session["FormatDateType"].ToString());
                        //lblConfDate.Text = myVRMNet.NETFunctions.GetFormattedDate(sDate); //.Split(' ')[0];
                        lblConfDate.Text = myVRMNet.NETFunctions.GetFormattedDate(sDate);//ToShortDateString(), Session["FormatDateType"].ToString(), Session["EmailDateFormat"].ToString()); //ZD 100995
                        //Code changed by Offshore for FB Issue 1073 -- End
                        lblConfTime.Text = myVRMNet.NETFunctions.GetFormattedTime(sDate.ToShortTimeString(), Session["timeFormat"].ToString());//.Split(' ')[1].Split(':')[0] + ":" + sDate.ToString().Split(' ')[1].Split(':')[1]) + " " + sDate.ToString().Split(' ')[2]; // Convert.ToUInt16(sHour.ToString(), 10) + ":" + Convert.ToUInt16(sMin.ToString(), 10) + " " + sSet; FB 1425
                        int dur = Convert.ToInt32(xmldoc.SelectSingleNode("/conference/confInfo/durationMin").InnerText);
                        lblConfDuration.Text = obj.GetProperValue((dur / 60) + " " +obj.GetTranslatedText("hrs") + " " +(dur % 60) +" "+ obj.GetTranslatedText("mins"));//ZD 100288 //100528
                        hdnConfDuration.Text = dur.ToString();
                        //ZD 101714
                        DateTime systemDate = Convert.ToDateTime(Session["systemDate"].ToString());
                        syear = systemDate.Year;// Convert.ToInt16(Session["systemDate"].ToString().Split('/')[2]);
                        smonth = systemDate.Month;// Convert.ToInt16(Session["systemDate"].ToString().Split('/')[0]);
                        sday = systemDate.Day;// Convert.ToInt16(Session["systemDate"].ToString().Split('/')[1]);
                        sHour = systemDate.Hour;// Convert.ToInt16(Session["systemTime"].ToString().Split(':')[0]);
                        sMin = systemDate.Minute;// Convert.ToInt16(Session["systemTime"].ToString().Split(':')[1].Split(' ')[0]);

                        DateTime sysDate = new DateTime(syear, smonth, sday, sHour, sMin, 0);

                        TimeSpan ts = sDate.Subtract(sysDate);
                        double temp = (ts.Days * 24) + ts.Hours;
                        txtTimeDifference.Text = temp.ToString();

                        //ZD 100602 Starts
                        //code added/changed for buffer zone --Start
                        //string durMin = xmldoc.SelectSingleNode("//conference/confInfo/durationMin").InnerText;
                        //double duration = 0;

                        //Double.TryParse(durMin, out duration);

                        //ZD 100085 Starts
                        //DateTime setupStartDateTime = sDate.AddMinutes(setupDuration);
                        //lblSetupDur.Text = myVRMNet.NETFunctions.GetFormattedDate(setupStartDateTime) + " " + setupStartDateTime.ToString(tformat);
                        lblSetupDur.Text = lblConfDate.Text + " " + lblConfTime.Text;
                        DateTime endDateTime = sDate.AddMinutes(dur);//ZD 100602
                        //DateTime teardownStartDateTime = endDateTime.AddMinutes(-tearDuration);
                        lblTearDownDur.Text = myVRMNet.NETFunctions.GetFormattedDate(endDateTime) + " " + endDateTime.ToString(tformat);
                        //ZD 100085 End

                        //ZD 100221 Starts
                        WebEXstartdate.Text = lblConfDate.Text;// myVRMNet.NETFunctions.GetFormattedDate(DateTime.Parse(smonth + "/" + sday + "/" + syear));
                        WebEXStartingTime.Text = lblConfTime.Text + " " + lblTimezone.Text;
                        WebEXDuration.Text = lblConfDuration.Text;
                        WebEXHost.Text = xmldoc.SelectSingleNode("conference/confInfo/hostName").InnerText;

                        //ZD 100221 Ends
						
						
                        //FB 2274 Starts
                        if (lblStatus.Text == obj.GetTranslatedText("Scheduled") && Session["UsrCrossAccess"].ToString() == "1" && Session["organizationID"].ToString() == "11")
                        {
                            if (Session["organizationID"] != null)
                            {
                                if (Session["multisiloOrganizationID"] != null)
                                {
                                    if (Session["organizationID"].ToString() == Session["multisiloOrganizationID"].ToString())
                                        trClone.Visible = true;
                                    else
                                        trClone.Visible = false;
                                }
                            }
                        }
                        //FB 2274 Ends
                        //code added for buffer zone --End


                        /* *** code added for buffer zone - start *** */
                            //bufferTableCell.Visible = false;
                            //if (setupStartDateTime != DateTime.MinValue && teardownStartDateTime != DateTime.MinValue)
                            //{
                            //    lblConfDate.Text = myVRMNet.NETFunctions.GetFormattedDate(setupStartDateTime);
                            //    lblConfTime.Text = myVRMNet.NETFunctions.GetFormattedTime(setupStartDateTime.ToShortTimeString(), Session["timeFormat"].ToString());
                            //    TimeSpan actualDur = teardownStartDateTime - setupStartDateTime;
                            //    lblConfDuration.Text = actualDur.TotalMinutes.ToString();
                            //}
                        /* *** code added for buffer zone - end *** */
                    }
                   
                }
                char[] splitter ={ '/' };
                lblDescription.Text = obj.GetProperValue(xmldoc.SelectSingleNode("/conference/confInfo/description").InnerText);
                lblDescription.Text = utilObj.ReplaceOutXMLSpecialCharacters(lblDescription.Text,2); //FB 2236
                //ZD 100288 Start
                if (lblDescription.Text == "N/A")
                    lblDescription.Text = obj.GetTranslatedText("N/A");
                //ZD 100288 End
                //if (xmldoc.SelectSingleNode("/conference/confInfo/publicConf").InnerText.Equals("0"))
                //{
                //    hdnIsPublicConf.Value = "0";//ZD 101233
                //    lblPublic.Text = obj.GetTranslatedText("No");//FB 1830 - Translation
                //}
                //else
                //{
                //    hdnIsPublicConf.Value = "1";//ZD 101233
                //    lblPublic.Text = obj.GetTranslatedText("Yes");//FB 1830 - Translation
                //    if (xmldoc.SelectSingleNode("/conference/confInfo/dynamicInvite").InnerText.Equals("1"))
                //        lblRegistration.Text = obj.GetTranslatedText(" (Open for Registration)");//FB 1830 - Translation
                //}

                //FB 2377 - Starts
                //FB 2359 Start

                //FB 2632 - Starts   //FB 3007 START         

                if (Session["EnableOnsiteAV"].ToString() == "1" && !hdnConfType.Value.Equals(ns_MyVRMNet.vrmConfType.HotDesking))
                {
                    lblOnsiteAV.InnerText = obj.GetTranslatedText("No");
                    if (xmldoc.SelectSingleNode("/conference/confInfo/ConciergeSupport/OnSiteAVSupport").InnerText.Equals("1"))
                    {
                        lblOnsiteAV.InnerText = obj.GetTranslatedText("Yes");
                    }
                }
                else
                    trOnSiteAVSupport.Attributes.Add("Style", "Display:None;");

                if (Session["EnableMeetandGreet"].ToString() == "1" && !hdnConfType.Value.Equals(ns_MyVRMNet.vrmConfType.HotDesking))
                {
                    lblmeet.InnerText = obj.GetTranslatedText("No");
                    if (xmldoc.SelectSingleNode("/conference/confInfo/ConciergeSupport/MeetandGreet").InnerText.Equals("1"))
                    {
                        lblmeet.InnerText = obj.GetTranslatedText("Yes");
                    }
                }
                else
                    trMeetandGreet.Attributes.Add("Style", "Display:None;");

                if (Session["EnableConciergeMonitoring"].ToString() == "1" && !hdnConfType.Value.Equals(ns_MyVRMNet.vrmConfType.HotDesking))
                {
                    lblConciergeMonitoring.InnerText = obj.GetTranslatedText("No");
                    if (xmldoc.SelectSingleNode("/conference/confInfo/ConciergeSupport/ConciergeMonitoring").InnerText.Equals("1"))
                    {
                        lblConciergeMonitoring.InnerText = obj.GetTranslatedText("Yes");
                    }
                }
                else
                    trConciergeMonitoring.Attributes.Add("Style", "Display:None;");
                
                lblConfVNOC.Text = obj.GetTranslatedText("N/A");
                lblVNOC.InnerText = obj.GetTranslatedText("No");
                if (Session["EnableVNOCselection"].ToString() == "1" && Session["EnableDedicatedVNOC"].ToString() == "1" && !hdnConfType.Value.Equals(ns_MyVRMNet.vrmConfType.HotDesking))
                {
                    trDedicatedVNOC.Visible = true;
                    XmlNodeList ConfVNOCnodes = xmldoc.SelectNodes("//conference/confInfo/ConciergeSupport/ConfVNOCOperators/VNOCOperator");
                    if (ConfVNOCnodes.Count > 0)
                    {
                        lblConfVNOC.Text = "";
                        lblVNOC.InnerText = obj.GetTranslatedText("Yes");
                        for (int i = 0; i < ConfVNOCnodes.Count; i++)
                        {
                            {
                                if (ConfVNOCnodes[i].InnerText != null)
                                {
                                    if (lblConfVNOC.Text == "")
                                        lblConfVNOC.Text = ConfVNOCnodes[i].InnerText.Trim();
                                    else
                                        lblConfVNOC.Text += ",\n" + ConfVNOCnodes[i].InnerText.Trim();
                                }
                            }
                        }
                    }                    
                }
                else
                    trDedicatedVNOC.Visible = false;
                //FB 2632 - End FB 3007 END
                

                //FB 2359 End
                //FB 2377 - End
                string tzID = xmldoc.SelectSingleNode("/conference/confInfo/timeZone").InnerText;
                hdnTimeZoneId.Value = tzID; //ZD 100602
                
                string tzName = "";
                XmlNodeList nodes = xmldoc.SelectNodes("/conference/confInfo/timezones/timezone");
                int length = nodes.Count;
                for (int i = 0; i < length; i++)
                    if (nodes[i].SelectSingleNode("timezoneID").InnerText.Equals(tzID))
                        tzName = nodes[i].SelectSingleNode("timezoneName").InnerText;
                if (!xmldoc.SelectSingleNode("/conference/confInfo/immediate").InnerText.Equals("1"))
                    lblTimezone.Text = obj.GetProperValue(tzName);

                if (Session["timezoneDisplay"].ToString() == "0") //FB 1425
                    lblTimezone.Text = ""; 

                nodes = xmldoc.SelectNodes("//conference/confInfo/fileUpload/file");
                lblFiles.Text = "";
                //Response.Write("here");
                foreach (XmlNode node in nodes)
                    if (!node.InnerText.Equals(""))
                    {
                        //FB 1830
                        String fileName = getUploadFilePath(node.InnerText);
                        String fPath = node.InnerText;
                        int startIndex = fPath.IndexOf(@"\en\");

                        fPath = fPath.Replace("\\", "/");
                        int len = fPath.Length - 1;
                        //Response.Write(fPath + " : " + startIndex + " : " + len);
                        //fPath = Request.Url.ToString().Substring(0, Request.Url.ToString().IndexOf("/" + language + "/") + 1) + "en" + fPath.Substring(startIndex + 3);//FB 1830
                        fPath = Request.Url.ToString().Substring(0, Request.Url.ToString().IndexOf("/en/") + 1) + "en" + fPath.Substring(startIndex + 3);//FB 1830 //ZD 102277

                        /*
                         * FB 2154
                         * 
                         * if ((language == "en" || fPath.IndexOf(@"\en\") > 0) && startIndex > 0)
                        {
                            fPath = fPath.Replace("\\", "/");                         
                            int len = fPath.Length - 1;
                            //Response.Write(fPath + " : " + startIndex + " : " + len);
                            fPath = Request.Url.ToString().Substring(0, Request.Url.ToString().IndexOf("/"+ language+"/") + 1) + "en" + fPath.Substring(startIndex + 3);//FB 1830
                        }
                        else
                            fPath = "../Image/" + fileName;*/

                        //fPath = fPath.Replace("\\", "/");
                        //int startIndex = fPath.IndexOf("/" + language + "/");//FB 1830
                        //int len = fPath.Length - 1;
                        ////Response.Write(fPath + " : " + startIndex + " : " + len);
                        //fPath = Request.Url.ToString().Substring(0, Request.Url.ToString().IndexOf("/" + language + "/") + 3) + fPath.Substring(startIndex + 3);//FB 1830

                        lblFiles.Text += "<a href='" + fPath + "' target='_blank'>" + fileName + "</a>, ";
                    }
                if (lblFiles.Text.Length > 0)
                    lblFiles.Text = lblFiles.Text.Substring(0, lblFiles.Text.Length - 2);
                else
                    lblFiles.Text = obj.GetProperValue(obj.GetTranslatedText("N/A")); //ZD 100288
                lblConfHost.Text = "<a href='mailto:" + xmldoc.SelectSingleNode("conference/confInfo/hostEmail").InnerText + "'>" + xmldoc.SelectSingleNode("conference/confInfo/hostName").InnerText + "</a>";
                // Code added for the Bug # 74- mpujari
                hdnConfHost.Text = xmldoc.SelectSingleNode("conference/confInfo/hostId").InnerText;
                ShowHostDetails(); //FB 1958
                //ALLDEV-857 -Start
                lblRequester.Text = "<a href='mailto:" + xmldoc.SelectSingleNode("conference/confInfo/lastModifiedByEmail").InnerText + "'>" + xmldoc.SelectSingleNode("conference/confInfo/lastModifiedByName").InnerText + "</a>";
                ShowSchedulerDetails();//ALLDEV-857 -End
                lblLastModifiedBy.Text = obj.GetProperValue(xmldoc.SelectSingleNode("conference/confInfo/loginlastModifiedByName").InnerText); //ZD 103445
                hdnLastModifiedBy.Text = xmldoc.SelectSingleNode("conference/confInfo/lastModifiedById").InnerText;

                WebEXHost.Text = xmldoc.SelectSingleNode("conference/confInfo/hostName").InnerText; //ZD 100221

                nodes = xmldoc.SelectNodes("/conference/confInfo/locationList/selected/level1ID");
                string selRooms = ", ";
                Int32[] selectedRooms = new Int32[nodes.Count]; // = { 0 }; // = new Int32();
                int index = 0;
                foreach (XmlNode selNode in nodes)
                {
                    selRooms += selNode.InnerText + ", ";
                    selectedRooms[index] = new Int32();
                    selectedRooms[index] = Convert.ToInt32(selNode.InnerText);
                    index++;
                }
                //FB 2426 Start
                int RoomCount = 0;
                XmlNodeList guestnodelist = null;
                guestnodelist = xmldoc.SelectNodes("//conference/confInfo/ConfGuestRooms/ConfGuestRoom");// ZD 101224
                RoomCount = nodes.Count + guestnodelist.Count;

                //Response.Write(nodes.Count);
                if (RoomCount > 0)
                    lblLocCount.Text = "(" + RoomCount + obj.GetTranslatedText(" rooms") + ")"; //Fb 2272
                else
                    lblLocCount.Text = obj.GetTranslatedText("(No rooms)");//FB 1830 - Translation
                //FB 2426 End
                lblLocation.Text = "";
                nodes = xmldoc.SelectNodes("/conference/confInfo/locationList/level3List/level3");
                Hashtable hostLocation = new Hashtable(); //ZD 103216
                string strRooms = "";
                foreach (XmlNode node in nodes)
                {
                    XmlNodeList nodes2 = node.SelectNodes("level2List/level2");
                    foreach (XmlNode node2 in nodes2)
                    {
                        XmlNodeList nodes1 = node2.SelectNodes("level1List/level1");

                        foreach (XmlNode node1 in nodes1)
                        {
                            if (IsRoomSelected(Convert.ToInt32(node1.SelectSingleNode("level1ID").InnerText), selectedRooms))
                            {
                                //ZD 103216
                                if (Session["EnableTravelAvoidTrack"].ToString() == "0")
                                {
                                    lblLocation.Text += "<font class='blackblodtext'>" + node.SelectSingleNode("level3Name").InnerText + "</font>" + " > ";  //Organization CSS Module
                                    lblLocation.Text += "<font class='blackblodtext'>" + node2.SelectSingleNode("level2Name").InnerText + "</font>" + " > ";  //Organization CSS Module
                                    lblLocation.Text += "<a href='#' onclick='javascript:chkresource(\"" + node1.SelectSingleNode("level1ID").InnerText + "\")'>" + node1.SelectSingleNode("level1Name").InnerText + "</a><br>";
                                }
                                else
                                {
                                    strRooms = "<font class='blackblodtext'>" + node.SelectSingleNode("level3Name").InnerText + "</font>" + " > ";  //Organization CSS Module
                                    strRooms += "<font class='blackblodtext'>" + node2.SelectSingleNode("level2Name").InnerText + "</font>" + " > ";  //Organization CSS Module
                                    strRooms += "<a href='#' onclick='javascript:chkresource(\"" + node1.SelectSingleNode("level1ID").InnerText + "\")'>" + node1.SelectSingleNode("level1Name").InnerText + "</a><br>";

                                    hostLocation[node1.SelectSingleNode("level1ID").InnerText] = strRooms;
                                }
                            }
                        }
                    }
                }

                //ZD 103216
                if (Session["EnableTravelAvoidTrack"].ToString() == "1")
                {
                    DataTable data = new DataTable();
                    data.Columns.Add("RoomName", typeof(string));
                    data.Columns.Add("RoomID", typeof(string));
                    data.Columns.Add("RoomHost", typeof(string));
                    data.Columns.Add("NoofAttendees", typeof(string));

                    nodes = xmldoc.SelectNodes("/conference/confInfo/locationList/RoomAttendees/RoomAttendee");
                    for (int i = 0; i < nodes.Count; i++)
                    {
                        data.Rows.Add(hostLocation[nodes[i].SelectSingleNode("RoomID").InnerText],
                           nodes[i].SelectSingleNode("RoomID").InnerText, nodes[i].SelectSingleNode("HostRoom").InnerText, nodes[i].SelectSingleNode("NoofAttendees").InnerText);
                    }

                    if (data.Rows.Count > 0)
                    {
                        dgRoomHost.DataSource = data;
                        dgRoomHost.DataBind();
                    }
                }
                //FB 2426 Start
                for (int g = 0; g < guestnodelist.Count; g++)
                {
                    lblLocation.Text += "<font class='blackblodtext'>" + guestnodelist[g].SelectSingleNode("Tier2Name").InnerText + "</font>" + " > ";
                    lblLocation.Text += "<font class='blackblodtext'>" + guestnodelist[g].SelectSingleNode("Tier1Name").InnerText + "</font>" + " > ";
                    lblLocation.Text += "<a href='#' onclick='javascript:chkresource(\"" + guestnodelist[g].SelectSingleNode("RoomID").InnerText + "\")'>" + guestnodelist[g].SelectSingleNode("GuestRoomName").InnerText + "</a>" + obj.GetTranslatedText("(Fly Room)") +" <br>"; //ZD 101344
                }
                //FB 2426 End
                if (dgRoomHost.Items.Count ==0 && lblLocation.Text.Length.Equals(0)) //ZD 103216
                    lblLocation.Text = "<font class='blackblodtext'>" + obj.GetTranslatedText("No Locations") + "</font>";//FB 1830 - Translation


                XmlNodeList endpointNodes = xmldoc.SelectNodes("//conference/confInfo/partys/party[partyAttended ='1']");//ZD 101098
                pnodes = xmldoc.SelectNodes("/conference/confInfo/partys/party");
                
                if (pnodes.Count > 0 && !Application["Client"].ToString().ToUpper().Equals("WASHU")) // fogbugz case 349
                {
                    BindData();
                    if (endpointNodes != null && endpointNodes.Count > 0)
                        BindAttendanceList(endpointNodes);//ZD 101098
                }
                else
                {
                    ShowNoParty();
                    lblPartyCount.Text = obj.GetTranslatedText("(No participants)");//FB 1830 - Translation
                }

                if (endpointNodes != null && endpointNodes.Count > 0)
                    tblAttendancelist.Visible = true;
                else
                    tblAttendancelist.Visible = false;
                //ZD 101098 END

                if (xmldoc.SelectSingleNode("/conference/confInfo/advAVParam/restrictProtocol") != null)
                    lblNWAccess.Text = xmldoc.SelectSingleNode("/conference/confInfo/advAVParam/restrictProtocol").InnerText;
                switch (lblNWAccess.Text)
                {
                    case "1":
                        lblNWAccess.Text = "IP";
                        break;
                    case "2":
                        lblNWAccess.Text = "IP,ISDN,SIP";
                        break;
                    case "3":
                        lblNWAccess.Text = "IP,ISDN,SIP";
                        break;
                    case "4":
                        lblNWAccess.Text = "IP,ISDN,SIP,MPI";
                        break;
                    default:
                        lblNWAccess.Text = "IP,ISDN,SIP,MPI";
                        break;
                }
                //for (int i = 0; i < nodes.Count; i++)
                //    if (nodes[i].SelectSingleNode("videoProtocolID").InnerText.Equals(xmldoc.SelectSingleNode("/conference/confInfo/advAVParam/restrictProtocol").InnerText))
                //        lblNWAccess.Text = obj.GetProperValue(nodes[i].SelectSingleNode("videoProtocolName").InnerText);
                //nodes = xmldoc.SelectNodes("/conference/confInfo/audioUsage/audio");
                //for (int i = 0; i < nodes.Count; i++)
                //    if (nodes[i].SelectSingleNode("audioUsageID").InnerText.Equals(xmldoc.SelectSingleNode("/conference/confInfo/advAVParam/restrictAV").InnerText))
                //        lblRestrictUsage.Text = obj.GetProperValue(nodes[i].SelectSingleNode("audioUsageName").InnerText);

                if (xmldoc.SelectSingleNode("/conference/confInfo/advAVParam/restrictAV") != null)
                    lblRestrictUsage.Text = xmldoc.SelectSingleNode("/conference/confInfo/advAVParam/restrictAV").InnerText;
                switch (lblRestrictUsage.Text)
                {
                    case "1":
                        lblRestrictUsage.Text = obj.GetTranslatedText("Audio only");
                        break;
                    case "2":
                        lblRestrictUsage.Text = obj.GetTranslatedText("Audio/Video");
                        break;
                    default:
                        lblRestrictUsage.Text = obj.GetTranslatedText("Audio only");
                        break;
                }

                lblMaxAudioPorts.Text = obj.GetProperValue(xmldoc.SelectSingleNode("/conference/confInfo/advAVParam/maxAudioPart").InnerText);
                lblMaxVideoPorts.Text = obj.GetProperValue(xmldoc.SelectSingleNode("/conference/confInfo/advAVParam/maxVideoPart").InnerText);
                obj.BindVideoCodecs(lstVideoCodecs); //ZD 100288
                obj.BindAudioCodecs(lstAudioCodecs);//ZD 100288
                obj.BindLineRate(lstLineRate);
                //nodes = xmldoc.SelectNodes("/conference/confInfo/videoSession/session");
                //for (int i = 0; i < nodes.Count; i++)
                //    if (nodes[i].SelectSingleNode("videoSessionID").InnerText.Equals(xmldoc.SelectSingleNode("/conference/confInfo/advAVParam/videoCodec").InnerText))
                //        lblVideoCodecs.Text = obj.GetProperValue(nodes[i].SelectSingleNode("videoSessionName").InnerText);
                lblVideoCodecs.Text = lstVideoCodecs.Items.FindByValue(xmldoc.SelectSingleNode("/conference/confInfo/advAVParam/videoCodec").InnerText).Text; ;
                //nodes = xmldoc.SelectNodes("/conference/confInfo/audioAlgorithm/audio");
                //for (int i = 0; i < nodes.Count; i++)
                //    if (nodes[i].SelectSingleNode("audioAlgorithmID").InnerText.Equals(xmldoc.SelectSingleNode("/conference/confInfo/advAVParam/audioCodec").InnerText))
                //        lblAudioCodecs.Text = obj.GetProperValue(nodes[i].SelectSingleNode("audioAlgorithmName").InnerText);
                lblAudioCodecs.Text = lstAudioCodecs.Items.FindByValue(xmldoc.SelectSingleNode("/conference/confInfo/advAVParam/audioCodec").InnerText).Text; ;
                if (xmldoc.SelectSingleNode("/conference/confInfo/advAVParam/dualStream").InnerText.Equals("1"))
                    lblDualStreamMode.Text = obj.GetTranslatedText("Yes");//FB 1830 - Translation
                else
                    lblDualStreamMode.Text = obj.GetTranslatedText("No");//FB 1830 - Translation
                if (xmldoc.SelectSingleNode("/conference/confInfo/advAVParam/confOnPort").InnerText.Equals("1"))
                    lblConfOnPort.Text = obj.GetTranslatedText("Yes");//FB 1830 - Translation
                else
                    lblConfOnPort.Text = obj.GetTranslatedText("No");//FB 1830 - Translation
                if (xmldoc.SelectSingleNode("/conference/confInfo/advAVParam/encryption").InnerText.Equals("1"))
                    lblEncryption.Text = obj.GetTranslatedText("Yes");//FB 1830 - Translation
                else
                    lblEncryption.Text = obj.GetTranslatedText("No");//FB 1830 - Translation
                if (xmldoc.SelectSingleNode("/conference/confInfo/advAVParam/lectureMode").InnerText.Equals("1"))
                    lblLectureMode.Text = obj.GetTranslatedText("Yes");//FB 1830 - Translation
                else
                    lblLectureMode.Text = obj.GetTranslatedText("No");//FB 1830 - Translation
                if (xmldoc.SelectSingleNode("/conference/confInfo/advAVParam/SingleDialin").InnerText.Equals("1"))
                    lblSingleDialin.Text = obj.GetTranslatedText("Yes");//FB 1830 - Translation
                else
                    lblSingleDialin.Text = obj.GetTranslatedText("No");//FB 1830 - Translation

                // FB 2501 FECC Starts
                if (xmldoc.SelectSingleNode("/conference/confInfo/advAVParam/FECCMode").InnerText.Equals("1"))
                    lblFECCMode.Text = obj.GetTranslatedText("Yes");
                else
                    lblFECCMode.Text = obj.GetTranslatedText("No");
                // FB 2501 FECC Ends

                //nodes = xmldoc.SelectNodes("/conference/confInfo/lineRate/rate");
                //for (int i = 0; i < nodes.Count; i++)
                //    if (nodes[i].SelectSingleNode("lineRateID").InnerText.Equals(xmldoc.SelectSingleNode("/conference/confInfo/advAVParam/maxLineRateID").InnerText))
                //    {
                //        lblMaxLineRate.Text = obj.GetProperValue(nodes[i].SelectSingleNode("lineRateName").InnerText);
                //        LblLineRate.Text = obj.GetProperValue(nodes[i].SelectSingleNode("lineRateName").InnerText);//Code added for disney
                //    } 
                lblMaxLineRate.Text = lstLineRate.Items.FindByValue(xmldoc.SelectSingleNode("/conference/confInfo/advAVParam/maxLineRateID").InnerText).Text;
                LblLineRate.Text = lstLineRate.Items.FindByValue(xmldoc.SelectSingleNode("/conference/confInfo/advAVParam/maxLineRateID").InnerText).Text; //LAtest issue

                String str = xmldoc.SelectSingleNode("//advAVParam/videoLayout").InnerText;
                if (str.Equals(""))
                    str = "1";
                txtSelectedImage.Text = Int32.Parse(str).ToString("00");
                if (xmldoc.SelectSingleNode("/conference/confInfo/advAVParam/videoLayout").InnerText != "")
                    if (Convert.ToInt32(xmldoc.SelectSingleNode("/conference/confInfo/advAVParam/videoLayout").InnerText) < 10)
                        imgVideoDisplay.ImageUrl = "image/displaylayout/0" + xmldoc.SelectSingleNode("/conference/confInfo/advAVParam/videoLayout").InnerText + ".gif";
                    else
                        imgVideoDisplay.ImageUrl = "image/displaylayout/" + xmldoc.SelectSingleNode("/conference/confInfo/advAVParam/videoLayout").InnerText + ".gif";

                //ScriptManager.RegisterStartupScript(this, this.GetType(), "saveLayouts", "saveLayout();", true);
                int.TryParse(txtSelectedImage.Text, out videoLayout);
                DisplayvideoLayout(videoLayout); //ZD 101931

                if (!isParticipant)
                    trAcceptReject.Visible = false;

                //RSS Feed Changes - Start
                String quryStrHf = "0"; //FB 1522
                if (Request.QueryString["hf"] != null)
                    quryStrHf = Request.QueryString["hf"].ToString();

                if (quryStrHf.Equals("1") || isViewUser) //FB 1522
                {
                    trClone.Visible = false;
                    trEdit.Visible = false;
                    trCancel.Visible = false;
                    trMCU.Visible = false;
                }
                //RSS Feed Changes - End

                //FB 1926
                if (xmldoc.SelectSingleNode("/conference/confInfo/isReminder") != null)
                {
                    lblReminders.Text = obj.GetTranslatedText("Yes"); //FB 2272
                    if (xmldoc.SelectSingleNode("/conference/confInfo/isReminder").InnerText == "0")
                        lblReminders.Text = obj.GetTranslatedText("No"); //FB 2272
                }
                //FB 1926

                //FB 2501 Starts
                
                string StartMode="0";
                StartMode = obj.GetProperValue(xmldoc.SelectSingleNode("/conference/confInfo/StartMode").InnerText);

                if (StartMode == "0")
                    lblStartMode.Text = obj.GetTranslatedText("Automatic");
                else
                    lblStartMode.Text = obj.GetTranslatedText("Manual");
                
                //FB 2501 Ends

                //FB 2595 Start //FB 2993  Starts
                if (xmldoc.SelectSingleNode("/conference/confInfo/Secured").InnerText.Equals("1"))
                    lblSecured.Text = obj.GetTranslatedText("NATO Secret");
                else 
                    lblSecured.Text = obj.GetTranslatedText("NATO Unclassified");
                //FB 2595 Ends //FB 2993  Starts

                if (Session["admin"].ToString().Equals("0"))
                    if (isParticipant)
                    {
                        trClone.Visible = false;
                        trEdit.Visible = false;
                        trCancel.Visible = false;
                        trMCU.Visible = false;
                    }
                if (Application["Client"].ToString().ToUpper().Equals("WASHU"))
                    Display24HoursErorrMessage();
                //2457 exchange round trip starts
                conferenceOrigin = xmldoc.SelectSingleNode("//conference/confInfo/confOrigin").InnerText;
                hdnconfOriginID.Value = conferenceOrigin;
                //2457 exchange round trip ends

                

            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace("DisplayconferenceDetails" + ex.Message);//ZD 100263
            }
        }
        protected bool IsRoomSelected(Int32 level1ID, Int32[] roomID)
        {
            try
            {
                for (int i = 0; i < roomID.Length; i++)
                    if (roomID[i].Equals(level1ID))
                        return true;
                return false;
            }
            catch (Exception ex)
            {
                log.Trace("IsRoomSelected" + ex.Message);//ZD 100263
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                errLabel.Visible = true;
                return false;
            }
        }

        public void LoadWorkOrders(DataGrid dg, string tpe, Label lbl, Table lblNoWO)
        {
            try
            {
                //Response.Write("here");
                Boolean flagFor48HoursMessage = false;
                //SearchConferenceWorkOrders(String UserID, String ConfID, String WOName, String Rooms, String DateFrom, String DateTo, String TimeFrom, String TimeTo, String Status, String Tpe, String MaxRecords, String PageEnable, String PageNo)
                String inXML = objInXML.SearchConferenceWorkOrders("", lblConfID.Text, "", "", "", "", "", "", "", tpe, "", "1", "0", "-1"); //ZD 103290
                string outxml = obj.CallMyVRMServer("SearchConferenceWorkOrders", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                outxml = outxml.Replace("&", "and"); //FB 2164
                //Response.Write(obj.Transfer(inXML));
                //Response.Write("<hr>" + obj.Transfer(outxml));
                if (outxml.IndexOf("<error>") >= 0)
                {
                    errLabel.Visible = true;
                    errLabel.Text = obj.ShowErrorMessage(outxml);
                }
                else
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outxml);

                    XmlNodeList nodes = xmldoc.SelectNodes("/WorkOrderList/WorkOrder");
                    XmlTextReader xtr;
                    DataSet ds = new DataSet();

                    foreach (XmlNode node in nodes)
                    {
                        xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                        ds.ReadXml(xtr, XmlReadMode.InferSchema);
                    }
                    DataView dv;
                    DataTable dt;

                    if (ds.Tables.Count > 0)
                    {
                        dv = new DataView(ds.Tables[0]);
                        dt = dv.Table;
                        foreach (DataRow dr in dt.Rows)
                        {
                            if (Request.QueryString["t"].Equals("hf") || (ViewState["hasManage"] != null && ViewState["hasManage"].ToString().Equals("0"))) //ZD 101388
                                dr["Name"] = dr["Name"];
                            else
                                dr["Name"] = "<a href='EditConferenceOrder.aspx?id=" + dr["ConfID"].ToString() + "&t=" + tpe + "&woid=" + dr["ID"].ToString() + "'>" + dr["Name"].ToString() + "</a>";

                            dr["AssignedToID"] = obj.GetMyVRMUserName(dr["AssignedToID"].ToString());
                            switch (dr["Status"].ToString())
                            {
                                case "0":
                                    dr["Status"] = obj.GetTranslatedText("Pending"); //ZD 100288
                                    if (tpe.Equals("3"))
                                        flagFor48HoursMessage = true;
                                    break;
                                case "1":
                                    dr["Status"] = obj.GetTranslatedText("Completed"); //ZD 100288
                                    break;
                                default:
                                    dr["Status"] = obj.GetTranslatedText("Undefined"); //ZD 100288 
                                    break;
                            }
                            //Code added for Fb Issue 1073 -- End
                            if (dt.Columns.Contains("StartByDate"))
                                dr["StartByDate"] = myVRMNet.NETFunctions.GetFormattedDate(dr["StartByDate"].ToString());
                            //WO Bug Fix
                            if (dt.Columns.Contains("StartByTime"))
                                dr["StartByTime"] = myVRMNet.NETFunctions.GetFormattedTime(dr["StartByTime"].ToString(), Session["timeFormat"].ToString());//FB 1906

                            if (dt.Columns.Contains("CompletedByDate"))
                                dr["CompletedByDate"] = myVRMNet.NETFunctions.GetFormattedDate(dr["CompletedByDate"].ToString());
                            //WO Bug Fix
                            if (dt.Columns.Contains("CompletedByTime"))
                                dr["CompletedByTime"] = myVRMNet.NETFunctions.GetFormattedTime(dr["CompletedByTime"].ToString(), Session["timeFormat"].ToString());//FB 1906
                            //Code added for Fb Issue 1073 -- End
                        }
                        if (flagFor48HoursMessage)
                            Display48HoursErorrMessage();
                        lbl.Text = "(" + dt.Rows.Count + " " + obj.GetTranslatedText("work orders") + ")";
                        dv = new DataView(dt);
                        dg.Visible = true;
                        dg.DataSource = dv;
                        dg.DataBind();
                        dg.AllowSorting = true;

                        foreach (DataGridItem dgi in dg.Items)
                        {
                            DataGrid tempDG = (DataGrid)dgi.FindControl("itemsGrid");
                            if (tpe.Equals("2"))
                            {

                            }
                            else
                            {
                                GetWorkOrderDetails(dg, dgi.Cells[0].Text.ToString(), dgi.ItemIndex);
                                dgi.Cells[1].Attributes.Add("onmouseover", "javascript:displayDetails('" + tempDG.ClientID.ToString() + "')");
                                dgi.Cells[1].Attributes.Add("onmouseout", "javascript:hideDetails()");
                                tempDG.Attributes.Add("style", "display:none");
                            }
                        }
                    }
                    else
                    {
                        lblNoWO.Visible = true;
                        if(lbl != null)
                            lbl.Text = obj.GetTranslatedText("(No work orders)");//FB 1830 - Translation
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace("LoadWorkOrders" + ex.Message);//ZD 100263
                errLabel.Visible = true;
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
            }
        }

        protected void Display48HoursErorrMessage()
        {
            try
            {
                if (txtTimeDifference.Text != "")
                    if ((Convert.ToInt16(txtTimeDifference.Text) <= 48) && (Convert.ToInt16(txtTimeDifference.Text) >= 0))
                    {
                        lblAlert.Text += "\nFacility work orders scheduled less than 48 hours in advance may encounter problems being fulfilled as requested."; //FB 2570
                        if (Application["Client"].ToString().ToUpper().Trim().Equals(ns_MyVRMNet.vrmClient.Wustl))
                            lblAlert.Text += "\nPlease phone 362-3563 (AFTER HOURS 362-3100) for further assistance.";
                        lblAlert.Visible = false;//FB 2181 
                    }
            }
            catch (Exception ex)
            {
                log.Trace("" + ex.Message);//ZD 100263
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                errLabel.Visible = true;
            }
        }

        protected void Display24HoursErorrMessage()
        {
            try
            {
                //Response.Write(txtTimeDifference.Text);
                if (txtTimeDifference.Text != "")
                    if ((Convert.ToInt16(txtTimeDifference.Text) <= 24) && (Convert.ToInt16(txtTimeDifference.Text) >= 0))
                    {
                        //lblAlert.Text = "Conferences scheduled less than 24 hours in advance may encounter problems being fulfilled as requested.";
                        lblAlert.Text += "\nPlease phone 362.3563 (AFTER HOURS 362-3100) to confirm your catering workorders.";
                        lblAlert.Visible = false;//FB 2181
                    }
            }
            catch (Exception ex)
            {
                log.Trace("Display24HoursErorrMessage" + ex.Message);//ZD 100263
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                errLabel.Visible = true;
            }
        }
        protected void btnSetupAtMCU_Click()
        {
            try
            {
                string inXML = String.Empty;
                inXML = "<Conference><confID>" + lblConfID.Text + "</confID>" + obj.OrgXMLElement() + "</Conference>"; //ZD 101657
                string outXML = String.Empty;
                //ZD 101657 Starts
                if ((!hdnConfStatus.Value.Equals(ns_MyVRMNet.vrmConfStatus.OnMCU) && !hdnConfStatus.Value.Equals(ns_MyVRMNet.vrmConfStatus.Ongoing)) && (hdnConfType.Value.Equals(ns_MyVRMNet.vrmConfType.AudioVideo)) || (hdnConfType.Value.Equals(ns_MyVRMNet.vrmConfType.OBTP))) 
                    outXML = obj.CallMyVRMServer("CheckDialNumber", inXML, Application["MyVRMServer_ConfigPath"].ToString()); 
                else
                    outXML = "<success>1</success>";

                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Visible = true;
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    return;
                }
                //ZD 101657 Ends
                outXML = obj.CallCOM2("SetConferenceOnMcu", inXML, Application["RTC_ConfigPath"].ToString());
                //Response.Write(obj.Transfer(outXML));

                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Visible = true;
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                }
                else
                {
                    Session.Add("confid", lblConfID.Text);
                    Response.Redirect("ManageConference.aspx?m=1&t=");
                }
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                
            }
        }
        protected void DeleteConference(Object sender, EventArgs e)
        {
			//ZD 100959 Starts
            string outXML = string.Empty;
            string inXML = string.Empty;
            try
            {
                inXML = "<login>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><delconference><conference><confID>" + lblConfID.Text + "</confID><reason></reason></conference></delconference></login>";

                if ((!hdnConfStatus.Value.Equals(ns_MyVRMNet.vrmConfStatus.OnMCU) && !hdnConfStatus.Value.Equals(ns_MyVRMNet.vrmConfStatus.Ongoing)) || (hdnConfType.Value.Equals(ns_MyVRMNet.vrmConfType.RoomOnly))) //FB Case 909 and FB 1088
                    outXML = obj.CallMyVRMServer("DeleteConference", inXML, Application["MyVRMServer_ConfigPath"].ToString()); //FB 2027
                else
                    outXML = "<success>1</success>";

                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Visible = true;
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                }
                else
                {
                    //FB 2363 - Start
                    if (Application["External"].ToString() != "")
                    {
                        String inEXML = "";
                        inEXML = "<SetExternalScheduling>";
                        inEXML += "<confID>" + lblConfID.Text + "</confID>";
                        inEXML += "</SetExternalScheduling>";

                        String outExml = obj.CallCommand("SetExternalScheduling", inEXML);
                    }
                    //FB 2363 - End

                    Session["CalendarMonthly"] = null;//FB 1850
                    //code added for Ciso ICAL FB 1602 -- Start 
                    //ZD 103095
                    if ((!hdnConfStatus.Value.Equals(ns_MyVRMNet.vrmConfStatus.OnMCU) && !hdnConfStatus.Value.Equals(ns_MyVRMNet.vrmConfStatus.Ongoing) && !hdnConfStatus.Value.Equals(ns_MyVRMNet.vrmConfStatus.WaitList)) || (hdnConfType.Value.Equals(ns_MyVRMNet.vrmConfType.RoomOnly)))
                    {
                        String inxml = "<DeleteCiscoICAL>" + obj.OrgXMLElement();
                        inxml += "<UserID>" + Session["userID"].ToString() + "</UserID>";
                        inxml += "<ConfID>" + lblConfID.Text + "</ConfID>";
                        inxml += "</DeleteCiscoICAL>";

                        obj.CallCommand("DeleteCiscoICAL", inxml);

                        //participant ICAl Fix -- Start //FB 1782 start
                        inxml = "<DeleteParticipantICAL>";
                        inxml += obj.OrgXMLElement();
                        inxml += "<UserID>" + Session["userID"].ToString() + "</UserID>";
                        inxml += "<ConfID>" + lblConfID.Text + "</ConfID>";
                        //FB 2457 Start //ZD 100924 Start
                        if (Application["Exchange"] != null && Application["Exchange"].ToString().ToUpper() == "YES")
                            inxml += "<isExchange>1</isExchange>";
                        else if (Application["Exchange"] != null && Application["Exchange"].ToString().ToUpper() == "MOD")
                            inxml += "<isExchange>2</isExchange>";
                        else
                            inxml += "<isExchange>0</isExchange>";
                        //ZD 100924 End
                        inxml += "</DeleteParticipantICAL>";
                        obj.CallCommand("DeleteParticipantICAL", inxml);
                        //participantICAl Fix -- End //FB 1782 end

                    }
                    //code added for Ciso ICAL FB 1602 -- End
                    //FB 2274 Starts
                    if (hdnCrossroomModule != null && hdnCrossroomModule.Value != "")
                        roomModule = hdnCrossroomModule.Value;
                    else if (Session["roomModule"] != null)
                        roomModule = Session["roomModule"].ToString();

                    if (hdnCrosshkModule != null && hdnCrosshkModule.Value != "")
                        hkModule = hdnCrosshkModule.Value;
                    else if (Session["hkModule"] != null)
                        hkModule = Session["hkModule"].ToString();

                    if (hdnCrossfoodModule != null && hdnCrossfoodModule.Value != "")
                        foodModule = hdnCrossfoodModule.Value;
                    else if (Session["foodModule"] != null)
                        foodModule = Session["foodModule"].ToString();
                    //FB 2274 Ends

                    if (foodModule.Equals("1") || roomModule.Equals("1") || hkModule.Equals("1")) //FB 2274
                    {
                        inXML = "";
                        inXML += "<login>";
                        inXML += obj.OrgXMLElement();
                        inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                        inXML += "  <ConferenceID>" + lblConfID.Text + "</ConferenceID>";
                        inXML += "  <WorkorderID>0</WorkorderID>";
                        inXML += "</login>";

                        outXML = obj.CallMyVRMServer("DeleteWorkOrder", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                    }


                    if ((hdnConfStatus.Value.Equals(ns_MyVRMNet.vrmConfStatus.OnMCU) || hdnConfStatus.Value.Equals(ns_MyVRMNet.vrmConfStatus.Ongoing)) && (!hdnConfType.Value.Equals(ns_MyVRMNet.vrmConfType.RoomOnly))) //FB Case 909
                    {
                        log.Trace("in if 1");
                        errLabel.Visible = true;
                        errLabel.Text = obj.GetTranslatedText("Operation Successful!");

                        inXML = "<login>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><conferenceID>" + lblConfID.Text + "</conferenceID>";

                        //ZD 100959
                        if (hdnConfStatus.Value.Equals(ns_MyVRMNet.vrmConfStatus.OnMCU))
                            inXML += "<FromService>0</FromService>"; //RPRM
                        else
                            inXML += "<FromService>1</FromService>"; //DMA

                        //FB 2457 Start  //ZD 100924 Start
                        if (Application["Exchange"] != null && Application["Exchange"].ToString().ToUpper() == "YES")
                            inXML += "<isExchange>1</isExchange>";
                        else if (Application["Exchange"] != null && Application["Exchange"].ToString().ToUpper() == "MOD")
                            inXML += "<isExchange>2</isExchange>";
                        else
                            inXML += "<isExchange>0</isExchange>";
						//ZD 100924 End
                        inXML += "</login>"; //FB 2441
                        //FB 2457 End

                        //FB 1433

                        outXML = obj.CallCOM2("TerminateConference", inXML, Application["RTC_ConfigPath"].ToString());

                        if (outXML.IndexOf("<error>") >= 0)
                        {
                            errLabel.Visible = true;
                            errLabel.Text = obj.ShowErrorMessage(outXML);
                            tblForceTerminate.Visible = true;
                        }
                        else
                        {
                            outXML = obj.CallMyVRMServer("TerminateConference", inXML, Application["MyVRMServer_ConfigPath"].ToString());//FB 2027

                            //ALLDEV-830 start
                            String inxml = "<DeleteCiscoICAL>" + obj.OrgXMLElement();
                            inxml += "<UserID>" + Session["userID"].ToString() + "</UserID>";
                            inxml += "<ConfID>" + lblConfID.Text + "</ConfID>";
                            inxml += "</DeleteCiscoICAL>";

                            obj.CallCommand("DeleteCiscoICAL", inxml);


                            inxml = "<DeleteParticipantICAL>";
                            inxml += obj.OrgXMLElement();
                            inxml += "<UserID>" + Session["userID"].ToString() + "</UserID>";
                            inxml += "<ConfID>" + lblConfID.Text + "</ConfID>";
                            if (Application["Exchange"] != null && Application["Exchange"].ToString().ToUpper() == "YES")
                                inxml += "<isExchange>1</isExchange>";
                            else if (Application["Exchange"] != null && Application["Exchange"].ToString().ToUpper() == "MOD")
                                inxml += "<isExchange>2</isExchange>";
                            else
                                inxml += "<isExchange>0</isExchange>";
                            inxml += "</DeleteParticipantICAL>";
                            obj.CallCommand("DeleteParticipantICAL", inxml);
                            //ALLDEV-830 End

                            if (((LinkButton)sender).Text.IndexOf("Delete &") >= 0)
                            {
                                CloneConference(sender, e);
                            }
                            else
                            {
                                Response.Redirect("ConferenceList.aspx?m=1&t=2", true);
                            }
                        }
                    }
                    else
                    {
                        log.Trace("In else main");
                        if (Session["errMsg"] != null) //FB Case 943
                        {
                            Session["errMsg"] = "";
                            Session["errMsg"] = null;
                        }

                        if (hdnConfStatus.Value.Trim().Equals(ns_MyVRMNet.vrmConfStatus.Ongoing))
                        {
                            if (((LinkButton)sender).Text.IndexOf("Delete &") >= 0 || ((LinkButton)sender).Text.IndexOf("Supprimer &") >= 0)
                            {
                                CloneConference(sender, e);
                            }
                            else
                            {
                                Response.Redirect("ConferenceList.aspx?m=1&t=2", true);
                            }
                        }
                        else
                        {
                            if (((LinkButton)sender).Text.IndexOf("Delete &") >= 0 || ((LinkButton)sender).Text.IndexOf("Supprimer &") >= 0)
                            {
                                CloneConference(sender, e);
                            }
                            else
                            {
                                Response.Redirect("SettingSelect2.aspx?m=1&t=", true);
                            }
                        }
                    }
                }
				//ZD 100959 End
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace("DeleteConference" + ex.Message);//ZD 100263
            }
        }

        protected void ForceTerminate(Object sender, EventArgs e)
        {
            try
            {
                String inXML = "<ForceConfDelete>";
                inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                inXML += "  <confID>" + lblConfID.Text + "</confID>";
                //ZD 101872 Start
                if (Application["Exchange"] != null && Application["Exchange"].ToString().ToUpper() == "YES")
                    inXML += "<isExchange>1</isExchange>";
                else if (Application["Exchange"] != null && Application["Exchange"].ToString().ToUpper() == "MOD")
                    inXML += "<isExchange>2</isExchange>";
                else
                    inXML += "<isExchange>0</isExchange>";
                //ZD 101872 End
                inXML += "</ForceConfDelete>";
                String outXML = obj.CallMyVRMServer("ForceConfDelete", inXML, Application["MyVRMServer_ConfigPath"].ToString()); //ZD 100221 //ZD 100152
                //ALLDEV-830 start
                String inxml = "<DeleteCiscoICAL>" + obj.OrgXMLElement();
                inxml += "<UserID>" + Session["userID"].ToString() + "</UserID>";
                inxml += "<ConfID>" + lblConfID.Text + "</ConfID>";
                inxml += "</DeleteCiscoICAL>";

                obj.CallCommand("DeleteCiscoICAL", inxml);


                inxml = "<DeleteParticipantICAL>";
                inxml += obj.OrgXMLElement();
                inxml += "<UserID>" + Session["userID"].ToString() + "</UserID>";
                inxml += "<ConfID>" + lblConfID.Text + "</ConfID>";
                if (Application["Exchange"] != null && Application["Exchange"].ToString().ToUpper() == "YES")
                    inxml += "<isExchange>1</isExchange>";
                else if (Application["Exchange"] != null && Application["Exchange"].ToString().ToUpper() == "MOD")
                    inxml += "<isExchange>2</isExchange>";
                else
                    inxml += "<isExchange>0</isExchange>";
                inxml += "</DeleteParticipantICAL>";
                obj.CallCommand("DeleteParticipantICAL", inxml);
                //ALLDEV-830 End
                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Visible = true;
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                }
                else
                {
                    Response.Redirect("ConferenceList.aspx?t=2&m=1");
                }
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace("ForceTerminate" + ex.Message);//ZD 100263
            }

        }

        protected void BindData()
        {
            try
            {
                XmlTextReader xtr;
                DataSet ds = new DataSet();
                foreach (XmlNode node in pnodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                DataView dv = new DataView();
                dv = new DataView(ds.Tables[0]);

                DataTable dt = dv.Table;
                //Response.Write(dt.Columns.Count);
                int lengthTable = dt.Columns.Count;

                foreach (DataRow dr in dt.Rows)
                {
                    if (dr["partyID"].ToString().Equals(Session["userID"].ToString()))
                        if (dr["partyInvite"].ToString().Equals("1") || dr["partyInvite"].ToString().Equals("2") || dr["partyInvite"].ToString().Equals("3") || dr["partyInvite"].ToString().Equals("4")) //FB 2376 //FB 2347
                            isParticipant = true;
                }

                String partyRoom = "";//FB 2101
                int ud = 0, acc = 0, rej = 0;
                foreach (DataRow dr in dt.Rows)
                {

                    partyRoom = "";//FB 2101
                    if (dr["partyRoom"] != null) //FB 2101
                        if (dr["partyRoom"].ToString().Trim() != "")
                            partyRoom = "( " + dr["partyRoom"].ToString().Trim() + " )";

                    switch (dr["partyStatus"].ToString())
                    {
                        case "0":
                            dr["partyStatus"] = obj.GetTranslatedText("Undecided");
                            ud++;
                            break;
                        case "1":
                            dr["partyStatus"] = obj.GetTranslatedText("Accepted");
                            acc++;
                            break;
                        case "2":
                            dr["partyStatus"] = obj.GetTranslatedText("Rejected");
                            rej++;
                            break;
                        case "3":
                            dr["partyStatus"] = obj.GetTranslatedText("Request for Reschedule"); //FB 1466
                            break;
                        case "4":
                            dr["partyStatus"] = obj.GetTranslatedText("Assigned"); //ZD 102916
                            break;
                    }
                    switch (dr["partyInvite"].ToString())
                    {
                        case "0":
                            dr["partyInvite"] = obj.GetTranslatedText("CC");
                            break;
                        case "1":
                            dr["partyInvite"] = obj.GetTranslatedText("External Attendee") + partyRoom; //FB 2101
                            break;
                        case "2":
                            dr["partyInvite"] = obj.GetTranslatedText("Room Attendee") + partyRoom; //FB 2101
                            break;
                        case "3":
                            dr["partyInvite"] = obj.GetTranslatedText("PC Attendee") + partyRoom; //FB 2347
							break;
                        case "4": //FB 2376
                            dr["partyInvite"] = obj.GetTranslatedText("Virtual Meeting Room Attendee"); //ZD 100806
                            break;
                        default:
                            dr["partyInvite"] = obj.GetTranslatedText("Undecided");
                            break;
                    }
                }

                lblPartyCount.Text = "(" + acc + " " +obj.GetTranslatedText("Accepted")+", " + rej + " " + obj.GetTranslatedText("Rejected") + " & " + ud + " " + obj.GetTranslatedText("Undecided") + ")";
                //Sorting criteria
                dv.Sort = SortField;
                if (!SortAscending)
                {
                    // append "DESC" to the sort field name in order to 
                    // sort descending
                    dv.Sort += " DESC";
                }

                partyGrid.DataSource = dv;
                partyGrid.DataBind();
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace("BindData" + ex.Message);//ZD 100263

                //Response.End();
            }

        }
        
        //ZD 101098 START
        protected void BindAttendanceList(XmlNodeList endpointNodes)
        {
            try
            {
                XmlTextReader xtr;
                DataSet ds = new DataSet();
                foreach (XmlNode node in endpointNodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }

                //ZD 103737 - Start
                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {
                        
                        DataRow row = null;

                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            row = ds.Tables[0].Rows[i];
                            row["partyConfdate"] = DateTime.Parse(row["partyConfdate"].ToString()).ToString(Session["FormatDateType"].ToString()) + " " + DateTime.Parse(row["partyConfdate"].ToString()).ToString(tformat);
                        }

                        DataView dv = new DataView();
                        dv = new DataView(ds.Tables[0]);

                        DataTable dt = dv.Table;
                        int lengthTable = dt.Columns.Count;

                        partyattendancelistGrid.DataSource = dv;
                        partyattendancelistGrid.DataBind();
                    }

                }
                //ZD 103737 - End
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("BindAttendanceList" + ex.Message);
            }
        }
        //ZD 101098 END

        protected void ShowNoParty()
        {
            tblNoParty.Visible = true;
        }

        protected string getUploadFilePath(string fpn)
        {
            string fPath = String.Empty;
            try 
            {
                
                if (fpn.Equals(""))
                    fPath = "";
                else
                {
                    char[] splitter = { '\\' };
                    string[] fa = fpn.Split(splitter[0]);
                    if (fa.Length.Equals(0))
                        fPath = "";
                    else
                        fPath = fa[fa.Length - 1];
                }
            
            }
            catch (Exception ex)
            { }
            
            return fPath;
        }
        // using ViewState for SortField property
        // For more information about using ViewState bags read <A href="http://msdn.microsoft.com/library/default.asp?url=/library/en-us/dnaspnet/html/asp11222001.asp">this</A> reference
        string SortField
        {

            get
            {
                object o = ViewState["SortField"];
                if (o == null)
                {
                    return String.Empty;
                }
                return (string)o;
            }

            set
            {
                if (value == SortField)
                {
                    //if ascending change to descending or vice versa.
                    SortAscending = !SortAscending;
                }
                ViewState["SortField"] = value;
            }
        }

        // using ViewState for SortAscending property
        bool SortAscending
        {

            get
            {
                object o = ViewState["SortAscending"];
                if (o == null)
                {
                    return true;
                }
                return (bool)o;
            }

            set
            {
                ViewState["SortAscending"] = value;
            }
        }
        protected void SortGrid(Object src, DataGridSortCommandEventArgs e)
        {

            partyGrid.CurrentPageIndex = 0;
            // get the requested sorting field name and set it to SortField
            SortField = e.SortExpression;
            BindData();
        }

        protected void EditConference(Object src, EventArgs e)
        {

            int isExpress = 0, isAdvancedForm = 0, isAdmin = 0; //ZD 102200

            if (Session["hasConference"] != null)
                int.TryParse(Session["hasConference"].ToString(),out isAdvancedForm );
            if (Session["hasExpConference"] != null)
                int.TryParse(Session["hasExpConference"].ToString(), out isExpress);
            if (Session["admin"] != null)
                int.TryParse(Session["admin"].ToString(), out isAdmin);
            //ZD 101233 START
            
            bool isExpressUser = false;

            if (Session["isExpressUser"] != null && Session["isExpressUserAdv"] != null && Session["isExpressManage"] != null)
            {
                if (Session["isExpressUser"].ToString() == "1" || Session["isExpressUserAdv"].ToString() == "1" || Session["isExpressManage"].ToString() == "1")
                    isExpressUser = true;
            }
            Session.Add("ConfID", lblConfID.Text);
            //ZD 101597 //ZD 102200 Starts
            if (isAdvancedForm == 1 && isExpress == 1)
            {
                if (isAdmin > 0)
                {
                    if (hdnEditForm.Value == "1")
                        Response.Redirect("ConferenceSetup.aspx?t=");
                    else if (hdnEditForm.Value == "2")
                        Response.Redirect("ExpressConference.aspx?t=");
                }
                else
                {
                    if (hdnExpressConf.Value == "0" && !isExpressUser)
                        Response.Redirect("ConferenceSetup.aspx?t=");
                    else
                        Response.Redirect("ExpressConference.aspx?t=");
                }
            }
            else if (isAdvancedForm == 1)
                Response.Redirect("ConferenceSetup.aspx?t=");
            else if (isExpress == 1)
                Response.Redirect("ExpressConference.aspx?t=");
            
            //ZD 101233 END //ZD 102200 Ends
            
            
            /*if (((LinkButton)src).Text.IndexOf("Delete &") >= 0 || ((LinkButton)src).Text.IndexOf("Supprimer &") >= 0)//FB 1133
            {//FB 1133
                //Added for the Deleting and Cloning future recurring conferences    
                DeleteConference(src, e);//FB 1133
            }//FB 1133
            else if (((LinkButton)src).Text.IndexOf("Edit") >= 0 || ((LinkButton)src).Text.IndexOf("�diter") >= 0)//FB 1133
            {//FB 1133
                Session.Add("ConfID", lblConfID.Text);
                Response.Redirect("ConferenceSetup.aspx?t=");
            }//FB 1133*/
        }
        protected void TerminalControl(Object src, EventArgs e)
        {
            Response.Redirect("dispatcher/admindispatcher.asp?cmd=GetTerminalControl&confid=" + lblConfID.Text);
        }
        protected void dgAVWO_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        protected void CloneConference(object sender, EventArgs e)
        {
            Session.Add("ConfID", lblConfID.Text);
            Response.Redirect("ConferenceSetup.aspx?t=o");
        }
        protected void ExportToPDF(object sender, EventArgs e)
        {
            try
            {
                while (tempText.Text.Equals(""))
                {
                    Response.Write("here");
                }
                String strScript = "<script language=\"javascript\">document.frmSubmit.Submit()</script>";
                RegisterClientScriptBlock("addItems", strScript);
                PdfConverter pdfConverter = new PdfConverter();
                pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
                pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal;
                pdfConverter.PdfDocumentOptions.ShowHeader = true;
                pdfConverter.PdfDocumentOptions.ShowFooter = true;
                pdfConverter.PdfDocumentOptions.LeftMargin = 5;
                pdfConverter.PdfDocumentOptions.RightMargin = 5;
                pdfConverter.PdfDocumentOptions.TopMargin = 5;
                pdfConverter.PdfDocumentOptions.BottomMargin = 5;
                pdfConverter.PdfDocumentOptions.GenerateSelectablePdf = true;

                pdfConverter.PdfDocumentOptions.ShowHeader = false;

                pdfConverter.PdfFooterOptions.FooterText = "myVRM Version " + Application["Version"].ToString() + ",(c)Copyright " + Application["CopyrightsDur"].ToString() + " myVRM.com. All Rights Reserved."; //FB 1648
                pdfConverter.PdfFooterOptions.FooterTextColor = System.Drawing.Color.Blue;
                pdfConverter.PdfFooterOptions.DrawFooterLine = false;
                pdfConverter.PdfFooterOptions.PageNumberText = "Page";
                pdfConverter.PdfFooterOptions.ShowPageNumber = true;
                int upYear = obj.GetYear(Session["systemDate"].ToString());
                int upMonth = obj.GetMonth(Session["systemDate"].ToString());
                int upDay = obj.GetDay(Session["systemDate"].ToString());
                int upHour = obj.GetHour(Session["systemTime"].ToString());
                int upMinute = obj.GetMinute(Session["systemTime"].ToString());
                String upSet = obj.GetTimeSet(Session["systemTime"].ToString());
                DateTime UserTime = new DateTime(upYear, upMonth, upDay, upHour, upMinute, 0);
                pdfConverter.PdfFooterOptions.FooterText += "\n" + UserTime.ToString("MMMM dd, yyyy, hh:mm tt ") + Session["systemTimeZone"].ToString();
                pdfConverter.LicenseKey = "kGb8Fr0gg2spzZd/SluMYY+Bv/RxuZmz6thATnaDnlkD20HkaCEyR4X+P9QqabXI";
                //Added for FB 1428 Start
                string txtHtml = "";
                if (Application["Client"].ToString().ToUpper() == "MOJ")
                {
                    txtHtml = tempText.Text; //"<html><body><TBODY><TR><TD align=middle colSpan=3><H3><SPAN id=lblHeader>Manage Conference</SPAN><INPUT id=cmd style='BORDER-RIGHT: 0px; BORDER-TOP: 0px; BORDER-LEFT: 0px; WIDTH: 0px; BORDER-BOTTOM: 0px' name=cmd> </H3></TD></TR><TR><TD align=middle colSpan=3><BR><BR><B><SPAN id=lblAlert style='COLOR: red'></SPAN></B></TD></TR><TR><TD align=middle colSpan=3><TABLE width='90%' border=0><TBODY><TR><TD><INPUT class=btprint id=lblConfID style='BORDER-LEFT-COLOR: white; BORDER-BOTTOM-COLOR: white; COLOR: white; BORDER-TOP-STYLE: none; BORDER-TOP-COLOR: white; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; BACKGROUND-COLOR: white; BORDER-RIGHT-COLOR: white; BORDER-BOTTOM-STYLE: none' value=2155 name=lblConfID> <TABLE><TBODY><TR><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana' align=right width=100>Title:</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana' align=left width=300><SPAN id=lblConfName style='FONT-WEIGHT: bold; FONT-SIZE: medium; COLOR: green; FONT-FAMILY: Verdana'>VetPro</SPAN> </TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana' align=right>Unique ID:</TD><TD style='HEIGHT: 21px' align=left width=200 colSpan=2><SPAN id=lblConfUniqueID style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: red; FONT-FAMILY: Verdana'>6067</SPAN></TD><TD class=btprint style='FONT-WEIGHT: bold; FONT-SIZE: medium; COLOR: green; FONT-FAMILY: Verdana' vAlign=top align=left width=200 rowSpan=8><TABLE id=tblActions style='BORDER-RIGHT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-LEFT: blue 1px solid; WIDTH: 100%; BORDER-BOTTOM: blue 1px solid; BORDER-COLLAPSE: collapse' borderColor=blue cellSpacing=0 cellPadding=0 border=0><TBODY><TR class=tableHeader style='HEIGHT: 25px'><TD class=tableHeader>Actions</TD></TR><TR id=trCancel style='FONT-WEIGHT: bold; FONT-SIZE: x-small; BORDER-LEFT-COLOR: gray; BORDER-BOTTOM-COLOR: gray; COLOR: blue; BORDER-TOP-COLOR: gray; HEIGHT: 20px; BACKGROUND-COLOR: #e0e0e0; BORDER-RIGHT-COLOR: gray'><TD id=TableCell2><A onclick=javascript:btnDeleteConference_Click() href='#'>Cancel</A> </TD></TR><TR id=trClone style='FONT-WEIGHT: bold; FONT-SIZE: x-small; BORDER-LEFT-COLOR: gray; BORDER-BOTTOM-COLOR: gray; COLOR: blue; BORDER-TOP-COLOR: gray; HEIGHT: 20px; BORDER-RIGHT-COLOR: gray'><TD id=TableCell4><A id=btnClone href='javascript:__doPostBack('btnClone','')'>Clone</A></TD></TR><TR id=trEdit style='FONT-WEIGHT: bold; FONT-SIZE: x-small; BORDER-LEFT-COLOR: gray; BORDER-BOTTOM-COLOR: gray; COLOR: blue; BORDER-TOP-COLOR: gray; HEIGHT: 20px; BACKGROUND-COLOR: #e0e0e0; BORDER-RIGHT-COLOR: gray'><TD id=TableCell5><A id=btnEdit href='javascript:__doPostBack('btnEdit','')'>Edit</A></TD></TR><TR style='FONT-WEIGHT: bold; FONT-SIZE: x-small; BORDER-LEFT-COLOR: gray; BORDER-BOTTOM-COLOR: gray; COLOR: blue; BORDER-TOP-COLOR: gray; HEIGHT: 20px; BORDER-RIGHT-COLOR: gray'><TD><A id=btnPDF onclick=javascript:pdfReport(); href='javascript:__doPostBack('btnPDF','')'>Export to PDF</A></TD></TR><TR style='FONT-WEIGHT: bold; FONT-SIZE: x-small; BORDER-LEFT-COLOR: gray; BORDER-BOTTOM-COLOR: gray; COLOR: blue; BORDER-TOP-COLOR: gray; HEIGHT: 20px; BACKGROUND-COLOR: #e0e0e0; BORDER-RIGHT-COLOR: gray'><TD><A id=btnPrint onclick=javascript:printpage(); href='javascript:__doPostBack('btnPrint','')'>Print</A></TD></TR><TR id=TableRow4 style='FONT-WEIGHT: bold; FONT-SIZE: x-small; BORDER-LEFT-COLOR: gray; BORDER-BOTTOM-COLOR: gray; COLOR: blue; BORDER-TOP-COLOR: gray; HEIGHT: 20px; BORDER-RIGHT-COLOR: gray'><TD id=TableCell6><A id=btnOutlook onclick='javascript:saveToOutlookCalendar('0','0','1','');' href='javascript:__doPostBack('btnOutlook','')'>Save to Outlook</A></TD></TR></TBODY></TABLE></TD></TR><TR><TD style='FONT-WEIGHT: bold' align=right width=100>Host:</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana' align=left width=300><SPAN id=lblConfHost style='FONT-WEIGHT: normal'><A href='mailto:vrmadmin@expeditevcs.com'>VRM Administrator</A></SPAN></TD><TD style='FONT-WEIGHT: bold' align=right>Password:</TD><TD style='HEIGHT: 21px' align=left width=200 colSpan=2><SPAN id=lblPassword style='FONT-SIZE: x-small; FONT-FAMILY: Verdana'></SPAN>&nbsp;</TD></TR><TR><TD style='FONT-WEIGHT: bold' vAlign=top align=right width=100>Date:</TD><TD style='FONT-WEIGHT: normal; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana' vAlign=top align=left width=300><SPAN id=lblConfDate style='FONT-WEIGHT: normal'></SPAN><SPAN id=lblConfTime style='FONT-WEIGHT: normal'>(GMT-05:00) EST</SPAN> <SPAN id=lblTimezone>Custom Date Selection: 9/13/2007, 9/20/2007, 9/27/2007</SPAN><INPUT id=Recur style='BORDER-LEFT-COLOR: transparent; BORDER-BOTTOM-COLOR: transparent; COLOR: white; BORDER-TOP-STYLE: none; BORDER-TOP-COLOR: transparent; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 10px; BORDER-RIGHT-COLOR: transparent; BORDER-BOTTOM-STYLE: none' value=26&amp;03&amp;00&amp;PM&amp;90#5#9/13/2007&amp;9/20/2007&amp;9/27/2007 name=Recur> <!--26&05&00&PM&60#1&2&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1#7/24/2007&2&5&-1--></TD><TD style='FONT-WEIGHT: bold' vAlign=top align=right>Duration:</TD><TD style='HEIGHT: 21px' vAlign=top align=left width=200 colSpan=2><SPAN id=lblConfDuration style='FONT-WEIGHT: normal; FONT-SIZE: x-small; FONT-FAMILY: Verdana'></SPAN></TD></TR><TR><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana; HEIGHT: 18px' vAlign=top align=right width=100>Status:</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana; HEIGHT: 18px' vAlign=top align=left><SPAN id=lblStatus style='FONT-WEIGHT: normal'>Scheduled</SPAN></TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana' vAlign=top align=right>Type:</TD><TD align=left colSpan=2><SPAN id=lblConfType style='FONT-WEIGHT: normal'>Room Conference</SPAN> </TD></TR><TR><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana' vAlign=top align=right width=100>Public:</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana' vAlign=top align=left colSpan=4><SPAN id=lblPublic style='FONT-WEIGHT: normal'>No</SPAN> <SPAN id=lblRegistration style='FONT-WEIGHT: normal'></SPAN></TD></TR><TR><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana' vAlign=top align=right width=100>Files:</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana' vAlign=top align=left colSpan=5><SPAN id=lblFiles style='FONT-WEIGHT: normal'></SPAN></TD></TR><TR><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana' vAlign=top align=right width=100>Description:</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana' vAlign=top align=left colSpan=5><SPAN id=lblDescription style='FONT-WEIGHT: normal'>Laura Graves, x6969</SPAN>&nbsp;</TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR><TR><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: green; FONT-FAMILY: Verdana' vAlign=top align=right colSpan=3><INPUT class=btprint id=chkExpandCollapse onclick=javascript:ExpandAll() type=checkbox>Collapse All</TD></TR><TR><TD align=middle colSpan=3><TABLE cellSpacing=0 cellPadding=0 width='90%' border=0><TBODY><TR><TD style='FONT-WEIGHT: bold; FONT-SIZE: medium; COLOR: green; FONT-FAMILY: Verdana' align=left colSpan=2><IMG id=img_LOC onclick='ShowHideRow('LOC', this,false)' src='image/loc/nolines_minus.gif' border=0>Locations <SPAN id=lblLocCount style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: #404040; FONT-FAMILY: Verdana'>(1 rooms)</SPAN> </TD></TR><TR id=tr_LOC><TD width='5%'>&nbsp;</TD><TD style='FONT-WEIGHT: bold' vAlign=top align=left><SPAN id=lblLocation style='FONT-WEIGHT: normal'>Ohio &gt; Chillicothe &gt; <A onclick='javascript:chkresource('97')' href='#'>Chillicothe b1 r285 Training Room</A><BR></SPAN></TD></TR></TBODY></TABLE></TD></TR><TR><TD align=middle colSpan=3><TABLE cellSpacing=0 cellPadding=0 width='90%' border=0><TBODY><TR><TD style='FONT-WEIGHT: bold; FONT-SIZE: medium; COLOR: green; FONT-FAMILY: Verdana' align=left colSpan=4><IMG id=img_PAR onclick='ShowHideRow('PAR', this,false)' src='image/loc/nolines_minus.gif' border=0>Participants <SPAN id=lblPartyCount style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: #404040; FONT-FAMILY: Verdana'>(No participants)</SPAN></TD></TR><TR id=tr_PAR><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana' align=middle colSpan=4 rowSpan=3><TABLE id=tblNoParty style='BORDER-RIGHT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-LEFT: blue 1px solid; WIDTH: 90%; BORDER-BOTTOM: blue 1px solid; BORDER-COLLAPSE: collapse' borderColor=blue cellSpacing=0 cellPadding=0 border=0><TBODY><TR class=tableHeader id=TableRow6 style='HEIGHT: 30px'><TD class=tableHeader id=TableCell7>Name</TD><TD class=tableHeader id=TableCell8>Email</TD><TD class=tableHeader id=TableCell9>Status</TD></TR><TR id=TableRow7 style='FONT-SIZE: x-small; FONT-FAMILY: Verdana; HEIGHT: 30px; BACKGROUND-COLOR: #e0e0e0' vAlign=center align=middle><TD id=TableCell10 colSpan=3>There are no participants in this hearing.</TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR><TR><TD align=middle colSpan=3></TD></TR><TR><TD align=middle colSpan=3></TD></TR><TR><TD align=middle colSpan=3><CENTER></CENTER></TD></TR><TR><TD align=middle colSpan=3></TD></TR></TBODY></body></html>";
                }
                else
                {
                    //Added for FB 1428 End
                    txtHtml = tempText.Text; //"<html><body><TBODY><TR><TD align=middle colSpan=3><H3><SPAN id=lblHeader>Manage Conference</SPAN><INPUT id=cmd style='BORDER-RIGHT: 0px; BORDER-TOP: 0px; BORDER-LEFT: 0px; WIDTH: 0px; BORDER-BOTTOM: 0px' name=cmd> </H3></TD></TR><TR><TD align=middle colSpan=3><BR><BR><B><SPAN id=lblAlert style='COLOR: red'></SPAN></B></TD></TR><TR><TD align=middle colSpan=3><TABLE width='90%' border=0><TBODY><TR><TD><INPUT class=btprint id=lblConfID style='BORDER-LEFT-COLOR: white; BORDER-BOTTOM-COLOR: white; COLOR: white; BORDER-TOP-STYLE: none; BORDER-TOP-COLOR: white; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; BACKGROUND-COLOR: white; BORDER-RIGHT-COLOR: white; BORDER-BOTTOM-STYLE: none' value=2155 name=lblConfID> <TABLE><TBODY><TR><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana' align=right width=100>Title:</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana' align=left width=300><SPAN id=lblConfName style='FONT-WEIGHT: bold; FONT-SIZE: medium; COLOR: green; FONT-FAMILY: Verdana'>VetPro</SPAN> </TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana' align=right>Unique ID:</TD><TD style='HEIGHT: 21px' align=left width=200 colSpan=2><SPAN id=lblConfUniqueID style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: red; FONT-FAMILY: Verdana'>6067</SPAN></TD><TD class=btprint style='FONT-WEIGHT: bold; FONT-SIZE: medium; COLOR: green; FONT-FAMILY: Verdana' vAlign=top align=left width=200 rowSpan=8><TABLE id=tblActions style='BORDER-RIGHT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-LEFT: blue 1px solid; WIDTH: 100%; BORDER-BOTTOM: blue 1px solid; BORDER-COLLAPSE: collapse' borderColor=blue cellSpacing=0 cellPadding=0 border=0><TBODY><TR class=tableHeader style='HEIGHT: 25px'><TD class=tableHeader>Actions</TD></TR><TR id=trCancel style='FONT-WEIGHT: bold; FONT-SIZE: x-small; BORDER-LEFT-COLOR: gray; BORDER-BOTTOM-COLOR: gray; COLOR: blue; BORDER-TOP-COLOR: gray; HEIGHT: 20px; BACKGROUND-COLOR: #e0e0e0; BORDER-RIGHT-COLOR: gray'><TD id=TableCell2><A onclick=javascript:btnDeleteConference_Click() href='#'>Cancel</A> </TD></TR><TR id=trClone style='FONT-WEIGHT: bold; FONT-SIZE: x-small; BORDER-LEFT-COLOR: gray; BORDER-BOTTOM-COLOR: gray; COLOR: blue; BORDER-TOP-COLOR: gray; HEIGHT: 20px; BORDER-RIGHT-COLOR: gray'><TD id=TableCell4><A id=btnClone href='javascript:__doPostBack('btnClone','')'>Clone</A></TD></TR><TR id=trEdit style='FONT-WEIGHT: bold; FONT-SIZE: x-small; BORDER-LEFT-COLOR: gray; BORDER-BOTTOM-COLOR: gray; COLOR: blue; BORDER-TOP-COLOR: gray; HEIGHT: 20px; BACKGROUND-COLOR: #e0e0e0; BORDER-RIGHT-COLOR: gray'><TD id=TableCell5><A id=btnEdit href='javascript:__doPostBack('btnEdit','')'>Edit</A></TD></TR><TR style='FONT-WEIGHT: bold; FONT-SIZE: x-small; BORDER-LEFT-COLOR: gray; BORDER-BOTTOM-COLOR: gray; COLOR: blue; BORDER-TOP-COLOR: gray; HEIGHT: 20px; BORDER-RIGHT-COLOR: gray'><TD><A id=btnPDF onclick=javascript:pdfReport(); href='javascript:__doPostBack('btnPDF','')'>Export to PDF</A></TD></TR><TR style='FONT-WEIGHT: bold; FONT-SIZE: x-small; BORDER-LEFT-COLOR: gray; BORDER-BOTTOM-COLOR: gray; COLOR: blue; BORDER-TOP-COLOR: gray; HEIGHT: 20px; BACKGROUND-COLOR: #e0e0e0; BORDER-RIGHT-COLOR: gray'><TD><A id=btnPrint onclick=javascript:printpage(); href='javascript:__doPostBack('btnPrint','')'>Print</A></TD></TR><TR id=TableRow4 style='FONT-WEIGHT: bold; FONT-SIZE: x-small; BORDER-LEFT-COLOR: gray; BORDER-BOTTOM-COLOR: gray; COLOR: blue; BORDER-TOP-COLOR: gray; HEIGHT: 20px; BORDER-RIGHT-COLOR: gray'><TD id=TableCell6><A id=btnOutlook onclick='javascript:saveToOutlookCalendar('0','0','1','');' href='javascript:__doPostBack('btnOutlook','')'>Save to Outlook</A></TD></TR></TBODY></TABLE></TD></TR><TR><TD style='FONT-WEIGHT: bold' align=right width=100>Host:</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana' align=left width=300><SPAN id=lblConfHost style='FONT-WEIGHT: normal'><A href='mailto:vrmadmin@expeditevcs.com'>VRM Administrator</A></SPAN></TD><TD style='FONT-WEIGHT: bold' align=right>Password:</TD><TD style='HEIGHT: 21px' align=left width=200 colSpan=2><SPAN id=lblPassword style='FONT-SIZE: x-small; FONT-FAMILY: Verdana'></SPAN>&nbsp;</TD></TR><TR><TD style='FONT-WEIGHT: bold' vAlign=top align=right width=100>Date:</TD><TD style='FONT-WEIGHT: normal; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana' vAlign=top align=left width=300><SPAN id=lblConfDate style='FONT-WEIGHT: normal'></SPAN><SPAN id=lblConfTime style='FONT-WEIGHT: normal'>(GMT-05:00) EST</SPAN> <SPAN id=lblTimezone>Custom Date Selection: 9/13/2007, 9/20/2007, 9/27/2007</SPAN><INPUT id=Recur style='BORDER-LEFT-COLOR: transparent; BORDER-BOTTOM-COLOR: transparent; COLOR: white; BORDER-TOP-STYLE: none; BORDER-TOP-COLOR: transparent; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 10px; BORDER-RIGHT-COLOR: transparent; BORDER-BOTTOM-STYLE: none' value=26&amp;03&amp;00&amp;PM&amp;90#5#9/13/2007&amp;9/20/2007&amp;9/27/2007 name=Recur> <!--26&05&00&PM&60#1&2&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1#7/24/2007&2&5&-1--></TD><TD style='FONT-WEIGHT: bold' vAlign=top align=right>Duration:</TD><TD style='HEIGHT: 21px' vAlign=top align=left width=200 colSpan=2><SPAN id=lblConfDuration style='FONT-WEIGHT: normal; FONT-SIZE: x-small; FONT-FAMILY: Verdana'></SPAN></TD></TR><TR><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana; HEIGHT: 18px' vAlign=top align=right width=100>Status:</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana; HEIGHT: 18px' vAlign=top align=left><SPAN id=lblStatus style='FONT-WEIGHT: normal'>Scheduled</SPAN></TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana' vAlign=top align=right>Type:</TD><TD align=left colSpan=2><SPAN id=lblConfType style='FONT-WEIGHT: normal'>Room Conference</SPAN> </TD></TR><TR><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana' vAlign=top align=right width=100>Public:</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana' vAlign=top align=left colSpan=4><SPAN id=lblPublic style='FONT-WEIGHT: normal'>No</SPAN> <SPAN id=lblRegistration style='FONT-WEIGHT: normal'></SPAN></TD></TR><TR><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana' vAlign=top align=right width=100>Files:</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana' vAlign=top align=left colSpan=5><SPAN id=lblFiles style='FONT-WEIGHT: normal'></SPAN></TD></TR><TR><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana' vAlign=top align=right width=100>Description:</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana' vAlign=top align=left colSpan=5><SPAN id=lblDescription style='FONT-WEIGHT: normal'>Laura Graves, x6969</SPAN>&nbsp;</TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR><TR><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: green; FONT-FAMILY: Verdana' vAlign=top align=right colSpan=3><INPUT class=btprint id=chkExpandCollapse onclick=javascript:ExpandAll() type=checkbox>Collapse All</TD></TR><TR><TD align=middle colSpan=3><TABLE cellSpacing=0 cellPadding=0 width='90%' border=0><TBODY><TR><TD style='FONT-WEIGHT: bold; FONT-SIZE: medium; COLOR: green; FONT-FAMILY: Verdana' align=left colSpan=2><IMG id=img_LOC onclick='ShowHideRow('LOC', this,false)' src='image/loc/nolines_minus.gif' border=0>Locations <SPAN id=lblLocCount style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: #404040; FONT-FAMILY: Verdana'>(1 rooms)</SPAN> </TD></TR><TR id=tr_LOC><TD width='5%'>&nbsp;</TD><TD style='FONT-WEIGHT: bold' vAlign=top align=left><SPAN id=lblLocation style='FONT-WEIGHT: normal'>Ohio &gt; Chillicothe &gt; <A onclick='javascript:chkresource('97')' href='#'>Chillicothe b1 r285 Training Room</A><BR></SPAN></TD></TR></TBODY></TABLE></TD></TR><TR><TD align=middle colSpan=3><TABLE cellSpacing=0 cellPadding=0 width='90%' border=0><TBODY><TR><TD style='FONT-WEIGHT: bold; FONT-SIZE: medium; COLOR: green; FONT-FAMILY: Verdana' align=left colSpan=4><IMG id=img_PAR onclick='ShowHideRow('PAR', this,false)' src='image/loc/nolines_minus.gif' border=0>Participants <SPAN id=lblPartyCount style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: #404040; FONT-FAMILY: Verdana'>(No participants)</SPAN></TD></TR><TR id=tr_PAR><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana' align=middle colSpan=4 rowSpan=3><TABLE id=tblNoParty style='BORDER-RIGHT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-LEFT: blue 1px solid; WIDTH: 90%; BORDER-BOTTOM: blue 1px solid; BORDER-COLLAPSE: collapse' borderColor=blue cellSpacing=0 cellPadding=0 border=0><TBODY><TR class=tableHeader id=TableRow6 style='HEIGHT: 30px'><TD class=tableHeader id=TableCell7>Name</TD><TD class=tableHeader id=TableCell8>Email</TD><TD class=tableHeader id=TableCell9>Status</TD></TR><TR id=TableRow7 style='FONT-SIZE: x-small; FONT-FAMILY: Verdana; HEIGHT: 30px; BACKGROUND-COLOR: #e0e0e0' vAlign=center align=middle><TD id=TableCell10 colSpan=3>There are no participants in this conference.</TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR><TR><TD align=middle colSpan=3></TD></TR><TR><TD align=middle colSpan=3></TD></TR><TR><TD align=middle colSpan=3><CENTER></CENTER></TD></TR><TR><TD align=middle colSpan=3></TD></TR></TBODY></body></html>"; //Edited fo FB 1428
                }

                //pdfConverter.LicenseKey = "put your serial number here";
                byte[] downloadBytes = pdfConverter.GetPdfBytesFromHtmlString(txtHtml);
                System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
                response.Clear();
                response.AddHeader("Content-Type", "binary/octet-stream");
                String downloadName = lblConfName.Text + ".pdf";
                response.AddHeader("Content-Disposition", "attachment; filename=MyVRM_Conference.pdf; size=" + downloadBytes.Length.ToString());
                //                Response.BinaryWrite(downloadBytes);
                response.BinaryWrite(downloadBytes);

                System.Web.HttpCookie exportCookie = new System.Web.HttpCookie("exportCookie"); // ZD 101326
                exportCookie.Expires = DateTime.Now.AddMinutes(5);
                exportCookie.Value = "1";
                response.Cookies.Add(exportCookie);
                //                response.Flush();
                response.End();
            }
            catch (Exception ex)
            {
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace("ExportToPDF" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }

        protected void SendReminderToHost(object sender, DataGridCommandEventArgs e)
        {
            try
            {
                string inxml = "<login>";
                inxml += obj.OrgXMLElement();//Organization Module Fixes
                inxml += "<userID>" + Session["userID"].ToString() + "</userID>";
                inxml += "<WorkOrderID>" + e.Item.Cells[0].Text.ToString() + "</WorkOrderID>";
                inxml += "</login>";
                //Response.Write(obj.Transfer(inxml));
                string outxml = obj.CallMyVRMServer("SendWorkOrderReminder", inxml, Application["MyVRMServer_ConfigPath"].ToString());
                errLabel.Visible = true;
                if (outxml.IndexOf("<error>") < 0)
                    errLabel.Text = obj.GetTranslatedText("Operation Successful! Reminder email has been sent to the person-in-charge of the work order.");//FB 1830 - Translation
                else
                    errLabel.Text = obj.ShowErrorMessage(outxml);
            }
            catch (Exception ex)
            {
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace("SendReminderToHost" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }
        protected void SendReminderToParticipant(object sender, DataGridCommandEventArgs e)
        {
            try
            {
                string inxml = "<login>";
                inxml += obj.OrgXMLElement();//Organization Module Fixes
                inxml += "<UserID>" + Session["userID"].ToString() + "</UserID>";
                inxml += "<ConfID>" + lblConfID.Text + "</ConfID>";
                inxml += "<PartyID>" + e.Item.Cells[0].Text.ToString() + "</PartyID>";
                inxml += "</login>";
                //Response.Write(obj.Transfer(inxml));
                //string outxml = obj.CallCOM("SendParticipantReminder", inxml, Application["COM_ConfigPath"].ToString());
                string outxml = obj.CallCommand("SendPartyReminder", inxml);  //FB 1830
                errLabel.Visible = true;
                if (outxml.IndexOf("<error>") < 0)
                    errLabel.Text = obj.GetTranslatedText("Operation Successful! Reminder email has been sent to selected participant.");//FB 1830 - Translation
                else
                    errLabel.Text = obj.ShowErrorMessage(outxml);
            }
            catch (Exception ex)
            {
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace("SendReminderToParticipant" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }

        protected void GetWorkOrderDetails(DataGrid sender, string woID, int i)
        {
            try
            {
                DataGrid dg = new DataGrid();
                dg = (DataGrid)sender;
                DataGrid subDG = new DataGrid();
                subDG = (DataGrid)dg.Items[i].FindControl("itemsGrid");

                string inxml = "<login>";
                inxml += obj.OrgXMLElement();//Organization Module Fixes
                inxml += "  <userID>" + Session["userID"].ToString() + "</userID>";
                inxml += "  <ConfID>" + lblConfID.Text + "</ConfID>";
                inxml += "  <WorkorderID>" + woID + "</WorkorderID>";
                inxml += "</login>";
                //Response.Write(obj.Transfer(inxml));
                string outxml = obj.CallMyVRMServer("GetWorkOrderDetails", inxml, Application["MyVRMServer_ConfigPath"].ToString());

                //Response.Write("<hr>" + obj.Transfer(outxml));
                //Response.End();
                if (outxml.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(outxml);
                    errLabel.Visible = true;
                }
                else
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outxml);
                    XmlNodeList nodes = xmldoc.SelectNodes("//WorkOrder/ItemList/Item");
                    XmlTextReader xtr;
                    DataSet ds = new DataSet();
                    foreach (XmlNode node in nodes)
                    {
                        if (!node.SelectSingleNode("QuantityRequested").InnerText.Equals("0"))
                        {
                            xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                            ds.ReadXml(xtr, XmlReadMode.InferSchema);
                        }
                    }
                    DataView dv = new DataView();
                    DataTable dt = new DataTable();

                    if (ds.Tables.Count > 0)
                    {
                        dv = new DataView(ds.Tables[0]);
                        dt = dv.Table;

                        if (dt.Columns.Contains("QuantityRequested").Equals(false))
                        {
                            dt.Columns.Add("QuantityRequested");
                        }
                        foreach (DataRow dr in dt.Rows)
                        {
                            if (dr["QuantityRequested"].ToString().Equals(""))
                                dr["QuantityRequested"] = "0";
                        }
                    }

                    subDG.Visible = true;
                    subDG.DataSource = dv;
                    subDG.DataBind();
                }
            }
            catch (Exception ex)
            {
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace("GetWorkOrderDetails" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }

        //protected void ChangeView(Object sender, EventArgs e)
        //{
        //    if (!rdChangeView.SelectedValue.Equals("1"))
        //    {
        //        if (ImagesPath.Text.Equals(""))
        //            GetVideoLayouts();
        //        LoadEndpoints();
        //    }

        //}
        protected void GetVideoLayouts()
        {
            try
            {
                ImagesPath.Attributes.Add("style", "display:none");
                txtSelectedImage.Attributes.Add("style", "display:none");
                ImageFiles.Attributes.Add("style", "display:none");
                ImageFilesBT.Attributes.Add("style", "display:none");
                ImagesPath.Text = "image/displaylayout/";
                if (Application["Client"].ToString().ToUpper().Equals("BTBOCES"))
                    ImagesPath.Text += "BTBoces/";
                ImageFiles.Text = "";
                //Response.Write(Server.MapPath(ImagesPath.Text));
                foreach (string file in Directory.GetFiles(Server.MapPath(ImagesPath.Text)))
                    ImageFiles.Text += file.Replace(Server.MapPath(ImagesPath.Text), "").Replace(".gif", "") + ":";
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " ||| " + ex.Message);
            }
        }

        protected void UpdateEndpoints(Object sender, EventArgs e) //FB Case 944, 936, 861 and 730 Saima
        {
            log.Trace("in UpdateEndpoints");
            Refreshchk.Checked = true;
            LoadEndpoints();
            DisplayvideoLayout(videoLayout);//ZD 101931
        }

        protected void LoadEndpoints()
        {
            try
            {
                //if (hdnConfType.Value.Equals(ns_MyVRMNet.vrmConfType.P2P) && lblStatus.Text == "Ongoing" && Refreshchk.Checked == true) //Blue Status Project
                //    p2pStatus = GetP2pStatus();

                if (hdnConfType.Value.Equals(ns_MyVRMNet.vrmConfType.P2P) && lblStatus.Text == obj.GetTranslatedText("Ongoing")) //ZD 101133
                {
                    //tdmessage.Attributes.Add("Style", "visibility:hidden;"); // ZD 101189
                    //tdsendmessage.Attributes.Add("Style", "visibility:hidden;");
                    //SendMsgAll.Attributes.Add("Style", "visibility:hidden;");
                    tdmessage.Attributes.Add("Style", "display:none;");
                    tdsendmessage.Attributes.Add("Style", "display:none;");
                    SendMsgAll.Attributes.Add("Style", "display:none;");
                }

                string isMonitorDMA = "", TranStxt = "", status = "0";// FB 2441 //ZD 100288 //ALLDEV-779
                int RPRMMCU = 0; // FB 2441

                String inXML = "<login>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                inXML += "  <confID>" + lblConfID.Text + "</confID>";//FB1642- Audio Add On..
                inXML += "</login>";
                log.Trace("GetTerminalControl: " + inXML);
                //String outXML = obj.CallCOM("GetTerminalControl", inXML, Application["COM_ConfigPath"].ToString());
                String outXML = obj.CallMyVRMServer("GetTerminalControl", inXML, Application["MyVRMServer_ConfigPath"].ToString());//FB 2027(GetTerminalControl)
                log.Trace("GetTerminalControl: " + outXML);
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                XmlDocument xmldocEP = new XmlDocument();
                XmlNodeList nodesEP = xmldoc.SelectNodes("//terminalControl/confInfo/terminals/terminal");
                XmlNodeList nodes = xmldoc.SelectNodes("//terminalControl/confInfo/terminals/terminal");
                if (xmldoc.SelectSingleNode("//terminalControl/confInfo/status") != null) //ALLDEV-779
                    status = xmldoc.SelectSingleNode("//terminalControl/confInfo/status").InnerText; //ALLDEV-779
                String outXMLEP = "<Endpoints>";
                obj.BindAddressType(lstAddressType);
                foreach (XmlNode nodeEP in nodesEP)
                {
                    inXML = "<GetConferenceEndpoint>";
                    inXML += obj.OrgXMLElement();//Organization Module Fixes
                    inXML += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
                    inXML += "  <ConfID>" + lblConfID.Text + "</ConfID>";
                    //Blue Status Project Code Review
                    //if (hdnConfType.Value.Equals(ns_MyVRMNet.vrmConfType.P2P) && lblStatus.Text == "Ongoing" && Refreshchk.Checked == true) 
                    //    inXML += "  <EndpointID></EndpointID>";
                    //else
                        inXML += "  <EndpointID>" + nodeEP.SelectSingleNode("endpointID").InnerText + "</EndpointID>";
                    String tpe = "U";
                    if (nodeEP.SelectSingleNode("type").InnerText.Equals("2"))
                        tpe = "R";
                    if (nodeEP.SelectSingleNode("type").InnerText.Equals("4"))
                        tpe = "C";
                    if (isMonitorDMA == "" || isMonitorDMA == "0")
                    {
                        if (nodeEP.SelectSingleNode("isMonitorDMA") != null)
                            isMonitorDMA = nodeEP.SelectSingleNode("isMonitorDMA").InnerText.Trim();
                    }
                    if (RPRMMCU != 13) //RPRM MCU Type
                    {
                        if (nodeEP.SelectSingleNode("BridgeTypeId") != null)
                            int.TryParse(nodeEP.SelectSingleNode("BridgeTypeId").InnerText.Trim(), out RPRMMCU);
                    }
                    //Blue Status Project Code Review
                    //if (hdnConfType.Value.Equals(ns_MyVRMNet.vrmConfType.P2P) && lblStatus.Text == "Ongoing" && Refreshchk.Checked == true)
                    //    inXML += "  <Type></Type>";
                    //else
                    inXML += "  <Type>" + tpe + "</Type>";
                    //ALLDEV-814 - Start
                    string eptprofileid = "";
                    if (nodeEP.SelectSingleNode("EptProfileID") != null)
                        eptprofileid = nodeEP.SelectSingleNode("EptProfileID").InnerText.Trim();
                    inXML += "  <EptProfileID>" + eptprofileid + "</EptProfileID>";
                    //ALLDEV-814 - End

                    inXML += "</GetConferenceEndpoint>";
                    outXML = obj.CallMyVRMServer("GetConferenceEndpoint", inXML, Application["MyVRMserver_ConfigPath"].ToString());

                    if (outXML.IndexOf("<error>") < 0)
                    {
                        outXML = outXML.Replace("++", ",");  //FB 1640
                        //Response.Write(obj.Transfer(outXML));
                        xmldocEP.LoadXml(outXML);
                        //ZD 101133  Start
                        if (hdnConfType.Value.Equals(ns_MyVRMNet.vrmConfType.P2P) && lblStatus.Text == obj.GetTranslatedText("Ongoing"))
                        {
                            if (xmldocEP.SelectSingleNode("//GetConferenceEndpoint/Endpoint/Connect2").InnerText.Equals("1"))
                            {
                                if (xmldocEP.SelectSingleNode("//GetConferenceEndpoint/confmessage").InnerText.Equals("1"))
                                {
                                    //tdmessage.Attributes.Add("Style", "visibility:visible;"); ZD 101189
                                    //tdsendmessage.Attributes.Add("Style", "visibility:visible;");
                                    //SendMsgAll.Attributes.Add("Style", "visibility:visible;");
                                    tdmessage.Attributes.Add("Style", "display:none;");
                                    tdsendmessage.Attributes.Add("Style", "display:none;");
                                    SendMsgAll.Attributes.Add("Style", "display:none;");
                                }
                            }
                        }
                        //ZD 101133 End
                        outXMLEP += "<Endpoint>" + nodeEP.InnerXml;
                        outXMLEP += xmldocEP.SelectSingleNode("//GetConferenceEndpoint/Endpoint").InnerXml + "</Endpoint>";
                    }
                }

                outXMLEP += "</Endpoints>";
                //Response.Write(obj.Transfer(outXMLEP));
                //Response.End();
                xmldocEP = new XmlDocument();
                xmldocEP.LoadXml(outXMLEP);
                XmlTextReader xtr;
                DataSet ds = new DataSet();
                //FB 2400 start
                String adds = ""; 
                nodes = xmldocEP.SelectNodes("//Endpoints/Endpoint/MultiCodec");
                foreach (XmlNode node in nodes)
                {
                    adds = "";
                    XmlNodeList nodess = node.SelectNodes("Address");
                    Int32 i = 0;
                    foreach (XmlNode snode in nodess)
                    {
                        if (i > 0)
                            adds += "�";

                        adds += snode.InnerText;
                        i = i + 1;
                    }
                    node.InnerText = adds;
                }
                //FB 2400 End
                
                nodes = xmldocEP.SelectNodes("//Endpoints/Endpoint");
                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                DataView dv;
                DataTable dt;
                DataTable dtNew = null; //FB 2400

                if (ds.Tables.Count > 0)
                {
                    dv = new DataView(ds.Tables[0]);
                    dt = dv.Table;
                    if (!dt.Columns.Contains("ImageURL"))
                        dt.Columns.Add("ImageURL");
                    obj.BindVideoProtocols(lstProtocol);
                    bool adminStatus = true; //FB 1462
                    string strError = ""; //FB 1736
                    foreach (DataRow dr in dt.Rows)
                    {
                        try
                        {
                            dr["addressType"] = lstAddressType.Items.FindByValue(dr["addressType"].ToString()).Text;
                        }
                        catch (Exception ex)
                        {
                            dr["addressType"] = "Undefined";
                            log.Trace(ex.Message + " : " + ex.StackTrace);
                        }
                        try
                        {
                            dr["DefaultProtocol"] = "IP";
                            if (dr["BridgeAddressType"].ToString().Equals("4"))
                                dr["DefaultProtocol"] = "ISDN";
                            if (dr["BridgeAddressType"].ToString().Equals("5"))
                                dr["DefaultProtocol"] = "MPI";
                            //FB 2390 Start
                            if (dr["BridgeAddressType"].ToString().Equals("6"))
                                dr["DefaultProtocol"] = "SIP";
                            //FB 2390 End
                            dr["BridgeAddressType"] = lstAddressType.Items.FindByValue(dr["BridgeAddressType"].ToString()).Text;
                        }
                        catch (Exception ex)
                        {
                            dr["BridgeAddressType"] = "Undefined";
                            log.Trace(ex.Message + " : " + ex.StackTrace);
                        }

                        //switch (dr["addressType"].ToString())
                        //{
                        //    case "1":
                        //        dr["addressType"] = "IP Address";
                        //        break;
                        //    case "2":
                        //        dr["addressType"] = "H323 ID";
                        //        break;
                        //    case "3":
                        //        dr["addressType"] = "E.164";
                        //        break;
                        //    case "4":
                        //        dr["addressType"] = "ISDN Phone Number";
                        //        break;
                        //    case "5":
                        //        dr["addressType"] = "MPI";
                        //        break;
                        //    default:
                        //        dr["addressType"] = "Undefined";
                        //        break;
                        //}
                        //switch (dr["BridgeAddressType"].ToString())
                        //{
                        //    case "1":
                        //        dr["BridgeAddressType"] = "IP Address";
                        //        dr["DefaultProtocol"] = "IP";
                        //        break;
                        //    case "2":
                        //        dr["BridgeAddressType"] = "H323 ID";
                        //        dr["DefaultProtocol"] = "IP";
                        //        break;
                        //    case "3":
                        //        dr["BridgeAddressType"] = "E.164";
                        //        dr["DefaultProtocol"] = "IP";
                        //        break;
                        //    case "4":
                        //        dr["BridgeAddressType"] = "ISDN Phone Number";
                        //        dr["DefaultProtocol"] = "ISDN";
                        //        break;
                        //    default:
                        //        dr["BridgeAddressType"] = "Undefined";
                        //        dr["DefaultProtocol"] = "Undefined";
                        //        break;
                        //}
                        switch (dr["connectionType"].ToString())
                        {
                            case ns_MyVRMNet.vrmConnectionTypes.DialIn:
                                dr["connectionType"] = obj.GetTranslatedText("Dial-in to Conference"); //ZD 100619
                                break;
                            case ns_MyVRMNet.vrmConnectionTypes.DialOut:
                            case ns_MyVRMNet.vrmConnectionTypes.DialOutOld:
                                dr["connectionType"] = obj.GetTranslatedText("Dial-out to Location"); //ZD 100619
                                break;
                            case ns_MyVRMNet.vrmConnectionTypes.Direct:
                                dr["connectionType"] = obj.GetTranslatedText("Direct");
                                break;
                            default:  //FB 2093
                                dr["connectionType"] = obj.GetTranslatedText("Dial-in to Conference"); //ZD 100619
                                break;
                        }
                        //Code added by offshore for FB 1175 Start
                        if (dr["Name"].ToString() != "")
                        {
                            if(dr["Name"].ToString().Trim().StartsWith(","))
                                dr["Name"] = dr["Name"].ToString().Trim().Remove(0, 1);
                        }
                        //Code added by offshore for FB 1175 End

                        if (dr["EndpointName"].ToString().Trim().Equals(""))
                            dr["EndpointName"] = dr["Name"];

                        if (!dt.Columns.Contains("MCUName"))
                            dt.Columns.Add("MCUName");
                        if (!hdnConfType.Value.Equals(ns_MyVRMNet.vrmConfType.P2P)) //FB 1462
                        {
                            inXML = "";
                            inXML += "<login>";
                            inXML += obj.OrgXMLElement();//Organization Module Fixes
                            inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                            inXML += "</login>";
                            outXML = obj.CallMyVRMServer("GetBridgeList", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                            XmlDocument xmldocMCU = new XmlDocument();
                            xmldocMCU.LoadXml(outXML);
                            XmlNodeList nodesMCU = xmldocMCU.SelectNodes("//bridgeInfo/bridges/bridge");
                            
                            foreach (XmlNode node in nodesMCU)
                            {
                                //Response.Write(node.SelectSingleNode("ID").InnerText + " : " + dr["BridgeID"]);
                                if (node.SelectSingleNode("ID").InnerText.Trim().Equals(dr["BridgeID"].ToString().Trim()))
                                {
                                    //added for RSS Feed 
                                    dr["MCUName"] = "<a href='#' onclick=\"javascript:ViewBridgeDetails('" + node.SelectSingleNode("ID").InnerText + "');return false;\">" + node.SelectSingleNode("name").InnerText;

                                    /* ** Code Added For FB 1462- Start */

                                    if (node.SelectSingleNode("userstate") != null)
                                    {
                                        if (node.SelectSingleNode("userstate").InnerText == "I")
                                        {
                                            if (strError == "") //FB 1736
                                            {
                                                strError += "Error:MCU (" + node.SelectSingleNode("name").InnerText + ") Admin User - " + node.SelectSingleNode("administrator").InnerText.Trim() + " is In-Active. Please make sure the user is Active."; //FB 1736
                                            }
                                            else
                                            {
                                                if (!strError.Contains(node.SelectSingleNode("name").InnerText)) //FB 1736
                                                    strError += "<br>Error:MCU (" + node.SelectSingleNode("name").InnerText + ") Admin User - " + node.SelectSingleNode("administrator").InnerText.Trim() + " is In-Active. Please make sure the user is Active."; //FB 1736
                                            }

                                            adminStatus = false;
                                        }
                                        else if (node.SelectSingleNode("userstate").InnerText == "D")
                                        {
                                            if (strError == "") //FB 1736
                                            {
                                                //strError += "Error:MCU (" + node.SelectSingleNode("name").InnerText + ") admin User has been deleted"; //FB 1736
                                                strError += "Error:MCU (" + xmldoc.SelectSingleNode("//bridge/name").InnerText + ")" + obj.GetErrorMessage(430); //FB 1881
                                            }
                                            else
                                            {
                                                if (!strError.Contains(node.SelectSingleNode("name").InnerText)) //FB 1736
                                                    //strError += "<br>Error:MCU (" + node.SelectSingleNode("name").InnerText + ") admin User has been deleted"; //FB 1736
                                                    strError += "<br>Error:MCU (" + xmldoc.SelectSingleNode("//bridge/name").InnerText + ")" + obj.GetErrorMessage(430); //FB 1881
                                            }

                                            adminStatus = false;
                                        }
                                    }
                                    /* *** Code Added For FB 1462- End *** */
                                }
                            }
                        } //FB 1462
                        dr["BridgeAddress"] = dr["BridgePrefix"].ToString() + " " + dr["BridgeAddress"].ToString();
                        //dr["DefaultProtocol"] = lstProtocol.Items.FindByValue(dr["DefaultProtocol"].ToString()).Text;
                        if (dr["displayLayout"].ToString().Trim().Equals("") || dr["displayLayout"].ToString().Trim().Equals("0"))
                            dr["displayLayout"] = txtSelectedImage.Text;
                        int.TryParse(dr["displayLayout"].ToString(), out videoLayout);//ZD 103060

                        dr["ImageURL"] = ImagesPath.Text + Int32.Parse(dr["displayLayout"].ToString()).ToString("00") + ".gif";

                        //Code Added For FB 1371 - Start
                        strEndPointID = dr["endpointID"].ToString();
                        strType = dr["type"].ToString();
                        //Code Added For FB 1371 - End

                        //FB 2400 - Start
                        //In fututre,if Telespresence endpoint multicode address need to be shown as separate
                        /*if (dt.Columns.Contains("MultiCodec"))
                        {
                            if (dr["MultiCodec"].ToString() != "")
                            {
                                if (dtNew == null)
                                    dtNew = dt.Clone();

                                String address = "";
                                address = dr["MultiCodec"].ToString();

                                String[] strAdds = null;
                                
                                strAdds = address.Split('�');

                                for (int s = 0; s < strAdds.Length; s++)
                                {
                                    if (s == 0)
                                        dr["address"] = strAdds[s];
                                    else
                                    {
                                        DataRow drNew = dtNew.NewRow();
                                        drNew.ItemArray = dr.ItemArray;

                                        drNew["address"] = strAdds[s];
                                        dtNew.Rows.Add(drNew);
                                    }
                                }
                            }
                        } */
                        //FB 2400 - End
                    }
                    if (!dt.Columns.Contains("CascadeLinkId")) //FB 1650
                        dt.Columns.Add(new DataColumn("CascadeLinkId"));

                    if (!dt.Columns.Contains("EndPointStatus")) //FB 1650 - Endpoint status issue
                        dt.Columns.Add(new DataColumn("EndPointStatus"));

                    //FB 2400 - Start
                    if (dtNew != null && dtNew.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dtNew.Rows)
                        {
                            DataRow newRow = dt.NewRow();
                            newRow.ItemArray = dr.ItemArray;
                            dt.Rows.Add(newRow);
                        }
                    }
                    //FB 2400 - End                 

                    dgEndpoints.DataSource = dt;
                    dgEndpoints.DataBind();
                    // FB 2441 Starts
					//ZD 102289 Starts
                    DataTable tempdt = new DataTable();
                    tempdt.Merge(dt);
                    tempdt.DefaultView.RowFilter = "isMonitorDMA=1";
                    dgMuteALL.DataSource = tempdt.DefaultView;
					//ZD 102289 End
                    dgMuteALL.DataBind();
                    // FB 2441 Ends

                    //Code Added For FB 1462 //FB 1736 - start
                    if(strError.Trim() != "")
                    {
                        errLabel.Text = strError;
                        errLabel.Visible = true;
                    }
                    //Code Added For FB 1462 //FB 1736- end
                    CascadeLink = "";//FB 2528
                    foreach (DataGridItem dgi in dgEndpoints.Items) //FB Case 944, 936, 861 and 730 Saima
                    {
                        //Code added for RSS- Start
                        if (Request.QueryString["hf"] != null)
                            if (Request.QueryString["hf"].ToString().Equals("1"))
                            {
                                LinkButton btnCon = ((LinkButton)dgi.FindControl("btnCon"));//ZD 100602
                                LinkButton btnMute = ((LinkButton)dgi.FindControl("btnMute"));
                                LinkButton btnEdit = ((LinkButton)dgi.FindControl("btnEdit"));
                                LinkButton btnDelete = ((LinkButton)dgi.FindControl("btnDelete"));

                                if (btnCon != null)
                                    btnCon.Visible = false; //ZD 100602
                                btnMute.Visible = false;
                                btnEdit.Visible = false;
                                btnDelete.Visible = false;
                            }
                        //RSS End

                        if (RPRMMCU == 13)
                        {
                            LinkButton btnTempCon = ((LinkButton)dgi.FindControl("btnCon"));
                            LinkButton btnEdit = ((LinkButton)dgi.FindControl("btnEdit"));
                            btnTempCon.Visible = false;
                            btnEdit.Visible = false;
                            if (status != "5") //ALLDEV-779
                            {
                                tdExndTm.Attributes.Add("style", "visibility: hidden");//ZD 102997 
                                tdExndTmlbl.Attributes.Add("style", "visibility: hidden");//ZD 102997 //ALLDEV-779
                            }
                        }

                        //FB 2528 Starts
                        CascadeLink = dgi.Cells[1].Text;
                        if (CascadeLink != null)
                        {
                            if (CascadeLink == "4")
                            {
                                LinkButton btnMute = ((LinkButton)dgi.FindControl("btnMute"));
                                LinkButton btnEdit = ((LinkButton)dgi.FindControl("btnEdit"));
                                LinkButton btnDelete = ((LinkButton)dgi.FindControl("btnDelete"));
                                LinkButton btnTempCon = ((LinkButton)dgi.FindControl("btnCon"));
                                if (btnMute != null)
                                {
                                    btnMute.Enabled = false;
                                    btnMute.ForeColor = System.Drawing.Color.Gray;
                                    btnMute.Attributes.Remove("onclick");
                                }
                                if (btnEdit != null)
                                {
                                    btnEdit.Enabled = false;
                                    btnEdit.ForeColor = System.Drawing.Color.Gray;
                                }
                                if (btnDelete != null)
                                {
                                    btnDelete.Enabled = false;
                                    btnDelete.ForeColor = System.Drawing.Color.Gray;
                                    btnDelete.Attributes.Remove("onclick");
                                }
                                if (btnTempCon != null)
                                {
                                    btnTempCon.Enabled = false;
                                    btnTempCon.ForeColor = System.Drawing.Color.Gray;
                                }
                            }
                        }
                        //FB 2528 Ends

                        //FB 2441 Starts
                        if (isMonitorDMA != null && RPRMMCU == 13 && isMonitorDMA == "0") //ZD 103090
                        {
                            lnkMuteAllExcept.ForeColor = System.Drawing.Color.Gray;
                            lnkMuteAllExcept.OnClientClick = null;

                            lnkUnMuteAllParties.ForeColor = System.Drawing.Color.Gray;
                            lnkMuteAllExcept.Enabled = false;
                            lnkUnMuteAllParties.Enabled = false;

                            ConfLayoutSubmit.Disabled = true;
                            ConfLayoutSubmit.Style.Add("cursor", "pointer");

                            //txtExtendedTime.Enabled = false; //ALLDEV-779
                            //btnExtendEndtime.Enabled = false; //ALLDEV-779

                            LinkButton btnMute = ((LinkButton)dgi.FindControl("btnMute"));
                            LinkButton btnDelete = ((LinkButton)dgi.FindControl("btnDelete"));
                            if (btnMute != null)
                            {
                                btnMute.Enabled = false;
                                btnMute.ForeColor = System.Drawing.Color.Gray;
                                btnMute.Attributes.Remove("onclick");
                            }
                            if (btnDelete != null)
                            {
                                btnDelete.Enabled = false;
                                btnDelete.ForeColor = System.Drawing.Color.Gray;
                                btnDelete.Attributes.Remove("onclick");
                            }
                        }
                        //FB 2441 Ends

                        //FB 2400 Start
                        Label lblisTelepresence = ((Label)dgi.FindControl("lblisTelepresence"));
                        if (lblisTelepresence != null)
                        {
                            if (lblisTelepresence.Text == "1")
                            {
                                LinkButton btnEdit = ((LinkButton)dgi.FindControl("btnEdit"));
                                btnEdit.Enabled = false;
                            }
                        }
                        //FB 2400 End
                        //FB 2501 EM7 Starts
                        if (dgi.Cells[15].Text.Trim() != "")
                        {
                            if (dgi.Cells[15].Text.Trim() == "Active")
                            {
                                dgi.Cells[15].Text = obj.GetTranslatedText("Active");
                                dgi.Cells[15].ForeColor = System.Drawing.Color.Green;
                            }
                            else if (dgi.Cells[15].Text.Trim() == "InActive")//ZD 100825
                            {
                                dgi.Cells[15].Text = obj.GetTranslatedText("Inactive");
                                dgi.Cells[15].ForeColor = System.Drawing.Color.Red;
                            }
                            //ZD 100825 Start
                            else
                            {
                                dgi.Cells[15].Text = obj.GetTranslatedText("Not Monitored");//ZD 101839
                                dgi.Cells[15].ForeColor = System.Drawing.Color.DarkMagenta;
                            }
                            //ZD 100825 End
                        }
                        //FB 2501 EM7 Starts
                        if (!hdnConfType.Value.Equals(ns_MyVRMNet.vrmConfType.P2P))
                        {
                            String strConnectionStatus = dgi.Cells[21].Text.Trim(); //FB 1650 Endpoint status issue  //FB 2501 EM7 //FB 2980 //ZD 104256
                            //strConnectionStatus = ConnectStatusDetails(dgi.Cells[0].Text, dgi.Cells[1].Text); //code changed for FB 1371
                            //Code Added/Modified for FB 1371 and 1367 
                            if (dgi.ItemType.Equals(ListItemType.Item) || dgi.ItemType.Equals(ListItemType.AlternatingItem))
                            {
                                LinkButton btnTempCon = ((LinkButton)dgi.FindControl("btnCon"));
                                if (strConnectionStatus.Equals(ns_MyVRMNet.vrmEndPointConnectionSatus.Connected))
                                    btnTempCon.Text = obj.GetTranslatedText("Disconnect");//FB 1830 - Translation
                            }
                            switch (strConnectionStatus)
                            {
                                case ns_MyVRMNet.vrmEndPointConnectionSatus.disconnect:
                                    dgi.Cells[7].Text = obj.GetTranslatedText("Disconnected");//FB 1830 - Translation
                                    dgi.Cells[7].BackColor = System.Drawing.Color.Red;
                                    dgi.Cells[7].Font.Bold = true;
                                    break;
                                case ns_MyVRMNet.vrmEndPointConnectionSatus.Connecting:
                                    dgi.Cells[7].Text = obj.GetTranslatedText("Connecting");//FB 1830 - Translation
                                    dgi.Cells[7].BackColor = System.Drawing.Color.Yellow;
                                    dgi.Cells[7].Font.Bold = true;
                                    break;
                                case ns_MyVRMNet.vrmEndPointConnectionSatus.Connected:
                                    dgi.Cells[7].Text = obj.GetTranslatedText("Connected");//FB 1830 - Translation
                                    dgi.Cells[7].BackColor = System.Drawing.Color.LimeGreen; //code changed for FB 1371
                                    dgi.Cells[7].Font.Bold = true;
                                    break;
                                case "-1":
                                    dgi.Cells[7].Text = obj.GetTranslatedText("N/A");//FB 1830 - Translation
                                    dgi.Cells[7].BackColor = System.Drawing.Color.LightBlue;
                                    break;
                                default:
                                    dgi.Cells[7].Text = "";
                                    break;
                            }
                        }
                    }
                    
                    if (hdnConfStatus.Value.Equals(ns_MyVRMNet.vrmConfStatus.Ongoing))
                    {
                        //Response.Write("in if");
                        dgEndpoints.Columns[6].Visible = true;
                        dgEndpoints.Columns[9].Visible = true;
                        dgEndpoints.Columns[7].Visible = true;
                        

                        String strConnectionStatus = "";
                        bool isEndpointConnected = false;
                        foreach (DataGridItem dgi in dgEndpoints.Items)
                        {
                            strConnectionStatus = dgi.Cells[21].Text.Trim();//FB 2581 //FB 2980 //ZD 104256
							//ZD 100602 Starts
                            if (dgi.ItemType.Equals(ListItemType.Item) || dgi.ItemType.Equals(ListItemType.AlternatingItem))
                            {
                                LinkButton  btnTemp = ((LinkButton)dgi.FindControl("btnCon"));
                                btnTemp.Visible = true;

                                btnTemp = ((LinkButton)dgi.FindControl("btnMute"));
                                if (btnTemp != null && strConnectionStatus == ns_MyVRMNet.vrmEndPointConnectionSatus.Connected)//FB 2581 //ZD 100672
                                {
                                    btnTemp.Visible = true;
                                    if (!isEndpointConnected)
                                        isEndpointConnected = true;

                                }
							//ZD 100602 End
                            }
                        }

                        //FB 2441
                        if (isEndpointConnected && RPRMMCU == 13)
                        {
                            lnkMuteAllExcept.Visible = true;
                            lnkUnMuteAllParties.Visible = true;
                        }
                        GetConferenceAlerts();
                    }
                    //added for RSS Feed - Start
                    if (Request.QueryString["hf"] != null)
                        if (Request.QueryString["hf"].ToString().Equals("1"))
                        {
                            dgEndpoints.Columns[15].Visible = false;
                            dgEndpoints.Columns[16].Visible = false;
                            btnAddNewEndpoint.Visible = false;
                            tblTerminalControl.Visible = false;
                        }
                    //RSS Feed - End

                    //ZD 100672 START //ZD 101388
                    if ((Request.QueryString["t"] != null && Request.QueryString["t"].ToString().Equals("hf")) || (ViewState["hasManage"] != null && ViewState["hasManage"].ToString().Equals("0")))
                    {
                        dgEndpoints.Columns[19].Visible = false; //ZD 104256
                    }
                    
                    //ZD 100672 END

                   
                    if (hdnConfType.Value.Equals(ns_MyVRMNet.vrmConfType.P2P))
                    {
                        // Code Modified For FB 1422 - Start
                        //foreach (DataRow dr in dt.Rows)
                        //{
                        //    dr["address"] = "<a href='http://" + dr["address"] + "' target='_blank'>" + dr["address"] + "</a>";
                        //}

                        foreach (DataRow dr in dt.Rows)
                        {
                            if (Convert.ToString(dr["connect2"]) == "1")
                                dr["connect2"] = obj.GetTranslatedText("Caller");
                            else if (Convert.ToString(dr["connect2"]) == "0")
                                dr["connect2"] = obj.GetTranslatedText("Callee");
                            else
                                dr["connect2"] = "";
                        }
                        
                        //if (!dt.Columns.Contains("EndPointStatus")) //Blue Status Project
                        //    dt.Columns.Add(new DataColumn("EndPointStatus"));

                        if (!dt.Columns.Contains("remoteEndpoint")) //Blue Status Project
                            dt.Columns.Add(new DataColumn("remoteEndpoint"));

                        // Code Modified For FB 1422 - End
                        dgP2PEndpoints.DataSource = dt;
                        dgP2PEndpoints.DataBind();
                        tblP2PEndpoints.Visible = true;
                        //Blue Point Status START
                        String remEp = "";
                        foreach (DataGridItem dgi in dgP2PEndpoints.Items) 
                        {
                            if(dgi.Cells[12].Text.Trim() != "")
                            p2pStatus = dgi.Cells[12].Text.Trim();

                            if (dgi.Cells[13].Text.Trim() != "")
                                remEp = dgi.Cells[13].Text.Trim();

                            if (dgi.Cells[13].Text.Trim().Contains("&nbsp;"))
                                remEp = "";
                            //FB 2501 EM7 Starts
                            if (dgi.Cells[14].Text.Trim() != "")
                            {
                                if (dgi.Cells[14].Text.Trim() == "Active")
                                {
                                    dgi.Cells[14].Text = obj.GetTranslatedText("Active");
                                    dgi.Cells[14].ForeColor = System.Drawing.Color.Green;
                                }
                                else if (dgi.Cells[14].Text.Trim() == "InActive")//ZD 100825
                                {
                                    dgi.Cells[14].Text = obj.GetTranslatedText("Inactive");
                                    dgi.Cells[14].ForeColor = System.Drawing.Color.Red;
                                }
                                //ZD 100825 Start
                                else
                                {
                                    dgi.Cells[14].Text = obj.GetTranslatedText("Not Monitored");//ZD 101839
                                    dgi.Cells[14].ForeColor = System.Drawing.Color.DarkMagenta;
                                }
                                //ZD 100825 End
                            }
                            //FB 2501 EM7 Ends
                            
                             if (dgi.ItemType.Equals(ListItemType.Item) || dgi.ItemType.Equals(ListItemType.AlternatingItem))
                             {
                                 LinkButton btConnect = ((LinkButton)dgi.FindControl("btnConP2P"));
                                 HyperLink lnkmsg = ((HyperLink)dgi.FindControl("btnMessage"));
                                 switch (p2pStatus)
                                 {
                                     case ns_MyVRMNet.vrmEndPointConnectionSatus.disconnect:
                                         dgi.Cells[10].Text = obj.GetTranslatedText("Disconnected");//FB 1830 - Translation
                                         dgi.Cells[10].BackColor = System.Drawing.Color.Red;
                                         dgi.Cells[10].Font.Bold = true;
                                         btConnect.Text = obj.GetTranslatedText("Connect");//FB 1830 - Translation
                                         lnkmsg.Attributes.Add("style", "display:none");
                                         if (remEp != "")
                                         {
                                             Transtxt = obj.GetTranslatedText("Disconnected"); //ZD 100288
                                             dgi.Cells[10].Attributes.Add("onmouseover", "javascript:ShowEpID('" + dgi.Cells[10].ClientID + "','" + remEp + "');return false;");
                                             dgi.Cells[10].Attributes.Add("onmouseout", "javascript:ShowEpID('" + dgi.Cells[10].ClientID + "','" + Transtxt + "');return false;"); //ZD 100288
                                         }
                                         break;
                                     case ns_MyVRMNet.vrmEndPointConnectionSatus.Connecting:
                                         dgi.Cells[10].Text =  obj.GetTranslatedText("Connecting");//Translated Text
                                         dgi.Cells[10].BackColor = System.Drawing.Color.Yellow;
                                         dgi.Cells[10].Font.Bold = true;
                                         if (remEp != "")
                                         {
                                             Transtxt = obj.GetTranslatedText("Connecting"); //ZD 100288
                                             dgi.Cells[10].Attributes.Add("onmouseover", "javascript:ShowEpID('" + dgi.Cells[10].ClientID + "','" + remEp + "');return false;");
                                             dgi.Cells[10].Attributes.Add("onmouseout", "javascript:ShowEpID('" + dgi.Cells[10].ClientID + "','" + Transtxt + "');return false;"); //ZD 100288
                                         }
                                         break;
                                     case ns_MyVRMNet.vrmEndPointConnectionSatus.Connected:
                                         dgi.Cells[10].Text = obj.GetTranslatedText("Connected");//Translated Text
                                         dgi.Cells[10].BackColor = System.Drawing.Color.LimeGreen;
                                         dgi.Cells[10].Font.Bold = true;
                                         if (remEp != "")
                                         {
                                             Transtxt = obj.GetTranslatedText("Connected"); //ZD 100288
                                             dgi.Cells[10].Attributes.Add("onmouseover", "javascript:ShowEpID('" + dgi.Cells[10].ClientID + "','" + remEp + "');return false;");
                                             dgi.Cells[10].Attributes.Add("onmouseout", "javascript:ShowEpID('" + dgi.Cells[10].ClientID + "','" + Transtxt + "');return false;"); //ZD 100288

                                         }
                                         btConnect.Text = obj.GetTranslatedText("Disconnect");//Translated Text
                                         break;
                                     case ns_MyVRMNet.vrmEndPointConnectionSatus.Online:
                                         dgi.Cells[10].Text = obj.GetTranslatedText("Online");//Translated Text
                                         dgi.Cells[10].BackColor = System.Drawing.Color.FromArgb(32,108,255);
                                         if (remEp != "")
                                         {
                                             Transtxt = obj.GetTranslatedText("Online"); //ZD 100288
                                             dgi.Cells[10].Attributes.Add("onmouseover", "javascript:ShowEpID('" + dgi.Cells[10].ClientID + "','" + remEp + "');return false;");
                                             dgi.Cells[10].Attributes.Add("onmouseout", "javascript:ShowEpID('" + dgi.Cells[10].ClientID + "','" + Transtxt + "');return false;"); //ZD 100288
                                         }
                                         dgi.Cells[10].Font.Bold = true;
                                         break;
                                     case "-1":
                                         dgi.Cells[10].Text = obj.GetTranslatedText("Unreachable.");//Translated Text
                                         btConnect.Attributes.Add("style", "display:none");
                                         lnkmsg.Attributes.Add("style", "display:none");
                                         break;
                                     default:
                                         dgi.Cells[10].Text = obj.GetTranslatedText("Status being updated.");//Translated Text
                                         btConnect.Attributes.Add("style", "display:none");
                                         lnkmsg.Attributes.Add("style", "display:none");
                                         break;
                                 }
                             }
                        }
                        //Blue Point Status End
                    }
                    //Code added for PSU Fix FB 1354
                    //ZD 101388 - If View is not selected in Menu, then hide all things.
                    if ((Request.QueryString["t"] != null && Request.QueryString["t"].ToString().Equals("hf")) || (ViewState["hasManage"] != null && ViewState["hasManage"].ToString().Equals("0")))
                    {
                        dgEndpoints.Columns[15].Visible = true; //FB 2501 EM7
                        dgEndpoints.Columns[16].Visible = false;
                        btnAddNewEndpoint.Visible = false;
                        tblTerminalControl.Visible = false;
                    }
                    //Code added for PSU Fix FB 1354
                }
                else
                {
                    tblNoEndpoints.Visible = true;
                    //Code added for PSU Fix FB 1354
                    //ZD 101388 - If View is not selected in Menu, then hide all things.
                    if ((Request.QueryString["t"] != null && Request.QueryString["t"].ToString().Equals("hf")) || (ViewState["hasManage"] != null && ViewState["hasManage"].ToString().Equals("0")))
                    {
                        btnAddNewEndpoint.Visible = false;
                        tblTerminalControl.Visible = false;
                    }
                    
                    //Code added for PSU Fix FB 1354

                    //Code added for RSS Feed
                    if (Request.QueryString["hf"] != null)
                        if (Request.QueryString["hf"].Equals("1"))
                        {
                            btnAddNewEndpoint.Visible = false;
                            tblTerminalControl.Visible = false;
                        }
                    //Code added for RSS Feed

                    // Code Added For FB 1422 - Start
                    if (hdnConfType.Value.Equals(ns_MyVRMNet.vrmConfType.P2P))
                        btnAddNewEndpoint.Visible = false;
                    // Code Added For FB 1422 - End

                }
                //FB 2598 Starts EnableEM7
                if (enableEM7 != null)
                {
                    if (enableEM7 == "0")
                    {
                        dgEndpoints.Columns[15].Visible = false;
                        dgP2PEndpoints.Columns[14].Visible = false;
                    }
                    else
                    {
                        dgEndpoints.Columns[15].Visible = true;
                        dgP2PEndpoints.Columns[14].Visible = true;
                    }
                }
                //FB 2598 Ends

            }
            catch (Exception ex)
            {
                log.Trace("LoadEndpoints" + ex.Message);//ZD 100263
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                errLabel.Visible = true;
            }
        }
        protected void GetConferenceAlerts()
        {
            try
            {
                /*
                 * <Alerts>
  <Alert>
  	<AlertID>12</AlertID>
	<Message>Test message</Message>
	<Timestamp>01:20:23pm</Timestamp>
  </Alert>
  <Alert>
  	<AlertID>12</AlertID>
	<Message>Test message</Message>
	<Timestamp>01:20:23pm</Timestamp>
  </Alert>
  <Alert>
  	<AlertID>12</AlertID>
	<Message>Test message</Message>
	<Timestamp>01:20:23pm</Timestamp>
  </Alert>
</Alerts>
                 * */
                tblAlerts.Visible = true;
                String confid = lblConfID.Text;
                String insID = "1";
                if (lblConfID.Text.IndexOf(",") >= 0)
                {
                    confid = lblConfID.Text.Split(',')[0];
                    insID = lblConfID.Text.Split(',')[1];
                }
                String inXML = "";
                inXML += "<GetConferenceAlerts>";
                inXML += "  <ConferenceID>" + confid + "</ConferenceID>";
                inXML += "  <InstanceID>" + insID + "</InstanceID>";
                if( lblConfType.Text == obj.GetTranslatedText("Point-To-Point Conference")) //ZD 100522
                    inXML += "  <P2PEvents>1</P2PEvents>";

                inXML += "</GetConferenceAlerts>";

                String outXML = obj.CallCOM2("GetConferenceAlerts", inXML, Application["RTC_ConfigPath"].ToString());
                //String outXML = "<Alerts>  <Alert>  	<AlertID>12</AlertID>	<Message>Test message</Message>	<Timestamp>01:20:23pm</Timestamp>  </Alert>  <Alert>  	<AlertID>12</AlertID>	<Message>Test message</Message>	<Timestamp>01:20:23pm</Timestamp>  </Alert>  <Alert>  	<AlertID>12</AlertID>	<Message>Test message</Message>	<Timestamp>01:20:23pm</Timestamp>  </Alert></Alerts>";
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                XmlNodeList nodes = xmldoc.SelectNodes("//Alerts/Alert");
                if (nodes.Count > 0)
                {
                    XmlTextReader xtr;
                    DataSet ds = new DataSet();

                    foreach (XmlNode node in nodes)
                    {
                        xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                        ds.ReadXml(xtr, XmlReadMode.InferSchema);
                    }
                    DataTable dt = ds.Tables[0];
                    dgAlerts.DataSource = dt;
                    dgAlerts.DataBind();
                }
                else
                {
                    dgAlerts.Visible = false; //ZD 102515
                    lblNoAlerts.Visible = true;
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }

        protected void InitializeEndpoints(Object sender, DataGridItemEventArgs e)
        {
            try
            {
                //Response.Write(e.Item.ItemType);
                if (e.Item.ItemType.Equals(ListItemType.Item) || e.Item.ItemType.Equals(ListItemType.AlternatingItem))
                {
                    ((LinkButton)e.Item.FindControl("btnDelete")).Attributes.Add("onclick"
                        , "var isconf =confirm('" + obj.GetTranslatedText("Are you sure you want to delete this endpoint?")
                        + "'); if(isconf == true) {DataLoading(1);return true;} else return false;");
                    ((LinkButton)e.Item.FindControl("btnMute")).Attributes.Add("onclick", "var ismute = confirm('" + obj.GetTranslatedText("Are you sure you want to mute/unmute this endpoint?")
                        + "'); if(ismute == true) {DataLoading(1);return true;} else return false;");
                    //Code Added For FB 1367 - Nathira - Start
                    
                    //Code Added For FB 1367 - Nathira - End
                    if (e.Item.Cells[3].Text.Equals("1"))
                    {
                        ((LinkButton)e.Item.FindControl("btnMute")).Text = obj.GetTranslatedText("Unmute");//FB 1830 - Translation // ZD 103628
                        lnkUnMuteAllParties.Enabled = true;//FB 2441
                    }
                    else
                        ((LinkButton)e.Item.FindControl("btnMute")).Text = obj.GetTranslatedText("Mute");//FB 2441

                    Button btnTemp = (Button)e.Item.FindControl("btnChangeEndpointLayout");
                    btnTemp.Attributes.Add("onclick", "javascript:managelayout('" + ((Image)e.Item.FindControl("imgVideoLayout")).ClientID + "','" + e.Item.Cells[0].Text + "','" + e.Item.Cells[1].Text + "');return false;");
                }
            }
            catch (Exception ex)
            {
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace("InitializeEndpoints" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }

        

       

        //Code Added For FB 1371-Nathira - Start
        private String ConnectStatusDetails(String strEndPointID1, String strType1)
        {
            String strConStatus = "";
            String inXML = "";
            String outXMLNew = "";
            inXML += "<GetTerminalStatus>";
            inXML += "  <login>";
            inXML += "      <userID>" + Session["userID"].ToString() + "</userID>";
            inXML += "      <confID>" + lblConfID.Text + "</confID>";
            inXML += "      <endpointID>" + strEndPointID1 + "</endpointID>";
            inXML += "      <terminalType>" + strType1 + "</terminalType>";
            inXML += "  </login>";
            inXML += "</GetTerminalStatus>";
            outXMLNew = obj.CallCOM2("GetTerminalStatus", inXML, Application["RTC_ConfigPath"].ToString());

            if (outXMLNew.IndexOf("<error>") >= 0)
            {
                strConStatus = "-1";
            }
            else
            {
                Session.Add("confid", lblConfID.Text);
                XmlDocument xmldoc2 = new XmlDocument();
                xmldoc2.LoadXml(outXMLNew);
                strConStatus = xmldoc2.SelectSingleNode("//GetTerminalStatus/connectionStatus").InnerText;
            }
            return strConStatus;

        }
        //Code Added For FB 1371-Nathira - End
        protected void ChangeVideoDisplay()
        {
            try
            {
                String inXML = "";
                inXML += "<login>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                if (Request.QueryString["t"].ToString().Equals("5"))
                {
                    inXML += "  <confID>" + lblConfID.Text + "</confID>";
                    inXML += "  <endpointID>0</endpointID>";
                    inXML += "  <terminalType>0</terminalType>";
                    inXML += "  <displayLayout>" + txtSelectedImage.Text + "</displayLayout>";
                    inXML += "  <displayLayoutAll>1</displayLayoutAll>";//FB 2530
                }
                else if (Request.QueryString["t"].ToString().Equals("6"))
                {
                    inXML += "  <confID>" + lblConfID.Text + "</confID>";
                    inXML += "  <endpointID>" + txtEndpointType.Text.Split(',')[1] + "</endpointID>";
                    inXML += "  <terminalType>" + txtEndpointType.Text.Split(',')[0] + "</terminalType>";
                    inXML += "  <displayLayout>" + txtSelectedImage.Text + "</displayLayout>";
                    inXML += "  <displayLayoutAll>2</displayLayoutAll>";//FB 2530
                }
                inXML += "</login>";
                //Response.Write(obj.Transfer(inXML));
                // <login> <userID>11</userID> <confID>15</confID> <endpointID></endpointID> <terminalType></terminalType> <displayLayout>01</displayLayout></login>
                String outXML = obj.CallCOM2("DisplayTerminal", inXML, Application["RTC_ConfigPath"].ToString());
                errLabel.Visible = true;
                if (outXML.IndexOf("<error>") >= 0)
                {
                    if (Request.QueryString["t"].Equals("5"))
                    {
                        txtSelectedImage.Text = txtTempImage.Text;
                    }
                    if (Request.QueryString["t"].Equals("6"))
                    {
                        txtSelectedImageEP.Text = txtTempImage.Text;
                    }
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                }
                else
                {
                    //outXML = obj.CallCOM("DisplayTerminal", inXML, Application["COM_ConfigPath"].ToString());
                    outXML = obj.CallMyVRMServer("DisplayTerminal", inXML, Application["MyVRMServer_ConfigPath"].ToString());//FB 2027
                    Session.Add("confid", lblConfID.Text);
                    // FB 118 Start
                    //Response.Redirect("ManageConference.aspx?m=1&t=");
                    errLabel.Text = obj.GetTranslatedText("Operation Successful!");
                    errLabel.Visible = true;
                    Menu22.Items[1].Selected = true;
                    Menu22.Items[1].Enabled = true;
                    dgEndpoints.Enabled = true;
                    // FB 118 End
                }
            }
            catch (Exception ex)
            {
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace("ChangeVideoDisplay" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }
        protected void ExtendEndtime(Object sender, EventArgs e)
        {
            try
            {
                string extTime = "0"; //FB 1562

                if (hdnConfType.Value.Equals(ns_MyVRMNet.vrmConfType.P2P)) //FB 1562
                    extTime = txtPExtTime.Text.Trim();
                else
                    extTime = txtExtendedTime.Text.Trim();

                String inXML = "<login>" + obj.OrgXMLElement() + //Organization Module Fixes
                "<userID>" + Session["userID"].ToString() + "</userID>" +
                "<confInfo><confID>" + lblConfID.Text + "</confID><retry>0</retry>" +
                "<extendEndTime>" + extTime + "</extendEndTime>" + //FB 1562
                "</confInfo></login>";
                // <login> <userID>11</userID> <confID>15</confID> <endpointID></endpointID> <terminalType></terminalType> <displayLayout>01</displayLayout></login>

                //ZD 100819 Start
                String outXML = obj.CallMyVRMServer("CheckForExtendTimeConflicts", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                errLabel.Visible = true;
                if (outXML.IndexOf("<error>") >= 0)
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                else
                {
                    outXML = obj.CallCOM2("SetTerminalControl", inXML, Application["RTC_ConfigPath"].ToString()); //Fb 1521  //Extend Time Fix
                    errLabel.Visible = true;
                    if (outXML.IndexOf("<error>") >= 0)
                        errLabel.Text = obj.ShowErrorMessage(outXML);
                    else
                    {
                        outXML = obj.CallMyVRMServer("SetTerminalControl", inXML, Application["MyVRMServer_ConfigPath"].ToString());    //Fb 1521 //Extend Time Fix  
                        Session.Add("confid", lblConfID.Text);
                        // FB 118 Start
                        //Response.Redirect("ManageConference.aspx?m=1&t=");
                        errLabel.Text = obj.GetTranslatedText("Operation Successful!");
                        errLabel.Visible = true;
                        Menu22.Items[1].Selected = true;
                        Menu22.Items[1].Enabled = true;
                        Menu22.Items[1].Selectable = true;//ZD 101978 
                        dgEndpoints.Enabled = true;
                        // FB 118 End
                    }
                }
                //ZD 100819 End

                txtPExtTime.Text = ""; //FB 1562
                txtExtendedTime.Text = "";
            }
            catch (Exception ex)
            {
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace("ExtendEndTime" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }

        protected void MuteEndpoint(Object sender, DataGridCommandEventArgs e)
        {
            try
            {
                String inXML = "";
                inXML += "<login>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                inXML += "  <confID>" + lblConfID.Text + "</confID>";
                inXML += "  <endpointID>" + e.Item.Cells[0].Text + "</endpointID>";
                inXML += "  <terminalType>" + e.Item.Cells[1].Text + "</terminalType>";
                inXML += " <EptProfileID>" + e.Item.Cells[22].Text + "</EptProfileID>"; //ALLDEV-814
                if (e.Item.Cells[3].Text.Equals("1"))
                    inXML += "  <mute>0</mute>";
                else
                    inXML += "  <mute>1</mute>";
                inXML += "  <muteAll>1</muteAll>";//FB 2530
                inXML += "</login>";
                //Response.Write(obj.Transfer(inXML));
                // <login> <userID>11</userID> <confID>15</confID> <endpointID></endpointID> <terminalType></terminalType> <displayLayout>01</displayLayout></login>
                String outXML = obj.CallCOM2("MuteTerminal", inXML, Application["RTC_ConfigPath"].ToString());
                errLabel.Visible = true;
                if (outXML.IndexOf("<error>") >= 0)
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                else
                {
                    //outXML = obj.CallCOM("MuteTerminal", inXML, Application["COM_ConfigPath"].ToString());
                    outXML = obj.CallMyVRMServer("MuteTerminal", inXML, Application["MyVRMServer_ConfigPath"].ToString());//FB 2027
                    Session.Add("confid", lblConfID.Text);
                    //Response.Redirect("ManageConference.aspx?m=1&t=");
                    //FB Case 903 - Saima starts here
                    DisplayvideoLayout(videoLayout);//ZD 101931
                    LoadEndpoints();
                    errLabel.Text = obj.GetTranslatedText("Operation Successful!");//FB 1830 - Translation
                    errLabel.Visible = true;
                    //FB Case 903 - Saima ends here
                }
            }
            catch (Exception ex)
            {
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace("MuteEndpoint" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }
        //Code Added For FB 1367-Nathira - Start
        protected void ConnectEndpoint(Object sender, DataGridCommandEventArgs e)
        {
            try
            {
                String inXML = "";
                inXML += "<login>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                inXML += "  <confID>" + lblConfID.Text + "</confID>";
                inXML += "  <endpointID>" + e.Item.Cells[0].Text + "</endpointID>";
                inXML += "  <EptProfileID>" + e.Item.Cells[22].Text + "</EptProfileID>"; //ALLDEV-814
                inXML += "  <terminalType>" + e.Item.Cells[1].Text + "</terminalType>";
                if (e.Item.Cells[7].Text.Equals("Connected"))
                    inXML += "  <connectOrDisconnect>0</connectOrDisconnect>";
                else
                    inXML += "  <connectOrDisconnect>1</connectOrDisconnect>";
                inXML += "</login>";
                //Response.Write(obj.Transfer(inXML));
                // <login> <userID>11</userID> <confID>15</confID> <endpointID></endpointID> <terminalType></terminalType> <displayLayout>01</displayLayout></login>
                String outXML = obj.CallCOM2("ConnectDisconnectTerminal", inXML, Application["RTC_ConfigPath"].ToString());
                errLabel.Visible = true;
                if (outXML.IndexOf("<error>") >= 0)
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                else
                {
                    //outXML = obj.CallCOM("ConnectDisconnectTerminal", inXML, Application["COM_ConfigPath"].ToString()); //FB 2027 - No such command
                    Session.Add("confid", lblConfID.Text);
                    //Response.Redirect("ManageConference.aspx?m=1&t=");
                    //FB Case 903 - Saima starts here
                    DisplayvideoLayout(videoLayout);//ZD 101931
                    LoadEndpoints();
                    errLabel.Text = obj.GetTranslatedText("Operation Successful!");//FB 1830 - Translation
                    errLabel.Visible = true;
                    //FB Case 903 - Saima ends here
                }
            }
            catch (Exception ex)
            {
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace("ConnectEndpoint" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }
        //Code Added For FB 1367-Nathira - End

       
        protected void DeleteTerminal(Object sender, DataGridCommandEventArgs e)
        {
            try
            {
                //FB 2528 Starts
                StringBuilder inXML = new StringBuilder();
                String outXML = "";
                if (e.Item.Cells[7].Text.Equals("Connected"))
                {
                    inXML.Append("<login>");
                    inXML.Append(obj.OrgXMLElement());
                    inXML.Append("<userID>" + Session["userID"].ToString() + "</userID>");
                    inXML.Append("<confID>" + lblConfID.Text + "</confID>");
                    inXML.Append("<endpointID>" + e.Item.Cells[0].Text + "</endpointID>");
                    inXML.Append("<EptProfileID>" + e.Item.Cells[22].Text + "</EptProfileID>"); //ALLDEV-814
                    inXML.Append("<terminalType>" + e.Item.Cells[1].Text + "</terminalType>");
                    inXML.Append("<connectOrDisconnect>0</connectOrDisconnect>");
                    inXML.Append("</login>");
                    outXML = obj.CallCOM2("ConnectDisconnectTerminal", inXML.ToString(), Application["RTC_ConfigPath"].ToString());
                }
                //FB 2528 Ends

                //FB 2027 - Starts
                inXML = new StringBuilder();
                outXML = ""; //FB 2528
                inXML.Append("<login>");
                inXML.Append(obj.OrgXMLElement());//Organization Module Fixes
                inXML.Append("<userID>" + Session["userID"].ToString() + "</userID>");
                inXML.Append("<confID>" + lblConfID.Text + "</confID>");
                inXML.Append("<endpointID>" + e.Item.Cells[0].Text + "</endpointID>");
                inXML.Append("<EptProfileID>" + e.Item.Cells[22].Text + "</EptProfileID>"); //ALLDEV-814
                inXML.Append("<terminalType>" + e.Item.Cells[1].Text + "</terminalType>");
                inXML.Append("</login>");
                log.Trace(inXML.ToString());
                //Response.Write(obj.Transfer(inXML));
                // <login> <userID>11</userID> <confID>15</confID> <endpointID></endpointID> <terminalType></terminalType> <displayLayout>01</displayLayout></login>
                //ZD 101285 Start
                if (hdnConfStatus.Value == ns_MyVRMNet.vrmConfStatus.Ongoing)
                {
                    outXML = obj.CallCOM2("DeleteTerminal", inXML.ToString(), Application["RTC_ConfigPath"].ToString());
                    errLabel.Visible = true;
                    if (outXML.IndexOf("<error>") >= 0)
                        errLabel.Text = obj.ShowErrorMessage(outXML);
                }

                if (outXML.IndexOf("<error>") < 0)
                {
                    //outXML = obj.CallCOM("DeleteTerminal", inXML, Application["COM_ConfigPath"].ToString());
                    outXML = obj.CallMyVRMServer("DeleteTerminal", inXML.ToString(), Application["MyVRMServer_Configpath"].ToString());
                    //FB 2027 - End
                    Session.Add("confid", lblConfID.Text);

                    if (e.Item.Cells[1].Text.Trim().Equals("2"))// FB 1675
                    {
                        String inxml = "<DeleteCiscoICAL>" + obj.OrgXMLElement();
                        inxml += "<UserID>" + Session["userID"].ToString() + "</UserID>";
                        inxml += "<ConfID>" + lblConfID.Text + "</ConfID>";
                        inxml += "  <endpointID>" + e.Item.Cells[0].Text + "</endpointID>";
                        inxml += "</DeleteCiscoICAL>";

                        
                        obj.CallCommand("DeleteCiscoICAL", inxml);

                    }
                    Response.Redirect("ManageConference.aspx?m=1&ep=1&t="); //FB 118
                }
                //ZD 101285 End
            }
            catch (Exception ex)
            {
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace("DeleteTerminal" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }
        protected void AddNewEndpoint(Object sender, EventArgs e)
        {
            try
            {
                //ZD 100602 Starts

                //selectedloc - Doubt - Pending
                //confID - Done
                //stDate - Conferen Start - Done - Testing pending for different timezone
                //enDate - Conferen End - Done - Testing pending for different timezone
                //tzone  - Conference Timezone - Done
                //serType - Conference Service Type - Done
                        //isVMR - No need as we dont display the 'Add New Endpoint' button. 
                //CloudConf  - IsCloudConference - Done
                        //immediate - No need.
                //ConfType  - hdnConfType.Value 

                //string Date = lblSetupDur.Text.Split(' ')[0];
                //string Time = lblSetupDur.Text.Split(' ')[1];
                //DateTime dStart = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(Date) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(Time));
                
                //Date = lblTearDownDur.Text.Split(' ')[0];
                //Time = lblTearDownDur.Text.Split(' ')[1];
                //DateTime dEnd = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(Date) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(Time));

                Session.Remove("stDate");
                Session.Remove("enDate");
                Session.Remove("tzone");
                Session.Remove("cloudConf");
                Session.Remove("confType");
                Session.Remove("confServiceType");

                Session.Add("stDate", hdnConfStart.Value);
                Session.Add("enDate", hdnConfEnd.Value);
                Session.Add("tzone", hdnTimeZoneId.Value); //Will split all by comma after done work with Call Monitor.
                Session.Add("cloudConf", hdnCloudConf.Value);
                Session.Add("confType", hdnConfType.Value);
                Session.Add("confServiceType", hdnServiceType.Value);
                
                //ZD 100602 End

                Session.Add("confid", lblConfID.Text);
                if (Session["EndpointID"] == null)
                    Session.Add("EndpointID", "new");
                else
                    Session["EndpointID"] = "new";
                Session["ConfID"] = lblConfID.Text;
                //Response.Redirect("AddTerminalEndpoint.aspx?epid=new&cid=" + lblConfID.Text + "&tpe=U");
                //Response.Redirect("EndpointList.aspx?t=TC");  //FB 1552
                Response.Redirect("AddNewEndpoint.aspx"); //FB 1552
            }
            catch (Exception ex)
            {
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace("AddNewEndpoint" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }
        protected void EditEndpoint(Object sender, DataGridCommandEventArgs e)
        {
            try
            {
                string epid = e.Item.Cells[0].Text.Trim(); //FB 1650
                String tpe = e.Item.Cells[1].Text.Trim();
                if (e.Item.Cells[1].Text.Trim().Equals("2"))
                    tpe = "R";
                if (e.Item.Cells[1].Text.Trim().Equals("1"))
                    tpe = "U";

                if (e.Item.Cells[1].Text.Trim().Equals("4")) //FB 1650
                {
                    epid = e.Item.Cells[17].Text.Trim(); //FB 1650
                }
                else
                {
                    epid = e.Item.Cells[0].Text.Trim(); //FB 1650
                }

                if (Session["EndpointID"] == null) //FB 1650
                    Session.Add("EndpointID", epid);
                else
                    Session["EndpointID"] = epid;

                string profileID = e.Item.Cells[22].Text.Trim(); //ALLDEV-814

                Response.Redirect("AddTerminalEndpoint.aspx?t=&tpe=" + tpe + "&pid=" + profileID); //ALLDEV-814
            }
            catch (System.Threading.ThreadAbortException) {} //FB 1650
            catch (Exception ex)
            {
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace("EditEndpoint" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }
        protected void CheckResourceAvailability()
        {
            try
            {
                List<string> confBridges = new List<string>();   //FB 1937   
                obj.BindBridges(lstBridges);
				//FB 1938
                //int[] countV, countA;
                //countV = new int[lstBridges.Items.Count];
                //countA = new int[lstBridges.Items.Count];
                //This DataTable is created to display Yes/No for sufficient Audio and Video ports for that bridge under a conference.
                DataTable dt = new DataTable();
                dt.Columns.Add("BridgeID");
                dt.Columns.Add("BridgeName");
                dt.Columns.Add("AudioPorts");
                dt.Columns.Add("VideoPorts");
                dt.Columns.Add("CardsAvailable");
                dt.Columns.Add("Description");

                // GetAdvancedAVSettings gets called to get all the endpoint information for a conference.
                String inXMLConf = "<GetAdvancedAVSettings><UserID>" + Session["userID"].ToString() + "</UserID>" + obj.OrgXMLElement() + "<ConfID>" + lblConfID.Text + "</ConfID></GetAdvancedAVSettings>";//Organization Module Fixes
                String outXMLConf = obj.CallMyVRMServer("GetAdvancedAVSettings", inXMLConf, Application["MyVRMServer_Configpath"].ToString());
                //Response.Write(obj.Transfer(outXMLConf)+"<hr>");
                //if ASPIL does not return any error then proceed.
                if (outXMLConf.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXMLConf);
                    XmlNodeList nodes = xmldoc.SelectNodes("//GetAdvancedAVSettings/Endpoints/Endpoint");
                    //For all the endpoints, we check if it is a User. Endpoint can either be a user or a room.
                    foreach (XmlNode node in nodes)
                    {
                        //if (node.SelectSingleNode("Type").InnerText.ToString().ToUpper().Equals("U"))
                       // {
                            for (int i = 0; i < lstBridges.Items.Count; i++)
                            {
                                //FB 1937 start
                                if (lstBridges.Items[i].Value.Equals(node.SelectSingleNode("BridgeID").InnerText.Trim()))
                                {
                                    if (!confBridges.Contains(node.SelectSingleNode("BridgeID").InnerText.Trim()))
                                        confBridges.Add(node.SelectSingleNode("BridgeID").InnerText.Trim());

                                    /*if (node.SelectSingleNode("Connection").InnerText.Equals("3"))
                                        countV[i]++;
                                    //if the bridge ID is same as the one for which we are checking against and the type is Audio-Only (2)
                                    if (node.SelectSingleNode("Connection").InnerText.Equals("2"))
                                        countA[i]++;*/
                                }
                                /*
                                //if the bridge ID is same as the one for which we are checking against and the type is Audio/Video (3)
                                if (lstBridges.Items[i].Value.Equals(node.SelectSingleNode("BridgeID").InnerText) && node.SelectSingleNode("Connection").InnerText.Equals("3"))
                                    countV[i]++;
                                //if the bridge ID is same as the one for which we are checking against and the type is Audio-Only (2)
                                if (lstBridges.Items[i].Value.Equals(node.SelectSingleNode("BridgeID").InnerText) && node.SelectSingleNode("Connection").InnerText.Equals("2"))
                                    countA[i]++;
                                */
                                //FB 1937 end
                            }
                        //}
                    }
                    // Generate a dropdown box for both audio and video ports used by this conference for all bridges
                    /*for (int i = 0; i < countV.Length; i++)
                        lstBridgeCountV.Items.Add(new ListItem(countV[i].ToString(), lstBridges.Items[i].Value));
                    for (int i = 0; i < countV.Length; i++)
                        lstBridgeCountA.Items.Add(new ListItem(countA[i].ToString(), lstBridges.Items[i].Value));*/
                }
                // for all bridges we need to call the command to get the available Audio/Video ports to match with the dropdown that we just created in above steps.
               
                for (int i = 0; i < lstBridges.Items.Count; i++)
                {
                    if (!lstBridges.Items[i].Value.Equals("-1"))
                    {
                        DataRow dr = dt.NewRow();
                        dr["BridgeID"] = lstBridges.Items[i].Value;
                        dr["BridgeName"] = lstBridges.Items[i].Text;

                        String inXML = "";
                        inXML += "<GetMCUAvailableResources>";
                        inXML += obj.OrgXMLElement();//Organization Module Fixes
                        inXML += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
                        inXML += "  <BridgeID>" + dr["BridgeID"].ToString() + "</BridgeID>";
                        inXML += "  <ConfID>" + lblConfID.Text + "</ConfID>";
                        //Code changed by Offshore for FB Issue 1073 -- Start
                        //inXML += "  <StartDate>" + lblConfDate.Text + "</StartDate>";
                        inXML += "  <StartDate>" + myVRMNet.NETFunctions.GetDefaultDate(lblConfDate.Text) + "</StartDate>";
                        //Code changed by Offshore for FB Issue 1073 -- End
                        inXML += "  <StartTime>" + lblConfTime.Text + "</StartTime>";
                        inXML += "  <Duration>" + hdnConfDuration.Text + "</Duration>";
                        inXML += "</GetMCUAvailableResources>";
                        String outXML = obj.CallMyVRMServer("GetMCUAvailableResources", inXML, Application["MyVRMServer_ConfigPath"].ToString());

                        if (outXML.IndexOf("<error>") < 0)
                        {
                            XmlDocument xmldoc1 = new XmlDocument();
                            xmldoc1.LoadXml(outXML);

                            // based on available numbers, we display yes or no against each bridge name
                            //FB 1937 start
                            if ((Int32.Parse(xmldoc1.SelectSingleNode("//MCUResources/AudioOnly/Available").InnerText) < 0))// - Int32.Parse(xmldoc1.SelectSingleNode("//MCUResources/AudioOnly/Available").InnerText)) >= countA[i]) FB 1937
                            {
                                dr["AudioPorts"] = obj.GetTranslatedText("No");
                                if ((Int32.Parse(xmldoc1.SelectSingleNode("//MCUResources/AudioOnly/Available").InnerText) <= -1000))// - Int32.Parse(xmldoc1.SelectSingleNode("//MCUResources/AudioOnly/Available").InnerText)) >= countA[i]) FB 1937
                                {
                                    dr["AudioPorts"] = obj.GetTranslatedText("N/A");
                                }
                            }
                            else
                            {
                                dr["AudioPorts"] = obj.GetTranslatedText("Yes");
                            }

                            if ((Int32.Parse(xmldoc1.SelectSingleNode("//MCUResources/AudioVideo/Available").InnerText) < 0))// Int32.Parse(xmldoc1.SelectSingleNode("//MCUResources/AudioVideo/Available").InnerText)) >= countV[i]) FB 1937
                            {
                                dr["VideoPorts"] = obj.GetTranslatedText("No"); 
                            }
                            else
                            {
                                dr["VideoPorts"] = obj.GetTranslatedText("Yes");
                            }
                            //FB 1937 end
                            dr["Description"] = xmldoc1.SelectSingleNode("//MCUResources/Description").InnerText.Trim(); //FB 1937

                        }
                        else
                        {
                            errLabel.Text = obj.ShowErrorMessage(outXML);
                            errLabel.Visible = true;
                        }
						//FB 1937
                        if (confBridges.Contains(lstBridges.Items[i].Value))
                        {
                            if (dr["AudioPorts"].ToString() == obj.GetTranslatedText("No") || dr["VideoPorts"].ToString() == obj.GetTranslatedText("No"))
                            {
                                dr["BridgeName"] = "<span style='color:Red'>" + dr["BridgeName"] + "</span>";
                                dr["CardsAvailable"] = "<span style='color:Red'>" + dr["CardsAvailable"] + "</span>";
                                dr["Description"] = "<span style='color:Red'>" + obj.GetTranslatedText(dr["Description"].ToString()) + "</span>";
                                dr["AudioPorts"] = "<span style='color:Red'>" + dr["AudioPorts"] + "</span>";
                                dr["VideoPorts"] = "<span style='color:Red'>" + dr["VideoPorts"] + "</span>";
                            }
                            else
                            {
                                dr["BridgeName"] = "<span style='color:green'>" + dr["BridgeName"] + "</span>";
                                dr["CardsAvailable"] = "<span style='color:green'>" + dr["CardsAvailable"] + "</span>";
                                dr["Description"] = "<span style='color:green'>" + dr["Description"] + "</span>";
                                dr["AudioPorts"] = "<span style='color:green'>" + dr["AudioPorts"] + "</span>";
                                dr["VideoPorts"] = "<span style='color:green'>" + dr["VideoPorts"] + "</span>";
                            }
                            
                        }

                        dt.Rows.Add(dr);
                    }
                }
                dgBridgeResources.DataSource = dt;
                dgBridgeResources.DataBind();
				//Code COmmented for FB 1937
                //foreach (DataGridItem dgi in dgBridgeResources.Items)
                //{
                //    if (dgi.Cells[2].Text.Trim().ToUpper().Equals("NO"))
                //    {
                //        dgi.Cells[2].ForeColor = System.Drawing.Color.Red;
                //        dgi.Cells[2].Font.Bold = true;
                //    }
                //    if (dgi.Cells[3].Text.Trim().ToUpper().Equals("NO"))
                //    {
                //        dgi.Cells[3].Font.Bold = true;
                //        dgi.Cells[3].ForeColor = System.Drawing.Color.Red;
                //    }
                //}
            }
            catch (Exception ex)
            {
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace("CheckResourceAvailablity" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }

        /*  Blue Status Project START**/
        /*
         protected String GetP2pStatus()
         {
             String stus = "";
             XmlDocument doc = null;
             try 
             {
                
                      String inXML = "";
                 inXML += "<login>";
                 inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                 inXML += "  <confID>" + lblConfID.Text + "</confID>";
                 inXML += "  <endpointID></endpointID>";
                 inXML += "  <terminalType></terminalType>";
                 inXML += "</login>";
                 //Response.Write(obj.Transfer(inXML));
                 // <login> <userID>11</userID> <confID>15</confID> <endpointID></endpointID> <terminalType></terminalType> <displayLayout>01</displayLayout></login>
                 String outXML = obj.CallCOM2("GetP2PConfStatus", inXML, Application["RTC_ConfigPath"].ToString());
                 errLabel.Visible = true;
                 if (outXML.IndexOf("<error>") >= 0)
                     errLabel.Text = obj.ShowErrorMessage(outXML);
                 else
                 {
                     doc = new XmlDocument();
                     doc.LoadXml(outXML);
                     if (doc.SelectSingleNode("GetP2PConfStatus/connectionStatus") != null)
                         stus = doc.SelectSingleNode("GetP2PConfStatus/connectionStatus").InnerText;
                 }
 
             }
             catch (Exception ex)
             {
                 errLabel.Text = ex.StackTrace;
                 errLabel.Visible = true; 
             }

             return stus;
         }//Code added for P2P status
         
         * **/
        /**Blue Status Project End**/
        protected void SendP2PMessage(Object sender, DataGridCommandEventArgs e)//Code added for P2P status
        {
            String text = "";
            try
            {
                System.Web.UI.HtmlControls.HtmlInputText msg = (System.Web.UI.HtmlControls.HtmlInputText)e.Item.FindControl("TxtMessageBox");

                if (msg != null)
                    text = msg.Value;

                String inXML = "";
                inXML += "<login>";
                inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                inXML += "  <confID>" + lblConfID.Text + "</confID>";
                inXML += "  <endpointID>" + e.Item.Cells[0].Text + "</endpointID>";
                inXML += "  <terminalType>" + e.Item.Cells[1].Text + "</terminalType>";
                inXML += " <EptProfileID>" + e.Item.Cells[22].Text + "</EptProfileID>"; //ALLDEV-814
                inXML += "  <messageText>" + text + "</messageText>";
                inXML += "</login>";
                //Response.Write(obj.Transfer(inXML));
                // <login> <userID>11</userID> <confID>15</confID> <endpointID></endpointID> <terminalType></terminalType> <displayLayout>01</displayLayout></login>
                String outXML = obj.CallCOM2("SendMessageToEndpoint", inXML, Application["RTC_ConfigPath"].ToString());
                errLabel.Visible = true;
                if (outXML.IndexOf("<error>") >= 0)
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                else
                {
                    errLabel.Text = obj.GetTranslatedText("Operation Successful!");//FB 1830 - Translation
                    errLabel.Visible = true;
                    //FB Case 903 - Saima ends here
                }
            }
            catch (Exception ex)
            {
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace("SendP2PMessages" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }

         protected void BroadCastP2PMsg(Object sender, EventArgs e)
        {
            try
            {
               

                String inXML = "";
                inXML += "<login>";
                inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                inXML += "  <confID>" + lblConfID.Text + "</confID>";
                inXML += "  <messageText>"+ TxtMessageBoxAll.Value +"</messageText>";
                inXML += "</login>";
                String outXML = obj.CallCOM2("SendMessageToConference", inXML, Application["RTC_ConfigPath"].ToString());
                errLabel.Visible = true;
                if (outXML.IndexOf("<error>") >= 0)
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                else
                {
                    errLabel.Text = obj.GetTranslatedText("Operation Successful!");//FB 1830 - Translation
                    errLabel.Visible = true;
                }
            
                
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace("BroadcastP2PMSg" + ex.Message);//ZD 100263
            }
        }

         protected void InitializeP2PEndpoints(Object sender, DataGridItemEventArgs e)
         {
             try
             {
                 //Response.Write(e.Item.ItemType);
                 if (e.Item.ItemType.Equals(ListItemType.Item) || e.Item.ItemType.Equals(ListItemType.AlternatingItem))
                 {

                     HyperLink eptWeb = (HyperLink)e.Item.FindControl("EptWebsite");
                     System.Web.UI.HtmlControls.HtmlTableRow msgTR = (System.Web.UI.HtmlControls.HtmlTableRow)e.Item.FindControl("Messagediv");
                     System.Web.UI.HtmlControls.HtmlInputButton canclTR = (System.Web.UI.HtmlControls.HtmlInputButton)e.Item.FindControl("btnClose");
                     System.Web.UI.HtmlControls.HtmlInputText txtmsg = (System.Web.UI.HtmlControls.HtmlInputText)e.Item.FindControl("TxtMessageBox");
                     HyperLink eptMonitor = (HyperLink)e.Item.FindControl("EptMonitor");

                     
                     if (eptWeb != null)
                     {
                         eptWeb.Text = e.Item.Cells[5].Text;
                         eptWeb.Attributes.Add("onclick", "javascript:fnOpenEpt('" + e.Item.Cells[5].Text + "')");
                     }

                     if (eptMonitor != null)
                         eptMonitor.Attributes.Add("onclick", "javascript:fnOpenRemote('" + e.Item.Cells[5].Text + "')");

                     if (lblStatus.Text == "Ongoing")
                     {

                         LinkButton btConnect = (LinkButton)e.Item.FindControl("btnConP2P");
                         Button btnms = (Button)e.Item.FindControl("SendMsg");
                         HyperLink lnkmsg = (HyperLink)e.Item.FindControl("btnMessage");
                         lnkmsg.Visible = false; // ZD 101189
                         HyperLink lnkmonit = (HyperLink)e.Item.FindControl("EptMonitor");
                         lnkmonit.Visible = false;

                         //btConnect.Attributes.Add("onclick", "return confirm('Are you sure you want to Connect/disconnect this endpoint?')");
                         btConnect.Attributes.Add("onclick", "return confirm('" + obj.GetTranslatedText("Are you sure you want to Connect/disconnect this endpoint ?") + "')"); //FB japnese
                         if (obj.GetTranslatedText("Callee") == e.Item.Cells[9].Text) // ZD 101189
                             btConnect.Style.Add("visibility", "hidden");

                         if (lnkmsg != null && msgTR != null)
                                 lnkmsg.Attributes.Add("onclick", "javascript:fnOpenMsg('"+ msgTR.ClientID +"','1')"); 

                         if (canclTR != null && msgTR != null)
                             canclTR.Attributes.Add("onclick", "javascript:fnOpenMsg('" + msgTR.ClientID + "','0')");

                         if (btnms != null && txtmsg != null)
                                 btnms.Attributes.Add("onclick", "javascript:return fnchkValue('" + txtmsg.ClientID + "')");

                         //switch (p2pStatus)//Blue Status Project
                         //{
                         //    case ns_MyVRMNet.vrmEndPointConnectionSatus.disconnect:
                         //        e.Item.Cells[10].Text = "Disconnected";
                         //        e.Item.Cells[10].BackColor = System.Drawing.Color.Red;
                         //        e.Item.Cells[10].Font.Bold = true;
                         //        btConnect.Text = "Connect";
                         //        lnkmsg.Attributes.Add("style", "display:none");
                         //        break;
                         //    case ns_MyVRMNet.vrmEndPointConnectionSatus.Connecting:
                         //        e.Item.Cells[10].Text = "Connecting";
                         //        e.Item.Cells[10].BackColor = System.Drawing.Color.Yellow;
                         //        e.Item.Cells[10].Font.Bold = true;
                         //        break;
                         //    case ns_MyVRMNet.vrmEndPointConnectionSatus.Connected:
                         //        e.Item.Cells[10].Text = "Connected";
                         //        e.Item.Cells[10].BackColor = System.Drawing.Color.LimeGreen;
                         //        e.Item.Cells[10].Font.Bold = true;
                         //        btConnect.Text = "Disconnect";
                         //        break;
                         //    case ns_MyVRMNet.vrmEndPointConnectionSatus.Online:
                         //        e.Item.Cells[10].Text = "Online";
                         //        e.Item.Cells[10].BackColor = System.Drawing.Color.Blue;
                         //        e.Item.Cells[10].Font.Bold = true;
                         //        break;
                         //    case "-1":
                         //        e.Item.Cells[10].Text = "Status not available.";
                         //        btConnect.Attributes.Add("style", "display:none");
                         //        lnkmsg.Attributes.Add("style", "display:none");
                         //        break;
                         //    default:
                         //        e.Item.Cells[10].Text = "Status being updated.";
                         //        btConnect.Attributes.Add("style", "display:none");
                         //        lnkmsg.Attributes.Add("style", "display:none");
                         //        break;
                         //}
                     }
                     else
                     {
                         dgP2PEndpoints.Columns[10].Visible = false;
                         dgP2PEndpoints.Columns[11].Visible = false;
                     }
                 }
             }
             catch (Exception ex)
             {
                 errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                 log.Trace("IntializeP2pEndpoints" + ex.Message);//ZD 100263
                 errLabel.Visible = true;
             }
         }//Code added for P2p Status

         protected void ConnectP2PEndpoint(Object sender, DataGridCommandEventArgs e)//Code added for P2P status
        {
            try
            {
                String inXML = "";
                inXML += "<login>";
                inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                inXML += "  <confID>" + lblConfID.Text + "</confID>";
                inXML += "  <endpointID>" + e.Item.Cells[0].Text + "</endpointID>";
                inXML += "  <terminalType>" + e.Item.Cells[1].Text + "</terminalType>";
                if (e.Item.Cells[10].Text.Equals("Connected"))
                    inXML += "  <connectOrDisconnect>0</connectOrDisconnect>";
                else
                    inXML += "  <connectOrDisconnect>1</connectOrDisconnect>";
                inXML += "</login>";
                //Response.Write(obj.Transfer(inXML));
                // <login> <userID>11</userID> <confID>15</confID> <endpointID></endpointID> <terminalType></terminalType> <displayLayout>01</displayLayout></login>
                String outXML = obj.CallCOM2("ConnectDisconnectTerminal", inXML, Application["RTC_ConfigPath"].ToString());
                errLabel.Visible = true;
                if (outXML.IndexOf("<error>") >= 0)
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                else
                {
                    errLabel.Text = obj.GetTranslatedText("Operation Successful!");//FB 1830 - Translation
                    errLabel.Visible = true;
                    //FB Case 903 - Saima ends here
                }
            }
            catch (Exception ex)
            {
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace("ConnectP2PEndpoint" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }

         /* ****  FB 2288 Starts  ****/
         # region AV_ItemDataBound

         protected void AV_ItemDataBound(object sender, DataGridItemEventArgs e)
         {
             try
             {
                 if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
                 {
                     e.Item.Cells[11].Enabled = false;
                     if (hdnConfStatus.Value == ns_MyVRMNet.vrmConfStatus.Pending || hdnConfStatus.Value == ns_MyVRMNet.vrmConfStatus.Scheduled)
                         e.Item.Cells[11].Enabled = true;
                 }
             }
             catch (Exception ex)
             {
                 errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                 errLabel.Visible = true;
             }
         }

         #endregion

         # region CAT_ItemDataBound

         protected void CAT_ItemDataBound(object sender, DataGridItemEventArgs e)
         {
             try
             {
                 if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
                 {
                     e.Item.Cells[09].Enabled = false;
                     if (hdnConfStatus.Value == ns_MyVRMNet.vrmConfStatus.Pending || hdnConfStatus.Value == ns_MyVRMNet.vrmConfStatus.Scheduled)
                         e.Item.Cells[09].Enabled = true;
                 }
             }
             catch (Exception ex)
             {
                 errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                errLabel.Visible = true;
             }
         }

         #endregion

         # region HK_ItemDataBound

         protected void HK_ItemDataBound(object sender, DataGridItemEventArgs e)
         {
             try
             {
                 if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
                 {
                     e.Item.Cells[10].Enabled = false;
                     if (hdnConfStatus.Value == ns_MyVRMNet.vrmConfStatus.Pending || hdnConfStatus.Value == ns_MyVRMNet.vrmConfStatus.Scheduled)
                         e.Item.Cells[10].Enabled = true;
                 }
             }
             catch (Exception ex)
             {
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                errLabel.Visible = true;
             }
         }

         #endregion
         /* ****  FB 2288 Ends  ****/

         /* *** Custom Attribute Fixes - start *** */
         #region FillCustomAttributeTable

         protected void FillCustomAttributeTable()
         {
             XmlDocument xmlDOC = new XmlDocument();
             xmlDOC.LoadXml(Session["outxml"].ToString());
             XmlNode node = (XmlNode)xmlDOC.DocumentElement;

             XmlNodeList nodes = node.SelectNodes("//conference/confInfo/CustomAttributesList/CustomAttribute");

             if (nodes == null)
             {
                 lbl_CustOpt.Text = obj.GetTranslatedText("(No custom options)");//FB 1830 - Translation
                 lblNoCustOption.Visible = true;
             }
             else
             {
                 if (nodes.Count > 0)
                 {
                     if (CAObj == null)
                         CAObj = new myVRMNet.CustomAttributes();

                     Table customAtts = CAObj.DisplayCustomOptions(nodes);
                     
                     if (customAtts != null)
                     {
                         if (customAtts.Rows.Count > 0)
                             tdCustOpt.Controls.Add(customAtts);
                         else
                             lblNoCustOption.Visible = true;
                     }
                     else
                     {
                         lbl_CustOpt.Text = obj.GetTranslatedText("(No custom options)");//FB 1830 - Translation
                         lblNoCustOption.Visible = true;
                     }
                 }
                 else
                 {
                     lbl_CustOpt.Text = obj.GetTranslatedText("(No custom options)");//FB 1830 - Translation
                     lblNoCustOption.Visible = true;
                 }
             }
         }
         #endregion
        /* *** Custom Attribute Fixes - end *** */

         //FB 1958
         #region ShowHostDetails
         /// <summary>
         /// Bind data to be printed
         /// </summary>
         protected void ShowHostDetails()
         {
             try
             {
                 Control HostDetails = LoadControl(ResolveUrl(@"ViewUserDetails.ascx"));
                 HostDetailHolder.Controls.Add(HostDetails);
                 en_ViewUserDetails userDetail = HostDetailHolder.Controls[0] as en_ViewUserDetails;
                 userDetail.BindUserData();
             }
             catch (Exception ex)
             {
                 log.Trace("ShowHostDetails" + ex.Message);
                 errLabel.Text = obj.ShowSystemMessage();
                 errLabel.Visible = true;
             }
         }

         #endregion 
        //ALLDEV-857
         #region ShowSchedulerDetails
         protected void ShowSchedulerDetails()
         {
             try
             {
                 Control SchedulerDetails = LoadControl(ResolveUrl(@"ViewUserDetails.ascx"));
                 SchedulerDetailHolder.Controls.Add(SchedulerDetails);
                 en_ViewUserDetails userDetail = SchedulerDetailHolder.Controls[0] as en_ViewUserDetails;
                 userDetail.BindRequesterData();
             }
             catch (Exception ex)
             {
                 log.Trace("ShowHostDetails" + ex.Message);
                 errLabel.Text = obj.ShowSystemMessage();
                 errLabel.Visible = true;
             }
         }

         #endregion 
         // Method added for FB 1552
         #region dgEndpoints_ItemCreated
         /// <summary>
         /// dgEndpoints_ItemCreated
         /// </summary>
         /// <param name="sender"></param>
         /// <param name="e"></param>
         protected void dgEndpoints_ItemCreated(object sender, DataGridItemEventArgs e) //FB 1552
         {

             if (e.Item.ItemType == ListItemType.Footer)
                 ((Button)e.Item.FindControl("Button1")).Visible = false;
         }
         #endregion

         //FB 1985 - Starts
         #region fnShowEndpoint
         /// <summary>
         /// fnShowEndpoint
         /// </summary>
         /// <param name="sender"></param>
         /// <param name="e"></param>
         protected void fnShowEndpoint(Object sender, EventArgs e)
         {
             if (LnkAVExpand.Text.ToUpper() == "EXPAND")
             {
                 LnkAVExpand.Text = obj.GetTranslatedText("Collapse"); //FB 2272
                 if (Menu22.Items[1] != null)
                 {
                     //Menu22.Items[1].Text = "&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;Endpoints&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                     Menu22.Items[1].Text = "<div align='center' valign='middle' style='width:123' onclick='javascript:DataLoading(1);'>Endpoints</div><br>"; //FB 3067 //ZD 100429
                     Menu22.Items[1].Enabled = true;
                     dgEndpoints.Enabled = true;
                 }
             }
             else
             {
                 LnkAVExpand.Text = obj.GetTranslatedText("Expand");
                 if (Menu22.Items[1] != null)
                 {
                     Menu22.Items[1].Text = String.Empty;
                     Menu22.Items[1].Enabled = false;
                     dgEndpoints.Enabled = false;
                 }
                 CheckResourceAvailability();

                 if (NormalView != null)
                     MultiView1.SetActiveView(NormalView);
                 if (Menu22.Items[2] != null)
                 {
                     Menu22.Items[2].Selected = true;
                 }
             }
         }
         #endregion
        //FB 1985 - End


         //FB 2023
         #region partyGridBound
        /// <summary>
         /// partyGridBound
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void partyGridBound(Object sender, DataGridItemEventArgs e)
         {
             try
             {
                 if (e.Item.ItemType.Equals(ListItemType.Item) || e.Item.ItemType.Equals(ListItemType.AlternatingItem))
                 {
                     if (e.Item.Cells[2].Text.Trim().Equals("") || e.Item.Cells[2].Text.Trim().Equals("&nbsp;"))
                     {
                         e.Item.Cells[3].Text = "";
                         e.Item.Cells[6].Enabled = false;
                     }
                 }
             }
             catch (Exception ex)
             {
                 errLabel.Text = obj.ShowSystemMessage();
                 log.Trace(ex.StackTrace);
                 errLabel.Visible = true;
             }
         }
        #endregion

        //FB 2441 Starts
        protected void btnMuteAllExcept(Object sender, EventArgs e)
        {
            try
            {
                StringBuilder inXML = new StringBuilder();
                inXML.Append("<login>");
                inXML.Append(obj.OrgXMLElement());
                inXML.Append("<userID>" + Session["userID"].ToString() + "</userID>");
                inXML.Append("<confID>" + lblConfID.Text + "</confID>");
                inXML.Append("<mode>1</mode>"); //0-UnMuteAllParties 1-MuteAllPartiesExcept
                inXML.Append("<endpoints>");

                for (int i = 0; i < dgMuteALL.Items.Count; i++)
                {
                    CheckBox btnMute = ((CheckBox)dgMuteALL.Items[i].FindControl("chk_muteall"));
                    if (btnMute.Checked)
                    {
                        inXML.Append("<endpoint>");
                        inXML.Append("<endpointId>" + dgMuteALL.Items[i].Cells[0].Text + "</endpointId>");
                        inXML.Append("<terminalType>" + dgMuteALL.Items[i].Cells[1].Text + "</terminalType>");
                        inXML.Append("</endpoint>");
                    }
                }
                inXML.Append("</endpoints>");
                inXML.Append("</login>");

                String outXML = obj.CallCOM2("MuteUnMuteParties", inXML.ToString(), Application["RTC_ConfigPath"].ToString());
                errLabel.Visible = true;
                if (outXML.IndexOf("<error>") >= 0)
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                else
                {
                    outXML = obj.CallMyVRMServer("MuteUnMuteParticipants", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                    Session.Add("confid", lblConfID.Text);
                    DisplayvideoLayout(videoLayout);//ZD 101931
                    LoadEndpoints();
                    errLabel.Text = obj.GetTranslatedText("Operation Successful!");
                    errLabel.Visible = true;
                }
            }
            catch (Exception ex)
            {
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace(ex.StackTrace);
                errLabel.Visible = true;
            }
        }


        #region UnMuteAllParties
        /// <summary>
        /// UnMuteAllParties
        /// </summary>
        /// <param name="src"></param>
        /// <param name="e"></param>
        protected void UnMuteAllParties(Object src, EventArgs e)
        {
            try
            {
                StringBuilder inXML = new StringBuilder();
                String outXML = "";
                inXML.Append("<login>");
                inXML.Append(obj.OrgXMLElement());
                inXML.Append("<userID>" + Session["userID"].ToString() + "</userID>");
                inXML.Append("<confID>" + lblConfID.Text + "</confID>");
                inXML.Append("<mode>0</mode>"); //0-UnMuteAllParties 1-MuteAllPartiesExcept
                inXML.Append("</login>");

                outXML = obj.CallCommand("MuteUnMuteParties", inXML.ToString());
                errLabel.Visible = true;

                if (outXML.IndexOf("<error>") >= 0)
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                else
                {
                    outXML = obj.CallMyVRMServer("MuteUnMuteParticipants", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                    Session.Add("confid", lblConfID.Text);
                    LoadEndpoints();
                    errLabel.Text = obj.GetTranslatedText("Operation Successful!");
                    errLabel.Visible = true;
                }
            }
            catch (Exception ex)
            {
                log.Trace("UnMuteAllParties" + ex.Message);//ZD 100263
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                errLabel.Visible = true;
            }
        }
        #endregion
        
        //FB 2441 Ends

        // ZD 100221 Starts
        #region Btn_WebEXStart
        protected void Btn_WebEXStart(object sender, EventArgs e)
        {
            try
            {
                string script = "", URL = "";
                if (!string.IsNullOrEmpty(hdnWebExHostURL.Value))
                    URL = hdnWebExHostURL.Value;
                else
                    URL = hdnWebExPartyURL.Value;

                script = "<script type='text/javascript'>window.open('" + URL + "')</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "myVRM WebEX", script, false);
            }
            catch (Exception ex)
            {
                log.Trace("Webex" + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;
            }

        }
        #endregion
        // ZD 100221 Ends

        //ZD 101931 start
        #region
        /// <summary>
        /// DisplayvideoLayout
        /// </summary>
        /// <param name="videoLayout"></param>
        protected void DisplayvideoLayout(int videoLayout)
        {
            if (videoLayout >= 100)
            {
                if (videoLayout == 101)
                {
                    imgVideoLayout.Style.Add("display", "none");
                    //imgVideoLayout.ImageUrl = "image/displaylayout/05.gif";
                    imgLayoutMapping6EP.Style.Add("display", "");
                    imgLayoutMapping6EP.ImageUrl = "image/displaylayout/06.gif";
                    imgLayoutMapping7EP.Style.Add("display", "");
                    imgLayoutMapping7EP.ImageUrl = "image/displaylayout/07.gif";
                    imgLayoutMapping8EP.Style.Add("display", "");
                    imgLayoutMapping8EP.ImageUrl = "image/displaylayout/44.gif";
                    lblCodianLOEP.Text = obj.GetTranslatedText("Family 1");

                    imgVideoDisplay.Style.Add("display", "");
                    imgVideoDisplay.ImageUrl = "image/displaylayout/05.gif";
                    //imgLayoutMapping6.Style.Add("display", "");
                    //imgLayoutMapping6.ImageUrl = "image/displaylayout/06.gif";
                    //imgLayoutMapping7.Style.Add("display", "");
                    //imgLayoutMapping7.ImageUrl = "image/displaylayout/07.gif";
                    //imgLayoutMapping8.Style.Add("display", "");
                    //imgLayoutMapping8.ImageUrl = "image/displaylayout/44.gif";
                    lblCodianLO.Text = obj.GetTranslatedText("Family 1");
                }
                else if (videoLayout == 102)
                {
                    imgVideoLayout.Style.Add("display", "none");
                    //imgVideoLayout.ImageUrl = "image/displaylayout/01.gif";
                    imgLayoutMapping6EP.Style.Add("display", "none");
                    imgLayoutMapping7EP.Style.Add("display", "none");
                    imgLayoutMapping8EP.Style.Add("display", "none");
                    lblCodianLOEP.Text = obj.GetTranslatedText("Family 2");

                    imgVideoDisplay.Style.Add("display", "");
                    imgVideoDisplay.ImageUrl = "image/displaylayout/01.gif";
                    //imgLayoutMapping6.Style.Add("display", "none");
                    //imgLayoutMapping7.Style.Add("display", "none");
                    //imgLayoutMapping8.Style.Add("display", "none");
                    lblCodianLO.Text = obj.GetTranslatedText("Family 2");
                }
                else if (videoLayout == 103)
                {
                    imgVideoLayout.Style.Add("display", "none");
                    //imgVideoLayout.ImageUrl = "image/displaylayout/02.gif";
                    imgLayoutMapping6EP.Style.Add("display", "none");
                    imgLayoutMapping7EP.Style.Add("display", "none");
                    imgLayoutMapping8EP.Style.Add("display", "none");
                    lblCodianLOEP.Text = obj.GetTranslatedText("Family 3");

                    imgVideoDisplay.Style.Add("display", "");
                    imgVideoDisplay.ImageUrl = "image/displaylayout/02.gif";
                    //imgLayoutMapping6.Style.Add("display", "none");
                    //imgLayoutMapping7.Style.Add("display", "none");
                    //imgLayoutMapping8.Style.Add("display", "none");
                    lblCodianLO.Text = obj.GetTranslatedText("Family 3");
                }
                else if (videoLayout == 104)
                {
                    imgVideoLayout.Style.Add("display", "none");
                    //imgVideoLayout.ImageUrl = "image/displaylayout/02.gif";
                    imgLayoutMapping6EP.Style.Add("display", "");
                    imgLayoutMapping6EP.ImageUrl = "image/displaylayout/03.gif";
                    imgLayoutMapping7EP.Style.Add("display", "");
                    imgLayoutMapping7EP.ImageUrl = "image/displaylayout/04.gif";
                    imgLayoutMapping8EP.Style.Add("display", "");
                    imgLayoutMapping8EP.ImageUrl = "image/displaylayout/43.gif";
                    lblCodianLOEP.Text = obj.GetTranslatedText("Family 4");

                    imgVideoDisplay.Style.Add("display", "");
                    imgVideoDisplay.ImageUrl = "image/displaylayout/02.gif";
                    //imgLayoutMapping6.Style.Add("display", "");
                    //imgLayoutMapping6.ImageUrl = "image/displaylayout/03.gif";
                    //imgLayoutMapping7.Style.Add("display", "");
                    //imgLayoutMapping7.ImageUrl = "image/displaylayout/04.gif";
                    //imgLayoutMapping8.Style.Add("display", "");
                    //imgLayoutMapping8.ImageUrl = "image/displaylayout/43.gif";
                    lblCodianLO.Text = obj.GetTranslatedText("Family 4");
                }
                else if (videoLayout == 105)
                {
                    imgVideoLayout.Style.Add("display", "none");
                    //imgVideoLayout.ImageUrl = "image/displaylayout/25.gif";
                    imgLayoutMapping6EP.Style.Add("display", "none");
                    imgLayoutMapping7EP.Style.Add("display", "none");
                    imgLayoutMapping8EP.Style.Add("display", "none");
                    lblCodianLOEP.Text = obj.GetTranslatedText("Family 5");

                    imgVideoDisplay.Style.Add("display", "");
                    imgVideoDisplay.ImageUrl = "image/displaylayout/25.gif";
                    //imgLayoutMapping6.Style.Add("display", "none");
                    //imgLayoutMapping7.Style.Add("display", "none");
                    //imgLayoutMapping8.Style.Add("display", "none");
                    lblCodianLO.Text = obj.GetTranslatedText("Family 5");
                }
                else if (videoLayout == 100)
                {

                    imgVideoLayout.Style.Add("display", "none");
                    imgLayoutMapping6EP.Style.Add("display", "none");
                    imgLayoutMapping7EP.Style.Add("display", "none");
                    imgLayoutMapping8EP.Style.Add("display", "none");
                    lblCodianLOEP.Text = obj.GetTranslatedText("Default Family");

                    imgVideoDisplay.Style.Add("display", "none");
                    //imgLayoutMapping6.Style.Add("display", "none");
                    //imgLayoutMapping7.Style.Add("display", "none");
                    //imgLayoutMapping8.Style.Add("display", "none");
                    lblCodianLO.Text = obj.GetTranslatedText("Default Family");
                }
            }
            else
            {
                imgVideoLayout.Style.Add("display", "");
                imgVideoLayout.ImageUrl = "image/displaylayout/" + txtSelectedImage.Text + ".gif";
                imgLayoutMapping6EP.Style.Add("display", "none");
                imgLayoutMapping7EP.Style.Add("display", "none");
                imgLayoutMapping8EP.Style.Add("display", "none");
                lblCodianLOEP.Text = "";

                imgVideoDisplay.Style.Add("display", "");
                imgVideoDisplay.ImageUrl = "image/displaylayout/" + txtSelectedImage.Text + ".gif";
                //imgLayoutMapping6.Style.Add("display", "none");
                //imgLayoutMapping7.Style.Add("display", "none");
                //imgLayoutMapping8.Style.Add("display", "none");
                lblCodianLO.Text = "";
                //document.getElementById("hdnFamilyLayout").value = 0;
            }

        }
        #endregion
        //ZD 101931 end

        //ZD 103216
        #region dgRoomHost_ItemDataBound
        protected void dgRoomHost_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DataRowView row = e.Item.DataItem as DataRowView;
                if (row != null)
                {
                    Image imgHost = (Image)e.Item.FindControl("imgHost");
                    if (row["RoomHost"].ToString() == "0")
                        imgHost.Visible = false;
                    else
                        imgHost.ImageUrl = "~/en/image/blue_button.png";

                    Label lblNoofAttendees = (Label)e.Item.FindControl("lblNoofAttendees");
                    lblNoofAttendees.Text = row["NoofAttendees"].ToString();
                }
            }
        }

        #endregion

        //ALLDEV-842 Starts
        private void RefreshEM7Endpoints()
        {
            try
            {
                String inXML = "";
                inXML = "<RefreshEM7Endpoints>" + obj.OrgXMLElement() + "</RefreshEM7Endpoints>";
                String outXML = obj.CallCOM2("RefreshEM7Endpoints", inXML, Application["RTC_ConfigPath"].ToString());
            }
            catch (Exception ex)
            {
                log.Trace("RefreshEM7Endpoints" + ex.Message);
            }
        }
        //ALLDEV-842 Ends
    }
     
}
