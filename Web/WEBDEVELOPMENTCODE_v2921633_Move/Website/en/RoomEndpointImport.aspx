﻿<%--/* Copyright (C) 2015 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.		
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%>

<%@ Page Language="C#" Inherits="ns_RoomEndpointImport.RoomEndpointImport" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<script type="text/javascript">
    function SelectAll(obj) {
        if (obj.tagName == "INPUT") {
            var chkstatus = "";
            chkstatus = obj.checked;
            var elements = document.getElementsByTagName('input');
            for (i = 0; i < elements.length; i++)
                if ((elements.item(i).id.indexOf("ChkSelect") >= 0)) {
                    if (elements.item(i).id != obj.id) {
                        elements.item(i).checked = chkstatus;
                    }
                }
        }
    }

    function CheckSelectAll() {
        var elements = document.getElementsByTagName('input');
        for (i = 0; i < elements.length; i++)
            if ((elements.item(i).id.indexOf("ChkSelectAll") >= 0)) {
                elements.item(i).checked = false;
            }
    }
    
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="frmSearchRoomInputParameters" runat="server">
    <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true">
        <Scripts>
            <asp:ScriptReference Path="~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>" />
        </Scripts>
    </asp:ScriptManager>
    <div>
        <table runat="server" width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td colspan="2" align="center" height="10px">
                    <h3>
                        <asp:Label ID="lblHeading" runat="server" Text="<%$ Resources:WebResources, ImportRPRMRooms %>"></asp:Label>
                    </h3>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table width="100%">
                        <tr>
                            <td align="center">
                                <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label><br />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trEndpoints" runat="server" visible="false">
                <td align="center" colspan="2">
                    <asp:DataGrid ID="dgEndpoints" runat="server" AutoGenerateColumns="False" CellPadding="4"
                        GridLines="None" BorderColor="blue" OnItemDataBound="dgEndpointList_ItemDataBound"
                        BorderStyle="solid" BorderWidth="1" ShowFooter="false" Width="50%" Visible="true"
                        Style="border-collapse: separate">
                        <SelectedItemStyle CssClass="tableBody" />
                        <EditItemStyle CssClass="tableBody" />
                        <AlternatingItemStyle CssClass="tableBody" />
                        <ItemStyle CssClass="tableBody" />
                        <HeaderStyle CssClass="tableHeader" HorizontalAlign="left" />
                        <Columns>
                            <asp:BoundColumn DataField="EndpointID" Visible="false" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader"
                                ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:WebResources, RoomEndPointImport_EndpointID%>">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="EndpointName" HeaderStyle-Width="25%" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader"
                                ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:WebResources,AddTerminalEndpoint_EndpointName%>">
                            </asp:BoundColumn>                            
                            <asp:TemplateColumn HeaderStyle-Width="25%" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center">
                                <HeaderTemplate>
                                    <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_SelectAll%>" runat="server"></asp:Literal><br />
                                    <asp:CheckBox runat="server" ID="ChkSelectAll" onclick="javascript:SelectAll(this);" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox runat="server" ID="ChkSelect" onclick="javascript:CheckSelectAll(this);" /></ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </td>
            </tr>

            <tr id="trRooms" runat="server" visible="false">
                <td align="center" colspan="2">
                    <asp:DataGrid ID="dgRooms" runat="server" AutoGenerateColumns="False" CellPadding="4"
                        GridLines="None" BorderColor="blue" OnItemDataBound="dgEndpointList_ItemDataBound"
                        BorderStyle="solid" BorderWidth="1" ShowFooter="false" Width="75%" Visible="true"
                        Style="border-collapse: separate">
                        <SelectedItemStyle CssClass="tableBody" />
                        <EditItemStyle CssClass="tableBody" />
                        <AlternatingItemStyle CssClass="tableBody" />
                        <ItemStyle CssClass="tableBody" />
                        <HeaderStyle CssClass="tableHeader" HorizontalAlign="left" />
                        <Columns>
                            <asp:BoundColumn DataField="RoomID" Visible="false" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader"
                                ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:WebResources, RoomEndPointImport_RoomID%>">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="RoomName" HeaderStyle-Width="25%" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader"
                                ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:WebResources, RoomSearch_RoomName%>">
                            </asp:BoundColumn>  
                            <asp:BoundColumn DataField="EndpointID" Visible="false" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader"
                                ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:WebResources, RoomEndPointImport_EndpointID%>">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="EndpointName" HeaderStyle-Width="25%" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader"
                                ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:WebResources,AddTerminalEndpoint_EndpointName%>">
                            </asp:BoundColumn>                            
                            <asp:TemplateColumn HeaderStyle-Width="25%" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center">
                                <HeaderTemplate>
                                    <asp:Literal ID="Literal1" Text="<%$ Resources:WebResources, ConferenceSetup_SelectAll%>" runat="server"></asp:Literal><br />
                                    <asp:CheckBox runat="server" ID="ChkSelectAll" onclick="javascript:SelectAll(this);" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox runat="server" ID="ChkSelect" onclick="javascript:CheckSelectAll(this);" /></ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Table runat="server" ID="tblNoRecords" Visible="false" Width="90%">
                        <asp:TableRow CssClass="lblError">
                            <asp:TableCell CssClass="lblError" HorizontalAlign="center">
                                <asp:Label ID="lblNoItems" runat="server" Text="<%$ Resources:WebResources, logList_lblNoRecords%>"></asp:Label>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </td>
            </tr>            
            <tr>
                <td align="center">
                    <asp:Table ID="tblPage" Visible="false" runat="server">
                        <asp:TableRow ID="TableRow1" runat="server">
                            <asp:TableCell ID="tc1" Font-Bold="True" Font-Names="Verdana" Font-Size="Small" ForeColor="Blue"
                                CssClass="subtitleblueblodtext" runat="server">
                                <asp:Literal Text="<%$ Resources:WebResources, ManageUser_Pages%>"
                                    runat="server"></asp:Literal>
                            </asp:TableCell>
                            <asp:TableCell ID="tc2" runat="server"></asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </td>
            </tr>            
            <tr style="height:25px">
            </tr>
            <tr>
            <td style="text-align:center">
            <button id="btnGoBack" type="button" runat="server" class="altLongYellowButtonFormat" onserverclick="GoBack" style="width: 150px"><asp:Literal Text="<%$ Resources:WebResources, ManageTier2_btnGoBack%>" runat="server"></asp:Literal></button>&nbsp;&nbsp;
            <%--<asp:Button ID="btnGoBack" runat="server" Text="<%$ Resources:WebResources, ManageTier2_btnGoBack%>" CssClass="altLongYellowButtonFormat" OnClick="GoBack" style="width: 150px"  />&nbsp;&nbsp;--%>
            <asp:Button ID="btnImport" runat="server" Text="<%$ Resources:WebResources, OrganisationSettings_btnCloudImport%>" CssClass="altLongYellowButtonFormat" OnClick="ImportData" style="width: 150px"  />
                                    </td>
            </tr>
            <tr style="height:25px">
            </tr>

        </table>
    </div>
    </form>
</body>
</html>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
<script type="text/javascript">
    if (document.getElementById('btnGoBack') != null)
        document.getElementById('btnGoBack').setAttribute("onblur", "document.getElementById('btnImport').focus(); document.getElementById('btnImport').setAttribute('onfocus', '');");

    document.onkeydown = function (evt) {
        evt = evt || window.event;
        var keyCode = evt.keyCode;
        if (keyCode == 8) {
            if (document.getElementById("btnCancel") != null) { // backspace
                var str = document.activeElement.type;
                if (!(str == "text" || str == "textarea" || str == "password")) {
                    document.getElementById("btnCancel").click();
                    return false;
                }
            }
            if (document.getElementById("btnGoBack") != null) { // backspace                
                var str = document.activeElement.type;
                if (!(str == "text" || str == "textarea" || str == "password")) {
                    document.getElementById("btnGoBack").click();
                    return false;
                }
            }
        }
        fnOnKeyDown(evt);
    };
</script>
<script type="text/javascript" src="inc/softedge.js"></script>
