﻿<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%>
<%--ZD 100147 ZD 100886 End--%>
<%--ZD 100263_T Start--%>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="en_ChangePassword" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--Window Dressing Start-->
<link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/border-table.css">
<link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/main-table.css">
<link rel="stylesheet" title="Expedite base styles" type="text/css" href="../en/Organizations/Original/Styles/main.css">
<%--FB 1830--%>
<!--Window Dressing End-->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>myVRM</title>

    <script type="text/javascript">        // FB 2790
        var path = '<%=Session["OrgCSSPath"]%>';
        if (path == "")
            path = "Organizations/Org_11/CSS/Mirror/Styles/main.css";
        path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css"; // FB 2815
        document.write("<link rel='stylesheet' title='Expedite base styles' type='text/css' href='" + path + "'?'" + new Date().getTime() + "'>"); //ZD 100152
    </script>

    <script language="JavaScript" src="inc/functions.js" type="text/javascript"></script>

    <script type="text/javascript">
        function fnLoginPage() {
            window.location.replace('genlogin.aspx');
            return false;
        }
        function fnCheckDetails() {


            var email = document.getElementById("TxtEmail").value;
            var pwd1 = document.getElementById("txtPassword1").value;
            var pwd2 = document.getElementById("txtPassword2").value;
            var labl = document.getElementById("LblError");
            if(!(email.indexOf('@') > -1 && email.split('@')[1].indexOf('.')>-1))
            {
                //alert("Enter Valid Email");
                labl.innerHTML = "Invalid Email Address";
                document.getElementById("TxtEmail").focus();
                return false;
            }

            if (pwd1 != "") {
                if (pwd2 == "") {
                    labl.innerHTML = "Please Re-enter password";
                    document.getElementById("txtPassword2").focus();
                    return false;
                }
            }
            else {
                labl.innerHTML = "Please enter password";
                document.getElementById("txtPassword1").focus();
                return false;
            }

            if (pwd1 != pwd2) {
                labl.innerHTML = "Password do not match";                
                return false;
            }
            
            labl.innerHTML = "";
            return true;           
        }
    </script>

</head>
<body>
    <center>
        <form id="frmEmailLogin" autocomplete="off" runat="server" defaultbutton="BtnSubmit"><%--ZD 101190--%>
        <%--<table>
            <tr style="height: 130px">
                <td style="display:none">
                </td>
            </tr>
        </table>--%>
        <br/><br/><br/><br/><br/><br/><br />
        <table cellpadding="2" cellspacing="2" width="700" border="0">
            <tr align="left">
                <td>
                    <div>
                        <table style="width: 100%" border="0">
                            <tr>
                                <td align="center">
                                    <h3>
                                        Change Password Request
                                    </h3>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Label ID="LblError" runat="server" Text="" CssClass="lblError"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="Left">
                                    <span class="subtitleblueblodtext">Enter the e-mail address associated with your
                                        myVRM account and the new password to Login:</span> 
                                </td>
                            </tr>
                        </table>
                        <table cellpadding="2" cellspacing="2" width="90%" border="0" style="padding-left:17%">
                            <tr>
                                <td align="left" style="width: 15%">
                                    <%--Window Dressing--%>
                                    <label class="blackblodtext">
                                        Email<span class="reqfldstarText">*</span></label>
                                </td>
                                <td align="left" style="width: 30%">
                                    <asp:TextBox ID="TxtEmail" runat="server" autocomplete="off" CssClass="altText" Style="width: 200px;"
                                        MaxLength="256"></asp:TextBox> <%--ZD 100263_Nov9--%>
                                </td>
                            </tr>
                            <tr style="height: 10px">
                            </tr>
                            <tr>
                                <td align="left" style="width: 15%">
                                    <%--Window Dressing--%>
                                    <label class="blackblodtext">
                                        New Password<span class="reqfldstarText">*</span></label>
                                </td>
                                <td align="left" style="width: 30%">
                                    <asp:TextBox ID="txtPassword1" runat="server" TextMode="Password" autocomplete="off" CssClass="altText" Style="width: 200px;"
                                        MaxLength="256"></asp:TextBox><%--ZD 100263_Nov9--%>
                                </td>
                            </tr>
                            <tr style="height: 10px">
                            </tr>
                            <tr>
                                <td align="left" style="width: 15%">
                                    <%--Window Dressing--%>
                                    <label class="blackblodtext">
                                        Confirm Password<span class="reqfldstarText">*</span></label>
                                </td>
                                <td align="left" style="width: 30%">
                                    <asp:TextBox ID="txtPassword2" runat="server" TextMode="Password" autocomplete="off" CssClass="altText" Style="width: 200px;"
                                        MaxLength="256"></asp:TextBox><%--ZD 100263_Nov9--%>
                                </td>
                            </tr>
                            <tr style="height: 10px">
                            </tr>
                        </table>
                        <table cellpadding="2" cellspacing="2" width="90%" border="0">
                            <tr align="center" style="width: 90%">
                                <td align="center">
                                    <input type="button" id="BtnBack" class="altMedium0BlueButtonFormat" value="Back"
                                        onclick="javascript:return fnLoginPage()" />
                                </td>
                               
                                <td align="center">
                                    <asp:Button CssClass="altMedium0BlueButtonFormat" ID="BtnSubmit" OnClientClick="javascript:return fnCheckDetails()" 
                                        Text="Submit" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
        </table>
        </form>
    </center>
</body>
</html>
<br />
<br />
<br />
<br />
<br />
<br />
<%--code added for Soft Edge button--%>

<script type="text/javascript" src="inc/softedge.js"></script>

<%--FB 2500--%>
<%--<!-- #INCLUDE FILE="inc/mainbottom2.aspx" -->--%>
