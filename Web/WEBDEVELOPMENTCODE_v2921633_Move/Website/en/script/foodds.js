/*ZD 100147 Start*/
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
/*ZD 100147 ZD 100886 End*/
// script for food data structure

function FoodItem(fid, name, imgSrc)
{
	this.fid = fid;
	this.name = name;
	this.icon = new Image();
	this.icon.src = imgSrc;
	this.ddobj = null;
	this.toString = foodToString;
}

function foodToString()
{
	return this.name;
}

function compareName(a, b)
{
	var aname = a.name.toUpperCase();
	var bname = b.name.toUpperCase();
	if (aname > bname) 
		return 1;
	else if (aname < bname)
		return -1;
	else 
		return 0;
}


