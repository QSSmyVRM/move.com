<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.	
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 ZD 100866 End--%>
<%@ Page Language="C#" Inherits="ns_Tiers2.Tiers2" Buffer="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=8" /> <!-- FB 2050 --> <%--FB 2779--%>

<!--window Dressing-->
<!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 

<script runat="server">

</script>
<script language="javascript">
    //ZD 100604 start
    var img = new Image();
    img.src = "../en/image/wait1.gif";
    //ZD 100604 End
    //ZD 100176 start
    function DataLoading(val) {
        if (val == "1")
            document.getElementById("dataLoadingDIV").style.display = 'block'; //ZD 100678
        else
            document.getElementById("dataLoadingDIV").style.display = 'none'; //ZD 100678
    }
    //ZD 100176 End
    //ZD 101244 start
    function EnableValidate() {
        if (document.getElementById("ChkSecure2") != null) {
            if ((document.getElementById("ChkSecure2").checked))
                ValidatorEnable(document.getElementById("reqSecurityemail"), true);
            else
                ValidatorEnable(document.getElementById("reqSecurityemail"), false);
        }
    }
    function EnableValidate1(obj) {
        var custId = obj.id.split("_");

        if (obj.checked)
            ValidatorEnable(document.getElementById(custId[0] + "_" + custId[1] + "_" + "reqSecurityemail1"), true);
        else
            ValidatorEnable(document.getElementById(custId[0] + "_" + custId[1] + "_" + "reqSecurityemail1"), false);
    }
    function Checkemail(obj) {
        var custId = obj.id.split("_");
        if ((document.getElementById(custId[0] + "_" + custId[1] + "_" + "ChkSecure2s") != null) && (document.getElementById(custId[0] + "_" + custId[1] + "_" + "ChkSecure2s").checked)) {
            if (document.getElementById(custId[0] + "_" + custId[1] + "_" + "txtsecurityemail2s").value == "") {
                ValidatorEnable(document.getElementById(custId[0] + "_" + custId[1] + "_" + "reqSecurityemail1"), true);
                return false;
            }
            else
                ValidatorEnable(document.getElementById("reqSecurityemail"), false);
        }
    }
    function fnCheckemail(sender, args) {
        var vaild = true;
        var emaillist = document.getElementById('txtsecurityemail2').value;
        var Spilitemail = emaillist.split(';');
        var regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        for (var i = 0; i < Spilitemail.length; i++) {
            if (Spilitemail[i] = "" || !regex.test(Spilitemail[i])) {
                document.getElementById('customsecurityemail').style.display = 'block';
                return args.IsValid = false;
            }
            else {
                document.getElementById('customsecurityemail').style.display = 'none';
            }
        }
    }

    function fnCheckemail1(sender, args) {
        var custId = sender.id.split("_");
        var emaillist = document.getElementById(custId[0] + "_" + custId[1] + "_" + "txtsecurityemail2s").value;
        var vaild = true;
        var Spilitemail = emaillist.split(';');
        var regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        for (var i = 0; i < Spilitemail.length; i++) {
            if (Spilitemail[i] = "" || !regex.test(Spilitemail[i])) {
                document.getElementById(custId[0] + "_" + custId[1] + "_" + "customsecurityemail1").style.display = 'block';
                args.IsValid = false;
            }
            else {
                document.getElementById(custId[0] + "_" + custId[1] + "_" + "customsecurityemail1").style.display = 'none';
            }
        }
    }
    //ZD 101244 End
</script>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Manage Tiers</title>
    <script type="text/javascript" src="inc/functions.js"></script>
    <%--//ZD 101244--%>
    <style type="text/css">
        .labelwrap
        {
          word-break :break-all;
        }
    </style>
    <%--ZD 101244 End--%>
</head>
<body>
    <form id="frmTierManagement" runat="server" method="post" onsubmit="return true">
     <%--ZD 101022 start--%>
        <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" >
		    <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		    </Scripts>
	    </asp:ScriptManager> <%--ZD 101022 End--%>
    <div>
      <input type="hidden" id="helpPage" value="65">

        <table width="100%">
            <tr>
                <td align="center">
                    <h3>
                        <asp:Label ID="lblTier1Name" runat="server" Text="" ></asp:Label>
                        <asp:TextBox ID="txtTier1ID" Visible="false" Text="" runat="server"></asp:TextBox>
                    </h3>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
                </td>
            </tr>
            <div id="dataLoadingDIV" style="display:none" align="center" >
                <img border='0' src='image/wait1.gif'  alt='Loading..' />
            </div> <%--ZD 100678 End--%>
            <tr>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <SPAN class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageTier2_ExistingTiers%>" runat="server"></asp:Literal></SPAN>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:DataGrid ID="dgTier2s" runat="server" AutoGenerateColumns="False" CellPadding="4" GridLines="None"
                         BorderColor="blue" BorderStyle="solid" BorderWidth="1" ShowFooter="true" OnUpdateCommand="UpdateTier2" OnCancelCommand="CancelTier2" OnItemCreated="BindRowsDeleteMessage2"
                        OnDeleteCommand="DeleteTier2" OnEditCommand="EditTier2" Width="90%" Visible="true">
                         <%--Window Dressing - Start--%>
                        <SelectedItemStyle CssClass="tableBody" />
                        <EditItemStyle CssClass="tableBody" />
                        <AlternatingItemStyle CssClass="tableBody" />
                        <ItemStyle CssClass="tableBody" />
                        <FooterStyle CssClass="tableBody"  />
                        <%--Window Dressing - End--%>
                        <HeaderStyle CssClass="tableHeader" />
                        <Columns>
                            <asp:BoundColumn DataField="ID" Visible="false" ><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, Name%>" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="60%" > <%-- FB 2050 --%>
                                <ItemTemplate>
                                    <asp:Label ID="lblTier2Name" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Name") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtTier2Name" CssClass="altText" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Name")%>'></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="reqTier2Name1" ControlToValidate="txtTier2Name" ValidationGroup="Update" runat="server" ErrorMessage="<%$ Resources:WebResources, Required%>" Display="dynamic"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="regName" ValidationGroup="Update" ControlToValidate="txtTier2Name" runat="server" Display="dynamic" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters35%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+?|!`\[\]{}\=^@#$%&()~]*$"></asp:RegularExpressionValidator> <%-- fogbugz case 137 for extra junk character validation--%> <%--FB 1888--%> <%--ZD 103424 ZD 104000--%>
                                </EditItemTemplate>
                             </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, Actions%>" ItemStyle-Width="15%">
                                <HeaderStyle CssClass="tableHeader" />
                                <ItemTemplate>
                                    <asp:LinkButton runat="server" Text="<%$ Resources:WebResources, ManageTier2_btnEdit%>" ID="btnEdit" CommandName="Edit" OnClientClick="DataLoading(1)"></asp:LinkButton> <%--ZD 100176--%> 
                                    &nbsp;<asp:LinkButton runat="server" Text="<%$ Resources:WebResources, ManageTier2_btnDelete%>" ID="btnDelete" CommandName="Delete" OnClientClick="DataLoading(1)"></asp:LinkButton><%--ZD 100176--%> 
                                    <asp:LinkButton runat="server" Text="<%$ Resources:WebResources, ManageTier2_btnUpdate%>" ID="btnUpdate" ValidationGroup="Update" CommandName="Update" Visible="false" OnClientClick="javascript:Checkemail(this);" ></asp:LinkButton><%--ZD 100176--%> 
                                    &nbsp;<asp:LinkButton runat="server" Text="<%$ Resources:WebResources, ManageTier2_btnCancel%>" ID="btnCancel" CommandName="Cancel" Visible="false" OnClientClick="DataLoading(1)"></asp:LinkButton><%--ZD 100176--%> 
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <%--ZD 101244 start--%>
                            <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, Secure%>" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader">
                             <ItemTemplate>
                               <asp:CheckBox ID="ChkSecure2s" runat="server" Checked='<%# DataBinder.Eval(Container, "DataItem.Secure").Equals("1") %>'  Enabled="false" onclick="javascript:EnableValidate1(this);"/>
                             </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, ManageTier_securityemail%>" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader">
                             <ItemTemplate>
                                <asp:Label ID="lblsecurityemail2s" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Securityemail") %>' Width="200px" CssClass="labelwrap"></asp:Label>
                             </ItemTemplate>
                             <EditItemTemplate>
                                <asp:TextBox ID="txtsecurityemail2s" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Securityemail") %>'/>
                                <asp:RequiredFieldValidator ID="reqSecurityemail1" ValidationGroup="Update" Enabled="false" ControlToValidate="txtsecurityemail2s" runat="server" ErrorMessage="<%$ Resources:WebResources, Required%>" Display="dynamic"></asp:RequiredFieldValidator>
                                <asp:CustomValidator id="customsecurityemail1" ErrorMessage="<%$ Resources:WebResources, Invalidemail%>" Display="Dynamic" ValidationGroup="Update"
                                    ControlToValidate="txtsecurityemail2s" runat="server"  ClientValidationFunction="fnCheckemail1"></asp:CustomValidator>
                             </EditItemTemplate>
                             <FooterTemplate>
                                <%--Window Dressing--%><div style="text-align :right">
                                    <span class="blackblodtext"> <asp:Literal Text="<%$ Resources:WebResources, ManageTier2_TotalTiers%>" runat="server"></asp:Literal></span> <asp:Label ID="lblTotalRecords" runat="server" Text=""></asp:Label><%--FB 2579--%>
                                    </div>
                                </FooterTemplate>
                            </asp:TemplateColumn><%--ZD 101244 End--%>
                        </Columns>
                    </asp:DataGrid>
                    <asp:Table runat="server" ID="tblNoTier2s" Visible="false" Width="90%">
                        <asp:TableRow CssClass="lblError">
                            <asp:TableCell CssClass="lblError" align="left">
                                <asp:Literal ID="Literal1" Text="<%$ Resources:WebResources, ManageTier2_TotalTiers%>" runat="server"></asp:Literal>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>     
                </td>
            </tr>
            <tr>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                               <%-- <SPAN class=subtitleblueblodtext>Create New Tier2</SPAN>--%><%--Commented for FB 2094 --%>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trTiers" runat="server"><td><table border="0" width="100%" ><%--ZD 101244--%>
            <tr id="trNew" runat="server"> <%--FB 2670--%>
            <td style="width:30%"></td>
                <%--Window Dressing --%>            
                    <td class="blackblodtext" style="width:13%"><asp:Literal Text="<%$ Resources:WebResources, ManageTier2_CreateNewTier%>" runat="server"></asp:Literal><span class="reqfldstarText">*</span><%--FB 2094--%>
                    </td><td><%--ZD 101244--%>
                   <asp:TextBox ID="txtNewTier2Name" runat="server" Text="" CssClass="altText"></asp:TextBox>
                   <asp:RequiredFieldValidator ID="reqTier2Name2" ValidationGroup="Create2" ControlToValidate="txtNewTier2Name" runat="server" ErrorMessage="<%$ Resources:WebResources, Required%>" Display="dynamic"></asp:RequiredFieldValidator>
                   <%-- fogbugz case 137 for extra junk character validation--%>
                    <asp:RegularExpressionValidator ID="regName1" ValidationGroup="Create2" ControlToValidate="txtNewTier2Name" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters35%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+?|!`\[\]{}\=^@#$%&()~]*$"></asp:RegularExpressionValidator> <%--FB 1888--%> <%--ZD 103424--%> <%--ZD 104000--%>
                </td>
            </tr><%--ZD 101244 start--%>
            <tr>
            <td style="width:30%"></td>
            <td class="blackblodtext">
             <asp:Literal Text="<%$ Resources:WebResources, EditEndpoint_lblSecured%>" runat="server"></asp:Literal>
            </td>
            <td> 
              <asp:CheckBox ID="ChkSecure2" runat="server" onclick="javascript:EnableValidate();"/>
            </td>
            </tr>    
            <tr>
            <td style="width:30%"></td>
             <td  class="blackblodtext">
              <asp:Literal Text="<%$ Resources:WebResources, ManageTier_securityemail%>" runat="server"></asp:Literal>
             </td>
             <td>
              <asp:TextBox runat="server" ID="txtsecurityemail2" CssClass="altText"></asp:TextBox>
              <asp:RequiredFieldValidator ID="reqSecurityemail" Enabled="false" ValidationGroup="Create2" ControlToValidate="txtsecurityemail2" runat="server" ErrorMessage="<%$ Resources:WebResources, Required%>" Display="dynamic"></asp:RequiredFieldValidator>
              <asp:CustomValidator id="customsecurityemail" ErrorMessage="<%$ Resources:WebResources, Invalidemail%>" Display="Dynamic"
               ControlToValidate="txtsecurityemail2"  runat="server"  ClientValidationFunction="fnCheckemail" ValidationGroup="Create2" ></asp:CustomValidator>
              <%--<asp:RegularExpressionValidator ID="regEmail1_1" ControlToValidate="txtsecurityemail2" Display="dynamic" runat="server"  ValidationGroup="Create2"
                 ErrorMessage="<%$ Resources:WebResources, Invalidemail%>" ValidationExpression="[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"></asp:RegularExpressionValidator>
                <asp:RegularExpressionValidator ID="regEmail1_2" ControlToValidate="txtsecurityemail2" Display="dynamic" runat="server" SetFocusOnError="true"  ValidationGroup="Create2"
                ErrorMessage="<%$ Resources:WebResources, InvalidCharacters14%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=:#$%&'~]*$"></asp:RegularExpressionValidator>--%>
             </td>
            </tr>
            </table></td></tr> <tr></tr><tr></tr> <tr id="trTierssubmit" runat="server"><td align="center">
            <asp:Button runat="server" ID="btnCreateNewTier2" style="margin-left:-35px" ValidationGroup="Create2" Text="<%$ Resources:WebResources, ManageTier2_btnCreateNewTier2%>" CssClass="altMedium0BlueButtonFormat" OnClick="CreateNewTier2" CommandName="Update" />
            </td></tr>
            <%--ZD 101244 End--%>
            <tr>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <SPAN class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageTier2_ManageTopTier%>" runat="server"></asp:Literal></SPAN>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                   <asp:Button runat="server" ID="btnGoBack" Text="<%$ Resources:WebResources, ManageTier2_btnGoBack%>" CssClass="altLongBlueButtonFormat" OnClick="GoBack" OnClientClick="DataLoading(1)" /><%--ZD 100176--%> 
                </td>
            </tr>
        </table>
    </div>

<img src="keepalive.asp" alt="Keepalive" name="myPic" width="1px" height="1px" style="display:none"> <%--ZD 100419--%>
    </form>
<%--code added for Soft Edge button--%>
<%--ZD 100420 Start--%>
<script language="javascript">
    document.onkeydown = function(evt) {
        evt = evt || window.event;
        var keyCode = evt.keyCode;
        if (keyCode == 8) {
            if (document.getElementById("btnCancel") != null) { // backspace
                var str = document.activeElement.type;
                if (!(str == "text" || str == "textarea" || str == "password")) {
                    document.getElementById("btnCancel").click();
                    return false;
                }
            }
            if (document.getElementById("btnGoBack") != null) { // backspace
                var str = document.activeElement.type;
                if (!(str == "text" || str == "textarea" || str == "password")) {
                    document.getElementById("btnGoBack").click();
                    return false;
                }
            }
        }
        fnOnKeyDown(evt);
    };

    if (document.getElementById('txtsecurityemail2') != null)
        document.getElementById('txtsecurityemail2').setAttribute("onblur", "document.getElementById('btnCreateNewTier2').focus(); document.getElementById('btnCreateNewTier2').setAttribute('onfocus', '');");
    if (document.getElementById('btnCreateNewTier2') != null)
        document.getElementById('btnCreateNewTier2').setAttribute("onblur", "document.getElementById('btnGoBack').focus(); document.getElementById('btnGoBack').setAttribute('onfocus', '');");               
</script>
<%--ZD 100420 End--%>
<script type="text/javascript" src="inc/softedge.js"></script>
    <!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
</body>
</html>

