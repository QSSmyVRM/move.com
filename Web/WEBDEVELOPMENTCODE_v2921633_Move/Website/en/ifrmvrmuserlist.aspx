<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.	
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 ZD 100866 End--%>
<%@ Page Language="C#" AutoEventWireup="true"  Inherits="en_IfrmVRMUserlist" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>ifrmadduserlist</title>
   <script type="text/javascript"> // FB 2815
    var path = '<%=Session["OrgCSSPath"]%>';
    path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css";
    document.write("<link rel='stylesheet' title='Expedite base styles' type='text/css' href='" + path + "'?'" + new Date().getTime() + "'>"); //ZD 100152
   </script>   
</head>
<body>
<script type="text/javascript" src="extract.js"></script>
<script type="text/javascript" src="sorttable.js"></script>
<script type="text/javascript" src="script/wincheck.js"></script> <%--FB 3055--%>

<script type="text/javascript">
<!--

function delUser(uid)
{
	if (eval("parent.document." + queryField("f") + ".SelectedUser") && (parent.ifrmADuserlist.document)) {
		curselusers = eval("parent.document." + queryField("f") + ".SelectedUser").value;
		seluserary = curselusers.split("||"); // FB 1888
		newselusers = ""

		found = -1;
		for (i = 0; i < seluserary.length-1; i++) {
			seluserary[i] = seluserary[i].split("``");
			if (seluserary[i][0] == uid) {
				found = i;
			}
		}

		if (found != -1) {
			for (i = 0; i <found; i++)
				newselusers += seluserary[i][0] + "``" + seluserary[i][1] + "``" + seluserary[i][2] + "``" + seluserary[i][3] + "||"; // FB 1888
			for (i = found + 1; i < seluserary.length-1; i++)
				newselusers += seluserary[i][0] + "``" + seluserary[i][1] + "``" + seluserary[i][2] + "``" + seluserary[i][3] + "||"; // FB 1888
		}

		//ZD 101362
        cb = parent.ifrmADuserlist.document.frmIfrmuserlist.seladuser;
        chkAll = parent.document.getElementById("CheckAllAdUser");

		if (cb != null) //FB 1599
		{
		    var c = 0;
		    for (i = 0; i < cb.length; i++) {
		        c = 1;
		        if (cb[i].value == uid)
		            cb[i].checked = false;
		    }

		    if (c == 0) {
		        if (cb.checked == true)
		            cb.checked = false;
		    }

		    if (chkAll != null)
		        chkAll.checked = false;
		}
		
		if (queryField("f") == "frmAllocation") {
		/*
			cb = parent.ifrmAllogrouplist.document.frmIfrmgrouplist.selgrp;
			var guxml = parent.ifrmAllogrouplist.document.frmIfrmgrouplist.groupuser.value;

			grpsary = guxml.split("||");
			for (var i = 0; i < grpsary.length-1; i++) {
				grpsary[i] = grpsary[i].split("##");
				guary = grpsary[i][1].split(";;");
					
				for (var j = 0; j < guary.length-1; j++) {
					guary[j] = guary[j].split("``");
					if (guary[j][0] == uid) {
						for (var k = 0; k < cb.length; k++) {
							if (cb[i].value == grpsary[i][0].split("``")[0])
								cb[i].checked = false;
						}
						continue;
					}						
				}
			}
		*/
		}
		
		eval("parent.document." + queryField("f") + ".SelectedUser").value = newselusers;
		window.location.reload();
	}
}


function sortlist(id)
{
	SortRows( vrm_list, ( (id == 4) ? 3 : id ) );
}

// ZD 101362 Start
function fnHideIframe(par) {
    if (par == "1")
        document.getElementById("hideScreen").style.display = "block";
    else
        document.getElementById("hideScreen").style.display = "none";
}
// ZD 101362 Ends

//-->
</script>



<form id="frmIfrmvrmuserlist" runat="server">
<div id="hideScreen" style="width:500px; height:500px; z-index: 1000; position:fixed; left:0px; top:0px; display:none; background-color:lightgray">
<span class="blackblodtext"><br />&nbsp;&nbsp;<asp:Literal ID="Literal1" Text='<%$ Resources:WebResources, SelectedAllUsers%>' runat='server'></asp:Literal></span>
</div>  <%--ZD 101362--%>
 <%--ZD 101022 start--%>
        <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" >
		    <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		    </Scripts>
	    </asp:ScriptManager> <%--ZD 101022 End--%>

  <center>


<script type="text/javascript">
<!-- 

    

	var xmlstr, usersary;
	
	if (eval("parent.document." + queryField("f"))) {
		xmlstr = eval("parent.document." + queryField("f") + ".SelectedUser").value;
	} else {
		xmlstr = "";
		setTimeout('window.location.reload();',500);
	}
	var vrm_list = new SortTable('vrm_list');
	
	vrm_list.AddColumn("Select","width=50","center","form");
	vrm_list.AddColumn("Frtname","width=160","center","email");
	vrm_list.AddColumn("Lstname","width=160","center","email");
	vrm_list.AddColumn("Email","width=190","center","email");

    
	usersary = xmlstr.split("||") // FB 1888

	for (i = 0; i < usersary.length-1; i++) {
		usersary[i] = usersary[i].split("``");

		if(usersary[i][3].length > 12){
			strLongEmail = usersary[i][3].substr(0,10) + "..."
		}
		else{
			strLongEmail = usersary[i][3]

}
		
		delstr  = "";	
		delstr += "<a href='#' onClick='delUser(\"" + usersary[i][0] + "\");' onMouseOver='window.status=\"\"; return true' onMouseOut='window.status=\"\"; return true'>";
		delstr += "<img border='0' src='image/btn_delete.gif' width='18' height='18' style='cursor:pointer;' title='" + RSDelete + "' alt='" + RSDelete + "' >"  //ZD 100419
		delstr += "</a>"
		emailstr = "<a href='mailto:" + usersary[i][3] + "' title='" + usersary[i][3] + "'>" + strLongEmail + "</a>"
		//emailstr = "<a href='mailto:" + usersary[i][3] + "'>" + usersary[i][3] + "</a>"
		vrm_list.AddLine(delstr, usersary[i][1], usersary[i][2], emailstr);
	}


		
//-->
</script>

    <table width='100%' border='0' cellpadding='2' cellspacing='1'>
      <script type="text/javascript">vrm_list.WriteRows()</script>
    </table>

  </center>

</form>

</body>

</html>
