<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.	
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 ZD 100866 End--%>
<%@ Page Language="C#" Inherits="ns_MyVRM.Users" Buffer="true" %>
<%--FB 2779 Starts--%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>   <%--ALLDEV-498--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<%--FB 2779 Ends--%>

 
<script type="text/javascript" src="script/mousepos.js"></script>
<script type="text/javascript" src="script/managemcuorder.js"></script>

<script language="javascript">

//ZD 103405
    function ChangeSortingOrder(par) {
        DataLoading('1');
        var SortingId = par.split("_")[2];
        if (SortingId.indexOf('spnAscending') > -1)
            document.getElementById("hdnSortingOrder").value = "1";
        else
            document.getElementById("hdnSortingOrder").value = "0";
    }
    //ZD 103405 End 

    //ZD 100604 start
    var img = new Image();
    img.src = "../en/image/wait1.gif";
    //ZD 100604 End
	function ManageOrder ()
	{
		change_mcu_order_prompt('image/pen.gif', 'Manage MCU Order', document.frmManagebridge.Bridges.value);
		
//		frmsubmit('SAVE', '');
	}
	
	function frmsubmit()
	{
	    document.getElementById("__EVENTTARGET").value="ManageOrder";
	    document.frmManagebridge.submit();
	}
	//Added for FB 1405 -Start 
	
	function fnSearch()
	{
	    DataLoading(1);//ZD 100176
	    var obj = document.getElementById("hdnAction");
	    obj.value = 'Y';
	    return true;
	    

	}
	//ZD 100176 start
	function DataLoading(val) {
	    if (val == "1")
	        document.getElementById("dataLoadingDIV").style.display = 'block'; //ZD 100678
	    else
	        document.getElementById("dataLoadingDIV").style.display = 'none'; //ZD 100678
	}
	//ZD 100176 End
	
	//Added for FB 1405 -End 
</script>
<head runat="server">
    <title>Manage Users</title>
    <script type="text/javascript" src="inc/functions.js"></script>

</head>
<body>
    <form id="frmManagebridge" runat="server" method="post" onsubmit="return true">
	   <%--ZD 101022 start--%>
        <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" >
		    <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		    </Scripts>
	    </asp:ScriptManager> <%--ZD 101022 End--%>
     
    <div id="divmanageuserheight"> <%--ZD 100393--%>
      <input type="hidden" id="helpPage" value="65"/>
      <input type="hidden" id="hdnSortingOrder" runat="server" value="0" /><%--ZD 103405--%>
      <input type="hidden" id="hdnsort" runat="server" value="0" /><%--ZD 103405--%>
      <input type="hidden" id="hdnsortBy" runat="server" value="0" /><%--ZD 103405--%>
       <input type="hidden" id="hdnAction" runat="server"/> <%--Added for FB 1405--%>
      <input type="hidden" id="hdnDeleteUserID" runat="server" /><%--ALLDEV-498--%>
      <input type="hidden" id="hdntotalPages" runat="server" /><%--ALLDEV-498--%>
        
        <table width="100%">
            <tr>
                <td align="center">
                    <h3>
                        <asp:Label ID="lblHeader" runat="server"></asp:Label>
                    </h3>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
                    <div id="dataLoadingDIV"  style="display:none" align="center" >
                        <img border='0' src='image/wait1.gif'  alt='Loading..' />
                    </div> <%--ZD 100678 End--%>
                </td>
            </tr>
            <tr>
                <td align="Left">
                    <table width="100%">
                        <tr>
                            <td >&nbsp;</td>
                            <td>
                                <SPAN class="subtitleblueblodtext" style="margin-left:-15px"><asp:Literal Text="<%$ Resources:WebResources, ManageUser_ExistingUsers%>" runat="server"></asp:Literal></SPAN>
                            </td>
                        </tr>
                        <tr>
                            <td width="20"></td>
                            <td align="center">
                                <table width="90%">
                                    <tr><%--ZD 100425 - Start--%>
                                        <td align="right"><span class="blackblodtext"><asp:Literal ID="Literal1" Text="<%$ Resources:WebResources, SortBy%>" runat="server"></asp:Literal></span>
                                            <asp:DropDownList ID="lstSortBy" CssClass="alt2SelectFormat" runat="server" AutoPostBack="true" 
											OnSelectedIndexChanged="ChangeAlphabets"  onchange="javascript:DataLoading('1');"> <%--ZD 100429--%>
                                                <asp:ListItem Value="1" Text="<%$ Resources:WebResources, ManageUser_FirstName%>"></asp:ListItem>
                                                <asp:ListItem Value="2" Text="<%$ Resources:WebResources, ManageUser_LastName%>"></asp:ListItem>
                                                <asp:ListItem Value="3" Text="<%$ Resources:WebResources, Login%>"></asp:ListItem>
                                                <asp:ListItem Value="4" Text="<%$ Resources:WebResources, Email%>"></asp:ListItem>
                                                <asp:ListItem Value="5" Text="<%$ Resources:WebResources, ManageUserRoles_RoleName%>"></asp:ListItem> <%--ZD 103405--%>
                                                <asp:ListItem Value="6" Text="<%$ Resources:WebResources, ManageUserRoles_BlockedUsers%>"></asp:ListItem> <%--ZD 103837--%>
                                            </asp:DropDownList> 
                                            <%--Window Dressing--%>
                                            <span class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUser_Startswith%>" runat="server"></asp:Literal></span>
                                        </td>
                                        <td align="left">
                                             <asp:Table runat="server"  ID="tblAlphabet" CellPadding="2" CellSpacing="5"></asp:Table>
                                        </td>
                                       <td> <%--ZD 103459--%>
                                          <asp:Button id="btnunblockAll"  runat="server" cssclass="altLongBlueButtonFormat" text="<%$ Resources:WebResources, ManageUser_UnblockAll%>" OnClick="UnBlockAlluser"  Width="145px" style="margin-right:-5px"></asp:Button> 
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:DataGrid ID="dgUsers" runat="server" AutoGenerateColumns="False" CellPadding="4" GridLines="None"
                         BorderColor="blue" BorderStyle="solid" BorderWidth="1" ShowFooter="false" OnItemDataBound="BindRowsDeleteMessage"
                        OnDeleteCommand="DeleteUser" OnEditCommand="EditUser" OnCancelCommand="LockUser" OnUpdateCommand="RestoreUser" Width="90%" Visible="true" style="border-collapse:separate"> <%--Edited for FF--%>
                        <%--Window Dressing - Start--%>                        
                        <SelectedItemStyle CssClass="tableBody" />
                        <EditItemStyle CssClass="tableBody" />
                        <AlternatingItemStyle CssClass="tableBody" />
                        <ItemStyle CssClass="tableBody" />
                         <%--Window Dressing - End--%> 
                        <HeaderStyle CssClass="tableHeader" HorizontalAlign="left" />
                        <Columns>
                            <asp:BoundColumn DataField="userID" Visible="false" ><HeaderStyle CssClass="tableHeader" HorizontalAlign="left" /></asp:BoundColumn><%--ZD 103405 start--%>
                            <asp:BoundColumn DataField="level" Visible="false" ><HeaderStyle CssClass="tableHeader" HorizontalAlign="left" /></asp:BoundColumn>
                            <asp:BoundColumn DataField="deleted" Visible="false" ><HeaderStyle CssClass="tableHeader" HorizontalAlign="left" /></asp:BoundColumn>
                            <asp:BoundColumn DataField="locked" Visible="false" ><HeaderStyle CssClass="tableHeader" HorizontalAlign="left" /></asp:BoundColumn>  <%--ZD 103405 Starts--%>                           
                            <asp:TemplateColumn ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" ItemStyle-HorizontalAlign="Left"><%--ZD 103415--%>
                            <HeaderTemplate>
                            <table width="100%" >
                            <tr>
                                        <td width="20%" runat="server" id="tdFirstName" align="left" class="tableHeader"> 
                                           <asp:Literal ID="Literal9" Text="<%$ Resources:WebResources, ManageUser_FirstName%>" runat="server">
                                           </asp:Literal><asp:LinkButton ID="btnSortName" runat="server" CommandArgument="1" OnCommand="SortGrid" OnClientClick="ChangeSortingOrder(this.childNodes[0].id);"><%if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("TRIDENT")) {%><asp:Label id="spnAscendingFirstNameIE" runat="server" style="font-family:Wingdings; font-weight:bolder;">&#233;</asp:Label><asp:Label id="spnDescndingFirstNameIE" runat="server" style="font-family:Wingdings; font-weight:bolder;">&#234;</asp:Label><% } else { %><asp:Label id="spnAscendingFirstName" runat="server" style="font-family:Wingdings; font-weight:bolder; font-size:x-large;">&#8593;</asp:Label><asp:Label id="spnDescndingFirstName" runat="server" style="font-family:Wingdings; font-weight:bolder; font-size:x-large;">&#8595;</asp:Label><% } %></asp:LinkButton>
                                        </td></tr>
                                        </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                            <table  width="100%">
                                  <tr>
                                   <td width="20%" align="left" valign="top" ><%--FB 2274--%>
                                            <asp:Label ID="lblfirstName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.firstName") %>'></asp:Label>
                                        </td>
                                        </tr>
                                        </table>
                            </ItemTemplate>
                            </asp:TemplateColumn>
                             <asp:TemplateColumn ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" ItemStyle-HorizontalAlign="Left"><%--ZD 103415--%>
                            <HeaderTemplate>
                            <table width="100%" >
                            <tr>
                                       <td width="20%" runat="server" id="tdLastName" align="left" class="tableHeader">
                                           <asp:Literal ID="Literal10" Text="<%$ Resources:WebResources, ManageUser_LastName%>" runat="server"></asp:Literal>
                                           <asp:LinkButton ID="btnSortLastName" runat="server" CommandArgument="2" OnCommand="SortGrid" OnClientClick="ChangeSortingOrder(this.childNodes[0].id);"><%if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("TRIDENT")) { %><asp:Label id="spnAscendingLastNameIE" runat="server" style="font-family:Wingdings; font-weight:bolder;">&#233;</asp:Label><asp:Label id="spnDescndingLastNameIE" runat="server" style="font-family:Wingdings; font-weight:bolder;">&#234;</asp:Label><% } else { %><asp:Label id="spnAscendingLastName" runat="server" style="font-family:Wingdings; font-weight:bolder; font-size:x-large;">&#8593;</asp:Label><asp:Label id="spnDescndingLastName" runat="server" style="font-family:Wingdings; font-weight:bolder; font-size:x-large;">&#8595;</asp:Label><% } %></asp:LinkButton> 
                                        </td></tr>
                                        </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                            <table  width="100%">
                                  <tr>
                                    <td width="20%" align="left" valign="top" ><%--FB 2274--%>
                                            <asp:Label ID="lbllastName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.lastname") %>'></asp:Label>
                                        </td>
                                        </tr>
                                        </table>
                            </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" ItemStyle-HorizontalAlign="Left"><%--ZD 103415--%>
                            <HeaderTemplate>
                            <table width="100%" >
                            <tr>
                                        <td width="20%"  runat="server" id="tdLogin" align="left" class="tableHeader"> 
                                           <asp:Literal ID="Literal11" Text="<%$ Resources:WebResources, Login%>" runat="server">
                                           </asp:Literal><asp:LinkButton ID="LinkButton1" runat="server" CommandArgument="3" OnCommand="SortGrid" OnClientClick="ChangeSortingOrder(this.childNodes[0].id);"><%if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("TRIDENT")) {%><asp:Label id="spnAscendingLoginIE" runat="server" style="font-family:Wingdings; font-weight:bolder;">&#233;</asp:Label><asp:Label id="spnDescndingLoginIE" runat="server" style="font-family:Wingdings; font-weight:bolder;">&#234;</asp:Label><% } else { %><asp:Label id="spnAscendingLogin" runat="server" style="font-family:Wingdings; font-weight:bolder; font-size:x-large;">&#8593;</asp:Label><asp:Label id="spnDescndingLogin" runat="server" style="font-family:Wingdings; font-weight:bolder; font-size:x-large;">&#8595;</asp:Label><% } %></asp:LinkButton>
                                        </td></tr>
                                        </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                            <table  width="100%">
                                  <tr>
                                   <td width="20%" align="left" valign="top" >
                                            <asp:Label ID="lbllogin" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.login") %>'></asp:Label>
                                        </td>
                                        </tr>
                                        </table>
                            </ItemTemplate>
                            </asp:TemplateColumn>
                             <asp:TemplateColumn ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" ItemStyle-HorizontalAlign="Left"><%--ZD 103415--%>
                            <HeaderTemplate>
                            <table width="100%" >
                            <tr>
                                         <td width="20%" runat="server" id="tdEmail" align="left" class="tableHeader">
                                           <asp:Literal ID="Literal12" Text="<%$ Resources:WebResources, Email%>" runat="server"></asp:Literal>
                                           <asp:LinkButton ID="LinkButton2" runat="server" CommandArgument="4" OnCommand="SortGrid" OnClientClick="ChangeSortingOrder(this.childNodes[0].id);"><%if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("TRIDENT")) { %><asp:Label id="spnAscendingEmailIE" runat="server" style="font-family:Wingdings; font-weight:bolder;">&#233;</asp:Label><asp:Label id="spnDescndingEmailIE" runat="server" style="font-family:Wingdings; font-weight:bolder;">&#234;</asp:Label><% } else { %><asp:Label id="spnAscendingEmail" runat="server" style="font-family:Wingdings; font-weight:bolder; font-size:x-large;">&#8593;</asp:Label><asp:Label id="spnDescndingEmail" runat="server" style="font-family:Wingdings; font-weight:bolder; font-size:x-large;">&#8595;</asp:Label><% } %></asp:LinkButton> 
                                        </td></tr>
                                        </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                            <table  width="100%">
                                  <tr>
                                   <td width="20%" align="left" valign="top" >
                                            <asp:Label ID="lblemail" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.email") %>'></asp:Label>
                                        </td>
                                        </tr>
                                        </table>
                            </ItemTemplate>
                            </asp:TemplateColumn>
                           
                            <asp:TemplateColumn ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" ItemStyle-HorizontalAlign="Left"><%--ZD 103415--%>
                              <HeaderTemplate>
                                <table width="100%" >
                                <tr>
                                        <td width="20%" runat="server" id="tdRole" align="left" class="tableHeader">
                                           <asp:Literal Text="<%$Resources:WebResources,  ManageUserRoles_RoleName%>" runat="server"></asp:Literal>
                                           <asp:LinkButton ID="LinkButton3" runat="server" CommandArgument="5" OnCommand="SortGrid" OnClientClick="ChangeSortingOrder(this.childNodes[0].id);"><%if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("TRIDENT")) {%><asp:Label id="spnAscendingRoleIE" runat="server" style="font-family:Wingdings; font-weight:bolder;">&#233;</asp:Label><asp:Label id="spnDescndingRoleIE" runat="server" style="font-family:Wingdings; font-weight:bolder;">&#234;</asp:Label><% } else { %><asp:Label id="spnAscendingRole" runat="server" style="font-family:Wingdings; font-weight:bolder; font-size:x-large;">&#8593;</asp:Label><asp:Label id="spnDescndingRole" runat="server" style="font-family:Wingdings; font-weight:bolder; font-size:x-large;">&#8595;</asp:Label><% } %></asp:LinkButton>
                                        </td>
                                        </tr>
                                </table>
                              </HeaderTemplate>
                                <ItemTemplate>
                                 <table  width="100%">
                                  <tr> 
                                        <td width="20%" align="left" valign="top" >
                                           <asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.RoleName").ToString() %>' Visible='<%# !DataBinder.Eval(Container, "DataItem.RoleName").ToString().Trim().Equals("") %>' CssClass="tableBody"></asp:Label>
                                        </td>
                                     </tr>
                                  </table>
                                    <%--ZD 103405 End--%>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, Actions%>" ItemStyle-CssClass="tableBody" ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Left">
                                <HeaderStyle CssClass="tableHeader" HorizontalAlign="left" />
                                <ItemTemplate>
                                <%--ZD 100263 Start--%>
                                <% if (Session["roomCascadingControl"].ToString().Equals("1"))
                                  {%>
                                    <asp:LinkButton runat="server" text="<%$ Resources:WebResources, ManageUser_btnEdit1%>" visible='<%# Session["admin"].ToString().Trim().Equals("2") && txtType.Text.Equals("1") && !DataBinder.Eval(Container, "DataItem.userID").ToString().Trim().Equals("11") && !DataBinder.Eval(Container, "DataItem.userID").ToString().Trim().Equals(Session["userID"].ToString()) && (Session["UsrCrossAccess"].ToString().Trim().Equals("1") || !DataBinder.Eval(Container, "DataItem.crossaccess").ToString().Trim().Equals("1") || DataBinder.Eval(Container, "DataItem.level").ToString().Trim().Equals("3")) %>' id="btnEdit1" commandname="Edit" onclientclick="DataLoading(1)"></asp:LinkButton> <%--FB 2670--%><%--ZD 100176--%> 
                                    <asp:LinkButton runat="server" text="<%$ Resources:WebResources, ManageUser_btnDelete1%>" visible='<%# (Session["admin"].ToString().Trim().Equals("2")) && (!txtType.Text.Equals("3")) && !DataBinder.Eval(Container, "DataItem.userID").ToString().Trim().Equals("11") && !DataBinder.Eval(Container, "DataItem.userID").ToString().Trim().Equals(Session["userID"].ToString()) && (Session["UsrCrossAccess"].ToString().Trim().Equals("1") || !DataBinder.Eval(Container, "DataItem.crossaccess").ToString().Trim().Equals("1") || DataBinder.Eval(Container, "DataItem.level").ToString().Trim().Equals("3")) %>' id="btnDelete1" commandname="Delete"></asp:LinkButton> <%--FB 2670--%><%--ZD 100176--%> 
                                    <asp:LinkButton runat="server" text="<%$ Resources:WebResources, ManageUser_btnLock1%>" visible='<%# (Session["admin"].ToString().Trim().Equals("2") || Session["admin"].ToString().Trim().Equals("1"))  && txtType.Text.Equals("1") && !DataBinder.Eval(Container, "DataItem.userID").ToString().Trim().Equals("11") && !DataBinder.Eval(Container, "DataItem.userID").ToString().Trim().Equals(Session["userID"].ToString()) && (Session["UsrCrossAccess"].ToString().Trim().Equals("1") || !DataBinder.Eval(Container, "DataItem.crossaccess").ToString().Trim().Equals("1") || DataBinder.Eval(Container, "DataItem.level").ToString().Trim().Equals("3"))%>' id="btnLock1" commandname="Cancel"></asp:LinkButton><%--FB 2670--%><%--ZD 100176--%> 
                                    <asp:LinkButton runat="server" text="<%$ Resources:WebResources, ManageUser_btnDeleteInactive1%>" visible='<%# (txtType.Text.Equals("3") && (Application["ssoMode"].ToString().ToUpper().Equals("NO"))) %>' commandname="Update" commandargument="1" id="btnDeleteInactive1"></asp:LinkButton>
                                    <asp:LinkButton runat="server" text="<%$ Resources:WebResources, ManageUser_btnReset1%>" visible='<%# txtType.Text.Equals("3") %>' commandname="Update" commandargument="2" id="btnReset1"></asp:LinkButton>
                                 <%}
                                 else
                                 {%>
                                 <asp:LinkButton runat="server" text="<%$ Resources:WebResources, ManageUser_btnEdit0%>" visible='<%# Session["admin"].ToString().Trim().Equals("2") && txtType.Text.Equals("1") %>' enabled='<%# !DataBinder.Eval(Container, "DataItem.userID").ToString().Trim().Equals("11") && !DataBinder.Eval(Container, "DataItem.userID").ToString().Trim().Equals(Session["userID"].ToString()) && (Session["UsrCrossAccess"].ToString().Trim().Equals("1") || !DataBinder.Eval(Container, "DataItem.crossaccess").ToString().Trim().Equals("1") || DataBinder.Eval(Container, "DataItem.level").ToString().Trim().Equals("3")) %>' id="btnEdit0" commandname="Edit" onclientclick="fnCheck(this);"></asp:LinkButton> <%--FB 2670--%><%--ZD 100176--%>  <%--ZD 102363--%>
                                    <asp:LinkButton runat="server" text="<%$ Resources:WebResources, ManageUser_btnDelete0%>" visible='<%# (Session["admin"].ToString().Trim().Equals("2")) && (!txtType.Text.Equals("3")) %>' enabled='<%# !DataBinder.Eval(Container, "DataItem.userID").ToString().Trim().Equals("11") && !DataBinder.Eval(Container, "DataItem.userID").ToString().Trim().Equals(Session["userID"].ToString()) && (Session["UsrCrossAccess"].ToString().Trim().Equals("1") || !DataBinder.Eval(Container, "DataItem.crossaccess").ToString().Trim().Equals("1") || DataBinder.Eval(Container, "DataItem.level").ToString().Trim().Equals("3")) %>' id="btnDelete0" commandname="Delete"></asp:LinkButton> <%--FB 2670--%><%--ZD 100176--%> 
                                    <asp:LinkButton runat="server" text="<%$ Resources:WebResources, ManageUser_btnLock0%>" visible='<%# (Session["admin"].ToString().Trim().Equals("2") || Session["admin"].ToString().Trim().Equals("1"))  && txtType.Text.Equals("1") %>' enabled='<%# !DataBinder.Eval(Container, "DataItem.userID").ToString().Trim().Equals("11") && !DataBinder.Eval(Container, "DataItem.userID").ToString().Trim().Equals(Session["userID"].ToString()) && (Session["UsrCrossAccess"].ToString().Trim().Equals("1") || !DataBinder.Eval(Container, "DataItem.crossaccess").ToString().Trim().Equals("1") || DataBinder.Eval(Container, "DataItem.level").ToString().Trim().Equals("3"))%>' id="btnLock0" commandname="Cancel"></asp:LinkButton><%--FB 2670--%><%--ZD 100176--%> 
                                    <asp:LinkButton runat="server" text="<%$ Resources:WebResources, ManageUser_btnDeleteInactive0%>" visible='<%# (txtType.Text.Equals("3") && (Application["ssoMode"].ToString().ToUpper().Equals("NO"))) %>' commandname="Update" commandargument="1" id="btnDeleteInactive0"></asp:LinkButton>
<%--                                <asp:LinkButton runat="server" Text="Delete" Visible='<%# (txtType.Text.Equals("2")) %>' CommandName="Update" CommandArgument="9" ID="btnDeleteGuest">
--%>                                <asp:LinkButton runat="server" text="<%$ Resources:WebResources, ManageUser_btnReset0%>" visible='<%# txtType.Text.Equals("3") %>' commandname="Update" commandargument="2" id="btnReset0"></asp:LinkButton>
                                    <%}%>
                                 <%--ZD 100263 End--%>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="crossaccess" Visible="false" ><HeaderStyle CssClass="tableHeader" HorizontalAlign="left" /></asp:BoundColumn>
                            <asp:BoundColumn DataField="ErrorMsg" Visible="false"><HeaderStyle CssClass="tableHeader" HorizontalAlign="left" /></asp:BoundColumn>  <%--ALLDEV-498--%>
                        </Columns>
                    </asp:DataGrid>
                    <asp:Table runat="server" ID="tblNoUsers" Visible="false" Width="90%">
                        <asp:TableRow CssClass="lblError">
                            <%--Windows Dressing--%>
                            <asp:TableCell CssClass="lblError" HorizontalAlign="center">
                                <asp:Literal ID="Literal2" Text="<%$ Resources:WebResources, NoUserFound%>" runat="server"></asp:Literal> <%--Edited for FB 1405--%>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>                    
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table width="90%" cellspacing="0" cellpadding="0" border="0">
                        <tr>
                             <%--FB 2919--%>
                            <%--Windows Dressing--%>
                            <td width="40%" valign="middle" align="left" id="tdLegends" runat="server" class="blackblodtext"><%--FB 2579--%><%--ZD 103415--%>
                               
                            </td>
                            <td align="right" width="15%">
								<%--ZD 100420--%>
                                <%--<asp:Button ID="btnDeleteAllGuest" Text="<%$ Resources:WebResources, ManageUser_btnDeleteAllGuest%>" Width="160pt" runat="server" OnClick="DeleteAllGuest"  OnClientClick="DataLoading(1)"/>--%> 
                                <button ID="btnDeleteAllGuest" style="width:160pt" runat="server" onserverclick="DeleteAllGuest"  onclick="DataLoading(1);"><asp:Literal ID="Literal8" Text="<%$ Resources:WebResources, ManageUser_btnDeleteAllGuest%>" runat="server"></asp:Literal></button><%--FB 2664--%><%--ZD 100176--%> <%--ZD 100420--%>
								<%--ZD 100420--%>
                        <%--Windows Dressing--%>
                                <b class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUser_TotalUsers%>" runat="server"></asp:Literal></b><b><asp:Label id="lblTotalUsers" runat="server" text=""></asp:Label> </b>
                            </td>
                            <td  align="right" width="15%" runat="server" id="tdLicencesRemaining">
                        <%--Windows Dressing--%>
				                <b class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUser_LicensesRemain%>" runat="server"></asp:Literal></b><b><asp:Label id="lblLicencesRemaining" runat="server" text=""></asp:Label> </b>                                
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Table id="tblPage" visible="false" runat="server">
                        <asp:TableRow id="TableRow1" runat="server">
                            <asp:TableCell id="TableCell1" font-bold="True" font-names="Verdana" font-size="Small" forecolor="Blue" runat="server">
                            <span class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUser_Pages%>" runat="server"></asp:Literal></span> </asp:TableCell>
                            <asp:TableCell id="TableCell2" runat="server"></asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>

                </td>
            </tr>
            <tr id="trSearchH" runat="server">
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <SPAN class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUser_SearchUsers%>" runat="server"></asp:Literal></SPAN>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trSearch" runat="server">
                <td align="center">
                    <table width="80%" cellpadding="2" cellspacing="5">
                        <tr>
                             <%--Window Dressing --%>                        
                            <td align="right" style="font-weight:bold" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUser_FirstName%>" runat="server"></asp:Literal></td>
                            <td align="left">
                                <asp:TextBox ID="txtFirstName" CssClass="altText" runat="server"></asp:TextBox>
                                 <asp:RegularExpressionValidator ID="regTemplateName" ControlToValidate="txtFirstName" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters43%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+?|!`\[\]{}\=@#$%&~]*$"></asp:RegularExpressionValidator><%--FB 1888--%>
                            </td>
                            <%--Window Dressing --%>                        
                            <td align="right" style="font-weight:bold" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUser_LastName%>" runat="server"></asp:Literal></td>
                            <td align="left">
                                <asp:TextBox ID="txtLastName" CssClass="altText" runat="server"></asp:TextBox>
                                 <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtLastName" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters43%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+?|!`\[\]{}\=@#$%&~]*$"></asp:RegularExpressionValidator><%--FB 1888--%>
                            </td>
                            <%--Window Dressing --%>                        
                            <td align="right" style="font-weight:bold" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUser_EmailAddress%>" runat="server"></asp:Literal></td>
                            <td align="left">
                                <asp:TextBox ID="txtEmailAddress" CssClass="altText" runat="server"></asp:TextBox>
                                 <asp:RegularExpressionValidator ID="RegularExpressionValidator2" ControlToValidate="txtEmailAddress" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters44%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:#$%&()'~]*$"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trSearchB" runat="server">
                <td align="center">
                    <table width="90%">
                        <tr>
                            <td width="50%">&nbsp;</td>
                            <td align="center"> <%--FB 2920--%>
                                <asp:Button id="btnSearchUser" onclientclick="javascript:return fnSearch();" onclick="SearchUser" runat="server" cssclass="altLongBlueButtonFormat" text="<%$ Resources:WebResources, ManageUser_btnSearchUser%>"></asp:Button> <%--Edited for FB 1405--%>
                            </td>
                            
                        </tr>
                    </table>
                </td>
            </tr>
            <%--Commented for ZD 100926--%>
            
          <%--<tr id="trNewH" runat="server">
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <SPAN class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUser_CreateNewUser%>" runat="server"></asp:Literal></SPAN>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>--%>
            <tr id="trNew" runat="server">
                <td align="center">
                    <table width="90%">
                        <tr>
                            <td width="50%">&nbsp;</td>
                            <td align="center"> <%--FB 2920--%>
                                <asp:Button id="btnNewMCU" onclick="CreateNewUser" runat="server" cssclass="altLongBlueButtonFormat" text="<%$ Resources:WebResources, ManageUser_CreateNewUser%>" onclientclick="DataLoading(1)"></asp:Button>  <%--ZD 100176--%> 
                            </td>
                            
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>   

<%--ALLDEV-498 Starts--%>
    <div id="modalPopup" runat="server">        
        <ajax:ModalPopupExtender ID="modalPopupExtend" TargetControlID="btnDummy"
            PopupControlID="modalPopupPanel" CancelControlID="btnCancel"
            DropShadow="false" BackgroundCssClass="modalBackground" runat="server">
        </ajax:ModalPopupExtender>
        <asp:Panel ID="modalPopupPanel" Width="65%" Height="90%" HorizontalAlign="Center" CssClass="treeSelectedNode" runat="server">
            <asp:UpdatePanel ID="modalPopupUpdatePanel" RenderMode="Inline" UpdateMode="Conditional" runat="server">
                <Triggers>
                </Triggers>
                <ContentTemplate>
                    <div align="left" id="Div1" style="width: 100%; height: 100%;background-color:White;border-color:Blue;overflow:auto;">
                        <table border="0" cellpadding="2" cellspacing="0" width="95%" style="height:100%;" align="left">
                            <tr>
                                <td colspan="2" class="subtitleblueblodtext">
                                    <asp:Literal ID="Literal4" Text="<%$ Resources:WebResources, MngUsrFieldH1%>" runat="server"></asp:Literal>:                                    
                                </td>
                            </tr>    
                            <tr>
                                <td class="blackblodtext" style="width:1%;">.</td>
                                <td>
                                    <asp:Literal Text="<%$ Resources:WebResources, ManageCustomAttribute_ChkSysApp%>" runat="server"></asp:Literal>                                    
                                </td>
                            </tr>     
                            <tr>
                                <td class="blackblodtext">.</td>
                                <td>
                                    <asp:Literal Text="<%$ Resources:WebResources, MCUAdministrator%>" runat="server"></asp:Literal>                                    
                                </td>
                            </tr>    
                            <tr>
                                <td class="blackblodtext">.</td>
                                <td>
                                    <asp:Literal Text="<%$ Resources:WebResources, ManageCustomAttribute_ChkRoomApp%>" runat="server"></asp:Literal>                                    
                                </td>
                            </tr>  
                            <tr>
                                <td class="blackblodtext">.</td>
                                <td>
                                    <asp:Literal Text="<%$ Resources:WebResources, RoomAdministrator%>" runat="server"></asp:Literal>                                    
                                </td>
                            </tr>      
                            <tr>
                                <td class="blackblodtext">.</td>
                                <td>
                                    <asp:Literal Text="<%$ Resources:WebResources, LDAP_Group%>" runat="server"></asp:Literal>                                    
                                </td>
                            </tr>        
                            <tr>
                                <td class="blackblodtext">.</td>
                                <td>
                                    <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_ConferenceTemp%>" runat="server"></asp:Literal>                                    
                                </td>
                            </tr>  
                            <tr>
                                <td class="blackblodtext">.</td>
                                <td>
                                    <asp:Literal Text="<%$ Resources:WebResources, Conference%>" runat="server"></asp:Literal>                                    
                                    <asp:Literal Text="<%$ Resources:WebResources, MngUsrField1%>" runat="server"></asp:Literal>                                    
                                </td>
                            </tr> 
                            <tr>
                                <td colspan="2" class="subtitleblueblodtext">
                                    <asp:Literal Text="<%$ Resources:WebResources, MngUsrFieldH2%>" runat="server"></asp:Literal>:
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <table width="100%"  cellpadding="5" cellspacing="0">
                                        <tr>
                                            <td class="blackblodtext">.
                                                <asp:Literal Text="<%$ Resources:WebResources, ManageCustomAttribute_ChkSysApp%>" runat="server"></asp:Literal>:
                                            </td>
                                            <td>
                                                <asp:Literal Text="<%$ Resources:WebResources, MngUsrField2%>" runat="server"></asp:Literal>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="blackblodtext">.
                                                <asp:Literal Text="<%$ Resources:WebResources, MCUAdministrator%>" runat="server"></asp:Literal>:
                                            </td>
                                            <td>
                                                <asp:Literal Text="<%$ Resources:WebResources, MngUsrField3%>" runat="server"></asp:Literal>
                                            </td>
                                        </tr>
                                         <tr>
                                            <td class="blackblodtext">.
                                                <asp:Literal Text="<%$ Resources:WebResources, ManageCustomAttribute_ChkRoomApp%>" runat="server"></asp:Literal>:
                                            </td>
                                            <td>
                                                <asp:Literal Text="<%$ Resources:WebResources, MngUsrField2%>" runat="server"></asp:Literal>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="blackblodtext">.
                                                <asp:Literal Text="<%$ Resources:WebResources, LDAP_Group%>" runat="server"></asp:Literal>:
                                            </td>
                                            <td>
                                                <asp:Literal Text="<%$ Resources:WebResources, MngUsrField2%>" runat="server"></asp:Literal>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="blackblodtext">.
                                                <asp:Literal Text="<%$ Resources:WebResources, PrivateConferenceTemplate%>" runat="server"></asp:Literal>:
                                            </td>
                                            <td>
                                                <asp:Literal Text="<%$ Resources:WebResources, MngUsrField4%>" runat="server"></asp:Literal>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="blackblodtext">.
                                               <asp:Literal Text="<%$ Resources:WebResources, PublicConferenceTemplate%>" runat="server"></asp:Literal>:
                                            </td>
                                            <td>
                                                <asp:Literal Text="<%$ Resources:WebResources, MngUsrField5%>" runat="server"></asp:Literal>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="blackblodtext">.
                                                <asp:Literal Text="<%$ Resources:WebResources, ConferenceRequester%>" runat="server"></asp:Literal>:
                                            </td>
                                            <td>
                                                <asp:Literal Text="<%$ Resources:WebResources, MngUsrField6%>" runat="server"></asp:Literal>
                                            </td>
                                        </tr>
                                         <tr>
                                            <td class="blackblodtext">.
                                                <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_Field4%>" runat="server"></asp:Literal>:
                                            </td>
                                            <td>
                                                <asp:Literal Text="<%$ Resources:WebResources, MngUsrField7%>" runat="server"></asp:Literal>
                                            </td>
                                        </tr>
                                          <tr>
                                            <td class="blackblodtext">.
                                                <asp:Literal Text="<%$ Resources:WebResources, ConferenceParticipant%>" runat="server"></asp:Literal>:
                                            </td>
                                            <td>
                                                <asp:Literal Text="<%$ Resources:WebResources, MngUsrField8%>" runat="server"></asp:Literal>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:Literal Text="<%$ Resources:WebResources, MngUsrField9%>" runat="server"></asp:Literal> <asp:Literal Text="<%$ Resources:WebResources, MngUsrField10%>" runat="server"></asp:Literal>
                                    <asp:Literal Text="<%$ Resources:WebResources, MngUsrField11%>" runat="server"></asp:Literal>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" align="center">
                                    <table width="100%" border="0">
                                        <tr>
                                            <td align="center">
                                                <asp:Button ID="btnViewReport" Text="<%$ Resources:WebResources, ReportDetails_btnviewRep%>" CssClass="altMedium0BlueButtonFormat" OnClientClick="javascript:CallDummyClick('V');"   runat="server"/>    
                                            </td>
                                            <td align="center">
                                                <asp:Button ID="btnDeleteUser" Text="<%$ Resources:WebResources, DeleteUser%>" CssClass="altMedium0BlueButtonFormat" style="width:180px;" OnClientClick="javascript:CallDummyClick('D');" runat="server"/>
                                            </td>
                                            <td align="center">
                                                <asp:Button ID="btnCancel" Text="<%$ Resources:WebResources, Cancel%>" CssClass="altMedium0BlueButtonFormat" OnClientClick="javascript:return HideModalPopup();" runat="server"/>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>                                
                        </table>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
        <asp:Button ID="btnDummy2" BackColor="Transparent" runat="server" OnClick="DeleteUserAssignedDetails" />
        <asp:Button ID="btnDummy1" BackColor="Transparent" runat="server" OnClick="ViewAssignedUserReport" />
        <input type="button" id="btnDummy" style="display:none" runat="server"/>
    </div>
<%--ALLDEV-498 Ends--%>

<%--<asp:TextBox ID="txtBridges" runat="server" Width="0" Height="0" ForeColor="transparent" BackColor="transparent" BorderColor="transparent"></asp:TextBox>--%> <%--ZD 100369--%>
  <input type="hidden" name="Bridges" width="200">
  <asp:TextBox ID="txtType" Visible="false" CssClass="altText" runat="server" />

<img style="display:none" src="keepalive.asp" name="myPic" width="0px" height="0px" alt="keepalive"><%--ZD 100419--%> <%--ZD 100369--%>
    </form>
    <%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
<%--ZD 100420 Start--%>
<script language="javascript">
    if (document.getElementById('txtEmailAddress') != null)
        document.getElementById('txtEmailAddress').setAttribute("onblur", "document.getElementById('btnSearchUser').focus(); document.getElementById('btnSearchUser').setAttribute('onfocus', '');");
    if (document.getElementById('btnSearchUser') != null)
        document.getElementById('btnSearchUser').setAttribute("onblur", "document.getElementById('btnNewMCU').focus(); document.getElementById('btnNewMCU').setAttribute('onfocus', '');");

    //ZD 100393 start
    if (document.getElementById("tblNoUsers") != null)
        document.getElementById("divmanageuserheight").style.height = "400px"
    if (document.getElementById("lblTotalUsers").innerHTML == "1" || document.getElementById("lblTotalUsers").innerHTML == "2")
        document.getElementById("divmanageuserheight").style.height = "500px" // ZD 100873
    //ZD 100393 End

    //ZD 102363
    function fnCheck(Obj) {
        if (Obj.getAttribute('disabled') == "disabled")
            return false;
        else
            DataLoading(1);
    }
    
    //ALLDEV-498 Starts
    function showModelPopup() {
        var args = showModelPopup.arguments;
        document.getElementById("hdnDeleteUserID").value = args[0];  
        document.getElementById("btnDummy").click();
        return false;
    }
    function showConfAdminError() {
        document.getElementById("errLabel").innerText = ConfAdminDelete;
        document.getElementById("errLabel").innerHTML = ConfAdminDelete;
        return false;
    }
    
    function CallDummyClick() {
        var args = CallDummyClick.arguments;
        HideModalPopup();
        if(args[0] == "V")
            document.getElementById("btnDummy1").click();
        else if(args[0] == "D")
            document.getElementById("btnDummy2").click();
    }
    
    function HideModalPopup() {
        $find("modalPopupExtend").hide();
        return true;
    }

    document.onkeydown = EscClosePopup;

    function EscClosePopup() {
        if (document.getElementById('modalPopupExtend') != null) {
            $find("modalPopupExtend").hide();
            return true;
        }
    }
    //ALLDEV-498 Ends
</script>
<%--ZD 100420 End--%>
    <!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
</body>
</html>

