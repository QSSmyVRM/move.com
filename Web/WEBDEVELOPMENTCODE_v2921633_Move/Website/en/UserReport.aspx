﻿<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.		
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 ZD 100866 End--%>
<%@ Page Language="C#"  Inherits="en_UserReport" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=8" /> <!-- FB 2050 --> <%--FB 2779--%>
<!-- #INCLUDE FILE="inc/maintopNet.aspx" --> 
<%
    if(Session["userID"] == null)
    {
        Response.Redirect("genlogin.asp");

    }    
%>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>myVRM</title>
    <meta name="Description" content="myVRM (Videoconferencing Resource Management) is a revolutionary Web-based software application that manages any video conferencing environment." />
  <meta name="Keywords" content="VRM, myVRM, Videoconferencing Resource Management, video conferencing, video bridges, video endpoints" />
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  
  <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/border-table.css" />
  <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/main-table.css" />
  <script type="text/javascript" src="extract.js"></script>
<script type="text/javascript" src="sorttable.js"></script>
<script type="text/javascript" src="inc/functions.js"></script> <%-- ZD 102723 --%>

<!--------------------------------- CONTENT GOES HERE --------------->

<script type="text/javascript">

tabs=new Array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","ALL")

function typeimg(sb)
{
	imgsrc = document.getElementById("img_" + sb).src;
	sorting = (imgsrc.indexOf(".gif") == -1) ? 0 : ((imgsrc.indexOf("up.gif")==-1) ? -1 : 1);
	for (i = 1; i < 5; i++) {
		document.getElementById("img_" + i).src = "image/bg.gif";
	}
	document.getElementById("img_" + sb).src = ((sorting == 0) ? "image/sort_up.gif" : ((sorting == 1) ? "image/sort_up.gif" : "image/sort_up.gif")) ;
}
	

function nameimg(sel)
{
	for (i in tabs) {
		document.getElementById("u" + tabs[i]).style.backgroundColor = ""; 
	}

	document.getElementById( "u" + sel ).style.backgroundColor = "#FF6699"; 
}

function userreport(userid)
{
	url = "UserReportChart.aspx?uid=" + userid  + "&wintype=pop";
	window.open(url, "userreportchart", "width=600,height=300,resizable=yes,scrollbars=yes,status=no");
}

function sortList(sortBy)
{
	document.frmUserreport.sortBy.value = sortBy;
		
	imgsrc = document.getElementById("img_" + sortBy).src;
		
	sorting = (imgsrc.indexOf(".gif") == -1) ? 0 : ((imgsrc.indexOf("up.gif")==-1) ? -1 : 1);
	for (i = 1; i < 4; i++) {
		document.getElementById("img_" + i).src = "image/bg.gif";
	}
	document.getElementById("img_" + sortBy).src = ((sorting == 0) ? "image/sort_up.gif" : ((sorting == 1) ? "image/sort_down.gif" : "image/sort_up.gif")) ;
		
	//window.location.href = "dispatcher/admindispatcher.asp?cmd=UserReport&al=" + document.frmUserreport.alphabet.value + "&sb=" + sortBy;
	window.location.href = "UserReport.aspx?cmd=UserReport&al=" + document.frmUserreport.alphabet.value + "&sb=" + sortBy;

		
}

function selname(sel)
{
	document.frmUserreport.alphabet.value = sel;
	document.getElementById( "u" + sel ).style.backgroundColor = "#FF6699"; 
	//window.location.href = "dispatcher/admindispatcher.asp?cmd=UserReport&al=" + sel + "&sb=" + document.frmUserreport.sortBy.value;
	window.location.href = "UserReport.aspx?cmd=UserReport&al=" + sel + "&sb=" + document.frmUserreport.sortBy.value;
	
	
}

function pageChg(cb)
{
	var sel = document.frmUserreport.alphabet.value;
	var sortBy = document.frmUserreport.sortBy.value;
	//window.location.href = "dispatcher/admindispatcher.asp?cmd=UserReport&al=" + sel + "&sb=" + sortBy + "&pg=" + cb.options[cb.selectedIndex].value;
	window.location.href = "UserReport.aspx?cmd=UserReport&al=" + sel + "&sb=" + sortBy + "&pg=" + cb.options[cb.selectedIndex].value;
	
}

function closePage()
{
	window.location.href = "dispatcher/gendispatcher.asp?cmd=GetSettingsSelect"
}

</script>
</head>
<center>
 <h3>User Reports</h3>
</center>

<body>
   <form name="frmUserreport" method="POST" action="dispatcher/admindispatcher.asp">
    <input type="hidden" name="cmd" value="UserReport"> 
    <input name="sortBy" id="sortBy" type="hidden"  runat="server" />
    <input name="alphabet" id="alphabet" type="hidden"  runat="server" />
    <input name="pageNo" id="pageNo" type="hidden"  runat="server" />
    <input name="totalPages" id="totalPages" type="hidden"  runat="server" />  
    <input type="hidden" name="xmljs" id="xmljs" runat="server" />  
    <input type="hidden" id="helpPage" value="83">
    <center>
      <table width="900" border="0" cellspacing="4" cellpadding="4">
      <tr align="center">
            <td align="center">
                <asp:Label ID="LblError" ForeColor="red" runat="server" Font-Bold="true" Font-Size="small"></asp:Label>
            </td>
        </tr>
        <tr>
          <td align="center">
          <table width="800" border="0">
             <tr>
               <td align="left" width="475">
                  <%--Window Dressing--%>
                 <font name="Verdana" size=2 class="blackblodtext"><b>Starts With:</b></font>
                 <a onclick="JavaScript: selname('A')"><span class="tabtext" id="uA">0-A</span></a>
                 <a onclick="JavaScript: selname('B')"><span class="tabtext" id="uB">B</span></a>
                 <a onclick="JavaScript: selname('C')"><span class="tabtext" id="uC">C</span></a>
                 <a onclick="JavaScript: selname('D')"><span class="tabtext" id="uD">D</span></a>
                 <a onclick="JavaScript: selname('E')"><span class="tabtext" id="uE">E</span></a>
                 <a onclick="JavaScript: selname('F')"><span class="tabtext" id="uF">F</span></a>
                 <a onclick="JavaScript: selname('G')"><span class="tabtext" id="uG">G</span></a>
                 <a onclick="JavaScript: selname('H')"><span class="tabtext" id="uH">H</span></a>
                 <a onclick="JavaScript: selname('I')"><span class="tabtext" id="uI">I</span></a>
                 <a onclick="JavaScript: selname('J')"><span class="tabtext" id="uJ">J</span></a>
                 <a onclick="JavaScript: selname('K')"><span class="tabtext" id="uK">K</span></a>
                 <a onclick="JavaScript: selname('L')"><span class="tabtext" id="uL">L</span></a>
                 <a onclick="JavaScript: selname('M')"><span class="tabtext" id="uM">M</span></a>
                 <a onclick="JavaScript: selname('N')"><span class="tabtext" id="uN">N</span></a>
                 <a onclick="JavaScript: selname('O')"><span class="tabtext" id="uO">O</span></a>
                 <a onclick="JavaScript: selname('P')"><span class="tabtext" id="uP">P</span></a>
                 <a onclick="JavaScript: selname('Q')"><span class="tabtext" id="uQ">Q</span></a>
                 <a onclick="JavaScript: selname('R')"><span class="tabtext" id="uR">R</span></a>
                 <a onclick="JavaScript: selname('S')"><span class="tabtext" id="uS">S</span></a>
                 <a onclick="JavaScript: selname('T')"><span class="tabtext" id="uT">T</span></a>
                 <a onclick="JavaScript: selname('U')"><span class="tabtext" id="uU">U</span></a>
                 <a onclick="JavaScript: selname('V')"><span class="tabtext" id="uV">V</span></a>
                 <a onclick="JavaScript: selname('W')"><span class="tabtext" id="uW">W</span></a>
                 <a onclick="JavaScript: selname('X')"><span class="tabtext" id="uX">X</span></a>
                 <a onclick="JavaScript: selname('Y')"><span class="tabtext" id="uY">Y</span></a>
                 <a onclick="JavaScript: selname('Z')"><span class="tabtext" id="uZ">Z-</span></a>
                 <a onclick="JavaScript: selname('ALL')"><span class="tabtext" id="uALL">All</span></a>                 
               </td>             
              
               </td>
               <td align="right">
               <% if (Convert.ToInt32(totalPages.Value) > 1)
                 {
                     Response.Write("Go To  <select size='1' name='SelPage' onChange='pageChg(this);'>");
                     for (Int32 i = 1; i <= Convert.ToInt32(totalPages.Value); i++)
                     {
                         			
                         if (Convert.ToInt32(pageNo.Value) == i)
                             Response.Write("    <option value='" + i + "' selected='true'>page " + i + "</option>");
                         else
                             Response.Write("    <option value='" + i + "'>page " + i + "</option>");

                     }
                     Response.Write("  </select>");
                 }   %>
               </td>
             </tr>
           </table>
          </td>
        </tr>
        <tr>
          <td align=center colspan=2>
            <table width="90%" border="0" cellspacing="0" cellpadding="2" bordercolor="#FFFFFF" bordercolorlight="#FFFFFF">
              <%--Window Dressing Start--%>
              <tr >
                <td width="20%" align="center" class="tableHeader">
                   <a href="JavaScript: sortList('1')"><font size="1"><b>FIRST NAME</b></font></a>
					<img id="img_1" border="0" src="image/bg.gif" alt="Sort" width="10" height="10"/> <%--ZD 100419--%>
                </td>
                <td width="30%" align="center" class="tableHeader">
                  <a href="JavaScript: sortList('2')"><font size="1"><b>LAST NAME</b></font></a>
                  <img id="img_2" border="0" src="image/bg.gif" alt="Sort" width="10" height="10" /><%--ZD 100419--%>
                </td>
                <td width="20%" align="center" class="tableHeader">
				  <a href="JavaScript: sortList('3')"><font size="1"><b>LOGIN NAME</b></font></a>
				  <img id="img_3" border="0" src="image/bg.gif" alt="Sort"  width="10" height="10"/> <%--ZD 100419--%>
                </td>
                <td width="10%" align="center" class="tableHeader">
                  <a href="JavaScript: sortList('4')"><font size="1"><b>EMAIL</b></font></a>
                  <img id="img_4" border="0" src="image/bg.gif" alt="Sort"  width="10" height="10" /><%--ZD 100419--%>
                </td>
                <td width="20%" align="center" class="tableHeader">
                  <SPAN>Report</SPAN>
                </td>
                    <%--Window Dressing end--%>
              </tr>
            <%            
            if (Convert.ToInt32(totalPages.Value) > 0 )
        {
               String xmlstr = ""; 
               xmlstr = xmljs.Value; 
               string[] usersary = null;
               string[] usersary1 = null;
              
               String tdbgcolor= "";
                            
                usersary = xmlstr.Split(';');
                
	            for (Int32 i = 0; i < usersary.Length-1; i++) 
	              {	   
		             usersary1 =  usersary[i].Split('%');		             		              
		            if ((i % 2) == 0 )
			            tdbgcolor = "#E0E0E0";
		            else
			            tdbgcolor = "";
            		
		            Response.Write("              <tr>");
		            //Window Dressing - Start
		            Response.Write("                <td width='20%' align='center' class='tableBody' bgcolor='" + tdbgcolor + "'>" + usersary1[1] + "&nbsp;</td>");
		            Response.Write("                <td width='30%' align='center' class='tableBody' bgcolor='" + tdbgcolor + "'>" + usersary1[2] + "&nbsp;</td>");
		            Response.Write("                <td width='20%' align='center' class='tableBody' bgcolor='" + tdbgcolor + "'>" + usersary1[4] + "&nbsp;</td>");
		            Response.Write("                <td width='10%' align='center' class='tableBody' bgcolor='" + tdbgcolor + "'>" + usersary1[3] + "&nbsp;</td>");
		            Response.Write("                <td width='20%' align='center' class='tableBody' bgcolor='" + tdbgcolor + "'>");
		            //Window Dressing - End
		            Response.Write("                 <input type='button' onfocus='this.blur()' name='report' value='Show...' class='altShortBlueButtonFormat' onClick='userreport(\"" + usersary1[0] + "\");'> ");
		            Response.Write("                </td>");
		            Response.Write("              </tr>");
	            }
	        }
        else
        {
	        Response.Write("              <tr>");
	        Response.Write("                <td colspan='10'><font color=red>No users record.</font> </td>");
	        Response.Write("                </td>");
	        Response.Write("              </tr>");     	

        }%>           
              
         </table>
          </td>
        </tr>
      </table>

      <br><br>
     
    </center>

</form>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>

<script type="text/javascript">

	typeimg( document.frmUserreport.sortBy.value);
	nameimg( document.frmUserreport.alphabet.value);

</script>
</body>
</html>

<br />
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
<!-- #INCLUDE FILE="inc/mainbottomNet.aspx" -->

