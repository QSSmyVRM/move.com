/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 100886
using System;
using System.Collections;
using System.Collections.Generic;

namespace myVRM.DataLayer
{
    /// <summary>
    /// Extends the <see cref="IDao{T,IdT}" /> behavior, 
    /// </summary>
    public interface IUserDao : IDao<vrmUser, int>
    {
        vrmUser GetByUserId(int id);
        vrmUser GetByUserEmail(string emailID);//FB 2342
        vrmUser GetByRFIDValue(string RFID);//FB 2724
        vrmUser GetByUserRequestID(string RequestID);//ZD 100263
    } 
    public interface IGuestUserDao : IDao<vrmGuestUser, int> 
    { 
        vrmGuestUser GetByUserId(int id);
        vrmGuestUser GetByUserEmail(string emailID);
    }
    public interface IInactiveUserDao : IDao<vrmInactiveUser, int> { vrmInactiveUser GetByUserId(int id);}
//    public interface IEmailDao : IDao<vrmEmail, int> { }
    public interface IUserLotusDao : IDao<vrmUserLotusNotesPrefs, int> { } //FB 2027
    public interface IUserRolesDao : IDao<vrmUserRoles, int> { }
    public interface IVRMLdapUserDao : IDao<vrmLDAPUser, int> { }
    public interface IUserTemplateDao : IDao<vrmUserTemplate, int> { }
    public interface IUserAccountDao : IDao<vrmAccount, int> { vrmAccount GetByUserId(int id);} //FB 2027SetBulkUserAddMinutes
    public interface IUserSearchTemplateDao : IDao<vrmUserSearchTemplate, int> { }
    public interface IGrpDetailDao : IDao<vrmGroupDetails, int> { }
    public interface IGrpParticipantsDao : IDao<vrmGroupParticipant, int> { }
    public interface IAccountGroupListDAO : IDao<vrmAccountGroupList, int> { } //FB 2027
	public interface ILanguageTextDao : IDao<vrmLanguageText, int> { List<vrmLanguageText> GetByLanguageId(int id); } //FB 1830 - Translation
	//NewLobby
    public interface IUserLobbyDAO : IDao<vrmUserLobby, int>{ }
    public interface IUserPCDAO : IDao<vrmUserPC, int> { } //FB 2693
    //ZD 100152 Starts
	//ALLDEV-856
    public interface IUserTokenDAO : IDao<vrmUserToken, Tuple<int, String>> { 
        vrmUserToken GetByUserId(Tuple<int, String> info);

    } //ZD 100152 Ends
    //ZD 103878
    public interface IUserDeleteListDAO : IDao<vrmUserDeleteList, int>
    {
        vrmUserDeleteList GetByuId(int id);
    }
}
