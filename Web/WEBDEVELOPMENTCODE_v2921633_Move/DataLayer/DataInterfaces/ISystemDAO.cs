/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 100886
using System;
using System.Collections;
using System.Collections.Generic;

namespace myVRM.DataLayer
{
    /// <summary>
    /// Extends the <see cref="IDao{T,IdT}" /> behavior, 
    /// </summary>
    public interface ISystemDAO : IDao<sysData, int> { sysData GetByAdminId(int id); }
    public interface ISysTimeZonePrefDAO : IDao<sysTimeZonePref, int> { List<sysTimeZonePref> GetAllPrefTimeZones(string ConfigPath);}
    public interface ISysMailDAO : IDao<sysMailData, int> { sysMailData GetByMailId(int id); }
    public interface IAccSchemeDAO : IDao<sysAccScheme, int> { }
    public interface IExternalSchedulingDAO : IDao<sysExternalScheduling, int> { }//FB 2363
    public interface IESMailUsrRptSettingsDAO : IDao<ESMailUsrRptSettings, int> { }//FB 2363
    public interface IsysWhygoSettings : IDao<sysWhygoSettings, int> { }//FB 2392
    public interface IDefaultLicenseDAO : IDao<vrmDefaultLicense, int> { }//FB 2659
}
