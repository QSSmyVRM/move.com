/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886
using System;
using System.Collections;
using System.Globalization;

namespace myVRM.DataLayer
{
    /// <summary>
    /// vrmDataObject with all relevent data info  
    /// 
    /// </summary>
    public class vrmDataObject
    {
        #region Private Internal Members

        private int m_ID;
        private string m_inXml;
        private string m_outXml;
        private string m_ConfigPath;
        private static log4net.ILog m_log;

        #endregion

        #region Public Properties

        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        public string inXml
        {
            get { return m_inXml; }
            set { m_inXml = value; }
        }
        public string outXml
        {
            get { return m_outXml; }
            set { m_outXml = value; }
        }
        public string ConfigPath
        {
            get { return m_ConfigPath; }
            set { m_ConfigPath = value; }
        }

        public log4net.ILog log
        {
            get { return m_log; }
            set { m_log = value; }
        }


        #endregion

        public vrmDataObject(string newPath)
        {
            ConfigPath = newPath;
            inXml = string.Empty;
            outXml = string.Empty;
        }
        public vrmDataObject()
        {
            ConfigPath = string.Empty;
            inXml = string.Empty;
            outXml = string.Empty;
        }
    }
    public class FetchData
    {
        public string name;
        public NHibernate.FetchMode type;
        public string alias;
    }

    [Serializable]
    public class confKey
    {
        private int m_confid = 0;
        private int m_instanceid = 0;

        #region ~ Properties ~
        public int confid
        {
            get { return m_confid; }
            set { m_confid = value; }
        }

        public int instanceid
        {
            get { return m_instanceid; }
            set { m_instanceid = value; }
        }
        #endregion

        #region ~ Constructor ~

        public confKey(int confid, int instanceid)
        {
            m_confid = confid;
            m_instanceid = instanceid;
        }
        public confKey() { }

        #endregion

        #region ~ Object overrides ~

        public override bool Equals(object obj)
        {
            if (obj == this) return true;

            if (obj == null || obj.GetType() != this.GetType()) return false;

            confKey test = (confKey)obj;

            return (m_confid == test.confid || (m_confid.Equals(test.m_confid)) &&
                    m_instanceid == test.m_instanceid || (m_instanceid.Equals(test.m_instanceid)));
        }

        public override int GetHashCode()
        {
            return confid.GetHashCode() ^ instanceid.GetHashCode();
        }
        #endregion
    }
}
