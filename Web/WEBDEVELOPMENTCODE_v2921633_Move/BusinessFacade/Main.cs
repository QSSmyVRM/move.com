//ZD 100147 Start
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End ZD 100886
using System;
using System.Xml;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;

using NHibernate;
using NHibernate.Cfg;
using NHibernate.Criterion;
using log4net;

using myVRM.BusinessLayer;
using myVRM.DataLayer;

namespace ASPIL
{

    public interface IVRMSERVER
    {
        string Operations(string configPath, string operation, string inXML);
    }

    public class VRMServer : IVRMSERVER
    {
        private Configuration config;
        private bool m_bWriteLog4NetConfig;
        private static log4net.ILog log;
        public static int m_Language;  //FB 1881
        public VRMServer()
        {
        }
        #region LoadConfigParams
        private bool LoadConfigParams(string configPath)
        {

            try
            {
                // Config file intialization

                if (configPath.Length < 1)
                {
                    configPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                }

                m_bWriteLog4NetConfig = false;
                string log4netXml = configPath + "log4net.xml";

                // read the log4net config file. if it needs path information write it
                // to the file. 

                XmlTextReader textReader = new XmlTextReader(log4netXml);
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(textReader);
                XmlNodeList NodeList = xmlDoc.SelectNodes(@"/log4net/appender");

                foreach (XmlNode Node in NodeList)
                {
                    if (((XmlElement)Node).Attributes["name"].Value == "rollingFile")
                    {
                        XmlNodeList childNodeList = Node.ChildNodes;
                        foreach (XmlNode childNode in childNodeList)
                        {
                            XmlElement element = (XmlElement)childNode;
                            if (element.HasAttribute("name") && element.Attributes["name"].Value == "File")
                            {
                                if (((XmlElement)childNode).Attributes["value"].Value != configPath + "myVRM.log")
                                {
                                    ((XmlElement)childNode).Attributes["value"].Value = configPath + "myVRM.log";
                                    m_bWriteLog4NetConfig = true;
                                    break;
                                }
                            }
                        }
                    }
                    if (((XmlElement)Node).Attributes["name"].Value == "NHibernateAppender")
                    {
                        XmlNodeList childNodeList = Node.ChildNodes;
                        foreach (XmlNode childNode in childNodeList)
                        {
                            XmlElement nelement = (XmlElement)childNode;
                            if (nelement.HasAttribute("name") && nelement.Attributes["name"].Value == "File")
                            {
                                if (((XmlElement)childNode).Attributes["value"].Value != configPath + "nhibernatelog.log")
                                {
                                    ((XmlElement)childNode).Attributes["value"].Value = configPath + "nhibernatelog.log";
                                    m_bWriteLog4NetConfig = true;
                                    break;
                                }
                            }
                        }
                    }
                }

                string log4netToString = xmlDoc.OuterXml;
                textReader.Close();

                if (m_bWriteLog4NetConfig)
                    using (StreamWriter sw = File.CreateText(log4netXml))
                    {
                        sw.Write(log4netToString);
                        sw.Close();
                    }

                log4net.Config.XmlConfigurator.Configure(new System.IO.FileInfo(log4netXml));
                log = log4net.LogManager.GetLogger(typeof(VRMServer));
                //	log4net.Util.LogLog.InternalDebugging  = true;
                //
                // first configure the persistance layer
                //
                string xml = configPath + "app.config.xml";
                //        System.Xml.XmlTextReader XmlText = new System.Xml.XmlTextReader(xml);
                config = new Configuration();

                /// <summary>
                /// Initialize static classes (timezones and errors,system settings) 
                /// These values do not change very often.  
                /// </summary>

                timeZone.Init(configPath);

                //FB 1881 - Error Handling start
                if (m_Language < 1)
                    m_Language = 1;

                int tempLang = vrmErrorDAO.m_Language;
                //To reload errlist based on login user preferred language

                vrmErrorDAO.m_Language = m_Language;
                if (m_Language != tempLang)
                {
                    vrmErrorDAO.m_ErrorList = null;
                }
                //FB 1881 - Error Handling end
                vrmErrorDAO.Init(configPath);
                sysSettings.Init(configPath);
                vrmGen.Init(configPath);
                return true;
            }
            catch (myVRMException e)
            {
                log.Error("vrmException", e);
                throw e;
            }
            catch (Exception e)
            {
                log.Error("sytemException", e);
                throw e;
            }
        }
        #endregion

        #region Operations
        public string Operations(string configPath, string operation, string inXml)
        {
            string outXml = string.Empty;

            try
            {
                inXml = inXml.Replace("&nbsp;", " ");
                configPath = configPath.Trim();
                bool ret = LoadConfigParams(configPath);
                if (!ret)
                {
                    outXml = "<error>";
                    outXml += "<errorCode>200</errorCode>";
                    outXml += "<message>";
                    outXml += "Cannot configure application</message>";
                    outXml += "<level>S</level>";
                    outXml += "</error>";
                    ret = true;
                }
                log.Info("Performing operation:" + operation);

                log.Debug("inXML = " + inXml);

                myVRMDefines commands = new myVRMDefines();

                operation = operation.Trim();

                int cmd = 0;
                try
                {
                    cmd = (int)commands.CommandReference[operation];
                }
                catch (Exception e)
                {
                    myVRMException ex = new myVRMException(233);
                    log.Error("Invalid Command", e);
                    throw ex;
                }

                ret = false;

                int theFactory = cmd / myVRMDefines.CommandOffset;
                int theMethod = cmd - (theFactory * myVRMDefines.CommandOffset);

                vrmDataObject obj = new vrmDataObject(configPath);

                obj.ID = theMethod;
                obj.inXml = inXml;
                obj.log = log;

                switch (theFactory)
                {
                    // break on group
                    case (int)myVRMDefines.CommandTypes.ACCOUNTING:
                        break;

                    case (int)myVRMDefines.CommandTypes.CONFERENCE:
                        ret = exeConference(ref obj);
                        break;

                    case (int)myVRMDefines.CommandTypes.GENERAL:
                        ret = exeGeneral(ref obj);
                        break;

                    case (int)myVRMDefines.CommandTypes.WORKORDER:
                        ret = exeWorkOrder(ref obj);
                        break;


                    case (int)myVRMDefines.CommandTypes.HARDWARE:
                        ret = exeHardware(ref obj);
                        break;

                    case (int)myVRMDefines.CommandTypes.REPORTS:
                        ret = exeReports(ref obj);
                        break;

                    case (int)myVRMDefines.CommandTypes.SEARCH:
                        ret = exeSearch(ref obj);
                        break;

                    case (int)myVRMDefines.CommandTypes.USER:
                        ret = exeUser(ref obj);
                        break;

                    case (int)myVRMDefines.CommandTypes.SYSTEM:
                        ret = exeSystem(ref obj);
                        break;
                    case (int)myVRMDefines.CommandTypes.ORGANIZATION:
                        ret = exeOrganization(ref obj);
                        break;
                    case (int)myVRMDefines.CommandTypes.RoomFactory: //FB 2027
                        ret = exeRoomFactory(ref obj);
                        break;
                    case (int)myVRMDefines.CommandTypes.IMAGEFACTORY: //FB 2136
                        ret = exeimageFactory(ref obj);
                        break;
                    default:
                        throw new myVRMException(233);
                }
                if (!ret)
                {
                    if (obj.outXml.Length < 1)
                        throw new myVRMException(200);
                    else
                        outXml = obj.outXml;
                }
                else
                {
                    if (obj.outXml.Length < 1)
                        outXml = "<success>1</success>";
                    else
                        outXml = obj.outXml;

                }

                log.Debug("OutXML = " + outXml);
            }
            catch (myVRMException e)
            {
                log.Error("vrmException", e);
                return e.FetchErrorMsg();
            }
            catch (Exception e)
            {
                log.Error("sytemException", e);
				//ZD 100263
                myVRMException myVRMEx = new myVRMException(200);
                return myVRMException.toXml(myVRMEx.Message);                
                //return myVRMException.toXml(e.Message);
            }

            return (outXml);
        }
        #endregion

        #region exeWorkOrder
        private bool exeWorkOrder(ref vrmDataObject obj)
        {
            bool iRet = false;
            string outXml = string.Empty;

            WorkOrderFactory wf;

            try
            {
                wf = new WorkOrderFactory(ref obj);

                switch (obj.ID)
                {
                    //
                    // WorkOrders

                    case (int)ASPIL.myVRMDefines.WorkOrderCommands.GetItemsList:
                        iRet = wf.GetItems(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.WorkOrderCommands.GetInventoryList:
                        iRet = wf.GetInventoryList(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.WorkOrderCommands.GetInventoryDetails:
                        iRet = wf.GetInventoryDetails(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.WorkOrderCommands.GetWorkOrderDetails:
                        iRet = wf.GetWorkOrderDetails(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.WorkOrderCommands.GetRoomSets:
                        iRet = wf.GetRoomSets(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.WorkOrderCommands.SetCategoryItem:
                        iRet = wf.SetCategoryItem(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.WorkOrderCommands.DeleteCategoryItem:
                        iRet = wf.DeleteCategoryItem(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.WorkOrderCommands.SetConferenceWorkOrders:
                        iRet = wf.SetConferenceWorkOrders(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.WorkOrderCommands.SetInventoryDetails:
                        iRet = wf.SetInventoryDetails(ref obj);
                        break;

                    case (int)ASPIL.myVRMDefines.WorkOrderCommands.DeleteInventory:
                        iRet = wf.DeleteInventoryItem(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.WorkOrderCommands.DeleteWorkOrder:
                        iRet = wf.DeleteWorkOrder(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.WorkOrderCommands.SendWorkOrderReminder:
                        iRet = wf.SendWorkOrderReminder(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.WorkOrderCommands.SearchConferenceWorkOrders:
                        iRet = wf.SearchConferenceWorkOrders(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.WorkOrderCommands.GetDeliveryTypes:
                        iRet = wf.GetDeliveryTypes(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.WorkOrderCommands.GetProviderDetails:
                        iRet = wf.GetProviderDetails(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.WorkOrderCommands.SetProviderDetails:
                        iRet = wf.SetProviderDetails(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.WorkOrderCommands.GetProviderWorkorderDetails:
                        iRet = wf.GetProviderWorkorderDetails(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.WorkOrderCommands.SetProviderWorkorderDetails:
                        iRet = wf.SetProviderWorkorderDetails(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.WorkOrderCommands.GetCateringServices:
                        iRet = wf.GetCateringServices(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.WorkOrderCommands.SearchProviderMenus:
                        iRet = wf.SearchProviderMenus(ref obj);
                        break;

                    // FB# 854, 882
                    case (int)ASPIL.myVRMDefines.WorkOrderCommands.DeleteProviderMenu:
                        iRet = wf.DeleteProviderMenu(ref obj);
                        break;

                    default:
                        {
                            throw new myVRMException("Invalid Method Call.");
                        }
                }
                if (!iRet)
                {
                    if (obj.outXml.Length < 1)
                        throw new myVRMException(200);
                    else
                        outXml = obj.outXml;
                }
                else
                {
                    if (obj.outXml.Length < 1)
                        outXml = "<success>1</success>";
                    else
                        outXml = obj.outXml;

                }
            }
            catch (myVRMException e)
            {
                log.Error("vrmException", e);
                obj.outXml = myVRMException.toXml(e.Message);
                iRet = false;
            }
            catch (Exception e)
            {
                log.Error("systemException", e);
                obj.outXml = myVRMException.toXml(e.Message);
                iRet = false;
            }
            return iRet;
        }
        #endregion

        #region exeConference
        private bool exeConference(ref vrmDataObject obj)
        {
            bool iRet = false;
            string outXml = string.Empty;

            Conference cf;

            try
            {
                cf = new Conference(ref obj);

                switch (obj.ID)
                {
                    // 
                    // conference commands
                    //                      
                    case (int)ASPIL.myVRMDefines.ConferenceCommands.ConfirmConferenceInvitation:
                        iRet = cf.ConfirmConferenceInvitation(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ConferenceCommands.GetAdvancedAVSettings:
                        iRet = cf.GetAdvancedAVSettings(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ConferenceCommands.GetConferenceEndpoint:
                        iRet = cf.GetConferenceEndpoint(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ConferenceCommands.GetOldConference:
                        iRet = cf.GetOldConference(ref obj);
                        break;

                    case (int)ASPIL.myVRMDefines.ConferenceCommands.SetAdvancedAVSettings:
                        iRet = cf.SetAdvancedAVSettings(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ConferenceCommands.SetConferenceEndpoint:
                        iRet = cf.SetConferenceEndpoint(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ConferenceCommands.SetConference:
                        iRet = cf.SetConference(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ConferenceCommands.SetTemplateOrder:
                        iRet = cf.SetTemplateOrder(ref obj);
                        break;

                    case (int)ASPIL.myVRMDefines.ConferenceCommands.DeletePastConference:
                        iRet = cf.DeletePastConference(ref obj);
                        break;

                    case (int)ASPIL.myVRMDefines.ConferenceCommands.DeleteCiscoICAL:
                        iRet = cf.DeleteCiscoICAL(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ConferenceCommands.GetEndpointStatus:
                        iRet = cf.GetEndpointStatus(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ConferenceCommands.CreateCiscoICALOnApproval:
                        iRet = cf.CreateCiscoICALOnApproval(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ConferenceCommands.DeleteRecurInstance: //FB 1772
                        iRet = cf.DeleteRecurInstance(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ConferenceCommands.DelRecurInstanceByUID: //FB 1772
                        iRet = cf.DelRecurInstanceByUID(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ConferenceCommands.EditRecurInstance: //FB 1772
                        iRet = cf.EditRecurInstance(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ConferenceCommands.EditRecurInstanceByUID: //FB 1772
                        iRet = cf.EditRecurInstanceByUID(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ConferenceCommands.DeleteParticipantICAL: //FB 1782
                        iRet = cf.DeleteParticipantICAL(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ConferenceCommands.CreateParticipantICALOnApproval: //FB 1782
                        iRet = cf.CreateParticipantICALOnApproval(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ConferenceCommands.UpdateICalID: //FB 1782
                        iRet = cf.UpdateICalID(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ConferenceCommands.CheckApprovalEntity: //FB 1830
                        iRet = cf.CheckApprovalEntity(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ConferenceCommands.SendPartyReminder: //FB 1830
                        iRet = cf.SendPartyReminder(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ConferenceCommands.DeleteTerminal: //FB 2027
                        iRet = cf.DeleteTerminal(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ConferenceCommands.GetTemplateList:  //FB 2027
                        iRet = cf.GetTemplateList(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ConferenceCommands.PartyInvitation:   //FB 2027
                        iRet = cf.PartyInvitation(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ConferenceCommands.TerminateConference://FB 2027
                        iRet = cf.TerminateConference(ref obj);
                        break;
					case (int)ASPIL.myVRMDefines.ConferenceCommands.DeleteTemplate://FB 2027
                        iRet = cf.DeleteTemplate(ref obj);
						break;
					case (int)ASPIL.myVRMDefines.ConferenceCommands.CounterInvite: //FB 2027
                        iRet = cf.CounterInvite(ref obj);
						break;
					case (int)ASPIL.myVRMDefines.ConferenceCommands.DisplayTerminal://FB 2027
                        iRet = cf.DisplayTerminal(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ConferenceCommands.MuteTerminal://FB 2027
                        iRet = cf.MuteTerminal(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ConferenceCommands.DeleteConference: //FB 2027
                        iRet = cf.DeleteConference(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ConferenceCommands.GetNewTemplate: //FB 2027
                        iRet = cf.GetNewTemplate(ref obj);
                        break;
					case (int)ASPIL.myVRMDefines.ConferenceCommands.GetNewConference: //FB 2027
                        iRet = cf.GetNewConference(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ConferenceCommands.GetApprovalStatus: //FB 2027
                        iRet = cf.GetApprovalStatus(ref obj);
                        break;
					case (int)ASPIL.myVRMDefines.ConferenceCommands.GetTerminalControl://FB 2027
                        iRet = cf.GetTerminalControl(ref obj);
                        break;
					case (int)ASPIL.myVRMDefines.ConferenceCommands.GetOldTemplate: //FB 2027
                        iRet = cf.GetOldTemplate(ref obj);
                        break;
					case (int)ASPIL.myVRMDefines.ConferenceCommands.GetConfGMTInfo: //FB 2027 
                        iRet = cf.GetConfGMTInfo(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ConferenceCommands.SetDynamicUser: //FB 2027 
                        iRet = cf.SetDynamicUser(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ConferenceCommands.SetTerminalControl: //FB 2027
                        iRet = cf.SetTerminalControl(ref obj);
                        break;
					case (int)ASPIL.myVRMDefines.ConferenceCommands.GetAvailableRoom: //FB 2027
                        iRet = cf.GetAvailableRoom(ref obj);
                        break;
					case (int)ASPIL.myVRMDefines.ConferenceCommands.GetTemplate: //FB 2027
                        iRet = cf.GetTemplate(ref obj);
                        break;
					case (int)ASPIL.myVRMDefines.ConferenceCommands.ResponseInvite://FB 2027
                        iRet = cf.ResponseInvite(ref obj);
                        break;     
                    case (int)ASPIL.myVRMDefines.ConferenceCommands.GetInstances: //FB 2027
                        iRet = cf.GetInstances(ref obj);
                        break;
					case (int)ASPIL.myVRMDefines.ConferenceCommands.GetApproveConference: //FB 2027GAC
                        iRet = cf.GetApproveConference(ref obj);
						break;
					case (int)ASPIL.myVRMDefines.ConferenceCommands.SetTemplate: //FB 2027
                        iRet = cf.SetTemplate(ref obj);  
                        break;
					case (int)ASPIL.myVRMDefines.ConferenceCommands.SetApproveConference: //FB 2027
                        iRet = cf.SetApproveConference(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ConferenceCommands.GetRecurDateList: //FB 2027 SetConference
                        iRet = cf.GetRecurDateList(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ConferenceCommands.GetRoombyMediaService: //FB 2038
                        iRet = cf.GetRoombyMediaService(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ConferenceCommands.GetConfMCUDetails: //FB 2448
                        iRet = cf.GetConfMCUDetails(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ConferenceCommands.GetConfMsg: //FB 2486
                        iRet = cf.GetConfMsg(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ConferenceCommands.GetPacketLossTerminal: //FB 2501 Call Monitoring
                        iRet = cf.GetPacketLossTerminal(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ConferenceCommands.LockTerminal: //FB 2501 Call Monitoring
                        iRet = cf.LockTerminal(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ConferenceCommands.MonitorMuteTerminal: //FB 2501 Call Monitoring
                        iRet = cf.MonitorMuteTerminal(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ConferenceCommands.SetP2PCallerEndpoint: //FB 2501 p2p Call Monitoring
                        iRet = cf.SetP2PCallerEndpoint(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ConferenceCommands.SetP2PCallerLinerate: //FB 2501 p2p Call Monitoring
                        iRet = cf.SetP2PCallerLinerate(ref obj);
                        break;
					case (int)ASPIL.myVRMDefines.ConferenceCommands.GetConfAvailableRoom: //FB 2392
                        iRet = cf.GetConfAvailableRoom(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ConferenceCommands.MuteUnMuteParticipants://FB 2441
                        iRet = cf.MuteUnMuteParticipants(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ConferenceCommands.ConferenceRecording://FB 2441
                        iRet = cf.ConferenceRecording(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ConferenceCommands.SetLeaderPerson: //FB 2553-RMX
                        iRet = cf.SetLeaderPerson(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ConferenceCommands.SetLectureParty: //FB 2553-RMX
                        iRet = cf.SetLectureParty(ref obj);
                        break;

					case (int)ASPIL.myVRMDefines.ConferenceCommands.TDGetAvailableSeats://FB 2659
                        iRet = cf.TDGetAvailableSeats(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ConferenceCommands.TDGetMaximumAvailableSeats://FB 2659
                        iRet = cf.TDGetMaximumAvailableSeats(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ConferenceCommands.SendSynchronousBridgeemails://FB 2659
                        iRet = cf.SendSynchronousBridgeemails(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ConferenceCommands.TDGetSlotAvailableSeats://FB 2659
                        iRet = cf.TDGetSlotAvailableSeats(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ConferenceCommands.TDGetDefaultSettings://FB 2659
                        iRet = cf.TDGetDefaultSettings(ref obj);
					    break;
					case (int)ASPIL.myVRMDefines.ConferenceCommands.SetConferenceSignin://FB 2724
                        iRet = cf.SetConferenceSignin(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ConferenceCommands.ChangeConfMCU://ZD 100369:MCU FailOver
                        iRet = cf.ChangeConfMCU(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ConferenceCommands.CheckConferenceApprovalStatus:   //ZD 100642
                        iRet = cf.CheckConferenceApprovalStatus(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ConferenceCommands.ForceConfDelete: //ZD 100221 //ZD 100152
                        iRet = cf.ForceConfDelete(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ConferenceCommands.SetInstantConferenceDetails://ZD 100167 102195
                        iRet = cf.SetInstantConferenceDetails(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ConferenceCommands.CheckForExtendTimeConflicts://ZD 100819
                        iRet = cf.CheckForExtendTimeConflicts(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ConferenceCommands.CheckDialNumber://ZD 101657
                        iRet = cf.CheckDialNumber(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ConferenceCommands.SetConferenceFromDateImport: //ZD 101879
                        iRet = cf.SetConferenceFromDateImport(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ConferenceCommands.SetConferenceDetails: //ZD 102195
                        iRet = cf.SetConferenceDetails(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ConferenceCommands.SendBJNBridgeemails://ZD 103263
                        iRet = cf.SendBJNBridgeemails(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ConferenceCommands.InsertConfAuditDetails://ZD 102754
                        iRet = cf.InsertConfAuditDetails(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ConferenceCommands.SetHDBusyConferences: //ALLDEV-807  
                        iRet = cf.SetHDBusyConferences(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ConferenceCommands.ScheduleHDBusyConferences: //ALLDEV-807  
                        iRet = cf.ScheduleHDBusyConferences(ref obj);
                        break;
                    default:
                        {
                            throw new myVRMException("Invaild method");
                        }
                }
                if (!iRet)
                {
                    if (obj.outXml.Length < 1)
                        throw new myVRMException(200);
                    else
                        outXml = obj.outXml;
                }
                else
                {
                    if (obj.outXml.Length < 1)
                        outXml = "<success>1</success>";
                    else
                        outXml = obj.outXml;

                }
            }
            catch (myVRMException e)
            {
                log.Error("vrmException", e);
                obj.outXml = myVRMException.toXml(e.Message);
                iRet = false;
            }
            catch (Exception e)
            {
                log.Error("sytemException", e);
                obj.outXml = myVRMException.toXml(e.Message);
                iRet = false;
            }
            return iRet;
        }
        #endregion

        #region exeSystem
        private bool exeSystem(ref vrmDataObject obj)
        {
            bool iRet = false;
            string outXml = string.Empty;
            vrmSystemFactory sysFactory;
            vrmLDAPFactory vrmLdap = null;
            try
            {
                sysFactory = new vrmSystemFactory(ref obj);
                switch (obj.ID)
                {
                    case (int)ASPIL.myVRMDefines.SystemCommands.GetLicenseDetails:
                        iRet = sysFactory.GetLicenseDetails(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.SystemCommands.GetActivation:
                        iRet = sysFactory.GetActivation(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.SystemCommands.GetLDAPUsers: //LDAP Fixes
                        vrmLdap =  new vrmLDAPFactory(ref obj);
                        iRet = vrmLdap.GetLDAPUsers(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.SystemCommands.SetImagekey://Site Logo
                        iRet = sysFactory.SetImagekey(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.SystemCommands.GetSiteImage://Site Logo
                        iRet = sysFactory.GetSiteImage(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.SystemCommands.DeleteSiteImage://Site Logo
                        iRet = sysFactory.DeleteSiteImage(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.SystemCommands.GetLastMailRunDate:
                        iRet = sysFactory.GetLastMailRunDate(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.SystemCommands.GetSysMailData://RSS Fix
                        iRet = sysFactory.GetSysMailData(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.SystemCommands.SetSuperAdmin: //FB 2027
                        iRet = sysFactory.SetSuperAdmin(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.SystemCommands.GetSystemDateTime://FB 2027
                        iRet = sysFactory.GetSystemDateTime(ref obj);
                        break;
					case (int)ASPIL.myVRMDefines.SystemCommands.SearchLog://FB 2027
                        iRet = sysFactory.SearchLog(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.SystemCommands.GetSuperAdmin://FB 2027
                        iRet = sysFactory.GetSuperAdmin(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.SystemCommands.GenerateESUserReport://FB 2363
                        iRet = sysFactory.GenerateESUserReport(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.SystemCommands.GenerateESErrorReport://FB 2363
                        iRet = sysFactory.GenerateESErrorReport(ref obj);
                        break;
					case (int)ASPIL.myVRMDefines.SystemCommands.SyncWithLdap: //FB 2462 //ZD 102045
                        vrmLdap = new vrmLDAPFactory(ref obj);
                        iRet = vrmLdap.SyncWithLdap(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.SystemCommands.DeltePublicRoomEP://FB 2594
                        iRet = sysFactory.DeltePublicRoomEP(ref obj);
                        break;
 					case (int)ASPIL.myVRMDefines.SystemCommands.KeepAlive: //FB 2616
                        iRet = sysFactory.KeepAlive(ref obj);
                        break;                       
                    case (int)ASPIL.myVRMDefines.SystemCommands.SetDefaultLicense: //FB 2659
                        iRet = sysFactory.SetDefaultLicense(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.SystemCommands.GetDefaultLicense: //FB 2659
                        iRet = sysFactory.GetDefaultLicense(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.SystemCommands.SendMailtoAdmin: //ZD 100230
                        iRet = sysFactory.SendMailtoAdmin(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.SystemCommands.SendPasswordRequest: //ZD 100230
                        iRet = sysFactory.SendPasswordRequest(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.SystemCommands.GetOrgLDAP: //ZD 101525
                        vrmLdap = new vrmLDAPFactory(ref obj);
                        iRet = vrmLdap.GetOrgLDAP(ref obj);
                        break;
                    default:
                        throw new myVRMException("Invaild method");
                }
                if (!iRet)
                {
                    if (obj.outXml.Length < 1)
                        throw new myVRMException(200);
                    else
                        outXml = obj.outXml;
                }
                else
                {
                    if (obj.outXml.Length < 1)
                        outXml = "<success>1</success>";
                    else
                        outXml = obj.outXml;

                }
            }
            catch (myVRMException ex)
            {
                log.Error("vrmException", ex);
                throw new myVRMException(ex.Message);
            }
            catch (Exception ex)
            {
                log.Error("sytemException", ex);
                throw new Exception(ex.Message, ex.InnerException);
            }
            return iRet;
        }
        #endregion

        #region exeUser
        private bool exeUser(ref vrmDataObject obj)
        {
            bool iRet = false;
            string outXml = string.Empty;

            UserFactory user;

            try
            {
                user = new UserFactory(ref obj);
                switch (obj.ID)
                {
                    case (int)ASPIL.myVRMDefines.UserCommands.GetMultipleUsers:
                        iRet = user.GetMultipleUsers(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.UserCommands.SetMultipleUsers:
                        iRet = user.SetMultipleUsers(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.UserCommands.DeleteLDAPUser:
                        iRet = user.DeleteLDAPUser(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.UserCommands.GetUserTemplateList:
                        iRet = user.GetUserTemplateList(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.UserCommands.GetUserTemplateDetails:
                        iRet = user.GetUserTemplateDetails(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.UserCommands.SetUserTemplate:
                        iRet = user.SetUserTemplate(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.UserCommands.DeleteUserTemplate:
                        iRet = user.DeleteUserTemplate(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.UserCommands.GetUserDetails:
                        iRet = user.GetUserDetails(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.UserCommands.SetUserDetails:
                        iRet = user.SetUserDetails(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.UserCommands.GetUserList:
                        iRet = user.GetUserList(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.UserCommands.ConvertToGMT:
                        iRet = user.ConvertToGMT(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.UserCommands.ConvertFromGMT:
                        iRet = user.ConvertFromGMT(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.UserCommands.ChangeUserStatus:
                        iRet = user.ChangeUserStatus(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.UserCommands.ChangeGuestStatus:
                        iRet = user.ChangeUserStatus(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.UserCommands.GetManageUser:
                        iRet = user.GetManageUser(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.UserCommands.GetOldUser:
                        iRet = user.GetOldUser(ref obj);
                        break;
                    //Code added for FB Issue 826
                    case (int)ASPIL.myVRMDefines.UserCommands.GetAllManageUser:
                        iRet = user.GetAllManageUser(ref obj);
                        break;
                    //Code added for FB Issue 826    
                    case (int)ASPIL.myVRMDefines.UserCommands.SetBulkUserUpdate:
                        iRet = user.SetBulkUserUpdate(ref obj);
                        break;
                    //Added Webservice Start
                    case (int)ASPIL.myVRMDefines.UserCommands.ChkUsrAuthentication:
                        iRet = user.ChkUserValidation(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.UserCommands.CheckUserCredentials:
                        iRet = user.CheckUserCredentials(ref obj);
                        break;
                    //Added Webservice End
                    case (int)ASPIL.myVRMDefines.UserCommands.BlockAllUsers:
                        iRet = user.BlockAllUsers(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.UserCommands.UnBlockAllUsers:
                        iRet = user.UnBlockAllUsers(ref obj);
                        break;
                    //Audio AddOn..
                    case (int)ASPIL.myVRMDefines.UserCommands.GetAudioUserList:
                        iRet = user.GetAudioUserList(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.UserCommands.RequestPassword:   //FB 1830
                        iRet = user.RequestPassword(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.UserCommands.ChangePasswordRequest:   //ZD 100263
                        iRet = user.ChangePasswordRequest(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.UserCommands.GetRequestIDUser:   //ZD 100263
                        iRet = user.GetRequestIDUser(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.UserCommands.RequestVRMAccount:  //FB 1830
                        iRet = user.RequestVRMAccount(ref obj);
                        break;
                    //case (int)ASPIL.myVRMDefines.UserCommands.GetUserEmailsBlockStatus:  //FB 1860 FB 2027
                    //    iRet = user.GetUserEmailsBlockStatus(ref obj);
                    //    break;
                    case (int)ASPIL.myVRMDefines.UserCommands.GetUserEmails:  //FB 1860
                        iRet = user.GetUserEmails(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.UserCommands.SetUserEmailsBlockStatus:  //FB 1860
                        iRet = user.SetUserEmailsBlockStatus(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.UserCommands.SetPreferedRoom:  //1959
                        iRet = user.SetPreferedRoom(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.UserCommands.GetPreferedRoom:  //1959
                        iRet = user.GetPreferedRoom(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.UserCommands.SetUserRoles:  //FB 2027
                        iRet = user.SetUserRoles(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.UserCommands.SearchUserOrGuest://FB 2027
                        iRet = user.SearchUserOrGuest(ref obj);
                        break;
					case (int)ASPIL.myVRMDefines.UserCommands.RetrieveUsers: //FB 2027
                        iRet = user.RetrieveUsers(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.UserCommands.RetrieveGuest: //FB 2027
                        iRet = user.RetrieveGuest(ref obj);
                        break;
					case (int)ASPIL.myVRMDefines.UserCommands.GetManageGuest: //FB 2027
                        iRet = user.GetManageGuest(ref obj);
					     break;
                    case (int)ASPIL.myVRMDefines.UserCommands.GetUsers: //FB 2027
                        iRet = user.GetUsers(ref obj);
                        break;
					case (int)ASPIL.myVRMDefines.UserCommands.SetOldUser:  //2027
                        iRet = user.SetOldUser(ref obj);
                        break; 
                    case (int)ASPIL.myVRMDefines.UserCommands.GetUserPreference:  //2027
                        iRet = user.GetUserPreference(ref obj);
                        break; 
					case (int)ASPIL.myVRMDefines.UserCommands.DeleteAllGuests:  //FB 2027
                        iRet = user.DeleteAllGuests(ref obj);
						break;
					case (int)ASPIL.myVRMDefines.UserCommands.GetAllocation:  //FB 2027
                        iRet = user.GetAllocation(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.UserCommands.SetGroup:  //FB 2027
                        iRet = user.SetGroup(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.UserCommands.GetGroup: //FB 2027
                        iRet = user.GetGroup(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.UserCommands.SearchGroup: //FB 2027
                        iRet = user.SearchGroup(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.UserCommands.SetUserStatus:  //FB 2027
                        iRet = user.SetUserStatus(ref obj);
						break;
					case (int)ASPIL.myVRMDefines.UserCommands.GuestRegister:  //FB 2027
                        iRet = user.GuestRegister(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.UserCommands.GetUserRoles:  //FB 2027
                        iRet = user.GetUserRoles(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.UserCommands.DeleteGroup:  //FB 2027
                        iRet = user.DeleteGroup(ref obj);
                        break;
					case (int)ASPIL.myVRMDefines.UserCommands.GetEmailList: //FB 2027
                        iRet = user.GetEmailList(ref obj);
						break;
					case (int)ASPIL.myVRMDefines.UserCommands.GetGuestList: //FB 2027
                        iRet = user.GetGuestList(ref obj);
                        break;
					case (int)ASPIL.myVRMDefines.UserCommands.GetManageBulkUsers:  //2027
                        iRet = user.GetManageBulkUsers(ref obj);
						break;
					case (int)ASPIL.myVRMDefines.UserCommands.GetSettingsSelect: //FB 2027
                        iRet = user.GetSettingsSelect(ref obj);
                        break;
					case (int)ASPIL.myVRMDefines.UserCommands.SetBulkUserAddMinutes:  //2027
                        iRet = user.SetBulkUserAddMinutes(ref obj);
						break;
					case (int)ASPIL.myVRMDefines.UserCommands.SetBulkUserBridge:  //2027
                        iRet = user.SetBulkUserBridge(ref obj);
						break;
					case (int)ASPIL.myVRMDefines.UserCommands.SetBulkUserDelete:  //2027
                        iRet = user.SetBulkUserDelete(ref obj);
						break;
					case (int)ASPIL.myVRMDefines.UserCommands.SetBulkUserDepartment:  //2027
                        iRet = user.SetBulkUserDepartment(ref obj);
						break;
					case (int)ASPIL.myVRMDefines.UserCommands.SetBulkUserExpiryDate:  //2027
                        iRet = user.SetBulkUserExpiryDate(ref obj);
						break;
					case (int)ASPIL.myVRMDefines.UserCommands.SetBulkUserLanguage:  //2027
                        iRet = user.SetBulkUserLanguage(ref obj);
						break;
					case (int)ASPIL.myVRMDefines.UserCommands.SetBulkUserLock:  //2027
                        iRet = user.SetBulkUserLock(ref obj);
						break;
					case (int)ASPIL.myVRMDefines.UserCommands.SetBulkUserRole:  //2027
                        iRet = user.SetBulkUserRole(ref obj);
						break;
					case (int)ASPIL.myVRMDefines.UserCommands.SetBulkUserTimeZone:  //2027
                        iRet = user.SetBulkUserTimeZone(ref obj);
						break;
					case (int)ASPIL.myVRMDefines.UserCommands.SetUser: //FB 2027S
                        iRet = user.SetUser(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.UserCommands.GetHome: //FB 2027
                        iRet = user.GetHome(ref obj);
                        break;
					case (int)ASPIL.myVRMDefines.UserCommands.GetLanguageTexts:  //FB 1830 - Translation
                        iRet = user.GetLanguageTexts(ref obj);
                        break; 
					case (int)ASPIL.myVRMDefines.UserCommands.SetUserLobbyIcons://NewLobby
                        iRet = user.SetUserLobbyIcons(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.UserCommands.SetBridgenumbers://FB 2227
                        iRet = user.SetBridgenumbers(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.UserCommands.SetPhoneNumber://FB 2268
                        iRet = user.SetPhoneNumber(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.UserCommands.UserAuthentication://FB 2558 WhyGo
                        iRet = user.UserAuthentication(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.UserCommands.GetVNOCUserList://FB 2670
                        iRet = user.GetVNOCUserList(ref obj);
                        break;
					case (int)ASPIL.myVRMDefines.UserCommands.FetchSelectedPCDetails: //FB 2693
                        iRet = user.FetchSelectedPCDetails(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.UserCommands.SetCalendarTimes: //ZD 100157
                        iRet = user.SetCalendarTimes(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.UserCommands.GetCalendarTimes: //ZD 100157
                        iRet = user.GetCalendarTimes(ref obj);
                        break;
					case (int)ASPIL.myVRMDefines.UserCommands.PasswordChangeRequest: //ZD 100781
                        iRet = user.PasswordChangeRequest(ref obj);
                        break;
					//ZD 100152 Starts
					case (int)ASPIL.myVRMDefines.UserCommands.SetUserToken: 
                        iRet = user.SetUserToken(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.UserCommands.GetGoogleChannelExpiredList: 
                        iRet = user.GetGoogleChannelExpiredList(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.UserCommands.GetGoogleChannelDetails: 
                        iRet = user.GetGoogleChannelDetails(ref obj);
                        break;
                        //ZD 100621 start
                    case (int)ASPIL.myVRMDefines.UserCommands.GetUserRoomViewType:
                        iRet = user.GetUserRoomViewType(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.UserCommands.SetUserRoomViewType:
                        iRet = user.SetUserRoomViewType(ref obj);
                        break;
						//ZD 100621 end
                    //ZD 100152 Ends 
                    case (int)ASPIL.myVRMDefines.UserCommands.GetUserType: //ZD 100815
                        iRet = user.GetUserType(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.UserCommands.SearchUserInfo: //ZD 101443
                        iRet = user.SearchUserInfo(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.UserCommands.SetAssignedUserAdmin: //ZD 101443
                        iRet = user.SetAssignedUserAdmin(ref obj);
                        break;
 					case (int)ASPIL.myVRMDefines.UserCommands.ChkADAuthentication: //ZD 101380
                        iRet = user.ChkADAuthentication(ref obj);
                        break;
                    // ZD 103954 Start
                    case (int)ASPIL.myVRMDefines.UserCommands.SetExpirePassword:
                        iRet = user.SetExpirePassword(ref obj);
                        break;
                    // ZD 103954 End
                    case (int)ASPIL.myVRMDefines.UserCommands.SetUsrBlockStatusFrmDataimport: //ZD 104862
                        iRet = user.SetUsrBlockStatusFrmDataimport(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.UserCommands.DeleteUserAssignedDetails: //ALLDEV-498
                        iRet = user.DeleteUserAssignedDetails(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.UserCommands.FetchGoogleDetails: //ALLDEV-856
                        iRet = user.FetchGoogleDetails(ref obj);
                        break;  
                    default:
                        throw new myVRMException("Invaild method");
                }
                if (!iRet)
                {
                    if (obj.outXml.Length < 1)
                        throw new myVRMException(200);
                    else
                        outXml = obj.outXml;
                }
                else
                {
                    if (obj.outXml.Length < 1)
                        outXml = "<success>1</success>";
                    else
                        outXml = obj.outXml;

                }
            }
            catch (myVRMException e)
            {
                log.Error("vrmException", e);
                throw new myVRMException(e.Message);
            }
            catch (Exception e)
            {
                log.Error("sytemException", e);
                throw new Exception(e.Message, e.InnerException);
            }
            return iRet;
        }
        #endregion

        #region exeReports
        private bool exeReports(ref vrmDataObject obj)
        {
            bool iRet = false;

            try
            {
                ReportFactory rf = new ReportFactory(ref obj);
                switch (obj.ID)
                {
                    //
                    // reports
                    case (int)ASPIL.myVRMDefines.ReportCommands.GetConferenceReportList:
                        iRet = rf.GetConferenceReportList(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ReportCommands.GetReportTypeList:
                        iRet = rf.GetReportTypeList(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ReportCommands.GetInputParameters:
                        iRet = rf.GetInputParameters(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ReportCommands.GenerateReport:
                        iRet = rf.GenerateReport(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ReportCommands.runReport:
                        iRet = rf.runReport();
                        break;
                    case (int)ASPIL.myVRMDefines.ReportCommands.DeleteConferenceReport:
                        iRet = rf.DeleteConferenceReport(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ReportCommands.GetOldScheduledReport:
                        iRet = rf.GetOldScheduledReport(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ReportCommands.ConferenceReports: // SQL Reports
                        iRet = rf.ConferenceReports(ref obj);
                        break;
                    //Added for Graphical Reports START
                    case (int)ASPIL.myVRMDefines.ReportCommands.GetTotalUsageMonth:
                        iRet = rf.GetTotalUsageMonth(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ReportCommands.GetSpecificUsageMonth:
                        iRet = rf.GetSpecificUsageMonth(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ReportCommands.GetLocationsUsage:
                        iRet = rf.GetLocationsUsage(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ReportCommands.GetMCUUsage:
                        iRet = rf.GetMCUUsage(ref obj);
                        break;
                    //Added for Graphical Reports End
                    case (int)ASPIL.myVRMDefines.ReportCommands.FetchUserLogin:  //FB 1969
                        iRet = rf.FetchUserLogin(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ReportCommands.GetUsageReports:  
                        iRet = rf.GetUsageReports(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ReportCommands.GetMCCReport: //FB 2047
                        iRet = rf.GetMCCReport(ref obj);
                        break;
                    //FB 2343 Start
                    case (int)ASPIL.myVRMDefines.ReportCommands.GetMonthlydays: 
                        iRet = rf.GetMonthlydays(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ReportCommands.SetMonthlydays: 
                        iRet = rf.SetMonthlydays(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ReportCommands.GetWeeklydays:
                        iRet = rf.GetWeeklydays(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ReportCommands.SetWeeklydays:
                        iRet = rf.SetWeeklydays(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ReportCommands.Setdays:
                        iRet = rf.Setdays(ref obj);
                        break;
                    //FB 2343 End
                    case (int)ASPIL.myVRMDefines.ReportCommands.UpdateCancelEvents: //FB 2363X
                        iRet = rf.UpdateCancelEvents(ref obj);
                        break;
                    //case (int)ASPIL.myVRMDefines.ReportCommands.GetEventLog:  //FB 2501 P2P Call Monitoring Commented for FB 2569
                    //    iRet = rf.GetEventLog(ref obj);
                    //    break;
					 //FB 2410 - Starts 
					case (int)ASPIL.myVRMDefines.ReportCommands.SetBatchReportConfig:
                        iRet = rf.SetBatchReportConfig(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ReportCommands.GetAllBatchReports:
                        iRet = rf.GetAllBatchReports(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ReportCommands.GenerateBatchReport:
                        iRet = rf.GenerateBatchReport(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ReportCommands.DeleteBatchReport:
                        iRet = rf.DeleteBatchReport(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ReportCommands.GetEntityCodes://ZD 102909
                        iRet = rf.GetEntityCodes(ref obj);
                        break;
					//FB 2410 - Ends
                    default:
                        {
                            throw new myVRMException("Invaild method");
                        }
                }
            }
            catch (myVRMException e)
            {
                log.Error("vrmException", e);
                throw e;
            }
            catch (Exception e)
            {
                log.Error("sytemException", e);
                throw e;
            }
            return iRet;
        }
        #endregion

        #region exeHardware
        private bool exeHardware(ref vrmDataObject obj)
        {
            bool iRet = false;

            try
            {
                HardwareFactory hw = new HardwareFactory(ref obj);
                switch (obj.ID)
                {
                    //
                    // Hardware
                    case (int)ASPIL.myVRMDefines.HardwareCommands.DeleteEndpoint:
                        iRet = hw.DeleteEndpoint(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.HardwareCommands.GetAddressType:
                        iRet = hw.GetAddressType(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.HardwareCommands.GetBridges:
                        iRet = hw.GetBridges(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.HardwareCommands.GetEndpointDetails:
                        iRet = hw.GetEndpointDetails(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.HardwareCommands.GetLineRate:
                        iRet = hw.GetLineRate(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.HardwareCommands.GetVideoEquipment:
                        iRet = hw.GetVideoEquipment(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.HardwareCommands.GetManufacturer://ZD 100736
                        iRet = hw.GetManufacturer(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.HardwareCommands.GetManufacturerModel://ZD 100736
                        iRet = hw.GetManufacturerModel(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.HardwareCommands.SetEndpoint:
                        iRet = hw.SetEndpoint(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.HardwareCommands.SearchEndpoint:
                        iRet = hw.SearchEndpoint(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.HardwareCommands.GetAudioCodecs:
                        iRet = hw.GetAudioCodecs(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.HardwareCommands.GetVideoCodecs:
                        iRet = hw.GetVideoCodecs(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.HardwareCommands.GetVideoModes:
                        iRet = hw.GetVideoModes(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.HardwareCommands.GetMediaTypes:
                        iRet = hw.GetMediaTypes(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.HardwareCommands.GetVideoProtocols:
                        iRet = hw.GetVideoProtocols(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.HardwareCommands.GetMCUAvailableResources:
                        iRet = hw.GetMCUAvailableResources(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.HardwareCommands.GetMCUCards:
                        iRet = hw.GetMCUCards(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.HardwareCommands.GetBridge:
                        iRet = hw.GetBridgeInformation(ref obj, false);
                        break;
                    case (int)ASPIL.myVRMDefines.HardwareCommands.GetBridgeList:
                        iRet = hw.GetBridgeInformation(ref obj, true);
                        break;
                    case (int)ASPIL.myVRMDefines.HardwareCommands.GetNewBridge:
                        iRet = hw.GetNewBridge(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.HardwareCommands.GetOldBridge:
                        iRet = hw.GetOldBridge(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.HardwareCommands.SetBridge:
                        iRet = hw.SetBridge(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.HardwareCommands.FetchBridgeInfo:  //FB 1462
                        iRet = hw.FetchBridgeInfo(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.HardwareCommands.GetAllEndpoints:  //Endpoint Search
                        iRet = hw.GetAllEndpoints(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.HardwareCommands.GetCompleteMCUusageReport:  //FB 1938
                        iRet = hw.GetCompleteMCUusageReport(ref obj);
                        break;
					case (int)ASPIL.myVRMDefines.HardwareCommands.DeleteBridge: //FB 2027
                        iRet = hw.DeleteBridge(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.HardwareCommands.GetEndpoint:   //FB 2027
                        iRet = hw.GetEndpoint(ref obj);
						break;
                    case (int)ASPIL.myVRMDefines.HardwareCommands.SetBridgeList://FB 2027
                        iRet = hw.SetBridgeList(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.HardwareCommands.GetAllMessage://FB 2486
                        iRet = hw.GetAllMessage(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.HardwareCommands.SetMessage://FB 2486
                        iRet = hw.SetMessage(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.HardwareCommands.DeleteMessage://FB 2486
                        iRet = hw.DeleteMessage(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.HardwareCommands.SetFavouriteMCU://FB 2501 - Call Monitoring
                        iRet = hw.SetFavouriteMCU(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.HardwareCommands.GetMCUProfiles: //FB 2591 
                        iRet = hw.GetMCUProfiles(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.HardwareCommands.GetExtMCUServices: //FB 2556 
                        iRet = hw.GetExtMCUServices(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.HardwareCommands.GetExtMCUSilo: //FB 2556 
                        iRet = hw.GetExtMCUSilo(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.HardwareCommands.GetBridgeDetails: //ZD 100522 
                        iRet = hw.GetBridgeDetails(ref obj);
                        break; 
                    case (int)ASPIL.myVRMDefines.HardwareCommands.GetEndPointP2PQuery: //ZD 100815 
                        iRet = hw.GetEndPointP2PQuery(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.HardwareCommands.GetSystemLocation: //ZD 101525 
                        iRet = hw.GetSystemLocation(ref obj);
                        break;
                    //ZD 101527 Starts
                    case (int)ASPIL.myVRMDefines.HardwareCommands.GetSyncEndpoints: 
                        iRet = hw.GetSyncEndpoints(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.HardwareCommands.SetSyncEndpoints: 
                        iRet = hw.SetSyncEndpoints(ref obj);
                        break;
                    //ZD 101527 Ends
                    //ZD 100040 start
                    case (int)ASPIL.myVRMDefines.HardwareCommands.GetEptResolution:
                        iRet = hw.GetEptResolution(ref obj);
                        break;
                    
                    case (int)ASPIL.myVRMDefines.HardwareCommands.SetMCUGroup:
                        iRet = hw.SetMCUGroup(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.HardwareCommands.GetMCUGroup:
                        iRet = hw.GetMCUGroup(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.HardwareCommands.SearchMCUGroup:
                        iRet = hw.SearchMCUGroup(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.HardwareCommands.DeleteMCUGroup:
                        iRet = hw.DeleteMCUGroup(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.HardwareCommands.GetMCUGroups:
                        iRet = hw.GetMCUGroups(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.HardwareCommands.GetVirtualBridges:
                        iRet = hw.GetVirtualBridges(ref obj);
                        break;
                    //ZD 100040 Ends
					case (int)ASPIL.myVRMDefines.HardwareCommands.GetMCUPoolOrders: //ZD 104256
                        iRet = hw.GetMCUPoolOrders(ref obj);
                        break;
                    default:
                        throw new myVRMException("Invaild method");
                }
            }
            catch (myVRMException e)
            {
                log.Error("vrmException", e);
                throw e;
            }
            catch (Exception e)
            {
                log.Error("sytemException", e);
                throw e;
            }
            return iRet;
        }
        #endregion

        #region exeGeneral
        private bool exeGeneral(ref vrmDataObject obj)
        {
            bool iRet = false;

            try
            {
                vrmFactory vrm = new vrmFactory(ref obj);
                switch (obj.ID)
                {
                    //
                    // General
                    case (int)ASPIL.myVRMDefines.GeneralCommands.GetTimezones:
                        iRet = vrm.GetTimezones(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.GeneralCommands.GetDialingOptions:
                        iRet = vrm.GetDialingOptions(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.GeneralCommands.Feedback:
                        iRet = vrm.Feedback(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.GeneralCommands.UpdateManageDepartment:
                        iRet = vrm.UpdateManageDepartment(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.GeneralCommands.GetCountryCodes:
                        iRet = vrm.GetCountryCodes(ref obj);
                        break;
                    //FB 2671 Starts
                    case (int)ASPIL.myVRMDefines.GeneralCommands.GetPublicCountries:
                        iRet = vrm.GetPublicCountries(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.GeneralCommands.GetPublicCountryStates:
                        iRet = vrm.GetPublicCountryStates(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.GeneralCommands.GetPublicCountryStateCities:
                        iRet = vrm.GetPublicCountryStateCities(ref obj);
                        break;
                    //FB 2671 Ends
                    case (int)ASPIL.myVRMDefines.GeneralCommands.GetCountryStates:
                        iRet = vrm.GetCountryStates(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.GeneralCommands.GetManageDepartment:
                        iRet = vrm.GetManageDepartment(ref obj);
                        break;
                    //code added for Custom attribute fixes - start
                    case (int)ASPIL.myVRMDefines.GeneralCommands.SetCustomAttribute:
                        iRet = vrm.SetCustomAttribute(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.GeneralCommands.DeleteCustomAttribute:
                        iRet = vrm.DeleteCustomAttribute(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.GeneralCommands.GetCustomAttributes:
                        iRet = vrm.GetCustomAttributes(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.GeneralCommands.GetCustomAttributeDescription:
                        iRet = vrm.GetCustomAttributeDescription(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.GeneralCommands.GetCustomAttributeByID:
                        iRet = vrm.GetCustomAttributeByID(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.GeneralCommands.IsCustomAttrLinkedToConf:
                        iRet = vrm.IsCustomAttrLinkedToConf(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.GeneralCommands.GetConfListByCustOptID:
                        iRet = vrm.GetConfListByCustOptID(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.GeneralCommands.DeleteConfsAttributeByID:
                        iRet = vrm.DeleteConfsAttributeByID(ref obj);
                        break;
                    //code added for Custom attribute fixes - end
                    case (int)ASPIL.myVRMDefines.GeneralCommands.DeleteAllData:
                        iRet = vrm.DeleteAllData(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.GeneralCommands.GetSQLServerInfo:
                        iRet = vrm.GetSQLServerInfo(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.GeneralCommands.GetAllTimezones:
                        iRet = vrm.GetAllTimezones(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.GeneralCommands.GetAllImages:
                        iRet = vrm.GetAllImages(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.GeneralCommands.SetImage:
                        iRet = vrm.SetImage(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.GeneralCommands.DeleteImage:
                        iRet = vrm.DeleteImage(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.GeneralCommands.GetLanguages: //FB 1830 start
                        iRet = vrm.GetLanguages(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.GeneralCommands.GetIconsReference:  //NewLobby start
                        iRet = vrm.GetIconsReference(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.GeneralCommands.SetIconReference:  //NewLobby start
                        iRet = vrm.SetIconReference(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.GeneralCommands.GetEmailTypes:
                        iRet = vrm.GetEmailTypes(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.GeneralCommands.GetEmailContent:
                        iRet = vrm.GetEmailContent(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.GeneralCommands.SetEmailContent:
                        iRet = vrm.SetEmailContent(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.GeneralCommands.DeleteEmailLanguage: //FB 1830 - DeleteEmailLang
                        iRet = vrm.DeleteEmailLanguage(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.GeneralCommands.GetEmailLanguages: //FB 1830 end
                        iRet = vrm.GetEmailLanguages(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.GeneralCommands.GetEmailsBlockStatus: //FB 1860
                        iRet = vrm.GetEmailsBlockStatus(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.GeneralCommands.DeleteEmails: //FB 1860
                        iRet = vrm.DeleteEmails(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.GeneralCommands.SetBlockEmail: //FB 1860
                        iRet = vrm.SetBlockEmail(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.GeneralCommands.GetFeedback: //FB 2027
                        iRet = vrm.GetFeedback(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.GeneralCommands.SaveFiles: //FB 2153
                        iRet = vrm.SaveFiles(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.GeneralCommands.SetEntityCode: //FB 2045
                        iRet = vrm.SetEntityCode(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.GeneralCommands.UpdateEntityCode: //FB 2045
                        iRet = vrm.UpdateEntityCode(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.GeneralCommands.GetEntityCode: //FB 2045
                        iRet = vrm.GetEntityCode(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.GeneralCommands.DeleteEntityCode: //FB 2045
                        iRet = vrm.DeleteEntityCode(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.GeneralCommands.GetConfListByEntityOptID: //FB 2045
                        iRet = vrm.GetConfListByEntityOptID(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.GeneralCommands.DeleteConfEntityOptionID: //FB 2045
                        iRet = vrm.DeleteConfEntityOptionID(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.GeneralCommands.EditConfEntityOption: //FB 2045
                        iRet = vrm.EditConfEntityOption(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.GeneralCommands.HelpRequestMail: //FB 2268
                        iRet = vrm.HelpRequestMail(ref obj);
                        break;
					case (int)ASPIL.myVRMDefines.GeneralCommands.GetAllDepartments: //FB 2047
                        iRet = vrm.GetAllDepartments(ref obj);
                        break;
					case (int)ASPIL.myVRMDefines.GeneralCommands.GetSecurityBadgeType:  //FB 2136
                        iRet = vrm.GetSecurityBadgeType(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.GeneralCommands.GetPCDetails: //FB 2693
                        iRet = vrm.GetPCDetails(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.GeneralCommands.GetGMTTime:  //ZD 101120
                        iRet = vrm.GetGMTTime(ref obj);
                        break;
                    default:
                        {
                            throw new myVRMException("Invaild method");
                        }
                }
            }
            catch (myVRMException e)
            {
                log.Error("vrmException", e);
                throw e;
            }
            catch (Exception e)
            {
                log.Error("sytemException", e);
                throw e;
            }
            return iRet;
        }
        #endregion

        #region exeSearch
        private bool exeSearch(ref vrmDataObject obj)
        {
            bool iRet = false;

            try
            {
                myVRMSearch vrmSearch = new myVRMSearch(obj);
                switch (obj.ID)
                {
                    //
                    // General
                    case (int)ASPIL.myVRMDefines.SearchCommands.SearchConference:
                        iRet = vrmSearch.SearchConference(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.SearchCommands.GetLocationsList:
                        iRet = vrmSearch.GetLocationsList(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.SearchCommands.GetSearchTemplateList:
                        iRet = vrmSearch.GetSearchTemplateList(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.SearchCommands.SetSearchTemplate:
                        iRet = vrmSearch.SetSearchTemplate(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.SearchCommands.GetSearchTemplate:
                        iRet = vrmSearch.GetSearchTemplate(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.SearchCommands.DeleteSearchTemplate:
                        iRet = vrmSearch.DeleteSearchTemplate(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.SearchCommands.GetLocations:
                        iRet = vrmSearch.GetLocations(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.SearchCommands.GetLocations2:
                        iRet = vrmSearch.GetLocations2(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.SearchCommands.SetTier1:
                        iRet = vrmSearch.SetTier1(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.SearchCommands.SetTier2:
                        iRet = vrmSearch.SetTier2(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.SearchCommands.DeleteTier1:
                        iRet = vrmSearch.DeleteTier1(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.SearchCommands.DeleteTier2:
                        iRet = vrmSearch.DeleteTier2(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.SearchCommands.GetRoomProfile:
                        iRet = vrmSearch.GetRoomProfile(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.SearchCommands.SetRoomProfile:
                        iRet = vrmSearch.SetRoomProfile(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.SearchCommands.GetDepartmentsForLocation:
                        iRet = vrmSearch.GetDepartmentsForLocation(ref obj);
                        break;
                    //FB 1048
                    case (int)ASPIL.myVRMDefines.SearchCommands.GetRoomTimeZoneMapping:
                        iRet = vrmSearch.GetRoomTimeZoneMapping(ref obj);
                        break;
                    //FB 826
                    case (int)ASPIL.myVRMDefines.SearchCommands.SearchAllConference:
                        iRet = vrmSearch.SearchAllConference(ref obj);
                        break;
                    //FB 826
                    case (int)ASPIL.myVRMDefines.SearchCommands.GetConfBaseDetails:
                        iRet = vrmSearch.GetConfBaseDetails(ref obj);
                        break;
                    /*Added for Reccurence Fix*/
                    case (int)ASPIL.myVRMDefines.SearchCommands.GetIfDirtyorPast:
                        iRet = vrmSearch.GetIfDirtyorPast(ref obj);
                        break;
                    /*Added for FB 1391*/
                    case (int)ASPIL.myVRMDefines.SearchCommands.GetIfDirty:
                        iRet = vrmSearch.GetIfDirty(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.SearchCommands.GetRoomsDepts:
                        iRet = vrmSearch.GetRoomsDepts(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.SearchCommands.GetBusyRooms:
                        iRet = vrmSearch.GetBusyRooms(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.SearchCommands.GetRoomBasicDetails:
                        iRet = vrmSearch.GetRoomBasicDetails(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.SearchCommands.GetAllRooms:
                        iRet = vrmSearch.GetAllRooms(ref obj);
                        break;
                    //FB 1756 
                    case (int)ASPIL.myVRMDefines.SearchCommands.GetAllRoomsBasicInfo:
                        iRet = vrmSearch.GetAllRoomsBasicInfo(ref obj);
                        break;
                    //ZD 100563
                    case (int)ASPIL.myVRMDefines.SearchCommands.GetAllRoomsInfo:
                        iRet = vrmSearch.GetAllRoomsInfo(ref obj);
                        break;
					//ZD 104482
                    case (int)ASPIL.myVRMDefines.SearchCommands.GetAllRoomsInfoOpt:
                        iRet = vrmSearch.GetAllRoomsInfoOpt(ref obj);
                        break;                        
                    case (int)ASPIL.myVRMDefines.SearchCommands.GetAllRoomsInfoforCache: //ZD 103790
                        iRet = vrmSearch.GetAllRoomsInfoforCache(ref obj); 
                        break;
                    case (int)ASPIL.myVRMDefines.SearchCommands.SelectedRooms: //ZD 101175
                        iRet = vrmSearch.SelectedRooms(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.SearchCommands.GetDeactivatedRooms:
                        iRet = vrmSearch.GetDeactivatedRooms(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.SearchCommands.GetRoomLicenseDetails:
                        iRet = vrmSearch.GetRoomLicenseDetails(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.SearchCommands.SearchRooms://COde added for modified date
                        iRet = vrmSearch.SearchRoomsbyDate(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.SearchCommands.GetEncrpytedText:
                        iRet = vrmSearch.GetEncrpytedText(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.SearchCommands.GetDecrpytedText: //ZD 100152
                        iRet = vrmSearch.GetDecrpytedText(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.SearchCommands.Isconferenceschedulable: //TimeZone Issue
                        iRet = vrmSearch.Isconferenceschedulable(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.SearchCommands.GetImages: //TimeZone Issue
                        iRet = vrmSearch.GetImages(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.SearchCommands.GetOngoing: //FB 1838
                        iRet = vrmSearch.GetOngoing(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.SearchCommands.SearchTemplate: //FB 2027
                        iRet = vrmSearch.SearchTemplate(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.SearchCommands.GetSearchConference: //FB 2027
                        iRet = vrmSearch.GetSearchConference(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.SearchCommands.McuUsage: //FB 2027 McuUsage
                        iRet = vrmSearch.McuUsage(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.SearchCommands.GetOngoingMessageOverlay: //FB 2027 McuUsage
                        iRet = vrmSearch.GetOngoingMessageOverlay(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.SearchCommands.GetCallsforMonitor: //FB 2051 Call Monitoring
                        iRet = vrmSearch.GetCallsforMonitor(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.SearchCommands.GetP2PCallsforMonitor: //FB 2051 p2p Call Monitoring
                        iRet = vrmSearch.GetP2PCallsforMonitor(ref obj);
                        break;
					case (int)ASPIL.myVRMDefines.SearchCommands.SetPrivatePublicRoomProfile: //FB 2392
                        iRet = vrmSearch.SetPrivatePublicRoomProfile(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.SearchCommands.GetPrivatePublicRoomProfile: //whygo T
                        iRet = vrmSearch.GetPrivatePublicRoomProfile(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.SearchCommands.GetPrivatePublicRoomID: //whygo T
                        iRet = vrmSearch.GetPrivatePublicRoomID(ref obj);
                        break;
					case (int)ASPIL.myVRMDefines.SearchCommands.GetCallsforFMS: //FB 2616
                        iRet = vrmSearch.GetCallsforFMS(ref obj);
                        break;
					case (int)ASPIL.myVRMDefines.SearchCommands.GetCallsForSwitching: //FB 2595
                        iRet = vrmSearch.GetCallsForSwitching(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.SearchCommands.UpdateCompletedConference: //Tik 100036
                        iRet = vrmSearch.UpdateCompletedConference(ref obj);
                        break;
                    //ZD 101244 start
                    case (int)ASPIL.myVRMDefines.SearchCommands.GetLocationsTier1:
                        iRet =vrmSearch.GetLocationsTier1(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.SearchCommands.GetLocationsTier2:
                        iRet = vrmSearch.GetLocationsTier2(ref obj);
                        break;
                        //ZD 101244 End
                    //ZD 101611 start
                    case (int)ASPIL.myVRMDefines.SearchCommands.setRoomResizeImage:
                        iRet = vrmSearch.setRoomResizeImage(ref obj);
                        break;
                    //ZD 102600
                    case (int)ASPIL.myVRMDefines.SearchCommands.GetRPRMConfforLaunch:
                        iRet = vrmSearch.GetRPRMConfforLaunch(ref obj);
                        break;

                    default:
                        throw new myVRMException("Invaild method");
                }
            }
            catch (myVRMException e)
            {
                log.Error("vrmException", e);
                throw e;
            }
            catch (Exception e)
            {
                log.Error("sytemException", e);
                throw e;
            }
            return iRet;
        }
        #endregion

        #region exeOrganization
        private bool exeOrganization(ref vrmDataObject obj)
        {
            bool iRet = false;
            string outXml = string.Empty;
            OrganizationFactory orgFactory;
            try
            {
                orgFactory = new OrganizationFactory(ref obj);
                switch (obj.ID)
                {
                    case (int)ASPIL.myVRMDefines.Organization.DeleteOrganizationProfile:
                        iRet = orgFactory.DeleteOrganizationProfile(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.Organization.GetAllOrgSettings:
                        iRet = orgFactory.GetAllOrgSettings(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.Organization.GetOrganizationList:
                        iRet = orgFactory.GetOrganizationList(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.Organization.GetOrganizationProfile:
                        iRet = orgFactory.GetOrganizationProfile(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.Organization.GetOrgOptions:
                        iRet = orgFactory.GetOrgOptions(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.Organization.GetOrgSettings:
                        iRet = orgFactory.GetOrgSettings(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.Organization.SetOrganizationProfile:
                        iRet = orgFactory.SetOrganizationProfile(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.Organization.SetOrgOptions:
                        iRet = orgFactory.SetOrgOptions(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.Organization.SetOrgSettings:
                        iRet = orgFactory.SetOrgSettings(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.Organization.GetActiveOrgDetails:
                        iRet = orgFactory.GetActiveOrgDetails(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.Organization.SetPurgeDetails:
                        iRet = orgFactory.SetPurgeDetails(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.Organization.GetPurgeDetails:
                        iRet = orgFactory.GetPurgeDetails(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.Organization.CheckAPIEnable: //License modification
                        iRet = orgFactory.CheckAPIEnable(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.Organization.UpdateOrgImages: //Image Project
                        iRet = orgFactory.UpdateOrgImages(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.Organization.SetTextChangeXML: //CSS Module
                        iRet = orgFactory.SetTextChangeXML(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.Organization.SetDefaultCSSXML: //CSS Module
                        iRet = orgFactory.SetDefaultCSSXML(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.Organization.GetOrgImages: //Login Management
                        iRet = orgFactory.GetOrgImages(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.Organization.GetOrgEmails: //FB 1860
                        iRet = orgFactory.GetOrgEmails(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.Organization.GetOrgHolidays: //FB 1861
                        iRet = orgFactory.GetOrgHolidays(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.Organization.SetOrgHolidays: //FB 1861
                        iRet = orgFactory.SetOrgHolidays(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.Organization.SendAutomatedReminders: //FB 1926
                        iRet = orgFactory.SendAutomatedReminders(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.Organization.GetLogPreferences://FB 2027
                        iRet = orgFactory.GetLogPreferences(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.Organization.DeleteModuleLog://FB 2027
                        iRet = orgFactory.DeleteModuleLog(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.Organization.GetOrgEmailDomains: //FB 2154
                        iRet = orgFactory.GetOrgEmailDomains(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.Organization.SaveEmailDomain: //FB 2154
                        iRet = orgFactory.SaveEmailDomain(ref obj);
                        break;
					 case (int)ASPIL.myVRMDefines.Organization.GetHolidayType: //FB 2052
                        iRet = orgFactory.GetHolidayType(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.Organization.DeleteHolidayDetails: //FB 2052
                        iRet = orgFactory.DeleteHolidayDetails(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.Organization.SetDayOrderList: //FB 2052
                        iRet = orgFactory.SetDayOrderList(ref obj);
                        break; 
                    case (int)ASPIL.myVRMDefines.Organization.PurgeOrganization: //FB 2074
                        iRet = orgFactory.PurgeOrganization(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.Organization.GetOrgLicenseAgreement: //FB 2337
                        iRet = orgFactory.GetOrgLicenseAgreement(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.Organization.SetOrgLicenseAgreement: //FB 2337
                        iRet = orgFactory.SetOrgLicenseAgreement(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.Organization.SetOrgTextMsg: //FB 2486
                        iRet = orgFactory.SetOrgTextMsg(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.Organization.SetTheme: // FB 2719 Theme
                        iRet = orgFactory.SetTheme(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.Organization.SetLDAPGroup: // ZD 101525
                        iRet = orgFactory.SetLDAPGroup(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.Organization.GetLDAPGroup: // ZD 101525
                        iRet = orgFactory.GetLDAPGroup(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.Organization.UpdateLDAPGroupRole: // ZD 101525
                        iRet = orgFactory.UpdateLDAPGroupRole(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.Organization.GetConfArchiveConfiguration: // ZD 101835
                        iRet = orgFactory.GetConfArchiveConfiguration(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.Organization.SetConfArchiveConfiguration: // ZD 101835
                        iRet = orgFactory.SetConfArchiveConfiguration(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.Organization.OrgOptionsInfo: // ZD 103398
                        iRet = orgFactory.OrgOptionsInfo(ref obj);
                        break;
                        
                    default:
                        throw new myVRMException("Invaild method");
                }
                if (!iRet)
                {
                    if (obj.outXml.Length < 1)
                        throw new myVRMException(200);
                    else
                        outXml = obj.outXml;
                }
                else
                {
                    if (obj.outXml.Length < 1)
                        outXml = "<success>1</success>";
                    else
                        outXml = obj.outXml;

                }
            }
            catch (myVRMException ex)
            {
                log.Error("vrmException", ex);
                throw new myVRMException(ex.Message);
            }
            catch (Exception ex)
            {
                log.Error("sytemException", ex);
                throw new Exception(ex.Message, ex.InnerException);
            }
            return iRet;
        }
        #endregion

        //FB 1881 start
        #region GetErrorTextByID
        /// <summary>
        /// GetErrorTextByID
        /// </summary>
        /// <param name="errID"></param>
        /// <returns></returns>
        public string GetErrorTextByID(int errID)
        {
            myVRMException myvrmEx = new myVRMException(200);
            try
            {
                if (vrmErrorDAO.m_ErrorList.ContainsKey(errID))
                    myvrmEx = new myVRMException(errID);
            }
            catch(Exception e)
            {
                log.Error("GetErrorTextByID: sytemException", e);
                return myvrmEx.FetchErrorMsg();
            }
            return myvrmEx.FetchErrorMsg();
        }
        #endregion
        //FB 1881 end

        //Method Added for FB 2027 start
        #region exeRoomFactory
        /// <summary>
        /// exeRoomFactory
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        private bool exeRoomFactory(ref vrmDataObject obj)
        {
            bool iRet = false;

            try
            {
                RoomFactory Room = new RoomFactory(ref obj);

                switch (obj.ID)
                {
                    case (int)ASPIL.myVRMDefines.RoomFactory.GetRoomMonthlyView: 
                        iRet = Room.GetRoomMonthlyView(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.RoomFactory.GetRoomWeeklyView: 
                        iRet = Room.GetRoomWeeklyView(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.RoomFactory.GetRoomDailyView:
                        iRet = Room.GetRoomDailyView(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.RoomFactory.GetOldRoom:
                        iRet = Room.GetOldRoom(ref obj);
                        break;
					case (int)ASPIL.myVRMDefines.RoomFactory.ManageConfRoom:
                        iRet = Room.ManageConfRoom(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.RoomFactory.GetLastModifiedRoom: //FB 2027
                        iRet = Room.GetLastModifiedRoom(ref obj);
                        break;
					case (int)ASPIL.myVRMDefines.RoomFactory.DeleteRoom: //FB 2027 DeleteRoom
                        iRet = Room.DeleteRoom(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.RoomFactory.ActiveRoom: //FB 2027 ActiveRoom
                        iRet = Room.ActiveRoom(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.RoomFactory.GetServiceType://FB 2219
                        iRet = Room.GetServiceType(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.RoomFactory.GetRoomDetails: //FB 2361
                        iRet = Room.GetRoomDetails(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.RoomFactory.GetRoomQueue: //FB 2361
                        iRet = Room.GetRoomQueue(ref obj);
                        break;
                    //FB 2426 Start
                    case (int)ASPIL.myVRMDefines.RoomFactory.SetGuesttoNormalRoom:
                        iRet = Room.SetGuesttoNormalRoom(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.RoomFactory.DeleteGuestRoom: 
                        iRet = Room.DeleteGuestRoom(ref obj);
                        break;
                    //FB 2426 End
                    //FB 2724 Start
                    case (int)ASPIL.myVRMDefines.RoomFactory.GetRoomConferenceMonthlyView:
                        iRet = Room.GetRoomConferenceMonthlyView(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.RoomFactory.RoomValidation:
                        iRet = Room.RoomValidation(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.RoomFactory.GetModifiedConference:
                        iRet = Room.GetModifiedConference(ref obj);
                        break;
                    //FB 2724 End
					//FB 2593 Starts
                    case (int)ASPIL.myVRMDefines.RoomFactory.GetAllRoomsList:
                        iRet = Room.GetAllRoomsList(ref obj);
                        break;
                    //FB 2593 End
                    //ZD 100196 Start
                    case (int)ASPIL.myVRMDefines.RoomFactory.ChkRoomAuthentication:
                        iRet = Room.ChkRoomAuthentication(ref obj);
                        break;
                    //ZD 100196 End
                    //ZD 101736 Start
                    case (int)ASPIL.myVRMDefines.RoomFactory.GetRoomEWSDetails:
                        iRet = Room.GetRoomEWSDetails(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.RoomFactory.UpdateRoomEWSDetails:
                        iRet = Room.UpdateRoomEWSDetails(ref obj);
                        break;
                    //ZD 101736 End
					//ZD 101527 Starts
                    case (int)ASPIL.myVRMDefines.RoomFactory.SetSyncRooms:
                        iRet = Room.SetSyncRooms(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.RoomFactory.GetSyncRooms:
                        iRet = Room.GetSyncRooms(ref obj);
                        break;
                    //ZD 101527 Ends
                    case (int)ASPIL.myVRMDefines.RoomFactory.GetTopTiers:
                        iRet = Room.GetTopTiers(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.RoomFactory.GetMiddleTiersByTopTier:
                        iRet = Room.GetMiddleTiersByTopTier(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.RoomFactory.GetRoomsByTiers:
                        iRet = Room.GetRoomsByTiers(ref obj);
                        break;
                    //ZD 102123 Starts
                    case (int)ASPIL.myVRMDefines.RoomFactory.SetFloorPlan:
                        iRet = Room.SetFloorPlan(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.RoomFactory.GetFloorPlans:
                        iRet = Room.GetFloorPlans(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.RoomFactory.DeleteFloorPlan:
                        iRet = Room.DeleteFloorPlan(ref obj);
                        break;
                    //ZD 102123 Ends
                    //ZD 103460
                    case (int)ASPIL.myVRMDefines.RoomFactory.GetIcontrolconference:
                        iRet = Room.GetIcontrolconference(ref obj);
                        break;
                    default:
                        throw new myVRMException("Invaild method");
                }
            }
            catch (myVRMException e)
            {
                log.Error("vrmException", e);
                throw e;
            }
            catch (Exception e)
            {
                log.Error("sytemException", e);
                throw e;
            }
            return iRet;
        }
        #endregion
        //Method Added for FB 2027 end

        //FB 2136 Starts
        #region exeimageFactory
        /// <summary>
        /// exeimageFactory
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        private bool exeimageFactory(ref vrmDataObject obj)
        {
            bool iRet = false;
            try
            {
                imageFactory Image = new imageFactory(ref obj);
                switch (obj.ID)
                {
                    case (int)ASPIL.myVRMDefines.ImageFactory.GetAllSecImages:
                        iRet = Image.GetAllSecImages(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ImageFactory.GetSecurityImage:
                        iRet = Image.GetSecurityImage(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ImageFactory.SetSecurityImage:
                        iRet = Image.SetSecurityImage(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ImageFactory.DeleteSecurityImage:
                        iRet = Image.DeleteSecurityImage(ref obj);
                        break;
                    case (int)ASPIL.myVRMDefines.ImageFactory.GetSecImages:
                        iRet = Image.GetSecImages(ref obj);
                        break;
                    default:
                        throw new myVRMException("Invaild method");
                }
            }
            catch (myVRMException e)
            {
                log.Error("vrmException", e);
                throw e;
            }
            catch (Exception e)
            {
                log.Error("sytemException", e);
                throw e;
            }
            return iRet;
        }
        #endregion
        //FB 2136 Ends
    }

}
