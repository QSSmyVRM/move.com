/*ZD 100147 Start*/
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
/*ZD 100147 ZD 100886 End*/
function needLecturer(isIm)
{
	if (isIm) {
		if (document.frmSettings2.MainLoc.value != "") 	// SEB 020905
			if (!haveInvitee(document.frmSettings2.PartysInfo.value)) {
				var willContinue = confirm(EN_144);
				if (!willContinue)
					return (false);
			}
	}
	
	if (document.frmSettings2.LectureMode.value == "1") {
//		url = "lecturerinfoinput.asp?h=" + document.frmSettings2.HostInvite.value + "&l=" + document.frmSettings2.Lecturer.value;
		url = "lecturerinfoinput.aspx?l=" + document.frmSettings2.Lecturer.value; //Login Management
		if (!window.winlii) {
			winlii = window.open(url,'','status=no,width=430,height=200,resizable=yes,scrollbars=no');
			if (winlii)
				winlii.focus();
			else
				alert(EN_132);
		} else {
			if (!winlii.closed) {     // still open
				winlii.focus();
			} else {
				winlii = window.open(url,'','status=no,width=430,height=200,resizable=yes,scrollbars=no');
				if (winlii)
					winlii.focus();
				else
					alert(EN_132);
			}
		}

		return true;
	} else {
		document.frmSettings2.Lecturer.value = "";
		document.frmSettings2.submit ();
	}
	return false;
}

//Code Added by Offshore FB issue no:412-Start
function needLecturerNET(isIm)
{
	if (isIm) {
		if (document.frmSettings2.MainLoc.value != "") 	// SEB 020905
			if (!haveInvitee(document.frmSettings2.txtPartysInfo.value)) {
				var willContinue = confirm(EN_144);
				if (!willContinue)
					return (false);
			}
	}
	
	if (document.frmSettings2.LectureMode.value == "1" && document.frmSettings2.ddlConType.value != "7") { //FB 1957
//		url = "lecturerinfoinput.asp?h=" + document.frmSettings2.HostInvite.value + "&l=" + document.frmSettings2.Lecturer.value;
		url = "lecturerinfoinput.aspx?l=" + document.frmSettings2.Lecturer.value;
		if (!window.winlii) {
			winlii = window.open(url,'','status=no,width=430,height=200,resizable=yes,scrollbars=no');
			if (winlii)
				winlii.focus();
			else
				alert(EN_132);
		} else {
			if (!winlii.closed) {     // still open
				winlii.focus();
			} else {
				winlii = window.open(url,'','status=no,width=430,height=200,resizable=yes,scrollbars=no');
				if (winlii)
					winlii.focus();
				else
					alert(EN_132);
			}
		}
		return true;
	} else {
		document.frmSettings2.Lecturer.value = "";
        document.frmSettings2.hdnSubmit.value = "M";
		document.frmSettings2.submit ();
	}
	return false;
}
//Code Added by Offshore FB issue no:412-End

function resetparty(lpg)
{
	if (document.ifrmLocation) {
		ifrmLocation.location.href = lpg; 
	}

	if (document.frmSettings2.PartysInfo) {
		document.frmSettings2.PartysInfo.value = "<%=partys%>";
		document.frmSettings2.PartysNum.value = "0";
		if (document.ifrmPartylist)
			ifrmPartylist.location.reload(); 
	}
}


function getConfRoom()
{
	document.frmSettings2.MainLoc.value = (s = ifrmLocation.document.frmSettings2loc.selectedloc.value).substring (2, s.length);

	var mod = "";
	mod = ( (document.location.href).indexOf("scheduleroom") != -1 ) ? 1 : mod;
	mod = ( (document.location.href).indexOf("settings2room") != -1 ) ? 2 : mod;
	mod = ( (document.location.href).indexOf("settings2immediate") != -1 ) ? 3 : mod;
	mod = ( (document.location.href).indexOf("settings2p2p") != -1 ) ? 4 : mod;
	mod = ( (document.location.href).indexOf("settings2template") != -1 ) ? 5 : mod;
	mod = ( (document.location.href).indexOf("settings2audio") != -1 ) ? 6 : mod;

	if (mod == 1)
		getConfRoomChart(mod, document.frmSettings2.confName.value, parseInt(document.frmSettings2.dhour.value, 10), parseInt(document.frmSettings2.dmin.value, 10), document.frmSettings2.startDate.value, document.frmSettings2.startHour.value, document.frmSettings2.startMin.value, document.frmSettings2.startSet.value, document.frmSettings2.timeZone.value);
	else
		getConfRoomChart(mod);
}


function getConfRoomChart(id, cfn, dh, dm, sd, sh, sm, sap, tz)
{
	switch (id) {
		case 1:
			confDate = sd;
			confTime = sh + ":" + sm + " " + sap;
			confTZ = tz;
			confDuration = dh * 60 + dm;
			rm = "";
			ConferenceName = cfn;
			
			break;
			
		case 2:
			if (document.frmSettings2.RecurringText.value == "") {
				confDate = document.frmSettings2.ConferenceDate.value;
			
				if (!isValidDate(confDate)) {
					alert(EN_45);
					document.frmSettings2.ConferenceDate.focus();
					return (false);
				}
				if (caldatecompare(confDate, servertoday) == -1) {
					alert(EN_49);
					document.frmSettings2.ConferenceDate.focus();
					return (false);
				}
			
				
				confStartHr = parseInt(document.frmSettings2.ConferenceTimehr.value, 10);
				confStartMi = parseInt(document.frmSettings2.ConferenceTimemi.value, 10);
				if (document.frmSettings2.ConferenceTimeap.selectedIndex == 0) {
					confStartAP = "AM";
				 } else  {
					confStartAP = "PM";
				}
	
				confTime = confStartHr + ":" + confStartMi + " " + confStartAP
				confTZ = document.frmSettings2.ConferenceTimetz.options[document.frmSettings2.ConferenceTimetz.selectedIndex].value;
				
				confDuration = getConfDur (document.frmSettings2);
				
				if ( confDuration < 15 ) {
					alert(EN_31);

					if (document.frmSettings2.ConferenceDurationhr)
					document.frmSettings2.ConferenceDurationhr.focus();
					if (document.frmSettings2.ConferenceEndDate)
						document.frmSettings2.ConferenceEndDate.focus();

					return (false);		
				}
				
			} else {
				confDate = document.frmSettings2.FirstRecurDate.value;
				confTime = document.frmSettings2.FirstRecurTime.value;
				confTZ = document.frmSettings2.FirstRecurTimeZone.value;
				confDuration = document.frmSettings2.FirstRecurDuration.value;

				if (!isValidDate(confDate)) {
				    alert("Error: error de un script de Java. Por favor, notifique al administrador de este error");
					return (false);
				}
				if (caldatecompare(confDate, servertoday) == -1) {
					alert(EN_49);
					return (false);
				}
			}
	
		
			rm = "";
			if (ifrmLocation.document.frmSettings2loc)
				rm = (s = ifrmLocation.document.frmSettings2loc.selectedloc.value).substring (2, s.length);
		
			
			ConferenceName = document.frmSettings2.ConferenceName.value;
			
			break;

		case 4:
		case 6:
			
			if (document.frmSettings2.RecurringText.value == "") {
				confDate = document.frmSettings2.ConferenceDate.value;
			
				if (!isValidDate(confDate)) {
					alert(EN_45);
					document.frmSettings2.ConferenceDate.focus();
					return (false);
				}
				if (caldatecompare(confDate, servertoday) == -1) {
					alert(EN_49);
					document.frmSettings2.ConferenceDate.focus();
					return (false);
				}
			
				
				confStartHr = parseInt(document.frmSettings2.ConferenceTimehr.value, 10);
				confStartMi = parseInt(document.frmSettings2.ConferenceTimemi.value, 10);
				if (document.frmSettings2.ConferenceTimeap.selectedIndex == 0) {
					confStartAP = "AM";
				 } else  {
					confStartAP = "PM";
				}
	
				confTime = confStartHr + ":" + confStartMi + " " + confStartAP
				confTZ = document.frmSettings2.ConferenceTimetz.options[document.frmSettings2.ConferenceTimetz.selectedIndex].value;

				confDuration = getConfDur (document.frmSettings2);
				
				if ( confDuration < 15 ) {
					alert(EN_31);

					if (document.frmSettings2.ConferenceDurationhr)
					document.frmSettings2.ConferenceDurationhr.focus();
					if (document.frmSettings2.ConferenceEndDate)
						document.frmSettings2.ConferenceEndDate.focus();

					return (false);		
				}
				
			} else {
				confDate = document.frmSettings2.FirstRecurDate.value;
				confTime = document.frmSettings2.FirstRecurTime.value;
				confTZ = document.frmSettings2.FirstRecurTimeZone.value;
				confDuration = document.frmSettings2.FirstRecurDuration.value;

				if (!isValidDate(confDate)) {
				    alert("Error: error de un script de Java. Por favor, notifique al administrador de este error");
					return (false);
				}
				if (caldatecompare(confDate, servertoday) == -1) {
					alert(EN_49);
					return (false);
				}
			}
	
		
			rm = "";
			if (ifrmLocation.document.frmSettings2loc)
				rm = (s = ifrmLocation.document.frmSettings2loc.selectedloc.value).substring (2, s.length);
		
			
			ConferenceName = document.frmSettings2.ConferenceName.value;
			
			break;
			
		case 3:
		case 5:
			confDate = "im";
			confTime = "im";
			confTZ = "im";

			confDuration = getConfDur (document.frmSettings2);
			
			if ( confDuration < 15 ) {
				alert(EN_31);
				
				if (document.frmSettings2.ConferenceDurationhr)
				document.frmSettings2.ConferenceDurationhr.focus();
				if (document.frmSettings2.ConferenceEndDate)
					document.frmSettings2.ConferenceEndDate.focus();

				return (false);		
			}

			rm = "";
			ConferenceName = document.frmSettings2.ConferenceName.value
			
			break;
	}

	url = "preconferenceroomchart.asp?wintype=ifr" +
		"&date=" + confDate + "&time=" + confTime + "&timeZone=" + confTZ +
		"&duration=" + confDuration + "&r=" + rm + "&n=" + ConferenceName;

	wincrm = window.open(url,'confroomchart','status=no,width=1,height=1,top=30,left=0,scrollbars=yes,resizable=yes')
	if (wincrm)
		wincrm.focus();
	else
		alert(EN_132);
}


function JAcall (starttime, endtime)
{
	if (!opener.document.frmSettings2) {
		alert(EN_88);
		window.close();
	}
	
//alert(opener.location.href)
	if ((opener.location.href).indexOf("customizeinstance") == -1) {
		cb1 = opener.document.frmSettings2.ConferenceDate;
		cb2 = opener.document.frmSettings2.ConferenceTimehr;
		cb3 = opener.document.frmSettings2.ConferenceTimemi;
		cb4 = opener.document.frmSettings2.ConferenceTimeap;
	} else {
		cb1 = null;
		cb2 = eval("opener.document.frmSettings2.InstanceTimehr" + queryField("i"));
		cb3 = eval("opener.document.frmSettings2.InstanceTimemi" + queryField("i"));
		cb4 = eval("opener.document.frmSettings2.InstanceTimeap" + queryField("i"));
	}
	
	
	if ((starttime != "") && (endtime != "")) {
		var fd = new Date(starttime);
		var td = new Date(endtime);
 					
		durmin = (td - fd) / 1000  / 60;

		var newdate = ( (fd.getMonth() < 9) ? "0" : "") + (fd.getMonth() + 1) + "/" + fd.getDate() + "/" + fd.getFullYear();
		if ((opener.location.href).indexOf("customizeinstance") == -1)
			cb1.value = newdate;
		else {
			if (caldatecompare(newdate, queryField("d")) != 0) {
				alert(EN_202);
				return;
			}
		}

		sthridx = (fd.getHours()==0) ? 11 : ( (fd.getHours() > 12) ? (fd.getHours() - 13) : fd.getHours() - 1 );

		//cb = opener.document.frmSettings2.ConferenceTimehr;
		cb2.options[sthridx].selected = true;

		//cb = opener.document.frmSettings2.ConferenceTimemi;
		for (var i=0; i<cb3.length; i++) {
			if (cb3.options[i].value == fd.getMinutes())
				cb3.options[i].selected = true;
		}
					
		//cb = opener.document.frmSettings2.ConferenceTimeap;
		cb4.options[((fd.getHours() < 12) ? 0 : 1)].selected = true;
	}
}


function deleteAllParty()
{
	if (document.frmSettings2.PartysInfo.value != "") {
	    var isRemove = confirm("\u00bfEst\u00e1 seguro de que quiere quitar todos los asistentes?")
		if (isRemove == false) {
			return (false);
		}
	}

	document.frmSettings2.PartysInfo.value = "";
	var url = ifrmPartylist.location + "";
	if(url.indexOf('settings2party.asp') != -1){
		ifrmPartylist.location.reload(); 
	}
	
	for (var i=0; i<document.frmSettings2.Group.length; i++) {
		document.frmSettings2.Group.options[i].selected = false;
	}
}

function deleteAllPartyNET()
{
	if (document.getElementById("txtPartysInfo").value != "") {
	    var isRemove = confirm("\u00bfEst\u00e1 seguro de que quiere quitar todos los asistentes?")
		if (isRemove == false) {
			return (false);
		}
	}
	document.getElementById("hdnParty").value = ""; //FB 2018
	document.getElementById("txtPartysInfo").value = "";
	var url = ifrmPartylist.location + "";
	//Code Added by Offshore FB issue no:412-Start
	//if(url.indexOf('settings2partyNET.asp') != -1){
    if(url.indexOf('settings2partyNET.aspx') != -1 || (url.indexOf('settings2partyNET.aspx') != -1)||(url.indexOf('settings2party.aspx') != -1)){
	{	
		ifrmPartylist.location.reload(); 
	}
	//Code Added by Offshore FB issue no:412-End
		ifrmPartylist.location.reload(); 
	}
}

function deleteAllPartyEx()
{
	if (document.frmSettings2.PartysInfoEx.value != "") {
	    var isRemove = confirm("\u00bfEst\u00e1 seguro de que quiere quitar todos los asistentes?")
		if (isRemove == false) {
			return (false);
		}
	}

	document.frmSettings2.PartysInfoEx.value = "";
	var url = ifrmPartylistEx.location + "";
	if(url.indexOf('settings2partyEx.asp') != -1){
		ifrmPartylistEx.location.reload(); 
	}
}


function deleteAllSession()
{
	if (document.frmSettings2.SessionInfo.value != "") {
	    var isRemove = confirm("\u00bfEst\u00e1 seguro de que quiere quitar todos los asistentes?")
		if (isRemove == false) {
			return (false);
		}
	}

	document.frmSettings2.SessionInfo.value = "";
	var url = ifrmSessionlist.location + "";
	if(url.indexOf('settings2Session.asp') != -1){
		ifrmSessionlist.location.reload(); 
	}
}

function deleteAllObjective()
{
	if (document.frmSettings2.ObjectiveInfo.value != "") {
	    var isRemove = confirm("\u00bfEst\u00e1 seguro de que quiere quitar todos los asistentes?")
		if (isRemove == false) {
			return (false);
		}
	}

	document.frmSettings2.ObjectiveInfo.value = "";
	var url = ifrmObjectivelist.location + "";
	if(url.indexOf('settings2Objective.asp') != -1){
		ifrmObjectivelist.location.reload(); 
	}
}

function limitDescriptionLen(obj)
{
	var iKey;
	var eAny_Event = window.event;
	iKey = eAny_Event.keyCode;
	var re 
	re = new RegExp("\r\n","g")  
	x = obj.value.replace(re,"").length ;
//	alert(iKey + ";" + ( ((iKey > 33 && iKey < 255) || (iKey > 95 && iKey < 106)) && (iKey != 13)) )
//	if ((x >= obj.maxlength) && ((iKey > 33 && iKey < 255) || (iKey > 95 && iKey < 106)) && (iKey != 13)) {
	if (x >= obj.maxlength) {
		alert(EN_140);
		obj.value = (obj.value).substr(0, obj.maxlength)
		window.event.returnValue=false;
	}
}


function clkpublic(cb, dynInvite)
{
	if (dynInvite == "1") {
		document.getElementById("DyamicInviteDIV").style.display = (cb.checked) ? "block" : "none";
		document.frmSettings2.DynamicInvite.checked = (cb.checked) ? document.frmSettings2.DynamicInvite.checked : false;
	}
}


function refreshroomNET()
{
	url = "dispatcher/conferencedispatcher.asp?cmd=GetAvailableRoom&f=frmSettings2&wintype=ifr&ci=" + document.frmSettings2.ConfID.value; // + "&m=" + queryField("m");

	if (document.frmSettings2.Recur.value == "" ) {
		sdt = document.getElementById("confStartDate").value;
		shr = document.getElementById("confStartTime_Text").value.split(":")[0];
		smn = document.getElementById("confStartTime_Text").value.split(":")[1].split(" ")[0];
		sst = document.getElementById("confStartTime_Text").value.split(":")[1].split(" ")[1];
		tz = document.getElementById("lstConferenceTZ").value;

		dm = getConfDurNET (document.frmSettings2);
		
		url += "&t=1&sdt=" + sdt + "&shr=" + shr + "&smn=" + smn + "&sst=" + sst + "&tz=" + tz + "&dm=" + dm;
	} else {

		url += "&t=2&r=" + ((document.frmSettings2.Recur.value).replace(/&/gi, "a")).replace(/#/gi, "b");
	}
}

function refreshroom()
{
//alert("in refresh room");
	//  if (!confDateCheck())
	//	return false;
//alert(queryField('m'));
	url = "dispatcher/conferencedispatcher.asp?cmd=GetAvailableRoom&f=frmSettings2&wintype=ifr&ci=" + document.frmSettings2.ConfID.value; // + "&m=" + queryField("m");

	if (document.frmSettings2.CreateBy.value == "3" ) {
		dm = getConfDur (document.frmSettings2);
			
		url += "&t=0&dm=" + dm;
	} else {
		if (document.frmSettings2.Recur.value == "" ) {
			sdt = document.frmSettings2.ConferenceDate.value;
			shr = document.frmSettings2.ConferenceTimehr.options[document.frmSettings2.ConferenceTimehr.selectedIndex].value;
			smn = document.frmSettings2.ConferenceTimemi.options[document.frmSettings2.ConferenceTimemi.selectedIndex].value;
			sst = document.frmSettings2.ConferenceTimeap.options[document.frmSettings2.ConferenceTimeap.selectedIndex].value;
			tz = document.frmSettings2.ConferenceTimetz.options[document.frmSettings2.ConferenceTimetz.selectedIndex].value;

			dm = getConfDur (document.frmSettings2);
			
			url += "&t=1&sdt=" + sdt + "&shr=" + shr + "&smn=" + smn + "&sst=" + sst + "&tz=" + tz + "&dm=" + dm;
		} else {

			url += "&t=2&r=" + ((document.frmSettings2.Recur.value).replace(/&/gi, "a")).replace(/#/gi, "b");
		}
		
	}

	ifrmLocation.location.href = url;
	document.frmSettings2.settings2locstr.value = "";
//	ifrmLocaltion.
//	ifrmLocation.location.reload();

	document.frmSettings2.GetAvailableRoom.disabled = false;
	window.status = "Por favor, espere a que acabe para refrescar la lista de ubicaciones...";
	document.frmSettings2.roomListDisplayMod[0].checked = true;
	if (typeof(aryConfRoom) != "undefined") aryConfRoom = "";
}


function UploadFile(fnum)
{
	isTm = ( (document.location.href).indexOf("2template") != -1 ) ;
	t = (isTm) ? (mousedownY + 8) : (mousedownY - 45);
	l = (isTm) ? (mousedownX - 175) : (mousedownX + 28);
	statusparam = "status=no,width=1,height=1,top=" + t + ",left=" + l + ",scrollbars=no,resizable=yes"

	switch (navigator.appName) {
		case "Microsoft Internet Explorer":
			uploadfile_prompt('image/pen.gif','Upload file', fnum);	
			ifrmPreloading.window.location.href = "fileupload1.asp?f=1"
			break;
			
		case "Netscape":
			winfu = window.open('fileupload.asp?n=3&f=1','fileupload',statusparam)
			if (winfu)
				winfu.focus();
			else
				alert(EN_132);
			break;
			
		defaule:
		alert("Esta usando un navegador que no soporta la funcion de adjuntar archivos. Por favor use IE6.0+ or NetScape7.0+ or +");
			break;
	}
}



function confDateCheck()
{
	var durtime;
	
	if ( (document.location.href).indexOf("2immediate") != -1 ) {
		durtime = getConfDur (document.frmSettings2);
		
		if ( durtime < 15 ) {
			alert(EN_31);

			if (document.frmSettings2.ConferenceDurationhr)
			document.frmSettings2.ConferenceDurationhr.focus();
			if (document.frmSettings2.ConferenceEndDate)
				document.frmSettings2.ConferenceEndDate.focus();
					
			return (false);		
		}
		return true;
	}

	if (document.frmSettings2.Recur.value == "") {
		if (document.frmSettings2.ModifyType.value!="1") {
			confDate = document.frmSettings2.ConferenceDate.value
			if (!isValidDate(confDate)) {
				alert(EN_45);
				document.frmSettings2.ConferenceDate.focus();
				return (false);
			}
		
			if (caldatecompare(confDate, servertoday) == -1) {
				alert(EN_49);
				document.frmSettings2.ConferenceDate.focus();
				return (false);
			}
	
			durtime = getConfDur (document.frmSettings2);
			if ( durtime < 15 ) {
				alert(EN_31);
				if ((document.location.href).indexOf("Settings2Event") != -1)
					parent.OpenDIV("1");

				if (document.frmSettings2.ConferenceDurationhr)
				document.frmSettings2.ConferenceDurationhr.focus();
				if (document.frmSettings2.ConferenceEndDate)
					document.frmSettings2.ConferenceEndDate.focus();
					
				return (false);		
			}
		}
	}
	return true;
}


function genuploadfilestr ()
{
	var m = "";
	var mod = "";

	mod = ( (document.location.href).indexOf("future") != -1 ) ? 2 : mod;
	mod = ( (document.location.href).indexOf("room") != -1 ) ? 1 : mod;
	mod = ( (document.location.href).indexOf("immediate") != -1 ) ? 1 : mod;
	mod = ( (document.location.href).indexOf("p2p") != -1 ) ? 1 : mod;
	mod = ( (document.location.href).indexOf("template") != -1 ) ? 1 : mod;
	mod = ( (document.location.href).indexOf("audio") != -1 ) ? 1 : mod;
	mod = ( (document.location.href).indexOf("Event") != -1 ) ? 1 : mod;
	m += "<table width=100% border=0>";
	m += (mod == 2) ? "<tr>" : "";
	for (var i = 1; i <= 3; i++) {
		v = document.getElementById("conffile" + i).value;
		m += (mod == 1) ? "<tr>" : "";
		m += "<td align=left valign=top><span class=''><a href='" + v.replace(/'/g, "&#39;") + "' target='_blank'>" + getUploadFileName(v) + "</a></span></td>";
		m += (mod == 1) ? "</tr>" : "";
	}
	m += (mod == 2) ? "</tr>" : "";
	m += "</table>";
			
	document.getElementById ("UploadFileDIV").innerHTML = m;
}

// ZD 102587- Start
function getGuest() {
    var url;    
    if (typeof (ifrmPartylist) != "undefined")    
        url = ifrmPartylist.location + "";
    else
        url = parent.ifrmPartylist.location + "";
	var fname = "settings2party.asp";
	var frm = "party2";
	//Code Added by Offshore FB issue no:412-Start
	if (window.location.href.indexOf("managetemplate2.aspx") != -1 || parent.window.location.href.indexOf("managetemplate2.aspx") != -1)
	{

	    fname = "settings2party.aspx";
	    frm = "party2Aspx";
	}
	else//Code Added by Offshore FB issue no:412-End
	    if (window.location.href.indexOf(".aspx") != -1 || parent.window.location.href.indexOf(".aspx") != -1)
	{
	    //Code Changed for FB 412 start
	    //fname = "settings2partyNET.asp";
	    fname = "settings2partyNET";//ZD 100834
	    //Code Changed for FB 412 End
	    frm = "party2NET";
	}
	//FB 2266 - Starts
	//ZD 100834 Starts
	if(parent.window.document.getElementById('hdndetailexpform') != null)
    {
        if(parent.window.document.getElementById('hdndetailexpform').value == 0)
        {
	        if (window.location.href.indexOf("ExpressConference.aspx?t=n") != -1) 
            {    
	            fname = "Settings2PartyNETExpress.aspx";
	            frm = "party2NET";
	        }
        }
    }
	//ZD 100834 End
	//FB 2266 - End
	if (url.indexOf(fname) != -1) {
	    if (typeof (ifrmPartylist) != "undefined")
	        willContinue = ifrmPartylist.bfrRefresh();
	    else
	        willContinue = parent.ifrmPartylist.bfrRefresh();

	    if (willContinue) {
	        if (typeof (ifrmPartylist) != "undefined")
	            ifrmPartylist.location.reload();
	        else
	            parent.ifrmPartylist.location.reload(); 

//			url = "dispatcher/conferencedispatcher.asp?frm=" + frm + "&cmd=GetGuestList&emailListPage=1&wintype=pop"; //Login Management
			url = "emaillist2main.aspx?t=g&frm="+frm+"&wintype=ifr";//Login Management
			
			if (!window.winrtc) {	// has not yet been defined
			    winrtc = window.open(url, "", "width=920,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no"); //FB 2735
				winrtc.focus();
			} else // has been defined
			    if (!winrtc.closed) {     // still open
			    	winrtc.close();
			    	winrtc = window.open(url, "", "width=920,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no"); //FB 2735
					winrtc.focus();
				} else {
				winrtc = window.open(url, "", "width=920,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no"); //FB 2735
			        winrtc.focus();
				}
		}
	}
}
// ZD 102587- End

function saveInGroup ()
{
	willContinue = ifrmPartylist.bfrRefresh(); 
	if (!willContinue) 
		return false;

	if (document.frmSettings2.PartysInfo.value == "") {
		alert(EN_128);
		return (false);
	}

	save_in_group_prompt('image/pen.gif', 'Intorduzca la Informaci�n del Grupo');
}
function saveInGroupNET ()
{
	willContinue = ifrmPartylist.bfrRefresh(); 
	if (!willContinue) 
		return false;
		
	if (document.getElementById("txtPartysInfo").value == "") {
		alert(EN_128);
		return (false);
	}

	save_in_group_prompt('image/pen.gif', 'Intorduzca la Informaci\u00f3n del Grupo');
}


function getAGroupDetail(frm, cb, gid)
{
//alert("cb: " + cb);
	if (cb == null) {
		if (gid != null) {
			id = gid;
		} else {
		alert("Lo lamentamos, el sistema encontr\u00f3 alg\u00fan error. Por favor, notifique a su administrador")
			return false;
		}
	} else {
//alert(cb.selectedIndex);
		if (cb.selectedIndex != -1) {
			if (gid != null) id = gid; else id = cb.options[cb.selectedIndex].value;
		} else {
			alert(EN_53);
			return false;
		}
	}
	//alert(frm);
	if (frm == "1") //Code modified by revathi start for aspx conversion.
	{
	    if (document.location.href.indexOf(".aspx") >= 0)
	        gm = window.open("SetSessionOutXml.aspx?tp=memberallstatus.aspx&f=" + frm + "&GroupID=" + id + "&wintype=pop", "groupmember", "status=no,width=420,height=300,scrollbars=yes,resizable=yes");
	    else
	        gm = window.open("memberallstatus.aspx?f=" + frm + "&GroupID=" + id + "&wintype=pop", "groupmember", "status=no,width=420,height=300,scrollbars=yes,resizable=yes");
	}
	else
    	gm = window.open("memberallstatus.aspx?f=" + frm + "&GroupID=" + id + "&wintype=pop", "groupmember", "status=no,width=420,height=300,scrollbars=yes,resizable=yes");
	   
	if (gm) gm.focus(); else alert(EN_132);
}
//Code modified by revathi end

function initGroup(gno, id)
{
	var setpartyinvitee = false;

	setpartyinvitee = ( (document.location.href).indexOf("room") != -1 ) ? true : setpartyinvitee;
	setpartyinvitee = ( (document.location.href).indexOf("p2p") != -1 ) ? true : setpartyinvitee;
	
	var url = ifrmPartylist.location + "";
	if(url.indexOf('settings2party.asp') != -1){
		new_pi = document.frmSettings2.PartysInfo.value

		usersinfo = document.frmSettings2.UsersStr.value;
		usersary = usersinfo.split("|");

		i = gno;
		partysary = usersary[i].split(";");
		for (var j=0; j < partysary.length-1; j++) {
			partyary = partysary[j].split(",");
			if ( (new_pi.indexOf("," + partyary[3] + ",") == -1) ) {
				new_pi += partyary[0] + "," + partyary[1] + "," + partyary[2] + "," + partyary[3] + ",";
				switch (id) {
					case 1:
						if (location)
						new_pi += ( (setpartyinvitee) ? "0,1,0," : "1,0,0," ) + "0,0,1,,,3,,,;";
					break
					case 2:
						new_pi += "0,0,1,0,0,1,,,3,,,;";
					break
				}
			}
		}

		document.frmSettings2.PartysInfo.value = new_pi;
		ifrmPartylist.location.reload(); 
	}
}



function addNewParty(isRm)
{
	var url = ifrmPartylist.location + "";
	if(url.indexOf('settings2party.asp') != -1){
	willContinue = ifrmPartylist.bfrRefresh();	
		if (willContinue) {
			partysinfo = document.frmSettings2.PartysInfo.value;
			partysinfo = ",,,,0,1,0,0,0,1,,,0,,,;" + partysinfo;
			document.frmSettings2.PartysInfo.value = partysinfo;
			ifrmPartylist.location.reload(); 
		}
	}
}
//FB 1759 - Starts
//Code Added by Offshore FB issue no:412-Start

/*function addNewParty1(isRm)
{
	var url = ifrmPartylist.location + "";
	if(url.indexOf('settings2party.aspx') != -1){
	willContinue = ifrmPartylist.bfrRefresh();	
		if (willContinue) {
				partysinfo = document.getElementById("txtPartysInfo").value;
			partysinfo = ",,,,0,1,0,0,0,1,,,0,,,;" + partysinfo;
	        document.getElementById("txtPartysInfo").value  = partysinfo;
			ifrmPartylist.location.reload(); 
		}
	}
}*/
//Code Added by Offshore FB issue no:412-End
  
function addNewParty1(isRm)
{
    isRm = parent.document.getElementById("CreateBy").value;
	var url = ifrmPartylist.location + "";
	if(url.indexOf('settings2party.aspx') != -1){
	willContinue = ifrmPartylist.bfrRefresh();	
		if (willContinue) {
			partysinfo = document.getElementById("txtPartysInfo").value;
			if (isRm == "7" || isRm == "6")
			    partysinfo = "!!!!!!!!0!!1!!0!!1!!0!!1!!!!!!0!!!!!!!!0!!0!!0!!0!!0!!1!!0||" + partysinfo; //FB 1888 //FB 2348 //FB 2550 - public Party ALLDEV-827
    			//partysinfo = ",,,,0,1,0,0,0,1,,,0,,,;" + partysinfo;
    		if (isRm == "2" || isRm == "4" || isRm == "9") //ZD 100513
    		    partysinfo = "!!!!!!!!0!!1!!0!!1!!1!!0!!!!!!0!!!!!!!!0!!0!!0!!0!!0!!1!!0||" + partysinfo; //FB 1888 //FB 2348 //FB 2550 - public Party ALLDEV-827
    		    //partysinfo = ",,,,1,0,0,0,1,0,,,0,,,;" + partysinfo;
    		//if (isRm == "6")//ALLDEV-827 start
    		 //   partysinfo = "!!!!!!!!1!!0!!0!!0!!0!!1!!!!!!0!!!!!!!!0!!0!!0!!0!!0!!1!!0||" + partysinfo; //FB 1888 //FB 2348 //FB 2550 - public Party
    		    //partysinfo = ",,,,1,0,0,0,0,1,,,0,,,;" + partysinfo;
			document.getElementById("txtPartysInfo").value = partysinfo;
			ifrmPartylist.location.reload(true);
		}
	}
}
//FB 1759 - End
function addNewPartyNET(isRm)
{
    isRm = parent.document.getElementById("CreateBy").value;
	var url = ifrmPartylist.location + "";
	//Code changed for FB 412 start
	//if(url.indexOf('settings2partyNET.asp') != -1){
	if((url.indexOf('settings2partyNET.aspx') != -1) || (url.indexOf('Settings2PartyNETExpress.aspx') != -1)){
	
	//Code changed for FB 412 End
	willContinue = ifrmPartylist.bfrRefresh();	
		if (willContinue) {
			partysinfo = document.getElementById("txtPartysInfo").value;
			if (isRm == "7" || isRm == "6") //ALLDEV-814
			    partysinfo = "!!!!!!!!0!!1!!0!!1!!0!!1!!!!!!0!!!!!!!!0!!0!!0!!0!!0!!1!!0||" + partysinfo; //FB 1888 //FB 2348 //FB 2550 - public Party //ZD 101322 ALLDEV-827
    			//partysinfo = ",,,,0,1,0,0,0,1,,,0,,,;" + partysinfo;
    		if (isRm == "2" || isRm == "4" || isRm == "9") //ZD 100513
    		    partysinfo = "!!!!!!!!0!!1!!0!!1!!1!!0!!!!!!0!!!!!!!!0!!0!!0!!0!!0!!1!!0||" + partysinfo; //FB 1888 //FB 2348 //FB 2550 - public Party //ZD 101322 ALLDEV-827
    		    //partysinfo = ",,,,1,0,0,0,1,0,,,0,,,;" + partysinfo;
    		//if (isRm == "6")//ALLDEV-827 start
    		  //  partysinfo = "!!!!!!!!1!!0!!0!!1!!0!!1!!!!!!0!!!!!!!!0!!0!!0!!0!!0!!1!!0||" + partysinfo; //FB 1888 //FB 2348 //FB 2550 - public Party //ZD 101322
    		    //partysinfo = ",,,,1,0,0,0,0,1,,,0,,,;" + partysinfo;
			document.getElementById("txtPartysInfo").value = partysinfo;
			ifrmPartylist.location.reload(true);//Edited For FF..
		}
	}
}
//ZD 101267 Starts
function addNewExpressPartyNET(isRm) {
    isRm = parent.document.getElementById("CreateBy").value;
    var url = ifrmPartylist.location + "";
    if ((url.indexOf('settings2partyNET.aspx') != -1) || (url.indexOf('Settings2PartyNETExpress.aspx') != -1)) {
        willContinue = ifrmPartylist.bfrRefresh();
        if (willContinue) {
            partysinfo = document.getElementById("txtPartysInfo").value;
            if (isRm == "7" || isRm == "6")
                partysinfo = "!!!!!!!!0!!1!!0!!1!!0!!1!!!!!!0!!!!!!!!0!!0!!0!!0!!0!!1!!0||" + partysinfo; //ZD 101322
            if (isRm == "2" || isRm == "4" || isRm =="9") //ZD 100513
                partysinfo = "!!!!!!!!0!!1!!0!!1!!1!!0!!!!!!0!!!!!!!!0!!0!!0!!0!!0!!1!!0||" + partysinfo; //ZD 101322
            document.getElementById("txtPartysInfo").value = partysinfo;
            ifrmPartylist.location.reload(true);
        }
    }
}
//ZD 101267 End
function addNewPartyEx(isRm)
{
	var url = ifrmPartylistEx.location + "";
	//parent.alert(url);
	if(url.indexOf('settings2partyEx.asp') != -1){
	willContinue = ifrmPartylistEx.bfrRefreshEx();
	//alert(willContinue);
		if (willContinue) {
			partysinfo = document.frmSettings2.PartysInfoEx.value;
			partysinfo = ",,,," + ( (isRm) ? "0,1,0," : "1,0,0," ) + "0,0,1,,,0,,,,;" + partysinfo;
			document.frmSettings2.PartysInfoEx.value = partysinfo;
			ifrmPartylistEx.location.reload(); 
		}
	}
}

//Method moved from ConferenceSetup.aspx to Settings2.js during FB 1734 STARTS
function getAudioparticipantListNET()
 {
    var url = ifrmPartylist.location + "";
    if((url.indexOf('settings2party.aspx') != -1)||(url.indexOf('settings2partyNET.aspx') != -1))//Changed for settings2partynet.aspx
	{
		willContinue = ifrmPartylist.bfrRefresh(); 
		if (willContinue) 
		{
			ifrmPartylist.location.reload(); 
			
			var partys =  document.getElementById("txtPartysInfo").value;
            //FB 1640 - Start
            var intIndexOfMatch = partys.indexOf( "++" );
            while (intIndexOfMatch != -1)
            {
                partys = partys.replace( "++", "@@" )
                intIndexOfMatch = partys.indexOf( "++" );
            }
            //FB 1640 - End
            
            url = "AudioparticipantList.aspx?partys="+ partys;
			
			if (!window.winrtc) {	// has not yet been defined
			    winrtc = window.open(url, "", "width=920,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no"); //FB 2735
			    winrtc.focus();
		    } else // has been defined
		        if (!winrtc.closed) {     // still open
		    	    winrtc.close();
		            winrtc = window.open(url, "", "width=920,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no"); //FB 2735
				    winrtc.focus();
			    } else {
		            winrtc = window.open(url, "", "width=920,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no"); //FB 2735
		            winrtc.focus();
			}
		}
	}
}
// FB 1734 ENDS
		
function addNewSession(isRm)
{
	var url = ifrmSessionlist.location + "";
	if(url.indexOf('settings2Session.asp') != -1){
	
	willContinue = ifrmSessionlist.bfrRefreshEx();
		if (willContinue) {
			partysinfo = document.frmSettings2.SessionInfo.value;
			partysinfo = ",,,,,;" + partysinfo;
			document.frmSettings2.SessionInfo.value = partysinfo;
			ifrmSessionlist.location.reload(); 
		}
	}
}
function addNewConsumer(isRm)
{
	var url = ifrmConsumerlist.location + "";
	if(url.indexOf('settings2Consumer.asp') != -1){
	willContinue = ifrmConsumerlist.bfrRefreshEx();
		if (willContinue) {
			partysinfo = document.frmSettings2.ConsumerInfo.value;
			partysinfo = ",,,,,,;" + partysinfo;
			document.frmSettings2.ConsumerInfo.value = partysinfo;
			ifrmConsumerlist.location.reload(); 
		}
	}
}
function addNewSR(RmID, RmName)
{
	var url = ifrmSRlist.location + "";
	if(url.indexOf('settings2SpecialRequest.asp') != -1){

	willContinue = ifrmSRlist.bfrRefreshEx();
		if (willContinue) {
			partysinfo = document.frmSettings2.SRInfo.value;
			partysinfo = "," + RmID + "," + RmName + ",,,,,,,,,,,,,,,;" + partysinfo;
			document.frmSettings2.SRInfo.value = partysinfo;
			ifrmSRlist.location.reload(); 
		}
	}
}

function addNewObjective(isRm)
{
	var url = ifrmObjectivelist.location + "";
	if(url.indexOf('settings2Objective.asp') != -1){
	willContinue = ifrmObjectivelist.bfrRefreshEx();
		if (willContinue) {
			partysinfo = document.frmSettings2.ObjectiveInfo.value;
			partysinfo = ",,,,,,,,;" + partysinfo;
			document.frmSettings2.ObjectiveInfo.value = partysinfo;
			ifrmObjectivelist.location.reload(); 
		}
	}
}



function chkConfPassword(cb)
{
	pwd = cb.value;//FB 2017
	if(document.frmSettings2.ddlConType.value != "7")
	{
	    if (pwd != "") {
		    if (!checkInvalidChar(pwd))  {
			    cb.focus();
			    return false;
		    }

		    if  (pwd.search("[^0-9]") >=0)  {
			    alert(EN_187);
			    cb.focus();
			    return false;
		    }
			//FB 2017
		    if (parseInt(pwd, 10)>9999999999) {
			    //alert(EN_187); //FB 1140
			    alert(EN_208);
			    cb.focus();
			    return false;
		    }

		    return true;
	    }
	}
	
	return true;
}


function viewconfofselroom()
{
	if (ifrmLocation) {
		if (ifrmLocation.childwinclose)
			ifrmLocation.childwinclose();
			
		if (ifrmLocation.viewselectedavailability)
			ifrmLocation.viewselectedavailability();
	}
}


function isRecur()
{
	var t = (document.frmSettings2.Recur.value == "") ? "" : "none";

	if ((document.location.href).indexOf("template") != -1) {
		for(var i = 1; i < 4; i++)
			document.getElementById("NONRecurringConferenceDiv"+i).style.display = t;
	} else {
		divnum = ( (document.location.href).indexOf("future") != -1 ) ? 4 : 5;

		for(var i = 1; i < divnum; i++)
			document.getElementById("NONRecurringConferenceDiv"+i).style.display = t;
	}
}


function getConfDur (pagefrm)
{
	var shr, smi, sst, sdt, ehr, emi, est, edt, dur;

	if (pagefrm.ConferenceDurationhr)
		dur = parseInt(pagefrm.ConferenceDurationhr.value, 10) * 60 + parseInt(pagefrm.ConferenceDurationmi.value, 10); // ZD 101722

	if (pagefrm.ConferenceEndDate) {
		sdt = pagefrm.ConferenceDate.value;
		shr = parseInt(pagefrm.ConferenceTimehr.value, 10); // ZD 101722
		smi = parseInt(pagefrm.ConferenceTimemi.value, 10); // ZD 101722
		sst = pagefrm.ConferenceTimeap.value;
		
		edt = pagefrm.ConferenceEndDate.value;
		ehr = parseInt(pagefrm.ConferenceEndTimehr.value, 10); // ZD 101722
		emi = parseInt(pagefrm.ConferenceEndTimemi.value, 10); // ZD 101722
		est = pagefrm.ConferenceEndTimeap.value;

		dur = calDur_long(sdt, shr, smi, sst, edt, ehr, emi, est);
	}
	
	return dur;
}
//function getConfDurNET (pagefrm,frmt) //FB 1454
function getConfDurNET (pagefrm,frmt,sTime,eTime) //FB 1454 //FB 2558
{
	var shr, smi, sst, sdt, ehr, emi, est, edt, dur;
		sdt = pagefrm.confStartDate.value;
        sdt = GetDefaultDate(sdt,frmt); //FB 1454
        //FB 2558
		shr = parseInt(sTime.split(":")[0],10);
		smi = parseInt(sTime.split(":")[1].split(" ")[0],10);
		sst = sTime.split(":")[1].split(" ")[1];

		edt = pagefrm.confEndDate.value;
		edt = GetDefaultDate(edt,frmt); //FB 1454
		//FB 2558
		ehr = parseInt(eTime.split(":")[0],10);
		emi = parseInt(eTime.split(":")[1].split(" ")[0],10);
		est = eTime.split(":")[1].split(" ")[1];
		dur = calDur_long(sdt, shr, smi, sst, edt, ehr, emi, est);
	
	return dur;
}

function openAdvanceRoom (show)
{
	var sdt, shr, smi, sst, dur, startdt, enddt;

	if ( (document.frmSettings2.AdvancedRoomAllocaiton.value == "0") && (ifrmLocation.document.frmSettings2loc.selectedloc) && (ifrmLocation.document.frmSettings2loc.locidname) ) {
		document.frmSettings2.MainLoc.value = (s = ifrmLocation.document.frmSettings2loc.selectedloc.value).substring (2, s.length);

		if (document.frmSettings2.Recur.value == "") {
			sdt = document.frmSettings2.ConferenceDate.value;
			shr = parseInt(document.frmSettings2.ConferenceTimehr.value, 10); // ZD 101722
			smi = parseInt(document.frmSettings2.ConferenceTimemi.value, 10); // ZD 101722
			sst = document.frmSettings2.ConferenceTimeap.value;
				
			dur = getConfDur( eval('document.frmSettings2') );
			if (dur < 15) {
				alert(EN_31);
			if ((document.location.href).indexOf("Settings2Event") != -1)
				parent.OpenDIV("1");

				if (document.frmSettings2.ConferenceDurationhr)
					document.frmSettings2.ConferenceDurationhr.focus();
				if (document.frmSettings2.ConferenceEndDate)
					document.frmSettings2.ConferenceEndDate.focus();
				
				return false;
			}
			
			startdt = sdt + " " + shr + ":" + smi + " " + sst;
			enddt = calEnd_long(startdt, dur);

			if (daysElapsed(sdt, enddt[0]) != 0) {
			    alert("Asignaci�n de Sala Anticipada requiere el mismo d�a de la conferencia. Por favor, cambie a conferencia de un d�a o.");
				if ((document.location.href).indexOf("Settings2Event") != -1)
					parent.OpenDIV("1");
					
				if (document.frmSettings2.ConferenceDurationhr)
					document.frmSettings2.ConferenceDurationhr.focus();
				if (document.frmSettings2.ConferenceEndDate)
					document.frmSettings2.ConferenceEndDate.focus();
				return false;
			}
			
		} else {
			rpstr = AnalyseRecurStr(document.frmSettings2.Recur.value);
				
			shr = atint[1];
			smi = atint[2];
			sst = atint[3];
			dur = atint[4];
			if (dur < 15) {
				alert(EN_31);
				if ((document.location.href).indexOf("Settings2Event") != -1)
					parent.OpenDIV("1");
				return false;
			}
		}

		startstr = ((shr<10) ? ("0"+shr) : shr) + ":" + ((smi<10) ? ("0"+smi) : smi) + " " + sst;
		starttime = calStart(shr, smi, sst);
		endstr = calEnd (starttime, dur)

		ehr = parseInt(endstr.split(":")[0], 10);
		emi = parseInt(endstr.split(":")[1].split(" ")[0], 10);
		est = endstr.split(":")[1].split(" ")[1];
		newendstr = ((ehr<10) ? ("0"+ehr) : ehr) + ":" + ((emi<10) ? ("0"+emi) : emi) + " " + est;
		
		
		selrmids = (document.frmSettings2.MainLoc.value).split(", ")
		stradvrmallc = "";
		
		for (var i=0; i<selrmids.length-1; i++) {
			rmname = getselrmidnames(selrmids[i], ifrmLocation.document.frmSettings2loc.locidname.value)
			if (rmname != "")
				stradvrmallc += selrmids[i] + ",," + rmname + ",," + startstr + ",," + newendstr + ";;";
		}
		
		document.frmSettings2.advAlocRoom.value = stradvrmallc;
		// alert(advAlocRoom=document.frmSettings2.advAlocRoom.value);
	}
	
	url = "advanceroomallocation.asp";
	winrtc = window.open(url, "RoomSplit", "width=750,height=450,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
	winrtc.focus();
}


function setAdvRoom(mod)
{
	locpg = "settings2loc.asp?f=frmSettings2";

	if (mod) {
		document.frmSettings2.AdvancedRoomAllocaiton.value = 1;
		
		selrids = ", ";
		advrms = (document.frmSettings2.advAlocRoom.value).split(";;");
		for (var i=0; i<advrms.length-1; i++) {
			advrm = advrms[i].split(",,");
			selrids += advrm[0] + ", ";
		}
		
		ifrmLocation.document.frmSettings2loc.selectedloc.value = selrids;
			
		if (document.frmSettings2.roomListDisplayMod[0].checked)
			chgRoomDisplay( locpg, 1, true);
		else
			chgRoomDisplay( locpg, 2, true);
	} else {
		document.frmSettings2.AdvancedRoomAllocaiton.value = 0;
		document.frmSettings2.advAlocRoom.value = "";
		chgRoomDisplay( locpg, parseInt(getFieldValue ("roomListDisplayMod"), 10), true); // ZD 101722
	}
		
		
	showAdvRmAllc();
}
	
	
function showAdvRmAllc()
{
	if (document.getElementById ("rmsplitDIV")) {
		document.getElementById ("rmsplitDIV").innerHTML = parseInt(document.frmSettings2.AdvancedRoomAllocaiton.value, 10) ? "&nbsp;ON&nbsp;" : "&nbsp;OFF&nbsp;"; // ZD 101722
		document.getElementById ("rmsplitDIV").style.color = parseInt(document.frmSettings2.AdvancedRoomAllocaiton.value, 10) ? "green" : "red"; // ZD 101722
	}
}
	

function delMember(pi_str)
{
	new_pi_str = "";

	pi_ary1 = pi_str.split("||");//FB 1888
	//pi_ary1 = pi_str.split(";");
	for (var i=0; i < pi_ary1.length - 1; i++) {
		pi_ary2 = pi_ary1[i].split("!!");//FB 1888
		//pi_ary2 = pi_ary1[i].split(",");
		if (pi_ary2[12]!="3") {
			new_pi_str += pi_ary1[i] + "||";//FB 1888
			//new_pi_str += pi_ary1[i] + ";";
		}
	}
	return (new_pi_str);
}


function getYourOwnEmailList()
{
   
    var url = ifrmPartylist.location + "";
	if(url.indexOf('settings2party.asp') != -1){
		willContinue = ifrmPartylist.bfrRefresh(); 
		if (willContinue) {
			ifrmPartylist.location.reload(); 
			//Code Added by Offshore FB issue no:412-Start
			//url = "dispatcher/conferencedispatcher.asp?frm=party2&cmd=GetEmailList&emailListPage=1&wintype=pop";		  
			if(document.location.href.indexOf('managetemplate2.aspx') != -1)
			 {
//			    url = "dispatcher/conferencedispatcher.asp?frm=party2Aspx&cmd=GetEmailList&emailListPage=1&wintype=pop"; //Login Management
			    if(queryField("sb") > 0 )//Login Management
                url = "emaillist2.aspx?t=e&frm=party2Aspx&wintype=ifr&fn=&n=";
                else
                url = "emaillist2main.aspx?t=e&frm=party2Aspx&fn=&n=";
			 }
			 else
			 {
//			    url = "dispatcher/conferencedispatcher.asp?frm=party2NET&cmd=GetEmailList&emailListPage=1&wintype=pop";//Login Management
			    if(queryField("sb") > 0 )//Login Management
                url = "emaillist2.aspx?t=e&frm=party2NET&wintype=ifr&fn=&n=";
                else
                url = "emaillist2main.aspx?t=e&frm=party2NET&fn=&n=";
             }
		    //Code Added by Offshore FB issue no:412-End
			
			if (!window.winrtc) {	// has not yet been defined
			    winrtc = window.open(url, "", "width=920,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no"); //FB 2735
				winrtc.focus();
			} else // has been defined
			    if (!winrtc.closed) {     // still open
			    	winrtc.close();
			    	winrtc = window.open(url, "", "width=920,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no"); //FB 2735
					winrtc.focus();
				} else {
				winrtc = window.open(url, "", "width=920,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no"); //FB 2735
			        winrtc.focus();
				}
		}
	}
}

function getYourOwnEmailListNET()
{

	var url = ifrmPartylist.location + "";
	//Code Changed by Offshore FB issue no:412-Start for managetemplate2 Conversion
	    //if(url.indexOf('settings2partyNET.asp') != -1){
	if((url.indexOf('settings2party.aspx') != -1)||(url.indexOf('settings2partyNET.aspx') != -1)||(url.indexOf('Settings2PartyNETExpress.aspx') != -1))//Changed for settings2partynet.aspx
	{
		 //Code Changed by Offshore FB issue no:412-End
		willContinue = ifrmPartylist.bfrRefresh(); 
		if (willContinue) {
			ifrmPartylist.location.reload(); 
			//url = "dispatcher/conferencedispatcher.asp?frm=party2NET&cmd=GetEmailList&emailListPage=1&wintype=pop";
			//Code Added by Offshore FB issue no:412-Start
			//FB 1779
			if (document.frmSettings2.hdnAudioInsID != null) //FB 1779
			 var audioid = document.frmSettings2.hdnAudioInsID.value;
			
			if(document.location.href.indexOf('managetemplate2.aspx') != -1)
			{
//			    url = "dispatcher/conferencedispatcher.asp?frm=party2Aspx&cmd=GetEmailList&emailListPage=1&wintype=pop"; //Login Mangement
                if(queryField("sb") > 0 )
                url = "emaillist2.aspx?t=e&frm=party2Aspx&wintype=ifr&fn=&n=";
                else
                    url = "emaillist2main.aspx?t=e&frm=party2Aspx&fn=&n=&audioid="; //FB 1779
            }
			 else
			 {
//			    url = "dispatcher/conferencedispatcher.asp?frm=party2NET&cmd=GetEmailList&emailListPage=1&wintype=pop";
			    if(queryField("sb") > 0)
                url = "emaillist2.aspx?t=e&frm=party2NET&wintype=ifr&fn=&n=";
                else
                url = "emaillist2main.aspx?t=e&frm=party2NET&fn=&n=&audioid="+ audioid; //FB 1779
			 }
		 //Code Added by Offshore FB issue no:412-End
			if (!window.winrtc) {	// has not yet been defined
			    winrtc = window.open(url, "", "width=920,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no"); //FB 2735
				winrtc.focus();
			} else // has been defined
			    if (!winrtc.closed) {     // still open
			    	winrtc.close();
			    	winrtc = window.open(url, "", "width=920,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no"); //FB 2735
					winrtc.focus();
				} else {
				winrtc = window.open(url, "", "width=920,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no"); //FB 2735
			        winrtc.focus();
				}
		}
	}
}


function chkdelBlankLine(str)
{
	newstr = ""
	strary = str.split("||");//FB 1888
	for (var i=0; i < strary.length-1; i++) {
		sary = strary[i].split("!!");//FB 1888
		if (sary[3]!="") {
			newstr += strary[i] + "||";//FB 1888
		}
	}
	return newstr;
}

function chkdelBlankLinePNG(str, j)
{
	newstr = ""
	strary = str.split(";");
	for (var i=0; i < strary.length-1; i++) {
		sary = strary[i].split(",");
		if (sary[j]!="") {
			newstr += strary[i] + ";";
		}
	}
	return newstr;
}

function chkHost()
{
	partysary = (document.frmSettings2.PartysInfo.value).split(";");
	for (var i=0; i < partysary.length-1; i++) {
		partyary = partysary[i].split(",");
		if (partyary[3] == document.frmSettings2.hostemail.value)
			return true;
	}
	
	return false;
}



function haveInvitee(partys)
{
	partysary = partys.split("||");//FB 1888
	for (var i=0; i < partysary.length-1; i++) {
		partyary = partysary[i].split("!!");//FB 1888
		if ( (partyary[3]!="") && (partyary[5]=="1") )
			return true;
	}
	return (false);
}


function groupChg()
{
	var mod = 0;
	mod = ( (document.location.href).indexOf("settings2room") != -1 ) ? 1 : mod;
	mod = ( (document.location.href).indexOf("settings2p2p") != -1 ) ? 1 : mod;


	var url = ifrmPartylist.location + "";
	if(url.indexOf('settings2party.asp') != -1){
		new_pi = "";
		willContinue = ifrmPartylist.bfrRefresh(); 
		if (willContinue) {
			partysinfo = document.frmSettings2.PartysInfo.value
			tmp_pi = delMember (partysinfo);
	
			usersinfo = document.frmSettings2.UsersStr.value;
			//usersary = usersinfo.split("|");//FB 1888
			usersary = usersinfo.split("``");//FB 1888
	
			pi_ary1 = tmp_pi.split("||");//FB 1888
			k = 0; pi_ary2 = pi_ary1[k].split("!!");
			while ( (pi_ary2[10]=="0") && (k < pi_ary1.length-1) ) {
				new_pi += pi_ary1[k] + "||";
				k++;
				pi_ary2 = pi_ary1[k].split("!!");
			}
	
			for (var i=0; i < usersary.length; i++) {
				if (document.frmSettings2.Group.options[i].selected) {
					partysary = usersary[i].split("||");
					for (var j=0; j < partysary.length-1; j++) {
						partyary = partysary[j].split("!!");
						if ( (tmp_pi.indexOf("!!" + partyary[3] + "!!") == -1) && (new_pi.indexOf("!!" + partyary[3] + "!!") == -1) ) {
							new_pi += partyary[0] + "!!" + partyary[1] + "!!" + partyary[2] + "!!" + partyary[3];
							new_pi += ( (mod) ? "!!0!!1!!0" : "!!1!!0!!0" ) + "!!0!!0!!1!!!!!!3!!!!!!||";//Fb 1888
							new_pi += ( (mod) ? ",0,1,0" : ",1,0,0" ) + ",0,0,1,,,3,,,;";
						}
					}
				}
			}
	
			while (k < pi_ary1.length-1) {
				new_pi += pi_ary1[k] + "||";//FB 1888
				k++;
				pi_ary2 = pi_ary1[k].split("!!");//FB 1888
			}
	
			document.frmSettings2.PartysInfo.value = new_pi;
			ifrmPartylist.location.reload(); 
		}
	}
}
function groupChgNET()
{
	var url = ifrmPartylist.location + "";

		new_pi = "";
		willContinue = ifrmPartylist.bfrRefresh(); 
		if (willContinue) {
			partysinfo = document.getElementById("txtPartysInfo").value;
			tmp_pi = delMember (partysinfo);
	
			usersinfo = document.getElementById("txtUsersStr").value;
			usersary = usersinfo.split("``");//FB 1888
			//usersary = usersinfo.split("|");
	
			pi_ary1 = tmp_pi.split("||");//FB 1888
			//pi_ary1 = tmp_pi.split(";");
			k = 0; pi_ary2 = pi_ary1[k].split("!!");//FB 1888
			while ( (pi_ary2[10]=="0") && (k < pi_ary1.length-1) ) {
				new_pi += pi_ary1[k] + "||";//FB 1888
				k++;
				pi_ary2 = pi_ary1[k].split("!!");//FB 1888
			}
			var grp = document.getElementById("Group");
            
            for (var i=0; i < usersary.length; i++) {
				if (grp.options[i].selected) {
					partysary = usersary[i].split("||");//FB 1888
					for (var j=0; j < partysary.length-1; j++) {
						partyary = partysary[j].split("!!");//FB 1888
						if ( (tmp_pi.indexOf("!!" + partyary[3] + "!!") == -1) && (new_pi.indexOf("!!" + partyary[3] + "!!") == -1) ) {//FB 1888
							new_pi += partyary[0] + "!!" + partyary[1] + "!!" + partyary[2] + "!!" + partyary[3];//FB 1888
							new_pi += "!!0!!1!!0!!1!!0!!1!!!!!!3!!!!!!!!0!!0!!0!!0!!1!!0||"; //FB 1888 //FB 2550 //ALLDEV-814
							//new_pi += ",0,1,0,1,0,1,,,3,,,;";
						}
					}
				}
			}
	
			while (k < pi_ary1.length-1) {
				new_pi += pi_ary1[k] + "||";
				k++;
				pi_ary2 = pi_ary1[k].split("!!");
			}
	
			document.getElementById("txtPartysInfo").value = new_pi;
			ifrmPartylist.location.reload(); 
		}
}


function genOutlookDailyAppointment(isImm)
{
	durtime = getConfDur (document.frmSettings2);
	if ( durtime < 15 ) {
		alert(EN_31);
				
		if (document.frmSettings2.ConferenceDurationhr)
			document.frmSettings2.ConferenceDurationhr.focus();
		if (document.frmSettings2.ConferenceEndDate)
			document.frmSettings2.ConferenceEndDate.focus();
					
		return (false);		
	}
	
	getOutlookDailyAppointment(isImm, durtime);

}

function showDataLoading()
{
	document.getElementById("dataLoading").style.display = "";
}

function hideDataLoading()
{
	document.getElementById("dataLoading").style.display = "";
}