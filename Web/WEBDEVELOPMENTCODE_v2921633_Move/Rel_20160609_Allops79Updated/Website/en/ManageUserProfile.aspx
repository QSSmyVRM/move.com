<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.	
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 ZD 100866 End--%>
<%@ Page Language="C#" Inherits="ns_MyVRM.UserProfile" Buffer="true" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><%--FB 2779--%>
<meta http-equiv="X-UA-Compatible" content="IE=8">
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<%--ZD 101388 Commented--%>
<%--<!-- FB 2719 Starts -->
<% if (Session["isExpressUser"].ToString() == "1"){%>
<!-- #INCLUDE FILE="inc/maintopNETExp.aspx" -->
<%}else{%>
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<%}%>
<!-- FB 2719 Ends -->--%>
<%--FB 2481 start--%>
<%@ Register Assembly="DevExpress.SpellChecker.v10.2.Core, Version=10.2.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraSpellChecker" TagPrefix="dxXSC" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v10.2, Version=10.2.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dxSC" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v10.2, Version=10.2.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dxHE" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v10.2, Version=10.2.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxE" %>
<%@ Register Assembly="DevExpress.Web.v10.2, Version=10.2.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dxP" %>
<%@ Register Assembly="DevExpress.Web.v10.2, Version=10.2.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dxRP" %>
    <%@ Register Assembly="DevExpress.Web.v10.2, Version=10.2.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGlobalEvents" TagPrefix="dx" %>
<%--FB 2481 end--%>
<script type="text/javascript">
  var servertoday = new Date();
</script>

<script type="text/javascript" src="inc/functions.js"></script>
<%--FB 1861--%>
<%--<script type="text/javascript" src="script/cal.js"></script>--%>
<script type="text/javascript" src="script/cal-flat.js"></script>
<script type="text/javascript" src="../<%=Session["language"] %>/lang/calendar-en.js"></script>
<script type="text/javascript" src="script/calendar-setup.js"></script>
<script type="text/javascript" src="script/calendar-flat-setup.js"></script>

<script type="text/javascript" src="script/mytreeNET.js"></script>
<script language="javascript" src="../<%=Session["language"] %>/Organizations/Original/Javascript/RGBColorPalette.js"> </script>
<script type="text/javascript" src="script/RoomSearch.js"></script>
<link rel="stylesheet" type="text/css" media="all" href="css/calendar-blue.css" /> <%--FB 1861--%> <%--FB 1982--%>
 <%--ZD 100664 Start--%>
    <script src="script/CallMonitorJquery/jquery.1.4.2.js" type="text/javascript"></script> 
    <script type="text/javascript">
     $(document).ready(function() {
           $('.treeNode').click(function() {
            var target = $(this).attr('tag');
            if ($('#' + target).is(":visible")) {
                $('#' + target).hide("slow");
                $(this).attr('src', 'image/loc/nolines_plus.gif');
            } else {
                $('#' + target).show("slow");
                
                $(this).attr('src', 'image/loc/nolines_minus.gif');
            }

        });
        });
    </script>
    <%--ZD 100664 End--%>
<style type="text/css">
#lstDepartment option /* FB 2611 */
{
	color:Black;
}
a img { outline:none;
text-decoration:none;
border:0;
}
</style>

<script runat="server">

</script>
<script language="javascript">
  //ZD 100604 start
  var img = new Image();
  img.src = "../en/image/wait1.gif";
  //ZD 100604 End

function DisplayWarning(obj)
{
    //ZD 100263
    //if ("<%=Session["userID"] %>" == document.getElementById("txtUserID").value)
    if ("<%=Session["userID"] %>" == "<%=Session["ModifiedUserID"] %>")
        if (obj.value != "-1")
        {
            alert(UserLobbyDisplay);
        }
}
function CheckDate(obj)
{
    //alert("in CheckDate" + Date.parse(obj.value) + " : " + Date.parse(new Date()) );
    if (Date.parse(obj.value) < Date.parse(new Date()))
    {
        alert(InvalidDate);
        //obj.focus(); FOGBUGZ CASE 100 - case was misexplained it is not for conferencesetup.
    }   
}

function CheckSecondaryEmail()
{
    var objL = document.getElementById("lstSendBoth");
    var objT = document.getElementById("txtUserEmail2");
    var objemail = document.getElementById("txtUserEmail");//FB 289
    if ( (objL.value == "1") && (objT.value == "") )
        {
            alert(UserValidEmail);
            objL.value = "0";
            return false;
        }
/* FB 289 Starts */        
    if((objT.value != "") && (objemail.value != ""))
    {
        if(objT.value.toUpperCase() == objemail.value.toUpperCase())
        {
            alert(UserValidSecEmail);
            objT.focus();
            return false;
        } 
    }
 /* FB 289 Ends */
}
//FB 2339 Start

function validatePassword (pw, options) 
{
	// default options (allows any password)
	var o = {
		lower:    0,
		upper:    0,
		alpha:    0, /* lower + upper */
		numeric:  0,
		special:  0,
		length:   [0, Infinity],
		custom:   [ /* regexes and/or functions */ ],
		badWords: [],
		badSequenceLength: 0,
		noQwertySequences: false,
		noSequential:      false
	};

	for (var property in options)
		o[property] = options[property];

	var	re = {
			lower:   /[a-z]/g,
			upper:   /[A-Z]/g,
			alpha:   /[A-Z]/gi,
			numeric: /[0-9]/g,
			special: /[\W_]/g
		},
		rule, i;

	// enforce min/max length
	if (pw.length < o.length[0] || pw.length > o.length[1])
		return false;

	// enforce lower/upper/alpha/numeric/special rules
	for (rule in re) {
		if ((pw.match(re[rule]) || []).length < o[rule])
			return false;
	}

	// enforce word ban (case insensitive)
	for (i = 0; i < o.badWords.length; i++) {
		if (pw.toLowerCase().indexOf(o.badWords[i].toLowerCase()) > -1)
			return false;
	}

	// enforce the no sequential, identical characters rule
	if (o.noSequential && /([\S\s])\1/.test(pw))
		return false;

	// enforce alphanumeric/qwerty sequence ban rules
	if (o.badSequenceLength) {
		var	lower   = "abcdefghijklmnopqrstuvwxyz",
			upper   = lower.toUpperCase(),
			numbers = "0123456789",
			qwerty  = "qwertyuiopasdfghjklzxcvbnm",
			start   = o.badSequenceLength - 1,
			seq     = "_" + pw.slice(0, start);
		for (i = start; i < pw.length; i++) {
			seq = seq.slice(1) + pw.charAt(i);
			if (
				lower.indexOf(seq)   > -1 ||
				upper.indexOf(seq)   > -1 ||
				numbers.indexOf(seq) > -1 ||
				(o.noQwertySequences && qwerty.indexOf(seq) > -1)
			) {
				return false;
			}
		}
	}

	// enforce custom regex/function rules
	for (i = 0; i < o.custom.length; i++) {
		rule = o.custom[i];
		if (rule instanceof RegExp) {
			if (!rule.test(pw))
				return false;
		} else if (rule instanceof Function) {
			if (!rule(pw))
				return false;
		}
	}
	return true;
}

function PreservePassword()
{
        document.getElementById("txtPassword1_1").value = document.getElementById("txtPassword1").value;
        document.getElementById("txtPassword1_2").value = document.getElementById("txtPassword2").value;
        var password = document.getElementById("txtPassword1").value;
        if("<%=Session["EnablePasswordRule"]%>" == "1")
        {
            var passed = validatePassword(password, {
	                length:   [6, Infinity],
	                lower:    1,
	                upper:    1,
	                numeric:  1,
	                special:  0,
	                badWords: [],
	                badSequenceLength: 0
                    });
            if(passed == false)
            {
                document.getElementById("cmpValPassword1").style.visibility = 'visible';
            }
            else
            {
                document.getElementById("cmpValPassword1").style.visibility = 'hidden';
            }
        }
}
function RulePassword()
{
        if(document.getElementById("reqPassword1").style.display == "inline" || document.getElementById("cmpValPassword1").style.display == "inline")
        {
            window.scrollTo(0,0);
            return false;
        }
        // ZD 100263
        if(document.getElementById("txtPassword1").style.backgroundImage == "" && document.getElementById("txtPassword1").value == "")
        {
            ValidatorEnable(document.getElementById('reqPassword1'), true);
            window.scrollTo(0,0);
            return false;
        }

        //ALLBUGS-90 - Start
        if(document.getElementById("regtxtExtvideonum").style.display != "none" || document.getElementById("regtxtIntvideonum").style.display != "none")
        {
            return false;
        }
        //ALLBUGS-90 - End

        var password = document.getElementById("txtPassword1").value;
        if("<%=Session["EnablePasswordRule"]%>" == "1")
        {
            var passed = validatePassword(password, {
	                length:   [6, Infinity],
	                lower:    1,
	                upper:    1,
	                numeric:  1,
	                special:  0,
	                badWords: [],
	                badSequenceLength: 0
                    });
            if(passed == false)
            {
               document.getElementById("cmpValPassword1").innerHTML = RSWeakPassword;  
                document.getElementById("txtPassword1").focus();
                document.getElementById("cmpValPassword1").style.display = "block";
                return false;          
            }
        }
        IsNumeric();
        //ALLDEV-814
        var txtConfCode = document.getElementById("txtConfCode");
        if (txtConfCode.value.search(/^[^<>&+]*$/)==-1) 
        {        
            RegtxtConfCode.style.display = 'block';
            txtConfCode.focus();
            return false;
        }    
        var txtLeaderPin = document.getElementById("txtLeaderPin");
        if (txtLeaderPin.value.search(/^[^<>&+]*$/)==-1) 
        {        
            RegtxtLeaderPin.style.display = 'block';
            txtLeaderPin.focus();
            return false;
        } 
        var txtPartyCode = document.getElementById("txtPartyCode");
        if (txtPartyCode.value.search(/^[^<>&+]*$/)==-1) 
        {        
            RegtxtPartyCode.style.display = 'block';
            txtPartyCode.focus();
            return false;
        } 
       DataLoading(1); // ZD 100176   
        
}
//FB 2339 End

function SavePassword()
{
        document.getElementById("txtPassword1").value = document.getElementById("txtPassword1_1").value;
        document.getElementById("txtPassword2").value = document.getElementById("txtPassword1_2").value;
}

function GetLocations()
{
    url = "LocationList.aspx?roomID=" + document.getElementById("hdnLocation").value;
	if (!window.winrtc) {	// has not yet been defined
		winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
		winrtc.focus();
	} else // has been defined
	    if (!winrtc.closed) {     // still open
	    	winrtc.close();
	        winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
			winrtc.focus();
		} else {
	        winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
	        winrtc.focus();
		}

}
//Code added for Ticker -Start

function fnShowFeed()
 {
    var tickerDisplay = document.getElementById("drpTickerDisplay");
    var tickerDisplay1 = document.getElementById("drpTickerDisplay1");
    
   
    if(tickerDisplay1.selectedIndex == "1")
    {
         document.getElementById("feedLink1").style.display = "Block";
         document.getElementById("txtFeedLink1").style.display = "Block";
    }
    else
    {
       document.getElementById("feedLink1").style.display = "None";
       document.getElementById("txtFeedLink1").style.display = "None";
    }
    
    if(tickerDisplay.selectedIndex == "1")
    {
         document.getElementById("feedLink").style.display = "Block";
         document.getElementById("txtFeedLink").style.display = "Block";
    }
    else
    {
       document.getElementById("feedLink").style.display = "None";
       document.getElementById("txtFeedLink").style.display = "None";
    }
 }
//Code changed for Ticker End
//API Port Starts...
function IsNumeric()

{
   
   var ValidChars = "0123456789";
   var IsNumber=true;
   var Char;
   var error = document.getElementById('lblapierror');
   var sTexttemp = document.getElementById("txtApiportno").value;
   for (i = 0; i < sTexttemp.length && IsNumber == true; i++) 
      { 
      Char = sTexttemp.charAt(i); 
      if (ValidChars.indexOf(Char) == -1) 
         {
         IsNumber = false;
         }
      }
   if(IsNumber == false)
    {
        error.style.display = 'block';
        error.innerHTML = "<asp:Literal Text='<%$ Resources:WebResources, NumericValuesOnly%>' runat='server'></asp:Literal>";
        
    }
   if(IsNumber == true)
    {
        error.style.display = 'none';
        error.innerHTML = "";
        
    } 
   return IsNumber;
   
   }
//API Port Ends...
//FB 2594 Starts
    function isNumberKey(evt)
      {
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

         return true;
      }
      //FB 2594 Ends

//FB 2565 
function fnCancel()
{
    window.location.replace('ManageUser.aspx?t=1');
    
}

// FB 2693 Starts

function fnShowPcConf(stat)
{
    if(stat == true)
    {
        if(document.getElementById("tblPcConf") != null)
            document.getElementById("tblPcConf").style.display = "block";
    }
    else
    {
        if(document.getElementById("tblPcConf") != null)
            document.getElementById("tblPcConf").style.display = "none";
    }
    
    if(document.getElementById("tblBlueJeans") != null)
        document.getElementById("tblBlueJeans").style.display = "none";
    if(document.getElementById("tblVidtel") != null)
        document.getElementById("tblVidtel").style.display = "none";
    if(document.getElementById("tblJabber") != null)
        document.getElementById("tblJabber").style.display = "none";
    if(document.getElementById("tblLync") != null)
        document.getElementById("tblLync").style.display = "none";
    
    //if(document.getElementById("expBlueJeans") != null)
        //document.getElementById("expBlueJeans").src = "../image/loc/nolines_plus.gif";
    if(document.getElementById("expVidtel") != null)
        document.getElementById("expVidtel").src = "../image/loc/nolines_plus.gif";
    if(document.getElementById("expJabber") != null)
        document.getElementById("expJabber").src = "../image/loc/nolines_plus.gif";
    if(document.getElementById("expLync") != null)
        document.getElementById("expLync").src = "../image/loc/nolines_plus.gif";
}

function fnShowPcConfOpt(par1, par2)
{
    if(document.getElementById("tblBlueJeans") != null)
        document.getElementById("tblBlueJeans").style.display = "none";
    if(document.getElementById("tblVidtel") != null)
        document.getElementById("tblVidtel").style.display = "none";
    if(document.getElementById("tblJabber") != null)
        document.getElementById("tblJabber").style.display = "none";
    if(document.getElementById("tblLync") != null)
        document.getElementById("tblLync").style.display = "none";
    
    
    if(par1.src.indexOf('plus') > -1)
    {
        document.getElementById(par2).style.display = "block";
        par1.src = "../image/loc/nolines_minus.gif";
    }
    else if(par1.src.indexOf('minus') > -1)
    {
        document.getElementById(par2).style.display = "none";
        par1.src = "../image/loc/nolines_plus.gif";
    }
    
    //if(document.getElementById("tblBlueJeans") != null && document.getElementById("tblBlueJeans").style.display == "none")
        //document.getElementById("expBlueJeans").src = "../image/loc/nolines_plus.gif";
    if(document.getElementById("tblVidtel") != null && document.getElementById("tblVidtel").style.display == "none")
        document.getElementById("expVidtel").src = "../image/loc/nolines_plus.gif";
    if(document.getElementById("tblJabber") != null && document.getElementById("tblJabber").style.display == "none")
        document.getElementById("expJabber").src = "../image/loc/nolines_plus.gif";
    if(document.getElementById("tblLync") != null && document.getElementById("tblLync").style.display == "none")
        document.getElementById("expLync").src = "../image/loc/nolines_plus.gif";
}
function toggle(par1, par2) 
    {
	    var ele = document.getElementById("par1");
	    var text = document.getElementById("par2");
	    if(ele.style.display == "block")
	    {
    		ele.style.display = "none";
		    text.innerHTML = "More";
  	    }
	    else 
	    {
		    ele.style.display = "block";
		    text.innerHTML = "Less";
	    }
    } 

// FB 2693 Ends
//FB 3054 Starts
function PasswordChange(par) 
{  
    //ZD 103550
    if (par == 1 || par == 2)
    {
        if(document.getElementById('reqPassword1') != null) // ZD 100263
        {
            ValidatorEnable(document.getElementById('reqPassword1'), true);
        }
    
        document.getElementById("hdnPasschange").value = true;
    
        if (par == 1)
            document.getElementById("hdnPW1Visit").value = true;
        else if(par == 2)
            document.getElementById("hdnPW2Visit").value = true;
    }
} 
function EPPasswordChange(par) 
{  

    document.getElementById("hdnEPPWD").value = true;
    
    if (par == 1)
        document.getElementById("hdnEP1Visit").value = true;
    else
        document.getElementById("hdnEP2Visit").value = true;
} 


//ZD 100221 Starts
function WebexPasswordChange(par) 
{  
    document.getElementById("hdnWebEX").value = true;
}   
//ZD 100221 Ends
//ZD 104116 Starts
function FillBJNUser()
{
    var email = document.getElementById("txtUserEmail");
    var EnableBJN = document.getElementById("ChkBJNConf");
    var BJNuser = document.getElementById("txtBJNUserName");

    if(EnableBJN != null)
        if(EnableBJN.checked)
            BJNuser.value = email.value;
}
//ZD 104116 Ends

 function fnTextFocus(xid,par) {
     
     // ZD 100263 Starts
     var obj1 = document.getElementById("txtPassword1");
     var obj2 = document.getElementById("txtPassword2");
     
     if(document.getElementById("hdnPasschange").value == "false")
     {
         if(obj1.value == "" && obj2.value == "")
         {
             document.getElementById("txtPassword1").style.backgroundImage="";
             document.getElementById("txtPassword2").style.backgroundImage="";
             document.getElementById("txtPassword1").value="";
             document.getElementById("txtPassword2").value="";
         }
     }
     return false;
     // ZD 100263 Ends
     
      var obj = document.getElementById(xid);
          
    if (par == 1) {
        if(document.getElementById("hdnPW2Visit") != null)
        {
            if(document.getElementById("hdnPW2Visit").value == "false")
            { 
                document.getElementById("txtPassword1").value = "";
                document.getElementById("txtPassword1_1").value="";
                document.getElementById("txtPassword2").value = "";
                document.getElementById("txtPassword1_2").value="";
            }else
            {
                document.getElementById("txtPassword1").value = "";
                document.getElementById("txtPassword1_1").value="";
            }
        }
        else
        {
            document.getElementById("txtPassword1").value = "";
            document.getElementById("txtPassword1_1").value="";
            document.getElementById("txtPassword2").value = "";
            document.getElementById("txtPassword1_2").value="";
        }
    }
       else{
           if(document.getElementById("hdnPW1Visit") != null)
           {
            if(document.getElementById("hdnPW1Visit").value == "false")
            { 
                document.getElementById("txtPassword1").value = "";
                document.getElementById("txtPassword1_1").value="";
                document.getElementById("txtPassword2").value = "";
                document.getElementById("txtPassword1_2").value="";
            }
            else{
                document.getElementById("txtPassword2").value = "";
                document.getElementById("txtPassword1_2").value="";
                }
           
           }
           else{
                document.getElementById("txtPassword2").value = "";
                document.getElementById("txtPassword1_2").value="";
                }
        }
        
        if(document.getElementById("reqPassword1")!= null)
        {
            ValidatorEnable(document.getElementById('reqPassword1'), false);
            ValidatorEnable(document.getElementById('reqPassword1'), true);
        }
         if(document.getElementById("cmpValPassword1")!= null)
        {
            ValidatorEnable(document.getElementById('cmpValPassword1'), false);
            ValidatorEnable(document.getElementById('cmpValPassword1'), true);
        }
         if(document.getElementById("reqPassword2")!= null)
        {
            ValidatorEnable(document.getElementById('reqPassword2'), false);
            ValidatorEnable(document.getElementById('reqPassword2'), true);
        }
         if(document.getElementById("cmpValPassword2")!= null)
        {
            ValidatorEnable(document.getElementById('cmpValPassword2'), false);
            ValidatorEnable(document.getElementById('cmpValPassword2'), true);
        }
}
 function fnEPTextFocus(xid,par) {
  // ZD 100263 Starts
     var obj1 = document.getElementById("txtEPPassword1");
     var obj2 = document.getElementById("txtEPPassword2");
     if(obj1.value == "" && obj2.value == "")
     {
         document.getElementById("txtEPPassword1").style.backgroundImage="";
         document.getElementById("txtEPPassword2").style.backgroundImage="";
         document.getElementById("txtEPPassword1").value="";
         document.getElementById("txtEPPassword2").value="";
     }
     return false;
     // ZD 100263 Ends
      var obj = document.getElementById(xid);
          
    if (par == 1) {
        if(document.getElementById("hdnEP2Visit") != null)
        {
            if(document.getElementById("hdnEP2Visit").value == "false")
            { 
                document.getElementById("txtEPPassword1").value = "";
                document.getElementById("txtEPPassword2").value="";
            }else
            {
                document.getElementById("txtPassword1").value = "";
            }
        }
        else
        {
             document.getElementById("txtEPPassword1").value = "";
                document.getElementById("txtEPPassword2").value="";
        }
    }
       else{
           if(document.getElementById("hdnEP1Visit") != null)
           {
            if(document.getElementById("hdnEP1Visit").value == "false")
            { 
                document.getElementById("txtEPPassword2").value = "";
                document.getElementById("txtEPPassword1").value="";
            }
            else{
                document.getElementById("txtEPPassword2").value = "";
                }
           
           }
           else{
                document.getElementById("txtEPPassword2").value = "";
                document.getElementById("txtEPPassword1").value="";
                }
        }
        
        if(document.getElementById("RegularExpressionValidator9")!= null)
        {
            ValidatorEnable(document.getElementById('RegularExpressionValidator9'), false);
            ValidatorEnable(document.getElementById('RegularExpressionValidator9'), true);
        }
         if(document.getElementById("CmpEP1")!= null)
        {
            ValidatorEnable(document.getElementById('CmpEP1'), false);
            ValidatorEnable(document.getElementById('CmpEP1'), true);
        }
         if(document.getElementById("CmpEP2")!= null)
        {
            ValidatorEnable(document.getElementById('CmpEP2'), false);
            ValidatorEnable(document.getElementById('CmpEP2'), true);
        }
}
//FB 3054 Ends
//ZD 100221 Start
function fnShowWebExConf(stat)
{
    if(stat == true)
    {
        document.getElementById("tblWebExConf").style.display = "";
        if(document.getElementById("txtWebExUserName") != null) //ZD 100381
            if(document.getElementById("txtWebExUserName").value == "")
                document.getElementById("txtWebExPassword").style.backgroundImage = "";
    }
    else
        document.getElementById("tblWebExConf").style.display = "none";
}
//ZD 100221 End
//ZD 103933 Start
function fnShowBJNConf(stat)
{
    if(stat == true)
        document.getElementById("tblBJNConf").style.display = "";
    else
        document.getElementById("tblBJNConf").style.display = "none";
}
//ZD 103933 End
</script>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>User Profile</title>
    <script type="text/javascript" src="inc/functions.js"></script>
    <script src="script/CallMonitorJquery/jquery.1.4.2.js" type="text/javascript"></script>
</head>
<body>
<script type="text/javascript">
    $(document).ready(function() {
        checkStaticID();
        $('#chkEnblStaticId').change(function(e) {
            checkStaticID();
        });
    });

    function checkStaticID() {
        if ($('#chkEnblStaticId').attr('checked') == true) {
            $('#trStaticBox').show();
        }
        else {
            $('#trStaticBox').hide();
        }
    }
</script>
    <form id="frmUserProfile" autocomplete="off" runat="server" method="post" onsubmit="return true"><%--ZD 101190--%>
        <%--ZD 101022 start--%>
        <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" >
		    <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		    </Scripts>
	    </asp:ScriptManager> <%--ZD 101022 End--%>
    <input type="hidden" value="<%=Session["ThemeType"]%>" id="sessionThemeType" /> <%--FB 2815--%>
    <div id="hideScreen" style="position:absolute; background-color:white; top:0; left:0; width:1500px; height:2000px; z-index:980; display:block"></div><%--FB 2491 FB 2811--%>
        
    <input id="txtPassword1_1" runat="server" type="hidden" />
    <input id="txtPassword1_2" runat="server" type="hidden" />
    <input name="selectedloc" type="hidden" id="selectedloc" runat="server"  /> <!--Added room search-->
     <input name="locstrname" type="hidden" id="locstrname" runat="server"  /> <!--Added room search-->
    <input id="txtLevel" runat="server" type="hidden" />
    <%--Code changed for FB 1425 QA Bug -Start--%>
      <input type="hidden" id="hdntzone" runat="server"/>
      <%--Code changed for FB 1425 QA Bug -End--%>
      <input type="hidden" name="hdnAVParamState" id="hdnAVParamState" runat="server" /> <%--FB 1985--%>
      <input type="hidden" name="hdnHelpReq" id="hdnHelpReq" runat="server" /> <%--FB 2268--%>
       <input type="hidden" id="hdnPasschange" value="false" runat="server"/> <%--FB 3054--%>
       <input type="hidden" id="hdnPW1Visit" value="false" runat="server"/> <%--FB 3054--%>
       <input type="hidden" id="hdnPW2Visit" value="false" runat="server"/> <%--FB 3054--%>
       <input type="hidden" id="hdnEPPWD" value="false" runat="server"/> <%--FB 3054--%>
       <input type="hidden" id="hdnEP1Visit" value="false" runat="server"/> <%--FB 3054--%>
       <input type="hidden" id="hdnEP2Visit" value="false" runat="server"/> <%--FB 3054--%>
       <input type="hidden" id="hdnWebEX" value="false" runat="server"/> <%--ZD 100221--%>
    <div>
      <input type="hidden" id="helpPage" value="65" /><%-- ZD 100263 --%>
      <%--<asp:TextBox ID="txtUserID" style="width:0" Height="0" runat="server" BorderStyle="none" BorderWidth="0"></asp:TextBox>--%>

        <table style="width:96%;" align="center" border="0"><%--FB 2611--%> <%--ZD 100806--%>
            <tr>
                <td align="center">
                    <h3>
                        <asp:Label ID="lblHeader" runat="server"></asp:Label>
                    </h3>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
                    <asp:CustomValidator runat="server" Display="dynamic" ID="cusVal1" OnServerValidate="ValidateIPAddress" CssClass="lblError"></asp:CustomValidator>
                </td>
            </tr>
            <tr><div id="dataLoadingDIV" style="display:none" align="center" >
                <img border='0' src='image/wait1.gif'  alt='Loading..' />
            </div> <%--ZD 100678 End--%>
            </tr><%--ZD 100176--%>

            <%--ZD 100664 - Start --%>
            <tr id="trHistory" runat="server">
                <td>
                    <table width="100%" border="0">
                        <tr>
                            <td valign="top" align="left" style="width: 2px">
                            <span class="blackblodtext" style="vertical-align:top"><asp:Literal ID="Literal4" Text="<%$ Resources:WebResources, History%>" runat="server"></asp:Literal></span>
                            <a href="#" onmouseup="if(event.keyCode == 13){this.childNodes[0].click();return false;}" onkeydown="if(event.keyCode == 13){document.getElementById('imgHistory').click();return false;}">
                                        <img id="imgHistory"  src="image/loc/nolines_plus.gif" class="treeNode" style="border:none" alt="Expand/Collapse" tag="tblHistory" /></a>
                                </td>
                            </tr>

                        </table>
                </td>
             </tr>
            <tr>
                 <td> 
                      <div id="tblHistory" style="display:none; width:600px; float:left">
                        <table cellpadding="4" cellspacing="0" border="0" style="border-color:Gray; border-radius:15px; background-color:#ECE9D8"  width="75%" class="tableBody" align="center" >
                            <tr>
                                <td class="subtitleblueblodtext" align="center">
                                    <asp:Literal ID="Literal5" Text="<%$ Resources:WebResources, ExpressConference_Details%>" runat="server"></asp:Literal>
                                </td>            
                            </tr>
                            <tr align="center">
                                <td align="left">
                                    <asp:DataGrid ID="dgChangedHistory" runat="server" AutoGenerateColumns="False" CellPadding="4" GridLines="None" AllowSorting="true" 
                                        BorderStyle="solid" BorderWidth="0" ShowFooter="False"  
                                        Width="100%" Visible="true" style="border-collapse:separate" >
                                        <SelectedItemStyle  CssClass="tableBody"/>
                                        <AlternatingItemStyle CssClass="tableBody" />
                                        <ItemStyle CssClass="tableBody"  />                        
                                        <FooterStyle CssClass="tableBody"/>
                                        <HeaderStyle CssClass="tableHeader" />
                                        <Columns>
                                            <asp:BoundColumn DataField="ModifiedUserId"  HeaderStyle-Width="0%" Visible="false" ItemStyle-BackColor="#ECE9D8" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="ModifiedDateTime" HeaderText="<%$ Resources:WebResources, Date%>" ItemStyle-BackColor="#ECE9D8" ></asp:BoundColumn>
                                            <asp:BoundColumn DataField="ModifiedUserName" HeaderText="<%$ Resources:WebResources, User%>"  ItemStyle-BackColor="#ECE9D8" ></asp:BoundColumn>
                                            <asp:BoundColumn DataField="Description" HeaderText="<%$ Resources:WebResources, Description%>"  ItemStyle-BackColor="#ECE9D8" ></asp:BoundColumn>
                                        </Columns>
                                    </asp:DataGrid>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
            <%--ZD 100664 - End --%>
            
            <tr>
                <td align="Left">
                    <table cellspacing="3" style="width:100%; margin-left:-60px; position:relative"> <%--FB 2611--%>
                        <tr>
                            <td>&nbsp;</td>
                            <td> <%--ZD 103550--%>
                                <span style="margin-left:-2px;" class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_SelectPersonal%>" runat="server"></asp:Literal></span>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table style="width:100%" cellspacing="3" cellpadding="2" border="0"> <%--FB 2611--%>
                        <tr>
                            <td style="width:20%" align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_FirstName%>" runat="server"></asp:Literal><span class="reqfldstarText">*</span></td><%-- FB 1773 --%>
                            <td style="width:30%" align="left">
                            <table border="0" cellspacing="0" cellpadding="1" align="left"><tr><td valign="bottom" align="left"> <%--Edited for FF--%>
                                <asp:TextBox ID="txtUserFirstName" Enabled='<%# Application["ssoMode"].ToString().ToUpper().Equals("NO") %>' CssClass="altText" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reqFirstName" runat="server" ControlToValidate="txtUserFirstName" Display="dynamic" ErrorMessage="<%$ Resources:WebResources, Required%>"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator20" ControlToValidate="txtUserFirstName" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters16%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+?|!`\[\]{}\=@#$%&~]*$"></asp:RegularExpressionValidator> <%--FB 1888--%>
                                </td></tr>
                             </table><%-- FB 1773 --%>
                                <%--<tr><td valign="top" align="left">
                                <asp:TextBox ID="txtUserLastName" CssClass="altText" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reqLastName" runat="server" ControlToValidate="txtUserLastName" Display="dynamic" ErrorMessage="Required"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtUserLastName" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ; ? | = ! ` , [ ] { } : # $ @ ~ and &#34; are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=:@#$%&'~]*$"></asp:RegularExpressionValidator>
                                </td></tr>--%><%--Edited for FF--%>
                                </td>
                            <td style="width:20%" align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_LastName%>" runat="server"></asp:Literal><span class="reqfldstarText">*</span></td>
                            <td valign="top" align="left">
                                <asp:TextBox ID="txtUserLastName" CssClass="altText" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reqLastName" runat="server" ControlToValidate="txtUserLastName" Display="dynamic" ErrorMessage="<%$ Resources:WebResources, Required%>"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator7" ControlToValidate="txtUserLastName" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters16%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+?|!`\[\]{}\=@#$%&~]*$"></asp:RegularExpressionValidator><%--FB 1888--%>
                             </td>
                            <%--<td style="width:30%" align="left">
                                <asp:TextBox CssClass="altText" ID="txtUserLogin" runat="server"></asp:TextBox>
                                
                            </td>--%><%-- FB 1773 --%>
                         </tr>   

                        <tr>
                            <td style="width:20%" align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_Password%>" runat="server"></asp:Literal><span class="reqfldstarText">*</span></td>
                            <td style="width:30%" align="left">
                                <asp:TextBox ID="txtPassword1" style="background-image:url('../image/pwdbg.png'); background-repeat:no-repeat" onchange="javascript:PreservePassword()" TextMode="Password"  onblur="PasswordChange(1)" onfocus="fnTextFocus(this.id,1)" CssClass="altText" runat="server"></asp:TextBox> <%--FB 3054  ZD 100263--%>
                                <asp:RequiredFieldValidator ID="reqPassword1" Enabled="false" ControlToValidate="txtPassword1" Display="dynamic" ErrorMessage="<%$ Resources:WebResources, Required%>" runat="server"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="regPassword1" ControlToValidate="txtPassword1" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters33%>" ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator><%--FB 2339--%>
                                <asp:CompareValidator ID="cmpValPassword1" runat="server" ControlToCompare="txtPassword2"
                                    ControlToValidate="txtPassword1" Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, EnterPassword%>"></asp:CompareValidator>
                            </td>
                            <td style="width:20%" align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_RetypePassword%>" runat="server"></asp:Literal><span class="reqfldstarText">*</span></td>
                            <td style="width:30%" align="left">
                                <asp:TextBox CssClass="altText" style="background-image:url('../image/pwdbg.png'); background-repeat:no-repeat" ID="txtPassword2" onchange="javascript:PreservePassword()"  onblur="PasswordChange(2)" onfocus="fnTextFocus(this.id,2)" TextMode="Password" runat="server"></asp:TextBox> <%--FB 3054  ZD 100263--%>
                                <asp:RegularExpressionValidator ID="regPassword2" ControlToValidate="txtPassword2" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters33%>" ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator><%--FB 2339--%>
                                <asp:CompareValidator ID="cmpValPassword2" runat="server" ControlToCompare="txtPassword1"
                                    ControlToValidate="txtPassword2" Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, PasswordNotMatch%>"></asp:CompareValidator>
                            </td>
                        </tr>
                         <%--FB 2339 - Start--%>
                        <tr  runat="server"> <%--ZD 100164 -Changes made by Inncrewin - Start--%>
                        <td  colspan = "2" align="left">
                           <span id="spanpassword" runat="server"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_spanpassword%>" runat="server"></asp:Literal></span> <%-- ZD 102689  --%>
                        </td>
                        <td id="tdNewEmail_lbl" runat="server" align="left" style="display:none" class="blackblodtext">User Email<span class="reqfldstarText">*</span></td>
                        <td id="tdNewEmail_txt" runat="server" align="left" style="display:none"></td>
                        <td id="tdNewUsrRole" runat="server" style="width:20%;display:none" align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_tdNewUsrRole%>" runat="server"></asp:Literal><span class="reqfldstarText">*</span></td>
                         <td id="tdNewDrpDownUsrRole" runat="server" style="width:30%;display:none" align="left">
                                <asp:DropDownList ID="lstNewUserRole" CssClass="altSelectFormat" DataValueField="ID" DataTextField="name" runat="server" OnSelectedIndexChanged="UpdateNewDepartment" AutoPostBack="true">
                                </asp:DropDownList>                                
                         </td><%--ZD 100164 -Changes made by Inncrewin - Ends--%>
                        </tr>
                         <%--FB 2339 - End--%>
                        <tr runat="server" ID="trMailinfo">
                            <td style="width:20%" align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_UserEmail%>" runat="server"></asp:Literal><span class="reqfldstarText">*</span></td>
                            <td align="left">
                                <asp:TextBox ID="txtUserEmail" runat="server" CssClass="altText" onblur="FillBJNUser()" ></asp:TextBox> <%--ZD 104116--%>
                                <asp:RequiredFieldValidator ID="reqUserEmail" ControlToValidate="txtUserEmail" Display="dynamic" ErrorMessage="<%$ Resources:WebResources, Required%>" runat="server"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="regEmail1_1" ControlToValidate="txtUserEmail" Display="dynamic" runat="server" 
                                    ErrorMessage="<%$ Resources:WebResources, Invalidemail%>" ValidationExpression="[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"></asp:RegularExpressionValidator>
                                <asp:RegularExpressionValidator ID="regEmail1_2" ControlToValidate="txtUserEmail" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters14%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=:#$%&'~]*$"></asp:RegularExpressionValidator>
                            </td>
                            <td id="tdSecondaryEmail" runat="server" style="width:20%" align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_tdSecondaryEmail%>" runat="server"></asp:Literal></td>
                            <td id="tdtxtUserEmail" runat="server" style="width:30%" align="left">
                                <asp:TextBox CssClass="altText" ID="txtUserEmail2" runat="server" onblur="CheckSecondaryEmail()"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="regEmail2_1" ControlToValidate="txtUserEmail2" Display="dynamic" runat="server" 
                                    ErrorMessage="<%$ Resources:WebResources, Invalidemail%>" ValidationExpression="[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"></asp:RegularExpressionValidator>
                                <asp:RegularExpressionValidator ID="regEmail2_2" ControlToValidate="txtUserEmail2" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters25%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=:#$%&'~]*$"></asp:RegularExpressionValidator>
                               
                            </td>
                        </tr>
                         <%--ZD 104850 start--%>
                        <tr>
                            <td style="width:20%" align="left" class="blackblodtext"><asp:Literal ID="Literal14" Text="<%$ Resources:WebResources, ManageUserProfile_EmailNotificat%>" runat="server"></asp:Literal></td>
                            <td style="width:30%" align="left">
                                    <asp:DropDownList ID="lstEmailNotification" runat="server" CssClass="altText">
                                        <asp:ListItem Selected="True" Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                        <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                    </asp:DropDownList>
                            </td>
                        </tr>
                        <%--ZD 104850 End--%>
                        <tr id="trLDAPLogin" runat="server"><%-- FB 1773-Starts --%><%--ZD 100164--%>
                        <td style="width:20%" align="left" class="blackblodtext">
                                <asp:Label id="Label4" runat="server" text="<%$ Resources:WebResources, ManageUserProfile_Label4%>"></asp:Label>
                        </td>
                        <td style="width:30%" align="left">
                        <asp:TextBox CssClass="altText" ID="txtUserLogin" runat="server"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="regUserLogin" ControlToValidate="txtUserLogin" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters33%>" ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> <%-- ZD 100263 --%>
                        </td>
                        <td class="blackblodtext" align="left"> <%-- ZD 104850 start--%>
                            <asp:Literal Text="<%$ Resources:WebResources, ADLDAPDomain%>" runat="server"></asp:Literal>
                        </td>
                        <td class="blackblodtext" align="left">
                            <asp:TextBox ID="txtUserDomain" runat="server" CssClass="altText" Width="172px" ></asp:TextBox>
                            <asp:RegularExpressionValidator ID="RegUserDomain" ControlToValidate="txtUserDomain" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters33%>" ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator>
                        </td><%-- ZD 104850 End--%>
                        </tr><%-- FB 1773-End --%>
                        <%--ZD 100164--%>
                        <tr id="trWork" runat="server"><%-- Phone - New Design START--%>
                            <td style="width:20%" align="left" class="blackblodtext">
                                <asp:Label id="Label2" runat="server" text="<%$ Resources:WebResources, ManageUserProfile_Label2%>"></asp:Label>
                            </td>
                            <td style="width:30%" align="left" >
                                <asp:TextBox CssClass="altText" ID="txtWorkPhone" runat="server"></asp:TextBox>                               
                            </td>
                            <td style="width:20%" align="left" class="blackblodtext">
                                <asp:Label id="Label3" runat="server" text="<%$ Resources:WebResources, ManageUserProfile_Label3%>"></asp:Label>
                            </td>
                            <td style="width:30%" align="left" >
                                <asp:TextBox CssClass="altText" ID="txtCellPhone" runat="server"></asp:TextBox>                               
                            </td>
                        </tr><%-- Phone - New Design End--%>
                        <tr id="trtzone" runat="server"><%-- ZD 102231--%>
                            <%--Code changed for FB 1425 QA Bug -Start--%>
                            <td style="width:20%" align="left" class="blackblodtext" id="TzTD1" runat="server"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_TzTD1%>" runat="server"></asp:Literal><span class="reqfldstarText">*</span></td>
                            <td style="width:30%" align="left" id="TzTD2" runat="server">
                            <div style="width:172px; overflow:hidden;"> <%--FB 2982--%>
                                <asp:DropDownList ID="lstTimeZone" runat="server" CssClass="altSelectFormat" DataTextField="timezoneName" DataValueField="timezoneID" Width="172px" onchange='ResetWidth(this)'  onblur='ResetWidth(this)' onmousedown='SetWidthToAuto(this)'> <%--FB 2982--%>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="reqTZ" ControlToValidate="lstTimeZone" ErrorMessage="<%$ Resources:WebResources, Required%>" InitialValue="-1" Display="dynamic" runat="server"></asp:RequiredFieldValidator>
                                </div><%--FB 2982--%>
                            </td>
                            <%--Code changed for FB 1425 QA Bug -End--%>
                            <td id="tdPreferredAddressBook" runat="server" style="width:20%" align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_tdPreferredAddressBook%>" runat="server"></asp:Literal></td>
                            <td id="tdlstAddressBook" runat="server" style="width:30%" align="left"><%--ZD 100164--%>
                                <asp:DropDownList ID="lstAddressBook" runat="server" CssClass="altSelectFormat" >
                                    <%--Code Modified. Removed selected text from existing on 21MAr09 - FB 412 - Start --%>
                                    <asp:ListItem  Value="-1" Text="None"></asp:ListItem> <%-- FB Case 526: Saima--%>
                                    <%--Code Modified. Removed selected text from existing on 21MAr09 - FB 412 - End    --%> 
                                    <%--<asp:ListItem Selected="True" Value="-1" Text="None"></asp:ListItem>--%> <%-- FB Case 526: Saima--%>
                                    <%--<asp:ListItem Value="1" Text="MS Outlook 2000/2002/XP"></asp:ListItem>--%>
<%--                                    <asp:ListItem Value="2" Text="Lotus Notes 6.x"></asp:ListItem>
--%>                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Enabled="false" ControlToValidate="lstAddressBook" ErrorMessage="<%$ Resources:WebResources, Required%>" InitialValue="-1" Display="dynamic" runat="server"></asp:RequiredFieldValidator> <%-- FB Case 526: Saima--%>
                            </td>
                        </tr>
                        <tr>
                            <td id="tdDefaultGroup" runat="server" style="width:20%" align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_tdDefaultGroup%>" runat="server"></asp:Literal></td>
                            <td id="tdlstDefaultGroup" runat="server" style="width:30%" align="left"><%--ZD 100164--%>
                                <asp:DropDownList ID="lstDefaultGroup" CssClass="altSelectFormat" DataValueField="groupID" DataTextField="groupName" runat="server"></asp:DropDownList>
                            </td> <%-- ZD 100263 --%>
                            <td id="tdUsrRole" runat="server" style="width:20%" align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_tdUsrRole%>" runat="server"></asp:Literal><span class="reqfldstarText">*</span></td>
                            <td id="tdDrpDownUsrRole" runat="server" style="width:30%" align="left">
                                <asp:DropDownList ID="lstUserRole" CssClass="altSelectFormat" DataValueField="ID" DataTextField="name" runat="server" OnSelectedIndexChanged="UpdateDepartments" AutoPostBack="true">
                                </asp:DropDownList>
                                <asp:DropDownList ID="lstRoles" Visible="false" DataValueField="ID" DataTextField="menuMask" runat="server"></asp:DropDownList><%-- ZD 103686 --%>
                                <asp:DropDownList ID="lstAdmin" Visible="false" DataValueField="ID" DataTextField="level" runat="server"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="reqRole" ControlToValidate="lstUserRole" ErrorMessage="<%$ Resources:WebResources, Required%>" InitialValue="-1" Display="dynamic" runat="server"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr id="trAccountExpiration" runat="server"><%--ZD 100164--%>
                            <td style="width:20%" align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_AccountExpirat%>" runat="server"></asp:Literal><span class="reqfldstarText">*</span></td>
                            <td style="width:30%" align="left">
                                <asp:TextBox ID="txtAccountExpiry" onchange="javascript:CheckDate(this)" CssClass="altText" runat="server" ></asp:TextBox><%--ZD 101744--%>
                                <%-- Code added by Offshore for FB Issue 1073 -- Start
                                 Code Modified For FB 1425- For Javascript date error.
                                 <img src="image/calendar.gif" border="0" style="width:20" height="20" id="Img1" style="cursor: pointer;" title="Date selector" onclick="return showCalendar('<%=txtAccountExpiry.ClientID %>', 'cal_triggerd', 1, '%m/%d/%Y');" />--%>
                                <a href="" onclick="this.childNodes[0].click();return false;"><img src="image/calendar.gif" alt="Date Selector" border="0" width="20" height="20" id="cal_triggerd" style='cursor: pointer;<%=((txtAccountExpiry.Enabled)? "" : "display:none;")%>'
                                 title="<asp:Literal Text='<%$ Resources:WebResources, DateSelector%>' runat='server'></asp:Literal>" onclick="return showCalendar('<%=txtAccountExpiry.ClientID %>', 'cal_triggerd', 1, '<%=format%>');" /></a><%--ZD 100420--%>                                
                                 <%-- Code added by Offshore for FB Issue 1073 -- End --%>
                                <asp:RequiredFieldValidator ID="reqAccExp" ControlToValidate="txtAccountExpiry" runat="server" ErrorMessage="<%$ Resources:WebResources, Required%>" Display="Dynamic"></asp:RequiredFieldValidator>
<%--                                <asp:RangeValidator ID="rangeExpiry" ControlToValidate="txtAccountExpiry" runat="server" ErrorMessage="Invalid Expiry Date" Display="dynamic"></asp:RangeValidator>
--%>                            </td>
                            <td id="tdScheduleMin" runat="server" style="width:20%" align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_tdScheduleMin%>" runat="server"></asp:Literal><span class="reqfldstarText">*</span></td><%--FB 2659--%>
                            <td style="width:30%" align="left">
								<%-- ZD 102043 starts --%>
                                <asp:Label ID="lblTimeRemaining" runat="server" ></asp:Label>
                                <%if (enableCloudInstallation == 0)
                                  {%>
                                <span>&nbsp;/&nbsp;</span><%-- FB 1773 --%>
                                <%} %>
                                <asp:TextBox ID="txtTotalTime" Width="113px" runat="server"></asp:TextBox><%-- FB 1773 --%>
                                <asp:RequiredFieldValidator ID="reqMinutes" SetFocusOnError="true" runat="server" ErrorMessage="<%$ Resources:WebResources, Required%>" CssClass="lblError" ControlToValidate="txtTotalTime" ></asp:RequiredFieldValidator>
                                <asp:RangeValidator ID="ValidatorTimeremaining" SetFocusOnError="true" runat="server" CssClass="lblError" Display="dynamic" ControlToValidate="txtTotalTime"
                                    ErrorMessage="<%$ Resources:WebResources, Reqtimeremaining%>" MaximumValue="2000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>                            </td>
								<%-- ZD 102043 End --%>
                        </tr>
                        <%--<tr>
                            <td style="width:20%" align="left" class="blackblodtext">Email Notification</td>
                            <td style="width:30%" align="left">
                                <asp:DropDownList ID="lstEmailNotification" runat="server" CssClass="altText">
                                    <asp:ListItem Selected="True" Value="1" Text="Yes"></asp:ListItem>
                                    <asp:ListItem Value="0" Text="No"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td style="width:20%" align="left" class="blackblodtext">My Default Search</td>
                            <td style="width:30%" align="left">
                                <asp:DropDownList ID="lstSearchTemplate" CssClass="altSelectFormat" DataTextField="Name" DataValueField="ID" runat="server" onchange="javascript:DisplayWarning(this);"></asp:DropDownList>
                            </td>
                        </tr>--%><%-- FB 1773 --%>
                        <tr id="trPreferredLocation" runat="server"><%--ZD 100164--%>
                            <td style="width:20%" align="left" class="blackblodtext" valign="top"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_PreferredLocat%>" runat="server"></asp:Literal></td>
                            <td style="width:30%" align="left" valign="top" runat="server" id="TDprefLocation">
                                <asp:TextBox ID="txtLocation" runat="server" CssClass="altText" style="display:none;" Enabled="false" ></asp:TextBox>
                                <select size="4" name="RoomList" runat="server" id="RoomList" class="treeSelectedNode" Rows="3"></select>
                                 <input name="addRooms" type="button" id="addRooms" onclick="javascript:AddRooms();" style="display:none;" />
                                <a href="javascript: OpenRoomSearch('frmUserProfile');" onmouseover="window.status='';return true;" runat="server" id="roomclick">
                                <img border="0" runat="server" id="roomimage" src="image/roomselect.gif" alt="edit" style="width:17px;cursor:pointer;"  
                                title="<%$ Resources:WebResources, SelectLocation%>"></a>  <%--FB 2798 ZD 100429--%>                             
                                <input type="hidden" id="hdnLocation" runat="server" />
                            </td>
                            <td style="width:20%" align="left" class="blackblodtext" valign="top"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_UsersDepartmen%>" runat="server"></asp:Literal></td><%-- FB 1773 --%>
                            <td style="width:30%" align="left" valign="top" nowrap="nowrap"><%--ZD 100425--%>
                                <asp:ListBox runat="server" ID="lstDepartment" CssClass="altSelectFormat" DataTextField="name" DataValueField="id" Rows="6" SelectionMode="Multiple"></asp:ListBox><%-- Added for FB 1507 --%>
                                <br /><span style='color: #666666;'>* <asp:Literal Text="<%$ Resources:WebResources, SelectMultiDept%>" runat="server"></asp:Literal></span>
                            </td>
                        </tr>
                        <tr id="trSendBoth" runat="server"><%--ZD 100164--%>
                            <td align="left" class="blackblodtext" valign="top" style="width:20%"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_SendEmailtoB%>" runat="server"></asp:Literal></td>
                            <td align="left" valign="top" style="width:30%">
                                <asp:DropDownList ID="lstSendBoth" CssClass="altText" runat="server" onchange="javascript:CheckSecondaryEmail()">  <%-- --%>
                                    <asp:ListItem Text="<%$ Resources:WebResources, No%>" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="<%$ Resources:WebResources, Yes%>" Value="1"></asp:ListItem>
                                </asp:DropDownList>
                            </td><%-- FB 1773 --%>
                            <td style="width:20%" align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_MyDefaultSear%>" runat="server"></asp:Literal></td>
                            <td style="width:30%" align="left">
                                <asp:DropDownList ID="lstSearchTemplate" CssClass="altSelectFormat" DataTextField="Name" DataValueField="ID" runat="server" onchange="javascript:DisplayWarning(this);"></asp:DropDownList>
                            </td>
                        </tr>
                        <%--Code added For FB Issue 1073 Start --%> 
                        <tr id="trDateFormat" runat="server"><%--ZD 100164--%>
                            <td style="width:20%" align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_DateFormat%>" runat="server"></asp:Literal></td>
                            <td style="width:30%" align="left" nowrap="nowrap"> <%--FB 2611--%>
                                <asp:DropDownList ID="DrpDateFormat" runat="server" CssClass="altSelectFormat" >
                                    <asp:ListItem Selected="True" Value="MM/dd/yyyy" Text="mm/dd/yyyy"></asp:ListItem> <%--ZD 101476--%>
                                    <asp:ListItem Value="dd/MM/yyyy" Text="dd/mm/yyyy"></asp:ListItem>                                  
                              </asp:DropDownList>/ 
                               <asp:DropDownList ID="lstTimeFormat" CssClass="altText" runat="server">
                                    <asp:ListItem Selected="True" Value="1" Text="<%$ Resources:WebResources, DateFormat_1%>"></asp:ListItem>
                                    <asp:ListItem Value="0" Text="<%$ Resources:WebResources, DateFormat_2%>"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="<%$ Resources:WebResources, DateFormat_3%>"></asp:ListItem><%--FB 2588--%>
                                </asp:DropDownList>
                                
                            </td>
                            <%--Code changed for FB 1425 QA Bug -Start--%>
                            <td style="width:20%" align="left" class="blackblodtext" id="TzTD3" runat="server"><asp:Literal Text="<%$ Resources:WebResources, DisplayTimezoneonallScreens%>" runat="server"></asp:Literal></td>
                            <td style="width:50%" align="left" id="TzTD4" runat="server">
                            <%-- Organization Css Module --%>   
                                <asp:DropDownList ID="lstTimeZoneDisplay" runat="server" CssClass="altSelectFormat">
                                    <asp:ListItem Selected="True" Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                    <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <%--Code changed for FB 1425 QA Bug -Start--%>
                        </tr>
                    <%--Code added For FB Issue 1073 End --%>
                        <%--Code changed for MOJ Phase 2 QA Bug -Start--%>
                        <tr id="EnableTR" runat="server">
                       <%--Code changed for MOJ Phase 2 QA Bug -End--%>
                            <td style="width:20%" align="left" class="blackblodtext">
                                <asp:Label id="ParLBL" runat="server" text="<%$ Resources:WebResources, ManageUserProfile_ParLBL%>"></asp:Label>
                            </td>
                            <td style="width:30%" align="left" >
                                <asp:DropDownList ID="DrpEnvPar" runat="server" CssClass="altSelectFormat" >
                                    <asp:ListItem Selected="True" Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem> 
                                    <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>                                  
                              </asp:DropDownList>                                
                            </td>
                            <td style="width:20%" align="left" class="blackblodtext">
                                <asp:Label id="AVLBL" runat="server" text="<%$ Resources:WebResources, ManageUserProfile_AVLBL%>"></asp:Label>
                            </td>
                            <td style="width:30%" align="left" >
                                <asp:DropDownList ID="DrpEnableAV" runat="server" CssClass="altSelectFormat" >
                                    <asp:ListItem Selected="True" Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem> 
                                    <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>                                  
                              </asp:DropDownList>                                
                            </td>
                        </tr>
                        <%--ZD 101093 START 102231--%>
                        <tr id="EbleWO" runat="server">
                            <td style="width:20%" align="left" class="blackblodtext">
                                <asp:Label ID="Lbladdopt" runat="server" Text="<%$ Resources:WebResources, EnableAdditionalOptions%>"></asp:Label>
                            </td>
                            <td style="width:30%" align="left" >
                                <asp:DropDownList ID="drpadditionaloption" runat="server" CssClass="altSelectFormat" >
                                    <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem> 
                                    <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>                                  
                              </asp:DropDownList>                                
                            </td>
                            <%--ZD 101122 Start--%>
                            <td style="width:20%" align="left" class="blackblodtext">
                                <asp:Label ID="LblAVWO" runat="server" Text="<%$ Resources:WebResources, EnableAudioVisualWO%>"></asp:Label>
                            </td>
                            <td style="width:30%" align="left" >
                                <asp:DropDownList ID="DrpAVWO" runat="server" CssClass="altSelectFormat" >
                                    <asp:ListItem Selected="True" Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem> 
                                    <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>                                  
                              </asp:DropDownList>                                
                            </td>                          
                        </tr>
                        <tr id="EbleCatWO" runat="server">
                            <td style="width:20%" align="left" class="blackblodtext">
                                <asp:Label ID="LblCateringWO" runat="server" Text="<%$ Resources:WebResources, EnableCateringWO%>"></asp:Label>
                            </td>
                            <td style="width:30%" align="left" >
                                <asp:DropDownList ID="DrpCateringWO" runat="server" CssClass="altSelectFormat" >
                                    <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem> 
                                    <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>                                  
                              </asp:DropDownList>                                
                            </td>
                            <td style="width:20%" align="left" class="blackblodtext">
                                <asp:Label ID="LblFacilityWO" runat="server" Text="<%$ Resources:WebResources, EnableFacilityWO%>"></asp:Label>
                            </td>
                            <td style="width:30%" align="left" >
                                <asp:DropDownList ID="DrpFacilityWO" runat="server" CssClass="altSelectFormat" >
                                    <asp:ListItem Selected="True" Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem> 
                                    <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>                                  
                              </asp:DropDownList>                                
                            </td>
                          
                        </tr>
                        <%--ZD 101122 End--%>
                          <%--ZD 101093 END--%>

                        <%--Code Added for License modification START--%>
                        <tr id="trUser" runat="server">
                            <td style="width:20%" align="left" class="blackblodtext">
                          <%-- FB 2098 --%> 
                          <asp:Label id="LblExc" runat="server" text="<%$ Resources:WebResources, ManageUserProfile_LblExc%>"></asp:Label>
                            </td>
                            <td style="width:30%" align="left" >
                                <asp:DropDownList ID="DrpExc" runat="server" CssClass="altSelectFormat" >
                                    <asp:ListItem Value="0" Selected="True" Text="<%$ Resources:WebResources, No%>"></asp:ListItem> 
                                    <asp:ListItem  Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>                                  
                              </asp:DropDownList>                                
                            </td>
                            <td style="width:20%" align="left" class="blackblodtext" id="lblDOmino" runat="server">
                          <%--  FB 2098  --%>  
                          <asp:Label id="LblDom" runat="server" text="<%$ Resources:WebResources, ManageUserProfile_LblDom%>"></asp:Label>
                            </td>
                            <td style="width:30%" align="left" >
                                <asp:DropDownList ID="DrpDom" runat="server" CssClass="altSelectFormat" >
                                    <asp:ListItem Value="0" Selected="True" Text="<%$ Resources:WebResources, No%>"></asp:ListItem> 
                                    <asp:ListItem  Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem> 
                                                                     
                              </asp:DropDownList>                                
                            </td>
                        </tr>
                        <%--FB 2023 start --%> 
                        <tr id="trEnableMobile" runat="server"><%--ZD 100164--%>
                            <td style="width:20%" align="left" class="blackblodtext">
                                    <asp:Label id="LblMob" runat="server" text="<%$ Resources:WebResources, ManageUserProfile_LblMob%>"></asp:Label>
                                </td>
                                <td style="width:30%" align="left" >
                                <asp:DropDownList ID="DrpMob" runat="server" CssClass="altSelectFormat" >
                                    <asp:ListItem Value="0" Selected="True" Text="<%$ Resources:WebResources, No%>"></asp:ListItem> 
                                    <asp:ListItem  Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>                                  
                                </asp:DropDownList>                                
                                </td>
                            <%--FB 1830 - Start--%>                               
                            <td style="width:20%" align="left" class="blackblodtext">
                            <asp:Label id="PrefLang" runat="server" text="<%$ Resources:WebResources, ManageUserProfile_PrefLang%>"></asp:Label>
                            </td>
                            <td style="width:30%" align="left" >
                                <asp:DropDownList ID="lstLanguage" runat="server" CssClass="altSelectFormat" DataTextField="name" DataValueField="ID"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="reglstLanguage" ControlToValidate="lstLanguage" ErrorMessage="<%$ Resources:WebResources, Required%>" InitialValue="-1" Display="dynamic" runat="server"></asp:RequiredFieldValidator>
                            </td>
                            </tr>
                            <%--FB 1979 - Start--%>
                            <tr id="trEmailLanguage" runat="server"><%--ZD 100164--%>
                                <td style="width:19%" align="left" valign="top" class="blackblodtext">
                                <asp:Label id="emaillang" runat="server" text="<%$ Resources:WebResources, ManageUserProfile_emaillang%>"></asp:Label>
                                </td>
                                <td align="left" style="width:30%" > <%--FB 1830 - DeleteEmailLang start--%>  
                                    <%-- FB 2029 starts --%>
                                    <asp:Button ID="btnDefine" runat="server" Text="<%$ Resources:WebResources, ManageUserProfile_btnDefine%>" OnClick="DefineEmailLanguage" class="altLongBlueButtonFormat"  OnClientClick="DataLoading(1);"/> <%--ZD 100176--%> 
                                    <asp:TextBox ID="txtEmailLang" runat="server" ReadOnly="true" CssClass="altText" Visible="false"></asp:TextBox>  <%--FB 2104 --%> <%-- FB 2029 end --%>                          
                                    <asp:ImageButton ImageUrl="image/btn_delete.gif" runat="server" ID="delEmailLang" ToolTip="<%$ Resources:WebResources, DeleteEmailLanguage%>" onclick="DeleteEmailLangugage" OnClientClick="javascript:return fnDelEmailLan()" />
                                </td>  
                                <td style="width:20%" align="left" class="blackblodtext">
                                    <asp:Label id="Label5" runat="server" text="<%$ Resources:WebResources, ManageUserProfile_Label5%>"></asp:Label>
                                </td>
                                <td style="width:30%" align="left" >
                                <asp:DropDownList ID="DrpPIMNotifications" runat="server" CssClass="altSelectFormat" >
									<%--ZD 102545--%>
                                    <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem> 
                                    <asp:ListItem  Value="1" Selected="True" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>                                  
                                </asp:DropDownList>     
                                    <input style="border:none;width:10px; display:none" id="hdntext" type="text" onfocus="document.getElementById('Button1').focus();"> <%--ZD 100420--%>                   
                                </td>
                            </tr>
                            <%--FB 1979 - End--%>
                            <%--New Lobby Page--%>                            
                            <tr id="trLobbyManagement" runat="server"><%--ZD 100164--%>
                            <td style="width:19%" align="left" valign="top" class="blackblodtext">
                                <asp:Label id="Loginmgmt" runat="server" text="<%$ Resources:WebResources, ManageUserProfile_Loginmgmt%>"></asp:Label>
                            </td>
                            <td style="width:10%" align="left" valign="bottom">
                                <asp:Button id="btnlogin" runat="server" text="<%$ Resources:WebResources, ManageUserProfile_btnlogin%>" class="altLongBlueButtonFormat" onclick="lobbyManage" onclientclick="DataLoading(1)"></asp:Button><%--ZD 100176--%> 
                            </td>
                            <%-- FB 2029 starts --%>
                            <td align="left" valign="center" class="blackblodtext"> <%--FB 1860--%>
                                <asp:Label id="LblBlockEmails" runat="server" text="<%$ Resources:WebResources, ManageUserProfile_LblBlockEmails%>"></asp:Label>
                            </td>
                            <td align="left"  colspan = "4"  > <%--FB 1830 - DeleteEmailLang end--%>  
                            <table><tr><td>
                                <asp:CheckBox ID="ChkBlockEmails" runat="server" />
                                </td><td>
                                <asp:Button ID="BtnBlockEmails" style="display:none;" runat="server" Text="<%$ Resources:WebResources, ManageUserProfile_BtnBlockEmails%>" OnClick="EditBlockEmails"  class="altMedium0BlueButtonFormat" /> <%--ZD 100369--%>
                                </td></tr></table>
                            </td> <%-- FB 2029 end --%>
                           </tr>
                           <tr id="trHelpRequestorPhone" runat="server" style="display:none"> <%--FB 2268 --%> <%--ZD 100164--%><%--ZD 100322--%>
                             <td style="width:19%" align="left" valign="top" class="blackblodtext">
                                <asp:Label id="lblHelpReqPhone" runat="server" text="<%$ Resources:WebResources, ManageUserProfile_lblHelpReqPhone%>"></asp:Label>
                            </td>
                            <td style="width:10%" align="left" valign="top" >
                                <asp:TextBox ID="txtHelpReqPhone" runat="server" CssClass="altText"></asp:TextBox>                                 
                            </td>
                            <td align="left" valign="top" class="blackblodtext">
                              <asp:Label id="lblHelpReqEmail" runat="server" text="<%$ Resources:WebResources, ManageUserProfile_lblHelpReqEmail%>"></asp:Label>
                             </td>
                             <td align="left" style="width:30%" valign="top">
                                <asp:TextBox ID="txtHelpReqEmail" runat="server" CssClass="altText"></asp:TextBox> 
                                <asp:Button id="btnAddHelpReq" runat="server" Text="<%$ Resources:WebResources, ManageUserProfile_btnAddHelpReq%>" class="altShortBlueButtonFormat" OnClientClick="javascript:return AddRemoveHelpReq('add')" />
                                 <asp:RegularExpressionValidator ID="RegtxtHelpReqEmail" ControlToValidate="txtHelpReqEmail" Display="dynamic" runat="server" 
                                    ErrorMessage="<%$ Resources:WebResources, Invalidemail%>" ValidationExpression="[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"></asp:RegularExpressionValidator>
                                <asp:RegularExpressionValidator ID="RegtxtHelpReqEmail_1" ControlToValidate="txtHelpReqEmail" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters14%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=:#$%&'~]*$"></asp:RegularExpressionValidator>                                    
                                <br /><br />
                                <asp:ListBox runat="server" ID="lstHelpReqEmail" CssClass="altSelectFormat" Rows="5" SelectionMode="Multiple"  onDblClick="javascript:return AddRemoveHelpReq('Rem')"  ></asp:ListBox>
                             </td>
                             
                           </tr>
                            <%--FB 2348 End--%><%--FB 2595 End--%>
                            <%--FB 2608 Start--%>                            
                            <tr id="trVNOCSelection" runat="server"><%--ZD 100164--%>
                             <td style="width:20%" align="left" class="blackblodtext">
                                <asp:Label id="Label1" runat="server" text="<%$ Resources:WebResources, ManageUserProfile_Label1%>"></asp:Label>
                            </td>
                            <td style="width:30%" align="left" >
                               <asp:CheckBox ID="chkVNOC" runat="server" />
                            </td>
                             <%--FB 2724 Start--%>        
                             <td style="width:20%" align="left" class="blackblodtext">
                                <asp:Label id="lblRFID" runat="server" text="<%$ Resources:WebResources, ManageUserProfile_lblRFID%>"></asp:Label>
                            </td>
                            <td style="width:30%" align="left" >
                                <asp:TextBox ID="txtRFIDValue" runat="server" CssClass="altText"></asp:TextBox> 
                            </td>
                             <%--FB 2724 Start--%>        
                           </tr>  
                            <%--FB 2348 Start--%><%--FB 2595 Start--%><%--ZD 100322 UI Modification START--%>
                           <tr id="trSurveyEmail" runat="server"><%--ZD 1001164--%>
                           <%--<td style="width:20%;" align="left" class="blackblodtext">
                                <asp:Label ID="LblSecured" runat="server" Text="Require Secure"></asp:Label>
                            </td>
                            <td id="tdchkSecured" style="width:30%;" align="left" >
                                <asp:CheckBox ID="chkSecured" runat="server" />
                            </td>--%>
                             <td id="tdSurveyEmail" style="width:20%;visibility:hidden" align="left" class="blackblodtext">
                                <asp:Label id="LblSurveyEmail" runat="server" text="<%$ Resources:WebResources, ManageUserProfile_LblSurveyEmail%>"></asp:Label>
                            </td>
                            <td id="tddrpSurveyEmail" style="width:30%;visibility:hidden" align="left" >
                                <asp:DropDownList Width="20%" ID="drpSurveyEmail" runat="server" CssClass="altText" >
                                    <asp:ListItem Value="0" Selected="True" Text="<%$ Resources:WebResources, No%>"></asp:ListItem> 
                                    <asp:ListItem  Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>                                  
                                </asp:DropDownList>   
                            </td>
                           </tr>                         
                           <%--FB 2608 End--%><%--ZD 100322 UI Modification End--%>                           
                           <tr style="display:none"><%--Audio Add On--%>
                             <td style="width:20%" align="left" class="blackblodtext">
                                <asp:Label id="Label6" runat="server" text="<%$ Resources:WebResources, ManageUserProfile_Label6%>"></asp:Label>
                            </td>
                            <td style="width:30%" align="left" >
                                <asp:CheckBox ID="chkAudioAddon" runat="server" />
                            </td>
                            <td colspan="2"></td>
                           </tr>
                            <%--FB 1830 - End--%><%--FB 2023 end --%> 
                        <%--Code Added for License modification END--%>                        
                         <%--Code changed for RSS Feed--%>
                         <%-- FB 1985--%>
                         <%if ((Application["Client"].ToString().ToUpper().Equals("DISNEY")))
                           {%>
                         <tr>
                            <td id="tblAVExpand" runat="server" colspan="4"> <%--Disney New Requirement--%>
                                <table border="0" align="left" width="80px">
                                    <tr> 
                                        <td id="Td1"  align="left"  onmouseover="javascript:return fnShowHideAVLink('1');" onmouseout="javascript:return fnShowHideAVLink('0');" runat="server">&nbsp;
                                            <asp:LinkButton ID="LnkAVExpand" style="display:none" runat="server" Text="<%$ Resources:WebResources, ManageUserProfile_LnkAVExpand%>" OnClientClick="javascript:return fnShowAVParams()"></asp:LinkButton>
                                        </td>
                                     </tr>
                                </table>
                             </td>
                            </tr>    <%} %> 
                        <tr style="display:none;">
                        <td style="width:20%" align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_RSSFeed%>" runat="server"></asp:Literal></td>
                        <td style="width:30%" align="left" >
                        <asp:ImageButton ID="ImageButton1"  src="image/rss.gif"  runat="server"  OnClick="RSSFeed" ToolTip="<%$ Resources:WebResources, ManageUserProfile_RSSFeed%>" />
                        </td>
                        </tr>
                        <tr id="trStaticID" runat="server" align ="left">
                            
                            <td class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_EnableStaticID%>" runat="server"></asp:Literal></td>
                            <td><asp:CheckBox id="chkEnblStaticId" runat="server"/></td>   
                            <td id="tdClear1" runat="server"></td> <%--ZD 100969--%>
                            <td id="tdClear2" runat="server"></td> <%--ZD 100969--%>                     
                        </tr>
                        <tr id="trStaticBox" style="display:none" align ="left">
                            
                           <td class="blackblodtext"><asp:Literal ID="Literal10" Text="<%$ Resources:WebResources,ManageUserProfile_StaticID%>" runat="server"></asp:Literal></td>
                           <td><asp:TextBox id="txtStaticBox" runat="server"/></td>
                           <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtStaticBox" CssClass="lblError" ErrorMessage="<%$ Resources:WebResources, ManageUserProfile_StaticIDErrMsg%>" 
                                 Display = "Dynamic" ValidationExpression="^[\s\S]{10,}$"></asp:RegularExpressionValidator>  <%--ZD 101083--%>
                           <td id="tdClear3" runat="server"></td><%--ZD 100969--%>
                           <td id="tdClear4" runat="server"></td><%--ZD 100969--%>
                        </tr>
                        <%-- ZD 100172 Start --%>
                        <tr id="trLandingPage" runat="server">
                            <td class="blackblodtext" align="left">
                                <asp:Literal ID="Literal1" Text="<%$ Resources:WebResources, LandingPage%>" runat="server"></asp:Literal>
                            </td>
                            <td align="left">
                                <asp:DropDownList ID="drpPageList" runat="server" DataTextField="Text" DataValueField="Value" CssClass="altSelectFormat" ></asp:DropDownList>
                            </td>
                            
                          
                        
                        </tr>
                        <%-- ZD 100172 End --%>
                    </table>
                </td>
            </tr>   
            
             <%--ZD 100221 - Start--%>
            <tr>
                <td align="left">
                    <table width="100%" border="0" style="border-collapse:collapse;" id="tblWebEx" runat="server" align="left">
                        <tr>
                            <td colspan="2" align="left" >
                                <span style="margin-left:-24px;" class="subtitleblueblodtext"><asp:Literal ID="Literal6" Text="<%$ Resources:WebResources, WebExConferencing%>" runat="server"></asp:Literal></span> 
                                <br />
                            </td>
                        </tr>
                         <tr>
                            <td>
                                <table border="0" width="100%" cellpadding="1"  cellspacing="3" align="left">
                                    <tr valign="bottom">
                                        <td style="width:17%" align="left"><b><asp:Literal ID="Literal7" Text="<%$ Resources:WebResources, EnableWebExConferencing1%>" runat="server"></asp:Literal></b></td>
                                        <td style="width:40%" align="left">
                                        <input id="ChkWebexConf" type="checkbox" onclick="javascript:fnShowWebExConf(this.checked)" runat="server" style=" margin-left:-1px " /></td>
                                        <td style="width:20%"></td>
                                        <td style="width:20%"></td>
                                    </tr>
                                 </table>
                            </td>
                           </tr>
                           <tr><td>
                                 <table  border="0" width="100%">
                                     <tr id="tblWebExConf" runat="server">
                                        <td style="width:17%" align="left" class="blackblodtext"><asp:Literal ID="Literal8" Text="<%$ Resources:WebResources, UserName%>" runat="server"></asp:Literal></td>
                                        <td style="width:33%" align="left">
                                                <asp:TextBox ID="txtWebExUserName" runat="server" CssClass="altText" Width="172px" ></asp:TextBox>
                                        </td>
                                        <td  style="width:22%" align="left" class="blackblodtext"><asp:Literal ID="Literal9" Text="<%$ Resources:WebResources, Password%>" runat="server"></asp:Literal></td>
                                        <td  style="width:25%" align="left">
                                            <asp:TextBox ID="txtWebExPassword" runat="server" TextMode="password" CssClass="altText" onblur="WebexPasswordChange(1)" style="background-image:url('../image/pwdbg.png'); background-repeat:no-repeat; margin-left:-51px" onfocus="this.style.backgroundImage=''"></asp:TextBox><%-- ZD 100221 WebEx --%>
                                        </td>
                                    </tr>
                                </table>
                            </td></tr>
                           
                        <%--</tr>--%>      <%--ZD 100873--%>                 
                    </table>
                </td>
            </tr>  
            <%--ZD 100221 - Ends--%> 
            <tr> <%--ZD 103550--%>
                <td align="left">
                    <table width="100%" border="0" style="border-collapse:collapse;" id="tblBlueJeansLogin" runat="server" align="left">
                        <tr>
                            <td colspan="2" align="left" >
                                <span style="margin-left:-24px;" class="subtitleblueblodtext"><asp:Literal ID="Literal2" Text="<%$ Resources:WebResources, BlueJeansLogin%>" runat="server"></asp:Literal></span> 
                                <br />
                            </td>
                        </tr>
                         <tr>
                            <td>
                                <table border="0" width="100%" cellpadding="1"  cellspacing="3" align="left">
                                    <tr valign="bottom">
                                        <td style="width:17%" align="left"><b><asp:Literal ID="Literal3" Text="<%$ Resources:WebResources, EnableBJNConferencing%>" runat="server"></asp:Literal></b></td>
                                        <td style="width:40%" align="left">
                                        <input id="ChkBJNConf" type="checkbox" onclick="javascript:fnShowBJNConf(this.checked)" runat="server" style=" margin-left:-1px " /></td>
                                        <td style="width:20%"></td>
                                        <td style="width:20%"></td>
                                    </tr>
                                 </table>
                            </td>
                           </tr>
                        <tr>
                            <td>
                                 <table  border="0" width="100%">
                                     <tr id="tblBJNConf" runat="server">
                                        <td style="width:17%" align="left" class="blackblodtext"><asp:Literal ID="Literal13" Text="<%$ Resources:WebResources, UserName%>" runat="server"></asp:Literal></td>
                                        <td style="width:33%" align="left">
                                                <asp:TextBox ID="txtBJNUserName" runat="server" CssClass="altText" Width="172px" ></asp:TextBox>
                                        </td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>  
             <%--FB 2392-Whygo Start--%>
            <tr id="trwhygouserSettings" runat="server"><%--ZD 100164--%>
                <td >
                    <table cellpadding="2" border="0" cellspacing="3" id="whygouserSettings" runat="server" width="100%" style="margin-left:-41px;position:relative">      
                        <tr id="trWhygUser" runat="server" >
                            <td colspan="4">
                                <table cellspacing="3" border="0">
                                    <tr>     
                                    <td>&nbsp;</td>                        
                                        <td>
                                            <span class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_WhyGoUserSett%>" runat="server"></asp:Literal></span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                     </table>
            <table cellpadding="2" id="whygousersettings1" runat="server" border="0" cellspacing="3" runat="server" width="100%">      
            <tr runat="server" id="trwhygoRegion"> <%--FB 2594--%>
             <td style="width:18%" align="left" valign="top" class="blackblodtext">
                <asp:Label id="lblParent" runat="server" text="<%$ Resources:WebResources, ManageUserProfile_lblParent%>"></asp:Label>
            </td>
            <td style="width:35%" align="left" valign="top" >
                 <input id="txtParentReseller" onkeypress="return isNumberKey(event)" maxlength="8" runat="server" type="text" />
            </td>
            <td style="width:19%" align="left" valign="top" class="blackblodtext">
                <asp:Label id="lblRegion" runat="server" text="<%$ Resources:WebResources, ManageUserProfile_lblRegion%>"></asp:Label>
             </td>
             <td align="left" valign="top" >
                <asp:DropDownList ID="lstRegion" runat="server" Width="25%" CssClass="altText"></asp:DropDownList>                                 
            </td>
           </tr>
            <tr runat="server" id="trCropUser">  <%--FB 2594--%>
             <td align="left" valign="top" class="blackblodtext">
              <asp:Label id="lblCorp" runat="server" text="<%$ Resources:WebResources, ManageUserProfile_lblCorp%>"></asp:Label>
            </td>
            <td align="left" valign="top">
                <input id="txtCorpUser" onkeypress="return isNumberKey(event)" maxlength="8" runat="server" type="text" name="txtCorpUser" />
             </td>
            <td align="left" valign="top" class="blackblodtext">
                <asp:Label id="lblWhyGoUsrId" runat="server" text="<%$ Resources:WebResources, ManageUserProfile_lblWhyGoUsrId%>"></asp:Label>
            </td>
            <td  align="left" valign="top" >
                 <input id="txtESUserID" onkeypress="return isNumberKey(event)"  maxlength="8"  runat="server" type="text" />
            </td>
           </tr>
           <tr runat="server" id="trRoomBrokerNum">  <%--FB 3001 Starts--%>
                 <td align="left" valign="top" class="blackblodtext">
                  <asp:Label id="Label7" runat="server" text="<%$ Resources:WebResources, ManageUserProfile_Label7%>"></asp:Label>
                </td>
                <td align="left" valign="top">
                <input id="txtBrokerNum" maxlength="25"  runat="server" type="text" />
                 </td>
                <td align="left" valign="top" class="blackblodtext">
                </td>
                <td  align="left" valign="top" >
                </td>
           </tr><%--FB 3001 Ends--%>
                    </table>
                </td>
            </tr>
            <%--FB 2392-Whygo End--%>
           
            <%--FB 2693 - Start--%>
            <tr id="trPCConferencing" runat="server"><%--ZD 100164--%>
                <td>
                    <table width="100%" border="0" style="border-collapse:collapse" align="left">
                        <tr>
                            <td colspan="2" >
                                <span style="margin-left:-26px; overflow:visible;" class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_PCConferencing%>" runat="server"></asp:Literal></span> <%-- FB 2811 --%> <%--ZD 100806--%><%--ZD 104021--%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table border="0" ><%--ZD 104021--%>
                                    <tr>
                                        <td width="15%"><b><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_EnablePCConfe%>" runat="server"></asp:Literal></b></td> <%--ZD 100807 ZD 100873--%>
                                        <td width="32%"><input id="chkPcConf" type="checkbox" onclick="javascript:fnShowPcConf(this.checked)" runat="server" style="margin-left:6px" /> </td> <%--ZD 100873--%>
                                    </tr>
                                </table>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" width="53%">
                                <table width="100%" border="0" id="tblPcConf" runat="server" style="display:none" >
                                    <%--ZD 104021--%>
                                    <%--<tr id="trBJ" runat="server" style="display:none" >
                                        <td width="32%" align="right">
                                        </td>
                                        <td width="18%" nowrap="nowrap">
                                            <input id="chkBJ" type="checkbox" runat="server" /> 
                                            <img alt="<%$ Resources:WebResources, BlueJeans%>" runat="server" src="../image/BlueJeans.jpg" title="<%$ Resources:WebResources, BlueJeans%>"/>
                                            <textarea class="altText" cols="20" rows="2" id="txtAreaBlueJeans" runat="server"></textarea>
                                            <a id="ancexpBlueJeans" href="#" onclick="this.childNodes[0].click();return false;"><img id="expBlueJeans" src="../image/loc/nolines_plus.gif" alt="Expand/Collapse" style="width:25px; cursor:pointer" 
                                            title="<%$ Resources:WebResources, UserMenuController_Options%>" onclick="javascript:fnShowPcConfOpt(this,'tblBlueJeans')" runat="server" /></a>
                                            <asp:RegularExpressionValidator ID="rgVdBlueJeans" ControlToValidate="txtAreaBlueJeans" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters33%>" ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> 
                                        </td>
                                    </tr>--%>
                                    <tr id="trJB" runat="server" >
                                        <td width="32%" align="right">
                                        </td>
                                        <td width="18%" nowrap="nowrap">
                                            <input id="chkJB" type="checkbox" runat="server" /> 
                                            <img alt="Jabber" src="../image/Jabber.jpg"  title="Jabber" /><%--ZD 100419--%>
                                            <textarea class="altText" cols="20" rows="2" id="txtAreaJabber" runat="server"></textarea>
                                            <a id="ancexpJabber" href="#" onclick="this.childNodes[0].click();return false;"><img id="expJabber" src="../image/loc/nolines_plus.gif" alt="Expand/Collapse" style="width:25px; cursor:pointer" title="<asp:Literal Text='<%$ Resources:WebResources, UserMenuController_Options%>' runat='server'></asp:Literal>" onclick="javascript:fnShowPcConfOpt(this,'tblJabber')" /></a><%--ZD 100420--%>  <%--ZD 100369--%>
                                            <asp:RegularExpressionValidator ID="rgVdJabber" ControlToValidate="txtAreaJabber" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < and > are invalid characters." ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> 
                                        </td>
                                    </tr>
                                    <tr id="trLync" runat="server" >
                                        <td width="32%" align="right">
                                        </td>
                                        <td width="18%" nowrap="nowrap">
                                            <input id="chkLync" type="checkbox" runat="server" />
                                            <img alt="Lync" src="../image/Lync.jpg"  title="Lync" /> <%--ZD 100419--%>
                                            <textarea class="altText" cols="20" rows="2" id="txtAreaLync" runat="server"></textarea>
                                            <a id="ancexpLync" href="#"  onclick="this.childNodes[0].click();return false;"><img id="expLync" src="../image/loc/nolines_plus.gif" alt="Expand/Collapse" style="width:25px; cursor:pointer" title="<%$ Resources:WebResources, UserMenuController_Options%>" runat="server" onclick="javascript:fnShowPcConfOpt(this,'tblLync')" /></a><%--ZD 100420--%>  <%--ZD 100369--%>
                                            <asp:RegularExpressionValidator ID="rgVdLync" ControlToValidate="txtAreaLync" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters33%>" ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> 
                                        </td>
                                    </tr>
                                    <tr id="trVidtel" runat="server" visible="false" > <%--ZD 102004--%>
                                        <td width="32%" align="right">
                                        </td>
                                        <td width="18%" nowrap="nowrap">
                                            <input id="chkVidtel" type="checkbox" runat="server" /> 
                                            <img alt="Vidtel" src="../image/Vidtel.jpg"  title="Vidtel" /><%--ZD 100419--%>
                                            <textarea class="altText" cols="20" rows="2" id="txtAreaVidtel" runat="server"></textarea>
                                            <a id="ancexpVidtel" href="#" onclick="this.childNodes[0].click();return false;"><img id="expVidtel" src="../image/loc/nolines_plus.gif" alt="Expand/Collapse" style="width:25px; cursor:pointer" title="<%$ Resources:WebResources, UserMenuController_Options%>" runat="server"  onclick="javascript:fnShowPcConfOpt(this,'tblVidtel')" /></a><%--ZD 100420--%>  <%--ZD 100369--%>
                                            <asp:RegularExpressionValidator ID="rgVdVidtel" ControlToValidate="txtAreaVidtel" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters33%>" ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> 
                                        </td>
                                    </tr>
                                    <tr id="trVidyo" runat="server" style="display:none" >
                                        <td width="20%" align="right">
                                            <input id="chkVidyo" type="checkbox" runat="server" /> 
                                            <img alt="Vidyo" src="../image/Vidyo.jpg" /><%--ZD 100419--%>
                                        </td>
                                        <td width="30%">
                                            <textarea class="altText" cols="20" rows="2" id="txtAreaVidyo" runat="server"></textarea>
                                            <asp:RegularExpressionValidator ID="rgVDVidyo" ControlToValidate="txtAreaVidyo" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters33%>" ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> 
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td width="47%">
                                <table width="100%" id="tblBlueJeans" style="display:none; border:1px solid black;">
                                    <tr>
                                        <td width="20%">
                                            <img alt="Bluejeans" src="../image/BlueJeans.jpg" /> <%--ZD 100419--%>
                                        </td>
                                        <td width="30%">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_Options%>" runat="server"></asp:Literal></td>
                                    </tr>
                                    <tr>
                                        <td class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_Skype%>" runat="server"></asp:Literal></td>
                                        <td>
                                            <input type="text" id="txtSkypeBJ" runat="server" />
                                            <asp:RegularExpressionValidator ID="rgVDSkypeBJ" ControlToValidate="txtSkypeBJ" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters33%>" ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_VideoConferenc%>" runat="server"></asp:Literal></td>
                                    </tr>
                                    <tr>
                                        <td class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_DialinIP%>" runat="server"></asp:Literal></td>
                                        <td>
                                            <input type="text" id="txtDialinIP" runat="server" />
                                            <asp:RegularExpressionValidator ID="rgExVdDialinIP" ControlToValidate="txtDialinIP" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters33%>" ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_MeetingID%>" runat="server"></asp:Literal></td>
                                        <td>
                                            <input type="text" id="txtMeetingVC" runat="server" />
                                            <asp:RegularExpressionValidator ID="rgExVdtxtMeetingVC" ControlToValidate="txtMeetingVC" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters33%>" ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_PhoneConferenc%>" runat="server"></asp:Literal></td>
                                    </tr>
                                    <tr>
                                        <td class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_DialinTollNum%>" runat="server"></asp:Literal></td>
                                        <td>
                                            <input type="text" id="txtTollBJ" runat="server" />
                                            <asp:RegularExpressionValidator ID="rgExVdtxtTollBJ" ControlToValidate="txtTollBJ" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters33%>" ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_DialinTollFre%>" runat="server"></asp:Literal></td>
                                        <td>
                                            <input type="text" id="txtTollFreeBJ" runat="server" />
                                            <asp:RegularExpressionValidator ID="rgExVdtxtTollFreeBJ" ControlToValidate="txtTollFreeBJ" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters33%>" ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_MeetingID%>" runat="server"></asp:Literal></td>
                                        <td>
                                            <input type="text" id="txtMeetingPC" runat="server" />
                                            <asp:RegularExpressionValidator ID="rgExVdtxtMeetingPC" ControlToValidate="txtMeetingPC" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters33%>" ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_Firsttimejoin%>" runat="server"></asp:Literal></td>
                                    </tr>
                                    <tr>
                                        <td class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_Fordetailedin%>" runat="server"></asp:Literal></td>
                                        <td>
                                            <input type="text" id="txtDetail" runat="server" />
                                            <asp:RegularExpressionValidator ID="rgVdDetail" ControlToValidate="txtDetail" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters33%>" ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> 
                                        </td>
                                    </tr>
                                </table>
                                <table width="100%" id="tblVidtel" style="display:none; border:1px solid black;">
                                    <tr>
                                        <td width="20%">
                                            <img alt="Vidtel" src="../image/Vidtel.jpg" /><%--ZD 100419--%>
                                        </td>
                                        <td width="30%">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_Options%>" runat="server"></asp:Literal></td>
                                    </tr>
                                    <tr>
                                        <td class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_Skype%>" runat="server"></asp:Literal></td>
                                        <td>
                                            <input type="text" id="txtSkypeVidtel" runat="server" />
                                            <asp:RegularExpressionValidator ID="rgExVdtxtSkypeVidtel" ControlToValidate="txtSkypeVidtel" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters33%>" ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_VideoConferenc%>" runat="server"></asp:Literal></td>
                                    </tr>
                                    <tr>
                                        <td class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_DialinSIP%>" runat="server"></asp:Literal></td>
                                        <td>
                                            <input type="text" id="txtDialinSIP" runat="server" />
                                            <asp:RegularExpressionValidator ID="rgExVdtxtDialinSIP" ControlToValidate="txtDialinSIP" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters33%>" ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_DialinH323%>" runat="server"></asp:Literal></td>
                                        <td>
                                            <input type="text" id="txtDialinH323" runat="server" />
                                            <asp:RegularExpressionValidator ID="rgExtxtDialinH323" ControlToValidate="txtDialinH323" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters33%>" ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_PIN%>" runat="server"></asp:Literal></td>
                                        <td>
                                            <input type="text" id="txtPinVc" runat="server" />
                                            <asp:RegularExpressionValidator ID="rgExtxtPinVc" ControlToValidate="txtPinVc" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters33%>" ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_PhoneConferenc%>" runat="server"></asp:Literal></td>
                                    </tr>
                                    <tr>
                                        <td class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_DialinTollNum%>" runat="server"></asp:Literal></td>
                                        <td>
                                            <input type="text" id="txtTollVidtel" runat="server" />
                                            <asp:RegularExpressionValidator ID="rgExtxtTollVidtel" ControlToValidate="txtTollVidtel" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters33%>" ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_DialinTollFre%>" runat="server"></asp:Literal></td>
                                        <td>
                                            <input type="text" id="txtTollFreeVidtel" runat="server" />
                                            <asp:RegularExpressionValidator ID="rgExtxtTollFreeVidtel" ControlToValidate="txtTollFreeVidtel" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters33%>" ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_PIN%>" runat="server"></asp:Literal></td>
                                        <td>
                                            <input type="text" id="txtPinPc" runat="server" />
                                            <asp:RegularExpressionValidator ID="rgExtxtPinPc" ControlToValidate="txtPinPc" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters33%>" ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> 
                                        </td>
                                    </tr>
                                </table>
                                <table width="100%" id="tblLync" style="display:none; border:1px solid black;">
                                    <tr>
                                        <td width="20%">
                                            <img alt="Lync" src="../image/Lync.jpg" /><%--ZD 100419--%>
                                        </td>
                                        <td width="30%">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_Options%>" runat="server"></asp:Literal></td>
                                    </tr>
                                    <tr>
                                        <td class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_Skype%>" runat="server"></asp:Literal></td>
                                        <td>
                                            <input type="text" id="txtSkypeLync" runat="server" />
                                            <asp:RegularExpressionValidator ID="rgExtxtSkypeLync" ControlToValidate="txtSkypeLync" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters33%>" ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_VideoConferenc%>" runat="server"></asp:Literal></td>
                                    </tr>
                                    <tr>
                                        <td class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_DialinIP%>" runat="server"></asp:Literal></td>
                                        <td>
                                            <input type="text" id="txtDialinIPLync" runat="server" />
                                            <asp:RegularExpressionValidator ID="rgExtxtDialinIPLync" ControlToValidate="txtDialinIPLync" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters33%>" ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_MeetingID%>" runat="server"></asp:Literal></td>
                                        <td>
                                            <input type="text" id="txtMeetingVCLync" runat="server" />
                                            <asp:RegularExpressionValidator ID="rgExtxtMeetingVCLync" ControlToValidate="txtMeetingVCLync" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters33%>" ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_PhoneConferenc%>" runat="server"></asp:Literal></td>
                                    </tr>
                                    <tr>
                                        <td class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_DialinTollNum%>" runat="server"></asp:Literal></td>
                                        <td>
                                            <input type="text" id="txtTollLync" runat="server" />
                                            <asp:RegularExpressionValidator ID="rgExtxtTollLync" ControlToValidate="txtTollLync" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters33%>" ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_DialinTollFre%>" runat="server"></asp:Literal></td>
                                        <td>
                                            <input type="text" id="txtTollFreeLync" runat="server" />
                                            <asp:RegularExpressionValidator ID="rgExtxtTollFreeLync" ControlToValidate="txtTollFreeLync" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters33%>" ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_MeetingID%>" runat="server"></asp:Literal></td>
                                        <td>
                                            <input type="text" id="txtMeetingPCLync" runat="server" />
                                            <asp:RegularExpressionValidator ID="rgExtxtMeetingPCLync" ControlToValidate="txtMeetingPCLync" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters33%>" ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_LyncMeeting%>" runat="server"></asp:Literal></td>
                                    </tr>
                                    <tr>
                                        <td class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_Fordetailedin%>" runat="server"></asp:Literal></td>
                                        <td>
                                            <input type="text" id="txtDetailLync" runat="server" />
                                            <asp:RegularExpressionValidator ID="rgExtxtDetailLync" ControlToValidate="txtDetailLync" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters33%>" ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> 
                                            
                                        </td>
                                    </tr>
                                </table>
                                <table width="100%" id="tblJabber" style="display:none; border:1px solid black;">
                                    <tr>
                                        <td width="20%">
                                            <img alt="Jabber" src="../image/Jabber.jpg" /><%--ZD 100419--%>
                                        </td>
                                        <td width="30%">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_Options%>" runat="server"></asp:Literal></td>
                                    </tr>
                                    <tr>
                                        <td class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_Skype%>" runat="server"></asp:Literal></td>
                                        <td>
                                            <input type="text" id="txtSkypeJB" runat="server" />
                                            <asp:RegularExpressionValidator ID="rgExtxtSkypeJB" ControlToValidate="txtSkypeJB" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters33%>" ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_VideoConferenc%>" runat="server"></asp:Literal></td>
                                    </tr>
                                    <tr>
                                        <td class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_DialinIP%>" runat="server"></asp:Literal></td>
                                        <td>
                                            <input type="text" id="txtDialinIPJB" runat="server" />
                                            <asp:RegularExpressionValidator ID="rgExtxtDialinIPJB" ControlToValidate="txtDialinIPJB" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters33%>" ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_MeetingID%>" runat="server"></asp:Literal></td>
                                        <td>
                                            <input type="text" id="txtMeetingVCJB" runat="server" />
                                            <asp:RegularExpressionValidator ID="rgExtxtMeetingVCJB" ControlToValidate="txtMeetingVCJB" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters33%>" ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_PhoneConferenc%>" runat="server"></asp:Literal></td>
                                    </tr>
                                    <tr>
                                        <td class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_DialinTollNum%>" runat="server"></asp:Literal></td>
                                        <td>
                                            <input type="text" id="txtTollJB" runat="server" />
                                            <asp:RegularExpressionValidator ID="rgExtxtTollJB" ControlToValidate="txtTollJB" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters33%>" ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_DialinTollFre%>" runat="server"></asp:Literal></td>
                                        <td>
                                            <input type="text" id="txtTollFreeJB" runat="server" />
                                            <asp:RegularExpressionValidator ID="rgExtxtTollFreeJB" ControlToValidate="txtTollFreeJB" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters33%>" ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_MeetingID%>" runat="server"></asp:Literal></td>
                                        <td>
                                            <input type="text" id="txtMeetingPCJB" runat="server" />
                                            <asp:RegularExpressionValidator ID="rgExtxtMeetingPCJB" ControlToValidate="txtMeetingPCJB" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters33%>" ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_JabberMeeting%>" runat="server"></asp:Literal></td>
                                    </tr>
                                    <tr>
                                        <td class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_Fordetailedin%>" runat="server"></asp:Literal></td>
                                        <td>
                                            <input type="text" id="txtDetailJB" runat="server" />
                                            <asp:RegularExpressionValidator ID="rgExtxtDetailJB" ControlToValidate="txtDetailJB" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters33%>" ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> 
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>  
            <%--FB 2693 - Ends--%> 
             <%--FB 1985 - Start--%>      
            <tr id="tdOtherSettings"  runat="server"><td align="center"><%--ZD 100164--%>
            <table style="width:100%;" border="0" cellspacing="3" cellpadding="2">  <%--FB 2611--%>
                <tr id="trLblAV" runat="server"> <%--<Code Modified For MOJ Phase2--%> 
                    <td align="Left">
                        <table cellspacing="5" style="width:100%; position:relative;" align="left"> <%--FB 2611--%>
                            <tr>
                                <td>
                                    <span style="margin-left:-35px;" class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_DetermineAudio%>" runat="server"></asp:Literal></span>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            <tr id="AVParams" runat="server">
                <%--Window Dressing--%>
                <td align="center" class="tableBody" > 
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" updatemode="always"> <%--ZD 104821 Starts--%>
                 <ContentTemplate>
                    <table style="width:100%;" cellpadding="2" cellspacing="3"> <%--FB 2611--%>
                        <tr>
                            <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_EndpointProfil%>" runat="server"></asp:Literal></td>
                            <td align="left">
                                <asp:TextBox ID="txtEndpointID" runat="server" Visible=false></asp:TextBox>
                                <asp:TextBox ID="txtEndpointName" runat="server" MaxLength="20" CssClass="altText"></asp:TextBox><%--FB 2523--%><%--FB 2995--%>
                                 <asp:RegularExpressionValidator ID="regConfName" ControlToValidate="txtEndpointName" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters19%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`\[\]{}\x22;=^:@$%&'~]*$"></asp:RegularExpressionValidator> <%--FB 1888--%><%--ZD 101865--%>
<%--                                <asp:RequiredFieldValidator ID="reqEPName" runat="server" ControlToValidate="txtEndpointName" ErrorMessage="Required" Display="dynamic"></asp:RequiredFieldValidator>
--%>                            </td>
                            <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_EndpointPasswo%>" runat="server"></asp:Literal></td>
                            <td align="left">
                                <asp:TextBox ID="txtEPPassword1" style="background-image:url('../image/pwdbg.png'); background-repeat:no-repeat" runat="server" TextMode="password" CssClass="altText" onblur="EPPasswordChange(1)" onfocus=" fnEPTextFocus(this.id,1)" ></asp:TextBox>
                                 <asp:RegularExpressionValidator ID="RegularExpressionValidator9" ControlToValidate="txtEPPassword1" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ ~ and &#34; are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=:#$%&()'~]*$"></asp:RegularExpressionValidator><%--FB 2319--%>
                                <asp:CompareValidator ID="CmpEP1" runat="server" ControlToCompare="txtEPPassword2"
                                    ControlToValidate="txtEPPassword1" Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, EnterPassword%>"></asp:CompareValidator>
                            </td>
                            <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_ConfirmPasswor%>" runat="server"></asp:Literal></td>
                            <td align="left"><asp:TextBox ID="txtEPPassword2" style="background-image:url('../image/pwdbg.png'); background-repeat:no-repeat" runat="server" TextMode="password" CssClass="altText" onblur="EPPasswordChange(2)" onfocus=" fnEPTextFocus(this.id,2)"></asp:TextBox>
                                <asp:CompareValidator ID="CmpEP2" runat="server" ControlToCompare="txtEPPassword1"
                                    ControlToValidate="txtEPPassword2" Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, PasswordNotMatch%>"></asp:CompareValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_AddressType%>" runat="server"></asp:Literal></td>
                            <td align="left">
                                <asp:DropDownList ID="lstAddressType" CssClass="altSelectFormat" DataTextField="Name" DataValueField="ID" runat="server" OnSelectedIndexChanged="ValidateTypes" AutoPostBack="true"></asp:DropDownList>
                            </td>
                            <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_DefaultIPISDN%>" runat="server"></asp:Literal></td>
                            <td align="left">
                                <asp:TextBox id="txtAddress" runat="server" CssClass="altText" ></asp:TextBox>
                                <%--FB 1972--%>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" ControlToValidate="txtAddress" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > + % \ / ( ) ? | ^ = ` [ ] { } $ and ~ are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+?|`\[\]{}\=$%&()~]*$"></asp:RegularExpressionValidator> <%--FB 2267--%>
                                <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator4" ControlToValidate="txtAddress" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ and &#34; are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>--%>
                            </td>
                            <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_DefaultEquipme%>" runat="server"></asp:Literal></td>
                            <td align="left">
                                <asp:DropDownList ID="lstVideoEquipment" CssClass="altSelectFormat" DataTextField="VideoEquipmentName" DataValueField="VideoEquipmentID" runat="server"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_DefaultLineRa%>" runat="server"></asp:Literal></td>
                            <td align="left">
                                <asp:DropDownList ID="lstLineRate" CssClass="altSelectFormat" DataTextField="LineRateName" DataValueField="LineRateID" runat="server"></asp:DropDownList>
                            </td>
                            <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_AssignedMCU%>" runat="server"></asp:Literal></td>
                            <td align="left">
                                <asp:DropDownList ID="lstBridges" CssClass="altSelectFormat" DataTextField="BridgeName" DataValueField="BridgeID" runat="server" OnSelectedIndexChanged="ChangeMCU"  AutoPostBack="true"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="reqBridges" Enabled="false" Display="dynamic" ControlToValidate="lstBridges" InitialValue="-1" ErrorMessage="Required for MPI." runat="server" SetFocusOnError="true" ></asp:RequiredFieldValidator>  
                            </td>
                            <%--ZD 104821 Starts--%>
                            <td align="left" class="blackblodtext"><asp:Label ID="SysLocation" Text="<%$ Resources:WebResources, SystemLocation%>" runat="server"></asp:Label></td>
                            <td align="left">
                                <asp:DropDownList ID="lstSystemLocation" CssClass="altSelectFormat" DataTextField="Name" DataValueField="SysLocId" runat="server"></asp:DropDownList>
                                <asp:TextBox CssClass="altText"  ID="txtSysLoc" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container, "DataItem.SysLocation") %>'></asp:TextBox>
                                <asp:DropDownList CssClass="altSelectFormat" ID="lstBridgeType" runat="server" DataTextField="BridgeType" DataValueField="BridgeID" Visible="false" ></asp:DropDownList> <%--ZD 100619--%>
                            </td>
                            <%--ZD 104821 Ends--%>
                        </tr>
                        <tr>
                           
                            <td align="left" class="blackblodtext"><asp:Literal ID="Literal014" Text="<%$ Resources:WebResources, ManageUserProfile_DefaultConnect%>" runat="server"></asp:Literal></td>
                            <td align="left">
                                <asp:DropDownList ID="lstConnectionType" CssClass="altSelectFormat" runat="server" DataTextField="Name" DataValueField="ID"></asp:DropDownList>
                            </td>
                            <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_Associatewith%>" runat="server"></asp:Literal></td>
                            <td align="left">
                                <asp:TextBox ID="txtAssociateMCUAddress" CssClass="altText" runat="server"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator10" ControlToValidate="txtAssociateMCUAddress" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters26%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>^+;?|`,\[\]{}\x22;=:@$%&()'~]*$"></asp:RegularExpressionValidator> <%--FB 2267--%>
                            </td>
                            <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_MCUAddressTyp%>" runat="server"></asp:Literal></td>
                            <td align="left">
                                <asp:DropDownList ID="lstMCUAddressType" CssClass="altSelectFormat" DataTextField="Name" DataValueField="ID" runat="server"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_DefaultProtoco%>" runat="server"></asp:Literal></td>
                            <td align="left">
                                <asp:DropDownList CssClass="altSelectFormat" ID="lstProtocol" runat="server" DataTextField="Name" DataValueField="ID"></asp:DropDownList>
                            </td>
                            <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_OutsideNetwork%>" runat="server"></asp:Literal></td>
                            <td align="left">
                                <!--[Vivek 29th Apr 2008] Defaulted Outside Network to No as per Issue No: 299-->
                            <%--Window Dressing--%>
                                <asp:DropDownList ID="lstIsOutsideNetwork" CssClass="altText" runat="server">
                                    <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>                                    
                                    <asp:ListItem Selected="True" Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_EncryptionPref%>" runat="server"></asp:Literal></td>
                            <td align="left">
                                <asp:DropDownList ID="lstEncryption" CssClass="altText" runat="server">
                                    <asp:ListItem Selected="True" Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                    <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_EmailID%>" runat="server"></asp:Literal></td>
                            <%-- ICAL Cisco Telepresence fix--%>
                            <td align="left">
                                <asp:TextBox CssClass="altText" ID="txtExchangeID" runat="server" Width="220px" TextMode="SingleLine"></asp:TextBox> <%-- ZD 100806--%>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" ControlToValidate="txtExchangeID"
                                    ValidationGroup="Submit" Display="Dynamic" runat="server" SetFocusOnError="true"
                                    ErrorMessage="<br>& < > ' + % \ ( ) ; ? | ^ = ! ` , [ ] { } # $ ~ and &#34; are invalid characters."
                                    ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>+;?|!`,\[\]{}\x22;=^#$%&()'~]*$"></asp:RegularExpressionValidator>
                            </td>
                            <%--API Port Starts--%>
                            <td align="left" class="blackblodtext">
                                <asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_APIPort%>" runat="server"></asp:Literal></td>

                            <td align="left" nowrap>
                                <asp:TextBox ID="txtApiportno" CssClass="altText" onblur="javascript:return IsNumeric()" MaxLength="5" runat="server"></asp:TextBox>
                                <label id="lblapierror" style="display:none;font-weight:normal" class="lblError" />
                            </td>
                            <%--API Port Ends--%>
                             <td align="left" class="blackblodtext"><asp:Literal ID="Literal15" Text="<%$ Resources:WebResources, ManageUserProfile_WebAccessURL%>" runat="server"></asp:Literal></td>
                            <td align="left">
                                <asp:TextBox ID="txtWebAccessURL" CssClass="altText" runat="server"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" ControlToValidate="txtWebAccessURL" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters3%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>+?|!`,;\[\]{}\x22;=@#$%&()'~]*$"></asp:RegularExpressionValidator>                                
                            </td>
                        </tr>
                        <%--ZD 101466 Starts--%>
                        <tr>
                        <%-- FB 1642 Audio add on- Starts --%>
                            <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_ConferenceCode%>" runat="server"></asp:Literal></td>
                            <td align="left">
                                <asp:TextBox cssclass="altText" id="txtConfCode" runat="server" MaxLength="15"></asp:TextBox> 
                                 <asp:RegularExpressionValidator ID="RegtxtConfCode" ControlToValidate="txtConfCode"
                                    Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, AudioCodeInvalidCharacter%>"
                                    ValidationExpression="^[^<>&+]*$" ValidationGroup="Submit"></asp:RegularExpressionValidator> <%--ZD 103540--%>
                            </td> 
                            <%-- FB 1642 Audio add on- End --%>                     
                            <td align="left" class="blackblodtext"><asp:Literal  Text="<%$ Resources:WebResources, ManageUserProfile_LeaderPin%>" runat="server"></asp:Literal></td>
                            <td align="left">
                                <asp:TextBox cssclass="altText" id="txtLeaderPin" runat="server" MaxLength="15"></asp:TextBox>                                   
                                <asp:RegularExpressionValidator ID="RegtxtLeaderPin" ControlToValidate="txtLeaderPin" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, AudioCodeInvalidCharacter%>"
                                 ValidationExpression="^[^<>&+]*$" ></asp:RegularExpressionValidator> <%--ZD 103540--%>                            
                            </td> 
                            <%--FB 2227--%>
                            <td align="left" class="blackblodtext"><asp:Literal  Text="<%$ Resources:WebResources, ParticipantCode%>" runat="server"></asp:Literal></td>
                            <td align="left">
                                <asp:TextBox cssclass="altText" id="txtPartyCode" runat="server" MaxLength="15"></asp:TextBox> 
                                <asp:RegularExpressionValidator ID="RegtxtPartyCode" ControlToValidate="txtPartyCode"
                                Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, AudioCodeInvalidCharacter%>"
                                ValidationExpression="^[^<>&+]*$" ></asp:RegularExpressionValidator> <%--ZD 103540--%>
                            </td> 
                        </tr>
                        <%--ZD 101446 Ends--%>
                        <%-- FB 1642 Audio add on- Starts --%> 
                        <tr>
                            <td align="left" class="blackblodtext">
                                <asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_InternalVideo%>" runat="server"></asp:Literal><%--FB 2611--%>
                            </td>
                            <td align="left" >
                                <asp:TextBox CssClass="altText" ID="txtIntvideonum" MaxLength="25" runat="server"></asp:TextBox>     
                                <asp:RegularExpressionValidator ID="regtxtIntvideonum" ControlToValidate="txtIntvideonum" 
                        Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters22%>"
                        ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>+?|!`,;\[\]{}\x22;=#$%&()'~]*$"></asp:RegularExpressionValidator>    <%--ZD 103540--%> <%--ALLBUGS-90--%>                      
                            </td>
                            <td align="left" class="blackblodtext">
                                <asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_ExternalVideo%>" runat="server"></asp:Literal> <%--FB 2611--%>
                            </td>
                            <td align="left" >
                                <asp:TextBox CssClass="altText" ID="txtExtvideonum" runat="server"></asp:TextBox>   
                                <asp:RegularExpressionValidator ID="regtxtExtvideonum" ControlToValidate="txtExtvideonum" 
                        Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters22%>"
                        ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>+?|!`,;\[\]{}\x22;=#$%&()'~]*$"></asp:RegularExpressionValidator> <%--ZD 103540--%>  <%--ALLBUGS-90--%>                          
                            </td> 
                            <%--FB 2227--%>
                        </tr>
                       
                       <%--FB 2262 //FB 2599--%>
                        <tr>
                            <%--<td align="right" class="blackblodtext"><%--FB 2481--%>
                                <%--Private VMR--%>
                            <%--</td>--%>
                            <%--<td align="left">
                                <asp:Button ID="btnPrivateVMR1" runat="server" Text="Manage" class="altLongBlueButtonFormat" /> 
                            </td>--%>
                            <td id="tdMeetlink" runat="server" visible="false" align="left" class="blackblodtext"> <%--FB 2834--%>
                                <asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_tdMeetlink%>" runat="server"></asp:Literal> <%--ZD 101714--%>
                            </td>
                            <td align="left" >
                                <asp:TextBox Visible="false" CssClass="altText" ID="txtVidyoURL" runat="server"></asp:TextBox>                               
                            </td> 
                            <td id="tdExten" Visible="false" runat="server" align="left" class="blackblodtext">
                                <asp:Literal ID="Literal11" Text="<%$ Resources:WebResources, ManageUserProfile_tdExten%>" runat="server"></asp:Literal>
                            </td>
                            <td align="left" >
                                <asp:TextBox Visible="false" CssClass="altText" ID="txtExtension" runat="server" MaxLength="50" ></asp:TextBox>                               
                            </td> 
                            
                        </tr>
                        <tr id="trpin" runat="server" visible="false">
                            <td align="left" class="blackblodtext">
                               <asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_Pin%>" runat="server"></asp:Literal>
                            </td>
                            <td align="left" >
                                <asp:TextBox CssClass="altText" ID="txtPin" MaxLength="25" runat="server"></asp:TextBox>  
                                <asp:CompareValidator ID="cmpPin" runat="server" ControlToCompare="txtConfirmPin"
                                   ControlToValidate="txtPin" Display="Dynamic" ErrorMessage="<br>Re-enter pin."></asp:CompareValidator>
                            </td>
                            <td align="left" class="blackblodtext">
                                <asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_ConfirmPin%>" runat="server"></asp:Literal>
                            </td>
                            <td align="left" >
                                <asp:TextBox CssClass="altText" ID="txtConfirmPin" runat="server" MaxLength="25"></asp:TextBox>   
                                <asp:CompareValidator ID="cmpConfirmPin" runat="server" ControlToCompare="txtPin"
                                    ControlToValidate="txtConfirmPin" Display="Dynamic" ErrorMessage="<br>Pin do not match."></asp:CompareValidator>
                            </td>
                            <td colspan="2"></td>
                         </tr>
                        <%--FB 2262 //FB 2599--%>
						 <%--FB 2481 Start--%>
                        <%--<tr>
                            <%--<td align="right" class="blackblodtext">
                                External VMR
                            </td>
                            <td align="left" >
                                <asp:Button ID="btnPrivateVMR" runat="server" Text="Manage" class="altLongBlueButtonFormat" /> 
                            </td>
                            <td colspan="4"></td>                           
                        </tr>--%>
                        
                        <%-- ZD 102535 Starts --%>
                        <tr>
                            <td align="left" class="blackblodtext" style="vertical-align:top" >
                                <asp:Literal ID="Literal12" Text="<%$ Resources:WebResources, ManageUserProfile_PrivateVMRCod%>" runat="server"></asp:Literal> <%--ZD 100806--%>
                            </td>
                            <td colspan="5">
                                <dxHE:ASPxHtmlEditor AccessibilityCompliant="true"  ID="PrivateVMR" runat="server" Height="200px">
                                    <SettingsImageUpload UploadImageFolder="~/image/maillogo/">
                                        <ValidationSettings MaxFileSize="100000" MaxFileSizeErrorText="Footer Image attachment is greater than 100KB. File has not been uploaded" />
                                    </SettingsImageUpload>
                                    <ClientSideEvents LostFocus="function(s,e){fnSetFocus();}" />
                                </dxHE:ASPxHtmlEditor>
                                <input type="file" id="fmMap" contenteditable="false" size="50" class="altText" runat="server"
                                    visible="false" />
                                <input type="hidden" id="fmMapImage" name="Map1ImageDt" runat="server" height="21%"
                                    style="display: none" /><%--FB 1982 --%>
                            </td>
                        </tr>
                        <%-- ZD 102535 Ends --%>

                        <%--FB 2481 End--%>
                        <%-- FB 1642 Audio add on- End --%> 
                    </table>
                    </ContentTemplate>
                    </asp:UpdatePanel>  <%-- ZD 104821 Ends--%>   
                </td>
            </tr>
            <%--Tickers Start--%>
            <tr> 
                <td align="Left">
                    <table cellspacing="5" style="width:100%; margin-left:-30px; position:relative"> <%--FB 2611--%>
                        <tr>
                            <%--<td style="width:20">&nbsp;</td>--%>
                            <td>
                                <span style="margin-left:-4px;" class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_TickerSettings%>" runat="server"></asp:Literal></span>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="Tr2" runat="server">
                <td align="center" class="tableBody"> 
                    <table style="width:100%" cellpadding="2" cellspacing="3"><%--FB 2611--%>
                        <tr>
                            <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_TickerStatus%>" runat="server"></asp:Literal></td>
                             <td align="left">
                                <asp:DropDownList ID="drpTickerStatus" CssClass="altText" runat="server">
                                    <asp:ListItem  Value="0" Text="<%$ Resources:WebResources, Show%>"></asp:ListItem>                                    
                                    <asp:ListItem  Selected="True" Value="1" Text="<%$ Resources:WebResources, Hide%>"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_TickerPosition%>" runat="server"></asp:Literal></td>
                            <td align="left">
                             <asp:DropDownList ID="drpTickerPosition" CssClass="altText" runat="server">
                                    <asp:ListItem Selected="True" Value="0" Text="<%$ Resources:WebResources, Top%>"></asp:ListItem>                                    
                                    <asp:ListItem  Value="1" Text="<%$ Resources:WebResources, Bottom%>"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_TickerSpeed%>" runat="server"></asp:Literal></td>
                            <td align="left">
                            <asp:DropDownList ID="drpTickerSpeed" CssClass="altText" runat="server">
                                    <asp:ListItem  Value="3" Text="<%$ Resources:WebResources, Slow%>"></asp:ListItem>                                    
                                    <asp:ListItem   Value="6" Text="<%$ Resources:WebResources, Medium%>"></asp:ListItem>
                                    <asp:ListItem  Value="18" Text="<%$ Resources:WebResources, Fast%>"></asp:ListItem>
                            </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_TickerBackgrou%>" runat="server"></asp:Literal></td>
                            <td align="left">
                                <asp:TextBox ID="txtTickerBknd" CssClass="altText" runat="server" EnableViewState="True"></asp:TextBox>
                                <a href="" onkeydown="if(event.keyCode == 13){ show_RGBPalette('txtTickerBknd',event); return false; }"><IMG runat="server" title="<%$ Resources:WebResources, SelectColor%>" onclick="show_RGBPalette('txtTickerBknd',event);return false;"
					                height="23" alt="Color Palette" src="../Image/color.jpg" width="27" name="imggen" style="vertical-align:middle"></a><%--ZD 100420--%>
					               
			                </td>
                             <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_TickerDisplay%>" runat="server"></asp:Literal></td>
                            <td align="left">
                               <asp:DropDownList ID="drpTickerDisplay" CssClass="altText" runat="server" onclick="javascript:fnShowFeed();">
                                    <asp:ListItem  Selected="True"  Value="0" Text="<%$ Resources:WebResources, MyConferences%>"></asp:ListItem>                                    
                                    <asp:ListItem  Value="1" Text="<%$ Resources:WebResources, RSSfeed%>"></asp:ListItem>
                            </asp:DropDownList>
                            </td>
                            <td align="left" class="blackblodtext" id="feedLink" runat="server" style="display:none;"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_feedLink%>" runat="server"></asp:Literal></td>
                            <td align="left">
                                <asp:TextBox ID="txtFeedLink" CssClass="altText" runat="server"></asp:TextBox>
                                
                                
                            </td>
                          </tr>
                           <tr>
                          <td colspan="6" align="left">
                          <asp:RegularExpressionValidator ID="RegularExpressionValidator6" ControlToValidate="txtFeedLink" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + %  | = ! ` , [ ] { }  # $ ~ and &#34; are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^<>+|!`,[\]{};=#$%&~]*$"></asp:RegularExpressionValidator></td>
                          </tr>
                    </table>
                </td>
            </tr>
            <tr> 
                <td align="left">
                    <table cellspacing="5" style="width:100%; margin-left:-30px; position:relative"><%--FB 2611--%>
                        <tr>
                            <%--<td style="width:20">&nbsp;</td>--%>
                            <td>
                                <span style="margin-left:-4px;" class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_TickerSettings1%>" runat="server"></asp:Literal></span>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="Tr1" runat="server">
                <td align="center" class="tableBody"> 
                    <table style="width:100%" cellpadding="2" cellspacing="3" border="0"> <%--FB 2611--%>
                        <tr>
                            <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_TickerStatus%>" runat="server"></asp:Literal></td>
                             <td align="left">
                                <asp:DropDownList ID="drpTickerStatus1" CssClass="altText" runat="server">
                                    <asp:ListItem  Value="0" Text="<%$ Resources:WebResources, Show%>"></asp:ListItem>                                    
                                    <asp:ListItem  Selected="True" Value="1" Text="<%$ Resources:WebResources, Hide%>"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_TickerPosition%>" runat="server"></asp:Literal></td>
                            <td align="left">
                             <asp:DropDownList ID="drpTickerPosition1" CssClass="altText" runat="server">
                                    <asp:ListItem Selected="True" Value="0" Text="<%$ Resources:WebResources, Top%>"></asp:ListItem>                                    
                                    <asp:ListItem  Value="1" Text="<%$ Resources:WebResources, Bottom%>"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_TickerSpeed%>" runat="server"></asp:Literal></td>
                            <td align="left">
                            <asp:DropDownList ID="drpTickerSpeed1" CssClass="altText" runat="server">
                                    <asp:ListItem  Value="3" Text="<%$ Resources:WebResources, Slow%>"></asp:ListItem>                                    
                                    <asp:ListItem  Selected="True" Value="6" Text="<%$ Resources:WebResources, Medium%>"></asp:ListItem>
                                    <asp:ListItem  Value="18" Text="<%$ Resources:WebResources, Fast%>"></asp:ListItem>
                            </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_TickerBackgrou%>" runat="server"></asp:Literal></td>
                            
                            <td align="left">
                                <asp:TextBox ID="txtTickerBknd1" CssClass="altText" runat="server" EnableViewState="True"></asp:TextBox>
                                <a href="" onkeydown="if(event.keyCode == 13){ show_RGBPalette('txtTickerBknd',event);return false; }"><IMG title="<%$ Resources:WebResources, SelectColor%>" runat="server" onclick="show_RGBPalette('txtTickerBknd1',event);return false;"
					                height="23" alt="Color" src="../Image/color.jpg" width="27" name="imggen" style="vertical-align:middle"></a><%--ZD 100420--%>
					                
			                </td>
                             <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_TickerDisplay%>" runat="server"></asp:Literal></td>
                            <td align="left">
                               <asp:DropDownList ID="drpTickerDisplay1" CssClass="altText" runat="server" onclick="javascript:fnShowFeed();">
                                    <asp:ListItem  Selected="True"  Value="0" Text="<%$ Resources:WebResources, MyConferences%>"></asp:ListItem>                                    
                                    <asp:ListItem  Value="1" Text="<%$ Resources:WebResources, RSSfeed%>"></asp:ListItem>
                            </asp:DropDownList>
                            </td>
                            <td align="left" class="blackblodtext" id="feedLink1" runat="server"><asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_feedLink1%>" runat="server"></asp:Literal></td>
                            <td align="left">
                                <asp:TextBox ID="txtFeedLink1" CssClass="altText" runat="server" style="display:none;"></asp:TextBox>
                            </td>
                          </tr>
                          <tr>
                          <td colspan="6" align="left">
                          &nbsp;&nbsp;&nbsp;<asp:RegularExpressionValidator ID="RegularExpressionValidator5" ControlToValidate="txtFeedLink1" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + %  | = ! ` , [ ] { }  # $ ~ and &#34; are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^<>+|!`,[\]{};=#$%&~]*$"></asp:RegularExpressionValidator></td>
                          </tr>
                    </table>
                </td>
            </tr>
             <%--Tickers End--%>
             </table></td></tr> <%--FB 1985 - End--%>
            <tr>
                <td style="height:30">&nbsp;</td>
            </tr>            
            <tr>
                <td align="center">
                    <table style="width:100%" border="0" ><%--FB 2611--%>
                        <tr>
                            <td style="width:15%" align="center" id="tdreset" runat="server"><%--FB 2611--%><%--FB 2565 Start--%>
                                <asp:Button runat="server" id="btnReset" text="<%$ Resources:WebResources, Reset%>" validationgroup="Reset" onclick="BindData" cssclass="altLongBlueButtonFormat" onclientclick="DataLoading('1')"></asp:Button>  <%--OnClientClick="javascript:testConnection();return false;"--%> <%--ZD 100176--%>                                
                            </td>
                            <td style="width:30%;" align="center" id="tdcancel" runat="server"><%--FB 2679--%>
                            <input type="button" id="btnCancel"  runat="server" class="altLongBlueButtonFormat" value="<%$ Resources:WebResources, Cancel%>" onclick="fnCancel()" OnClientClick="DataLoading('1')"/><%--ZD 100176--%>  
                            </td>
                            <%-- FB 2025 starts --%>
                            <td style="width:30%;" align="center" id="tdnewsubmit1" runat="server"> <%--FB 2611--%><%--FB 2679--%>
                            <asp:Button runat="server" id="btnnewSubmit" onclientclick="javascript:return RulePassword()" onclick="SubmitNewUser" width="195pt" text="<%$ Resources:WebResources, ManageUserProfile_btnnewSubmit%>"></asp:Button><%--FB 2339 FB 2796--%>
                            </td><%-- FB 2025 end --%>
                            <td style="width:20%" align="center" id="tdsubmit" runat="server"> <%--FB 2611--%>
                                <asp:Button id="btnSubmit" onclientclick="javascript:return RulePassword();" onclick="SubmitUser" width="150pt" runat="server" text="<%$ Resources:WebResources, ManageUserProfile_btnSubmit%>"></asp:Button><%--API Port--%> <%--FB 2339 FB 2796--%><%--ALLBUGS-90--%>
                            </td><%--FB 2565 End--%>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
<script language="javascript">

if(document.getElementById('ChkWebexConf') !=null) //ZD 100923
    fnShowWebExConf(document.getElementById('ChkWebexConf').checked); // ZD 100369 508 Issues
if(document.getElementById('ChkBJNConf') !=null) //ZD 103933
    fnShowBJNConf(document.getElementById('ChkBJNConf').checked);//ZD 104021
if(document.getElementById('chkPcConf')!=null) //ZD 100923
    fnShowPcConf(document.getElementById('chkPcConf').checked);
    
    SavePassword();
//FB 1830 - DeleteEmailLang start
    function fnDelEmailLan() {
        if (document.getElementById("txtEmailLang").value == "")
            return false;
        else
            return true;
    }
//FB 1830 - DeleteEmailLang end

    //FB 1985 - Start
    function fnShowHideAVLink()
	{
	  
	    var args = fnShowHideAVLink.arguments;
	    var obj = eval(document.getElementById("LnkAVExpand"));
	  
	    if(obj)
	    {
	       
	        obj.style.display = 'none';
	        if(args[0] == '1')
	        {
	            obj.style.display = '';
	        }
	        
	    }
	}
	
	function fnShowAVParams()
	{
	    //var obj = eval(document.getElementById("tdrssFeed"));
	    var obj1 = eval(document.getElementById("tdOtherSettings"));
	    var linkState = eval(document.getElementById("hdnAVParamState"));
	    var expandlink = eval(document.getElementById("LnkAVExpand"));
	    
	    if(linkState)
	    {
	        //if(obj && obj1)
	        if(obj1)
	        {
	            //obj.style.display = 'none';
	            obj1.style.display = 'none';
	            if(linkState.value == '')
	            {
	                //obj.style.display = '';
	                obj1.style.display = '';
	                linkState.value = '1';
	                
	                if(expandlink)
	                {
	                    expandlink.innerHTML = 'Collapse';
	                }
	            }
	            else
	            {
	                //obj.style.display = 'none';
	                obj1.style.display = 'none';
	                linkState.value = '';
	                if(expandlink)
	                {
	                    expandlink.innerHTML = 'Expand';
	                }
	            }
	        }
	     }
	     return false;
	}
	//FB 1985 - End

	function AddRemoveHelpReq(opr)//FB 2268
	{
	    var lstHelpReq = document.getElementById("lstHelpReqEmail");
	    var txtHelpReq = document.getElementById("txtHelpReqEmail");
	    var hdnHelpReq = document.getElementById("hdnHelpReq");

	    if (!Page_ClientValidate())
	        return Page_IsValid;
	    
	    if (opr == "Rem")
	    {
	        var i;
	        for (i = lstHelpReq.options.length - 1; i >= 0; i--) {
	            if (lstHelpReq.options[i].selected)
	            {
	                hdnHelpReq.value = hdnHelpReq.value.replace(lstHelpReq.options[i].text, "").replace(/��/i, "�");
	                lstHelpReq.remove(i);
	            }
	        }
	    }
	    else if (opr == "add")
	    {
	        if (txtHelpReq.value.replace(/\s/g, "") == "") //trim the textbox
	            return false;
	            
	        if (lstHelpReq.options.length >= 5) {
	            document.getElementById("errLabel").innerHTML = "Maximum 5 Emails";
	            document.getElementById("errLabel").className = "lblError";//FB 2487
	            document.getElementById("errLabel").focus();
	            return false;
	        }
	        else {
	            if (hdnHelpReq.value.indexOf(txtHelpReq.value) >= 0) 
	            {
	                document.getElementById("errLabel").innerHTML = "Already Added Email address";
	                document.getElementById("errLabel").className = "lblError";//FB 2487
	                return false;
	            }
	        }
	        
	        if (lstHelpReq.options.length > 0)
	            hdnHelpReq.value = hdnHelpReq.value + "�";

	        var option = document.createElement("Option");
	        option.text = txtHelpReq.value;
	        option.title = txtHelpReq.value;
	        lstHelpReq.add(option);
	        hdnHelpReq.value = hdnHelpReq.value + txtHelpReq.value;
	        
	        txtHelpReq.value = "";
	        txtHelpReq.focus();
	    }

	    return false;
	}
	
	//FB 2348 Start
    if("<%=Session["EnableSurvey"]%>" == "1")
    {
        document.getElementById("tdSurveyEmail").style.visibility = "visible";
        document.getElementById("tddrpSurveyEmail").style.visibility = "visible";
    }
	//FB 2348 End
	//FB 2481 Start
	if(document.getElementById("dxHTMLEditor_TD_T0_DXI15_Img"))
	       document.getElementById("dxHTMLEditor_TD_T0_DXI15_Img").style.display = "none";
	//FB 2481 End
</script>
    </form>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
<%--ZD 101388 Commented--%>
<%--<!-- FB 2719 Starts -->
<% if (Session["isExpressUser"].ToString() == "1"){%>
<!-- #INCLUDE FILE="inc/mainbottomNETExp.aspx" -->
<%}else{%>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
<%}%>
<!-- FB 2719 Ends -->--%>
</body>
</html>
<%--FB 2491 Start--%>
<script type="text/javascript">
    // FB 2693 Starts
    function fnIEuiAlign() {
        var agent = navigator.userAgent;
        if (agent.indexOf("Trident/6.0") != -1) {
            // IE 10
            if (document.getElementById("chkPcConf") != null && document.getElementById("tblPcConf") != null) { //ZD 100873
                document.getElementById("chkPcConf").style.marginLeft = "2px";
                document.getElementById("tblPcConf").style.marginLeft = "-22px";
            }
        }
        else if (agent.indexOf("MSIE") != -1) {
        // IE 6 7 8 9
        if (document.getElementById("chkPcConf") != null && document.getElementById("tblPcConf") != null) { //ZD 100873
            document.getElementById("chkPcConf").style.marginLeft = "6px";
            document.getElementById("tblPcConf").style.marginLeft = "-6px";
        }
        }
    }
    // FB 2693 Ends
    
function fnScrollTop() 
    {
       fnIEuiAlign(); // FB 2693
       window.scrollTo(0, 0);
       document.getElementById('hideScreen').style.display = "none";
    }
    setTimeout("fnScrollTop()", 1);
    //FB 2982 start
    var isIE = navigator.appVersion.indexOf("MSIE 8.0");
    var LstTimeZone = document.getElementById("lstTimeZone");
    
    function SetWidthToAuto(LstTimeZone) {
        if (isIE > -1) {
            LstTimeZone.style.width = 'auto';
        }
    }
    function ResetWidth(lstTimeZone) {
        if (isIE > -1) {
            LstTimeZone.style.width = '172px';
        }
    }
    //FB 2982 End

    // ZD 100263
    var obj1 = document.getElementById("txtPassword1");
    var obj2 = document.getElementById("txtPassword2");

    if ((obj1.value != "" && obj2.value != "") || '<%=Session["ModifiedUserID"]%>' == 'new') {
        document.getElementById("txtPassword1").style.backgroundImage = "";
        document.getElementById("txtPassword2").style.backgroundImage = "";
    }
    
    //ZD 100420 - Start
    document.getElementById('reglstLanguage').setAttribute("onblur", "document.getElementById('btnDefine').focus()");
    document.getElementById('btnDefine').setAttribute("onfocus", "");
    document.getElementById('hdntext').setAttribute("onblur", "document.getElementById('btnlogin').focus()");
    document.getElementById('btnlogin').setAttribute("onfocus", "");
    document.getElementById('txtExtvideonum').setAttribute("onblur", "document.getElementById('btnPrivateVMR').focus()");
    document.getElementById('btnPrivateVMR').setAttribute("onfocus", "");
    document.getElementById('drpTickerDisplay1').setAttribute("onblur", "document.getElementById('btnReset').focus()");
    document.getElementById('btnReset').setAttribute("onfocus", "");
    document.getElementById('btnReset').setAttribute("onblur", "document.getElementById('btnCancel').focus()");
    document.getElementById('btnCancel').setAttribute("onfocus", "");
    document.getElementById('btnCancel').setAttribute("onblur", "document.getElementById('btnnewSubmit').focus()");
    document.getElementById('btnnewSubmit').setAttribute("onfocus", "");
    document.getElementById('btnnewSubmit').setAttribute("onblur", "document.getElementById('btnSubmit').focus()");
    document.getElementById('btnSubmit').setAttribute("onfocus", "");
    //ZD 100420 - End
    
</script>
<%--FB 2491 End--%>

<%--ZD 100428 START--%>
<script language="javascript" type="text/javascript">
//ZD 100369
    if (document.getElementById('BtnBlockEmails') != null) {
        if (document.getElementById('ChkBlockEmails') != null)
            document.getElementById('ChkBlockEmails').setAttribute("onblur", "document.getElementById('BtnBlockEmails').focus(); document.getElementById('BtnBlockEmails').setAttribute('onfocus', '');");
    }

    document.onkeydown = function(evt) {
        evt = evt || window.event;
        var keyCode = evt.keyCode;
        if (keyCode == 8) {
            if (document.getElementById("btnCancel") != null) { // backspace
                var str = document.activeElement.type;
                if (!(str == "text" || str == "textarea" || str == "password")) {
                    document.getElementById("btnCancel").click();
                    return false;
                }
            }
            if (document.getElementById("btnGoBack") != null) { // backspace
                var str = document.activeElement.type;
                if (!(str == "text" || str == "textarea" || str == "password")) {
                    document.getElementById("btnGoBack").click();
                    return false;
                }
            }
        }
        fnOnKeyDown(evt);
    };

    function EscClosePopup() {
            if (document.getElementById('PopupPrivateVMRPanel') != null &&
                    document.getElementById('btnPrivateVMR_backgroundElement') != null) {
                document.getElementById('PopupPrivateVMRPanel').style.display = "none";
                document.getElementById('btnPrivateVMR_backgroundElement').style.display = "none";
            }
    }

    var isIE = false; //ZD 100420
    if (navigator.userAgent.indexOf('Trident') > -1)
        isIE = true;
    if (!isIE) {
        //ZD 104021
    //if(document.getElementById('ancexpBlueJeans')!= null)
        //document.getElementById('ancexpBlueJeans').setAttribute("onmouseup", "this.childNodes[0].click();return false;");
    if (document.getElementById('ancexpJabber') != null)
        document.getElementById('ancexpJabber').setAttribute("onmouseup", "this.childNodes[0].click();return false;");
    if (document.getElementById('ancexpLync') != null)
        document.getElementById('ancexpLync').setAttribute("onmouseup", "this.childNodes[0].click();return false;");
    if (document.getElementById('ancexpVidtel') != null)
        document.getElementById('ancexpVidtel').setAttribute("onmouseup", "this.childNodes[0].click();return false;");
}
var glob = false;
function fnSetFocus() {
    if (navigator.userAgent.indexOf('Trident') > -1 && glob)
        document.getElementById("PrivateVMR_TC_T1T").focus();
    glob = true;
    document.getElementById("PrivateVMR_FFI").setAttribute("tabindex","-1");
}

// ZD 100580 START
var selVal;
function fnHandleChangeLang() {
    if (document.getElementById("delEmailLang") != null) {
        var stat = confirm(UserLanguage);
        if (stat) {
            document.getElementById("delEmailLang").click();
        }
        else {
            document.getElementById("lstLanguage").selectedIndex = selVal;
        }
    }
}
selVal = document.getElementById("lstLanguage").selectedIndex;
//ZD 100580 END

function CancelClicked() {
    EscClosePopup();
}

</script>