<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.		
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 ZD 100886 End--%>
<%@ Page Language="C#" Async="true" AutoEventWireup="true" EnableEventValidation="false" Inherits="PersonalCalendar" ValidateRequest="false" %><%-- ZD 100170 --%>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="DayPilot" Namespace="DayPilot.Web.Ui" TagPrefix="DayPilot" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Assembly="DevExpress.Web.v10.2, Version=10.2.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dxtc"%>
<%@ Register Assembly="DevExpress.Web.v10.2, Version=10.2.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxClasses" tagprefix="dxw"%>
<%@ Register Assembly="MetaBuilders.WebControls.ComboBox" Namespace="MetaBuilders.WebControls" TagPrefix="mbcbb" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=8" /> <!-- FB 2050 -->
<!-- ZD 101022 -->
<script src="script/CallMonitorJquery/jquery.1.4.2.js" type="text/javascript"></script>
<script src="../i18n/i18Utils.js" type="text/javascript" ></script>
<style type="text/css">
    #ModalDiv
    {
        opacity:0.5;
        filter: alpha(opacity=50);
    }
</style>

<%
    if(Session["userID"] == null)
    {
        Response.Redirect("genlogin.aspx");

    }
    else
    {
        Application.Add("COM_ConfigPath", "C:\\VRMSchemas_v1.8.3\\ComConfig.xml");
        Application.Add("MyVRMServer_ConfigPath", "C:\\VRMSchemas_v1.8.3\\");
    }
%>

<% 
Boolean showCompanyLogo = false;
if(Request.QueryString["hf"] != null)
{
    if(Request.QueryString["hf"].ToString() == "1")
    {
	    showCompanyLogo = false;
%>
    <%--<!-- #INCLUDE FILE="inc/maintop2.aspx" --> --%> <%--ZD 101714--%>
<%	    
      if(Session["browser"].ToString().Contains("Netscape") || Session["browser"].ToString().Contains("Firefox"))
      { 
%>
		<link rel="stylesheet" title="Expedite base styles" type="text/css" media="all" href="css/aqua/theme.css" title="Aqua" />
	<% }%>
<%
    }
    else 
    {
%>
    <!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
    <%--ZD 101388 Commented--%>
    <%--<!-- FB 2719 Starts -->
    <% if (Session["isExpressUser"].ToString() == "1"){%>
    <!-- #INCLUDE FILE="inc/maintopNETExp.aspx" -->
    <%}else{%>
    <!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
    <%}%>
    <!-- FB 2719 Ends -->--%>
<%    
         if(Session["browser"].ToString().Contains("Netscape") || Session["browser"].ToString().Contains("Firefox"))
        {
%>
    		<%--<link rel="stylesheet" title="Expedite base styles" type="text/css" media="all" href="css/aqua/theme.css" title="Aqua" />--%> <%--FB 1982--%>
     <% }%>
<% }

}%>

<%--FB 3055-URL Starts--%>
    <script>
    var ar = window.location.href.split("/");
    var pg = ar[ar.length - 1];
    var page = pg.toLowerCase();
    var cnt = 0;

    if (page != "personalcalendar.aspx?v=1&r=1&hf=&m=&pub=&d=&comp=&f=v")
        cnt++;
    if (page != "personalcalendar.aspx?type=d&v=1&r=1&hf=&d=")
        cnt++;
    if (page != "personalcalendar.aspx?v=1&r=1&hf=&d=")
        cnt++;
    
    if(cnt == 3)
        window.location.href = "thankyou.aspx"

    function fnTimeScroll() {

        fnHidelbl();
        if (document.getElementById('CalendarContainer_schDaypilot') != null) {
            document.getElementById('CalendarContainer_schDaypilot').childNodes[1].style.height = '300px';
            document.getElementById('CalendarContainer_schDaypilot').childNodes[1].style.overflowX = 'hidden';
            document.getElementById('CalendarContainer_schDaypilot').childNodes[1].style.overflowY = 'auto';
        }
        if (document.getElementById('CalendarContainer_schDaypilotweek') != null) {
            document.getElementById('CalendarContainer_schDaypilotweek').childNodes[1].style.height = '300px';            
            document.getElementById('CalendarContainer_schDaypilotweek').childNodes[1].style.overflowX = 'hidden';
            document.getElementById('CalendarContainer_schDaypilotweek').childNodes[1].style.overflowY = 'auto';

            var offset = document.getElementById("CalendarContainer_schDaypilotweek").childNodes[1].offsetHeight;
            var scroll = document.getElementById("CalendarContainer_schDaypilotweek").childNodes[1].scrollHeight;
            if(scroll > offset)
                document.getElementById('CalendarContainer_schDaypilotweek').childNodes[0].style.marginRight = '16px';                        
        }

        var StartHr;
        var n = 0;
        var d = new Date();
        var t = d.getHours();
        if (document.getElementById("lstStartHrs_Text") != null)
            StartHr = document.getElementById("lstStartHrs_Text").value.toLowerCase();

        if (StartHr.indexOf("z") > -1)
            n = parseInt(StartHr.substring(0, 2), 10);

        else if (StartHr.indexOf("am") > -1 || StartHr.indexOf("pm") > -1) {
            if (StartHr.indexOf("12") > -1 && StartHr.indexOf("am") > -1)
                n = 0;
            else
                n = parseInt(StartHr.substring(0, 2), 10);

            if (StartHr.indexOf("pm") > -1)
                n += 12;
        }
        else {
            n = parseInt(StartHr.substring(0, 2), 10);
        }

        var pos;

        //ALLDEV-697 - Start
        if (document.getElementById("officehrDaily").checked == true)
            pos = (t * 40) - (n * 40);
        else
            pos = t * 40;

        pos -= 80;
        //ALLDEV-697 - End
        var dv; //ZD 101055
        var dv1;
        var dv2;
        //ZD 101055
        if (document.getElementById('CalendarContainer_schDaypilot') != null && document.getElementById('CalendarContainer_schDaypilot').childNodes[1] != null)
            dv = document.getElementById('CalendarContainer_schDaypilot').childNodes[1];
        if (document.getElementById('CalendarContainer_schDaypilotweek') != null && document.getElementById('CalendarContainer_schDaypilotweek').childNodes[1] != null)
            dv1 = document.getElementById('CalendarContainer_schDaypilotweek').childNodes[1];
        if (document.getElementById('CalendarContainer_schDaypilotweek') != null && document.getElementById('CalendarContainer_schDaypilot').childNodes[1] != null)
            dv2 = document.getElementById('CalendarContainer_schDaypilot').childNodes[1];

        //if (pos > 80)
            //pos += 15;

        //ZD 101055
        if (dv != null && dv != undefined) { //ZD 100152 //ZD 101388
            dv.scrollTop = pos;
        }
        if (dv1 != null && dv1 != undefined) { 
            dv1.scrollTop = pos;
        }
        if (dv2 != null && dv2 != undefined) { 
            dv2.scrollTop = pos;
        }
        //alert("n: " + n + ", pos:" + pos);
    }
        
    </script>
<%--FB 3055-URL End--%>
    
<link rel="StyleSheet" href="css/divtable.css" type="text/css" />


<script type="text/javascript" src="script/errorList.js"></script>
<script type="text/javascript" language="JavaScript" src="inc/functions.js"></script>
<script type="text/javascript" src="extract.js"></script>
<script type="text/javascript" src="script/mousepos.js"></script> <%-- ZD 102723 --%>
<script type="text/javascript" src="script/showmsg.js"></script> <%-- ZD 102723 --%>

<%--FB 1861--%>
<%--<link rel="stylesheet" type="text/css" media="all" href="css/aqua/theme.css" title="Aqua" />--%>

<script type="text/javascript" src="script/mytreeNET.js"></script>
<script type="text/javascript">    // FB 2790
    var path = '<%=Session["OrgCSSPath"]%>';
    path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css"; // FB 2815
    document.write("<link rel='stylesheet' title='Expedite base styles' type='text/css' href='" + path + "'?'" + new Date().getTime() + "'>"); //ZD 100152
</script>
<link rel="stylesheet" type="text/css" media="all" href="css/calendar-blue.css" /> <%--FB 1861--%> <%--FB 1982--%>

<script type="text/javascript">

  var servertoday = new Date(parseInt("<%=DateTime.Now.Year%>", 10), parseInt("<%=DateTime.Now.Month%>", 10)-1, parseInt("<%=DateTime.Now.Day%>", 10),
  parseInt("<%=DateTime.Now.Hour%>", 10), parseInt("<%=DateTime.Now.Minute%>", 10), parseInt("<%=DateTime.Now.Second%>", 10));
  
</script>
<script type="text/javascript" language="JavaScript" src="inc/disablerclick.js"></script>
<script type="text/javascript" src="script/cal-flat.js"></script>
<script type="text/javascript" src="../<%=Session["language"] %>/lang/calendar-en.js" ></script> <%--ZD 101714--%>
<script type="text/javascript" src="script/calendar-flat-setup.js"></script>
<script language="javascript1.1" src="script/calview.js"></script>
<html id="Html1" runat="server"  xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title><asp:Literal Text="<%$ Resources:WebResources, PersonalCalendar_PersonalCalend%>" runat="server"></asp:Literal></title>
</head>
<body>

  <form id="frmPersonalCalendar" runat="server" >
  <div id="ModalDiv" style="z-index:1100; position:fixed; left:0px; top:0px; width:1366px; height:768px; display:none; background-color:White" ></div>
   <%--ZD 101022 start--%>
        <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" >
		    <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		    </Scripts>
	    </asp:ScriptManager> <%--ZD 101022 End--%>
  
    <input type="hidden" id="cmd" value="GetSettingsSelect" />
    <input type="hidden" id="settings2locstr" value="" />
    <input type="hidden" id="helpPage" value="79" />                       
    <input type="hidden" id="IsMonthChanged" runat="server" />
    <input type="hidden" id="IsWeekOverLap" runat="server" />
    <input type="hidden" id="IsWeekChanged" runat="server" />                        
    <input type="hidden" id="MonthNum"  />
    <input type="hidden" id="YrNum"  />
    <input type="hidden" id="Weeknum"  />
    <input type="hidden" runat="server" id="hdnStartTime" name="hdnStartTime" /><%--ZD 100157--%>
	<input type="hidden" runat="server" id="hdnConfTime" name="hdnConfTime"  /><%--ZD 102008--%>
	<input type="hidden" runat="server" id="hdnEndTime" name="hdnEndTime"  /><%--ZD 100157--%>
    <input type="hidden" runat="server" id="hdnConfID" name="hdnConfID"  /><%--ZD 101942--%>
  
<%
    if (Request.QueryString["hf"] != null)
    {
        if (Request.QueryString["hf"].ToString() == "1")
        {
%>
            <table width="100%" border="0"><tr><td align="right">
	            <input type="button" name="close" onfocus="this.blur()" id="close" value="Close" class="altShort2BlueButtonFormat" onclick="javascript:window.close();">

<%              
                if (Request.QueryString["pub"] != null)
                {
                    if (Request.QueryString["pub"].ToString() == "1")
                    {
%>
	                    <input type="button" name="login" id="login" value="Login" class="altShort2BlueButtonFormat" onclick="javascript:window.location.href='genlogin.aspx'">
                        </td></tr></table>
<%                  }
                    else{%> </td></tr></table><%}
                }
                else{%> </td></tr></table><%}
         }
    }   
%>  
 
     <table align="center"> <%--Added for FF--%>
       <tr>
        <td>
     <div id="footer" style="width:100%;">
      <table border="0"  width="100%" align="left"  style="vertical-align:bottom;">
          <tr>
              <td colspan="12" align="center">
                 <h3><asp:Literal Text="<%$ Resources:WebResources, PersonalCalendar_PersonalCalend%>" runat="server"></asp:Literal>&nbsp;&nbsp; 
                    <%--<input type="button" name="RoomCal" onfocus="this.blur()"  value="Switch to Room Calendar" style="width:180px" class="altShortBlueButtonFormat" onclick="javascript:viewrmdy()" /></h3>--%>
                    <asp:DropDownList runat="server" ID="lstCalendar" class="altText" tabindex="0" ToolTip="View" onChange="goToCal();javascript:DataLoading1('1');">
                        <asp:ListItem Value="1" Text="<%$ Resources:WebResources, PersonalCalendar_lstcal1%>"></asp:ListItem>
                        <asp:ListItem Value="3" Text="<%$ Resources:WebResources, PersonalCalendar_lstcal3%>"></asp:ListItem>
                        <%--<asp:ListItem Value="2" Text="<%$ Resources:WebResources, PersonalCalendar_lstcal2%>"></asp:ListItem>--%> <%--ZD 101388 - Commented--%>
                    </asp:DropDownList>
                    <br />
              </td>
        </tr>
      </table>
     </div>
        </td> <%--Added for FF--%>
      </tr>
     </table> 
     
    <table width="100%" border="0" style="vertical-align:super;">
        <tr valign="top">
            <td id="ActionsTD" valign="top" align="right" style="width:15%" runat="server">
              <table><tr height="15PX"><td></td></tr></table> <%--ZD 100393--%>
                 <div align="center" id="othview">
                    <table border="0" width="100%" class="treeSelectedNode"> 
                         <tr id="trofficehrDaily" >
                            <td align="left">  <%--ZD 100157 Starts--%>
                             <table border="0" style="border-collapse:collapse; text-align:left" >
                            <tr>
                                <td nowrap="nowrap" valign='top'><%--ZD 100284--%>
                                    <asp:CheckBox id="officehrDaily" Checked="true" onclick="javascript:chnghrs();ShowHrs();"   AutoPostBack="false" runat="server" tooltip="show office hour only" />
                                    <label class="blackblodtext"  id="lblhour"><asp:Literal ID="Literal1" Text="<%$ Resources:WebResources, PersonalCalendar_lbl2%>" runat="server"></asp:Literal></label>&nbsp;    
                                    <label class="rmnamediv" for="officehr" id="lblOfficehrs" title="show office hours only" style="display:none;">Show Office Hours Only</label>
                                </td>
                                <td  nowrap="nowrap" align="left">
                                    <mbcbb:combobox id="lstStartHrs" cssclass="altText" causesvalidation="true" runat="server" enabled="true" rows="15" >
                                    </mbcbb:combobox> 
                                    <br />
                                    <asp:RequiredFieldValidator ID="reqlstStartHrs" runat="server" ControlToValidate="lstStartHrs"
                                    Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, Required_Time%>"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="reglstStartHrs" runat="server" ControlToValidate="lstStartHrs"
                                    Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, InvalidTime%>"
                                    ValidationExpression="[0-1][0-9]:[0-5][0-9] ?[A|a|P|p][M|m]"></asp:RegularExpressionValidator>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2"></td>
                            </tr>
                            <tr>
                                <td></td><%--ZD 100284--%>
                                <td nowrap="nowrap" align="left">
                                    <mbcbb:combobox id="lstEndHrs" cssclass="altText" causesvalidation="true"
                                    runat="server" enabled="true" rows="15" >
                                    </mbcbb:combobox> 
                                    <asp:RequiredFieldValidator ID="reqlstEndHrs" runat="server" ControlToValidate="lstEndHrs"
                                    Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, Required_Time%>"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="reglstEndHrs" runat="server" ControlToValidate="lstEndHrs"
                                    Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, InvalidTime%>"
                                    ValidationExpression="[0-1][0-9]:[0-5][0-9] ?[A|a|P|p][M|m]"></asp:RegularExpressionValidator>
                                     <%--ZD 100157 Ends--%>
                                </td>
                            </tr>
                         </table>
                            </td>
                         </tr>
                         <tr id="trofficehrWeek" style="display:none">
                             <td align="left"  nowrap>
                                <asp:CheckBox id="officehrWeek" Checked="true" onclick="javascript:chnghrs();"   AutoPostBack="false" runat="server" tooltip="show office hour only" />
                                <asp:Label class="blackblodtext" runat="server"  id="lblweek" Text="<%$ Resources:WebResources, PersonalCalendar_lbl3%>"></asp:Label>
                                <label class="rmnamediv" for="officehr" id="Label1" title="show office hours only"  style="display:none;">Show Weekdays Only</label>
                             </td>
                         </tr>
                         <tr id="trofficehrMonth" style="display:none">
                            <td align="left"  nowrap>
                                <asp:CheckBox id="officehrMonth" Checked="true" onclick="javascript:chnghrs();"  AutoPostBack="false" runat="server" tooltip="show week days only" />
                                <asp:Label class="blackblodtext" runat="server"  id="lblmonth" Text="<%$ Resources:WebResources, PersonalCalendar_lbl3%>"></asp:Label>
                                <label class="rmnamediv" for="officehr" id="Label2" title="show office hours only" style="display:none;">Show Weekdays Only</label>
                            </td>
                         </tr>
                         <tr><%--FB 1800--%>
                              <td valign="top" align="left">
                                <asp:CheckBox ID="showDeletedConf" runat="server" AutoPostBack="false"  onclick="DeleteClick()" />                                
                                <span class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, PersonalCalendar_DeletedConfere%>" runat="server"></asp:Literal></span>
                              </td>
                         </tr>
                         <tr>
                            <td class="tableHeader"  align="left"><asp:Literal Text="<%$ Resources:WebResources, PersonalCalendar_Calendar%>" runat="server"></asp:Literal></td>
                         </tr>
                        <tr height="180" align="left"  valign="baseline">
                            <td> &nbsp; <%--ZD 100420--%>
                            <a href="#" class="name" onclick="return false;"><div id="flatCalendarDisplay" align="center" style="background-color:#D4D0C8;" ></div></a> <%--ZD 100639--%>
                            <div id="subtitlenamediv"></div></td> <%--ZD 100426--%>
                        </tr>
                    </table>
                 </div>
                 <table><tr height="15"><td>&nbsp;</td></tr></table>
                 <div align="center" id="conftypeDIV" style="z-index:1;width:100%;" class="treeSelectedNode">
                    <table border="0" width="100%">
                        <tr><td height="5" class="tableHeader"><asp:Literal Text="<%$ Resources:WebResources, PersonalCalendar_Filters%>" runat="server"></asp:Literal></td></tr>
                        <tr valign="top">
                            <td align="left">
                                <asp:TreeView ID="treeRoomSelection" runat="server" BorderColor="White" Height="100%" ShowCheckBoxes="All" valign="top"  
                                  ShowLines="True" Width="100%" SkipLinkText ="" onclick="javascript:getTabCalendar(event)" BorderStyle="None"> <%--ZD 100420--%>
                                    <NodeStyle CssClass="blacktext" />
                                    <RootNodeStyle CssClass="blacktext" />
                                    <ParentNodeStyle CssClass="blacktext" />
                                    <LeafNodeStyle CssClass="blacktext" />
                                    <Nodes>
                                        <asp:TreeNode SelectAction="None"   Value="ALL" text="<%$ Resources:WebResources, PersonalCalendar_All%>">
                                            <asp:TreeNode  SelectAction="None" Value="OG" text="<%$ Resources:WebResources, PersonalCalendar_OngoingConfere%>"></asp:TreeNode>
                                            <asp:TreeNode  SelectAction="None" Value="OM" text="<%$ Resources:WebResources, PersonalCalendar_OnMCUConferen%>"></asp:TreeNode> <%--ZD 100036--%>
                                            <asp:TreeNode selectaction="None" value="FC" text="<%$ Resources:WebResources, PersonalCalendar_FutureConferen%>"></asp:TreeNode>
                                            <asp:TreeNode selectaction="None" value="WLC" text="<%$ Resources:WebResources, PersonalCalendar_WaitListConferen%>"></asp:TreeNode><%--ZD 102532--%>
                                            <asp:TreeNode selectaction="None" value="PC" text="<%$ Resources:WebResources, PersonalCalendar_PublicConferen%>"></asp:TreeNode>
                                            <asp:TreeNode selectaction="None" value="PNC" text="<%$ Resources:WebResources, PersonalCalendar_PendingConfere%>"></asp:TreeNode>
                                            <asp:TreeNode selectaction="None" value="AC" text="<%$ Resources:WebResources, PersonalCalendar_ApprovalConfer%>"></asp:TreeNode>
                                        </asp:TreeNode>
                                    </Nodes>
                                </asp:TreeView>
                            </td>
                        </tr>
                    </table>
                 </div>
              </td>
              <td  style="width:2%"></td> 
              <td style="width:83%" align="left">
                <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
                <label id="labErrTime" class="lblError"  style="margin-left: 300px; display:none">
                <asp:Literal ID="Literal2" Text="<%$ Resources:WebResources, EndTimeValidation%>" runat="server"></asp:Literal></label> <%--ZD 100157--%>
                <asp:UpdatePanel ID="PanelTab" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                    <Triggers><asp:AsyncPostBackTrigger ControlID="btnDate" /></Triggers>
                    <ContentTemplate>
                        <input type="hidden" id="HdnMonthlyXml" runat="server" /> 
                        <div id="dataLoadingDIV" name="dataLoadingDIV" align="center" style="display:none">
                            <img border='0' src='image/wait1.gif' alt='Loading..' />
                        </div><%--ZD 100678--%>    
                            <input type="hidden" id="HdnXml" runat="server" />
                            <input type="hidden" id="IsSettingsChange" runat="server" />
                            <dxtc:ASPxPageControl ClientInstanceName="CalendarContainer" ID="CalendarContainer" runat="server" ActiveTabIndex="0" Width="99%" 
                            > <%--ZD 100151--%>
                                <ClientSideEvents ActiveTabChanged="function(s,e){ChangeTabsIndex(s,e);}" />
                                <TabPages>
                                 <dxtc:TabPage Text="<%$ Resources:WebResources, PersonalCalendar_DailyView%>" Name="Daily">
                                    <ContentCollection>
                                        <dxw:ContentControl ID="ContentControl1" runat="server">
                                            <table width="100%" style="width: 100%; vertical-align: top; overflow: auto; height:380px">  <%-- ZD 100157--%><%-- ZD 100085--%>
                                               <tr valign="top" id="trlegend1" runat="server"> <%--FB 1985--%>
                                                    <td>
                                                        <table  width="100%">
                                                            <tr>
                                                                <td  id="TdAV1" runat="server" width="15" height="15" bgcolor="#BBB4FF"></td>
                                                                <td id="TdAV" nowrap="nowrap" runat="server"> <span id="spnAV"  runat="server"><asp:Literal Text="<%$ Resources:WebResources, PersonalCalendar_spnAV%>" runat="server"></asp:Literal></span></td>
                                                                <td width="15" height="15" bgcolor="#F16855"></td>
                                                                <td nowrap="nowrap"><span id="spnRmHrg" runat="server"><asp:Literal Text="<%$ Resources:WebResources, PersonalCalendar_spnRmHrg%>" runat="server"></asp:Literal></span></td>
                                                                <td id="TdP2p" runat="server" width="15" height="15" bgcolor="#EAA2D4"></td>
                                                                <td id="TdA" nowrap="nowrap" runat="server"><span id="spnAudCon" runat="server"><asp:Literal Text="<%$ Resources:WebResources, PersonalCalendar_spnAudCon%>" runat="server"></asp:Literal></span></td>
                                                                <td id="TdA1" runat="server" width="15" height="15" bgcolor="#85EE99"></td>
                                                                <td id="TdP2p1" nowrap="nowrap" runat="server"><span id="spnPpConf" runat="server"><asp:Literal Text="<%$ Resources:WebResources, PersonalCalendar_spnPpConf%>" runat="server"></asp:Literal></span></td>
                                                                <td id="TdVMR" runat="server" width="15" height="15" bgcolor="#82CAFF"></td> <%--FB 2448--%>
                                                                <td id="Td27" nowrap="nowrap" runat="server"><span id="Span1" runat="server"><asp:Literal Text="<%$ Resources:WebResources, PersonalCalendar_Span1%>" runat="server"></asp:Literal></span></td>
                                                            </tr>
                                                            <tr>
                                                                <td id="TdDailyHotdeskingColor" runat="server" width="15" height="15" bgcolor="#cc0033"></td><%--FB 2694--%><%--ZD 100085--%>	
			                                                    <td id="TdDailyHotdesking" nowrap="nowrap" runat="server"><asp:Literal Text="<%$ Resources:WebResources, PersonalCalendar_TdDailyHotdesking%>" runat="server"></asp:Literal></td>	
			                                                    <td id="Td21" runat="server" width="15" height="15" bgcolor="#01DFD7"></td>
			                                                    <td id="Td22" nowrap="nowrap" runat="server"><asp:Literal Text="<%$ Resources:WebResources, PersonalCalendar_Td22%>" runat="server"></asp:Literal></td>
															<%-- ZD 100085 End--%>
                                                            </tr>
                                                        </table>
                                                    </td>
                                               </tr>  
                                               <tr valign="top">
                                                    <td>
                                                     <div id="imgInfo" name="imgInfo" align="left">
                                                    <span ID="lblPerCal" runat="server" style="color: #666666;" > <asp:Literal Text="<%$ Resources:WebResources, PersonalCalendar_lbl1%>" runat="server"> </asp:Literal></span> <%--ZD 100422--%>
                                                    </div>
                                                        <div id="DayPilotCalendar" style="height:300px;"> <%--ZD 100151--%>
                                                            <DayPilot:DayPilotCalendar  ID="schDaypilot"  runat="server" Days="1"   Visible="false" 
                                                            DataStartField="start"   DataEndField="end"  DataTextField="confDetails" DataValueField="ConfID" 
                                                            DataTagFields="ConferenceType,CustomDescription,confName,ID,Heading,ConfTime,Hours,Minutes,Password,Location,ConfSupport,CustomOptions,ConferenceTypeName,hasRightstoEdit,ConferenceStatus,hasEdit,HostName,RequesterName" 
                                                            HeaderFontSize="8pt" HeaderHeight="17" CellDuration="30"  OnBeforeEventRender="BeforeEventRenderhandler"  OnBeforeHeaderRender="BeforeHeaderRenderDaily" BubbleID="Details" ShowToolTip="false"    EventClickHandling="JavaScript"   EventClickJavaScript="vieweditOption(e);" EventFontSize="10" 
                                                            ClientObjectName="dps1" TimeRangeSelectedHandling="JavaScript" HeightSpec="Fixed" Height="300" TimeRangeSelectedJavaScript="getConfType(start, end)"  UseEventBoxes="Never" OnBeforeTimeHeaderRender="BeforeTimeHeaderRenderDaily" /> <%--FB 1673--%> <%-- FB 2588 ZD 101714--%><%--ZD 103918--%><%--ALLDEV-697--%>
                                                            <DayPilot:DayPilotBubble ID="Details" runat="server"  Visible="true"  Width="0" OnRenderContent="BubbleRenderhandler" ZIndex="999"></DayPilot:DayPilotBubble>    <%-- ZD 100157--%><%-- ZD 100421--%><%-- ZD 103879 ALLDEV-857--%>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </dxw:ContentControl>   
                                    </ContentCollection>
                                  </dxtc:TabPage>
                                  <dxtc:TabPage Text="<%$ Resources:WebResources, PersonalCalendar_WeekView%>" name="Weekly">
                                    <ContentCollection>
                                        <dxw:ContentControl ID="ContentControl2" runat="server">
                                            <table width="100%" style="width: 100%; vertical-align: top; overflow: auto; height: 380px"> <%-- ZD 100157--%><%-- ZD 100085--%>
                                                <tr valign="top" id="trlegend2" runat="server"><%--FB 1985--%>
                                                    <td>
                                                        <table width="100%">
                                                            <tr><%-- ZD 100085 Starts--%>
                                                                <td id="Td1" runat="server" width="15" height="15" bgcolor="#BBB4FF"></td>
                                                                <td id="Td2" nowrap="nowrap" runat="server"><span id="spnAVW" runat="server"><asp:Literal Text="<%$ Resources:WebResources, PersonalCalendar_spnAVW%>" runat="server"></asp:Literal></span></td>
                                                                <td width="15" height="15" bgcolor="#F16855"></td>
                                                                <td nowrap="nowrap"><span id="spnRmHrgW" runat="server"><asp:Literal Text="<%$ Resources:WebResources, PersonalCalendar_spnRmHrgW%>" runat="server"></asp:Literal></span></td>
                                                                <td id="Td3" runat="server" width="15" height="15" bgcolor="#EAA2D4"></td>
                                                                <td id="Td4" nowrap="nowrap" runat="server"><span id="spnAudConW" runat="server"><asp:Literal Text="<%$ Resources:WebResources, PersonalCalendar_spnAudConW%>" runat="server"></asp:Literal></span></td>
                                                                <td id="Td5" runat="server" width="15" height="15" bgcolor="#85EE99"></td>
                                                                <td id="Td6" nowrap="nowrap" runat="server"><span id="spnPpConfW" runat="server"><asp:Literal Text="<%$ Resources:WebResources, PersonalCalendar_spnPpConfW%>" runat="server"></asp:Literal></span></td>
                                                                <td id="Td29" runat="server" width="15" height="15" bgcolor="#82CAFF"></td> <%--FB 2448--%>
                                                                <td id="Td30" nowrap="nowrap" runat="server"><span id="Span2" runat="server"><asp:Literal Text="<%$ Resources:WebResources, PersonalCalendar_Span2%>" runat="server"></asp:Literal></span></td>
                                                            </tr>
                                                            <tr>
                                                                <td id="TdWeeklyHotdeskingColor" runat="server" width="15" height="15" bgcolor="#cc0033"></td><%--FB 2694--%><%--ZD 100085--%>	
			                                                    <td id="TdWeeklyHotdesking" nowrap="nowrap" runat="server"><asp:Literal Text="<%$ Resources:WebResources, PersonalCalendar_TdWeeklyHotdesking%>" runat="server"></asp:Literal></td>		                                                    
			                                                    <td id="Td23" runat="server" width="15" height="15" bgcolor="#01DFD7"></td>
			                                                    <td id="Td24" nowrap="nowrap" runat="server"><asp:Literal Text="<%$ Resources:WebResources, PersonalCalendar_Td24%>" runat="server"></asp:Literal></td>
															<%-- ZD 100085 End--%>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr valign="top">
                                                    <td>
                                                      <div id="Div1" align="left"  style="height:300px;" ><%--ZD 100151--%>
                                                         <DayPilot:DayPilotCalendar CssClass="tableHeader" ID="schDaypilotweek" runat="server" Days="7" Visible="false" 
                                                        DataStartField="start" DataEndField="end"  DataTextField="confDetails" DataValueField="ConfID" 
                                                        DataTagFields="ConferenceType,ConfID,CustomDescription,confName,ID,Heading,ConfTime,Hours,Minutes,Password,Location,ConfSupport,CustomOptions,ConferenceTypeName,hasRightstoEdit,ConferenceStatus,hasEdit,HostName,RequesterName" 
                                                        HeaderFontSize="8pt" HeaderHeight="17" CellDuration="30"  OnBeforeEventRender="BeforeEventRenderhandler"  BubbleID="DetailsWeekly" ShowToolTip="false"    EventClickHandling="JavaScript" HeightSpec="Fixed" Height="300"  EventClickJavaScript="vieweditOption(e);" EventFontSize="10" CssClassPrefix="calendar_silver_"
                                                        ClientObjectName="dps1"  UseEventBoxes="Never" OnBeforeTimeHeaderRender="BeforeTimeHeaderRenderWeekly" /> <%-- FB 2588 --%> <%-- ZD 100157--%> <%-- ZD 100421--%><%-- ZD 103879--%><%--ALLDEV-697 ALLDEV-857--%>
                                                        <DayPilot:DayPilotBubble ID="DetailsWeekly" runat="server"  Visible="true"  Width="0" OnRenderContent="BubbleRenderhandler" ZIndex="999"></DayPilot:DayPilotBubble>   
                                                      </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </dxw:ContentControl>    
                                    </ContentCollection>
                                 </dxtc:TabPage>
                                 <dxtc:TabPage Text="<%$ Resources:WebResources, PersonalCalendar_MonthView%>" Name="Monthly">
                                    <ContentCollection>
                                        <dxw:ContentControl ID="ContentControl3" runat="server">
                                            <table width="100%"  style="width:100%;vertical-align:top;overflow:auto;height:550px">
                                                <tr valign="top" id="trlegend3" runat="server"><%--FB 1985--%>
                                                    <td>
                                                        <table width="100%">
                                                            <tr><%-- ZD 100085 Starts--%>
                                                                <td id="Td11" runat="server" width="15" height="15" bgcolor="#BBB4FF"></td>
                                                                <td id="Td12" nowrap="nowrap" runat="server"><span id="spnAVM" runat="server"><asp:Literal Text="<%$ Resources:WebResources, PersonalCalendar_spnAVM%>" runat="server"></asp:Literal></span></td>
                                                                <td width="15" height="15" bgcolor="#F16855"></td>
                                                                <td nowrap="nowrap"><span id="spnRmHrgM" runat="server"><asp:Literal Text="<%$ Resources:WebResources, PersonalCalendar_spnRmHrgM%>" runat="server"></asp:Literal></span></td>
                                                                <td id="Td13" runat="server" width="15" height="15" bgcolor="#EAA2D4"></td>
                                                                <td id="Td14" nowrap="nowrap" runat="server"><span id="spnAudConM" runat="server"><asp:Literal Text="<%$ Resources:WebResources, PersonalCalendar_spnAudConM%>" runat="server"></asp:Literal></span></td>
                                                                <td id="Td15" runat="server" width="15" height="15" bgcolor="#85EE99"></td>
                                                                <td id="Td16" nowrap="nowrap" runat="server"><span id="spnPpConfM" runat="server"><asp:Literal Text="<%$ Resources:WebResources, PersonalCalendar_spnPpConfM%>" runat="server"></asp:Literal></span></td>
                                                                <td id="Td31" runat="server" width="15" height="15" bgcolor="#82CAFF"></td> <%--FB 2448--%>
                                                                <td id="Td32" nowrap="nowrap" runat="server"><span id="Span3" runat="server"><asp:Literal Text="<%$ Resources:WebResources, PersonalCalendar_Span3%>" runat="server"></asp:Literal></span></td>
                                                            </tr>
                                                            <tr>
                                                                <td id="TdMontlyHotdeskingColor" runat="server" width="15" height="15" bgcolor="#cc0033"></td><%--FB 2694--%><%--ZD 100085--%>	
			                                                    <td id="TdMontlyHotdesking" nowrap="nowrap" runat="server"><asp:Literal Text="<%$ Resources:WebResources, PersonalCalendar_TdMontlyHotdesking%>" runat="server"></asp:Literal></td>
			                                                    <td id="Td25" runat="server" width="15" height="15" bgcolor="#01DFD7"></td>
			                                                    <td id="Td26" nowrap="nowrap" runat="server"><asp:Literal Text="<%$ Resources:WebResources, PersonalCalendar_Td26%>" runat="server"></asp:Literal></td>
																<%-- ZD 100085 End--%>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr valign="top">
                                                    <td>
                                                       <div id="DayPilotCalendarMonth"  ><%--ZD 100151--%>
                                                         <DayPilot:DayPilotMonth CssClass="tableHeader" ID="schDaypilotMonth" runat="server"  Visible="false" 
                                                            DataStartField="start" DataEndField="end"  DataTextField="confDetails" DataValueField="ConfID"  OnBeforeCellRender="BeforeCellEventRenderhandler"
                                                            DataTagFields="ConferenceType,CustomDescription,confName,ID,Heading,ConfTime,Hours,Minutes,Password,Location,ConfSupport,CustomOptions,ConferenceTypeName,hasRightstoEdit,ConferenceStatus,hasEdit,HostName,RequesterName"
                                                            HeaderFontSize="8pt" HeaderHeight="17"  WeekStarts="Monday"  OnBeforeEventRender="BeforeEventRenderhandler"  OnBeforeHeaderRender="BeforeHeaderRenderMonthly"
                                                            EventClickHandling="JavaScript"   EventClickJavaScript="vieweditOption(e);" JavaScript="vieweditOption(e);" EventFontSize="10" 
                                                            ClientObjectName="dps1" Width="100%"  BubbleID="DetailsMonthly" ShowToolTip="false"  HeightSpec="Fixed" Height="300"   />  <%-- ZD 100157--%><%-- ZD 100421 ZD 101714--%><%-- ZD 103879 ALLDEV-857--%>
                                                            <DayPilot:DayPilotBubble ID="DetailsMonthly" runat="server"  Visible="true" Width="0" OnRenderContent="BubbleRenderhandler" ZIndex="999"></DayPilot:DayPilotBubble>   
                                                       </div>
                                                    </td>
                                                </tr>
                                            </table>
                                    </dxw:ContentControl>
                                    </ContentCollection>
                                 </dxtc:TabPage>
                             </TabPages>
                        </dxtc:ASPxPageControl>
                    </div>
                    <asp:Button ID="btnDate" style="display:none" OnClick="ChangeCalendarDate" OnClientClick="DataLoading('1');" runat="server"/>
                    <asp:Button ID="btnWithinMonth" style="display:none" OnClick="changeDate" OnClientClick="DataLoading('1');" runat="server"/>
                    <asp:Button ID="btnbsndhrs" style="display:none" OnClick="changeDate" OnClientClick="DataLoading('');" runat="server"/>
                    
                </ContentTemplate>
             </asp:UpdatePanel>
           </td>
        </tr>
     </table>
     <%--ZD 102008 Start--%>
     <div id="PopupFormList" align="center" style="position: absolute; overflow: hidden; z-index:1200;
            border: 1px; width: 450px; display: none; top: 300px; left: 510px; height: 450px;">
            <table align="center" class="tableBody" width="80%" cellpadding="5" cellspacing="5">
                <tr class="tableHeader">
                    <td colspan="2" align="left" class="blackblodtext">
                        <asp:Literal ID="Literal3" Text="<%$ Resources:WebResources, ChooseFormHeading1%>" runat="server"></asp:Literal>
                    </td>
                </tr>
                <tr>
                    <td align="left" colspan="2" nowrap="nowrap"><%--ZD 103051--%>
                        <asp:RadioButtonList ID="rdEditForm" runat="server" onfocus="javascript:void(0);"
                            RepeatDirection="Horizontal" RepeatLayout="Table" RepeatColumns="1" CellPadding="3"
                            CellSpacing="3" Width="100%">
                            <asp:ListItem Text="<%$ Resources:WebResources, LongForm%>" Value="1"></asp:ListItem>
                            <asp:ListItem Text="<%$ Resources:WebResources, ExpressForm%>" Value="2"></asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input id="btnOk" runat="server" type="button" class="altMedium0BlueButtonFormat"
                            onclick="javascript:return fnRedirectForm();" value="<%$ Resources:WebResources, MasterChildReport_cmdConfOk%>" />
                    </td>
                    <td>
                        <input id="btnCancel" runat="server" type="button" class="altMedium0BlueButtonFormat"
                            onclick="javascript:fnDivClose();" value="<%$ Resources:WebResources, Cancel%>" />
                    </td>
                </tr>
            </table>
        </div>
        <%--ZD 102008 End--%>
    <%--ZD 101942 Starts--%>
    <div id="EditPopUp" align="center" style="position: absolute; overflow: hidden; z-index:1200;
        border: 1px; width: 450px; display: none; top: 300px; left: 510px; height: 450px;">
        <table align="center" class="tableBody" width="80%" cellpadding="5" cellspacing="5">
            <tr class="tableHeader">
                <td colspan="2" align="left" class="blackblodtext">
                    <asp:Literal Text="<%$ Resources:WebResources, ChooseEditForm%>" runat="server"></asp:Literal>
                </td>
            </tr>
            <tr>
                <td align="left" colspan="2" nowrap="nowrap"><%--ZD 103051--%>
                    <asp:RadioButtonList ID="rdEditOption" runat="server" onfocus="javascript:void(0);"
                        RepeatDirection="Horizontal" RepeatLayout="Table" RepeatColumns="1" CellPadding="3"
                        CellSpacing="3" Width="100%">
                        <asp:ListItem Text="<%$ Resources:WebResources, LongFormEdit%>" Value="1"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:WebResources, ExpressFormEdit%>" Value="2"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:WebResources, ManageConference%>" Value="3"></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td>
                    <input id="btnEditOk" runat="server" type="button" class="altMedium0BlueButtonFormat"
                        onclick="javascript:return fnEditOptionForm();" value="<%$ Resources:WebResources, MasterChildReport_cmdConfOk%>" />
                </td>
                <td>
                    <input id="btnEditCancel" runat="server" type="button" class="altMedium0BlueButtonFormat"
                        onclick="javascript:fnEditClose();" value="<%$ Resources:WebResources, Cancel%>" />
                </td>
            </tr>
        </table>
    </div>
    <%--ZD 101942 Ends--%>
    <asp:TextBox ID="txtType" Visible="false" runat="server"></asp:TextBox>  
    <input type="hidden" ID="txtSelectedDate" runat="server" />
    <input type="hidden" ID="txtSelectedDate1" runat="server" />
    
    <asp:UpdatePanel ID="UpdateXmls" runat="server">
<Triggers>
<asp:AsyncPostBackTrigger ControlID="xmlsAsync" EventName="Click" />
</Triggers>
<ContentTemplate>
<asp:Button ID="xmlsAsync" runat="server" style="display:none;"
onclick="LoadXmlsAsync" />
</ContentTemplate>
</asp:UpdatePanel>
    
  </form>
  </body>
  </html>  
  
<script language="javascript">
  //ZD 100604 start
   var img = new Image();
   img.src = "../en/image/wait1.gif";
  //ZD 100604 End
//FB 2598 Starts
//"<%=Session["emailClient"] %>"
    
    //ZD 101388 Commeted
    /*function removeOption()
    {
        if("<%=Session["EnableCallmonitor"]%>" == "0")
        {
        
        var x=document.getElementById("lstCalendar");
        x.remove(4);
        x.remove(3);
        }
       
    }*/

//FB 2598 Ends

var asyncReqCompleted = "1";    
//setTimeout(function(){document.getElementById ("btnLdAsync").click();},500);

function DeleteClick()
{
    document.getElementById ("btnDate").click();
    
}


//loadAsync();
function loadAsync()
{

// if("<%=Session["CalendarMonthly"]%>" == "") //FB 1888
    document.getElementById ("xmlsAsync").click();
        
}

function ChangeTabsIndex(s,e)
{
    var activetab = s.GetActiveTab();
   var tabname;
   tabname = activetab.name;
   var hour = document.getElementById('trofficehrDaily');
   var week = document.getElementById('trofficehrWeek');
   var month = document.getElementById('trofficehrMonth');
   var hdnmonth = document.getElementById('HdnMonthlyXml');
   
   if(tabname == "Daily")
   {
   hour.style.display = 'block';
   week.style.display = 'none';
   month.style.display = 'none';
   //if(document.getElementById("IsWeekOverLap").value == "Y")
       document.getElementById ("btnDate").click();
   }
   else if(tabname == "Weekly")
   {
    hour.style.display = 'none';
   week.style.display = 'block';
   month.style.display = 'none';
   //if(document.getElementById("IsMonthChanged").value == "Y"|| document.getElementById("IsWeekOverLap").value == "Y" || hdnmonth.value == "") //ZD 100151
       document.getElementById ("btnDate").click();
  
  
   }
   else if(tabname== "Monthly")
   {
    hour.style.display = 'none';
   week.style.display = 'none';
   month.style.display = 'block';
   //if(hdnmonth.value == "" || document.getElementById("IsMonthChanged").value == "Y" || document.getElementById("IsWeekOverLap").value == "Y") //ZD 100151
       document.getElementById ("btnDate").click();
   }
   if(fnTimeScroll()) //ZD 100157
    fnTimeScroll();
   
   } 
   
   function chnghrs()
   {
     if (fncheckTime() == false) //ZD 100157
     {
            DataLoading();
            return false;
            
     }
     var hourbtn = document.getElementById('btnbsndhrs');
     if(hourbtn)
        hourbtn.click();
   }

function DataLoading(val)
{   
    
    if (val=="1")
    {
        document.getElementById("dataLoadingDIV").style.display = 'block'; //ZD 100678
        //document.getElementById("dataLoadingDIV").innerHTML="<b><font color='#FF00FF' size='2'>Data loading ...</font></b>&nbsp;&nbsp;&nbsp;&nbsp;<img border='0' src='image/wait1.gif' >"; // FB 2742
        //document.body.style.cursor = "wait";
        document.body.style.cursor = "normal";//FB 2803
        
        
    
        
       }
    else
    {
        document.getElementById("dataLoadingDIV").style.display = 'none'; //ZD 100678
        document.body.style.cursor = "normal";
     } 
                  
}

function DataLoading1(val)
 {
    //alert(val);
    if (val == "1")
     document.getElementById("dataLoadingDIV").style.display = 'block'; //ZD 100678
    else
     document.getElementById("dataLoadingDIV").style.display = 'none'; //ZD 100678
}   
 var prm = Sys.WebForms.PageRequestManager.getInstance();
    prm.add_initializeRequest(InitializeRequestHandler);
    prm.add_endRequest(EndRequestHandler);        

    var pbQueue = new Array();
    var argsQueue = new Array();       

    function InitializeRequestHandler(sender, args) {
        if (prm.get_isInAsyncPostBack()) {
            args.set_cancel(true);
            pbQueue.push(args.get_postBackElement().id);
            argsQueue.push(document.forms[0].__EVENTARGUMENT.value);
        }
        
        DataLoading("1");
    }       

    function EndRequestHandler(sender, args) {
        if (pbQueue.length > 0) {
            __doPostBack(pbQueue.shift(), argsQueue.shift());
        }
        else
            DataLoading("0");
    }
var myPopup;
var IE4 = (document.all) ? true : false;


var	dayname = new Array();
var pageleft = 10;
var totaltop = 190, topleft = 245, totalh = 960, calendarh = 180, totalw = getwinwid() - topleft - pageleft, yaxlwid = 65, lineh = 15, splitwid=1;


var allowedrmnum = 0,	isOhOn = false;
var strConfs = "", startDate; 
var aryRooms="", selConfTypeCode;

var isWdOn = false, confmonthfirst = "";

function getwinwid() 
{
	var myWidth = 0, myHeight = 0;
	if( typeof( window.innerWidth ) == 'number' ) {
		//Non-IE
		myWidth = window.innerWidth;
		myHeight = window.innerHeight;
	} else if( document.documentElement && ( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) {
		//IE 6+ in 'standards compliant mode'
		myWidth = document.documentElement.clientWidth;
		myHeight = document.documentElement.clientHeight;
	} else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) {
		//IE 4 compatible
		myWidth = document.body.clientWidth;
		myHeight = document.body.clientHeight;
	}
	  
	return (myWidth);
}


function getConfType(start, end)//ZD 103918
{
 
  var time = start.toString(); //Edited for FF 
  var conftime = new Date(time);

  //ZD 103918
  var etime = end.toString();
  var confetime = new Date(etime);
  var diffMin = Math.floor(((confetime.getTime() - conftime.getTime()) / 1000) / 60);
  
  var hour,min;
  
  var splttim = conftime.toTimeString().split(':');
  
  hour = splttim[0];
  min = splttim[1];
  
  if(timeFormat == "1")
  {
  
      hour = conftime.getHours();
      min = conftime.getMinutes();
      
      
      if(eval(hour) < 10)
        hour = "0"+hour
        
      if(eval(min) < 10)
        min = "0"+min
        
      //min = min +" "+conftime.toLocaleTimeString().toString().split(" ")[1]
      min = min +" "+ conftime.format("tt"); //FB 2108
  }


  if (queryField("hf") == "1") {
      alert(CannotSChedule);
      return false;
  }

  else 
  {

      if ('<%=Session["hasConference"]%>' == '1' && '<%=Session["hasExpConference"]%>' == "1") 
      {
          document.getElementById('hdnConfTime').value = confdate + "_" + hour + "_" + min + "_" + diffMin; //ZD 103918
          document.getElementById("PopupFormList").style.display = 'block';
          document.getElementById("ModalDiv").style.display = 'block';
          if(document.getElementById('rdEditForm_1') != null)
              document.getElementById('rdEditForm_1').checked = false;
          if(document.getElementById('rdEditForm_0') != null)
              document.getElementById('rdEditForm_0').checked = false;          
      }
      else if ('<%=Session["hasConference"]%>' == '1') {
          url = "ConferenceSetup.aspx?t=n&sd=" + confdate + "&st=" + hour + ":" + min + "&dur=" + diffMin; //ZD 103918
          DataLoading("1");
          window.location.href = url;

      }
      else if ('<%=Session["hasExpConference"]%>' == '1') {
          url = "ExpressConference.aspx?t=n&sd=" + confdate + "&st=" + hour + ":" + min + "&dur=" + diffMin; //ZD 103918
          DataLoading("1");
          window.location.href = url;
      }
      else {
          return false;
      }
      
  }
     
//        var mmm_numc, mm_strc, mm_aryc,mmm_strc,mmm_intc,menu;
//        
//        var menuset = "0";
//        var time ;
//        
//        mm_strc = "<%=Session["sMenuMask"].ToString() %>";;
//        mm_aryc = mm_strc.split("-");
//        mmm_strc = mm_aryc[0];
//        mmm_aryc = mmm_strc.split("*");
//        mmm_numc = parseInt(mmm_aryc[0], 10);
//        mmm_intc = parseInt(mmm_aryc[1], 10);
//        for (i=1; i<=mmm_numc; i++) {
//        menu = ( mmm_intc & (1 << (mmm_numc-i)) ) ? ("menu_" + i) : "";
//        //FB 1929
//        if(menu == "menu_3")
//            menuset = "1";
//	    
//        }
//        //FB 1929
//        if(menuset > 0)
//        {         
////        if(confirm("Are you sure you want to create a new conference beginning " + confdate + " @ " + hour+":"+min  + "?"))
////        {
//            
//               url = "ConferenceSetup.aspx?t=n&sd=" + confdate + "&st=" + hour+":"+min  ; 
//            
//	        
//            DataLoading("1");
//            

////        }
//       }
   
  }
  function fnRedirectForm() {
      
      var a,b;
      if(document.getElementById('hdnConfTime') != null)
          a = document.getElementById('hdnConfTime').value;

      if(a != null)
        b = a.split('_');

      if (document.getElementById('rdEditForm_0').checked == true) 
      {
          url = "ConferenceSetup.aspx?t=n&sd=" + b[0] + "&st=" + b[1] + ":" + b[2] + "&dur=" + b[3]; //ZD 103918
          DataLoading("1");
          window.location.href = url;
      }
      else if (document.getElementById('rdEditForm_1').checked == true)
      {
          url = "ExpressConference.aspx?t=n&sd=" + b[0] + "&st=" + b[1] + ":" + b[2] + "&dur=" + b[3]; //ZD 103918
          DataLoading("1");
          window.location.href = url;
      }
      

  }
  function fnDivClose() {
      document.getElementById("PopupFormList").style.display = 'None';
      document.getElementById("ModalDiv").style.display = 'None';
  }
  //ZD 102008 - End

function ViewDetails(e)
{
    
  url = "ManageConference.aspx?confid="+e.value()+"&t=hf";
//  myPopup = window.open(url, "viewconference", "width=1,height=1,resizable=yes,scrollbars=yes,status=no");
  myPopup.focus();
  
}

//ZD 101942 Starts
function vieweditOption(e) {
    var confid, hasRightstoEdit, ConferenceStatus, hasEdit; //ZD 103051 //ZD 103879
    confid = e.value();
    hasRightstoEdit = e.tag("hasRightstoEdit");
    ConferenceStatus = e.tag("ConferenceStatus"); //ZD 103051
    hasEdit = e.tag("hasEdit"); //ZD 103879
    if (document.getElementById('hdnConfID') != null)
        document.getElementById('hdnConfID').value = confid;
    if (hasRightstoEdit == "0") {
        url = "ManageConference.aspx?confid=" + confid + "&t=hf";
        myPopup = window.open(url, "viewconference", "width=1,height=1,resizable=yes,scrollbars=yes,status=no");
        myPopup.focus();
        //url = "ManageConference.aspx?t=&confid=" + confid;
        //DataLoading("1");
        //window.location.href = url;

    } else  if ('<%=Session["hasConference"]%>' == '1' && '<%=Session["hasExpConference"]%>' == "1") {
        document.getElementById("EditPopUp").style.display = 'block';
        document.getElementById("ModalDiv").style.display = 'block';
        if (document.getElementById('rdEditOption_1') != null) {//ZD 103051 STARTS
            document.getElementById('rdEditOption_1').checked = false;
            if (ConferenceStatus == "5" || ConferenceStatus == "7" || ConferenceStatus == "9" || ConferenceStatus == "3" || hasEdit == "0") { //ZD 103879
                document.getElementById('rdEditOption_1').parentNode.style.display = "none";
            }

            else if (document.getElementById('rdEditOption_1').parentNode.style.display == "none") {
                document.getElementById('rdEditOption_1').parentNode.style.display = "block";
            }
        }


        if (document.getElementById('rdEditOption_0') != null) {
            document.getElementById('rdEditOption_0').checked = false;
            if (ConferenceStatus == "5" || ConferenceStatus == "7" || ConferenceStatus == "9" || ConferenceStatus == "3" || hasEdit == "0") { //ZD 103879
                document.getElementById('rdEditOption_0').parentNode.style.display = "none";
            }
            else if (document.getElementById('rdEditOption_0').parentNode.style.display == "none") {
                document.getElementById('rdEditOption_0').parentNode.style.display = "block";
            } 
        }
            
            if (document.getElementById('rdEditOption_2') != null)
            document.getElementById('rdEditOption_2').checked = false;
    }
    
    
    else if ('<%=Session["hasConference"]%>' == '1') {
        document.getElementById("EditPopUp").style.display = 'block';
        document.getElementById("ModalDiv").style.display = 'block';
        if (document.getElementById('rdEditOption_0') != null) {
            document.getElementById('rdEditOption_0').checked = false;
            if (document.getElementById('rdEditOption_1') != null) //104126
                document.getElementById('rdEditOption_1').parentNode.style.display = "none"; //104126
            if (ConferenceStatus == "5" || ConferenceStatus == "7" || ConferenceStatus == "9" || ConferenceStatus == "3" || hasEdit == "0") { //ZD 103879
                document.getElementById('rdEditOption_0').parentNode.style.display = "none";
            }
            else if (document.getElementById('rdEditOption_0').parentNode.style.display == "none") {
                document.getElementById('rdEditOption_0').parentNode.style.display = "block";
            } 
        }
                
        
        if (document.getElementById('rdEditOption_2') != null)
            document.getElementById('rdEditOption_2').checked = false;

    }
    else if ('<%=Session["hasExpConference"]%>' == '1') {

        document.getElementById("EditPopUp").style.display = 'block';
        document.getElementById("ModalDiv").style.display = 'block';
        if (document.getElementById('rdEditOption_1') != null) {
            document.getElementById('rdEditOption_1').checked = false;
            if (document.getElementById('rdEditOption_0') != null)//104126
                document.getElementById('rdEditOption_0').parentNode.style.display = "none"; //104126
            if (ConferenceStatus == "5" || ConferenceStatus == "7" || ConferenceStatus == "9" || ConferenceStatus == "3" || hasEdit == "0") { //ZD 103879
                document.getElementById('rdEditOption_1').parentNode.style.display = "none";
            }
            else if (document.getElementById('rdEditOption_1').parentNode.style.display == "none") {
                document.getElementById('rdEditOption_1').parentNode.style.display = "block";
            }
        }//ZD 103051 ENDS
        
        
        if (document.getElementById('rdEditOption_2') != null)
            document.getElementById('rdEditOption_2').checked = false;

        var element = document.getElementById('rdEditOption');
        if (document.getElementById('rdEditOption_0') != null)
            element.deleteRow(0);
    }
    else {
        return false;
    }
}
function fnEditOptionForm() {
var confid;

if (document.getElementById('hdnConfID') != null)
    confid = document.getElementById('hdnConfID').value;

if (document.getElementById('rdEditOption_0') != null) {
    if (document.getElementById('rdEditOption_0').checked == true) {
        url = "ConferenceSetup.aspx?t=&confid=" + confid;
        DataLoading("1");
        window.location.href = url;
    }
}
if (document.getElementById('rdEditOption_1') != null) {
    if (document.getElementById('rdEditOption_1').checked == true) {
        url = "ExpressConference.aspx?t=&id=" + confid;
        DataLoading("1");
        window.location.href = url;
    }
}
if (document.getElementById('rdEditOption_2') != null) {
    if (document.getElementById('rdEditOption_2').checked == true) {
        url = "ManageConference.aspx?t=&confid=" + confid;
        DataLoading("1");
        window.location.href = url;
    }
}
}
function fnEditClose() {
    document.getElementById("EditPopUp").style.display = 'None';
    document.getElementById("ModalDiv").style.display = 'None';
}

//ZD 101942 Ends

function initsubtitle ()
{
    var dd, m = "";
    var monthNew,yrNew
    
    dd = new Date(confdate);
                     
    monthNew = (dd.getMonth() + 1);
    yrNew = dd.getFullYear();
    
    document.getElementById ("IsWeekOverLap").value = "";
    document.getElementById ("IsMonthChanged").value = "";
    
    if(WeekOverlap(confdate))
           document.getElementById ("IsWeekOverLap").value = "Y";
    if(document.getElementById ("MonthNum").value != monthNew || document.getElementById ("YrNum").value != yrNew )
    {
        document.getElementById ("YrNum").value = yrNew ;
        document.getElementById ("MonthNum").value = monthNew;
        document.getElementById ("IsMonthChanged").value = "Y";
    }
    else
    {
    
        document.getElementById ("IsMonthChanged").value = "";
        
    }
    
}
function datechg() {
    
   var hdndmonth = document.getElementById('HdnMonthlyXml');

    DataLoading("1");



    document.getElementById("txtSelectedDate").value=confdate;
    initsubtitle();
    
    if(hdndmonth.value != "" && document.getElementById ("IsMonthChanged").value == "")
    {
       document.getElementById ("btnWithinMonth").click();
       
    }
    else
        document.getElementById ("btnDate").click();
    
}


 document.getElementById ("btnDate").style.display = "none";
            
var dtFormatType, convDate ,calendarTyp ,timeFormat;
dtFormatType = '<%=dtFormatType%>';
calendarTyp = '<%=CalendarType%>';
timeFormat = '<%=timeFormat%>';
var confdate,confweekmon,curwkDate
var curdate = new Date();

//datechg("1");

if (document.getElementById("txtSelectedDate").value == "") {
	confdate = (curdate.getMonth() + 1) + "/" + curdate.getDate() + "/" + curdate.getFullYear();
	document.getElementById ("MonthNum").value = (curdate.getMonth() + 1); 
	document.getElementById ("YrNum").value = curdate.getFullYear();
	document.getElementById ("IsMonthChanged").value = "";
	confweekmon = addDates(curdate.getFullYear(), curdate.getMonth() + 1, curdate.getDate(), (curdate.getDay() == 0) ? -6 : (1-curdate.getDay()) );
	document.getElementById("<%=txtSelectedDate.ClientID %>").value=confdate;
	showFlatCalendar(0, '%m/%d/%Y');
} else {
	confdate = document.getElementById("txtSelectedDate").value;
	curwkDate = new Date(confdate);
	confweekmon = addDates(curwkDate.getFullYear(), curwkDate.getMonth() + 1, curwkDate.getDate(), (curwkDate.getDay() == 0) ? -6 : (1-curwkDate.getDay()) );
	showFlatCalendar(0, '%m/%d/%Y', document.getElementById("txtSelectedDate").value);
}
document.getElementById ("flatCalendarDisplay").style.position = 'relative';
document.getElementById ("flatCalendarDisplay").style.top = -15;
document.getElementById ("flatCalendarDisplay").style.height = 170;
document.getElementById ("flatCalendarDisplay").style.textAlign = "center";
//document.getElementById ("flatCalendarDisplay").style.display = "none";
//DataLoading("0");
initsubtitle();

function goToCal()
{
        if(document.getElementById("lstCalendar") != null)
        {
		    //ZD 101388 Starts Commented
		    /*if (document.getElementById("lstCalendar").value == "2")//FB 1779
		    {
			   if( isExpressUser == 1 ) //FB 1779
			      window.location.href = "ConferenceList.aspx?t=3";
			   else
			    window.location.href = "ConferenceList.aspx?t=2";
		    }*/
		    if (document.getElementById("lstCalendar").value == "3"){
			    viewrmdy();
			    //window.location.href = "roomcalendar.aspx?v=1&r=1&hf=&d=&pub=&m=&comp=" ; //code changed for calendar conversion FB 412
		    }
			//FB 2501 Call Monitoring
            //if (document.getElementById("lstCalendar").value == "4"){
            //    window.location.href = "MonitorMCU.aspx";
            //}       
            //FB 2501 P2P Call Monitoring
            //if (document.getElementById("lstCalendar").value == "5"){
            //        window.location.href = "point2point.aspx";
            //}
		    //ZD 101388 End Commented
		}
}

if(queryField("hf") == "1")
{

   if(document.getElementById("lstCalendar") != null)
    {
        document.getElementById("lstCalendar").style.display = 'none'  
    		    
    }
}

function WeekOverlap(weekBeginDate) {
		var overlaps = false;
		var week = new Date(weekBeginDate);
		var nextweeks =  new Date(weekBeginDate);
		var weekbegin = new Date();
		weekbegin.setDate(week.getDate() - week.getDay());
		nextweeks.setDate(weekbegin.getDate() + 7);
		var nextweek = new Date(nextweeks);
		if (nextweek.getMonth() != weekbegin.getMonth()) {
			overlaps = true;
		}
		var weeknew = getWeek(weekbegin);
		document.getElementById ("IsWeekChanged").value = "";
		(document.getElementById ("Weeknum").value != weeknew)
		{
		    document.getElementById ("IsWeekChanged").value = "Y";
		    
		}
		
		return overlaps;
		}
		
function getWeek (weekbegin)
 {
 var wkbegin = new Date(weekbegin);
 var onejan = new Date(wkbegin.getFullYear(),0,1);
 return Math.ceil((((wkbegin - onejan) / 86400000) + onejan.getDay()+1)/7);
 }

function WaitforSession()
{
DataLoading("1");
//        if("<%=Session["CalendarMonthly"]%>" == "")//FB 1888
        {
            naptime = 25 * 1000;
            //alert("<%=Session["CalendarMonthly"]%>");
            var sleeping = true;
            var now = new Date();
            var alarm;
            var startingMSeconds = now.getTime();
            while(sleeping){
            alarm = new Date();
            alarmMSeconds = alarm.getTime();
            if(alarmMSeconds - startingMSeconds > naptime){ sleeping = false; }
            }	
         }
}
//FB 2598 Starts
//removeOption(); //ZD 101388 Commeted
//FB 2598 Ends
//ZD 100157 Starts
   function  ShowHrs()
   {
       var ShwHrs= document.getElementById("officehrDaily");
       if(!ShwHrs.checked)
       {
           if(document.getElementById("lstStartHrs_Container")!= null)
            document.getElementById("lstStartHrs_Container").style.display="none";
           if(document.getElementById("lstEndHrs_Container")!= null)
            document.getElementById("lstEndHrs_Container").style.display="none";
           }
       else
       {
           if(document.getElementById("lstStartHrs_Container")!= null)
            document.getElementById("lstStartHrs_Container").style.display="inline";
           if(document.getElementById("lstEndHrs_Container")!= null)
            document.getElementById("lstEndHrs_Container").style.display="inline";
       
       } 
   }
   if(document.getElementById("reglstStartHrs") != null)
        document.getElementById("reglstStartHrs").controltovalidate = "lstStartHrs_Text"; 
    if (document.getElementById("reqlstStartHrs") != null)
        document.getElementById("reqlstStartHrs").controltovalidate = "lstStartHrs_Text";
    if (document.getElementById("reqlstEndHrs") != null)
        document.getElementById("reqlstEndHrs").controltovalidate = "lstEndHrs_Text"; 
    if (document.getElementById("reglstEndHrs") != null)
        document.getElementById("reglstEndHrs").controltovalidate = "lstEndHrs_Text";
     
 function fncheckTime() {

        var stdate = '';
        var stime = document.getElementById("lstStartHrs_Text").value;
        var etime = document.getElementById("lstEndHrs_Text").value;
        
        if('<%=Session["timeFormat"]%>' == "2") 
        {
             stime = stime.replace('Z', '')
            stime = stime.substring(0, 2) + ":" + stime.substring(2, 4);
            
            etime = etime.replace('Z', '')
            etime = etime.substring(0, 2) + ":" + etime.substring(2, 4);
        }
        document.getElementById("labErrTime").style.display="none";
        
        if (document.getElementById("lstEndHrs_Text") && document.getElementById("lstStartHrs_Text")) {
            stdate = GetDefaultDate('01/01/1901', '<%=((Session["timeFormat"] == null) ? "1" : Session["timeFormat"])%>');
            
            if (Date.parse(stdate + " " + etime) < Date.parse(stdate + " " + stime)) {
                document.getElementById("labErrTime").style.display="block";
                return false;
                }
            else if (Date.parse(stdate + " " + etime) == Date.parse(stdate + " " + stime)) {
                document.getElementById("labErrTime").style.display="block";
                return false;
                }
            else
                return true;
        }
    }
    
    if(document.getElementById("lstStartHrs_Text") != null)
    {
		//ZD 100284
        if(navigator.userAgent.indexOf('Trident') > -1)
            document.getElementById("lstStartHrs_Text").setAttribute("onblur","javascript:formatTimeNew('lstStartHrs_Text', 'reglstStartHrs',timeFormat);chnghrs();");
        document.getElementById("lstStartHrs_Text").setAttribute("onchange","javascript:formatTimeNew('lstStartHrs_Text', 'reglstStartHrs',timeFormat);chnghrs();");
    }
    if(document.getElementById("lstEndHrs_Text") != null)
    {
			//ZD 100284
        if(navigator.userAgent.indexOf('Trident') > -1)
            document.getElementById("lstEndHrs_Text").setAttribute("onblur","javascript:formatTimeNew('lstEndHrs_Text', 'reglstStartHrs',timeFormat);chnghrs();");
        document.getElementById("lstEndHrs_Text").setAttribute("onchange","javascript:formatTimeNew('lstEndHrs_Text', 'reglstStartHrs',timeFormat);chnghrs();");
    }




function fnUpdtInfo()
{
    
    if(document.getElementById("lstStartHrs_Text") != null)
        document.getElementById("lstStartHrs_Text").style.width="60px";
    if(document.getElementById("lstEndHrs_Text") != null)
        document.getElementById("lstEndHrs_Text").style.width="60px";
    fnTimeScroll();
}
setTimeout("fnUpdtInfo();",100);
ShowHrs();

   //ZD 100157 Ends
     //ZD 100393 start
    function refreshStyle() {
        var i, a, s;
        a = document.getElementsByTagName('link');
        for (i = 0; i < a.length; i++) {
            s = a[i];
            if (s.rel.toLowerCase().indexOf('stylesheet') >= 0 && s.href) {
                var h = s.href.replace(/(&|\\?)forceReload=d /, '');
                s.href = h + (h.indexOf('?') >= 0 ? '&' : '?') + 'forceReload=' + (new Date().valueOf());
            }
        }
    }
    if (navigator.userAgent.indexOf('Safari') > -1)
        refreshStyle();
    //ZD 100393 End

    //ZD 102008
    function fnSetModalDiv() {
        var w = screen.width;
        var h = screen.height;
        document.getElementById("ModalDiv").style.width = w + 'px';
        document.getElementById("ModalDiv").style.height = h + 'px';        
    }
    fnSetModalDiv();

    function fnHidelbl() {
        if ('<%=Session["hasConference"]%>' == '0' && '<%=Session["hasExpConference"]%>' == "0") {
            if (document.getElementById('CalendarContainer_lblPerCal') != null)
                document.getElementById('CalendarContainer_lblPerCal').style.display = 'none';
        }
    }
    fnHidelbl();

    document.onkeydown = EscClosePopup;
    function EscClosePopup(e) {
        if (e == null)
            var e = window.event;
        if (e.keyCode == 27) {
            if (document.getElementById("btnCancel") != null) 
            document.getElementById("btnCancel").click();
            if (document.getElementById("btnEditCancel") != null) //ZD 101942 
            document.getElementById("btnEditCancel").click();

        }
    }

</script>
<script type="text/javascript" src="inc/softedge.js"></script>

<% if (Request.QueryString["hf"] != null)
   {
       if(Request.QueryString["hf"] != "1" )
       {
%>
        <div class="btprint">        
        <!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
        <%--ZD 101388 Commented--%>
        <%--<!-- FB 2719 Starts -->
        <% if (Session["isExpressUser"].ToString() == "1"){%>
        <!-- #INCLUDE FILE="inc/mainbottomNETExp.aspx" -->
        <%}else{%>
        <!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
        <%}%>
        <!-- FB 2719 Ends -->--%>
        </div>
<%     } 
   }
%>

