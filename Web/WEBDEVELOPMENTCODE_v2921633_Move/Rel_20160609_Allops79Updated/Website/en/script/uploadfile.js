/*ZD 100147 Start*/
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
/*ZD 100147 ZD 100899 End*/
var userfilearray = new Array();
var oparray = new Array();
var rfarray = new Array();

function uploadfile_prompt(promptpicture, prompttitle, fnum) 
{ 
	isTm = ( (document.location.href).indexOf("2template") != -1 ) ;

	promptbox = document.createElement('div'); 
	promptbox.setAttribute ('id' , 'prompt');
	document.getElementsByTagName('body')[0].appendChild(promptbox);
	promptbox = eval("document.getElementById('prompt').style");

	w = 330;
	promptbox.position = 'absolute';
	document.getElementById('prompt').style.top = (isTm) ? (mousedownY + 28) : (mousedownY - 85);
	document.getElementById('prompt').style.left = (isTm) ? (mousedownX - 175) : (mousedownX + 28);
	promptbox.width = w;
	promptbox.border = 'outset 1 #bbbbbb'; 


	m  = "<table cellspacing='0' cellpadding='0' border='0' width='100%'><tr valign='middle'><td width='22' height='22' style='text-indent:2;' class='titlebar'><img src='" + promptpicture + "' height='18' width='18' alt='prompt'></td><td class='titlebar'>" + prompttitle + "</td></tr></table>"  //ZD 100419
	m += "<table cellspacing='0' cellpadding='0' border='0' width='100%' class='promptbox'><tr><td id='dlgcontent'>";
	
	m += "<table border=0>";
	m += "  <tr>";
	m += "  <td colspan='3' class='longprompt'>Only Alphanumeric characters are allowed in filenames/paths.</td>";
	m += "  </tr>";
//alert(fnum);
	for (var i=1; i<=fnum; i++) {
		m += "  <tr>";
		m += "  <td align='right' width='70'><b>File " + i + "</b></td>";
		m += "  <td width='2'></td>";
		if ( (v = document.getElementById("conffile" + i).value) == "" ) {
			m += "  <td>";
			m += "    <input type='text' id='userFile" + i + "' name='userFile" + i + "' value='' style='width: 220px' onClick='JavaScript: openbrowse(" + i + ");' readOnly> <img src='image/browse.gif' onClick='JavaScript: openbrowse(" + i + ");' alt='Browse'>"; //ZD 100419
			m += "  </td>";
		} else {
			m += "  <td id='userfile" + i + "DIV' name='userfile" + i + "DIV'>";
			m += getUploadFileName(v);
			m += "    <img src='image/edit.gif' alt='change' border='0' onclick='chgfile(" + i + ")'>"
			m +="     <img src='image/btn_delete.gif' alt='remove' border='0' onclick='rmvfile(" + i + ")'>"
			m +="   </td>";

		}
		m += "  </tr>"
		
	}

	m += "  <tr><td colspan=3 align='right'>"
	m += "    <input type='button' class='prompt' value='Cancel' onMouseOver='this.style.border=\"1 outset transparent\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='document.getElementsByTagName(\"body\")[0].removeChild(document.getElementById(\"prompt\"))'>"
	m += "    <input type='button' class='longprompt' value='Submit / Upload' onMouseOver='this.style.border=\"1 outset #dddddd\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='uploadfiles(" + fnum + ");'>"
	m += "  </td></tr>"
	m += "</table>" 
	
	m += "</td></tr></table>" 
	document.getElementById('prompt').innerHTML = m;
} 



function uploadfiles(fnum) 
{
	var i, j;
	var hasupload = false;
	var processstr = "";
	var allValid = true;
	var checkOK = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789\\:./-_ ";
	var flname;

	for (i=1; i<=fnum; i++) {

	    if ( (document.getElementById("userFile" + i) != null) && (typeof(document.getElementById("userFile" + i).value) =="string") ) {
		flname = document.getElementById("userFile" + i).value;

		for (k = 0;  k < flname.length;  k++)
		{
			ch = flname.charAt(k);
			for (j = 0;  j < checkOK.length;  j++)
				if (ch == checkOK.charAt(j))
				break;
			if (j == checkOK.length)
			{
				allValid = false;
				break;
			}
		}
	    }
	}
	//alert(allValid);
	
  if (allValid) {

	for (i=1; i<=fnum; i++) {
	    if ( (document.getElementById("userFile" + i) != null) && (typeof(document.getElementById("userFile" + i).value) =="string") ) {
		cb = document.getElementById("userFile" + i);
		rfarray[i] = "";
		
		if (!cb) {
			oparray[i] = false;
			userfilearray[i] = "";
			continue;
		}
		oparray[i] = true;
		userfilearray[i] = Trim(cb.value);
		
		for (j = 1; j < i; j++) {
			if ( (userfilearray[i] == userfilearray[j]) && (userfilearray[i] != "") ) {
				alert(EN_176);
				return -1;
			}
		}
	    }
	}
		
	for (i=1; i<userfilearray.length; i++) {
		if (!oparray[i])
			continue;
			
		if (userfilearray[i] == "") {
			
			if ( (v = document.getElementById("conffile" + i).value) != "" ) {
				processstr += "</td><td align=center valign=top><image src='image/arrow.gif' border=0></td><td>file " + i + " (" + getUploadFileName(v) + ") : remove ... <u><i>done</u></i></td></tr><tr><td>";
				rfarray[i] = getUploadFileName(v);
			}
			
			document.getElementById("conffile" + i).value = "";
			continue;
		}
		
		processstr += "</td><td align=center valign=top><image src='image/arrow.gif' border=0></td><td>file " + i + " (" + getUploadFileName(userfilearray[i]) + ") : upload ... processing</td></tr><tr><td>";
		hasupload = true;
	}
	
	if (processstr == "") {
		document.getElementsByTagName("body")[0].removeChild(document.getElementById("prompt"));
		return -1;
	}
	
	processstr += (hasupload) ? "</td></tr><tr><td colspan=3>&nbsp;&nbsp;&nbsp;&nbsp;<img border='0' src='image/wait1.gif' alt='Loading..'>" : "</td></tr><tr><td align=right colspan=3><input type='button' class='prompt' value='Close' onMouseOver='this.style.border=\"1 outset transparent\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='document.getElementsByTagName(\"body\")[0].removeChild(document.getElementById(\"prompt\"))'>"; //ZD 100419
	
	document.getElementById('dlgcontent').innerHTML = "<table><tr><td>" + processstr + "</td></tr></table>";
	
	if (hasupload) {
		if (ifrmPreloading == null) {
			alert("Error: can not support file upload now. Please notify administrator about this.");
			document.getElementsByTagName("body")[0].removeChild(document.getElementById("prompt"));
			return -1;
		}

		ifrmPreloading.frmsubmit();
	} else {
		genuploadfilestr ();
	}
  }
  else alert(EN_209);
} 



function chgfile (fno)
{
	document.getElementById("userfile" + fno + "DIV").innerHTML = "<input type='text' name='userFile" + fno + "' id='userFile" + fno + "' value='' style='width: 220px' onClick='JavaScript: openbrowse(" + fno + ");' readOnly> <img src='image/browse.gif' onClick='JavaScript: openbrowse(" + fno + ");' alt='browse'>"; //ZD 100419
}



function rmvfile(fno)
{
	var isRemove = confirm(EN_174);
	if (isRemove) {
		chgfile (fno);
	}
}


function openbrowse(fno)
{
	if (ifrmPreloading == null) {
		alert("Error: can not support file upload now. Please notify administrator about this.");
		document.getElementsByTagName("body")[0].removeChild(document.getElementById("prompt"));
		return -1;
	}

	ifrmPreloading.document.getElementById("File" + fno).click();
}


function finishupload(f, infrmlmt, inf1lmt, inf2lmt, inf3lmt, strfrmlmt, strf1lmt, strf2lmt, strf3lmt)
{
	var processstr = "";
	
//alert("infrmlmt=" + infrmlmt +"; inf1lmt=" + inf1lmt +"; inf2lmt=" + inf2lmt +"; inf3lmt=" + inf3lmt +"; ");
	if (!infrmlmt) {
		processstr = "</td><td align=center valign=top><image src='image/arrow.gif' border=0></td><td>file(s) uploads <u><i>failed</u></i>: total size over limit[" + strfrmlmt + "], no files uploaded.</td></tr><tr><td>";
	}
	
	for (i=1; i<userfilearray.length; i++) {
		if (!oparray[i])
			continue;

		if ( rfarray[i] != "" ) {
			processstr += "</td><td align=center valign=top><image src='image/arrow.gif' border=0></td><td>file " + i + " (" + rfarray[i] + ") : remove ... <u><i>done</u></i></td></tr><tr><td>";
			continue;
		}

		if (userfilearray[i] == "")
			continue;

//alert("inf" + i + "lmt=" + eval("inf" + i + "lmt"))

		v = document.getElementById("conffile" + i).value
		oldfn = getUploadFileName(v);		newfn = getUploadFileName(userfilearray[i]);
		if (infrmlmt) {
			substr = (oldfn == newfn) ? ( "new " + oldfn + " overwrite ... " ) : ( (oldfn == "") ? (newfn + " upload ... ") : (oldfn + " remove and " + newfn + " upload ... ") );
			processstr += "</td><td align=center valign=top><image src='image/arrow.gif' border=0></td><td>" + "file " + i + " (" + substr + ") : ";
			processstr += (eval("inf" + i + "lmt")) ? "<u><i>done</u>" : ( "<u><i>failed</u></i>: file size over limit[" + eval("strf" + i + "lmt") + "]" );
			processstr += "</i></td></tr><tr><td>";
		}
	}
	
//alert(processstr)
	if (processstr == "") {
		document.getElementsByTagName("body")[0].removeChild(document.getElementById("prompt"));
		return -1;
	}
	
	processstr += "</td></tr><tr><td align=right colspan=3><input type='button' class='prompt' value='Close' onMouseOver='this.style.border=\"1 outset transparent\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='document.getElementsByTagName(\"body\")[0].removeChild(document.getElementById(\"prompt\"))'>";
	
	document.getElementById('dlgcontent').innerHTML = "<table><tr><td>" + processstr + "</td></tr></table>";
}


function failupload(f)
{
	var processstr = "";

	processstr += "Error: file upload failed.</td></tr><tr><td align=right><input type='button' class='prompt' value='Close' onMouseOver='this.style.border=\"1 outset transparent\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='document.getElementsByTagName(\"body\")[0].removeChild(document.getElementById(\"prompt\"))'>";
	
	document.getElementById('dlgcontent').innerHTML = "<table><tr><td>" + processstr + "</td></tr></table>";
	
	switch (f) {
		case "1" :
			genuploadfilestr ();
			break;
		case "2" :
		case "3" :
			break;
	}
}