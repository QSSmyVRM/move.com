/*ZD 100147 Start*/
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
/*ZD 100147 ZD 100886 End*/
function addOption(cb, txt, val, defsel, sel, optid) 
{
	cb.options[optid] = new Option();
	cb.options[optid].text = txt;
	cb.options[optid].value = val;
	cb.options[optid].defaultSelected = defsel;
	cb.options[optid].selected = sel;
}



var updater=null, updater2=null;

function generateLocCombo(str, cb3, cb2, cb1, selid)
{
	var v1i = new Array();		var v2i = new Array();		var v3i = new Array();
	var v1n = new Array();		var v2n = new Array();		var v3n = new Array();
	var ps1i = new Array();		var ps2i = new Array();
	
	
	selectedloc = ( (tmpstr=str.split("!")[0]) == "" ) ? "" : ( ((tmpary=tmpstr.split(";")).length == 2) ? tmpary[0] : "" );
	locstr = (str.split("!"))[1];
//	alert(selectedloc);
	selectedAry = (selectedloc=="") ? null : selectedloc.split(",");
	levelnum = (locstr.indexOf("^")==-1) ? 3 : ( (locstr.indexOf("+")==-1) ? 2 : 1);

	selectedloc3 = -1;	// 2 & 1
	locary = locstr.split("|");
	for (i=0; i<locary.length; i++) {
		locary[i] = locary[i].split("^");
		
		locary[i][0] = locary[i][0].split("?");
		v3i = v3i.concat(locary[i][0][0]);
		v3n = v3n.concat(locary[i][0][1]);
		
		if (selectedAry != null)
			if (locary[i][0][0] == selectedAry[0])
				selectedloc3=i;
				

		if (locary[i].length == 2) {
			selectedloc2 = -1;	
			locary[i][1] = locary[i][1].split("%");
			v2i_1 = new Array();	v2n_1 = new Array();
			for (j=0; j<locary[i][1].length; j++) { 
				locary[i][1][j] = locary[i][1][j].split("+");
				
				locary[i][1][j][0] = locary[i][1][j][0].split("?");
				v2i_1 = v2i_1.concat(locary[i][1][j][0][0]);
				v2n_1 = v2n_1.concat(locary[i][1][j][0][1]);
				
				if (selectedAry != null)
					if (locary[i][1][j][0][0] == selectedAry[1])
						selectedloc2=j;

				if (locary[i][1][j].length == 2) {
					selectedloc1 = -1;	
					locary[i][1][j][1] = locary[i][1][j][1].split("=");
					v1i_1 = new Array();	v1n_1 = new Array();
							
					for (k=0; k<locary[i][1][j][1].length; k++) {
						locary[i][1][j][1][k] = locary[i][1][j][1][k].split("?");
						v1i_1 = v1i_1.concat(locary[i][1][j][1][k][0]);	// 11
						v1n_1 = v1n_1.concat(locary[i][1][j][1][k][1]);	// 11
					
						if (selectedAry != null)
							if (locary[i][1][j][1][k][0] == selectedAry[2])
								selectedloc1=k;
					}
					v1n_2 = new Array(v1n_1);
					v1n = v1n.concat(v1n_2)
					v1i_2 = new Array(v1i_1);
					v1i = v1i.concat(v1i_2);
					
					ps1i = ps1i.concat((selectedloc1==-1) ? 0 : selectedloc1);
				} else {
					v1n_1 = new Array("");
					v1n_2 = new Array(v1n_1);
					v1n = v1n.concat(v1n_2);
					
					v1i_1 = new Array("");
					v1i_2 = new Array(v1i_1);
					v1i = v1i.concat(v1i_2);
					
					ps1i = ps1i.concat(0);
				}
			}
			v2n_2 = new Array(v2n_1);
			v2n = v2n.concat(v2n_2);
			
			v2i_2 = new Array(v2i_1);
			v2i = v2i.concat(v2i_2);
			ps2i = ps2i.concat((selectedloc2==-1) ? 0 : selectedloc2);

		} else {
			v2n_1 = new Array("");
			v2n_2 = new Array(v2n_1);
			v2n = v2n.concat(v2n_2);
			
			v2i_1 = new Array("");
			v2i_2 = new Array(v2i_1);
			v2i = v2i.concat(v2i_2);
			
			ps2i = ps2i.concat(0);

			if (levelnum==1) {
				v1n_1 = new Array("");
				v1n_2 = new Array(v1n_1);
				v1n = v1n.concat(v1n_2);
				
				v1i_1 = new Array("");
				v1i_2 = new Array(v1i_1);
				v1i = v1i.concat(v1i_2);

				ps1i = ps1i.concat(0);
			}
		}
	}
	
//	alert("selectedloc3:=" + selectedloc3);
//	alert("ps1i:=" + ps1i);
//	alert("v3i:=" + v3i);
//	alert("v1i:=" + v1i);
//	alert("v2n.length=" + v2n.length)
//	alert("v2i:=" + v2i);
//	alert("ps2i:=" + ps2i);
	
	if (selectedloc3== -1) {
		selectedloc3 = 0
	}

	
	for (i=0; i<v3n.length; i++) {
		if (i==selectedloc3)
			addOption(cb3, v3n[i], v3i[i], true, true, i);
		else
			addOption(cb3, v3n[i], v3i[i], false, false, i);
	}
	
	
	// Create the updater for 'int'.
	updater3 = new UpdateDepSel( cb3, cb2, v2n, v2i, ps2i);	
	if (levelnum == 1)
		updater2 = new UpdateDepSel( cb2, cb1, v1n, v1i, ps1i);	

	updater3.updateDependent(null, null);	
	if (levelnum == 1)
		updater2.updateDependent(null, updater3);
}