<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 ZD 100866 End--%>
<%@ Page Language="C#" Inherits="en_ExpressConference.ExpressConference" AutoEventWireup="true" %><%--ZD 100170--%>

<%@ Register Assembly="MetaBuilders.WebControls.ComboBox" Namespace="MetaBuilders.WebControls"
    TagPrefix="mbcbb" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<%--ZD 101388 Commeted--%>
<%--<!-- FB 2719 Starts -->
<% if (Session["isExpressUser"].ToString() == "1"){%>
<!-- #INCLUDE FILE="inc/maintopNETExp.aspx" -->
<%}else{%>
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<%}%>
<!-- FB 2719 Ends -->--%>
<!--Window Dressing--> 

<%--ZD 102909 Start--%>
<style type="text/css">
    
.completionList 
{
    border:solid 1px Gray;
    margin:0px;
    padding:3px;
    height: 120px;
    overflow:auto;
    background-color: #FFFFFF;     
} 
        
.listItem 
{
    color: #191919;
} 
        
.itemHighlighted
{
    background-color: #ADD6FF;       
}
        
.loadingImg
{
    background-image: url(image/wait1.gif);
    background-position: right;
    background-repeat: no-repeat;
}        

</style>
<%--ZD 102909 End--%>

<script type="text/javascript">
    var isIE = false; //ZD 100420
    if (navigator.userAgent.indexOf('Trident') > -1)
        isIE = true;
	//ZD 100604 start
    var img = new Image();
    img.src = "../en/image/wait1.gif";
    //ZD 100604 End

    //ZD 102909 - Start
    //ZD 103265 start
    function OnClientPopulating(sender, e) 
    {       
        //sender._element.className = "loadingImg"; //ZD 103265
    } 
    function OnClientCompleted(sender, e) 
    {           
        sender._element.className = "";        
    }   

    function validate(par) 
    {    
        if(par != null)
        {
            document.getElementById('hdnSelEntityCodeVal').value = par.value;

            //ZD 103265
            if(par.value == "")            
                document.getElementById("hdnRemoveEntityVal").value = "";                            
            else
                document.getElementById("hdnRemoveEntityVal").value = par.value;
                
        }
    }
    //ZD 102909 - End

    // ZD 100335 start
    function setCookie(c_name, value, exdays) {
        var exdate = new Date();
        exdate.setDate(exdate.getDate() + exdays);
        var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
        document.cookie = c_name + "=" + c_value;
    }
    setCookie("hdnscreenres", screen.width, 365);
    //ZD 100335 End
    var servertoday = new Date();
    var hdnCfSt;
    var dFormat;
    dFormat = "<%=format %>";

    var servertoday = new Date(parseInt("<%=DateTime.Today.Year%>", 10), parseInt("<%=DateTime.Today.Month%>", 10) - 1, parseInt("<%=DateTime.Today.Day%>", 10),
        parseInt("<%=DateTime.Today.Hour%>", 10), parseInt("<%=DateTime.Today.Minute%>", 10), parseInt("<%=DateTime.Today.Second%>", 10));

    var maxDuration = 24;
    if ('<%=Application["MaxConferenceDurationInHours"] %>' != "")
        maxDuration = parseInt('<%=Application["MaxConferenceDurationInHours"] %>', 10);

    function fnShowHideAVLink() {
        var args = fnShowHideAVLink.arguments;
        var obj = eval(document.getElementById("LnkAVExpand"));

        if (obj) {
            obj.style.display = 'none';
            if (args[0] == '1') {
                obj.style.display = '';
            }
        }
    }

	//ZD 100704 Starts
    //FB 1825 Start
    function fnHideStartNow() {

        var startNow = document.getElementById('hdnSetStartNow').value;
        var chkrecurrence = document.getElementById('chkRecurrence');
        if (startNow == "hide" || (chkrecurrence && chkrecurrence.checked == true)) {
            document.getElementById("StartNowRow").style.display = "none"; //FB 2634
        }
    }
    //FB 1825 End
	//ZD 100704 End
    //FB 2659 - Starts
    function fnShowSeats() {
        document.getElementById("modalDivPopup").style.display = 'block';
        document.getElementById("modalDivContent").style.display = 'block';
    }

    function fnPopupSeatsClose() {
        document.getElementById("modalDivPopup").style.display = 'none';
        document.getElementById("modalDivContent").style.display = 'none';
        return false;
    }

    function SelectOneDefault(obj) {
        var elements = document.getElementById(obj).innerHTML;
        var a = elements.toUpperCase().split("<BR>"); //Split the dateTime and assign. Start Date/Time: 04/30/2013 08:00<br>End Date/Time: 04/30/2013 09:00<br>Available Seats: 4
        var c = a[0];
        var b = c.split(": ");
        var strStartDate = b[1];

        c = a[1];
        b = c.split(": ");
        var strEndDate = b[1];
        //ZD 100995 start
        if("<%=Session["EmailDateFormat"].ToString()%>" != null &&  "<%=Session["EmailDateFormat"].ToString()%>" == '1')
         {   
            var monthNames =["JAN", "FEB", "MAR", "APR", "MAY", "JUN","JUL", "AUG", "SEP", "OCT", "NOV", "DEC"];
            var month=["01","02","03","04","05","06","07","08","09","10","11","12"]
            
            var confStartDate = strStartDate.split(" ")[0] + "/" + month[monthNames.indexOf(strStartDate.split(" ")[1])] + "/" +strStartDate.split(" ")[2];
            if('<%=Session["FormatDateType"]%>' != null && '<%=Session["FormatDateType"]%>' == 'dd/MM/yyyy')
                document.getElementById("confStartDate").value = confStartDate;
            else
               document.getElementById("confStartDate").value = GetDefaultDate(confStartDate);
                            
            if(strStartDate.split(" ")[4] != null)
                document.getElementById("confStartTime_Text").value = strStartDate.split(" ")[3] + " " + strStartDate.split(" ")[4];
            else    
                document.getElementById("confStartTime_Text").value = strStartDate.split(" ")[3];
                
            var confEnDate = strEndDate.split(" ")[0] + "/" + month[monthNames.indexOf(strEndDate.split(" ")[1])] + "/" +strEndDate.split(" ")[2];
            if('<%=Session["FormatDateType"]%>' != null && '<%=Session["FormatDateType"]%>' == 'dd/MM/yyyy')
                document.getElementById("confEndDate").value = confEnDate;
            else
               document.getElementById("confEndDate").value = GetDefaultDate(confEnDate);
               
            if(strEndDate.split(" ")[4] != null)
                document.getElementById("confEndTime_Text").value = strEndDate.split(" ")[3] + " " + strEndDate.split(" ")[4];
            else    
                document.getElementById("confEndTime_Text").value = strEndDate.split(" ")[3];  
         }
        else //ZD 100995 end
        {
        document.getElementById("confStartDate").value = strStartDate.split(" ")[0];
        if (strStartDate.split(" ")[2] != null)
            document.getElementById("confStartTime_Text").value = strStartDate.split(" ")[1] + " " + strStartDate.split(" ")[2];
        else
            document.getElementById("confStartTime_Text").value = strStartDate.split(" ")[1];

        document.getElementById("confEndDate").value = strEndDate.split(" ")[0];
        if (strEndDate.split(" ")[2] != null)
            document.getElementById("confEndTime_Text").value = strEndDate.split(" ")[1] + " " + strEndDate.split(" ")[2];
        else
            document.getElementById("confEndTime_Text").value = strEndDate.split(" ")[1];
         }
        fnPopupSeatsClose();
    }  
    //FB 2659 - End

    //ZD 100568 21-12-2013 Inncrewin
    function fnShowPopupConfAlert() {
        document.getElementById("modalDivPopup").style.display = 'block';
        document.getElementById("modalDivContentforConf").style.display = 'block';
    }

    function fnPopupConfAlertClose() {
        document.getElementById("modalDivPopup").style.display = 'none';
        document.getElementById("modalDivContentforConf").style.display = 'none';
        return false;
    }
    //ZD 100568 21-12-2013 Inncrewin
    
    //FB 3055-Filter in Upload Files Starts

    function checkTipoFileInput(fileTypes) {

        var fileUpload1 = document.getElementById("FileUpload1");
        var fileUpload2 = document.getElementById("FileUpload2");
        var fileUpload3 = document.getElementById("FileUpload3");
        var retError = false;

        if ((fileUpload1 != null && (fileUpload1.value == "" || fileUpload1.value.length < 3)) && (fileUpload2 != null && (fileUpload2.value == "" || fileUpload2.value.length < 3)) && (fileUpload3 != null && (fileUpload3.value == "" || fileUpload3.value.length < 3)))
            return false;

        if (fileUpload1 != null && fileUpload1.value != "") {
            dots = fileUpload1.value.split(".");
            fileType = "." + dots[dots.length - 1];

            if (fileTypes.join(".").indexOf(fileType.toLowerCase()) == -1)
                retError = true;
        }
        if (fileUpload2 != null && fileUpload2.value != "") {
            dots = fileUpload2.value.split(".");
            fileType = "." + dots[dots.length - 1];
            if (fileTypes.join(".").indexOf(fileType.toLowerCase()) == -1)
                retError = true;
        }
        if (fileUpload3 != null && fileUpload3.value != "") {
            dots = fileUpload3.value.split(".");
            fileType = "." + dots[dots.length - 1];
            if (fileTypes.join(".").indexOf(fileType.toLowerCase()) == -1)
                retError = true;
        }
        if (retError) {
            alert(RFileTypeValid);
            return false;
        }
    }
    //FB 3055-Filter in Upload Files - End

    function SelectAudioParty() {
        if (document.getElementById("txtAudioDialNo") != null)
            document.getElementById("txtAudioDialNo").value = "";

        if (document.getElementById("txtConfCode") != null)
            document.getElementById("txtConfCode").value = "";

        if (document.getElementById("txtLeaderPin") != null)
            document.getElementById("txtLeaderPin").value = "";

        if (document.getElementById("txtPartyCode") != null) //ZD 101446
            document.getElementById("txtPartyCode").value = "";

        document.getElementById("lblError2").innerText = "";
        document.getElementById("lblError3").innerText = "";
        document.getElementById("lblError4").innerText = "";
        document.getElementById("lblErrorParty").innerText = ""; //ZD 101446

        RegtxtAudioDialNo.style.display = "none";
        RegtxtConfCode.style.display = "none";
        RegtxtLeaderPin.style.display = "none";
        RegtxtPartyCode.style.display = "none"; //ZD 101446
        var audins = "";//FB 2341
        var count; 
        var audinsitems = document.getElementsByName("lstAudioParty"); 
        for (var i=0; i < audinsitems.length; i++)
        {
            if (audinsitems[i].checked)
            {
                audins = audinsitems[i].value;
                //alert(audins);
            }
        }
        if (audins == -1) {
            document.getElementById("hdnAudioInsID").value = "";
            if(document.getElementById("txtAudioDialNo") != null) document.getElementById("txtAudioDialNo").readOnly = true;
            document.getElementById("txtConfCode").readOnly = true;
            document.getElementById("txtLeaderPin").readOnly = true;
            document.getElementById("txtPartyCode").readOnly = true; //ZD 101446
        }
        else {

            document.getElementById("hdnSelectedAudioDetails").value = audins; //ALLDEV-814
            party = audins.split("|");
            //alert(party);
            if (party.length > 0) {
                document.getElementById("hdnAudioInsID").value = party[0];
                document.getElementById("txtAudioDialNo").readOnly = false;
                if(party[10] == "1")
                {
                    document.getElementById("txtConfCode").readOnly = false;
                    document.getElementById("txtLeaderPin").readOnly = false;
                    document.getElementById("txtPartyCode").readOnly = false;
                }
                else
                {
                    //document.getElementById("txtAudioDialNo").readOnly = true;
                    document.getElementById("txtConfCode").readOnly = true;
                    document.getElementById("txtLeaderPin").readOnly = true;
                    document.getElementById("txtPartyCode").readOnly = true;
                }
                document.getElementById("txtAudioDialNo").value = party[2];

                //ZD 104289 - Start
                var dialString = "";
                var arydialString
                if(document.getElementById("hdnDialString"))
                    dialString = document.getElementById("hdnDialString").value;
                
                arydialString = dialString.split('�');
                if(party[3] == "" && dialString != "" && party[10] == "1") //ALLDEV-814
                    document.getElementById("txtConfCode").value = arydialString[0];
                else
                    document.getElementById("txtConfCode").value = party[3];

                if(party[4] == "" && dialString != "" && party[10] == "1")//ALLDEV-814
                    document.getElementById("txtLeaderPin").value = arydialString[1];
                else
                    document.getElementById("txtLeaderPin").value = party[4];
                    
                if(party[5] == "" && dialString != "" && party[10] == "1")//ALLDEV-814
                    document.getElementById("txtPartyCode").value = arydialString[2];//ZD 101446
                else
                    document.getElementById("txtPartyCode").value = party[5]; //ZD 101446
                        
                //ZD 104289 - End
            }
        }
    }

    function fnShowAVParams() {
        var obj = eval(document.getElementById("trAVCommonSettings"));
        var obj2 = eval(document.getElementById("trSelRoomsTitle"));
        var obj3 = eval(document.getElementById("trSelRoomsDetails"));
        var linkState = eval(document.getElementById("hdnAVParamState"));
        var expandlink = eval(document.getElementById("LnkAVExpand"));

        if (linkState) {
            if (obj) {
                obj.style.display = 'none';
                obj2.style.display = 'none';
                obj3.style.display = 'none';
                if (linkState.value == '') {
                    obj.style.display = '';
                    obj2.style.display = '';
                    obj3.style.display = '';
                    linkState.value = '1';

                    if (expandlink) {
                        expandlink.innerText = 'Collapse';
                    }
                }
                else {
                    obj.style.display = 'none';
                    obj2.style.display = 'none';
                    obj3.style.display = 'none';

                    linkState.value = '';
                    if (expandlink) {
                        expandlink.innerText = 'Expand';
                    }
                }
            }
        }
        return false;
    }

    function fnShowHideMeetLink() {
        var args = fnShowHideMeetLink.arguments;
        var obj = eval(document.getElementById("LnkMeetExpand"));

        if (obj) {
            obj.style.display = 'none';
            if (args[0] == '1') {
                obj.style.display = '';
            }
        }
    }

    //FB 2426 Start

    function ValidateflyEndpoints()
	{
    

	  var ret = true; var ConnectionType =2; var MCU =8, AddressType =0, Address=0;
      var ErrorTxt="";
      var CheckIP = /^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/;
      //var CheckISDN =/^[0-9]+$/;   /^[0-9]+$/;

      var CheckISDN = /^\d+/

	  ValidatorEnable(document.getElementById("reqRoomName"), true);
      ValidatorEnable(document.getElementById("regRoomName"), true);
      ValidatorEnable(document.getElementById("reqcontactName"), true);   
      ValidatorEnable(document.getElementById("reqcontactEmail"), true); 
      var dgProfileCount = document.getElementById('dgProfiles').rows.length;
      for(var i=2; i<= dgProfileCount; i++)
      {
        if(document.getElementById("dgProfiles_ctl0" + i + "_lstGuestConnectionType1"))
            ConnectionType = document.getElementById("dgProfiles_ctl0" + i + "_lstGuestConnectionType1").selectedIndex;
       if(document.getElementById("dgProfiles_ctl0" + i + "_lstBridgeType"))
            MCU = document.getElementById("dgProfiles_ctl0" + i + "_lstBridgeType").selectedIndex;
             
        ValidatorEnable(document.getElementById("dgProfiles_ctl0" + i + "_reqGuestAddress"), false);
        if(ConnectionType == 1)
        {
            if(MCU ==8)
                ValidatorEnable(document.getElementById("dgProfiles_ctl0" + i + "_reqGuestAddress"), true);
        }
        else
            ValidatorEnable(document.getElementById("dgProfiles_ctl0" + i + "_reqGuestAddress"), true); 


            
        if(document.getElementById("dgProfiles_ctl0" + i + "_lstGuestAddressType"))
            AddressType = document.getElementById("dgProfiles_ctl0" + i + "_lstGuestAddressType").selectedIndex;
       if(document.getElementById("dgProfiles_ctl0" + i + "_txtGuestAddress"))
            Address = document.getElementById("dgProfiles_ctl0" + i + "_txtGuestAddress").value;
       if(document.getElementById("dgProfiles_ctl0" + i + "_regIPAddress"))
            ErrorTxt = document.getElementById("dgProfiles_ctl0" + i + "_regIPAddress").value;

           ValidatorEnable(document.getElementById("dgProfiles_ctl0" + i + "_regIPAddress"), true); 
           document.getElementById("dgProfiles_ctl0" + i + "_regIPAddress").enabled = true;
            if(AddressType == 1)
            {
                if(CheckIP.test(Address))
                    ValidatorEnable(document.getElementById("dgProfiles_ctl0" + i + "_regIPAddress"), false); 
            }else  if(AddressType == 4)
            {
                    CheckISDN = isFinite(Address);
                    if(CheckISDN)
                        ValidatorEnable(document.getElementById("dgProfiles_ctl0" + i + "_regIPAddress"), false); 
            }
            else
                ValidatorEnable(document.getElementById("dgProfiles_ctl0" + i + "_regIPAddress"), false); 

       }     
       //ALLBUGS-111
       if(Page_ClientValidate("SubmitG") == false)
            return false;

//        if (!Page_ClientValidate())
//               return Page_IsValid;
                  
	  return true;
	}

    function GusetLocClose()
    {
        document.getElementById("txtsiteName").value = "";
        document.getElementById("txtApprover5").value = "";
        document.getElementById("txtEmailId").value = "";
        document.getElementById("txtPhone").value = "";
        document.getElementById("txtContactPhone").value = "";

        document.getElementById("txtAddress").value = "";
        document.getElementById("txtState").value = "";
        document.getElementById("txtCity").value = "";
        document.getElementById("txtZipcode").value = "";
        document.getElementById("lstCountries").value = -1;//ZD 100484
        
        var dgProfileCount = document.getElementById('dgProfiles').rows.length;
        if(dgProfileCount > 2)
        {
            for(var i=2; i< dgProfileCount; i++)
            {
                //document.getElementById('dgProfiles').rows(i+1)="";
                document.getElementById('dgProfiles').deleteRow(2);
            }
        }
        dgProfileCount = document.getElementById('dgProfiles').rows.length;
        for(var i=2; i<= dgProfileCount; i++)
        {
            document.getElementById("dgProfiles_ctl0" + i + "_txtGuestAddress").value = "";
            document.getElementById("dgProfiles_ctl0" + i + "_lstGuestConnectionType1").selectedIndex = 2; 
            document.getElementById("dgProfiles_ctl0" + i + "_lstGuestAddressType").selectedIndex = 1; 
            document.getElementById("dgProfiles_ctl0" + i + "_lstGuestBridges").selectedIndex = 0;  
        }
        document.getElementById("btnGuestLocationSubmit").value = RbtnGuestLocationSubmit;
        document.getElementById("hdnGuestRoom").value = "-1";
    }

    
    function deleteAssistant() {
        document.getElementById("txtApprover5").value = "";
        document.getElementById("txtEmailId").value = "";
    }

    function fnClearGuestGrid()
     {
        document.getElementById("txtsiteName").value = "";
        document.getElementById("txtApprover5").value = "";
        document.getElementById("txtEmailId").value = "";
        document.getElementById("txtPhone").value = "";
        document.getElementById("txtContactPhone").value = "";

        document.getElementById("txtAddress").value = "";
        document.getElementById("txtState").value = "";
        document.getElementById("txtCity").value = "";
        document.getElementById("txtZipcode").value = "";
        document.getElementById("lstCountries").value = -1;//ZD 100484
        var dgProfileCount = document.getElementById('dgProfiles').rows.length;
        for(var i=2; i<= dgProfileCount; i++)
        {
              document.getElementById("dgProfiles_ctl0" + i + "_txtGuestAddress").value = "";
//            document.getElementById("dgProfiles_ctl0" + i + "_lstGuestConnectionType1").selectedIndex = 2; 
//            document.getElementById("dgProfiles_ctl0" + i + "_lstGuestAddressType").selectedIndex = 1; 
//            document.getElementById("dgProfiles_ctl0" + i + "_lstGuestBridges").selectedIndex = 0;  
        }
       
        document.getElementById("btnGuestLocationSubmit").value = RbtnGuestLocationSubmit;
        document.getElementById("hdnGuestRoom").value = "-1";
    }

    function fnValidator()
    {
        fnClearGuestGrid(); 
        document.getElementById("expColStat").value =0;
        var ConnectionType =2,MCU=8;
        document.getElementById("reqRoomName").enabled = "true";
        document.getElementById("regRoomName").enabled = "true";
        document.getElementById("reqcontactName").enabled = "true"; 
        document.getElementById("reqcontactEmail").enabled = "true"; 
        var dgProfileCount = document.getElementById('dgProfiles').rows.length;
        for(var i=2; i<= dgProfileCount; i++)
        {

             if(document.getElementById("dgProfiles_ctl0" + i + "_lstGuestConnectionType1"))
                ConnectionType = document.getElementById("dgProfiles_ctl0" + i + "_lstGuestConnectionType1").selectedIndex;
             if(document.getElementById("dgProfiles_ctl0" + i + "_lstBridgeType"))
                MCU = document.getElementById("dgProfiles_ctl0" + i + "_lstBridgeType").selectedIndex;
             
            
            document.getElementById("dgProfiles_ctl0" + i + "_reqGuestConnectionType").enabled = "true"; 
            document.getElementById("dgProfiles_ctl0" + i + "_reqGuestAddressType").enabled = "true"; 
            document.getElementById("dgProfiles_ctl0" + i + "_reqGuestLineRate").enabled = "true"; 

             document.getElementById("dgProfiles_ctl0" + i + "_reqGuestAddress").enabled = "false";  
            if(ConnectionType == 1)
            {
                if(MCU ==8)
                    ValidatorEnable(document.getElementById("dgProfiles_ctl0" + i + "_reqGuestAddress"), true);
            }
            //else
                //ValidatorEnable(document.getElementById("dgProfiles_ctl0" + i + "_reqGuestAddress"), true); 
        }

        document.getElementById("btnGuestLocation2").click(); // FB 2525
        return false; // FB 2525
    }

    //ZD 101268 //ZD 101318 Starts
    function CheckIPAddress(obj, par)
    {
        var connectionValue = "";
        var Address;
        var AddressTypeChg;//ZD 104068
        if(par == 0)
        {
            Address = document.getElementById(obj.id.replace("txtGuestAddress", "lstGuestAddressType"));            
            AddressTypeChg = document.getElementById(obj.id.replace("txtGuestAddress", "hdnGuestAddrTypeChg"));//ZD 104068
        }
        else if(par == 1)
        {
            Address = document.getElementById(obj.id.replace("txtAddress", "lstAddressType"));
            connectionValue = document.getElementById(obj.id.replace("txtAddress", "lstConnection"));
            AddressTypeChg = document.getElementById(obj.id.replace("txtAddress", "hdnAddrTypeChg"));//ZD 104068
        }
        var e = document.getElementById(Address.id);

        var CheckIP = /^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/;

        if(CheckIP.test(obj.value))
            e.options[1].selected = true;
        else
        {
            if(AddressTypeChg.value != 1)//ZD 104068
            {        
                if(par == 1 && connectionValue.value == "1")
                    e.options[4].selected = true;
                else
                    e.options[0].selected = true;
            }
        }
    }
    //ZD 101318 End

    //ZD 104068 - Start
    function fnAddresstypechg(obj, par)
    {        
        var Address;
        if(par == 0)
            Address = document.getElementById(obj.id.replace("lstGuestAddressType", "hdnGuestAddrTypeChg"));
        else if(par == 1)
            Address = document.getElementById(obj.id.replace("lstAddressType", "hdnAddrTypeChg"));                           
        
        document.getElementById(Address.id).value = 1;
    }
    //ZD 104068 - End

    function fnDisableValidator()
    {
        ValidatorEnable(document.getElementById("reqRoomName"), false);
        ValidatorEnable(document.getElementById("regRoomName"), false);
        ValidatorEnable(document.getElementById("reqcontactName"), false); 
        ValidatorEnable(document.getElementById("reqcontactEmail"), false); 
        ValidatorEnable(document.getElementById("regTestemail"), false);
        ValidatorEnable(document.getElementById("regcontactEmail"), false);
        //ValidatorEnable(document.getElementById("regcontactEmail"), false); 
        ValidatorEnable(document.getElementById("reqAddress2"), false);
        ValidatorEnable(document.getElementById("reqState"), false);
        ValidatorEnable(document.getElementById("reqCity"), false); 
        ValidatorEnable(document.getElementById("reqZipcode"), false); 
        ValidatorEnable(document.getElementById("reqPhone"), false); 
        var dgProfileCount = document.getElementById('dgProfiles').rows.length;

        for(var i=2; i<= dgProfileCount; i++)
        {
            ValidatorEnable(document.getElementById("dgProfiles_ctl0" + i + "_reqGuestAddress"), false);            
            ValidatorEnable(document.getElementById("dgProfiles_ctl0" + i + "_regIPAddress"), false);   
            ValidatorEnable(document.getElementById("dgProfiles_ctl0" + i + "_reqGuestConnectionType"), false);   
            ValidatorEnable(document.getElementById("dgProfiles_ctl0" + i + "_reqGuestLineRate"), false);   
            //ValidatorEnable(document.getElementById("dgProfiles_ctl0" + i + "_reqGuestAddress"), false);   
            
        }    
        //GusetLocClose();    
        document.getElementById("btnGuestLocationSubmit").value = RbtnGuestLocationSubmit;
        document.getElementById("hdnGuestloc").value = "";
        document.getElementById("hdnGuestRoomID").value = "";
        
        //__doPostBack('btnUpdtGrid',''); //ALLBUGS-111
        document.getElementById("hdnSaveData").value = "1";
		//ALLBUGS-111
        document.getElementById('PopupLdapPanel').style.display = "none";
        if(document.getElementById('btnGuestLocation2_backgroundElement') != null)
            document.getElementById('btnGuestLocation2_backgroundElement').style.display = "none";

    }   

    //FB 2426 End
    
    function fnShowMeetPlanner() {
        var obj = eval(document.getElementById("tdMeetingPlanner"));
        var linkState = eval(document.getElementById("hdnMeetLinkSt"));
        var expandlink = eval(document.getElementById("LnkMeetExpand"));

        if (linkState) {
            if (obj) {
                obj.style.display = 'none';
                if (linkState.value == '') {
                    obj.style.display = '';
                    linkState.value = '1';

                    if (expandlink) {
                        expandlink.innerText = 'Collapse';
                    }
                }
                else {
                    obj.style.display = 'none';
                    linkState.value = '';
                    if (expandlink) {
                        expandlink.innerText = 'Expand';
                    }
                }
            }
        }
        return false;
    }

    function CheckUploadedFiles() {
        if (document.getElementById("FileUpload1") != null && document.getElementById("FileUpload2") != null && document.getElementById("FileUpload3") != null) {
            if (document.getElementById("FileUpload1").value == "" && document.getElementById("FileUpload2").value == "" && document.getElementById("FileUpload3").value == "") {
                document.getElementById("aFileUp").innerHTML = RAddDocument;
            }
        }
    }

    function fnOpenGuest() {
        window.open('ManageInterSPGuest.aspx?frm=frmSettings2', 'spGuest', 'titlebar=yes,width=550,height=350,resizable=yes,scrollbars=yes,status=yes');
    }

    function fnOpenRemote() {
        var traccess = document.getElementById("trAccessnumber");
        var chkacess = document.getElementById("chkRemoteSP");
        var trintersp = document.getElementById("trInterSpGuest");

        if (chkacess) {
            if (chkacess.checked) {
                traccess.style.display = ""; //block
                trintersp.style.display = "none";
                document.getElementById("NONRecurringConferenceDiv8").style.display = "none";

            }
            else {
                traccess.style.display = "none";
                document.getElementById("NONRecurringConferenceDiv8").style.display = ""; //block
                trintersp.style.display = ""; //block

            }
        }

    }

    //FB 2501 Starts
    function deleteApprover(id) {
        eval("document.getElementById('hdnApprover" + (id) + "')").value = "";
        eval("document.getElementById('txtApprover" + (id) + "')").value = "";
    }
    //FB 2501 Ends
	//ZD 100221 Starts
    function fnShowWebExConf() {
        //ZD 101837
        var ConfRecurLimit = document.getElementById('spnRecurLimit').innerHTML;
        if (document.getElementById("chkWebex") != null) {
            if (document.getElementById("chkWebex").checked) {
				//ZD 100221 Starts
                document.getElementById("divWebPW").style.display = "";
                document.getElementById("divWebCPW").style.display = "";
				//ZD 100221 Ends
                ValidatorEnable(document.getElementById('RequiredFieldValidator7'), true);//ZD 100221_17Feb 

                //ZD 101837
                if(ConfRecurLimit > 50)
                    document.getElementById('spnRecurLimit').innerHTML = 50;
            }
            else {
                document.getElementById('spnRecurLimit').innerHTML = <%=Session["ConfRecurLimit"]%>;
                document.getElementById("divWebPW").style.display = "none";
                document.getElementById("divWebCPW").style.display = "none";
                ValidatorEnable(document.getElementById('RequiredFieldValidator7'), false);//ZD 100221_17Feb 
            }
        }
    }
    function PasswordChange(par) {

        document.getElementById("hdnPasschange").value = true;

        if (par == 1) {
            document.getElementById("hdnPW1Visit").value = true;
        }
        else
            document.getElementById("hdnPW2Visit").value = true;
        document.getElementById("hdnWebExPwd").value = document.getElementById("txtPW1").value; // ZD PRABU
    }
    function fnTextFocus(xid, par) {

        var obj = document.getElementById(xid);

        if (par == 1) {
            if (document.getElementById("hdnPW2Visit") != null) {
                if (document.getElementById("hdnPW2Visit").value == "false") {
                    document.getElementById("txtPW1").value = "";
                    document.getElementById("txtPW2").value = "";
                } else {
                    document.getElementById("txtPW1").value = "";
                }
            }
            else {
                document.getElementById("txtPW1").value = "";
                document.getElementById("txtPW2").value = "";
            }
        }
        else {
            if (document.getElementById("hdnPW1Visit") != null) {
                if (document.getElementById("hdnPW1Visit").value == "false") {
                    document.getElementById("txtPW1").value = "";
                    document.getElementById("txtPW2").value = "";
                }
                else {
                    document.getElementById("txtPW2").value = "";
                }

            }
            else {
                document.getElementById("txtPW2").value = "";
            }
        }

        if (document.getElementById("reqPassword1") != null) {
            ValidatorEnable(document.getElementById('reqPassword1'), false);
            ValidatorEnable(document.getElementById('reqPassword1'), true);
        }
//        if (document.getElementById("cmpValPassword1") != null) { //ZD 102692
//            ValidatorEnable(document.getElementById('cmpValPassword1'), false);
//            ValidatorEnable(document.getElementById('cmpValPassword1'), true);
//        }
        if (document.getElementById("reqPassword2") != null) {
            ValidatorEnable(document.getElementById('reqPassword2'), false);
            ValidatorEnable(document.getElementById('reqPassword2'), true);
        }
        if (document.getElementById("cmpValPassword2") != null) {
            ValidatorEnable(document.getElementById('cmpValPassword2'), false);
            ValidatorEnable(document.getElementById('cmpValPassword2'), true);
        }
    }
    //ZD 100221 Ends
 //ZD 101226
 function DataLoading(val) {
 
        if (val == "1") {
            document.getElementById("dataLoadingDIV").style.display = 'block'; //ZD 100678
            document.body.style.cursor = "wait";
        }
        else {
            document.getElementById("dataLoadingDIV").style.display = 'none';  //ZD 100678
            document.body.style.cursor = "none";
        }
    }
    //ZD 100522 Starts
    function ChangeRoomVMRPIN()
    {
        document.getElementById("hdnROOMPIN").value = "1";
    
        if(document.getElementById("trVMRPIN1") != null)
            document.getElementById("trVMRPIN1").style.display  = '';
        if(document.getElementById('trVMRPIN2') != null)
            document.getElementById('trVMRPIN2').style.display  = '';
    }
     function ConfPWDLen(par)
    {
        var passlen = "4", obj1 ="",obj2="",obj3 = ""; //ZD 102692
    
        if(document.getElementById("hdnPINlen")!= null && document.getElementById("hdnPINlen").value != "")
            passlen = document.getElementById("hdnPINlen").value ;

        obj1 = document.getElementById("ConferencePassword");
        ValidatorEnable(document.getElementById("numPassword1"), false);
        //ZD 103843
        var numbers = /^[0-9]+$/;
        if(!obj1.value.match(numbers)) 
        {
            ValidatorEnable(document.getElementById("numPassword1"), true);
        }

        if( (obj1.value != "" && obj1.value.length < passlen))
        {
            //alert("Passwords should have a minimum of "+ passlen +" characters");
            ValidatorEnable(document.getElementById("numPassword1"), true);
        }

        obj3 = document.getElementById("ConferencePassword2"); //ZD 102692
        if(obj1.value != obj3.value)
        {
         if(document.getElementById("cmpValPassword") != null)
            document.getElementById("cmpValPassword").style.display = "inline";
        }
        return  true;
    }

    //ZD 102692 - Start
    function ConfpwdValidation()
    {
        var confPwd1 = "", confPwd2 = "";
        confPwd1 = document.getElementById("ConferencePassword");
        confPwd2 = document.getElementById("ConferencePassword2"); 

        if(confPwd1.value != confPwd2.value)
        {
         if(document.getElementById("cmpValPassword") != null)
            document.getElementById("cmpValPassword").style.display = "inline";
        }
    }
    //ZD 102692 - End

    function VMRPWDLen()
    {
        var passlen = "4", obj1 ="",obj2="";

        if(document.getElementById("hdnisBJNConf")!= null && document.getElementById("hdnisBJNConf").value == "1") //ZD 103263
            return true;
    
        if(document.getElementById("hdnPINlen")!= null && document.getElementById("hdnPINlen").value != "")
            passlen = document.getElementById("hdnPINlen").value ;

        obj1 = document.getElementById("txtVMRPIN1");
        ValidatorEnable(document.getElementById("RegVMRPIN"), false);
 
        if( (obj1.value != "" && obj1.value.length < passlen))
        {
            //alert("Passwords should have a minimum of "+ passlen +" characters");
            ValidatorEnable(document.getElementById("RegVMRPIN"), true);
//                if(document.getElementById("numPassword1") != null)
//                {
//                    document.getElementById("numPassword1").style.display = "";
//                    document.getElementById("numPassword1") = "<span id=\"numPassword1\" style=\"color: red;\">Minimum Length is " + hdnPINlen.Value + ". First digit should be non-zero.</span>";
//                    }
//            return false;
        }
        return  true;
    }
    //ZD 100522 Ends

    //ALLDEV-826 Starts
    function HostPWDLen()
    {
        var minPwdLength = "4", hostPwd1 ="", hostPwd2="";
    
        if(document.getElementById("hdnPINlen")!= null && document.getElementById("hdnPINlen").value != "")
            minPwdLength = document.getElementById("hdnPINlen").value ;

        hostPwd1 = document.getElementById("txtHostPwd");
        hostPwd2 = document.getElementById("txtCfmHostPwd");
        ValidatorEnable(document.getElementById("regValHostPwd"), false);
        
        if(document.getElementById("ConferencePassword") != null)
        {
            if (document.getElementById("txtHostPwd") != null) 
            {
                var confPwd = "", reqValHostPwd = "";
                confPwd = document.getElementById("ConferencePassword");
                reqValHostPwd = document.getElementById("reqValHostPwd");

                if (confPwd.value != "" && hostPwd1.value == "") {
                    ValidatorEnable(reqValHostPwd, true);
                }
                else{
                    ValidatorEnable(reqValHostPwd, false);
                }
                if (document.getElementById('lstVMR') != null) {
                    if (document.getElementById('lstVMR').value == "2") {                    
                        ValidatorEnable(reqValHostPwd, false);
                    }
                }
            }
        }

        var numbers = /^[0-9]+$/;
        if(!hostPwd1.value.match(numbers)) 
        {
            ValidatorEnable(document.getElementById("regValHostPwd"), true);
        }

        if( (hostPwd1.value != "" && hostPwd1.value.length < minPwdLength))
        {
            ValidatorEnable(document.getElementById("regValHostPwd"), true);
        }
                
        if(hostPwd1.value != hostPwd2.value)
        {
            if(document.getElementById("cmpValHostPwd") != null)
                document.getElementById("cmpValHostPwd").style.display = "inline";
        }
        return  true;
    }

    function HostPwdValidation()
    {
        var hostPwd1 = document.getElementById("txtHostPwd").value;
        var hostPwd2 = document.getElementById("txtCfmHostPwd").value;
        if(hostPwd1 != hostPwd2)
        {
            if(document.getElementById("cmpValHostPwd") != null)
                document.getElementById("cmpValHostPwd").style.display = "inline";
        }
    }

    function chkRequiredHostPwd()
    {
        if(document.getElementById("ConferencePassword") != null)
        {
            if (document.getElementById("txtHostPwd") != null) 
            {
                var confPwd = "", hostPwd = "", reqValHostPwd = "";
                confPwd = document.getElementById("ConferencePassword");
                hostPwd = document.getElementById("txtHostPwd");
                reqValHostPwd = document.getElementById("reqValHostPwd");

                if (confPwd.value != "" && hostPwd.value == "") {
                    ValidatorEnable(reqValHostPwd, true);
                }
                else
                {
                    ValidatorEnable(reqValHostPwd, false);
                }
                if (document.getElementById('lstVMR') != null) {
                    if (document.getElementById('lstVMR').value == "2") {                    
                        ValidatorEnable(reqValHostPwd, false);
                    }
                }
            }
        }
    }

    function host_gen()
    {        
        var num =  randam();
        if (num.indexOf(0) == 0) 
            num = num.replace(0,1);
                  
        if(document.getElementById("cmpValHostPwd") != null)
            document.getElementById("cmpValHostPwd").style.display = "none";
        if(document.getElementById("regValHostPwd") != null)
            document.getElementById("regValHostPwd").style.display = "none";

        var hostPwd1 = document.getElementById("txtHostPwd");
        var hostPwd2 = document.getElementById("txtCfmHostPwd");
        if(hostPwd1 != null)
            hostPwd1.value = num;
        if(hostPwd2 != null)
            hostPwd2.value = num;

        SaveHostPassword(); 
    }
        
    function SaveHostPassword()
    {
      document.getElementById("<%=hdnHostPassword.ClientID %>").value = document.getElementById("<%=txtHostPwd.ClientID %>").value;
    }
    //ALLDEV-826 Ends
</script>

<script type="text/javascript" language="JavaScript" src="script/errorList.js"></script>

<script type="text/javascript" language="javascript1.1" src="extract.js"></script>

<script language="VBScript" src="script/lotus.vbs"></script>

<script type="text/javascript" src="../<%=Session["language"] %>/inc/functions.js"></script>

<script type="text/javascript" src="script/mytreeNET.js"></script>

<script type="text/javascript" src="script/settings2.js"></script>

<script type="text/vbscript" src="script/settings2.vbs"></script>

<script type="text/javascript" src="script/mousepos.js"></script>

<script type="text/javascript" src="script/cal-flat.js"></script>

<script type="text/javascript" src="script/calview.js"></script>

<script type="text/javascript" src="../<%=Session["language"] %>/lang/calendar-en.js"></script>

<script type="text/javascript" src="script/calendar-setup.js"></script>

<script language="VBScript" src="script/outlook.vbs" type="text/vbscript"></script>

<script type="text/javascript" src="script/saveingroup.js"></script>

<script type="text/javascript" src="script/calendar-flat-setup.js"></script>

<script type="text/javascript" src="script/RoomSearch.js"></script> <%--FB 2367--%>
<%--FB 2693--%>
<script type="text/javascript" src="script/CallMonitorJquery/jquery.1.4.2.js" ></script>
<script type="text/javascript" src="script/CallMonitorJquery/jquery.bpopup-0.7.0.min.js"></script>
<script type="text/javascript" src="script/pcconference.js"></script>
<script type="text/javascript" src="script/CallMonitorJquery/json2.js" ></script>

<script language="javascript" type="text/javascript">

    function callalert(val) {
        alert(val);
        return false;
    }

    function OpenSetRoom() {

        var lb = document.getElementById("RoomList"); //FB 2367
        if (lb != null) {

            for (var i = lb.options.length - 1; i >= 0; i--)
                lb.options[i] = null;

            lb.selectedIndex = -1;

            lb.disabled = true;
        }
        if (document.getElementById("btnRoomSelect"))
            document.getElementById("btnRoomSelect").click();
    }


    function ProccedClick() {

        /*var lb = document.getElementById("seltdRooms");
        if(lb != null)
        {
        
        for (var i=lb.options.length-1; i>=0; i--)    
        lb.options[i] = null;
        
        lb.selectedIndex = -1;
             
        lb.disabled=true;
        }
        if(document.getElementById("btnRoomSelect"))
        document.getElementById("btnRoomSelect").click();*/

        if (document.getElementById("ClosePUpbutton"))
            document.getElementById("ClosePUpbutton").click();

        return false;


    }
    
</script>

<script language="javascript" type="text/javascript">
    function fnEnableBuffer()
     {	//ZD 100085
        /*var chkenablebuffer = document.getElementById("chkEnableBuffer");
        
        //FB 1911
        if(document.getElementById("RecurSpec").value  != "")
            return;
            
         //FB 2341
         if(document.getElementById("chkStartNow").checked)
            return;
    
        var chkrecurrence = document.getElementById("chkRecurrence");
     
        if(chkrecurrence && chkrecurrence.checked == true) //Recurrence enabled and buffer zone checked
        {
            var confstdate = '';
            confstdate = GetDefaultDate(document.getElementById("confStartDate").value,'<%=format%>');
        
            var endTime = new Date(confstdate + " " + document.getElementById("confStartTime_Text").value);        
            //var apBfr = endTime.toLocaleTimeString().toString().split(" ")[1]; 
            var apBfr = endTime.format("tt"); //FB 2108
            var hrs = document.getElementById("RecurDurationhr").value;
            var mins = document.getElementById("RecurDurationmi").value;
            
            if(hrs == "")
                hrs = "0";
            
            if(mins == "")
                mins = "0"; 
            
            var timeMins = parseInt(hrs) * 60 ;
            timeMins = timeMins + parseInt(mins);
            endTime.setMinutes(endTime.getMinutes()  + parseInt(timeMins));
             //FB 2614
            var hh = endTime.getHours(); // parseInt(endTime.toLocaleTimeString().toString().split(" ")[0].toString().split(":")[0],10);            
            if('<%=Session["timeFormat"]%>' == "1") //FB 2108
            {
                if(hh >= 12)
                    hh = hh - 12;
                    
                if(hh == 0)
                    hh = 12
            }
            if (hh < 10)
                hh = "0" + hh;
            //FB 2614
            var mm = endTime.getMinutes();// parseInt(endTime.toLocaleTimeString().toString().split(" ")[0].toString().split(":")[1],10);
            if (mm < 10)
                mm = "0" + mm;
            //var ap = endTime.toLocaleTimeString().toString().split(" ")[1];
            var ap = endTime.format("tt"); //FB 2108
            var evntime = parseInt(hh,10);
                
            if(evntime < 12)
                evntime = evntime + 12
                           
            if('<%=Session["timeFormat"]%>' == "0")
            {
                if(ap == "AM")
                {
                    if(hh == "12")
                        document.getElementById("confEndTime_Text").value = "00:" + mm ;
                    else
                        document.getElementById("confEndTime_Text").value = hh + ":" + mm ;
                    
                }
                else
                {
                    if(evntime == "24")
                        document.getElementById("confEndTime_Text").value = "12:" + mm ;
                    else
                        document.getElementById("confEndTime_Text").value = evntime + ":" + mm ;
                }
            }
            else
            {
                document.getElementById("confEndTime_Text").value = hh + ":" + mm + " " + ap;
            }
              //FB 2634  
            //document.getElementById("TearDownDate").value = document.getElementById("confEndDate").value;
            //document.getElementById("TeardownTime_Text").value = document.getElementById("confEndTime_Text").value;
        }
     
//        if(chkenablebuffer && chkenablebuffer.checked) //Enable buffer zone check box checked state
//        {
//            //FB 2634
//            //document.getElementById("SDateText").innerHTML = "<span class='reqfldstarText'>*&nbsp;</span>Conference Setup ";
//            //document.getElementById("EDateText").innerHTML  = "Post Conference End <span class='reqfldstarText'>*</span>";
//            document.getElementById("SetupRow").style.display = "";//CTX hiding
//            document.getElementById("ConfStartRow").style.display = "";//CTX hiding
//            document.getElementById("ConfEndRow").style.display = "";//Block
//            //document.getElementById("SetupTime_Text").style.width = "75px"; 
//            //document.getElementById("TeardownTime_Text").style.width = "75px";
//        }
//        else
//        {   
//            //document.getElementById("SetupDate").value = document.getElementById("confStartDate").value;
//            //document.getElementById("SetupTime_Text").value = document.getElementById("confStartTime_Text").value;
//            
//            if(document.getElementById("Recur").value == "" && document.getElementById("hdnRecurValue").value == "")
//            { 
//                document.getElementById("TearDownDate").value = document.getElementById("confEndDate").value;
//                document.getElementById("TeardownTime_Text").value = document.getElementById("confEndTime_Text").value;
//            } 
//              
//        }*/
     }

     function SetRecurBuffer()
     {
        //FB 2588
        ChangeTimeFormat("D");
		//FB 2634
        //var setupdate = Date.parse(document.getElementById("SetupDate").value + " " + document.getElementById("SetupTime_Text").value);
        var sDate = Date.parse(document.getElementById("confStartDate").value + " " + document.getElementById("hdnStartTime").value);
        var chkenablebuffer = document.getElementById("chkEnableBuffer");
        var chkrecurrence = document.getElementById("chkRecurrence");
        
            if(chkrecurrence && chkrecurrence.checked == true)
            {
                var confstdate = '';
                confstdate = GetDefaultDate(document.getElementById("confStartDate").value,'<%=format%>');
        
                var endTime = new Date(confstdate + " " + document.getElementById("hdnStartTime").value);        
                //var apBfr = endTime.toLocaleTimeString().toString().split(" ")[1]; 
                var apBfr = endTime.format("tt"); //FB 2108
                var hrs = document.getElementById("RecurDurationhr").value;
                var mins = document.getElementById("RecurDurationmi").value;
            
                if(hrs == "")
                    hrs = "0";
                
                if(mins == "")
                    mins = "0"; 
            
                var timeMins = parseInt(hrs, 10) * 60 ; // ZD 101722
            
                timeMins = timeMins + parseInt(mins, 10); // ZD 101722
                endTime.setMinutes(endTime.getMinutes()  + parseInt(timeMins, 10)); // ZD 101722
                
                var endTime1 = endTime;
                var month,date;
                month = endTime1.getMonth() + 1;
                date = endTime1.getDate();
                
                if(eval(endTime1.getMonth() + 1) <10)
                    month = "0" + (endTime1.getMonth() + 1);
                
                if(eval(date) <10)
                    date = "0" + endTime1.getDate();
                   
                if('<%=format%>' == 'MM/dd/yyyy')
                {
                    document.getElementById("confEndDate").value = month + "/" + date + "/" + endTime1.getFullYear();
                    //document.getElementById("TearDownDate").value = month + "/" + date + "/" + endTime1.getFullYear();//FB 2634
                }
                else
                {
                    document.getElementById("confEndDate").value =  date + "/" + month + "/" + endTime1.getFullYear();
                    //document.getElementById("TearDownDate").value =  date + "/" + month + "/" + endTime1.getFullYear();//FB 2634
                }
                //FB 2614
                var hh = endTime.getHours(); // parseInt(endTime.toLocaleTimeString().toString().split(" ")[0].toString().split(":")[0],10);            
                if("<%=Session["timeFormat"]%>" == "1") //FB 2108
                {
                    if(hh >= 12)
                        hh = hh - 12;
                        
                    if(hh == 0)
                        hh = 12
                }
                if (hh < 10)
                    hh = "0" + hh;
                
                //FB 2614
                var mm = endTime.getMinutes(); // parseInt(endTime.toLocaleTimeString().toString().split(" ")[0].toString().split(":")[1],10);
                if (mm < 10)
                    mm = "0" + mm;
                //var ap = endTime.toLocaleTimeString().toString().split(" ")[1];
                var ap = endTime.format("tt"); //FB 2108
                var evntime = parseInt(hh,10);
                
                if(evntime < 12)
                    evntime = evntime + 12
                       
                if('<%=Session["timeFormat"]%>' == '0' || '<%=Session["timeFormat"]%>' == '2')
                {
                    if(ap == "AM")
                    {
                        if(hh == "12")
                            document.getElementById("confEndTime_Text").value = "00:" + mm ;
                        else
                            document.getElementById("confEndTime_Text").value = hh + ":" + mm ;
                        
                    }
                    else
                    {
                        if(evntime == "24")
                            document.getElementById("confEndTime_Text").value = "12:" + mm ;
                        else
                            document.getElementById("confEndTime_Text").value = evntime + ":" + mm ;
                    }
                }
                else
                {
                    document.getElementById("confEndTime_Text").value = hh + ":" + mm + " " + ap;
                }
                //FB 2588
                ChangeTimeFormat("O");
                document.getElementById("confEndTime_Text").value = document.getElementById("hdnEndTime").value;
			//FB 2634
//                document.getElementById("TearDownDate").value = document.getElementById("confEndDate").value;
//                document.getElementById("TeardownTime_Text").value = document.getElementById("confEndTime_Text").value;
//                document.getElementById("SetupDate").value = document.getElementById("confStartDate").value;
//            
//                var setupdate = Date.parse(document.getElementById("SetupDate").value + " " + document.getElementById("SetupTime_Text").value);
//                var tDate = Date.parse(document.getElementById("TearDownDate").value + " " + document.getElementById("TeardownTime_Text").value);
//                
//                if(setupdate > tDate)
//                    document.getElementById("SetupTime_Text").value = document.getElementById("SetupTime_Text").value;
//                    
//               if(sDate > setupdate)
//               {
//                   document.getElementById("SetupDate").value = document.getElementById("confStartDate").value;
//                   document.getElementById("SetupTime_Text").value = document.getElementById("confStartTime_Text").value;
//               }
            
            }
     }
         
    function randam() { 
    //ZD 100522 Starts
   var pwdCharLength =  document.getElementById("hdnPINlen").value; //ZD 100522
   if(pwdCharLength <= 0)
      pwdCharLength = 4;
   var totalDigits = pwdCharLength;
//ZD 100522 End
       
       var n = Math.random()*10000;
       n = Math.round(n);
       n = n.toString(); 
          var pd = ''; 
          if (totalDigits > n.length) { 
             for (i=0; i < (totalDigits-n.length); i++) { 
                pd += '0'; 
             } 
          } 
       return pd + n.toString();
    }
    //FB 2501 - Starts
    /*function num_gen() 
    {
       var num = randam()
       if (num.indexOf(0) == 0) 
          num = num.replace(0,1)

       var pass = document.getElementById("ConferencePassword")
       var pass1 = document.getElementById("ConferencePassword2")
       if(pass != "")
         pass.value = num;
       if(pass1 != "")
         pass1.value = num;
         
    } */
    function num_gen() 
    {
       var num = randam()
      //alert(num);
       if (num.indexOf(0) == 0) 
          num = num.replace(0,1)
      
        //FB 2244 - Starts
        if(document.getElementById("cmpValPassword") != null)
            document.getElementById("cmpValPassword").style.display = "none";
        //if(document.getElementById("cmpValPassword1") != null) //ZD 102692
            //document.getElementById("cmpValPassword1").style.display = "none";
        if(document.getElementById("numPassword1") != null)
            document.getElementById("numPassword1").style.display = "none";

        var pass = document.getElementById("ConferencePassword")
        var pass1 = document.getElementById("ConferencePassword2")
        //if(pass != "")
        if(pass != null)
            pass.value = num;
        //if(pass1 != "")
        if(pass1 != null)
            pass1.value = num;
        //FB 2244 - End
         
       SavePassword(); 
    } //fb 676 end 
    
    function SavePassword()
    {
        //alert(document.getElementById("<%=ConferencePassword.ClientID %>").value);
        document.getElementById("<%=confPassword.ClientID %>").value = document.getElementById("<%=ConferencePassword.ClientID %>").value;
    }
 	//FB 2501 - End

    function UpdateCheckbox(obj)
    {
        if (obj.value == "")
        {
           document.getElementById(obj.id.replace("txtQuantity", "chkSelectedMenu")).checked = false;
           UpdatePrice(obj);
        }
        else
        {
            if (!isNaN(obj.value) && obj.value.indexOf(".") < 0)
            {
                document.getElementById(obj.id.replace("txtQuantity", "chkSelectedMenu")).checked = true;
                UpdatePrice(obj);
            }
            else
            {
                alert(RInvalidValue);
                obj.value = "";
            }
        }
    }
    
    function UpdatePrice(obj)
    {
        var lblPrice = document.getElementById(obj.id.substring(0, obj.id.indexOf("_dgCateringMenus")) + "_lblPrice");
        var price = parseFloat("0.00");
        var i = 2;
        
        while(document.getElementById(obj.id.substring(0, (obj.id.indexOf("Menus_ctl0") + 10)) + i + "_txtQuantity"))
        {
            if (i<10)
            {
                price += document.getElementById(obj.id.substring(0, (obj.id.indexOf("Menus_ctl0") + 10)) + i + "_txtPrice").value * document.getElementById(obj.id.substring(0, (obj.id.indexOf("Menus_ctl0") + 10)) + i + "_txtQuantity").value;
            }
            else
            {
                price += document.getElementById(obj.id.substring(0, (obj.id.indexOf("Menus_ctl") + 9)) + i + "_txtPrice").value * document.getElementById(obj.id.substring(0, (obj.id.indexOf("Menus_ctl") + 9)) + i + "_txtQuantity").value;
            }
            i++;
        }
        lblPrice.innerHTML = (price * 100.00) / 100.00;
    }

    function ShowItems(obj)
    {
        getMouseXY();
        var str = document.getElementById(obj.id.substring(0, obj.id.lastIndexOf("_")) + "_dgMenuItems").innerHTML;
        str = str.replace("<TBODY>", "<TABLE border='0' cellspacing='0' cellpadding='3' class='tableBody' width='200'><TBODY>");
        str = str.replace("</TBODY>", "</TBODY></TABLE>");
        document.getElementById("tblMenuItems").style.position = 'absolute';
        document.getElementById("tblMenuItems").style.left = mousedownX - 200;
        document.getElementById("tblMenuItems").style.top = mousedownY;
        document.getElementById("tblMenuItems").style.borderWidth = 1;
        document.getElementById("tblMenuItems").style.display="";
        document.getElementById("tblMenuItems").innerHTML = str;
    }
    
    function HideItems()
    {
       document.getElementById("tblMenuItems").style.display="none";
    }

    function ShowImage(obj)
    {
        document.getElementById("myPic").src = obj.src;
        getMouseXY();
        document.getElementById("divPic").style.position = 'absolute';
        document.getElementById("divPic").style.left = mousedownX + 20;
        document.getElementById("divPic").style.top = mousedownY;
        document.getElementById("divPic").style.display="";
    }

    function HideImage()
    {
        document.getElementById("divPic").style.display="none";
    }

    function CheckParty()
    {
        if (document.getElementById("ifrmPartylist"))
        {
			//ZD 100834 Starts
            if(typeof ifrmPartylist.bfrRefresh == "function")
            {
				//ZD 101254  Starts
                if (!ifrmPartylist.bfrRefresh())
                {
                    DataLoading(0);
                    return false;
                }
				//ZD 101254  End
            }
			//ZD 100834 End
        }
        return true;
    }

function viewendpoint(val)
{
	
	url = "dispatcher/admindispatcher.asp?eid=" + val + "&cmd=GetEndpoint&ed=1&wintype=pop";

	if (!window.winrtc) {	// has not yet been defined
		winrtc = window.open(url, "", "width=400,height=310,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
		winrtc.focus();
	} else { // has been defined
	    if (!winrtc.closed) {     // still open
	    	winrtc.close();
	        winrtc = window.open(url, "", "width=400,height=310,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
			winrtc.focus();
		} else {
	        winrtc = window.open(url, "", "width=400,height=310,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
	        winrtc.focus();
    	}
	}
}

function viewconflict(id, confTime, confDate, confDuration, confTZ, ConferenceName, confID)
{
    var rms = document.getElementById("selectedloc").value;
	url = "preconferenceroomchart.asp?wintype=ifr&cid=" + confID + "&cno=" + id +
		"&date=" + confDate + "&time=" + confTime + "&timeZone=" + confTZ +
		"&duration=" + confDuration + "&r=" + rms + "&n=" + ConferenceName;
	wincrm = window.open(url,'confroomchart','status=no,width=1,height=1,top=30,left=0,scrollbars=yes,resizable=yes')
	if (wincrm)
		wincrm.focus();
	else
		alert(EN_132);
}
		
/* //ZD 100284
function formatTime(timeText, regText)//FB 1715
{

   if("<%=Session["timeFormat"]%>" == "1")
   {    
    //ZD 100284 
		var tText = document.getElementById(timeText);
        var isColon = true;
        var timeVal = tText.value;
        
	    if(timeVal.length < 8)
        {
            if(timeVal.indexOf(':') < 0)
                isColon = false;
            
            if(isColon == true)
            {
                var timSplit = timeVal.split(":");
                
                if(timSplit[0].length == 1)
                    timeVal = "0" + timSplit[0] + ":" + timSplit[1];
                
                timeVal = timeVal.replace('a','A').replace('p','P').replace('m','M');
                
                if(timeVal.indexOf(' AM') < 0)
                    timeVal = timeVal.replace('AM',' AM');
                    
                if(timeVal.indexOf(' PM') < 0)
                    timeVal = timeVal.replace('PM',' PM');               
                
            }
        }

        if (timeVal.search(/^[0|1][0-9]:[0-5][0-9] ?[a|A|p|P][M|m]/) != -1 && isColon == true)
        {
            var a_p = "";
            //FB 2614
            var t = new Date();
            //var t = new Date(GetDefaultDate(document.getElementById("confStartDate").value,'<%=format%>'));
            
            var d = new Date(t.getDay() + "/" + t.getMonth() + "/" + t.getYear() + " " + timeVal); //document.getElementById(timeText).value);
            //var d = new Date(GetDefaultDate(document.getElementById("confStartDate").value,'<%=format%>') + " " + timeVal);
           
            var curr_hour = d.getHours();
            if (curr_hour < 12) a_p = "AM"; else a_p = "PM";

            if (curr_hour == 0) curr_hour = 12;
            if (curr_hour > 12) curr_hour = curr_hour - 12;
            
            curr_hour = curr_hour + "";             
            if (curr_hour.length == 1)
               curr_hour = "0" + curr_hour;
                     
            var curr_min = d.getMinutes();
            curr_min = curr_min + "";
            if (curr_min.length == 1)
               curr_min = "0" + curr_min;

            document.getElementById(timeText).value = curr_hour + ":" + curr_min + " " + a_p;            
            document.getElementById(regText).style.display = "None"; 
            return true;            
       }
       else
       {    
            document.getElementById(regText).style.display = ""; 
            //document.getElementById(timeText).focus();
            return false;
       }
   }
    return true;
}
*/
//FB 1716

function ChangeDuration()
{ 
    if ('<%=isEditMode%>' == "1" )    
    {        
        var duration = document.getElementById("hdnDuration").value;
        var chgVar =  document.getElementById("hdnChange").value;
              
        if(duration != '')
        {        
            var durArr = duration.split("&");
            var changeTime;
            
            var setupTime = new Date(GetDefaultDate(document.getElementById("confStartDate").value,'<%=format%>') + " "
            + document.getElementById("confStartTime_Text").value);
            
            var startTime = new Date(GetDefaultDate(document.getElementById("SetupDate").value,'<%=format%>') + " "
            + document.getElementById("SetupTime_Text").value);
            
            var endTime = new Date(GetDefaultDate(document.getElementById("TearDownDate").value,'<%=format%>') + " "
            + document.getElementById("TeardownTime_Text").value);
           
            switch(chgVar)
            {
                case "ST":  
                    setupTime = setCDuration(setupTime,durArr[1]);                    
                    document.getElementById("SetupDate").value = getCDate(setupTime);                    
                    document.getElementById("SetupTime_Text").value = getCTime(setupTime);
                    var startTime = new Date(GetDefaultDate(document.getElementById("SetupDate").value,'<%=format%>') + " "
                    + document.getElementById("SetupTime_Text").value);
                    
                    startTime = setCDuration(startTime,durArr[0]);                    
                    document.getElementById("TearDownDate").value = getCDate(startTime);                    
                    document.getElementById("TeardownTime_Text").value = getCTime(startTime);
                    var endTime = new Date(GetDefaultDate(document.getElementById("TearDownDate").value,'<%=format%>') + " "
                    + document.getElementById("TeardownTime_Text").value);                
                    
                    endTime = setCDuration(endTime,durArr[2]);
                    document.getElementById("confEndDate").value = getCDate(endTime);                   
                    document.getElementById("confEndTime_Text").value = getCTime(endTime);
                break;
                case "SU":
                
                    startTime = setCDuration(startTime,durArr[0]);                    
                    document.getElementById("TearDownDate").value = getCDate(startTime);                    
                    document.getElementById("TeardownTime_Text").value = getCTime(startTime);
                    var endTime = new Date(GetDefaultDate(document.getElementById("TearDownDate").value,'<%=format%>') + " "
                    + document.getElementById("TeardownTime_Text").value);                
                    
                    endTime = setCDuration(endTime,durArr[2]);
                    document.getElementById("confEndDate").value = getCDate(endTime);                   
                    document.getElementById("confEndTime_Text").value = getCTime(endTime);
                break;
                case "TD":
                   endTime = setCDuration(endTime,durArr[2]);
                   document.getElementById("confEndDate").value = getCDate(endTime);                   
                   document.getElementById("confEndTime_Text").value = getCTime(endTime);
                break;               
            }
        }
    }  
}

//FB 1716
function setCDuration(setupTime, dura)
{
    var chTime = new Date(setupTime);
    var d = 0;
    var min = 0;
    var hh = 0, dur = 0;
    if(dura > 60)
    {   
        hh = dura / 60;
        min = dura % 60;                        
        if(min > 0)
            hh = Math.floor(hh) + 1;                        
     
        for(d = 1; d <= hh ; d++)
        {
            if(min > 0 && d == hh)
                dur = dura % 60;
            else
                dur = 60;
                
             chTime.setMinutes(chTime.getMinutes() + dur);   
        }
    } 
    else //if(dura > 0) //FB 2501 - Start     FB 2634
    {
        chTime.setTime(chTime.getTime() + (dura * 60 * 1000));
        //if("<%=Session["DefaultConfDuration"]%>" != null)
        //{
//            var ConfDuration = "<%=Session["DefaultConfDuration"]%>";
//            chTime.setTime(chTime.getTime() + (dura * ConfDuration * 1000));        
//        }
    } 
    //FB 2501 - End 
    //else
      //  chTime;
        
   return chTime
}
//FB 1716

function getCDate(changeTime)
{
    var strDate;
    var month,date;
    month = changeTime.getMonth() + 1;
    date = changeTime.getDate();
    
    if(eval(changeTime.getMonth() + 1) <10)
        month = "0" + (changeTime.getMonth() + 1);
    
    if(eval(date) <10)
        date = "0" + changeTime.getDate();
       
    if('<%=format%>' == 'MM/dd/yyyy')    
        strDate = month + "/" + date + "/" + changeTime.getFullYear();
    else
        strDate =  date + "/" + month + "/" + changeTime.getFullYear();
    
    return strDate;
}
//FB 1716

function getCTime(changeTime)
{
    //FB 2614
    var hh = changeTime.getHours(); // parseInt(changeTime.toLocaleTimeString().toString().split(" ")[0].toString().split(":")[0],10);            
    if("<%=Session["timeFormat"]%>" == "1") //FB 2108
    {
        if(hh >= 12)
            hh = hh - 12;
            
        if(hh == 0)
            hh = 12
    }
    if (hh < 10)
        hh = "0" + hh;
    //FB 2614
    var mm = changeTime.getMinutes(); // parseInt(changeTime.toLocaleTimeString().toString().split(" ")[0].toString().split(":")[1],10);
    if (mm < 10)
        mm = "0" + mm;
     //var ap = changeTime.toLocaleTimeString().toString().split(" ")[1];
    var ap = changeTime.format("tt"); //FB 2108
   
    var evntime = parseInt(hh,10);
    
    if(evntime < 12 && ap == "PM")
       evntime = evntime + 12;
       
    var tiFormat =  "<%=Session["timeFormat"]%>" ;

    if(tiFormat == '0')
    {
        if(ap == "AM")                        
            strTime = (hh == "12") ? "00:" + mm : hh + ":" + mm ;
        else
        {
            if(evntime == "24")
                strTime = (hh == "12") ? "00:" + mm : evntime + ":" + mm;
            else
                strTime =  evntime + ":" + mm;
        }   
    }
    else
        strTime = hh + ":" + mm + " " + ap;
        
   return strTime;
}
//FB 2634
function fnEndDateValidation()
{
    ChangeTimeFormat("D"); //FB 2588
    var sDate = Date.parse(GetDefaultDate(document.getElementById("confStartDate").value,'<%=format%>') + " " + document.getElementById("hdnStartTime").value);
    var eDate = Date.parse(GetDefaultDate(document.getElementById("confEndDate").value,'<%=format%>') + " " + document.getElementById("hdnEndTime").value);
    
   //ZD 103624
    if(!validateStartEndDate(sDate,eDate,0,1))
        return false;

    if ( (sDate >= eDate) && (document.getElementById("hdnStartTime").value.search(/^[0|1][0-9]:[0-5][0-9] ?[a|A|p|P][M|m]/) != -1)
    && (document.getElementById("hdnEndTime").value.search(/^[0|1][0-9]:[0-5][0-9] ?[a|A|p|P][M|m]/) != -1))
    {
        if (sDate == eDate)
        {
             if (sDate > eDate)
                alert(EndTimeChangeAlert);
            else if (sDate == eDate)
                alert(EndTimeChangeAlert);            
        }
        else
            alert(EndDateChangeAlert);
            
         ChangeEndDate();
      }
}

//FB 2588
function ChangeTimeFormat() {

    var args = ChangeTimeFormat.arguments;
    var stime = document.getElementById("confStartTime_Text").value;
    var etime = document.getElementById("confEndTime_Text").value;
    var hdnsTime = document.getElementById("hdnStartTime");
    var hdneTime = document.getElementById("hdnEndTime");

    if ('<%=Session["timeFormat"]%>' == "2") {
        if (args[0] == "D") {
            stime = stime.replace('Z', '')
            stime = stime.substring(0, 2) + ":" + stime.substring(2, 4);
            etime = etime.replace('Z', '')
            etime = etime.substring(0, 2) + ":" + etime.substring(2, 4);
        }
        else {
            if (stime.indexOf("Z") < 0)
                stime = stime.replace(':', '') + "Z";
            if (etime.indexOf("Z") < 0)
                etime = etime.replace(':', '') + "Z";
        }
    }

    hdnsTime.value = stime
    hdneTime.value = etime
}
//ZD 103624
function validateStartEndDate(startDateTime,endDateTime, isStartValidation, isEndValidation)
{
    if(isStartValidation == 1)
    {
        if(startDateTime == "Invalid Date" && !formatTimeNew('confStartTime_Text','regStartTime',"<%=Session["timeFormat"]%>"))
        {
            document.getElementById("regStartTime").style.display = '';
            document.getElementById("confStartTime_Text").focus();
            return false;    
        }
        else if (startDateTime == "Invalid Date" || isNaN(startDateTime))//ZD 104163
        {
            document.getElementById("regStartDate").style.display = '';
            document.getElementById("confStartDate").focus();
            return false;            
        }
        else
            document.getElementById("regStartDate").style.display = 'None';
            
    }

    if(isEndValidation == 1)
    {
        if(isNaN(endDateTime) && !formatTimeNew('confEndTime_Text','regEndTime',"<%=Session["timeFormat"]%>"))
        {
            document.getElementById("regEndTime").style.display = '';
            document.getElementById("confEndTime_Text").focus();
            return false;  
        }
        else if(isNaN(endDateTime))
        {
            document.getElementById("regEndDate").style.display = '';
            document.getElementById("confEndDate").focus();
            return false;  
        }
        else
            document.getElementById("regEndDate").style.display = 'None';
    }

     return true;
}
function ChangeEndDate()
{
    setTimeout("fnUpdateStartList();", 100);// ZD 101444
    setTimeout("fnUpdateEndList();", 100);
    // FB 2692 Starts
//    if ('<%=isEditMode%>' == "1" && document.getElementById("chkRecurrence").checked == false)//ZD 102796 start
//    {
//        var sDate = Date.parse(GetDefaultDate(document.getElementById("confStartDate").value,'<%=format%>') + " " + document.getElementById("confStartTime_Text").value);
//        var eDate = Date.parse(GetDefaultDate(document.getElementById("confEndDate").value,'<%=format%>') + " " + document.getElementById("confEndTime_Text").value);
//        
//         //ZD 103624
//        if(!validateStartEndDate(sDate,eDate,1,1))
//            return false;

//        if(sDate >= eDate)
//        {
//            document.getElementById("errLabel").innerHTML = "Start time should be less than end time";
//            window.scrollTo(0,0);
//            return false;
//        }
//        else
//        {
//            var nDate = eDate - sDate;
//            if(nDate < 900000)
//            {
//            document.getElementById("errLabel").innerHTML = "Minimum duration should be 15 minutes";
//            window.scrollTo(0,0);
//            return false;
//            }
//        }
//        document.getElementById("errLabel").innerHTML = "";
//        return false;
//    }
//    else if('<%=isEditMode%>' == "1")
//        return false; //ZD 102796 End
    // FB 2692 Ends
    ChangeTimeFormat("D"); //FB 2588
    var args = ChangeEndDate.arguments;
    var startDateTime = new Date(GetDefaultDate(document.getElementById("confStartDate").value,'<%=format%>') + " " + document.getElementById("hdnStartTime").value);
    var endDateTime = Date.parse(GetDefaultDate(document.getElementById("confEndDate").value,'<%=format%>') + " " + document.getElementById("hdnEndTime").value);
    var skipcheck = 0;
    
    //ZD 103624
    if(!validateStartEndDate(startDateTime,endDateTime,1,1))
        return false;

    if(args[0] == "ET")
    {
        if(startDateTime >= endDateTime)
            skipcheck = 0;
        else
            skipcheck = 1;
    }
    
    if('<%=Session["DefaultConfDuration"]%>' != null && skipcheck == 0)
    {
        var ConfDuration = '<%=Session["DefaultConfDuration"]%>';
        
//        if ('<%=isEditMode%>' == "1" )//ZD 102796 start
//        {
//            var duration = document.getElementById("hdnDuration").value;
//            if(duration != "")
//                ConfDuration = duration            
//        } //ZD 102796 End
        
        var ConfHours = parseInt(ConfDuration, 10) / 60; // ZD 101722
        var ConfMinutes = parseInt(ConfDuration, 10) % 60; // ZD 101722
        ConfMinutes = ConfMinutes + startDateTime.getMinutes();
        if(ConfMinutes > 59)
        {
           ConfMinutes = ConfMinutes - 60;
           ConfHours = ConfHours + 1;
        }
        startDateTime.setHours(startDateTime.getHours() + ConfHours);
        startDateTime.setMinutes(ConfMinutes);
    }
    else
        startDateTime = new Date(endDateTime);

    var month,date;
    month = startDateTime.getMonth() + 1;
    date = startDateTime.getDate();
    
    if(eval(month) < 10)
        month = "0" + month;
    
    if(eval(date) < 10)
        date = "0" + startDateTime.getDate();
    
    if('<%=format%>' == 'MM/dd/yyyy')
        document.getElementById("confEndDate").value = month + "/" + date + "/" + startDateTime.getFullYear();
    else
        document.getElementById("confEndDate").value =  date + "/" + month + "/" + startDateTime.getFullYear();
        
    var hh = startDateTime.getHours();
        
    if('<%=Session["timeFormat"]%>' == "1")
    {
        if(hh >= 12)
            hh = hh - 12;
            
        if(hh == 0)
            hh = 12
    }
            
    if (hh < 10)
        hh = "0" + hh;
    var mm = startDateTime.getMinutes()
    if (mm < 10)
        mm = "0" + mm;
    var ap = startDateTime.format("tt");
    var evntime = parseInt(hh,10);
    
     if(evntime < 12 && ap == "PM")
        evntime = evntime + 12;
               
    if('<%=Session["timeFormat"]%>' == "0" || '<%=Session["timeFormat"]%>' == "2") //FB 2588
    {
        if(ap == "AM")
        {
            if(hh == "12")
                document.getElementById("confEndTime_Text").value = "00:" + mm ;
            else
                document.getElementById("confEndTime_Text").value = hh + ":" + mm ;
        }
        else
        {
            if(evntime == "24")
                document.getElementById("confEndTime_Text").value = "12:" + mm ;
            else
                document.getElementById("confEndTime_Text").value = evntime + ":" + mm ;
        }
    }
    else
        document.getElementById("confEndTime_Text").value = hh + ":" + mm + " " + ap;
        
    //FB 2588
    ChangeTimeFormat("O");
    document.getElementById("confEndTime_Text").value = document.getElementById("hdnEndTime").value;

    //ZD 103624
    var eDtTime = new Date((GetDefaultDate(document.getElementById("confEndDate").value,'<%=format%>') + " " + document.getElementById("confEndTime_Text").value))
    if(eDtTime != "Invalid Date")
    {
        ValidatorEnable(document.getElementById("regEndDate"), false);    
        ValidatorEnable(document.getElementById("regEndTime"), false);    
    }
    
}


function DuraionCalculation()
{
    var confenddate = '';
    var confstdate = '';
    var durationMin = '';
    var totalMinutes;
    var totalDays;
    var totalHours;
    var lblconfduration = document.getElementById('<%=lblConfDuration.ClientID%>');
    
    lblconfduration.innerText = '';
    confenddate = GetDefaultDate(document.getElementById("confEndDate").value + " " + document.getElementById("confEndTime_Text").value,'<%=format%>');
    confstdate = GetDefaultDate(document.getElementById("confStartDate").value + " " + document.getElementById("confStartTime_Text").value,'<%=format%>');
    
    var durationMin = parseInt(Date.parse(confenddate) - Date.parse(confstdate), 10) /  1000  ; // ZD 101722
    if(durationMin != '')
    {
        totalDays = Math.floor(durationMin / (24 * 60 * 60));
        totalHours = Math.floor((durationMin - (totalDays * 24 * 60 * 60)) / (60 * 60));
        totalMinutes = Math.floor((durationMin - (totalDays * 24 * 60 * 60) -  (totalHours * 60 * 60)) / 60);
    }
    
    if (Math.floor(durationMin) < 0)
        lblconfduration.innerText = "Invalid duration";
    if (totalDays > 0)
        lblconfduration.innerText = totalDays + " day(s) ";
    if (totalHours > 0)
        lblconfduration.innerText += totalHours + " hrs "; //ZD 100528
    if (totalMinutes > 0)
        lblconfduration.innerText += totalMinutes + " mins"; //ZD 100528
}
//FB 2634
function EndDateValidation1(frm,confstdate,confenddate,msgchk)
{
    var setupdate = Date.parse(document.getElementById("SetupDate").value + " " + document.getElementById("SetupTime_Text").value);
    var sDate = Date.parse(document.getElementById("confStartDate").value + " " + document.getElementById("confStartTime_Text").value);
    var eDate = Date.parse(document.getElementById("confEndDate").value + " " + document.getElementById("confEndTime_Text").value);
    var tDate = Date.parse(document.getElementById("TearDownDate").value + " " + document.getElementById("TeardownTime_Text").value);
    //FB 1715
    if ( (sDate >= eDate)&& (document.getElementById("confStartTime_Text").value.search(/^[0|1][0-9]:[0-5][0-9] ?[a|A|p|P][M|m]/) != -1)        
    && (document.getElementById("confEndTime_Text").value.search(/^[0|1][0-9]:[0-5][0-9] ?[a|A|p|P][M|m]/) != -1))
    {
         if(msgchk == 'S')
        {
            if (Date.parse(confstdate) == Date.parse(confenddate))        
            {
                if (Date.parse(confstdate + " " + document.getElementById("confStartTime_Text").value) > Date.parse(confenddate + " " + document.getElementById("confEndTime_Text").value) )        
                    alert(EndTimeChangeAlert);  
               else if (Date.parse(confstdate + " " + document.getElementById("confStartTime_Text").value) == Date.parse(confenddate + " " + document.getElementById("confEndTime_Text").value) )        
                    alert(EndTimeChangeAlert);              
            }    
            else
                alert(EndDateChangeAlert);
        }
            
        ChangeEndDateTime(confstdate,confenddate); 
     }
     if(!document.getElementById("chkStartNow").checked) //FB 2341
      {
        var chkbuffer = document.getElementById("chkEnableBuffer");
        if (('<%=enableBufferZone%>' == "1") && (chkbuffer.checked))
        { 
              if ((setupdate < sDate) || (setupdate >= tDate) ||(setupdate > eDate) || (tDate > eDate) || (tDate < setupdate))
              {
                    //Modification for FB 1716 - Start
                    if( (setupdate < sDate) || (setupdate >= tDate) || (setupdate > eDate)|| (tDate < setupdate))
                    {
                        //if(!(setupdate >= tDate))
                        //{
                            document.getElementById("SetupDate").value = document.getElementById("confStartDate").value;
                            document.getElementById("SetupTime_Text").value = document.getElementById("confStartTime_Text").value;
                            document.getElementById("TearDownDate").value = document.getElementById("confEndDate").value;
                            document.getElementById("TeardownTime_Text").value = document.getElementById("confEndTime_Text").value;
                        //}
                    }
                    //Modification for FB 1716 - End 
              }
              if(tDate < setupdate)
               {
                     document.getElementById("TearDownDate").value = document.getElementById("SetupDate").value;                   
                    document.getElementById("TeardownTime_Text").value = document.getElementById("confEndTime_Text").value;
               } 
              
                  if(hdnCfSt == '1')
                  if(setupdate < sDate)
                  {
                     alert(SetupTimeAlert);
                     document.getElementById("confStartDate").value = document.getElementById("SetupDate").value;
                     document.getElementById("confStartTime_Text").value = document.getElementById("SetupTime_Text").value;
                     hdnCfSt = '0';
                  }
             if(setupdate >= tDate)
              {
                    var hhT;
                    var mmT;
                    var apT;
                    var zerto;
                                        
                    if(setupdate < tDate)
                       confstdate = GetDefaultDate(document.getElementById("TearDownDate").value,'<%=format%>');
                    else
                       confstdate = GetDefaultDate(document.getElementById("SetupDate").value,'<%=format%>');
                        
                    
            
            
                    var endTimeT = new Date(confstdate + " " + document.getElementById("SetupTime_Text").value); 
                    
                    
                               
                    if((setupdate > sDate))// && (document.getElementById("SetupTime_Text").value <= document.getElementById("confEndTime_Text").value))
                    {
                        endTimeT.setHours(endTimeT.getHours() + 1);
                        
                        
                        var monthT,dateT;
                        monthT = endTimeT.getMonth() + 1;
                        dateT = endTimeT.getDate();
                        
                        if(eval(endTimeT.getMonth() + 1) <10)
                            monthT = "0" + (endTimeT.getMonth() + 1);
                        
                        if(eval(dateT) <10)
                            dateT = "0" + endTimeT.getDate();
                           
                        if('<%=format%>' == 'MM/dd/yyyy')
                        {
                            document.getElementById("confEndDate").value = monthT + "/" + dateT + "/" + endTimeT.getFullYear();
                            document.getElementById("TearDownDate").value = monthT + "/" + dateT + "/" + endTimeT.getFullYear();
                        }
                        else
                        {
                            document.getElementById("confEndDate").value =  dateT + "/" + monthT + "/" + endTimeT.getFullYear();
                            document.getElementById("TearDownDate").value =  dateT + "/" + monthT + "/" + endTimeT.getFullYear();
                        }
                        //FB 2614
                        hhT = endTimeT.getHours(); //parseInt(endTimeT.toLocaleTimeString().toString().split(" ")[0].toString().split(":")[0],10);            
                        if (hhT < 10)
                            hhT = "0" + hhT;
                        
                        //FB 2614
                        mmT = endTimeT.getMinutes(); // parseInt(endTimeT.toLocaleTimeString().toString().split(" ")[0].toString().split(":")[1],10);
                        if (mmT < 10)
                            mmT = "0" + mmT;
                        //FB 2614
                        apT = endTimeT.format("tt"); //.toLocaleTimeString().toString().split(" ")[1];
                        
                        zerto = hhT + ":" + mmT + " " + apT;                
                        var evntime = parseInt(hhT,10);
                
                        if(evntime < 12 && apT == "PM")
                            evntime = evntime + 12;
                            if('<%=Session["timeFormat"]%>' == "0")
                            {
                                if(apT == "AM")
                                {
                                    if(hhT == "12")
                                        document.getElementById("confEndTime_Text").value = "00:" + mmT ;
                                    else
                                        document.getElementById("confEndTime_Text").value = hhT + ":" + mmT ;
                                    
                                }
                                else
                                {
                                    if(evntime == "24")
                                        document.getElementById("confEndTime_Text").value = "12:" + mmT ;
                                    else
                                        document.getElementById("confEndTime_Text").value = evntime + ":" + mmT ;
                                }
                                    
                            }
                            else
                            {
                                document.getElementById("confEndTime_Text").value = zerto;
                                
                            }
                        }
                   document.getElementById("TearDownDate").value = document.getElementById("confEndDate").value;
                   document.getElementById("TeardownTime_Text").value = document.getElementById("confEndTime_Text").value;
              }
              var chkrecurrence = document.getElementById("chkRecurrence");    
              if(chkrecurrence != null && chkrecurrence.checked == true)
              {
                    document.getElementById("confEndDate").value = document.getElementById("TearDownDate").value;
                    document.getElementById("confEndTime_Text").value = document.getElementById("TeardownTime_Text").value;
              }
        }
        else
        {
            document.getElementById("SetupDate").value = document.getElementById("confStartDate").value;
            document.getElementById("SetupTime_Text").value = document.getElementById("confStartTime_Text").value;
                        
            document.getElementById("TearDownDate").value = document.getElementById("confEndDate").value;
            document.getElementById("TeardownTime_Text").value = document.getElementById("confEndTime_Text").value;
        }   
    }
      DuraionCalculation();  
}

function ChangeEndDateTime(confstdate,confenddate)
{
      document.getElementById("confEndDate").value = document.getElementById("confStartDate").value;
         
        var endTime = new Date(confstdate + " " + document.getElementById("confStartTime_Text").value);
        
        //var apBfr = endTime.toLocaleTimeString().toString().split(" ")[1];          
        var apBfr = endTime.format("tt");      //FB 2108
        
        endTime.setHours(endTime.getHours() + 1);
            //if('<%=client.ToString().ToUpper()%>' =="MOJ") FB 2501
            //endTime.setMinutes(endTime.getMinutes() - 45);
        
        var month,date;
        month = endTime.getMonth() + 1;
        date = endTime.getDate();
        
        if(eval(endTime.getMonth() + 1) <10)
            month = "0" + (endTime.getMonth() + 1);
        
        if(eval(date) <10)
            date = "0" + endTime.getDate();
        document.getElementById("SetupDate").value = document.getElementById("confStartDate").value;//FB 1716   
        if('<%=format%>' == 'MM/dd/yyyy')
        {
            document.getElementById("confEndDate").value = month + "/" + date + "/" + endTime.getFullYear();
            document.getElementById("TearDownDate").value = month + "/" + date + "/" + endTime.getFullYear();
        }
        else
        {
            document.getElementById("confEndDate").value =  date + "/" + month + "/" + endTime.getFullYear();
            document.getElementById("TearDownDate").value =  date + "/" + month + "/" + endTime.getFullYear();
        }
        
        //FB 2614
        var hh = endTime.getHours(); // parseInt(endTime.toLocaleTimeString().toString().split(" ")[0].toString().split(":")[0],10);            
        if("<%=Session["timeFormat"]%>" == "1") //FB 2108
        {
            if(hh >= 12)
                hh = hh - 12;
                
            if(hh == 0)
                hh = 12
        }
        if (hh < 10)
            hh = "0" + hh;
        //FB 2614
        var mm = endTime.getMinutes() ;// parseInt(endTime.toLocaleTimeString().toString().split(" ")[0].toString().split(":")[1],10);
        if (mm < 10)
            mm = "0" + mm;
        //var ap = endTime.toLocaleTimeString().toString().split(" ")[1];
        var ap = endTime.format("tt");    //FB 2108  
        var evntime = parseInt(hh,10);
        
         if(evntime < 12 && ap == "PM")
            evntime = evntime + 12;
            
        if('<%=Session["timeFormat"]%>' == "0")
        {
            if(ap == "AM")
            {
                if(hh == "12")
                    document.getElementById("confEndTime_Text").value = "00:" + mm ;
                else
                    document.getElementById("confEndTime_Text").value = hh + ":" + mm ;
                
            }
            else
            {
                if(evntime == "24")
                    document.getElementById("confEndTime_Text").value = "12:" + mm ;
                else
                    document.getElementById("confEndTime_Text").value = evntime + ":" + mm ;
            }
                
        }
        else
        {
            document.getElementById("confEndTime_Text").value = hh + ":" + mm + " " + ap;
        }      
}

function ChangeStartDate(frm)
{
    if(document.getElementById("Recur").value == "" && document.getElementById("hdnRecurValue").value == "")
    {
        var confenddate = '';
        confenddate = GetDefaultDate(document.getElementById("confEndDate").value,'<%=format%>');
        var confstdate = '';
        confstdate = GetDefaultDate(document.getElementById("confStartDate").value,'<%=format%>');
       /*
         if (Date.parse("<%=Session["systemDate"]%>" + " " + "<%=Session["systemTime"]%>") > Date.parse(confenddate + " " + document.getElementById("confEndTime_Text").value))
        {
            document.getElementById("confEndDate").value = document.getElementById("confStartDate").value;
            if (frm == "0") 
            {
                alert("Invalid End Date or Time. It should be greater than time current time in user preferred timezone.");
                DataLoading(0);
                return false;
            }
        }
        */
        fnEndDateValidation(frm,confstdate,confenddate,'S');

    }
    ChangeDuration();//FB 1716
    return true;
}

function CheckDuration()
{
    if (document.getElementById("Recur").value == "")
    if (Date.parse(document.getElementById("confStartDate").value + " " + document.getElementById("confStartTime_Text").value) > Date.parse(document.getElementById("confEndDate").value + " " + document.getElementById("confEndTime_Text").value))
    {
        alert(InvalidDuration1);
        return false;
    }
}
//FB 2226
function ChangePublic()
{    	
    if(document.getElementById("chkPublic") != null)
    {
        if ((document.getElementById("chkPublic").checked) && ("<%=Session["dynInvite"] %>" == "1") )    
        {
            document.getElementById("openRegister").style.display = "";
        }
        else
        {
            document.getElementById("openRegister").style.display ="none";
        }
    }
}
//FB 2870 Start
function ChangeNumeric()
{
    if(document.getElementById("ChkEnableNumericID") != null)
    {
        if ((document.getElementById("ChkEnableNumericID").checked))  
        {
            document.getElementById("DivNumeridID").style.display = "";
            ValidatorEnable(document.getElementById("ReqNumeridID"), true);
        }
        else
        {
            document.getElementById("DivNumeridID").style.display ="none";
            ValidatorEnable(document.getElementById("ReqNumeridID"), false);
        }
    }
}
//FB 2870 End
//ZD 103550 - Start
function fnBJNMeetingOptSelection() 
{
		//ZD 104116 - Start
        var BJNSelectOption;
        var BJNIntegration;
        var BJNDisplay;
        BJNSelectOption = "<%=Session["BJNSelectOption"] %>";
        BJNIntegration = "<%=Session["EnableBJNIntegration"] %>";//ZD 104116
        BJNDisplay = "<%=Session["BJNDisplay"] %>";

        if (document.getElementById("chkBJNMeetingID") != null) {
            if ((document.getElementById("chkBJNMeetingID").checked)) {
                document.getElementById("hdnChangeBJNVal").value = "1";
                if(BJNSelectOption == 1)
                    document.getElementById("DivBJNMeetingID").style.display = "";                
                else
                    document.getElementById("DivBJNMeetingID").style.display = "none"; 

                
                    if(document.getElementById("trVMR") != null)
                        document.getElementById("trVMR").style.display ="none";

                    if(document.getElementById("trPCConf") != null)
                        document.getElementById("trPCConf").style.display = "none";
                
            }
            else {
                document.getElementById("hdnChangeBJNVal").value = "0";
                document.getElementById("DivBJNMeetingID").style.display = "none";                  
                if(BJNIntegration == 1 && BJNDisplay == 0)
                {
                    if(document.getElementById("trVMR") != null)
                        document.getElementById("trVMR").style.display ="none";

                    if(document.getElementById("trPCConf") != null)
                        document.getElementById("trPCConf").style.display = "none";
                }
                else
                {

                if(('<%=Session["ShowHideVMR"]%>' == 1)&&('<%=Session["EnablePersonaVMR"]%>' == 1 ||'<%=Session["EnableRoomVMR"]%>' == 1 ||'<%=Session["EnableExternalVMR"]%>' == 1)) //ZD 104307
                    if(document.getElementById("trVMR") != null)
                        document.getElementById("trVMR").style.display =""; 
                    
                    if (document.getElementById("trPCConf") != null && '<%=Session["EnablePCUser"]%>' == 1) 
                        document.getElementById("trPCConf").style.display = "";
                } 
                //ZD 104116 - End                
            }
        }    
 }

 //ZD 103550 - End
//VMR
function changeVMR()
{
    var vmrParty = document.getElementById("isVMR");//ZD 100834
    var ShowVMRPIN; //ZD 100522
    var confType = document.getElementById("lstConferenceType").value; //ZD 100704

    //ALLDEV-826 Starts
    var reqValHostPwd = document.getElementById("reqValHostPwd");
    if (document.getElementById('lstVMR') != null) {
        if (document.getElementById('lstVMR').value == "2") {                 
            ValidatorEnable(reqValHostPwd, false);
        }
    }
    //ALLDEV-826 Ends

    //ZD 101869 - Start
    if((document.getElementById('lstVMR') != null && (document.getElementById('lstVMR').value == "1" || document.getElementById('lstVMR').value == "3"))
     || (document.getElementById('chkPCConf') != null && document.getElementById('chkPCConf').checked) || confType == 8 || confType == 7 || confType == 4)
    {
        if (document.getElementById("trLayout") != null) 
            document.getElementById("trLayout").style.display = "none";
        if (document.getElementById("tdMCUProfile") != null) //ZD 101931
           document.getElementById("tdMCUProfile").style.display = "none";
        if (document.getElementById("trPoolOrderSelection") != null) //ZD 104256 
           document.getElementById("trPoolOrderSelection").style.display = "none";
    }
    else
    {
        if (document.getElementById("trLayout") != null) 
            document.getElementById("trLayout").style.display = "";
        if (document.getElementById("tdMCUProfile") != null) //ZD 101931
           document.getElementById("tdMCUProfile").style.display = "";
           if (document.getElementById("trPoolOrderSelection") != null) //ZD 104256 
           document.getElementById("trPoolOrderSelection").style.display = "";
    }
    //ZD 101869 - End


    if(document.getElementById("lstVMR") != null) //FB 2620
    {
		//ZD 100704 Starts
        //ZD 100753 Starts
        var e = document.getElementById("lstVMR");
        var VMRType=  e.options[e.selectedIndex].value;
        var ConfPass = "<%=EnableConferencePassword %>";
        var HostGuestPass = "<%=EnableHostGuestPwd %>";    //ALLDEV-826
 	    ShowVMRPIN  = "<%=VMRPINChange %>";//ZD 100522

        if(document.getElementById("divChangeVMRPIN") != null)
            document.getElementById("divChangeVMRPIN").style.display = "None"; //ZD 100522


        if(VMRType == "2" || confType == 8 || confType == 7 ) //|| confType == 4) //ZD 100704 //ZD 103876
        {
            if(document.getElementById("trConfPass1") != null)
                document.getElementById("trConfPass1").style.display = 'None';
            if(document.getElementById('trConfPass') != null)
                document.getElementById('trConfPass').style.display = 'None';
            //ALLDEV-826 Starts
            if(document.getElementById("trHostPwd") != null)
                document.getElementById("trHostPwd").style.display = 'None';
            if(document.getElementById("trCfmHostPwd") != null)
                document.getElementById("trCfmHostPwd").style.display = 'None';
            //ALLDEV-826 Ends
            if(VMRType == "2" && document.getElementById("trExternalUsers") != null)
                document.getElementById("trExternalUsers").style.display = "";
        }
        else if(ConfPass == 1)
        {
            if(document.getElementById("trConfPass1") != null)
                document.getElementById("trConfPass1").style.display = '';
            if(document.getElementById('trConfPass') != null)
                document.getElementById('trConfPass').style.display = '';
        }
        //ALLDEV-826 Starts
        else if(HostGuestPass == 1)
        {
            if(document.getElementById("trConfPass1") != null)
                document.getElementById("trConfPass1").style.display = '';
            if(document.getElementById('trConfPass') != null)
                document.getElementById('trConfPass').style.display = '';
            if(document.getElementById("trHostPwd") != null)
                document.getElementById("trHostPwd").style.display = '';
            if(document.getElementById("trCfmHostPwd") != null)
                document.getElementById("trCfmHostPwd").style.display = '';
        }
        //ALLDEV-826 Ends
        //ZD 100753 Ends
        //ZD 100522
        if (document.getElementById("lstVMR") != null && document.getElementById("lstVMR").selectedIndex >0) // FB 2620
        {
      
            if(VMRType == 1) //ZD 100753
                document.getElementById("divbridge").style.display = "block";
            else
                document.getElementById("divbridge").style.display ="none";
            
            if(VMRType == 2) //ZD 100522 //ZD 103871 - Start
            { 
                document.getElementById("btnGuestLocation").style.visibility = "visible";
                if(ShowVMRPIN == "1")
                    if(document.getElementById("divChangeVMRPIN") != null)
                        document.getElementById("divChangeVMRPIN").style.display = "block";
            }
            else            
            {
                document.getElementById("OnFlyRowGuestRoom").style.visibility = "hidden";
                document.getElementById("btnGuestLocation").style.visibility = "hidden";
                document.getElementById("Image6").style.visibility = "hidden";
                document.getElementById("divIdRoomList").style.height = "1px";
            }//ZD 103871 - End
            //document.getElementById("idHr").style.visibility = "hidden";
            

            document.getElementById("trFECC").style.display ="none";//FB 2583 //FB 2595
            
            // FB 2501 Starts
            if('<%=isEditMode%>' == "0")
            {
                if(document.getElementById('ConferencePassword') != null )
                {
                  if(document.getElementById('ConferencePassword').value == "")
                    num_gen();
                }         
            }   
            if(document.getElementById('trStartMode1') != null && document.getElementById('trStartMode2') != null)
            {
               document.getElementById('trStartMode1').style.display = 'none';
               document.getElementById('trStartMode2').style.display = 'none';
            }         
            //ZD 103550
            if('<%=Session["EnableBJNIntegration"]%>' == 1)
            {
                if(document.getElementById('trBJNMeeting') != null)
                    document.getElementById('trBJNMeeting').style.display = 'None';
            }   
        }
        else
        {   
        	var enableFecc;
            if (document.getElementById('hdnEnableFECC') != null && document.getElementById('hdnEnableFECC').value != "")
                {
                    if (document.getElementById('hdnEnableFECC').value == "1")
                        enableFecc = document.getElementById('hdnEnableFECC').value;
                }
                else if (Session["EnableFECC"] != null)
                {
                    if (Session["EnableFECC"].ToString() == "1")
                    enableFecc = '<%=Session["EnableFECC"]%>';                        
                }

            if (enableFecc == "1" && ( confType == 2 || confType == 9|| confType == 6)) //FB 2583 //ZD 100704 //ZD 100513
                document.getElementById("trFECC").style.display = ""; //FB 2595//FB 2592
            else
                 document.getElementById("trFECC").style.display = "none"; //FB 2595
               
            if(document.getElementById('trStartMode') != null && document.getElementById('trStartMode1') != null && document.getElementById('trStartMode2') != null)
            {     
                if('<%=EnableStartMode%>' == 1 && (confType == 2  || confType == 9|| confType == 6))//ZD 100704 //ZD 100513
                {
                    document.getElementById('trStartMode').style.display = '';
                    document.getElementById('trStartMode1').style.display = '';
                    document.getElementById('trStartMode2').style.display = '';
                }
                else
                {
                    document.getElementById('trStartMode').style.display = 'none';
                    document.getElementById('trStartMode1').style.display = 'none';
                    document.getElementById('trStartMode2').style.display = 'none';
                }
            }
            // FB 2501 Ends     
            document.getElementById("divbridge").style.display ="none";
            if("<%=GuestRooms %>" != "0" && (confType == 2 || confType == 6 || confType ==  9 || confType == 4)) //ZD 100815 //ZD 100513
            {
                document.getElementById("OnFlyRowGuestRoom").style.visibility = "visible";
                document.getElementById("btnGuestLocation").style.visibility = "visible";
                document.getElementById("Image6").style.visibility = "visible";
                //document.getElementById("idHr").style.visibility = "hidden"; //ZD 100834
                document.getElementById("divIdRoomList").style.height = "150px";
            }
            else
            {
                document.getElementById("OnFlyRowGuestRoom").style.visibility = "hidden";
                document.getElementById("btnGuestLocation").style.visibility = "hidden";
                document.getElementById("Image6").style.visibility = "hidden";
                //document.getElementById("idHr").style.visibility = "hidden";
                document.getElementById("divIdRoomList").style.height = "1px";
            }
            //ZD 103550
            if('<%=Session["EnableBJNIntegration"]%>' == 1)
            {
                if(document.getElementById('trBJNMeeting') != null)
                    document.getElementById('trBJNMeeting').style.display = '';
            }
        }
   }
  
   //FB 2819 Starts
   var isPCCOnf;
   if(document.getElementById("chkPCConf") != null) 
      isPCCOnf = document.getElementById("chkPCConf").checked;
   
   var VMRselected =0;
   if(document.getElementById("lstVMR") != null) 
     VMRselected = document.getElementById("lstVMR").selectedIndex;
    
    if(isPCCOnf)
    {
       if(document.getElementById("isPCCOnf") != null)
            document.getElementById("isPCCOnf").value = "1";//ZD 100834
        document.getElementById("OnFlyRowGuestRoom").style.visibility = "hidden";
	    document.getElementById("btnGuestLocation").style.visibility = "hidden";
	    document.getElementById("Image6").style.visibility = "hidden";
	    
        if(document.getElementById("trVMR") != null)
            document.getElementById("trVMR").style.display ="none";
    
        if(document.getElementById("trStartMode") != null)
            document.getElementById("trStartMode").style.display = 'None';

        if(document.getElementById('trStartMode1') != null)
            document.getElementById('trStartMode1').style.display = 'None';

        //ZD 103550
        if('<%=Session["EnableBJNIntegration"]%>' == 1)
        {
            if(document.getElementById('trBJNMeeting') != null)
                document.getElementById('trBJNMeeting').style.display = 'none';
        }
    }
    else
    {
          if(document.getElementById("isPCCOnf") != null)
            document.getElementById("isPCCOnf").value = "0";//ZD 100834
          if(document.getElementById("trPCConf") != null)
			//ZD 104116 - Start
		    if('<%=EnablePCUser %>' == 1)  //ZD 100873 //ZD 103550
            {
                if ('<%=Session["EnableBJNIntegration"] %>' == 1 && '<%=Session["BJNDisplay"] %>' == 0)
                    document.getElementById("trPCConf").style.display = 'None';  
                else
                {
                    if(document.getElementById("chkBJNMeetingID") == null) 
                        document.getElementById("trPCConf").style.display ="";
                    else if(document.getElementById("chkBJNMeetingID").checked == false) 
                        document.getElementById("trPCConf").style.display ="";
                    else
                        document.getElementById("trPCConf").style.display = 'None';  
                 }
				//ZD 104116 - End
            }
         if (confType == 4 || confType == 7 || confType == 8 || VMRselected >0 || '<%=enableCloudInstallation%>' == 1 || '<%=EnablePCUser %>' == 0) //ZD 100166 //ZD 100704
            {
             if(document.getElementById("trPCConf") != null)
                document.getElementById("trPCConf").style.display = "none";
             if (document.getElementById("chkPCConf") != null) 
                document.getElementById("chkPCConf").checked = false;    
            }
            else
            {
                //ZD 103550
                if(VMRselected <= 0)
                {                
                    if('<%=Session["EnableBJNIntegration"]%>' == 1)
                    {
                        if(document.getElementById('trBJNMeeting') != null)
                            document.getElementById('trBJNMeeting').style.display = '';
                    }
                }                
            }
        
        
        //ZD 100166  
        if(document.getElementById("trVMR") != null)
        {
         //ZD 100707 Start //ZD 100704
         
            if('<%=enableCloudInstallation%>' == 1)
            {
                document.getElementById("trVMR").style.display = 'None';  
            }
            else if((confType == 2 || confType ==  9 || confType == 6) && ('<%=Session["ShowHideVMR"]%>' == 1)&&('<%=Session["EnablePersonaVMR"]%>' == 1 ||'<%=Session["EnableRoomVMR"]%>' == 1 ||'<%=Session["EnableExternalVMR"]%>' == 1)) //ZD 100513
            {
				//ZD 104116 - Start
                if ('<%=Session["EnableBJNIntegration"] %>' == 1 && '<%=Session["BJNDisplay"] %>' == 0)
                    document.getElementById("trVMR").style.display = 'None';  
                else
                {
                    if(document.getElementById("chkBJNMeetingID") == null) 
                        document.getElementById("trVMR").style.display ="";
                    else if(document.getElementById("chkBJNMeetingID").checked == false) 
                        document.getElementById("trVMR").style.display ="";
                    else
                        document.getElementById("trVMR").style.display = 'None';  
                }
            }
//            else
//            {
                var e = document.getElementById("lstVMR");
                var lstVMR =  e.options[e.selectedIndex].value;
                if ('<%=Session["EnableBJNIntegration"] %>' == 1 && '<%=Session["BJNDisplay"] %>' == 0)
                    document.getElementById("trVMR").style.display = 'None';  
                else
                {
                    if(lstVMR == "2" && (document.getElementById("chkBJNMeetingID") != null && document.getElementById("chkBJNMeetingID").checked == true))
                    document.getElementById("trVMR").style.display = '';
                }
//                else
//                    document.getElementById("trVMR").style.display ='None';
//            }
             //ZD 100707 End //ZD 104116 - End
        }
        if(document.getElementById("trBJNMeeting") != null)
        {
            if(confType == 2 || confType == 6 || confType == 9)
            {
                if(VMRselected <= 0)
                {
                    if('<%=Session["EnableBJNIntegration"]%>' == 1)
                    {
                        document.getElementById("trBJNMeeting").style.display = '';
                    }
                }
                else
                {                   

                    if('<%=Session["EnableBJNIntegration"]%>' == 1)
                    {
                        document.getElementById("trBJNMeeting").style.display = 'none';
                    }
                }
            }
            else
            {
                if('<%=Session["EnableBJNIntegration"]%>' == 1)
                {
                    document.getElementById("trBJNMeeting").style.display = 'none';
                }
            }

        }
        if('<%=Session["EnableStartMode"]%>' == 1 && (VMRselected ==0 &&(confType == 2|| confType ==  9 || confType == 6)))//ZD 100619 //ZD 100513
        {
         if(document.getElementById("trStartMode") != null)
            document.getElementById("trStartMode").style.display = '';

         if(document.getElementById('trStartMode1') != null)
            document.getElementById('trStartMode1').style.display = ''; 
        }
     }
    //FB 2819 Ends
    
    DisplayMCUConnectRow("<%=mcuSetupDisplay %>","<%=mcuTearDisplay %>",'<%=EnableBufferZone%>'); //FB 2998
    fnHideStartNow(); //ZD 100704

	//ZD 100834 Starts
     if (VMRselected > 0)
     {
          vmrParty.value = "1";
     }
    else
    {
        vmrParty.value = "0";
    }
	//ZD 100834 End
  return true;
}

//FB 2266 
    function refreshIframe()
    {
        var iframeId = document.getElementById('ifrmPartylist');
        iframeId.src = iframeId.src;
    }

function ChangeImmediate()
{
    var t;
	var t1;
	var confType = document.getElementById("lstConferenceType").value; //ZD 100704
    document.getElementById("lstDuration_Text").style.width = "75px";
    document.getElementById("confStartTime_Text").style.width = "75px";
    document.getElementById("confEndTime_Text").style.width = "75px";
	//FB 2634
    //document.getElementById("SetupTime_Text").style.width = "75px"; 
    //document.getElementById("TeardownTime_Text").style.width = "75px"; 
    
	document.getElementById("NONRecurringConferenceDiv9").style.display = 'None'; //FB 2266 
    if (document.frmSettings2.Recur.value == "")
    {
        if( document.getElementById("hdnRecurValue").value == 'R')
        {
             t = "none";
             t1 = "";
            Page_ValidationActive=false;
            document.getElementById("divDuration").style.display = "none";
        }
        else if (document.getElementById("chkStartNow").checked)  //FB 2341
        {
            t = "none";
            t1 = "none";
            Page_ValidationActive=false;
            document.getElementById("divDuration").style.display = "";
            //document.getElementById("chkEnableBuffer").checked = false;
        }
        else
        {
            t = "";
            t1 = "";
            Page_ValidationActive=true;
            document.getElementById("divDuration").style.display ="none";
        }
        //FB 2634 //ZD 100704
        if(confType == 8 || '<%=EnableBufferZone%>' == 0 || t1 == "none")//ZD 100166 //ZD 101543
         //ZD 101755 Start
        {
            document.getElementById("SetupRow").style.display = 'None';
            document.getElementById("TearDownRow").style.display = 'None';                                
        }
        else
        {
            if('<%=Session["EnableSetupTimeDisplay"]%>' == "1")
                document.getElementById("SetupRow").style.display = '';
            else
                document.getElementById("SetupRow").style.display = 'None';
            if('<%=Session["EnableTeardownTimeDisplay"]%>' == "1")
                document.getElementById("TearDownRow").style.display = '';
            else
                document.getElementById("TearDownRow").style.display = 'None';
        }
          //ZD 101755 End      
	    
	    document.getElementById("NONRecurringConferenceDiv8").style.display = t1;
	    document.getElementById("NONRecurringConferenceDiv3").style.display = t1;
	    document.getElementById("ConfStartRow").style.display = t1;
	    document.getElementById("ConfEndRow").style.display = t1;
	    //document.getElementById("NONRecurringConferenceDiv9").style.display = 'None'; //FB 2266
	    if(document.getElementById("recurDIV"))
	        document.getElementById("recurDIV").style.display = 'None';
 		
 		//FB 1911
	    var args = ChangeImmediate.arguments;
	    if(args != null)
	    {
	        if(args[0] == "S")
	        {
	            document.getElementById("recurDIV").style.display = "None"; // FB 2050
	            document.frmSettings2.RecurSpec.value = '';
	            document.frmSettings2.RecurringText.value = '';	            
	        }
	    }
	    
        //FB 2266
        //if ('<%=Application["recurEnable"]%>' == '0')
		if ('<%=Session["recurEnable"]%>' == "0") 
        {
            document.getElementById("NONRecurringConferenceDiv8").style.display = "none";
        }
        else if ("<%=isInstanceEdit%>" == "Y" )
            document.getElementById("NONRecurringConferenceDiv8").style.display = "none";
        else
        {
            document.getElementById("NONRecurringConferenceDiv8").style.display = t1;
        }
        
        if ('<%=timeZone%>' == "0" ) 
            document.getElementById("NONRecurringConferenceDiv3").style.display = "none";    
          
        document.getElementById("EndDateArea").style.display = t;
	    //document.getElementById("TeardownArea").style.display = t;
	    
	    fnEnableBuffer();
    }
    
    DisplayMCUConnectRow("<%=mcuSetupDisplay %>","<%=mcuTearDisplay %>",'<%=EnableBufferZone%>') //FB 2998
    
}


function isRecur()
{
    var t = (document.frmSettings2.Recur.value == "") ? "" : "none";
    
    if(isRecur.arguments.length > 0 || document.getElementById("hdnRecurValue").value == 'R')
    {
        if(isRecur.arguments[0] == 'R' || document.getElementById("hdnRecurValue").value == 'R')
            t = "None";
    }
    
//FB 2634
   if ("<%=client.ToString().ToUpper() %>" == "MOJ")  
   {  
        document.getElementById("StartNowRow").style.display = "none";//FB 2341
	    document.getElementById("ConfEndRow").style.display = "none"; 
    }
    else if("<%=EnableImmConf %>" == "0") //FB 2036
       document.getElementById("StartNowRow").style.display = "none";//FB 2341 
    else
       document.getElementById("StartNowRow").style.display = t;
 
	document.getElementById("lstDuration_Text").style.width = "75px";
    document.getElementById("confStartTime_Text").style.width = "75px";
    document.getElementById("confEndTime_Text").style.width = "75px";
//    document.getElementById("SetupTime_Text").style.width = "75px"; 
//    document.getElementById("TeardownTime_Text").style.width = "75px"; 
    if(t == "")
    {
        document.getElementById("RecurrenceRow").style.display = 'None';
        //document.getElementById("ConfEndRow").style.display = ''; 
        document.getElementById("DurationRow").style.display = 'None';
        
    }
    else
    {
        document.getElementById("RecurrenceRow").style.display = '';//Block
        document.getElementById("DurationRow").style.display = '';//Block
    }
        
	if (t != "")
	{
	    document.getElementById("<%=RecurFlag.ClientID %>").value="1";
	}
	
	if ("<%=timeZone%>" == "0" )
      document.getElementById("NONRecurringConferenceDiv3").style.display = "none"; 
      
	document.getElementById("EndDateArea").style.display = t;
    document.getElementById("StartDateArea").style.display = t;
    //document.getElementById("TeardownArea").style.display = t;//FB 2634
    document.getElementById("ConfEndRow").style.display = t;
      
      return false;
}


function getYourOwnEmailList (i,j) //ZD 100221  j : 0- Req , 1- Conf Host
{
	if(i == 999)
	    url = "emaillist2main.aspx?t=e&frm=party2NET&fn=Setup&n=" + i;	
	else
	    url = "emaillist2main.aspx?t="+j+"&frm=approverNET&fn=Setup&n=" + i; //ZD 100221
	if (!window.winrtc) {	// has not yet been defined
		winrtc = window.open(url, "", "width=920,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no"); //FB 2735
		winrtc.focus();
	} else // has been defined
	    if (!winrtc.closed) {     // still open
	    	winrtc.close();
	        winrtc = window.open(url, "", "width=920,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no"); //FB 2735
			winrtc.focus();
		} else {
	        winrtc = window.open(url, "", "width=920,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no"); //FB 2735
	        winrtc.focus();
		}
}
//FB 2670 Starts
function getVNOCEmailList()
{
    var Confvnoc = document.getElementById("hdnVNOCOperator");
    url = "VNOCparticipantlist.aspx?cvnoc="+Confvnoc.value;

    //ALLBUGS-110 Starts
    if (!window.winrtc) {
        winrtc = window.open(url, "", "width=920,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no");//FB 2596 //FB 2735
        winrtc.focus();
    }
    else if (!winrtc.closed) {
        winrtc.close();
        winrtc = window.open(url, "", "width=920,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no");//FB 2596 //FB 2735
        winrtc.focus();
    }
    else {
        winrtc = window.open(url, "", "width=920,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no");//FB 2596 //FB 2735
        winrtc.focus();
    } 
    //ALLBUGS-110 Ends
}

function deleteVNOC()
{
   document.getElementById('txtVNOCOperator').value = "";
   document.getElementById('hdnVNOCOperator').value = "";
}
//FB 2670 Ends
function goToCal()
{
	roomcalendarview();
}


function openAdvancesetting()
{
    var args = openAdvancesetting.arguments; //ALLDEV-814
	var chkadvance = document.getElementById("chkAdvancesetting");

    //ALLDEV-814
    if(args.length > 0 && args[0] == "C" && chkadvance.checked)
    {        
        document.getElementById("btnAdvSettings").click();
    }
	
	if(chkadvance.checked)
	    document.getElementById("Advancesetting").style.display = '';//Block
	else
	    document.getElementById("Advancesetting").style.display = 'None';
	//ZD 100834 Starts
    var chkOptions = document.getElementById("chkAdvancedOptions");
	
	if(chkOptions.checked)
	    document.getElementById("trOptions").style.display = '';
	else
	    document.getElementById("trOptions").style.display = 'None';

    var chkDetail = document.getElementById("chkDetails");
	
	if(chkDetail.checked)
	    document.getElementById("trDetails").style.display = '';
	else
	    document.getElementById("trDetails").style.display = 'None';
	//ZD 100834 End
	//ALLDEV-814
     if (!Page_ClientValidate() && chkadvance.checked)
     {
        chkadvance.checked = false;
        openAdvancesetting();
     }
    
}

function openRecur()
{

    //ZD 101714
    if(document.getElementById("hdnSpecRec") != null)
        document.getElementById("hdnSpecRec").value = "0";
    //ZD 101837
    var ConfRecurLimit = document.getElementById('spnRecurLimit').innerHTML;
    if(document.getElementById('chkWebex') != null)
    {
        if(document.getElementById('chkWebex').checked == true)
        {
            if(ConfRecurLimit > 50)
                document.getElementById('spnRecurLimit').innerHTML = 50;
        }
    }
    else
    {
        document.getElementById('spnRecurLimit').innerHTML = <%=Session["ConfRecurLimit"]%>;
    }
//FB 2634
	var confType = document.getElementById("lstConferenceType").value; //ZD 100704
	var chkrecurrence = document.getElementById("chkRecurrence");
    var args = openRecur.arguments;
    var recType = "";
    if(args.length > 0)
        if(args[0] == 'S')
            recType = "S";
            
    if((recType == "" && chkrecurrence.checked))
        recType = "N";
        
    document.getElementById("confStartTime_Text").style.width = "75px";
    document.getElementById("confEndTime_Text").style.width = "75px";
            
	if(recType == "N")
	{
	    document.getElementById("RecurrenceRow").style.display = '';
	    document.getElementById("<%=RecurFlag.ClientID %>").value="1";
	    
        document.getElementById("StartDateArea").style.display = 'None';
	    document.getElementById("EndDateArea").style.display = 'None';
        document.getElementById("StartNowRow").style.display = 'None';
        document.getElementById("divDuration").style.display = "None";
        document.getElementById("DurationRow").style.display = '';//FB 2634
        document.getElementById("ConfEndRow").style.display = 'None';
        document.getElementById("ConfStartRow").style.display = '';                
        
        if(confType == 8 || '<%=EnableBufferZone%>' == 0) //ZD 100166 //ZD 100704 //ZD 101543
            //ZD 101755 Start
        {
            document.getElementById("SetupRow").style.display = 'None';
            document.getElementById("TearDownRow").style.display = 'None';                                
        }
        else
        {
            if('<%=Session["EnableSetupTimeDisplay"]%>' == "1")
                document.getElementById("SetupRow").style.display = '';
            else
                document.getElementById("SetupRow").style.display = 'None';
            if('<%=Session["EnableTeardownTimeDisplay"]%>' == "1")
                document.getElementById("TearDownRow").style.display = '';
            else
                document.getElementById("TearDownRow").style.display = 'None';
        }
          //ZD 101755 End                 
        
        document.getElementById("RecurSpec").value = "";
        document.getElementById("hdnRecurValue").value = 'R';
        document.getElementById("recurDIV").style.display = 'None';       
        //FB 2634
        initial();
        fnShow();
        
	}
	else if (recType == "S")
	{
	    var st = document.getElementById("<%=lstConferenceTZ.ClientID%>");
        var sd = GetDefaultDate(document.getElementById("confStartDate").value,'<%=format%>')        
        var stime = document.getElementById("<%=confStartTime.ClientID%>");
        
        removerecur();
        
        chkrecurrence.checked = false;               
        document.getElementById("recurDIV").style.display = '';
        document.getElementById("SetupRow").style.display = 'None';                
        document.getElementById("ConfStartRow").style.display = 'None';                
        document.getElementById("ConfEndRow").style.display = 'None';
        document.getElementById("RecurrenceRow").style.display = 'None';
        document.getElementById("DurationRow").style.display = 'None';
        document.getElementById("StartNowRow").style.display = 'None';
        //FB 2588
        ChangeTimeFormat("D");
        //FB 2634
        top.recurWin = window.open('recurNET.aspx?pn=E&frm=frmSettings2&st='+ st.value + "&sd=" + sd + "&stime=" + stime 
        + "&su="+ document.getElementById("SetupDuration").value + "&tdn="+ document.getElementById("TearDownDuration").value 
        + "&wintype=pop", 'recur1', 'titlebar=yes,width=650,height=563,resizable=yes,scrollbars=yes,status=yes');//buffer zone
	}
	else
	{
        document.getElementById("DurationRow").style.display = 'None';
        document.getElementById("recurDIV").style.display = 'None';
        document.getElementById("hdnRecurValue").value = '';
	    document.getElementById("RecurrenceRow").style.display = 'None';
	    //ZD 100704 Starts
        if(confType == 8 || '<%=EnableBufferZone%>' == 0 ) //ZD 100166 //ZD 101543
            //ZD 101755 Start
        {
            document.getElementById("SetupRow").style.display = 'None';
            document.getElementById("TearDownRow").style.display = 'None';                                
        }
        else
        {
            if('<%=Session["EnableSetupTimeDisplay"]%>' == "1")
                document.getElementById("SetupRow").style.display = '';
            else
                document.getElementById("SetupRow").style.display = 'None';
            if('<%=Session["EnableTeardownTimeDisplay"]%>' == "1")
                document.getElementById("TearDownRow").style.display = '';
            else
                document.getElementById("TearDownRow").style.display = 'None';
        }
          //ZD 101755 End   
            
        document.getElementById("ConfStartRow").style.display = '';                
        document.getElementById("ConfEndRow").style.display = '';
        document.getElementById("StartDateArea").style.display = '';
	    document.getElementById("EndDateArea").style.display = '';
        if('<%=EnableImmConf %>' == "1" && confType != 8) //FB 2634
            document.getElementById("StartNowRow").style.display = '';
        else
        {
            document.getElementById("chkStartNow").checked = false;
            document.getElementById("StartNowRow").style.display = 'None';
        }   
        //removerecur();//FB 2605-Commented
	}
	
	fnHideStartNow();
	//ZD 100704 End
}

//FB 1911
function visControls()
{

    if(document.getElementById("RecurSpec").value == "")
    {
       var confType = document.getElementById("lstConferenceType").value;
	//FB 2634
	   document.getElementById("StartDateArea").style.display = '';
//	   document.getElementById("TeardownArea").style.display = '';
	   document.getElementById("EndDateArea").style.display = '';
	   //FB 2634
	   //ZD 100704
	   if(confType == 8 || '<%=EnableBufferZone%>' == 0 ) //ZD 100166 //ZD 101543
          //ZD 101755 Start
        {
            document.getElementById("SetupRow").style.display = 'None';
            document.getElementById("TearDownRow").style.display = 'None';                                
        }
        else
        {
            if('<%=Session["EnableSetupTimeDisplay"]%>' == "1")
                document.getElementById("SetupRow").style.display = '';
            else
                document.getElementById("SetupRow").style.display = 'None';
            if('<%=Session["EnableTeardownTimeDisplay"]%>' == "1")
                document.getElementById("TearDownRow").style.display = '';
            else
                document.getElementById("TearDownRow").style.display = 'None';
        }
          //ZD 101755 End   
	   
	   document.getElementById("TearDownRow").style.display = 'None';
	   document.getElementById("ConfStartRow").style.display = '';
	   document.getElementById("ConfEndRow").style.display = '';
	   //FB 2959 START
	   if('<%=EnableImmConf %>' == "1" && confType != 8)//FB 2634 //ZD 100704
        document.getElementById("StartNowRow").style.display = '';
       else
        document.getElementById("StartNowRow").style.display = 'None';
       //FB 2959 END

	   //ZD 103054
       if(document.getElementById("hdnRecurValue").value =="R" && confType == 8)
       {
         document.getElementById("StartDateArea").style.display = 'None';
	     document.getElementById("ConfEndRow").style.display = 'None';
       }
       //ZD 103054
       document.getElementById("recurDIV").style.display = 'None';     
       document.getElementById("RecurText").value = ''; 
       
       document.getElementById("confStartTime_Text").style.width = "75px";
//       document.getElementById("SetupTime_Text").style.width = "75px"; 
//       document.getElementById("TeardownTime_Text").style.width = "75px";          
	}
}

function showSpecialRecur()
{
	//FB 2634
    document.getElementById("recurDIV").style.display = '';
    document.getElementById("SetupRow").style.display = 'None';                
    document.getElementById("ConfStartRow").style.display = 'None';                
    document.getElementById("ConfEndRow").style.display = 'None';
    document.getElementById("EndDateArea").style.display = 'None';
    document.getElementById("StartDateArea").style.display = 'None';
    //document.getElementById("TeardownArea").style.display = 'None';
    document.getElementById("TearDownRow").style.display = 'None';
    document.getElementById("RecurrenceRow").style.display = 'None';
}

function roomcalendarview()
{   
    var frm = document.getElementById("confStartDate").value;
    rn = document.getElementById("selectedloc").value; 
    rn = getListViewChecked(rn);	
    url = "dispatcher/admindispatcher.asp?cmd=ManageConfRoom&f=v&hf=1&m=" + rn + "&d=" + frm;
    window.open(url,"","left=50,top=50,width=860,height=550,resizable=yes,scrollbars=yes,status=no");
    return false;
}

function roomconflict(confDate) //FB 1154 & //FB 2027 SetConference
{   
    var frm = confDate;
    rn = document.getElementById("selectedloc").value
    
    //rn = getListViewChecked(rn);	
    
    //url = "dispatcher/admindispatcher.asp?cmd=ManageConfRoom&f=v&hf=1&m=" + rn  + "&d=" + frm;
    url = "roomcalendar.aspx?f=v&hf=1&m=" + rn + "&d=" + confDate;
    window.open(url,"","left=50,top=50,width=860,height=550,resizable=yes,scrollbars=yes,status=no");
    return false;
}

function CallMeetingPla()
{   

        var confTimeZone = "<%=lstConferenceTZ.SelectedValue%>";
        var sd = "<%=confStartDate.Text %>";
        var stime = "<%=confStartTime.Text %>";
        var confDateTime = sd + " " + stime;
        var roomId = document.getElementById("selectedloc").value;

        roomId = getListViewChecked(roomId); 
        rn = roomId.split(",").length - 1;
        rId = roomId.split(",").length - 1;

        if(rn==0 || (rn==1 && roomId == ', '))
        {
          alert(AtLeastOneRoom)
          return false;
        }
        if(rn >5)
        {
          alert(OnlyfiveRoom)
          return false;
        }
       
        url = "MeetingPlanner.aspx?";
                url += "ConferenceDateTime=" + confDateTime + "&";
                url += "ConferenceTimeZone=" + confTimeZone + "&";
                url += "RoomID=" + roomId;
    
	  window.open(url,"","left=50,top=50,width=860,height=550,resizable=yes,scrollbars=yes,status=no");
      return false;
          
      }

function AudioAddon()
{
    var args = AudioAddon.arguments;
    
    var drptyp = document.getElementById("CreateBy");
    var controlName = 'dgUsers_ctl';
    var ctlid = '';
    
    if(drptyp)
    {
        if(drptyp.value == "2")
        {
            if(args)
            {
                var tbl1 = document.getElementById(args[0]);
                var tbl2 = document.getElementById(args[1]);
                var drp = document.getElementById(args[2]);
                var drp2 = document.getElementById(args[3]);
                 
                var usrArr=args[2].split("_");
                var len=usrArr.length;

                if(len > 2)
                {
                    ctlid = usrArr[1];
                    ctlid = ctlid.replace(/ctl/i,'');
                }
                
                var lstAddType = document.getElementById((controlName + ctlid +'_lstAddressType'));
                var lstConType = document.getElementById((controlName + ctlid +'_lstConnectionType'));
                var txtAdd = document.getElementById((controlName + ctlid +'_txtAddress'));

                var txtaddphone = document.getElementById((controlName + ctlid +'_txtaddphone'));
                var txtconfcode = document.getElementById((controlName + ctlid +'_txtConfCode'));
                var txtleader = document.getElementById((controlName + ctlid +'_txtleaderPin'));
                var txtPartyCode = document.getElementById((controlName + ctlid +'_txtPartyCode')); //ZD 101446
                
                if(drp.value == "2" && '<%=Application["EnableAudioAddOn"]%>' == "1")
                {
                    tbl1.style.display = 'none';
                    tbl2.style.display = '';//block
                    
                    if(lstAddType)
                    {
                        lstAddType.value = '1';
                    }
                    if(lstConType)
                    {
                        lstConType.value = '2';
                    }
                    if(txtAdd)
                    {
                        if(Trim(txtAdd.value) == '')
                            txtAdd.value = '127.0.0.1';
                    }
                }
                else
                {
                    if(txtaddphone)
                    {
                        if(Trim(txtaddphone.value) == '')
                        {
                            txtaddphone.value = '127.0.0.1';
                        }
                    }
                    if(txtconfcode)
                    {
                        if(Trim(txtconfcode.value) == '')
                            txtconfcode.value = '0';
                    }
                    if(txtleader)
                    {
                        if(Trim(txtleader.value) == '')
                            txtleader.value = '0';
                    }
                    if(txtPartyCode)
                    {
                        if(Trim(txtPartyCode.value) == '')
                            txtPartyCode.value = '0';
                    }
                    tbl1.style.display = '';//block
                    tbl2.style.display = 'none';
                }
                drp2.value = drp.value;
            }
                
        }
    }
}

</script>

<%--Merging Recurrence Script Start Here--%>

<script type="text/javascript">

var openerfrm = document.frmSettings2;


function CheckDate(obj)
{    
    var endDate;//FB 2246
    if ('<%=format%>'=='dd/MM/yyyy')
        endDate = GetDefaultDate(obj.value,'dd/MM/yyyy');
    else
        endDate = obj.value;
        
    if (Date.parse(endDate) < Date.parse(new Date()))
        alert(InvalidDate);
}

function fnHide()
{    
    document.getElementById("Daily").style.display = 'none';
    document.getElementById("Weekly").style.display = 'none';
    document.getElementById("Monthly").style.display = 'none';
    document.getElementById("Yearly").style.display = 'none';
    document.getElementById("Custom").style.display = 'none';
    document.getElementById("RangeRow").style.display = 'block';    
}

function fnShow()
{  
     var a = null;
     var f = document.forms[1];
     //ZD 103929
     var args = fnShow.arguments;
     if (args.length == 0)
         a = document.getElementById("hdnRecurType").value;
     else
         a = args[0];

     document.frmSettings2.RecPattern.value = a;
     /*
     var e = f.elements["RecurType"];
     for (var i=0; i < e.length; i++)
     {
     if (e[i].checked)
     {
     a = e[i].value;
     document.frmSettings2.RecPattern.value = a;
     break;
     }
     }
     */

    if(a != null)
    {
        fnHide();
        calendar = null; //ZD 100420
        switch(a)
        {
            case "1":
	            initialdaily(rpstr);
                document.getElementById("Daily").style.display = 'block';
                
                break;
            case "2":
	            initialweekly(rpstr);
                document.getElementById("Weekly").style.display = 'block';                            
                break;
            case "3":
	            initialmonthly(rpstr);
                document.getElementById("Monthly").style.display = 'block';                            
                break;
            case "4":
	            initialyearly(rpstr);
                document.getElementById("Yearly").style.display = 'block';                            
                break;
            case "5":
            
                document.getElementById('flatCalendarDisplay').innerHTML = "";  // clear the div values
                showFlatCalendar(1, dFormat,document.getElementById("StartDate").value);  //for custom calendar display      
	            initialcustomly(rpstr);
                document.getElementById("Custom").style.display = 'block';                            
                document.getElementById("RangeRow").style.display = 'none';
                break;            
        }
    }      
}

function initial()
{

    var setuphr;
    var setupmin;
    var setupap;
    var teardownhr;
    var teardownmin;
    var teardownap;
    //FB 2634
//    document.getElementById("hdnSetupTime").value = document.getElementById("SetupTime_Text").value;
//    document.getElementById("hdnTeardownTime").value = document.getElementById("TeardownTime_Text").value;
    
	if (document.getElementById("Recur").value=="") {
    	
	//FB 2588
	    ChangeTimeFormat("D")
		timeMin = document.getElementById("hdnStartTime").value.split(":")[1].split(" ")[0] ;
		timeDur = getConfDurNET(document.frmSettings2, dFormat, document.getElementById("confStartTime_Text").value, document.getElementById("confEndTime_Text").value); //FB 2588

		//ZD 103624
		if (isNaN(timeDur)) {
		    document.getElementById("chkRecurrence").checked = false;
		    openRecur();
		    return false;
		}

		if('<%=Session["timeFormat"].ToString()%>' == '0' || '<%=Session["timeFormat"].ToString()%>' == '2')//FB 2588
		    tmpstr = document.getElementById("lstConferenceTZ").options[document.getElementById("lstConferenceTZ").selectedIndex].value + "&" + document.getElementById("hdnStartTime").value.split(":")[0] + "&" + timeMin + "&" + "-1" + "&" + timeDur + "#"
		else
		    tmpstr = document.getElementById("lstConferenceTZ").options[document.getElementById("lstConferenceTZ").selectedIndex].value + "&" + document.getElementById("confStartTime_Text").value.split(":")[0] + "&" + timeMin + "&" + document.getElementById("confStartTime_Text").value.split(" ")[1] + "&" + timeDur + "#"
		
		tmpstr += "1&1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1#"
		tmpstr += document.getElementById("confStartDate").value + "&1&-1&-1";

 		document.frmSettings2.RecurValue.value = tmpstr;
		document.frmSettings2.RecurPattern.value = "1&1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1";
		
	}
	else
	{
	    setuptime = document.getElementById("hdnBufferStr").value.split("&")[0];
	    teardowntime = document.getElementById("hdnBufferStr").value.split("&")[1];
	    //FB 2634
//	    if(document.getElementById("SetupTime_Text"))
//	        document.getElementById("SetupTime_Text").value = setuptime;
//	    
//	    if(document.getElementById("TeardownTime_Text"))
//	        document.getElementById("TeardownTime_Text").value = teardowntime;
	}
     
    //FB 2266
    if(document.frmSettings2.RecurValue.value == "")
	    document.frmSettings2.RecurValue.value = document.getElementById("Recur").value; 

	rpstr = AnalyseRecurStr(document.frmSettings2.RecurValue.value);
	//FB 1635 starts
	if('<%=Session["timeFormat"].ToString()%>' == '0' || '<%=Session["timeFormat"].ToString()%>' == '2')//FB 2588
	{
	    if(atint[1] < 12 && atint[3] == "PM")
             atint[1] = atint[1] + 12;
    }
	//FB 1635 Ends
	
	if(atint[1] < 10)
	    atint[1] = "0" + atint[1];
	    
	//FB 1715
	if(atint[2] < 10)
	    atint[2] = "0" + atint[2];
	
	//FB 1799
	if('<%=Session["timeFormat"].ToString()%>' == '0' || '<%=Session["timeFormat"].ToString()%>' == '2')//FB 2588
	    startstr = (atint[1] == "12" && atint[3] == "AM"? "00" : atint[1]) + ":" + (atint[2]=="0" ? "00" : atint[2]);
	else
	    startstr = atint[1] + ":" + (atint[2]=="0" ? "00" : atint[2]) + " " + atint[3];
	    	
    //FB 2634
//	if(document.getElementById("confStartTime_Text"))
//	    document.getElementById("confStartTime_Text").value = startstr;
	
	var actualDur = atint[4];
    var setup = document.getElementById("SetupDuration").value;
    var teardown = document.getElementById("TearDownDuration").value;
    //ZD 100085
    //if(document.getElementById("Recur").value != "")
        //actualDur = parseInt(actualDur,10) - (parseInt(setup,10) + parseInt(teardown,10));
        
	if (document.frmSettings2.RecurDurationhr) {
		set_select_field (document.frmSettings2.RecurDurationhr, parseInt(actualDur/60, 10) , true);
		set_select_field (document.frmSettings2.RecurDurationmi, actualDur%60, true);
	}

    document.frmSettings2.RecurPattern.value = rpstr;
    //ZD 103929
    if (document.getElementById("RecurType_" + rpint[0])) {
        document.getElementById("RecurType_" + rpint[0]).checked = true;
        document.getElementById("hdnRecurType").value = rpint[0];
    }

//	if(document.frmSettings2.RecurType)
//	    document.frmSettings2.RecurType[rpint[0]-1].checked = true;

    document.frmSettings2.Occurrence.value ="";
    document.frmSettings2.EndDate.value ="";
   
	if (rpint[0] != 5) {
		document.frmSettings2.StartDate.value = rrint[0];

	
		switch (rrint[1]) 
		{
			case 1:
		        document.frmSettings2.EndType.checked = true;
				break;
			case 2:
		        document.frmSettings2.REndAfter.checked = true;
				document.frmSettings2.Occurrence.value = rrint[2];
				break;
			case 3:
		        document.frmSettings2.REndBy.checked = true;
				document.frmSettings2.EndDate.value = rrint[3];
				break;
			default:
				alert(EN_36);
				break;
		}
	}
	
}

function recurTimeChg()
{
   //FB 2588
    ChangeTimeFormat("D")   

    var aryStart = document.getElementById("hdnStartTime").value.split(" ");
    	
	rshr = aryStart[0].split(":")[0];
	rsmi = aryStart[0].split(":")[1];
	rsap = aryStart[1];

	if (document.frmSettings2.RecurDurationhr) {
		rdhr = parseInt(document.frmSettings2.RecurDurationhr.value, 10);
		rdmi = parseInt(document.frmSettings2.RecurDurationmi.value, 10);
		recurduration = rdhr * 60 + rdmi;
//        if (recurduration > (maxDuration *60)) //ZD 103925
//            alert(EN_314);
		document.frmSettings2.EndText.value = calEnd(calStart(rshr, rsmi, rsap), recurduration);
	}
	
	if (document.frmSettings2.RecurEndhr) {
		rehr = document.frmSettings2.RecurEndhr.options[document.frmSettings2.RecurEndhr.selectedIndex].value;
		remi = document.frmSettings2.RecurEndmi.options[document.frmSettings2.RecurEndmi.selectedIndex].value;
		reap = document.frmSettings2.RecurEndap.options[document.frmSettings2.RecurEndap.selectedIndex].value;
		
		document.frmSettings2.DurText.value = calDur(document.getElementById("confStartDate").value, document.getElementById("confEndDate").value, rshr, rsmi, rsap, rehr, remi, reap, 1);
	}
}

</script>

<%--daily Script--%>

<script type="text/javascript">
    function summarydaily() {
        dg = Trim(document.frmSettings2.DayGap.value);
        rp = "1&";
        rp += (document.frmSettings2.DEveryDay.checked) ? ("1&" + ((dg == "") ? "-1" : dg)) : "2&-1";
        rp += "&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1";

        document.frmSettings2.RecurPattern.value = rp;
    }

    function initialdaily(rp) {
        var rpary = (rp).split("&");
        if (rpary[0] == 1) {
            if (!isNaN(rpary[2]))
                dg = parseInt(rpary[2], 10);
            if ((dg == -1) || isNaN(dg))
                document.frmSettings2.DayGap.value = "";
            else
                document.frmSettings2.DayGap.value = dg;
            if (!isNaN(rpary[1]))
                dt = parseInt(rpary[1], 10);
            if ((dt == -1) || isNaN(dt))
                rpary[1] = 1;

            if (rpary[1] == 1)
                document.frmSettings2.DEveryDay.checked = true;
            else if (rpary[1] == 2)
                document.frmSettings2.DWeekDay.checked = true;
        }
        else {
            document.frmSettings2.DayGap.value = "";
            document.frmSettings2.DEveryDay.checked = true;
        }
    }

</script>

<%--Weekly Script--%>

<script type="text/javascript">

    function summaryweekly() {
        wg = Trim(document.frmSettings2.WeekGap.value);
        rp = "2&-1&-1&";
        rp += ((wg == "") ? "" : wg) + "&";

        var elementRef = document.getElementById("WeekDay");
        var checkBoxArray = elementRef.getElementsByTagName('input');
        var checkedValues = '';
        for (var i = 0; i < checkBoxArray.length; i++)
            rp += checkBoxArray[i].checked ? ((i + 1) + ",") : ""

        if (rp.substr(rp.length - 1, 1) == ",")
            rp = rp.substr(0, rp.length - 1);

        rp += "&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1";

        document.frmSettings2.RecurPattern.value = rp;
    }

    function initialweekly(rp) {
        var elementRef = document.getElementById("WeekDay");
        var checkBoxArray = elementRef.getElementsByTagName('input');
        rpary = (rp).split("&");
        if (rpary[0] == 2) {
            if (!(isNaN(rpary[3])))
                wg = parseInt(rpary[3], 10);
            if ((wg == -1) || isNaN(wg))
                document.frmSettings2.WeekGap.value = "1";
            else
                document.frmSettings2.WeekGap.value = wg;
            if (rpary[4] != -1) {
                wdary = (rpary[4]).split(",");

                for (i = 0; i < wdary.length; i++)
                    checkBoxArray[parseInt(wdary[i], 10) - 1].checked = true // ZD 101722
            }
        }
        else {
            document.frmSettings2.WeekGap.value = "1";

            for (var i = 0; i < checkBoxArray.length; i++) {
                if (checkBoxArray[i].checked == true)
                    checkBoxArray[i].checked = false;
            }
        }
    }
</script>

<%--Monthly Script--%>

<script type="text/javascript">

    function summarymonthly() {
        mdn = Trim(document.frmSettings2.MonthDayNo.value);
        mg1 = Trim(document.frmSettings2.MonthGap1.value);
        mg2 = Trim(document.frmSettings2.MonthGap2.value);
        rp = "3&-1&-1&-1&-1&";
        rp += (document.frmSettings2.MEveryMthR1.checked) ? ("1&" + ((mdn == "") ? "-1" : mdn) + "&" + ((mg1 == "") ? "-1" : mg1) + "&-1&-1&-1")
		: ("2&-1&-1&" + (document.frmSettings2.MonthWeekDayNo.selectedIndex + 1) + "&" + (document.frmSettings2.MonthWeekDay.selectedIndex + 1) + "&" + ((mg2 == "") ? "-1" : mg2));
        rp += "&-1&-1&-1&-1&-1&-1";

        document.frmSettings2.RecurPattern.value = rp;
    }

    function initialmonthly(rp) {
        rpary = (rp).split("&");
        if (rpary[0] == 3) {
            for (i = 5; i < 11; i++) {
                if (!isNaN(rpary[i]))
                    rpary[i] = parseInt(rpary[i], 10);
                else
                    rpary[i] = -1;
            }

            switch (rpary[5]) {
                case 1:
                    document.frmSettings2.MEveryMthR1.checked = true;
                    document.frmSettings2.MonthDayNo.value = (rpary[6] == -1) ? "" : rpary[6];
                    document.frmSettings2.MonthGap1.value = (rpary[7] == -1) ? "" : rpary[7];
                    document.frmSettings2.MonthWeekDayNo[0].checked = true;
                    document.frmSettings2.MonthWeekDay[0].checked = true;
                    document.frmSettings2.MonthGap2.value = "";
                    break;
                case 2:
                    document.frmSettings2.MEveryMthR2.checked = true;
                    document.frmSettings2.MonthDayNo.value = "";
                    document.frmSettings2.MonthGap1.value = "";
                    if (rpary[8] == -1)
                        document.frmSettings2.MonthWeekDayNo[0].selected = true;
                    else
                        document.frmSettings2.MonthWeekDayNo[rpary[8] - 1].selected = true;
                    if (rpary[9] == -1)
                        document.frmSettings2.MonthWeekDay[0].selected = true;
                    else
                        document.frmSettings2.MonthWeekDay[rpary[9] - 1].selected = true;
                    document.frmSettings2.MonthGap2.value = (rpary[10] == -1) ? "" : rpary[10];
                    break;
            }
        }
        else {
            document.frmSettings2.MEveryMthR1.checked = true;
            document.frmSettings2.MonthDayNo.value = "";
            document.frmSettings2.MonthGap1.value = "";
            document.frmSettings2.MonthWeekDayNo[0].checked = true;
            document.frmSettings2.MonthWeekDay[0].checked = true;
            document.frmSettings2.MonthGap2.value = "";
        }
    }
</script>

<%--Yearly Script--%>

<script type="text/javascript">

    function summaryyearly() {
        ymd = Trim(document.frmSettings2.YearMonthDay.value);
        rp = "4&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&";
        rp += (document.frmSettings2.YEveryYr1.checked) ? ("1&" + (document.frmSettings2.YearMonth1.selectedIndex + 1) + "&" + ((ymd == "") ? "-1" : ymd) + "&-1&-1&-1")
		: ("2&-1&-1&" + (document.frmSettings2.YearMonthWeekDayNo.selectedIndex + 1) + "&" + (document.frmSettings2.YearMonthWeekDay.selectedIndex + 1) + "&" + (document.frmSettings2.YearMonth2.selectedIndex + 1));

        document.frmSettings2.RecurPattern.value = rp;
    }

    function initialyearly(rp) {
        rpary = (rp).split("&");
        if (rpary[0] == 4) {
            for (i = 11; i < 17; i++) {
                if (!isNaN(rpary[i]))
                    rpary[i] = parseInt(rpary[i], 10);
                else
                    rpary[i] = -1;
            }
            switch (rpary[11]) {
                case 1:
                    document.frmSettings2.YEveryYr1.checked = true;
                    if (rpary[12] == -1)
                        document.frmSettings2.YearMonth1[0].selected = true;
                    else
                        document.frmSettings2.YearMonth1[rpary[12] - 1].selected = true;
                    document.frmSettings2.YearMonthDay.value = (rpary[13] == -1) ? "" : rpary[13];
                    document.frmSettings2.YearMonthWeekDayNo[0].selected = true;
                    document.frmSettings2.YearMonthWeekDay[0].selected = true;
                    document.frmSettings2.YearMonth2[0].selected = true;
                    break;
                case 2:
                    document.frmSettings2.YEveryYr2.checked = true;
                    document.frmSettings2.YearMonth1[0].selected = true;
                    document.frmSettings2.YearMonthDay.value = "";
                    if (rpary[14] == -1)
                        document.frmSettings2.YearMonthWeekDayNo[0].selected = true;
                    else
                        document.frmSettings2.YearMonthWeekDayNo[rpary[14] - 1].selected = true;
                    if (rpary[15] == -1)
                        document.frmSettings2.YearMonthWeekDay[0].selected = true;
                    else
                        document.frmSettings2.YearMonthWeekDay[rpary[15] - 1].selected = true;
                    if (rpary[16] == -1)
                        document.frmSettings2.YearMonth2[0].selected = true;
                    else
                        document.frmSettings2.YearMonth2[rpary[16] - 1].selected = true;
                    break;
            }
        }
        else {
            document.frmSettings2.YEveryYr1.checked = true;
            document.frmSettings2.YearMonth1[0].selected = true;
            document.frmSettings2.YearMonthDay.value = "";
            document.frmSettings2.YearMonthWeekDayNo[0].selected = true;
            document.frmSettings2.YearMonthWeekDay[0].selected = true;
            document.frmSettings2.YearMonth2[0].selected = true;
        }
    }
</script>

<%--Custom Script--%>

<script type="text/javascript" language="JavaScript">

    function removedate(cb) {
        if (cb.selectedIndex != -1) {
            cb.options[cb.selectedIndex] = null;
        }
        cal.refresh();
    }

    function initialcustomly(seldates) {
        if (seldates != "") {
            seldatesary = seldates.split("&");

            cb = document.getElementById("CustomDate");
            if (seldatesary[0].length > 2) {
                for (var i = 0; i < seldatesary.length; i++)
                    chgOption(cb, seldatesary[i], seldatesary[i], false, false)

                if (cal)
                    cal.refresh();
            }
        }
    }

    function summarycustomly() {
        var selecteddate = "";
        dFormat = document.getElementById("HdnDateFormat").value;
        SortDates();
        cb = document.getElementById("CustomDate");
        for (i = 0; i < cb.length; i++)
            selecteddate += cb.options[i].value + "&";

        document.frmSettings2.CutomDates.value = (selecteddate == "") ? "" : selecteddate.substring(0, selecteddate.length - 1);
    }

    function isOverInstanceLimit(cb) {
        csl = parseInt("<%=CustomSelectedLimit%>", 10); // ZD 101722

        if (!isNaN(csl)) {
            if (cb.length >= csl) {
                alert(EN_211)
                return true;
            }
        }

        return false;
    }

    function SortDates() {
        var temp;

        datecb = document.frmSettings2.CustomDate;

        var dateary = new Array();

        for (var i = 0; i < datecb.length; i++) {
            dateary[i] = datecb.options[i].value;

            dateary[i] = ((parseInt(dateary[i].split("/")[0], 10) < 10) ? "0" + parseInt(dateary[i].split("/")[0], 10) : parseInt(dateary[i].split("/")[0], 10)) + "/" + ((parseInt(dateary[i].split("/")[1], 10) < 10) ? "0" + parseInt(dateary[i].split("/")[1], 10) : parseInt(dateary[i].split("/")[1], 10)) + "/" + (parseInt(dateary[i].split("/")[2], 10));
        }

        for (i = 0; i < dateary.length - 1; i++)
            for (j = i + 1; j < dateary.length; j++)
            if (mydatesort(dateary[i], dateary[j]) > 0) {
            temp = dateary[i];
            dateary[i] = dateary[j];
            dateary[j] = temp;
        }


        for (var i = 0; i < dateary.length; i++) {
            datecb.options[i].text = dateary[i];
            datecb.options[i].value = dateary[i];
        }

        return false;
    }

    //-->
</script>

<%--Submit Recurence --%>

<script type="text/javascript">

function SubmitRecurrence()
{
    var chkrecurrence = document.getElementById("chkRecurrence");
    
    //ZD 103624
    var sDate = new Date(GetDefaultDate(document.getElementById("confStartDate").value,'<%=format%>') + " " + document.getElementById("hdnStartTime").value); 
    var eDate = Date.parse(GetDefaultDate(document.getElementById("confEndDate").value,'<%=format%>') + " " + document.getElementById("hdnEndTime").value); 
    if(!validateStartEndDate(sDate,eDate,1,1))
        return false;

    if (typeof(Page_ClientValidate) == 'function') 
    if (!Page_ClientValidate())
    {
        DataLoading(0);
        return false;
    }
    
    if(!CheckParty())
        return false;
        
    if(chkrecurrence != null && chkrecurrence.checked == true)
    {
        if (validateConfDuration())  
        {
            if(summary())
                return true;
        }
        return false;        
    }
}

function validateDurationHr()
{
    var obj = document.getElementById("RecurDurationhr");
    
    if (obj.value == "") 
         obj.value = "0";
         
    if (isNaN(obj.value)) 
    {
        //alert(EN_314);
        obj.focus();    
        return false;
    }

    var maxdur = '<%= Application["MaxConferenceDurationInHours"] %>';
    
    if(maxdur == "")
        maxdur = "120";
    
    if (obj.value < 0 || eval(obj.value) > eval(maxdur))
    {
        alert(EN_314);
        obj.focus();    
        return false;
    }
    return true;
}

function validateDurationMi()
{
    var obj = document.getElementById("RecurDurationmi");
    if (obj.value == "") 
        obj.value = "0";
    if (isNaN(obj.value))
    {
        alert(EN_314);
        obj.focus();
        return false;
    }
    return true;
}

function validateConfDuration()
{
    var obj = document.getElementById("RecurDurationhr");
    var obj1 = document.getElementById("RecurDurationmi");
    if (obj.value == "") 
         obj.value = "0";
         
    if (obj1.value == "") 
        obj1.value = "0";
         
    if (isNaN(obj.value)) 
    {
        alert(EN_314);
        return false;
    }
    if (isNaN(obj1.value)) 
    {
        alert(EN_314);
        return false;
    }

    var maxdur = '<%= Application["MaxConferenceDurationInHours"] %>';
    if(maxdur == "")
        maxdur = "24";

    var totDur = parseInt(obj.value, 10) * 60; // ZD 101722
    totDur = totDur + parseInt(obj1.value, 10); // ZD 101722
    
    if (totDur < 0 ) // || totDur > eval(maxdur*60)) //ZD  103925
    {
        alert(EN_314);
        return false;
    }
  
    return true;
}

function summary()
{
     SetRecurBuffer();
     switch(document.frmSettings2.RecPattern.value)
     {
        case "1":
		    summarydaily();
		    break;
		case "2":	
		    summaryweekly();
		    break;
        case "3":
		    summarymonthly();
		    break;
	    case "4":
		    summaryyearly();
		    break;
        case "5":
		    summarycustomly();
		    if(document.frmSettings2.CutomDates.value != "")
		    {
		        var cuDates = document.frmSettings2.CutomDates.value.split("&");
		        if(cuDates.length <=1)
		        {
		            alert(InstanceCheck);
		            return false;   
		        }
		    }
		    break;
     }
    
     if(document.frmSettings2.RecPattern.value != "5" && document.frmSettings2.REndAfter.checked)
     {
       if(document.frmSettings2.Occurrence.value <= 1)
       {
           alert(InstanceCheck);
           document.frmSettings2.Occurrence.focus();
           return false;   
       }
     }

	//FB 2634
	//var aryStart = document.getElementById("confStartTime_Text").value.split(" ");
	//FB 2588
    ChangeTimeFormat("D")
	var cstartdate = new Date(GetDefaultDate(document.getElementById("confStartDate").value,'<%=format%>') + " "
            + document.getElementById("hdnStartTime").value);
                
    var dura = parseInt(document.getElementById("SetupDuration").value,10)    
	//var sttime = setCDuration(cstartdate, -dura); //ZD 100085
	var sttime = cstartdate;
	var sttime1 = getCTime(sttime);
	var aryStart = sttime1.split(" ");
	
	var sh = aryStart[0].split(":")[0];//ZD 101837
	var sm = aryStart[0].split(":")[1];
	var ss = aryStart[1];
	var tz = document.getElementById("<%=lstConferenceTZ.ClientID%>").value;
	
	if('<%=Session["timeFormat"].ToString()%>' == '0' || '<%=Session["timeFormat"].ToString()%>' == '2') //FB 2588
	{
	    if(sh != "")
	    { 
	        if(eval(sh) >= 12)
	            ss = "PM";
	        else
	            ss = "AM";
	    }
	}
	//FB 2634
//	var aryStup = document.getElementById("confStartTime_Text").value.split(" ");
//	
//	setuphr = aryStup[0].split(":")[0];
//	setupmi = aryStup[0].split(":")[1];
//	setupap = aryStup[1];
//FB 2634
//	var aryTear = document.getElementById("confEndTime_Text").value.split(" ");
//	tearhr = aryTear[0].split(":")[0];
//	tearmi = aryTear[0].split(":")[1];
//	tearap = aryTear[1];
//	
//	if("<%=Session["timeFormat"].ToString()%>" == '0')
//	{ 
//	    startTime = sh + ":" + (sm =="0" ? "00" : sm);
//	    setupTime = setuphr + ":" + (setupmi =="0" ? "00" : setupmi);
//        teardownTime =  tearhr + ":" + (tearmi =="0" ? "00" : tearmi);
//    }
//	else
//	{
//	    startTime = sh + ":" + (sm =="0" ? "00" : sm) + " " + ss;
//	    setupTime = setuphr + ":" + (setupmi =="0" ? "00" : setupmi) + " " + setupap;
//        teardownTime =  tearhr + ":" + (tearmi =="0" ? "00" : tearmi) + " " + tearap;
//    }
//    
//    var insStDate = '';
//    if (document.frmSettings2.RecPattern.value == "5")
//    {
//        var cb = document.getElementById("CustomDate");

//	    if(cb.length > 0)
//	    {
//		    insStDate = cb.options[0].value;
//        }
//    }
//    else
//    {
//        insStDate = document.frmSettings2.StartDate.value;
//    }
//    startdate = insStDate + " " + startTime;
//   
//    var obj = document.getElementById("RecurDurationhr");
//    var obj1 = document.getElementById("RecurDurationmi");
//    
//    var totDur = parseInt(obj.value) * 60;
//    totDur = totDur + parseInt(obj1.value);
//    
//    if(totDur > 1440)
//    {
//        alert(EN_314);
//        return false;
//    }
//    
//    var confEndDt = '';
//    var sysdate = dateAddition(startdate,"m",totDur);
//    var dateP = sysdate.getDate();
//	
//	var monthN = sysdate.getMonth() + 1;
//	var yearN = sysdate.getFullYear();
//	var hourN = sysdate.getHours();
//	var minN = sysdate.getMinutes();
//	var secN = sysdate.getSeconds();
//	var timset = 'AM';
//	
//	if("<%=Session["timeFormat"].ToString()%>" == '0')
//	{
//	    timset = '';
//	}
//	else
//	{
//	    if(hourN == 12)
//	    {
//	        timset = 'PM';
//	    }
//	    else if( hourN > 12)
//	    {
//	         timset = 'PM';
//	         hourN = hourN - 12;
//	    }
//	    else if(hourN == 0)
//	    {
//	        timset = "AM";
//	        hourN = 12;
//	    }
//	 }
//	
//	var confDtAlone = monthN + "/" + dateP + "/" + yearN;
//	
//	confEndDt = monthN + "/" + dateP + "/" + yearN + " "+ hourN + ":" + minN + ":" + secN +" "+ timset;
//	if(document.getElementById("chkEnableBuffer").checked == true)	
//	{
//        setupDate = insStDate + " " + setupTime;
//        if(Date.parse(setupDate) < Date.parse(startdate))
//        {
//            setupDate = confDtAlone + " " + setupTime;
//        }
//        
//        teardownDate = confDtAlone + " " + teardownTime;
//        
//        if(Date.parse(confEndDt) < Date.parse(teardownDate))
//        {
//            teardownDate = insStDate + " " + teardownTime;
//        }
//      
//        if(Date.parse(setupDate) < Date.parse(startdate))
//        {
//            alert(EN_316);
//            return false;
//        }

//        if( (Date.parse(teardownDate) < Date.parse(setupDate)) || (Date.parse(teardownDate) < Date.parse(startdate)) || (Date.parse(teardownDate) > Date.parse(confEndDt)))
//        {
//             alert(EN_317);
//             return false;
//        }
//      
//        var setupDurMin = parseInt(Date.parse(setupDate) - Date.parse(startdate)) / (1000 * 60);
//        var tearDurMin = parseInt(Date.parse(confEndDt) - Date.parse(teardownDate)) / (1000 * 60);
//      }
//    else
//    {
//        setupDate = startdate;
//        teardownDate = confEndDt;
//    }  
//    
//    if(isNaN(setupDurMin))
//        setupDurMin = 0;
//    
//    if(isNaN(tearDurMin))
//        tearDurMin = 0;
//        
//    if(setupDurMin >0 || tearDurMin > 0)
//    {
//        if( (totDur -(setupDurMin + tearDurMin)) < 15)
//        {
//            alert(EN_314);
//            return false;
//        }
//    }
//    
//    document.frmSettings2.hdnBufferStr.value = setupTime + "&" + teardownTime;
//	document.frmSettings2.hdnSetupTime.value = setupDurMin;
//	document.frmSettings2.hdnTeardownTime.value = tearDurMin;
	
	if (document.frmSettings2.RecurDurationhr)
		dr = parseInt(document.frmSettings2.RecurDurationhr.value, 10) * 60 + parseInt(document.frmSettings2.RecurDurationmi.value, 10);
    
    //FB 2634
	if (dr < 15) 
    {
	    alert(EN_31);
	    return (false);		
	}
	//ZD 100085
    //dr = dr + parseInt(document.getElementById("SetupDuration").value,10) + parseInt(document.getElementById("TearDownDuration").value,10);
	
	if (document.frmSettings2.RecurEndhr) {
		rehr = document.frmSettings2.RecurEndhr.options[document.frmSettings2.RecurEndhr.selectedIndex].value;
		remi = document.frmSettings2.RecurEndmi.options[document.frmSettings2.RecurEndmi.selectedIndex].value;
		reap = document.frmSettings2.RecurEndap.options[document.frmSettings2.RecurEndap.selectedIndex].value;
		
		dr = calDur(sh, sm, ss, rehr, remi, reap, 0);
	}
	if ( dr < 15 ) {
		alert(EN_31);
		
		if (document.frmSettings2.RecurDurationhr)
			document.frmSettings2.RecurDurationhr.focus();
		if (document.frmSettings2.RecurEndhr)
			document.frmSettings2.RecurEndhr.focus();
			
		return (false);		
	}

	rv = tz + "&" + sh + "&" + sm + "&" + ss + "&" + dr + "#";
	recurrange = document.frmSettings2.StartDate.value + "&" + (document.frmSettings2.EndType.checked ? "1&-1&-1" : ( document.frmSettings2.REndAfter.checked ? ("2&" + document.frmSettings2.Occurrence.value + "&-1") : (document.frmSettings2.REndBy.checked ? ("3&-1&" + document.frmSettings2.EndDate.value) : "-1&-1&-1") ));
	rv += (document.frmSettings2.RecPattern.value == 5) ? ("5#" + document.frmSettings2.CutomDates.value) : (document.frmSettings2.RecurPattern.value + "#" + recurrange);
	if (frmRecur_Validator(document.frmSettings2.RecurPattern.value, recurrange, (document.frmSettings2.RecPattern.value == 5)?true:false)) {
		document.getElementById("Recur").value = rv;
	
		if (document.frmSettings2.EndText)
			endvalue = document.frmSettings2.EndText.value;
		if (document.frmSettings2.RecurEndhr)
			endvalue = document.frmSettings2.RecurEndhr.value + ":" + document.frmSettings2.RecurEndmi.value + " " + document.frmSettings2.RecurEndap.value;
		document.getElementById("RecurringText").value = recur_discription(rv, endvalue, document.frmSettings2.TimeZoneText.value, document.frmSettings2.StartDate.value,"<%=Session["timeFormat"].ToString()%>","<%=Session["timezoneDisplay"].ToString()%>");
		//FB 2588
		if('<%=Session["timeFormat"].ToString()%>' == '2')
		    document.getElementById("RecurringText").value = document.getElementById("RecurringText").value.replace(" AM","Z").replace(" PM","Z").replace(":","")
		isRecur();
		BtnCheckAvailDisplay ();
		return true;
	}
}

function frmRecur_Validator(rp, rr, isCustomly)
{
    //ZD 101837
    var ConfRecurLimit = <%=Session["ConfRecurLimit"]%>;
    if(document.getElementById('chkWebex') != null)
    {
        if(document.getElementById('chkWebex').checked == true)
        {
            if(ConfRecurLimit > 50)
                ConfRecurLimit = 50;
        }
    }
	if (parseInt(document.frmSettings2.Occurrence.value, 10) > parseInt(ConfRecurLimit, 10)) // 101722 //ZD 101837
	{
		alert(MaxLimit4 + ConfRecurLimit +MaxLimit5);
		return false;
	}
	
	if (isCustomly) {
		if (document.frmSettings2.CutomDates.value == "") {
			alert(EN_193)
			return false;
		} else
			return true;
	}

	if (chkPattern(rp))
		if (chkRange(rr))
			return true;
		else
			return false;
	else
		return false;
}

function chkPattern(rp)
{
	rpary = rp.split("&");
	
	switch (rpary[0]) {
		case "1":
			for (i=3; i<rpary.length; i++) {
				if  (rpary[i]!= "-1") {
					alert(EN_37);
					return false;
				}
			}

			switch (rpary[1]) {
				case "1":
					rst = isPositiveInt (rpary[2], "interval");
					if (rst == 1)
						return true;
					else
						return false;
					break;
				case "2":
					if (rpary[2] == "-1")
						return true;						
					else {
						alert(EN_37);
						return false;						
					}				
					break;
				default:
					alert(EN_38);
					return false;
					break;
			}
			break;

		case "2":
			for (i=1; i<3; i++) {
				if  (rpary[i]!= "-1") {
					alert(EN_37);
					return false;
				}
			}
			for (i=5; i<rpary.length; i++) {
				if  (rpary[i]!= "-1") {
					alert(EN_37);
					return false;
				}
			}
			rst = isPositiveInt (rpary[3], "weeks interval");
			if (rst == 1) {
				if (rpary[4]!="")
					return true;
				else {
					alert(EN_107);
					return false;
				}
			}
			break;
			
		case "3":
			for (i=1; i<5; i++) {
				if  (rpary[i]!= "-1") {
					alert(EN_37);
					return false;
				}
			}
			for (i=11; i<rpary.length; i++) {
				if  (rpary[i]!= "-1") {
					alert(EN_37);
					return false;
				}
			}
			
			switch (rpary[5]) {
				case "1":
					for (i=8; i<11; i++) {
						if  (rpary[i]!= "-1") {
							alert(EN_37);
							return false;
						}
					}
					if ( (isPositiveInt (rpary[6], "days interval") == 1) && (isPositiveInt (rpary[7], "months interval") == 1) )
						if ( isMonthDayNo(parseInt(rpary[6], 10)) )
							return true;
						else
							return false;
					else
						return false;
					break;
				case "2":
					for (i=6; i<8; i++) {
						if  (rpary[i]!= "-1") {
							alert(EN_37);
							return false;
						}
					}
					if (isPositiveInt(rpary[10], "months interval") == "1")
						return true;						
					else {
						return false;						
					}				
					break;
				default:
					alert(EN_38);
					return false;
					break;
			}
			break;
			
		case "4":
			for (i=1; i<11; i++) {
				if  (rpary[i]!= "-1") {
					alert(EN_37);
					return false;
				}
			}
			switch (rpary[11]) {
				case "1":
					for (i=14; i<rpary.length; i++) {
						if  (rpary[i]!= "-1") {
							alert(EN_37);
							return false;
						}
					}
					if (isPositiveInt (rpary[13], "date of the month") == 1)
						if ( isYearMonthDay(parseInt(rpary[12], 10), parseInt(rpary[13], 10)) )
							return true;
						else
							return false;
					else
						return false;						
					break;
				case "2":
					for (i=12; i<14; i++) {
						if  (rpary[i]!= "-1") {
							alert(EN_37);
							return false;
						}
					}
					return true;
					break;
				default:
					alert(EN_38);
					return false;
					break;
			}
			break;
		default:
			alert(EN_38);
			return false;
			break;
	}
}

var british = false;

function chkRange(rr)
{
	rrary = rr.split("&");
    
//    if('<%=Session["FormatDateType"]%>' == 'dd/MM/yyyy')
//        british = true; //104097
    
    var dDate = GetDefaultDate(rrary[0],dFormat);
     //ZD 103929
	if ( (!isValidDate(dDate)) || ( caldatecompare(dDate, servertoday) == -1 ) ) {		// check start time is reasonable future time
		alert(EN_74);
		document.frmSettings2.StartDate.focus();
		return (false);
	}

	switch (rrary[1]) {
		case "1":
			if ( (rrary[2]!= "-1") && (rrary[3]!= "-1") ) {
				alert(EN_38);
				return false;
			} else 
				return true;
			break;
		case "2":
			if (rrary[3]!= "-1") {
				alert(EN_38);
				return false;
			} else {
				if ( isPositiveInt(rrary[2], "times of occurrences")==1 )
					return true;
				else {
					document.frmSettings2.Occurrence.focus();
					return false;
				}
			}
			break;
		case "3":
			if (rrary[2]!= "-1") {
				alert(EN_38);
				return false;
			} else {
				if ( (!isValidDate(GetDefaultDate(rrary[3],dFormat))) ||  (caldatecompare(GetDefaultDate(rrary[3],dFormat), servertoday)== -1) ) { //104097
					alert(EN_108);
					document.frmSettings2.EndDate.focus();
					return false;
				} else
				{
					//ZD 104854 - Start
				    var fstdate = GetDefaultDate(rrary[0],dFormat);  //FB 2366
			        var snddate = GetDefaultDate(rrary[3],dFormat);
					//ZD 104854 - End
//			        if(!british) 104097
//				    {
//				        fstdate = GetDefaultDate(rrary[0],"dd/MM/yyyy");
//				        snddate = GetDefaultDate(rrary[3],"dd/MM/yyyy");				         
//				    }
					if ( !dateIsBefore(fstdate,snddate) ) {
						alert(EN_109);
						document.frmSettings2.EndDate.focus();
						return false;
					} else
						return true;
				}
			}
			return false;
			break;
		default:
			alert(EN_38);
			return false;
			break;
	}	
}

function BtnCheckAvailDisplay ()
{		
	if ( (e = document.getElementById("btnCheckAvailDIV")) != null ) {
		e.style.display = (document.getElementById("Recur").value=="") ? "" : "none";
	}
}
</script>

<script type="text/javascript">

    function removerecur() {
        document.getElementById("Recur").value = "";
        document.getElementById("RecurringText").value = "";
        document.getElementById("hdnRecurValue").value = "";
        document.getElementById('RecurValue').value = "";
        isRecur();
        initial();

        return false;
    }
    function fnRemoveFiles() {
        var lblflelist = document.getElementById("lblFileList");
        var lblCtrl = document.getElementById(fnRemoveFiles.arguments[0]);

        if (lblflelist != null && lblCtrl != null) {
            lblflelist.innerText = lblflelist.innerText.replace(lblCtrl.innerText + "; ", "");
        }

    }

    function clearError(controlID, clearId) {
        var ErrorLabel = document.getElementById(clearId);
        if (controlID.value != "") {
            ErrorLabel.innerText = "";
        }

    }

    function fnCheckSpecialCharacters(ID) {
        if (document.getElementById(ID) != null)//FB 2509
        {
            var controlID = document.getElementById(ID);
            var specialChars = "&<>+'dD";

            for (var i = 0; i < controlID.value.length; i++) {
                if (specialChars.indexOf(controlID.value.charAt(i)) != -1) {
                    controlID.focus();
                    return false;
                }
            }
        }
        return true;
    }

    function isNumber(ID) {
        var numbers = '0123456789';
        var controlID = document.getElementById(ID);

        for (i = 0; i < controlID.value.length; i++) {
            if (numbers.indexOf(controlID.value.charAt(i), 0) == -1) {
                controlID.focus();
                return false;
            }
        }
        return true;
    }

    function fnCheckRequiredField() {
    //ZD 101446 Starts
    //var LeaderPin ,EnablePartyCode,ConferenceCode; //Code commented for ZD 103530


//Code commented for ZD 103530 START
//    if(document.getElementById("hdnCrossConferenceCode").value != null)
//        ConferenceCode = document.getElementById("hdnCrossConferenceCode").value;
//    else 
//        ConferenceCode = "<%=Session["ConferenceCode"] %>";

//    if(document.getElementById("hdnCrossLeaderPin").value != null)
//        LeaderPin = document.getElementById("hdnCrossLeaderPin").value;
//    else 
//        LeaderPin = "<%=Session["LeaderPin"] %>";

//    if(document.getElementById("hdnCrossPartyCode").value != null)
//        EnablePartyCode = document.getElementById("hdnCrossPartyCode").value;
//    else 
//        EnablePartyCode = "<%=Session["EnablePartyCode"] %>";

        //ZD 101446 Starts
        var isRequired = true;                
        if (document.getElementById("hdnCrossPartyCode") != null) {//FB 2509
            if (document.getElementById("txtAudioDialNo") != null && document.getElementById("txtAudioDialNo").value == "")//@@@@@
            {
                document.getElementById("lblError2").innerText = "Required";
                document.getElementById("txtAudioDialNo").focus();
                //isRequired=false;
                return false;
            }
        }
        else
            return true;
//        if(ConferenceCode == "1") //ZD 101446
//        {
//            if (document.getElementById("txtConfCode") != null && document.getElementById("txtConfCode").value == "") {
//                document.getElementById("lblError3").innerText = "Required";
//                document.getElementById("txtConfCode").focus();
//                isRequired = false;
//            }
//        }
//        if(LeaderPin == "1") //ZD 101446
//            {
//            if (document.getElementById("txtLeaderPin") != null && document.getElementById("txtLeaderPin").value == "") {
//                document.getElementById("lblError4").innerText = "Required";
//                document.getElementById("txtLeaderPin").focus();
//                isRequired = false;
//            }
//        }
//        if(EnablePartyCode == "1") //ZD 101446
//        {
//            if (document.getElementById("txtPartyCode") != null && document.getElementById("txtPartyCode").value == "") { //ZD 101446
//                document.getElementById("lblErrorParty").innerText = "Required";
//                document.getElementById("txtPartyCode").focus();
//                isRequired = false;
//            }
//        }
        if (isRequired == false) {
            return false;
        }
        return true;
    }

    //FB 2634
    //Code commented for ZD 103530 END
function fnValidation() 
{
    //FB 2588
    ChangeTimeFormat("D")
    //Remove Date Validation
    var sDate = Date.parse(document.getElementById("confStartDate").value + " " + document.getElementById("hdnStartTime").value); 
    var eDate = Date.parse(document.getElementById("confEndDate").value + " " + document.getElementById("hdnEndTime").value); 

    var actEndate = new Date(sDate);
    var timeMins = 15;
    actEndate.setMinutes(actEndate.getMinutes() + parseInt(timeMins, 10)); // ZD 101722

    var errStr = "";
    if ((eDate <= sDate && document.getElementById("chkRecurrence").checked == false)) {// FB 2692  || (eDate <= sDate &&  document.getElementById("RecurSpec").value == "") ZD 101204
        errStr = EndDateValidation;
        document.getElementById("confEndTime_Text").focus();
    }
    else if (eDate < actEndate && document.getElementById("chkRecurrence").checked == false) {// FB 2692 || eDate <= sDate && document.getElementById("RecurSpec").value == ""ZD 101204
    errStr = InvalidDuration2;
        document.getElementById("confEndTime_Text").focus();
   
    }

    if (errStr != "") {
        alert(errStr);
        return false;
    }
	//FB 2997 starts
    /*if (document.getElementById("ConferenceName").value == "") {
        document.getElementById("lblConferenceNameError").innerText = "Required";
        document.getElementById("ConferenceName").focus();
        return false;
    } */ 
	//FB 2997 ends
    //FB 2592 - Start
    var hdnCusID = document.getElementById("hdnCusID");
    if (hdnCusID) {
        var Str1 = hdnCusID.value.split('?');
        var i
        var ctrlName = "";
        for (i = 0; i < Str1.length; i++) {
            var r = document.getElementById("ReqValCA_" + Str1[i].split("_")[2]);
            if (r) {
                ValidatorValidate(r)
                if (!r.isvalid) {
                    //ZD 101886
                    //var chkadvance = document.getElementById("chkAdvancesetting");
                    var chkadvance = document.getElementById("chkAdvancedOptions");
                    if (chkadvance) {
                        chkadvance.checked = true;
                        openAdvancesetting();
                        //ZD 101886
                        if (ctrlName == "")
                            ctrlName = Str1[i];
                    }
                }
            }
        }
        //ZD 101866
        if (ctrlName != "") {
            window.scrollTo(0, 0);
            document.getElementById(ctrlName).focus();
        }
    }
    //FB 2592 - End

    var list = "";
    var audinsitems = document.getElementsByName("lstAudioParty"); //FB 2341
    for (var i = 0; i < audinsitems.length; i++) {
        if (audinsitems[i].checked) {
            list = audinsitems[i].value;
        }
    }

    if (list != "-1") {
        if (fnCheckRequiredField() == false) {
            return false;
        }
//ZD 103574
//        if (fnCheckSpecialCharacters('txtAudioDialNo') == false) {
//            return false;
//        }
//        if (fnCheckSpecialCharacters('txtConfCode') == false) {
//            return false;
//        }
//        if (fnCheckSpecialCharacters('txtLeaderPin') == false) {
//            return false;
//        }
//        if (fnCheckSpecialCharacters('txtPartyCode') == false) { //ZD 101446
//            return false;
//        }
    }
    else {
        document.getElementById("lblError2").innerText = "";
        document.getElementById("lblError3").innerText = "";
        document.getElementById("lblError4").innerText = "";
        document.getElementById("lblErrorParty").innerText = ""; //ZD 101446
        return true;
    }
    return true;
}


    function fnValidation1() {

        //Remove Date Validation
        var setupdate = Date.parse(document.getElementById("confStartDate").value + " " + document.getElementById("confStartTime_Text").value); //Actual Setup
        var sDate = Date.parse(document.getElementById("SetupDate").value + " " + document.getElementById("SetupTime_Text").value); //Actual Start - Name InterChanged :(
        var tDate = Date.parse(document.getElementById("confEndDate").value + " " + document.getElementById("confEndTime_Text").value); //Actula Teardown time
        var eDate = Date.parse(document.getElementById("TearDownDate").value + " " + document.getElementById("TeardownTime_Text").value); //Actual End TIme

        var actEndate = new Date(sDate);
        var timeMins = 15;
        actEndate.setMinutes(actEndate.getMinutes() + parseInt(timeMins, 10)); // ZD 101722

        var errStr = "";
        if (setupdate > sDate) {
            errStr = StartTimeValid;
            document.getElementById("confStartTime_Text").focus();
        }
        else if (eDate <= sDate) {
        errStr = EndTimeValidation;
            document.getElementById("TeardownTime_Text").focus();
        }
        else if (eDate < actEndate) {
        errStr = InvalidDuration2;
            document.getElementById("TeardownTime_Text").focus();
        }

        if (errStr != "") {
            alert(errStr);
            return false;
        }
		//FB 2997 starts
        /*if (document.getElementById("ConferenceName").value == "") {
            document.getElementById("lblConferenceNameError").innerText = "Required";
            document.getElementById("ConferenceName").focus();
            return false;
        } */
		//FB 2997  ENDS
        //FB 2592 - Start
        var hdnCusID = document.getElementById("hdnCusID");
        if (hdnCusID) {
            var Str1 = hdnCusID.value.split('?');
            var i
            for (i = 0; i < Str1.length; i++) {
                var r = document.getElementById("ReqValCA_" + Str1[i].split("_")[2]);
                if (r) {
                    ValidatorValidate(r)
                    if (!r.isvalid) {
                        var chkadvance = document.getElementById("chkAdvancesetting");
                        if (chkadvance) {
                            chkadvance.checked = true;
                            openAdvancesetting()
                        }
                    }
                }
            }
        }
        //FB 2592 - Start
        
        
        
        var list = "";
        var audinsitems = document.getElementsByName("lstAudioParty"); //FB 2341
        for (var i=0; i < audinsitems.length; i++)
        {
            if (audinsitems[i].checked)
            {
                list = audinsitems[i].value;
            }
        }

        

        if (list != "-1") {
            if (fnCheckRequiredField() == false) {
                return false;
            }
//ZD 103574
//            if (fnCheckSpecialCharacters('txtAudioDialNo') == false) {
//                return false;
//            }
//            if (fnCheckSpecialCharacters('txtConfCode') == false) {
//                return false;
//            }
//            if (fnCheckSpecialCharacters('txtLeaderPin') == false) {
//                return false;
//            }
//            if (fnCheckSpecialCharacters('txtPartyCode') == false) { //ZD 101446
//                return false;
//            }
        }
        else {
            document.getElementById("lblError2").innerText = "";
            document.getElementById("lblError3").innerText = "";
            document.getElementById("lblError4").innerText = "";
            document.getElementById("lblErrorParty").innerText = ""; //ZD 101446
            return true;
        }
        return true;

    }

    //FB 1830 Email Edit - start
    function fnemailalert() {
        //ZD 101120 Starts
        if (document.getElementById("hdnGLWarningPopUp") != null) {
            
            var alertmessage, GLCount = "0";
            var GLWarningPopUP = document.getElementById("hdnGLWarningPopUp");
            var isPopUP = GLWarningPopUP.value.trim();

            var GLApprTime = document.getElementById("hdnGLApprTime");
            var GLAppr = GLApprTime.value.trim(); var ActualGLApprTime;

            var GLConfTime = document.getElementById("hdnGLConfTime");
            var GLConf = GLConfTime.value.trim();
            GLCount = document.getElementById("hdnGLCount").value;
            if (GLAppr == "0")
                ActualGLApprTime = 24;
            else if (GLAppr == "2")
                ActualGLApprTime = 72;
            else
                ActualGLApprTime = 48;

            if (isPopUP == "1") //Guest Location Pop up
            {
                if (GLCount != "" && GLCount != "0") {
                    //ZD 101500 - Start
                   
                   if (document.getElementById("dataLoadingDIV1") != null)
                        document.getElementById("dataLoadingDIV1").style.display = 'none';
                   
                    //ZD 101500 - End
                    if (GLConf == "1") {
                        alertmessage = Confalertmessage1 + ActualGLApprTime + Confalertmessage2;
                        alert(alertmessage);
                    } else {
                        alertmessage = Confalertmessag1 + ActualGLApprTime + Confalertmessag2;
                        alert(alertmessage);
                    }

                    if (document.getElementById("dataLoadingDIV1") != null)
                        document.getElementById("dataLoadingDIV1").style.display = 'block';
                   
                }
            }
        }
        //ZD 101120 Ends

        var trRooms = document.getElementById("modalrooms");

        var trfiles = document.getElementById("fileuploadsection");

        if (trfiles) {
            trfiles.style.display = 'None';
        }
        if (trRooms) {
            trRooms.style.display = 'none';
        }
        
        setTimeout("fnClick()", 100);//ZD 101538 
    }

    //ZD 101538 - Start
    function fnClick() {
        document.getElementById('hdnSaveData').value = '1';
        if (document.getElementById('btnDummy')) {
            document.getElementById('btnDummy').click();
        }
    }
    //ZD 101538 - End          
        
    
    //FB 1830 Email Edit - end

    // ZD 100828 Starts
    function fnVerifySmartP2P() {
    
        var count = 0, isGusetLoc = 0; //ZD 101057
        var confType = 0;
        if(document.getElementById("lstConferenceType") != null)
            confType = document.getElementById("lstConferenceType").value;

        if (document.getElementById("lstAudioParty") != null) {
            var length = document.getElementById("lstAudioParty").getElementsByTagName("tr").length;
            if (length > 1 && document.getElementById("lstAudioParty_0").checked == false)
                count++;
        }

        if (document.getElementById("RoomList") != null && document.getElementById("RoomList").length > 0) 
            count += document.getElementById("RoomList").length;
        
        if (document.getElementById("chkPCConf") != null)
            if (document.getElementById("chkPCConf").checked == true)
                count = 0;

        if (document.getElementById("lstVMR") != null)
            if (document.getElementById("lstVMR").value != "0")
                count = 0;
        //ZD 101057 Starts
        if (document.getElementById("hdnGusetLoc") != null)
            if (document.getElementById("hdnGusetLoc").value == "1")
                isGusetLoc = 1;
        //ZD 101057 Ends

        //ZD 100834 Starts
        if ("<%=enableDetailedExpressForm %>" == 1 && document.getElementById("hdnextusrcnt") != null && document.getElementById("hdnextusrcnt").value != "") {
            var extusrcnt = 0;
            extusrcnt = parseInt(document.getElementById("hdnextusrcnt").value, 10); // ZD 101722
            if (extusrcnt > 0)
                count += extusrcnt;
        }
        //ZD 100834 Ends
//        //ZD 101371 Starts
//        var e, VMRType = 0;
//        if (document.getElementById("lstVMR"))
//            e = document.getElementById("lstVMR");
//        if (e != null)
//            VMRType = e.options[e.selectedIndex].value;
//        //ZD 100371 Ends
//        if (count == 2 && '<%=Session["EnableSmartP2P"]%>' == "1" && (confType == 2 || confType == 6) && "<%=EnableExpressConfType %>" == 1 && isGusetLoc != 1 && VMRType ==0) {  //ZD 101057 //ZD 101371
//            if (confirm("This conference type is being changed to point-to-point and will not use an MCU connection")) {
//                document.getElementById("hdnconftype").value = "4";
//                document.getElementById("lstConferenceType").value = "4";
//            }
//        }
    }
    // ZD 100828 Ends
    //ZD 100815 Start
    function InitSetConference() {

        document.getElementById('hdnSaveData').value = '1';
        var count = 0, isGusetLoc = 0; //ZD 101057
        var confType = 0;
        if (document.getElementById("lstConferenceType") != null)
            confType = document.getElementById("lstConferenceType").value;

        if (document.getElementById("lstAudioParty") != null) {
            var length = document.getElementById("lstAudioParty").getElementsByTagName("tr").length;
            if (length > 1 && document.getElementById("lstAudioParty_0").checked == false)
                count++;
        }

        if (document.getElementById("RoomList") != null && document.getElementById("RoomList").length > 0)
            count += document.getElementById("RoomList").length;

        if (document.getElementById("chkPCConf") != null)
            if (document.getElementById("chkPCConf").checked == true)
            count = 0;

        if (document.getElementById("lstVMR") != null)
            if (document.getElementById("lstVMR").value != "0")
            count = 0;
        if ("<%=enableDetailedExpressForm %>" == 1 && document.getElementById("hdnextusrcnt") != null && document.getElementById("hdnextusrcnt").value != "") {
            var extusrcnt = 0;
            extusrcnt = parseInt(document.getElementById("hdnextusrcnt").value, 10); // ZD 101722
            if (extusrcnt > 0)
                count += extusrcnt;
        }

        DataLoading(1);

        var conftype;
        if (document.getElementById("lstConferenceType") != null)
            conftype = document.getElementById("lstConferenceType").value;
        if (document.getElementById("hdnRecurToggle").value == "0" && count == 2 && conftype != "4") {
            if (document.getElementById("hdnSmartP2PNotify").value == "1") {
                document.getElementById("p2pConfirmBtn").click();
                return false;
            }
            else if (document.getElementById("hdnSmartP2PNotify").value == "0") {
                document.getElementById("hdnconftype").value = "4";
                document.getElementById("lstConferenceType").value = "4";
            }
            else if (document.getElementById("hdnSmartP2PNotify").value == "2") {
                if ("<%=isEditMode%>" == "0") {
                    document.getElementById("hdnconftype").value = "4";
                    document.getElementById("lstConferenceType").value = "4";
                }
                else {
                    document.getElementById("p2pConfirmBtn").click();
                    return false;
                }
            }
        }
    }

    function p2pSetConference(par) {
        document.getElementById("isP2PConfirm").value = "1";
        if (par == '0') {
            document.getElementById("hdnconftype").value = "4";
            document.getElementById("lstConferenceType").value = "4";
        }
        else if (document.getElementById("hdnSmartP2PNotify").value == "2") {
            document.getElementById("hdnconftype").value = "2";
            document.getElementById("lstConferenceType").value = "2";
        }
        else
            document.getElementById("hdnconftype").value = "";

        document.getElementById("hdnRecurToggle").value = "1"        
        document.getElementById("btnConfSubmit").click();
    }
    //ZD 100815 End
    function Final() {
    
		//ZD 100875
        if (document.getElementById("hdnSaveData") != null)
            document.getElementById("hdnSaveData").value = "1";

        //ZD 103624
        if(!formatTimeNew('confStartTime_Text','regStartTime',"<%=Session["timeFormat"]%>"))
            return false;
        if(!formatTimeNew('confEndTime_Text','regEndTime',"<%=Session["timeFormat"]%>"))
            return false;

        //ZD 102692
        if(document.getElementById("cmpValPassword") != null)
        {
            if(document.getElementById("cmpValPassword").style.display == "inline")
                return false;
        }
    
        if (fnValidation() == false) {
            return false;
        }
        if (SubmitRecurrence() == false) {
            return false;
        }

        fnVerifySmartP2P(); // ZD 100828
        
        var trRooms = document.getElementById("modalrooms");
        var trfiles = document.getElementById("fileuploadsection");


        if (trRooms) {
            trRooms.style.display = 'None';
        }

        if (trfiles) {
            trfiles.style.display = 'None';
        }

        //DataLoading(1); //ZD 101500

        if (document.getElementById("topExpDiv1") != null)
            document.getElementById("topExpDiv1").style.display = 'block';

        return true;
    }
    //FB 2634    
    function fnClear()
    {
        var args = fnClear.arguments;
        
        var txtCtrl = document.getElementById(args[0]);
        
        if(txtCtrl)
        {
            if(txtCtrl.value == "")
                txtCtrl.value = "0";
        }
    }
    
    // FB 2693
    function fnPCconf()
    {
        if (document.getElementById("chkPCConf").checked)
            document.getElementById("tblPcConf").style.display = "";
        else
            document.getElementById("tblPcConf").style.display = "none";
    
        DisplayMCUConnectRow("<%=mcuSetupDisplay %>","<%=mcuTearDisplay %>",'<%=EnableBufferZone%>'); //FB 2998
    }
    
    $(document).ready(function() {
    $("#confStartTime_Button").live('click', function() {
        if($("#confStartTime").css('opacity')==0)
            $("#confStartTime").css('opacity', 1);
    });

    $("#confEndTime_Button").live('click', function() {
    if ($("#confEndTime").css('opacity') == 0)
        $("#confEndTime").css('opacity', 1);
    });
});
//ZD 100433 start
function fnCompareSetup(sender, args) {

    var setupDur = 0; var mcupreStart = 0;
    setupDur = parseInt(document.getElementById("SetupDuration").value, 10);
    mcupreStart = parseInt(document.getElementById("txtMCUConnect").value, 10);

    if (document.getElementById("SetupDuration") != null && setupDur != 0) {
        if (setupDur < mcupreStart) {
            document.getElementById("customSetup").style.display = 'block';
            document.getElementById("customMCUPreStart").style.display = 'block';
            return args.IsValid = false;
        }
        else {
            document.getElementById("customSetup").style.display = 'none';
            document.getElementById("customMCUPreStart").style.display = 'none';
            return args.IsValid = true;
        }
    }
    else {
        document.getElementById("customSetup").style.display = 'none';
        document.getElementById("customMCUPreStart").style.display = 'none';
        return args.IsValid = true;
    }
}
//ZD 100433 End
//ZD 101755 start

function fnCompareTear(sender, args) {
    var TearDown = 0; var mcupreEnd = 0;
    if (document.getElementById("TearDownDuration") != null)
        TearDown = parseInt(document.getElementById("TearDownDuration").value, 10);
    if (document.getElementById("txtMCUDisConnect") != null)
        mcupreEnd = parseInt(document.getElementById("txtMCUDisConnect").value, 10);

    if (TearDown != 0) {
        if (TearDown < mcupreEnd && TearDown != mcupreEnd) {
            document.getElementById("customTearDown").style.display = 'block';
            document.getElementById("customMCUPreEnd").style.display = 'block';//ZD 102392
            return args.IsValid = false;
        }
        else {
            document.getElementById("customTearDown").style.display = 'none';
            document.getElementById("customMCUPreEnd").style.display = 'none';//ZD 102392
            return args.IsValid = true;
        }
    }
    else {
        document.getElementById("customTearDown").style.display = 'none';
        document.getElementById("customMCUPreEnd").style.display = 'none';//ZD 102392
        return args.IsValid = true;
    }

}
//ZD 101755 End

function ValidateInput() {return true;}//ZD 101126

</script>


<%--Merging Recurrence script End here--%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf8" />
    <title>Express Conference</title>
    <%--Merging  Recurrence start--%>
    <link rel="StyleSheet" href="css/divtable.css" type="text/css" />
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/border-table.css" />
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/main-table.css" />
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="App_Themes/CSS/main.css" />
    <%-- FB 1830 - Translation Menu--%>
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="App_Themes/CSS/buttons.css" />

    <link rel="stylesheet" type="text/css" media="all" href="css/calendar-blue.css" />
    <%--FB 1947--%>
     <%--ZD 100570 inncrewin 30/12/2013--%>
    <style type="text/css">
        img{ border:0;} 
        .opcty
        { 
            -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";filter: alpha(opacity=0);-moz-opacity: 0;-khtml-opacity: 0;opacity:0;
        }
        #chkRecurrence, #chkPublic, #ChkEnableNumericID
        {
            margin-left:0 !important;
            padding-left:0 !important;
        }
        .dnTableheadingRad label
        {
            font-weight: bold;
            font-size: 10pt;
            color: #556AAF;
            font-family: Arial;
            text-decoration: none;
        }
        .dnTableheadingChk label
        {
            font-size: 11px;
            vertical-align: middle;
            font-weight: bold;
            color: #556AAF;
        }
    </style>
</head>
<body>
    <div id="topExpDiv" runat="server" style="width:1600px; height:1400px; z-index: 10000px; position:absolute; left:0px; top:0px; background-color:White; display:block;"></div>
    <form id="frmSettings2" runat="server" method="post" enctype="multipart/form-data" autocomplete="off" onsubmit="return ValidateInput()"  action="Expressconference.aspx"> <%--ZD 101077 101226--%>
    <asp:ScriptManager ID="ConfScriptManager" runat="server" AsyncPostBackTimeout="600">
        <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		</Scripts>
    </asp:ScriptManager>
    
	<div id="dataLoadingDIV" name="dataLoadingDIV" align="center" style="display:none">
                        <img border="0" src="image/wait1.gif" alt="<asp:Literal Text='<%$ Resources:WebResources, Loading%>' runat='server'></asp:Literal>" />
                     </div>
     <%--START ZD 100568 27-12-2013 Inncrewin--%> 
     <div id="modalDivPopup" style="display:none;  position:fixed; z-index:1000; left:0px; top:0px; width:100%; height:1000px; background-color:Gray; opacity:0.5;"></div>
                                <div id="modalDivContent" style="display:none; left:5%; position:fixed; top:30%; padding-bottom:0.5%; padding-top:0.5%; z-index:10000; background-color:White; width:90%;">
                                <table border="0" width="98%" cellpadding="5">
                                <tr>
                                <td align="left"><asp:Literal ID="Literal127" Text="<%$ Resources:WebResources, ExpressConference_ConferenceTime%>" runat="server"></asp:Literal></td>
                                <td bgcolor="#65FF65"></td>
                                <td align="left"><asp:Literal ID="Literal128" Text="<%$ Resources:WebResources, ExpressConference_TotallyFree%>" runat="server"></asp:Literal></td>
                                <td bgcolor="#F8F075"></td>
                                <td align="left"><asp:Literal ID="Literal129" Text="<%$ Resources:WebResources, ExpressConference_PartiallyFree%>" runat="server"></asp:Literal></td>
                                </tr>
                                <tr>
                                <td colspan="5">
                                <table id="tblSeatsAvailability" cellpadding="4" cellspacing="0" runat="server" height="50%" width="100%" border="1" ></table>
                                </td>
                                </tr>
                                </table>
                                <input type="button" align="middle" id="btnClose" runat="server" onclick="javascript:fnPopupSeatsClose(); return false;" value="<%$ Resources:WebResources, Close%>" class="altMedium0BlueButtonFormat" /> <%--FB 2997--%>
                                <br />
                                </div>
     <div id="modalDivContentforConf" style="display: none; left: 13%; position: fixed;border:2px solid #a1a1a1;border-radius:25px;
            top: 30%; padding-bottom: 0.5%; padding-top: 0.5%; z-index: 10000; background-color: White;
            width: 70%;height:150px;">
            <div>
                <p style=" font-size: 12pt;left:10px; position:relative;">
                <asp:Literal ID="Literal130" Text="<%$ Resources:WebResources, ExpressConference_seatlimit%>" runat="server"></asp:Literal></p>
            </div>
            <div style="height: 30%;">
            </div>
            <div>
                <table style="width: 100%;">
                    <tr>
                       <td align="right"> <%--ZD 100875--%>
                            <asp:Button ID="Button1" runat="server" style="width: auto;" CssClass="altMedium0BlueButtonFormat" OnClientClick="javascript:document.getElementById('hdnSaveData').value = '1';fnPopupConfAlertClose();"
                            OnClick= "CheckSeatsAvailability" Text="<%$ Resources:WebResources, ExpressConference_CheckAvailableSeat%>" causesvalidation="false" /> 
                             <input style=" width: auto;" type="button" onclick="javascript:fnPopupConfAlertClose(); return false;" runat="server"
                                value="<%$ Resources:WebResources, Cancel%>" class="altMedium0BlueButtonFormat">                       
                        </td>
                    </tr>
                </table>
            </div>
            <br />
        </div>
        <%--END ZD 100568 27-12-2013 Inncrewin--%>    
    <input type="hidden" runat="server" id="hdnSaveData"  /><%-- ZD 100875--%>
    <input type="hidden" id="hdnSetStartNow" runat="server" /><%--FB 1825--%>
    <input type="hidden" name="hdnconftype" id="hdnconftype" runat="server" />
    <asp:TextBox ID="txtModifyType" Text="" Visible="false" runat="server"></asp:TextBox>
    <asp:TextBox ID="txtTimeCheck" Text="" Visible="false" runat="server"></asp:TextBox>
    <input type="hidden" runat="server" id="CreateBy" value="2" />
    <input type="hidden" name="ModifyType" value="0" />
    <input type="hidden" name="Recur" id="Recur" runat="server" />
    <input id="confPassword" runat="server" type="hidden" />
    <input runat="server" id="RecurFlag" type="hidden" />
    <input runat="server" id="selectedloc" type="hidden" />
    <input id="RecurringText" runat="server" type="hidden" />
    <input type="hidden" name="ConfID" value="" />
    <input type="hidden" name="NeedInitial" value="2" />
    <input type="hidden" name="outlookEmails" value="" />
    <input type="hidden" name="lotusEmails" value="" />
    <input type="hidden" name="hdnSetupTime" id="hdnSetupTime" runat="server" />
    <input type="hidden" name="hdnTeardownTime" id="hdnTeardownTime" runat="server" />
    <input type="hidden" name="hdnBufferStr" id="hdnBufferStr" runat="server" />
    <input type="hidden" runat="server" id="txtHasCalendar" />
    <input type="hidden" id="hdnAudioInsIDs" name="hdnAudioInsIDs" runat="server" />
    <input type="hidden" name="hdnHostIDs" id="hdnHostIDs" runat="server" />
    <input type="hidden" id="hdnAudioInsID" name="hdnAudioInsID" runat="server" />
    <%--Merging Recurrence - Start--%>
    <input type="hidden" id="hdnOldTimezone" name="hdnOldTimezone" runat="server" value=""/> <%--FB 2884--%>
    <input type="hidden" id="TimeZoneText" name="TimeZoneText" value="" />
    <input type="hidden" id="TimeZoneValues" name="TimeZoneValues" value="" />
    <input type="hidden" id="EndDateText" name="EndDateText" value="" />
    <input type="hidden" id="RecurValue" name="RecurValue" value="" />
    <input type="hidden" id="RecurPattern" name="RecurPattern" value="" />
    <input type="hidden" id="CustomDates" name="CutomDates" value="" />
    <input type="hidden" id="RecPattern" name="RecPattern" value="" />
    <input type="hidden" id="HdnDateFormat" name="HdnDateFormat" value="<%=format %>" />
    <input type="hidden" id="hdnValue" runat="server" />
    <input type="hidden" id="hdnRecurValue" runat="server" />
    <%--Merging Recurrence - End--%>
    <input runat="server" name="hdnemailalert" id="hdnemailalert" type="hidden" />
    <%--FB 1830 Email Edit--%>
    <%--FB 1716--%>
    <input runat="server" id="hdnChange" type="hidden" />
    <input runat="server" id="hdnDuration" type="hidden" />
    <%--FB 1911--%>
    <input runat="server" id="hdnSpecRec" type="hidden" />
    <input type="hidden" name="Recur" id="RecurSpec" runat="server" />
    <input type="hidden" runat="server" id="guardSet" value="" />
    <input name="locstrname" type="hidden" id="locstrname" runat="server" /><%--FB 2367--%>    
    <input type="hidden" id="isVMR" runat="server" value=""  /> <%--ZD 100834--%>
	<input type="hidden" runat="server" id="hdnCusID" /><%--FB 2592--%>
	<input type="hidden" runat="server" id="hdnStartTime" name="hdnStartTime" /><%--FB 2588--%>
    <input type="hidden" runat="server" id="hdnEndTime" name="hdnEndTime"  /><%--FB 2588--%>
    <input type="hidden" runat="server" id="hdnMcuProfileSelected" /><%--FB 2839--%>
    <input type="hidden" runat="server" id="hdnMcuPoolOrder" /><%--ZD 104256--%>
    <input type="hidden" id="hdnPasschange" value="false" runat="server"/> <%--ZD 100221--%>
    <input type="hidden" id="hdnPW1Visit" value="false" runat="server"/> <%--ZD 100221--%>
    <input type="hidden" id="hdnPW2Visit" value="false" runat="server"/> <%--ZD 100221--%>	 
    <input type="hidden" id="hdnWebExPwd" value="" runat="server"/> <%--ZD PRABU--%> 	
	<input type="hidden" runat="server" id="hdnConferenceName" name="hdnConferenceName" /><%-- ZD 100704--%> 
    <input type="hidden" runat="server" id="hdnHDConfName" name="hdnHDConfName" /><%--ALLDEV-807--%> 
	<input type="hidden" id="hdnEnableWebExIngt" value="" runat="server" /> <%--ZD 100935--%>
    <input type="hidden" id="isPCCOnf" runat="server" value=""  /> <%--ZD 100834--%>
	<input type="hidden" name="hdnParty" id="hdnParty" runat="server" /> <%--ZD 100834--%>
    <input type="hidden" runat="server" id="hdnicalID" name="hdnicalID" /><%--ZD 101229 exchange round trip--%>
    <input type="hidden" runat="server" id="hdnconfOriginID" name="hdnconfOriginID" /><%--ZD 101229 exchange round trip--%>
    <input type="hidden" runat="server" id="hdnGLWarningPopUp" name="hdnGLWarningPopUp" /> <%--ZD 101120--%>
    <input type="hidden" runat="server" id="hdnGLApprTime" name="hdnGLApprTime" /> <%--ZD 101120--%> 
    <input type="hidden" runat="server" id="hdnGLConfTime" name="hdnGLConfTime" /> <%--ZD 101120--%> 
    <input type="hidden" runat="server" id="hdnGLCount" name="hdnGLCount" /> <%--ZD 101120--%> 
    <input type="hidden" runat="server" id="hdnROOMPIN" value="0" /> <%--ZD 100522--%>
    <input type="hidden" runat="server" id="hdnPINlen" name="hdnPINlen" /> <%--ZD 100522--%> 

    <%--ZD 101233 START--%>
    <input type="hidden" name="hdnCrossrecurEnable" id="hdnCrossrecurEnable" runat="server" />
    <input type="hidden" name="hdnCrossdynInvite" id="hdnCrossdynInvite" runat="server" />
    <input type="hidden" name="hdnCrossroomModule" id="hdnCrossroomModule" runat="server" />
    <input type="hidden" name="hdnCrossfoodModule" id="hdnCrossfoodModule" runat="server" />
    <input type="hidden" name="hdnCrosshkModule" id="hdnCrosshkModule" runat="server" />
    <input type="hidden" name="hdnCrossisVIP" id="hdnCrossisVIP" runat="server" />
    <input type="hidden" name="hdnCrossEnableRoomServiceType" id="hdnCrossEnableRoomServiceType" runat="server" />                   
    <input type="hidden" name="hdnCrossisSpecialRecur" id="hdnCrossisSpecialRecur" runat="server" />
    <input type="hidden" name="hdnCrossConferenceCode" id="hdnCrossConferenceCode" runat="server" />
    <input type="hidden" name="hdnCrossLeaderPin" id="hdnCrossLeaderPin" runat="server" />
    <input type="hidden" name="hdnCrossPartyCode" id="hdnCrossPartyCode" runat="server" /> <%--ZD 101446--%>
    <input type="hidden" name="hdnCrossAdvAvParams" id="hdnCrossAdvAvParams" runat="server" />
    <input type="hidden" name="hdnCrossEnableBufferZone" id="hdnCrossEnableBufferZone" runat="server" />
    <input type="hidden" name="hdnCrossEnableEntity" id="hdnCrossEnableEntity" runat="server" />
    <input type="hidden" name="hdnCrossAudioParams" id="hdnCrossAudioParams" runat="server" />
    <input type="hidden" name="hdnCrossdefaultPublic" id="hdnCrossdefaultPublic" runat="server" />
    <input type="hidden" name="hdnCrossP2PEnable" id="hdnCrossP2PEnable" runat="server" />
    <input type="hidden" name="hdnCrossEnableRoomConfType" id="hdnCrossEnableRoomConfType" runat="server" />
    <input type="hidden" name="hdnCrossEnableHotdeskingConfType" id="hdnCrossEnableHotdeskingConfType" runat="server" />
    <input type="hidden" name="hdnCrossEnableAudioVideoConfType" id="hdnCrossEnableAudioVideoConfType" runat="server" />
    <input type="hidden" name="hdnCrossDefaultConferenceType" id="hdnCrossDefaultConferenceType" runat="server" />
    <input type="hidden" name="hdnCrossEnableAudioOnlyConfType" id="hdnCrossEnableAudioOnlyConfType" runat="server" />
    <input type="hidden" name="hdnCrossenableAV" id="hdnCrossenableAV" runat="server" />
    <input type="hidden" name="hdnCrossenableParticipants" id="hdnCrossenableParticipants" runat="server" />
    <input type="hidden" name="hdnCrossisMultiLingual" id="hdnCrossisMultiLingual" runat="server" />
    <input type="hidden" name="hdnCrossroomExpandLevel" id="hdnCrossroomExpandLevel" runat="server" />
    <input type="hidden" name="hdnCrossEnableImmConf" id="hdnCrossEnableImmConf" runat="server" />
    <input type="hidden" name="hdnCrossEnableAudioBridges" id="hdnCrossEnableAudioBridges" runat="server" />
    <input type="hidden" name="hdnCrossEnablePublicConf" id="hdnCrossEnablePublicConf" runat="server" />
    <input type="hidden" name="hdnCrossEnableConfPassword" id="hdnCrossEnableConfPassword" runat="server" />
    <input type="hidden" name="hdnCrossEnableRoomParam" id="hdnCrossEnableRoomParam" runat="server" />
    <input type="hidden" name="hdnCrossAddtoGroup" id="hdnCrossAddtoGroup" runat="server" />
	<input type="hidden" name="hdnCrossEnablePC" id="hdnCrossEnablePC" runat="server" />
	<input type="hidden" name="hdnCrossEnableSurvey" id="hdnCrossEnableSurvey" runat="server" />
    <input type="hidden" name="hdnCrossSetupTime" id="hdnCrossSetupTime" runat="server" />
	<input type="hidden" name="hdnCrossTearDownTime" id="hdnCrossTearDownTime" runat="server" />
	<input type="hidden" name="hdnCrossMeetGreetBufferTime" id="hdnCrossMeetGreetBufferTime" runat="server" />
    <input type="hidden" runat="server" id="hdnCrossEnableLinerate" name="hdnCrossEnableLinerate" />
	<input type="hidden" runat="server" id="hdnCrossEnableStartMode" name="hdnCrossEnableStartMode" />
    <input type="hidden" runat="server" id="hdnCrossEnableSmartP2P" />
    <input type="hidden" runat="server" id="hdnTxtMsg" />
    <input type="hidden" name="hdnEnableNumericID" id="hdnEnableNumericID" runat="server" />
    <input type="hidden" name="hdnEnableProfileSelection" id="hdnEnableProfileSelection" runat="server" />
    <input type="hidden" name="hdnEnablePoolOrderSelection" id="hdnEnablePoolOrderSelection" runat="server" /> <%--ZD 104256--%>
    <input type="hidden" id="hdnMCUSetupTime" runat="server" />
    <input type="hidden" id="hdnMCUTeardownTime" runat="server" />
    <input type="hidden" id="hdnMCUConnectDisplay" runat="server" />
    <input type="hidden" id="hdnEnableFECC" runat="server" /><%--ZD 101931--%>
    <input type="hidden" id="hdnMCUDisconnectDisplay" runat="server" />
    <input type="hidden" name="hdnNetworkSwitching" id="hdnNetworkSwitching" runat="server" />
    <input type="hidden" name="hdnWebExCOnf" id="hdnWebExCOnf" runat="server" />
    <input type="hidden" runat="server" id="hdnVMRPINChange" name="hdnVMRPINChange" />
    <%--ZD 101233 END--%>
	<input type="hidden" runat="server" id="hdnRecurToggle" value="0" /> <%--ZD 101815--%>
    <input type="hidden" runat="server" id="isIPAddress" name="isIPAddress" /> <%--ZD 101815--%> 
    <input type="hidden" runat="server" id="isTestedModel" name="isTestedModel" /> <%--ZD 101815--%> 
    <input type="hidden" runat="server" id="hdnSmartP2PTotalEps" /><%--FB 2430--%>
    <input type="hidden" runat="server" id="hdnSmartP2PNotify" name="SmartP2PNotify" /> <%--ZD 101815--%> 
    <input type="hidden" runat="server" id="isP2PGL" name="isP2PGL" /> <%--ZD 101815--%> 
    <input type="hidden" runat="server" id="isP2PConfirm" value="0" name="isP2PConfirm"/><%--ZD 101815--%>
    <input type="hidden" runat="server" id="hdnisEditMode" value="0" name="isEditMode"/><%--ZD 101815--%> 
    <input type="hidden" runat="server" id="hnDetailedExpForm" value="0" name="hnDetailedExpForm"/><%--ZD 101345--%> 
	<input type="hidden" runat="server" id="hdnEnablEOBTP" name="hdnEnablEOBTP" value="0" /> <%--ZD 100513--%> 
    <input type="hidden" runat="server" id="hdnWebExLaunch" name="hdnWebExLaunch" value="0" /> <%--ZD 100513--%> 
	<input type="hidden" runat="server" id="hdnEnableExpressConfType" value="0" name="hdnEnableExpressConfType"/><%--ZD 101490--%>
    <input type="hidden" runat="server" id="hdntemplatebooking" value="0" name="hdntemplatebooking" /> <%--ZD 101562--%> 
    <input type="hidden" runat="server" id="hdnSetuptimeDisplay" value="0" name="hdnSetuptimeDisplay" /> <%--ZD 101755 --%>
    <input type="hidden" runat="server" id="hdnTeardowntimeDisplay" value="0" name="hdnTeardowntimeDisplay" /> <%--ZD 101755 --%>
    <input type="hidden" runat="server" id="hdndetailexpform" value="0" name="hdndetailexpform" />     
    <input type="hidden" runat="server" id="hdnShowVideoLayout" value="0" name="hdnShowVideoLayout" /> <%--ZD 101931--%>    
	<input type="hidden" runat="server" id="hdnIsRecurConference" value="0" name="hdnIsRecurConference" /> <%--ZD 102963--%>
    <input type="hidden" runat="server" id="hdnSelEntityCodeVal" name="hdnSelEntityCodeVal" /> <%--ZD 102909--%>
	<input type="hidden" runat="server" id="hdnRemoveEntityVal" name="hdnRemoveEntityVal" /> <%--ZD 103265--%>
	<input type="hidden" runat="server" id="hdnChgCallerCallee" name="hdnChgCallerCallee" value="0" /> <%--ZD 103402--%>    
    <input type="hidden" runat="server" id="hdnisBJNConf" value="0" name="hdnisBJNConf" /><%--ZD 103263--%>
    <input type="hidden" name="hdnCrossBJNSelectOption" id="hdnCrossBJNSelectOption" runat="server" /><%--ZD 103550--%>    
    <input type="hidden" name="hdnCrossBJNIntegration" id="hdnCrossBJNIntegration" runat="server" />
    <input type="hidden" name="hdnCrossEnableBlueJeans" id="hdnCrossEnableBlueJeans" runat="server" />
	<input type="hidden" name="hdnRecurType" id="hdnRecurType" runat="server" /> <%--ZD 103929--%>
    <input type="hidden" name="hdnCrossBJNDisplay" id="hdnCrossBJNDisplay" runat="server" /><%--ZD 104116--%>
    <input type="hidden" name="hdnChangeBJNVal" id="hdnChangeBJNVal" runat="server" /><%--ZD 104116--%>
    <input type ="hidden" id="hdnDialString" runat="server" name ="hdnDialString" /> <%--ZD 104289--%>            
	<input type="hidden"  id="hasVisited" runat="server" /> <%--ALLDEV-814--%>
    <input type="hidden"  id="hdnSelectedAudioDetails" runat="server" /> <%--ALLDEV-814--%>
    <%--ALLDEV-826 Starts--%>
    <input type="hidden" id="hdnHostPassword" runat="server"/>
    <input type="hidden" name="hdnCrossEnableHostGuestPwd" id="hdnCrossEnableHostGuestPwd" runat="server" />
    <%--ALLDEV-826 Ends--%>	
    <input type="hidden" name="hdnIsHDBusy" id="hdnIsHDBusy" runat="server" /> <%--ALLDEV-807--%>

    <table width="100%" cellpadding="2" cellspacing="0">
        <tr align="center" style="display: none;">
            <td>
                <h3>
                    <asp:Label ID="lblConfHeader" runat="server" Text="<%$ Resources:WebResources, ExpressConference_lblConfHeader%>"></asp:Label>&nbsp;
                    <asp:Literal ID="Literal125" Text="<%$ Resources:WebResources, ExpressConference_a%>" runat="server"></asp:Literal>
                </h3>
            </td>
        </tr>
        <tr>
            <td align="center">
                <table width="80%" border="0">
                    <tr>
                        <td align="center">
                        <h3><asp:Label ID="lblExpHead" runat="server" Text=""></asp:Label></h3><br /><!-- FB 2570 -->
                            <asp:Label ID="errLabel" runat="server" CssClass="lblError" Visible="false"></asp:Label> 
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr style="display: none;">
            <td class="reqfldstarText" align="right">
                * <asp:Literal ID="Literal126" Text="<%$ Resources:WebResources, ExpressConference_RequiredField%>" runat="server"></asp:Literal>
            </td>
        </tr>
        <tr style="display: none;">
            <td>
                <img src="image/DoubleLine.jpg" alt="DoubleLine" height="6px" width="100%" /> <%--ZD 100419--%>
            </td>
        </tr>
        <tr style="height: 38" valign="middle" class="Bodybackgorund">
            <td>
                <span style="width: 30;" class="ExpressHeadingsNumbers">&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;</span><span
                    class="ExpressHeadings">&nbsp;&nbsp;&nbsp;
                    <asp:Literal ID="Literal2" Text="<%$ Resources:WebResources, ExpressConference_BasicInformati%>" runat="server"></asp:Literal>
                </span>
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%" border="0">
                    <tr>
                        <td style="width: 50%;" valign="top">
                            <table width="100%" cellpadding="2" border="0" cellspacing="0">
                             <%-- ZD 100570 Inncrewin 23-12-2013 Starts--%>
                                 <tr id="trConfTemp" runat="server" > 
                                                         <td class="blackblodtext" align="left" nowrap="nowrap">
                                                             <asp:Literal ID="Literal124" Text="<%$ Resources:WebResources, ExpressConference_CreateFromTemplate%>" runat="server"></asp:Literal> 
                                                            </td>
                                                         <td style="height: 20px" valign="top">
                                                            <asp:DropDownList ID="lstTemplates" CssClass="altText" runat="server" onclick="javascript:document.getElementById('hdnSaveData').value = '1';" DataTextField="name" DataValueField="ID"  AutoPostBack="true" OnSelectedIndexChanged="UpdateTemplates"  Width="80%"> <%--ZD 100522--%>
                                                            </asp:DropDownList>
                                                         </td>                     
                                                     </tr>
                               <%-- ZD 100570 Inncrewin 23-12-2013 End--%>
                               <%--ZD 100704--%>
                                <tr id="trConfTitle" runat="server" >
                                    <td class="blackblodtext" align="left" style="width: 40%">
                                        <asp:Label ID="lblConfID" runat="server" Visible="False" Width="20%"></asp:Label>
                                        <span id="Field2"><asp:Literal ID="Literal1" Text="<%$ Resources:WebResources, ExpressConference_Field2%>" runat="server"></asp:Literal>
                                        </span><span class="reqfldstarText">*&nbsp;</span>
                                    </td>
                                    <td style="height: 20px" valign="top">
                                     	<%--FB 2997 Starts--%>
                                    	<%-- <asp:TextBox ID="ConferenceName" onblur="javascript:clearError(this,'lblConferenceNameError');javascript:this.className='altText'"
                                            runat="server" CssClass="altText" Width="60%"  onfocus="javascript:this.className='altText3'"></asp:TextBox> --%>

                                     	<asp:TextBox ID="ConferenceName" runat="server" CssClass="altText" Width="60%"></asp:TextBox>

                                         <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ConferenceName" ErrorMessage="<%$ Resources:WebResources, ExpressConference_RequiredField%>" Font-Bold="True" Display="Dynamic"  ></asp:RequiredFieldValidator>
                                        
                                        <%--FB 2997 End--%>
                                        
                                        <asp:RegularExpressionValidator ID="regConfName" ControlToValidate="ConferenceName"
                                            Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters12%>"
                                            ValidationExpression="^[^<>&]*$" ValidationGroup="Submit"></asp:RegularExpressionValidator><%--@@@@@ --%><%--FB 2321--%>
                                        <asp:RegularExpressionValidator runat="server" ID="valInput" ControlToValidate="ConferenceName"  ValidationExpression="^[\s\S]{0,256}$"   ErrorMessage="<%$ Resources:WebResources, MaxLimit2%>"
                                         Display="Dynamic"></asp:RegularExpressionValidator><%--FB 2508--%>
                                        <label id="lblConferenceNameError" runat="server" style="font-weight: normal; color: Red"> <%--FB 2592--%>
                                        </label>
                                    </td>
                                </tr>
                                <%--ZD 100704--%>
								<%--ZD 100834 Starts--%>
                                <tr id="trchkDetails" runat="server">
                                    <td class="blackblodtext" align="left">
                                        <span class="reqfldstarText"></span>
                                        <asp:Literal ID="Literal3" Text="<%$ Resources:WebResources, ExpressConference_Details%>" runat="server"></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:CheckBox onClick="openAdvancesetting()" runat="server" ID="chkDetails" style="margin-left:-4px;" />
                                    </td>
                                </tr>
								<%--ZD 100834 End--%>
                                <tr id="trConfType" runat="server">
                                    <td class="blackblodtext" align="left" id="Field3" width="35%" runat="server">
                                    <asp:Literal ID="Literal4" Text="<%$ Resources:WebResources, ExpressConference_ConfType%>" runat="server"></asp:Literal></td>
                                    <td style="height: 24px">
                                        <asp:DropDownList ID="lstConferenceType" CssClass="altSelectFormat" runat="server" onchange="javascript:fnChangeConferenceType();addExternalParty();showHideAdvSettings();" > <%--ZD 100604--%>
                                            <asp:ListItem Value="6" Text="<%$ Resources:WebResources, AudioOnly%>"></asp:ListItem>
                                            <asp:ListItem Selected="True" Value="2" Text="<%$ Resources:WebResources, AudioVideo%>"></asp:ListItem>
                                            <asp:ListItem Value="4" Text="<%$ Resources:WebResources, ManageConference_pttopt%>"></asp:ListItem>
                                            <asp:ListItem Value="7" Text="<%$ Resources:WebResources, RoomConference%>"></asp:ListItem>
                                            <asp:ListItem Value="8" Text="<%$ Resources:WebResources, Hotdesking%>"></asp:ListItem>
                                            <asp:ListItem Value="9" Text="<%$ Resources:WebResources, OBTP%>"></asp:ListItem><%--ZD 100513--%>
                                        </asp:DropDownList>
                                    </td>
                                </tr>                    
                                <tr>
                                    <td colspan="2" style="color: Red">
                                        <asp:Label runat="server" ID="lblAudioNote" Visible="false"></asp:Label>
                                    </td>
                                </tr>
                                <%--FB 2634--%>
                                <tr id="SetupRow" runat="server">
                                    <td class="blackblodtext" align="left" id="SDateText" nowrap="nowrap">
                                         <asp:Literal ID="Literal5" Text="<%$ Resources:WebResources, ExpressConference_Setupmin%>" runat="server"></asp:Literal><%--ZD 100528--%>
                                    </td>
                                    <td valign="top" style="text-align: left; height: 27px; color: black; font-family: arial;
                                        width: 403px;" align="center" colspan="2"><%--FB 2592--%>
                                         <asp:TextBox ID="SetupDuration" runat="server" CssClass="altText" Width="70px" AutoPostBack="false" onblur="javascript:fnClear('SetupDuration')"></asp:TextBox>
                                         <asp:ImageButton ID="imgSetup" AlternateText="<%$ Resources:WebResources, ExpressConference_Info%>" src="image/info.png" runat="server" style="cursor:default" OnClientClick="javascript:return false;"/><%--FB 2998--%> <%--ZD 100419--%><%-- ZD 102590 --%>
                                        <asp:RegularExpressionValidator ID="regSetupDuration" runat="server" ControlToValidate="SetupDuration"
                                            Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, InvalidDuration%>" ValidationExpression="\d+"></asp:RegularExpressionValidator>
                                            <asp:CustomValidator id="customSetup" Enabled="true"  ErrorMessage="<%$ Resources:WebResources, SetuptimeMsg%>" Display="Dynamic"
                                            ControlToValidate="SetupDuration"  runat="server"  ClientValidationFunction="fnCompareSetup"></asp:CustomValidator><%-- ZD 100433--%>
                                            
                                    </td>
                                </tr>
                                <tr id="ConfStartRow">
                                    <td class="blackblodtext" align="left">
                                        <asp:Literal ID="Literal6" Text="<%$ Resources:WebResources, ExpressConference_ConferenceStar%>" runat="server"></asp:Literal><span class="reqfldstarText">*&nbsp;</span>
                                    </td>
                                    <td valign="top" style="text-align: left; height: 27px; color: black; font-family: arial;
                                        width: 403px;" align="center" colspan="2">
                                        <span id="StartDateArea"><%--FB 2592--%>
                                            <asp:TextBox ID="confStartDate" runat="server" CssClass="altText" Width="70px" onblur="javascript:ChangeEndDate()"
                                             AutoPostBack="false"></asp:TextBox>
                                            <a href="#" onkeydown="if(event.keyCode == 13){if(!isIE){document.getElementById('cal_triggerd').click();return false;}}" onclick="if(isIE){this.childNodes[0].click();return false;}"><img src="image/calendar.gif" alt="<asp:Literal Text='<%$ Resources:WebResources, DateSelector%>' runat='server'></asp:Literal>" border="0" width="20" height="20" id="cal_triggerd" style="cursor: pointer;vertical-align: middle;" title="<asp:Literal Text='<%$ Resources:WebResources, DateSelector%>' runat='server'></asp:Literal>" 
                                            onclick="return showCalendar('<%=confStartDate.ClientID %>', 'cal_triggerd', 1, '<%=format%>');return false;" /></a><%--ZD 100420--%><%--ZD 100419--%><%--ZD 100420--%>
                                            <span class="blackblodtext">@</span> </span>
                                        <mbcbb:ComboBox ID="confStartTime" runat="server" CssClass="altText" Rows="10" CausesValidation="True"
                                            onblur="javascript:formatTime('confStartTime_Text');return ChangeEndDate();" Style="width: 70px; -ms-filter: 'progid:DXImageTransform.Microsoft.Alpha(Opacity=0)'; filter: alpha(opacity=0); -moz-opacity: 0; -khtml-opacity: 0; opacity:0;" AutoPostBack="false"><%--ZD 100381--%>
                                        </mbcbb:ComboBox>
                                        
                                        <%--ZD 101720 START--%>
                                        &nbsp;&nbsp;
                                        <asp:ImageButton ID="imgTDBConferencenote" src="image/info.png" AlternateText="Info" runat="server" style="cursor:default"  OnClientClick="javascript:return false;"/><%-- ZD 102590 --%>
                                        <%--ZD 101720 END--%>
                                        
                                        <asp:RequiredFieldValidator ID="reqStartTime" runat="server" ControlToValidate="confStartTime"
                                            Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, Required_Time%>"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="regStartTime" runat="server" ControlToValidate="confStartTime"
                                            Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, InvalidTime%>" ValidationExpression="[0|1][0-9]:[0-5][0-9] ?[a|A|p|P][M|m]"></asp:RegularExpressionValidator><%--FB 1715--%>
                                        <asp:RequiredFieldValidator ID="reqStartData" runat="server" ControlToValidate="confStartDate"
                                            Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, Required_Date%>"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="regStartDate" runat="server" ControlToValidate="confStartDate"
                                            Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, InvalidDate%>" ValidationExpression="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d|(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d"></asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <tr id="ConfEndRow">
                                    <td class="blackblodtext" align="left" id="EDateText">
                                        <asp:Literal ID="Literal7" Text="<%$ Resources:WebResources, ExpressConference_EDateText%>" runat="server"></asp:Literal><span class="reqfldstarText">*</span>
                                    </td>
                                    <td valign="top" style="text-align: left; height: 27px; color: black; font-family: arial;
                                        width: 403px;" align="center" colspan="2">
                                        <span id="EndDateArea"><%--FB 2592--%>
                                        <asp:TextBox ID="confEndDate" runat="server" CssClass="altText" Width="70px" onblur="javascript:fnEndDateValidation()"  AutoPostBack="false"></asp:TextBox>
                                        <a href="" onkeydown="if(event.keyCode == 13){if(!isIE){document.getElementById('cal_trigger1').click();return false;}}" onclick="if(isIE){this.childNodes[0].click();return false;}"><img src="image/calendar.gif" border="0" width="20" height="20" id="cal_trigger1"
                                            style="cursor: pointer; vertical-align: middle;" title="<asp:Literal Text='<%$ Resources:WebResources, DateSelector%>' runat='server'></asp:Literal>" onclick="return showCalendar('<%=confEndDate.ClientID %>', 'cal_trigger1', 1, '<%=format%>');" /></a><%--ZD 100420--%><%--ZD 100420--%>
                                        <span class="blackblodtext">@ </span>
                                        </span>
                                        <mbcbb:ComboBox ID="confEndTime" runat="server" CssClass="altSelectFormat" Rows="10"
                                            onblur="javascript:formatTime('confEndTime_Text');" Style="width: 70px; -ms-filter: 'progid:DXImageTransform.Microsoft.Alpha(Opacity=0)'; filter: alpha(opacity=0); -moz-opacity: 0; -khtml-opacity: 0; opacity:0;" CausesValidation="True" AutoPostBack="false"><%--ZD 100381--%>
                                        </mbcbb:ComboBox>
                                        <asp:RequiredFieldValidator ID="reqEndTime" runat="server" ControlToValidate="confEndTime"
                                            Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, Required_Time%>"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="regEndTime" runat="server" ControlToValidate="confEndTime"
                                            Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, InvalidTime%>" ValidationExpression="[0-1][0-9]:[0-5][0-9] ?[A|a|P|p][M|m]"></asp:RegularExpressionValidator><%--FB 1715--%>
                                        <asp:RequiredFieldValidator ID="reqEndDate" runat="server" ControlToValidate="confEndDate"
                                            Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, Required_Date%>"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="regEndDate" runat="server" ControlToValidate="confEndDate"
                                            Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, InvalidDate%>" ValidationExpression="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d|(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d"></asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <tr id="TearDownRow" runat="server"> <%--ZD 101755 start--%>
                                    <td class="blackblodtext" align="left" id="Td4" nowrap="nowrap">
                                       <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_Td1%>" runat="server"></asp:Literal>
                                    </td>
                                    <td>
                                        <span id="TearDownArea">
                                            <asp:TextBox ID="TearDownDuration" runat="server" CssClass="altText" Width="70px" AutoPostBack="false" onblur="javascript:fnClear('TearDownDuration')"></asp:TextBox>
                                            <asp:ImageButton ID="imgTear" src="image/info.png" AlternateText="Info" runat="server" style="cursor:default"  OnClientClick="javascript:return false;"/><%-- ZD 102590 --%>
                                            <asp:RegularExpressionValidator ID="regTearDown" runat="server" ControlToValidate="TearDownDuration"
                                            Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, InvalidDuration%>" ValidationExpression="\d+"></asp:RegularExpressionValidator>
                                            <asp:CustomValidator id="customTearDown" Enabled="true"  ErrorMessage=" <%$ Resources:WebResources, TeardowntimetoMCUDisconnectTime%> " Display="Dynamic"
                                                ControlToValidate="TearDownDuration"  runat="server"  ClientValidationFunction="fnCompareTear"></asp:CustomValidator>
                                        </span>
                                    </td>
                                </tr><%--ZD 101755 End--%>
                                <%--FB 2659 Starts--%>
                                <tr id="trseatavailable" runat="server" >
                                    <td class="blackblodtext" align="left">
                                    <asp:Literal ID="Literal8" Text="<%$ Resources:WebResources, ExpressConference_SeatsAvailable%>" runat="server"></asp:Literal></td>
                                    <td style="height: 24px">   <%--ZD 100875--%>
                                        <input type="button" id="btnchkSeatAvailable" value="<%$ Resources:WebResources, ExpressConference_SeatsAvailable%>" cssclass="altText" class="altMedium0BlueButtonFormat" runat="server"  style="width:150px;"
                                        onclick="javascript:document.getElementById('hdnSaveData').value = '1';" onserverclick="CheckSeatsAvailability" causesvalidation="false" />
                                    </td>
                                </tr>
                                <tr >
                                <td></td>
                                <td align="center">
                                <%--ZD 100568 Commented this code <div id="modalDivPopup" style="display:none;  position:fixed; z-index:1000; left:0px; top:0px; width:100%; height:1000px; background-color:Gray; opacity:0.5;"></div>
                                <div id="modalDivContent" style="display:none; left:5%; position:fixed; top:30%; padding-bottom:0.5%; padding-top:0.5%; z-index:10000; background-color:White; width:90%;">
                                <table border="0" width="98%" cellpadding="5">
                                <tr>
                                <td align="left">Conference Time Availability : Number of Seats</td>
                                <td bgcolor="#65FF65"></td>
                                <td align="left">Totally Free</td>
                                <td bgcolor="#F8F075"></td>
                                <td align="left">Partially Free</td>
                                </tr>
                                <tr>
                                <td colspan="5">
                                <table id="tblSeatsAvailability" cellpadding="4" cellspacing="0" runat="server" height="50%" width="100%" border="1" ></table>
                                </td>
                                </tr>
                                </table>
                                <input type="button" align="middle" id="btnClose" runat="server" onclick="javascript:fnPopupSeatsClose(); return false;" value="Close" class="altMedium0BlueButtonFormat" /> 
                                <br />
                                </div>END ZD 100568 Commented this code--%><%--FB 2997--%>
                                </td>
                                </tr>
                                <%--FB 2659 End--%>
                                <tr id="NONRecurringConferenceDiv8">
                                    <td class="blackblodtext" align="left"> <%--ZD 102167--%>
                                        <asp:Literal ID="Literal9" Text="<%$ Resources:WebResources, ConferenceSetup_Recurrence%>" runat="server"></asp:Literal><span class="reqfldstarText">&nbsp;</span>  <%--ZD 100570 inncrewin 30/12/2013--%>
                                    </td>
                                    <%--FB 1911 Start //FB 2052--%>
                                    <td>
                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td>
                                                    <asp:CheckBox onClick="openRecur()" runat="server" ID="chkRecurrence" />
                                                </td>
                                                <td class="blackblodtext" align="right" nowrap id="SPCell1">
                                                    <span class="subtitleblueblodtext">&nbsp;<asp:Literal ID="Literal11" Text="<%$ Resources:WebResources, OR%>" runat="server"></asp:Literal> </span>&nbsp; <asp:Literal ID="Literal10" Text="<%$ Resources:WebResources, ExpressConference_SPCell1%>" runat="server"></asp:Literal> &nbsp;
                                                </td>
                                                <td id="SPCell2" width="45%">
                                                    <a href="" onkeydown="if(event.keyCode == 13){document.getElementById('imgSplRec').click();return false;}" onmouseup="this.childNodes[0].click();return false;" style="cursor: hand;"> <%--ZD 100420--%>
                                                        <img id="imgSplRec" runat="server" src="image/recurring.gif" width="25" height="25" border="0" alt="<%$ Resources:WebResources, Recurrence%>" onclick="openRecur('S');return false;" /> <%--ZD 100419--%><%--ZD 100420--%>
                                                    </a>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                 <tr id="recurDIV" style="display: none;">
                                    <td align="left" valign="top"> <%--FB 2052--%>
                                        <span class="blackblodtext"><asp:Literal ID="Literal12" Text="<%$ Resources:WebResources, ConferenceSetup_SpecialRecurre%>" runat="server"></asp:Literal></span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="RecurText" Enabled="false" runat="server" CssClass="altText" Rows="4"
                                            TextMode="MultiLine" Width="70%">No recurrence</asp:TextBox>
                                    </td>
                                </tr>
                                <%--FB 1911 End--%>
                                <%--FB 2226 Start--%>
                                 <tr id="trPublic" runat="server">
                                   <td class="blackblodtext" align="left" width="182px"><asp:Literal ID="Literal13" Text="<%$ Resources:WebResources, ExpressConference_Public%>" runat="server"></asp:Literal></td><%--FB 2684--%>
                                   <td style="height: 24px">
                                     <table cellpadding="0" cellspacing="0"><tr><td>
                                        <asp:CheckBox ID="chkPublic" runat="server" />
                                        </td><td>
                                        <div id="openRegister" style="display:none">
                                            <asp:CheckBox ID="chkOpenForRegistration" runat="server" TextAlign="Left" /><span  class="blackblodtext" align="right"><asp:Literal ID="Literal14" Text="<%$ Resources:WebResources, ExpressConference_OpenforRegist%>" runat="server"></asp:Literal></span>
                                        </div>
                                        </td></tr>
                                     </table>
                                   </td>
                                </tr>
                                <%--FB 2226 End--%>	
                                <%--FB 2595 Start--%><%--FB 2684 Start--%>
                                 <tr id="trNetworkState" runat="server">
                                   <td  valign="top" id="tdNetworkState" runat="server" align="left" class="blackblodtext">&nbsp;<asp:Literal ID="Literal15" Text="<%$ Resources:WebResources, ExpressConference_tdNetworkState%>" runat="server"></asp:Literal></td>
                                   <td valign="bottom" >
                                      <asp:DropDownList ID="drpNtwkClsfxtn" runat="server" CssClass="alt2SelectFormat" style="margin-left:3px; width:140px">
                                                            <asp:ListItem Value="1" Text="<%$ Resources:WebResources, ExpressConference_NATOSecret%>"></asp:ListItem>
                                                            <asp:ListItem Value="0" Text="<%$ Resources:WebResources, ExpressConference_NATOUnclassified%>"></asp:ListItem>
                                                            </asp:DropDownList>
                                    </td>
                                 </tr>                       
                                <%--FB 2595 End--%><%--FB 2684 End--%>							
								<%--FB 2501 Starts--%>
                                <tr id="trVMR" runat="server"> 
                                      <td class="blackblodtext" align="left" valign="top" ><asp:Literal ID="Literal16" Text="<%$ Resources:WebResources, ExpressConference_VMR%>" runat="server"></asp:Literal></td><%-- ZD 100806--%>
                                                        <td style="height: 24px">
                                                           <table border="0"><tr><td>
                                                           <%--FB 2620 Starts--%>
                                                <asp:DropDownList ID="lstVMR" runat="server" style="margin-left:-3px;" CssClass="alt2SelectFormat" OnChange="javascript:changeVMR();addExternalParty();showHideAdvSettings();" Width="140px"> <%--FB 2993--%>
                                                    <%--FB 2448--%>
                                                    <asp:ListItem Value="0" Selected="True" Text="<%$ Resources:WebResources, None%>"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="<%$ Resources:WebResources, ExpressConference_Personal%>"></asp:ListItem>
                                                    <asp:ListItem Value="2" Text="<%$ Resources:WebResources, ExpressConference_Room%>"></asp:ListItem>
                                                    <asp:ListItem Value="3" Text="<%$ Resources:WebResources, ExpressConference_External%>"></asp:ListItem>
                                                    <%--<asp:ListItem Value="4" Text="Random"></asp:ListItem>--%> <%--FB 2620--%>
                                                    <%--FB 2481--%>
                                                </asp:DropDownList>
                                                
                                                           </td>
                                                           <td>
                                                           </td>
                                                           </tr></table>
                                                           
                                                
                                                <%--FB 2620 Ends--%>
                                        <%--VMR Start--%>
                                    <div id="divbridge" style="display:none" >
		                            <table>
                                        <tr>
                                            <td class="blackblodtext" align="left"><asp:Literal ID="Literal17" Text="<%$ Resources:WebResources, ExpressConference_InternalBridge%>" runat="server"></asp:Literal></td>
                                            <td style="height: 24px">
                                               <asp:TextBox ID="txtintbridge" ReadOnly="true" runat="server" ></asp:TextBox>
				                            <input type="hidden" name="intbridge" id="hdnintbridge" runat="server" />
				                            </td>
                                            </tr>
                                            <tr>
                                                <td class="blackblodtext" align="left"><asp:Literal ID="Literal18" Text="<%$ Resources:WebResources, ExpressConference_ExternalBridge%>" runat="server"></asp:Literal></td>
                                                <td style="height: 24px">
                                                   <asp:TextBox ID="txtextbridge" ReadOnly="true" runat="server" ></asp:TextBox>
				                            <input type="hidden" name="extbridge" id="hdnextbridge" runat="server" />
				                            </td>
                                        </tr>
                                    </table>
                                    </div>
                                    <%--VMR End--%>
                                    <%--ZD 100522 Starts--%>
                                    <table id="divChangeVMRPIN" style="display:none" runat="server">
                                        <tr>
                                            <td class="blackblodtext" align="left" nowrap="nowrap">
                                            <button runat="server" ID="btnChngPIN" type="button" style="width:180px" class="altMedium0BlueButtonFormat" onclick="javascript:ChangeRoomVMRPIN();">
                                            <asp:Literal Text="<%$ Resources:WebResources, Confsetup_ChangePassword%>" runat="server"></asp:Literal>
                                            </button> 
                                            <asp:ImageButton ID="imgVMRPIN" src="image/info.png" ToolTip="<%$ Resources:WebResources, VMRPasswordtip%>" AlternateText="Info" style="cursor:default" runat="server" OnClientClick="javascript:return false;"/><%-- ZD 102590 --%>
                                            </td>
                                            <td style="height: 24px">
                                            </td>
                                        </tr>
                                        <tr id="trVMRPIN1" style="display:none">
                                            <td align="left" class="blackblodtext">
                                            <asp:Literal Text="<%$ Resources:WebResources,  ConfSetup_NewPass%>" runat="server"></asp:Literal>
                                            </td>
                                            <td valign="top" nowrap="nowrap">
                                            <asp:TextBox ID="txtVMRPIN1" runat="server" TextMode="SingleLine" CssClass="altText" onblur="VMRPWDLen()"></asp:TextBox> <%--ZD 100522--%>
                                            <br />
                                            <asp:CompareValidator ID="CmpVMRPIN1" runat="server" ControlToCompare="txtVMRPIN2"
                                            ControlToValidate="txtVMRPIN1" Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, Required_password%>"></asp:CompareValidator>
                                            <asp:RegularExpressionValidator ID="RegVMRPIN" runat="server" SetFocusOnError="True"  ErrorMessage="<%$ Resources:WebResources, ConfSetup_PasswordLength%>" ToolTip="Only numeric values are allowed." ControlToValidate="txtVMRPIN1" Display="Dynamic"></asp:RegularExpressionValidator>
                                            </td>
                                        </tr>
                                        <tr id="trVMRPIN2" style="display:none">
                                            <td align="left" class="blackblodtext"> <asp:Literal ID="Literal146" Text="<%$ Resources:WebResources,  Confsetup_ConfirmNewPass%>" runat="server"></asp:Literal></td>
                                            <td style="height: 20px" valign="top">
                                            <asp:TextBox ID="txtVMRPIN2" runat="server" CssClass="altText" TextMode="SingleLine" ></asp:TextBox>
                                            <asp:CompareValidator ID="CmpVMRPIN2" runat="server" ControlToCompare="txtVMRPIN1"
                                            ControlToValidate="txtVMRPIN2" Display="Dynamic" ErrorMessage="Your passwords do not match."></asp:CompareValidator>
                                            </td>
                                        </tr>
                                    </table>
                                    <%--ZD 100522 Ends--%>
                                    </td>
                                </tr>
                                <%--ZD 100890 Starts--%>
                  	            <tr id="trStatic" runat="server">
                                    <td class="blackblodtext" align="left" width="182px"> <%--ZD 101714--%>
                                        <asp:Literal Text="<%$ Resources:WebResources,  Static%>" runat="server"></asp:Literal>
                                    </td>
                                    <td style="height: 24px">
                                        <table cellpadding="0" cellspacing="0"><tr><td>
                                            <asp:CheckBox ID="chkStatic" runat="server" onclick="javascript:fnchkStatic();" style="margin-left:-4px;"/><%--ZD 100969--%>
                                            </td><td>
                                            <div id="divStaticID" runat="server" style="display:none">
                                                <asp:TextBox ID="txtStatic" ReadOnly="true" runat="server" Enabled="false" ></asp:TextBox>
                                            </div>
                                        </td></tr></table>
                                    </td>
                                </tr>
                                <%--ZD 100890 End--%>
                                <%--FB Vidyo Start--%>
                                <tr id="trCloudConferencing" runat="server"> 
                                    <td class="blackblodtext" align="left" valign="bottom">&nbsp;<asp:Literal ID="Literal19" Text="<%$ Resources:WebResources, ExpressConference_Vidyo%>" runat="server"></asp:Literal></td><%--FB 2834--%>
                                    <td style="height: 24px">
                                       <asp:CheckBox ID="chkCloudConferencing" runat="server" onClick="fnChkCloudConference()" />
                                       </td>
                                  </tr>
                                  <%--FB Vidyo End--%>
                                								
                                <tr id="trConfPass" runat="server"> <%--ZD 100704--%>
                                    <td class="blackblodtext" align="left">
                                        <%--<asp:Literal ID="litNumericOrGuestPwd" Text="<%$ Resources:WebResources, ExpressConference_NumericPasswor%>" runat="server"></asp:Literal>--%> <%--ALLDEV-826--%>
                                        <asp:Literal ID="litNumericOrGuestPwd" runat="server"></asp:Literal> <%--ALLDEV-826--%>
                                    </td>
                                    <td valign="top" nowrap="nowrap">
                                  
                                        <asp:TextBox ID="ConferencePassword" maxlength="15" runat="server" TextMode="SingleLine" CssClass="altText" style="margin-left:-3px;"  onblur="ConfPWDLen()"></asp:TextBox><%--FB 2244--%> <%--ZD 100522--%><%--ALLDEV-826--%>
                                        <%--Code changes for FB : 1232--%>
                                        <%--ZD 102692 Start--%>
                                        <button runat="server" ID="btnGeneratePassword" type="button" style="width:180px" class="altMedium0BlueButtonFormat" onkeydown="if(event.keyCode == 13){ javascript:num_gen(); return false;}" onmousedown="javascript:num_gen(); return false;">
                                         <asp:Literal  Text="<%$ Resources:WebResources, ConferenceSetup_btnGeneratePassword%>" runat="server"></asp:Literal></button>
                                        <%--<br />--%>
                                        <%--<asp:CompareValidator ID="cmpValPassword1" runat="server" ControlToCompare="ConferencePassword2"
                                            ControlToValidate="ConferencePassword" Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, Required_password%>"></asp:CompareValidator>--%>
                                        <asp:RegularExpressionValidator ID="numPassword1" runat="server" ErrorMessage="<%$ Resources:WebResources, ConfSetup_PasswordLength%>" SetFocusOnError="True" 
                                        ToolTip="<%$ Resources:WebResources, NumericValuesOnly1%>" ControlToValidate="ConferencePassword" Display="Dynamic"></asp:RegularExpressionValidator> <%--Comments: Fogbugz case 107, 522 --%> <%--ZD 100522--%>
                                    </td><%--ZD 102692 End--%>
                               </tr>
                                <tr id="trConfPass1" runat="server"><%--ZD 100704--%>
                                    <td class="blackblodtext" align="left"><asp:Literal ID="Literal21" Text="<%$ Resources:WebResources, ExpressConference_ConfirmPasswor%>" runat="server"></asp:Literal></td>
                                    <td style="height: 20px" valign="top">
                                        <asp:TextBox ID="ConferencePassword2" maxlength="15" runat="server" CssClass="altText" TextMode="SingleLine" style="margin-left:-3px;" onblur="ConfpwdValidation()" ></asp:TextBox><%--FB 2244--%><%--ALLDEV-826--%>
                                        <asp:CompareValidator ID="cmpValPassword" runat="server" ControlToCompare="ConferencePassword"
                                            ControlToValidate="ConferencePassword2" Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, PasswordNotMatch1%>"></asp:CompareValidator>
                                    </td>
                                </tr>
								<%--FB 2501 End--%>
								<tr><td></td></tr><%-- ZD 100873--%>
                                <%--ALLDEV-826 Starts--%>                                
                                <tr id="trHostPwd" runat="server">
                                    <td class="blackblodtext" align="left">
                                        <asp:Literal Text="<%$ Resources:WebResources, ExpressConference_HostPassword%>" runat="server"></asp:Literal>
                                    </td>
                                    <td valign="top" nowrap="nowrap">                                  
                                        <asp:TextBox ID="txtHostPwd" maxlength="15" TextMode="SingleLine" CssClass="altText" style="margin-left:-3px;" onblur="HostPWDLen()" runat="server"></asp:TextBox>
                                        <button ID="btnGenerateHostPwd" type="button" style="width:180px" class="altMedium0BlueButtonFormat" onkeydown="if(event.keyCode == 13){ javascript:host_gen(); return false;}" onmousedown="javascript:host_gen(); return false;" onmouseup="javascript:chkRequiredHostPwd(); return false;" runat="server">
                                            <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_btnGeneratePassword%>" runat="server"></asp:Literal>
                                        </button>
                                        <asp:RegularExpressionValidator ID="regValHostPwd" ControlToValidate="txtHostPwd" ErrorMessage="<%$ Resources:WebResources, ConfSetup_PasswordLength%>" 
                                            ToolTip="<%$ Resources:WebResources, NumericValuesOnly1%>" Display="Dynamic" SetFocusOnError="True" runat="server">
                                        </asp:RegularExpressionValidator>
                                        <br /><asp:RequiredFieldValidator ID="reqValHostPwd" ControlToValidate="txtHostPwd" ErrorMessage="<%$ Resources:WebResources, ExpressConference_RequiredField%>" Font-Bold="True" Enabled="false" Display="Dynamic" runat="server"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr id="trCfmHostPwd" runat="server">
                                    <td class="blackblodtext" align="left">
                                        <asp:Literal Text="<%$ Resources:WebResources, ExpressConference_ConfirmPasswor%>" runat="server"></asp:Literal>
                                    </td>
                                    <td style="height:20px" valign="top">
                                        <asp:TextBox ID="txtCfmHostPwd" maxlength="15" CssClass="altText" TextMode="SingleLine" style="margin-left:-3px;" onblur="HostPwdValidation()" runat="server"></asp:TextBox>
                                        <asp:CompareValidator ID="cmpValHostPwd" ControlToValidate="txtCfmHostPwd" ControlToCompare="txtHostPwd"
                                            ErrorMessage="<%$ Resources:WebResources, PasswordNotMatch1%>" Display="Dynamic" runat="server">
                                        </asp:CompareValidator>
                                    </td>
                                </tr>                                
                                <%--ALLDEV-826 Ends--%>
                                <%--FB 2693 Start--%>
                                <tr id="trPCConf" runat="server" > 
                                    <td class="blackblodtext" align="left" style="height: 24px; vertical-align:top"><asp:Literal ID="Literal139" Text="<%$ Resources:WebResources, ExpressConference_DesktopVideo%>" runat="server"></asp:Literal></td> <%-- ZD 100221--%><%-- ZD 100807 100873--%>
                                    <td style="height: 24px; vertical-align:top;"><%--100873--%>
                                        <input type="checkbox" runat="server" id="chkPCConf" onclick="javascript:fnPCconf();changeVMR();addExternalParty();" style="margin-left:-2px;" /> <%--FB 2819--%>
                                        <table width="100%" border="0" id="tblPcConf" runat="server" style="display:none; margin-left:30px; margin-top:-20px" >
                                            <tr id="trBJ"  >
                                            <%--ZD 104021--%>
                                                <%--<td width="50%" align="left" id="tdBJ" runat="server" >
                                                    <input type="radio" style="vertical-align:top" id="rdBJ" name="PCSelection" value="1" runat="server" />
                                                    <img alt="<asp:Literal Text='<%$ Resources:WebResources, ExpressConference_BlueJeans%>' runat='server'></asp:Literal>" width="20px" src="../image/BlueJeans.jpg" title="<asp:Literal Text='<%$ Resources:WebResources, ExpressConference_BlueJeans%>' runat='server'></asp:Literal>" /> 
                                                    <a href="#" runat="server" class="PCSelected" id="btnBJ" style="cursor:pointer; text-decoration:none; vertical-align:top" ><b style="color:Blue; vertical-align:top" ><asp:Literal ID="Literal145" Text="<%$ Resources:WebResources, View%>" runat="server"></asp:Literal></b></a>
                                                </td>--%>
                                                <td align="left" id="tdJB" runat="server">
                                                    <input type="radio" style="vertical-align:top" id="rdJB" name="PCSelection" value="2" runat="server" /> 
                                                    <img alt="<asp:Literal Text='<%$ Resources:WebResources, ExpressConference_Jabber%>' runat='server'></asp:Literal>" width="20px" src="../image/Jabber.jpg"  title="<asp:Literal Text='<%$ Resources:WebResources, ExpressConference_Jabber%>' runat='server'></asp:Literal>" /><%--ZD 100419--%>
                                                    <a href="#" class="PCSelected" runat="server" id="btnJB" style="cursor:pointer; text-decoration:none; vertical-align:top;"> <b style="color:Blue; vertical-align:top" ><asp:Literal ID="Literal144" Text="<%$ Resources:WebResources, View%>" runat="server"></asp:Literal></b></a>
                                                </td>
                                            </tr>
                                            <tr id="trLync"  >
                                                <td align="left" id="tdLy" runat="server">
                                                    <input type="radio" style="vertical-align:top" id="rdLync" name="PCSelection" value="3" runat="server" /> 
                                                    <img alt="<asp:Literal Text='<%$ Resources:WebResources, ExpressConference_Lync%>' runat='server'></asp:Literal>" width="20px" src="../image/Lync.jpg"  title="<asp:Literal Text='<%$ Resources:WebResources, ExpressConference_Lync%>' runat='server'></asp:Literal>" /><%--ZD 100419--%>
                                                    <a href="#" class="PCSelected" id="btnLync"  runat="server"  style="cursor:pointer; text-decoration:none; vertical-align:top" ><b style="color:Blue; vertical-align:top" ><asp:Literal ID="Literal143" Text="<%$ Resources:WebResources, View%>" runat="server"></asp:Literal></b></a>
                                                </td>
                                                <td align="left" id="tdVid" runat="server" visible="false"> <%--ZD 102004--%>
                                                    <input type="radio" style="vertical-align:top" id="rdVidtel" name="PCSelection" value="4" runat="server" />                                                                         
                                                    <img alt="<asp:Literal Text='<%$ Resources:WebResources, ExpressConference_Vidtel%>' runat='server'></asp:Literal>" width="20px" src="../image/Vidtel.jpg"  title="<asp:Literal Text='<%$ Resources:WebResources, ExpressConference_Vidtel%>' runat='server'></asp:Literal>" /> <%--ZD 100419--%>
                                                    <a href="#" runat="server" class="PCSelected" id="btnVidtel" style="cursor:pointer; text-decoration:none; vertical-align:top" ><b style="color:Blue; vertical-align:top" ><asp:Literal ID="Literal142" Text="<%$ Resources:WebResources, View%>" runat="server"></asp:Literal></b></a>
                                                </td>
                                            </tr>
                                            <tr id="trVidyo"  style="display:none" >
                                                <td align="left">
                                                    <input type="radio" id="rdVidyo" name="PCSelection" value="5" runat="server" />                                                                        
                                                    <img alt="<asp:Literal Text='<%$ Resources:WebResources, ExpressConference_Vidyo%>' runat='server'></asp:Literal>" src="../image/Vidyo.jpg" /> <%--ZD 100419--%>
                                                </td>
                                                <td>
                                                    <a href="#" class="PCSelected" id="btnVidyo" ><asp:Literal ID="Literal141" Text="<%$ Resources:WebResources, View%>" runat="server"></asp:Literal></a>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <%--FB 2693 End--%>
								 <%--ZD 100221 Starts--%>
                                 <tr id="trWebex" runat="server"><%--ZD 100704--%>
                                    <td class="blackblodtext" style="vertical-align:middle" align="left"><asp:Literal ID="Literal140" Text="<%$ Resources:WebResources, ManageConference_Webex%>" runat="server"></asp:Literal></td>
                                    <td style="height: 24px" nowrap="nowrap">
                                  <%--  <table cellpadding="0"  cellspacing="0"><tr><td style="vertical-align:top">--%> 
                                       <asp:CheckBox runat="server" ID="chkWebex"  onclick="javascript:fnShowWebExConf()" style="margin-left:-4px;" />
                                       <input type="hidden" name="hdnHostWebEx" id="hdnHostWebEx" runat="server" /> <%-- ZD 101015--%>
                                       <input type="hidden" name="hdnSchdWebEx" id="hdnSchdWebEx" runat="server" /> <%-- ZD 101015--%>
                                     </td>
                                 </tr>
                                  <tr id="divWebPW" runat="server" style="display:none">
                                            <td class="blackblodtext"><asp:Literal ID="Literal22" Text="<%$ Resources:WebResources, ExpressConference_Password%>" runat="server"></asp:Literal><span class="reqfldstarText">*</span></td>
                                            <td>
                                            <asp:TextBox ID="txtPW1" Width="150px" TextMode="Password" CssClass="altText" runat="server" onblur="PasswordChange(1)" onfocus="fnTextFocus(this.id,1)"></asp:TextBox>
                                            <asp:ImageButton id="ImgWebExPW1" src="image/info.png" runat="server" alt="<%$ Resources:WebResources, ExpressConference_Info%>" style="cursor:default"
                                                            tooltip="<%$ Resources:WebResources, WebexToolTip1%>" OnClientClick="javascript:return false;" /><%--ZD 100935--%> <%--ZD 100221_17Feb--%><%-- ZD 102590 --%>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtPW1" Enabled="false"
                                                                    Display="Dynamic" ValidationGroup="Submit" ErrorMessage="<%$ Resources:WebResources, ExpressConference_RequiredField%>" Font-Bold="True"></asp:RequiredFieldValidator>
                                                             <asp:Label runat="server" ID="lblwebexerror" ForeColor="Red" style="display:none;margin-top:-16px;margin-left:15px;" Text="<%$ Resources:WebResources, Required_WebexPwd%>"></asp:Label>
                                                             <asp:RegularExpressionValidator style="margin-left:15px"   ID="regPassword1" ControlToValidate="txtPW1" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, Required_WebexPwd%>" ValidationExpression="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[A-Za-z\d]{6,}"></asp:RegularExpressionValidator> 
                                                     </td>   
                                   </tr>
                                    <tr id="divWebCPW" runat="server" style="display:none">
                                             <td class="blackblodtext"><asp:Literal ID="Literal23" Text="<%$ Resources:WebResources, ExpressConference_ConfirmPasswor%>" runat="server"></asp:Literal><span class="reqfldstarText">*</span>
                                             </td>
                                             <td>
                                                <asp:TextBox CssClass="altText" ID="txtPW2" TextMode="Password"  runat="server" onblur="PasswordChange(2)" onfocus="fnTextFocus(this.id,2)"></asp:TextBox><%-- ZD 100221 --%>
                                                       <asp:ImageButton id="ImgWebExPW2" src="image/info.png" runat="server" alt="<%$ Resources:WebResources, ExpressConference_Info%>" style="cursor:default"
                                                        tooltip="<%$ Resources:WebResources, WebexToolTip1%>" OnClientClick="javascript:return false;" /><%--ZD 100935--%> <%--ZD 100221_17Feb--%><%-- ZD 102590 --%>
                                                        <asp:Label runat="server" ID="Label3" ForeColor="Red" style="display:none" ></asp:Label>   
                                                        <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtPW2"
                                                        ControlToValidate="txtPW1" Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, EnterPassword%>" style="margin-left:10px;margin-top:-16px;"></asp:CompareValidator>
                                                        <asp:RegularExpressionValidator ID="regPassword2" ControlToValidate="txtPW2" style="margin-left:10px;margin-top:-16px;" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, Required_WebexPwd%>" ValidationExpression="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[A-Za-z\d]{6,}"></asp:RegularExpressionValidator> 
                                                        <asp:CompareValidator ID="cmpValPassword2" runat="server" ControlToCompare="txtPW1" style="margin-left:10px;margin-top:-16px;"
                                                        ControlToValidate="txtPW2" Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, PasswordNotMatch%>"></asp:CompareValidator>
                                                        </td>
                                    </tr>
                                   
                                  <%--ZD 100221 Ends--%>
                                <tr id="StartNowRow"><%--FB 2341--%>
                                    <td class="blackblodtext" align="left">
                                        
                                        <span class="reqfldstarText"></span><asp:Literal ID="Literal24" Text="<%$ Resources:WebResources, ExpressConference_StartNow%>" runat="server"></asp:Literal> <%-- ZD 100221--%>
                                    </td>
                                    <td>
                                        
                                        <asp:CheckBox runat="server" ID="chkStartNow" style="margin-left:-4px;" />
                                    </td>
                                </tr>
                                <%--ZD 103550 - Start--%>
                                <tr id="trBJNMeeting" runat="server">
                                   <td class="blackblodtext" align="left" width="182px"><asp:Literal Text="<%$ Resources:WebResources, mainadministrator_BJNMeetingID%>" runat="server"></asp:Literal></td>
                                   <td style="height: 24px">
                                     <table cellpadding="1" cellspacing="3"><tr><td><%-- ZD 100221--%>
                                        <asp:CheckBox ID="chkBJNMeetingID" runat="server" onclick="javascript:fnBJNMeetingOptSelection();" style="margin-left:-8px;" />
                                        </td><td>
                                        <div id="DivBJNMeetingID" runat="server" style="display:none">
                                            <asp:DropDownList ID="DrpBJNMeetingID" runat="server" CssClass="alt2SelectFormat" style="width:172px">                                                
                                                <asp:ListItem Value="1" Text="<%$ Resources:WebResources, mainadministrator_PesonalMeetingID%>"></asp:ListItem>
                                                <asp:ListItem Value="2" Text="<%$ Resources:WebResources, mainadministrator_OneTimeMeetingID%>"></asp:ListItem>                                                
                                            </asp:DropDownList> 
                                        </div>
                                        </td></tr>
                                     </table>
                                   </td>
                                </tr>
                                <%--ZD 103550 - End--%>
                                <%--FB 2870 Start--%>
                                 <tr id="TrCTNumericID" runat="server">
                                   <td class="blackblodtext" align="left" width="182px"><asp:Literal ID="Literal25" Text="<%$ Resources:WebResources, ExpressConference_CTSNumericID%>" runat="server"></asp:Literal></td>
                                   <td style="height: 24px">
                                     <table cellpadding="1" cellspacing="3"><tr><td><%-- ZD 100221--%>
                                        <asp:CheckBox ID="ChkEnableNumericID" runat="server" onclick="javascript:ChangeNumeric();" style="margin-left:-4px;" />
                                        </td><td>
                                        <div id="DivNumeridID" runat="server" style="display:none">
                                            <asp:TextBox ID="txtNumeridID" runat="server" ></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="ReqNumeridID" Enabled="false" runat="server" ControlToValidate="txtNumeridID"
                                                                        Display="dynamic" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, ExpressConference_RequiredField%>"></asp:RequiredFieldValidator>
                                             <asp:RegularExpressionValidator ID="RegNumeridID" ControlToValidate="txtNumeridID" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters12%>" ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator>
                                        </div>
                                        </td></tr>
                                     </table>
                                   </td>
                                </tr>
                                <%--FB 2870 End--%>
                                <tr id="trLineRate" runat="server"><%--FB 2394--%> <%--FB 2641--%>
                                    <td class="blackblodtext" align="left">
                                        
                                        <span class="reqfldstarText"></span><asp:Literal ID="Literal26" Text="<%$ Resources:WebResources, ExpressConference_MaximumLineRa%>" runat="server"></asp:Literal>
                                    </td>
                                    <td>
                                        
                                        <asp:DropDownList ID="lstLineRate" CssClass="altSelectFormat" DataTextField="LineRateName" DataValueField="LineRateID" runat="server" Width="30%"></asp:DropDownList>
                                    </td>
                                </tr>
                                 <%--FB 2501 starts--%>
                                <tr id="trStartMode" runat="server"> <%--FB 2641--%>
                                   <td class="blackblodtext" align="left"><div id="trStartMode1" runat="server"><asp:Literal ID="Literal27" Text="<%$ Resources:WebResources, ExpressConference_trStartMode1%>" runat="server"></asp:Literal></div></td>
                                   <td ><div id="trStartMode2" runat="server">
                                        <asp:DropDownList ID="lstStartMode" runat="server" CssClass="alt2SelectFormat">
                                            <asp:ListItem Value="0" Selected="True" Text="<%$ Resources:WebResources, Automatic%>"></asp:ListItem>
                                            <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Manual%>"></asp:ListItem>
                                        </asp:DropDownList></div> 
                                   </td>
                                </tr>
                                <%--FB2501 ends--%>
                                <tr id="trAdvancedSettings" runat="server">
                                    <td class="blackblodtext" align="left">
                                         <%--FB 2266--%>
                                        <span class="reqfldstarText"></span><asp:Literal ID="Literal28" Text="<%$ Resources:WebResources, ExpressConference_AdvancedSettin%>" runat="server"></asp:Literal><%-- ZD 100221--%>
                                    </td>
                                    <td><%--ALLDEV-814--%>
                                         <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:CheckBox runat="server" ID="chkAdvancesetting" onclick="openAdvancesetting('C');" style="margin-left:-4px;" />                                        
                                            <asp:Button ID="btnAdvSettings" BackColor="Transparent" style="display:none;" runat="server" OnClick="FetchAvailableAudioParticipants" />
                                         </ContentTemplate>
                                        </asp:UpdatePanel>

                                    </td>
                                </tr>
                                <%-- ZD 100834 Starts--%>
                                <tr id="trCheckOptions" runat="server">                                     
                                    <td class="blackblodtext" align="left">
                                        <span class="reqfldstarText"></span><asp:Literal ID="Literal29" Text="<%$ Resources:WebResources, ExpressConference_Options%>" runat="server"></asp:Literal>
                                    </td>
                                    <td>
                                       
                                        <asp:CheckBox onClick="openAdvancesetting()" runat="server" ID="chkAdvancedOptions" style="margin-left:-4px;" />
                                       
                                    </td>
                                </tr>
                                <%-- ZD 100834 End--%>
                                <tr id="divDuration" style="display: none">
                                    <td class="blackblodtext" align="left">
                                        <asp:Literal ID="Literal30" Text="<%$ Resources:WebResources, ExpressConference_Duration%>" runat="server"></asp:Literal><span class="reqfldstarText">*</span>
                                    </td>
                                    <td>
                                        <mbcbb:ComboBox ID="lstDuration" runat="server" CssClass="altSelectFormat" Rows="10"
                                            CausesValidation="True" width="80px">
                                            <asp:ListItem Value="01:00" Selected="True">01:00</asp:ListItem>
                                            <asp:ListItem Value="02:00">02:00</asp:ListItem>
                                            <asp:ListItem Value="03:00">03:00</asp:ListItem>
                                            <asp:ListItem Value="04:00">04:00</asp:ListItem>
                                            <asp:ListItem Value="05:00">05:00</asp:ListItem>
                                            <asp:ListItem Value="06:00">06:00</asp:ListItem>
                                            <asp:ListItem Value="07:00">07:00</asp:ListItem>
                                            <asp:ListItem Value="08:00">08:00</asp:ListItem>
                                            <asp:ListItem Value="09:00">09:00</asp:ListItem>
                                            <asp:ListItem Value="10:00">10:00</asp:ListItem>
                                            <asp:ListItem Value="11:00">11:00</asp:ListItem>
                                            <asp:ListItem Value="12:00">12:00</asp:ListItem>
                                        </mbcbb:ComboBox>
                                        hh:mm
                                    </td>
                                </tr>
                                <tr id="NONRecurringConferenceDiv5" style="display: none;">
                                    <td class="blackblodtext" align="right">
                                        <asp:Literal ID="Literal31" Text="<%$ Resources:WebResources, ExpressConference_Duration%>" runat="server"></asp:Literal>
                                    </td>
                                    <td align="left">
                                        <asp:Label ID="lblConfDuration" runat="server"></asp:Label>
                                        <asp:Button ID="btnRefresh" CssClass="dnButtonLong" Text="<%$ Resources:WebResources, ExpressConference_btnRefresh%>" OnClick="CalculateDuration"
                                            OnClientClick="javascript:DataLoading(1);" runat="server" ValidationGroup="Update" />
                                    </td>
                                </tr>
                                <tr id="DurationRow" style="display: none;">
                                    <td align="left" class="blackblodtext" valign="top"> <%--FB 2592--%>
                                        <asp:Literal ID="Literal32" Text="<%$ Resources:WebResources, ExpressConference_Duration%>" runat="server"></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="RecurDurationhr" runat="server" CssClass="altText" Width="15%" onchange="javascript: SetRecurBuffer();"
                                            onblur="Javascript:  return validateDurationHr();"></asp:TextBox><font class="blackblodtext"><asp:Literal ID="Literal131" Text="<%$ Resources:WebResources, ExpressConference_hrs%>" runat="server"></asp:Literal></font>
                                        <asp:TextBox ID="RecurDurationmi" runat="server" CssClass="altText" Width="15%" onchange="javascript: SetRecurBuffer();recurTimeChg();"
                                            onblur="Javascript: return validateDurationMi();"></asp:TextBox><font class="blackblodtext"><asp:Literal ID="Literal132" Text="<%$ Resources:WebResources, ExpressConference_mins%>" runat="server"></asp:Literal></font>
                                        <br />
                                        <span class="blackblodtext" style="font-size: 8pt">
                                            <%if (EnableBufferZone == 0)
                                              { %>
                                            (<asp:Literal ID="Literal133" Text="<%$ Resources:WebResources, MaxLimit1%>" runat="server"></asp:Literal>
                                            <%=Application["MaxConferenceDurationInHours"]%>
                                            <asp:Literal ID="Literal134" Text="<%$ Resources:WebResources, ExpressConference_hrs%>" runat="server"></asp:Literal>) <%--ZD 100528--%>
                                            <%}
                                              else
                                              { %>
                                            (<asp:Literal ID="Literal135" Text="<%$ Resources:WebResources, MaxLimit1%>" runat="server"></asp:Literal>
                                            <%=Application["MaxConferenceDurationInHours"]%>
                                            <asp:Literal ID="Literal136" Text="<%$ Resources:WebResources, ExpressConference_MaxLimit12%>" runat="server"></asp:Literal>)<%--ZD 100528--%>
                                            <%} %>
                                        </span>
                                        <asp:TextBox ID="EndText" runat="server" Style="display: none"></asp:TextBox>
                                    </td>
                                </tr>
                               
                            </table>
                        </td>
                        
                        <td valign="top" align="left" width="50%">
                            <table width="70%">
                             <%--ZD 100574 - Inncrewin Starts--%>
                                  <tr id="trHostRow" runat="server">
                                 <td nowrap="nowrap" class="blackblodtext" align="left" id="Td2" runat="server" valign="top"><asp:Literal ID="Literal33" Text="<%$ Resources:WebResources, ExpressConference_Field4%>" runat="server"></asp:Literal>
                                 <span class="reqfldstarText">*&nbsp;</span>
                                 </td>
                                    <td>
                                    <asp:TextBox ID="txtHost_exp" runat="server" CssClass="altText" Enabled="false" style="width:165px;" ></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtHost_exp" ErrorMessage="<%$ Resources:WebResources, ExpressConference_RequiredField%>" Font-Bold="True" Display="Dynamic"  ></asp:RequiredFieldValidator>
                                    <a href="" onclick="this.childNodes[0].click();return false;"><img id="Img1" onclick="javascript:getYourOwnEmailList(3,1)" alt="<asp:Literal Text='<%$ Resources:WebResources, ExpressConference_Edit%>' runat='server'></asp:Literal>" src="image/edit.gif" style="cursor:pointer;" title="<asp:Literal Text='<%$ Resources:WebResources, AddressBook%>' runat='server'></asp:Literal>" /> </a> 
                                    <asp:TextBox ID="hdntxtHost_exp" runat="server" BackColor="Transparent" BorderColor="White" TabIndex="-1" BorderStyle="None" Width="0px" ForeColor="Black"></asp:TextBox>
                                    <asp:TextBox ID="TxtHostMail" runat="server" Visible="false"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr id="trDescription" runat="server">
                                <td class="blackblodtext" align="left" nowrap="nowrap" style="vertical-align: top;">
                                                                <asp:Literal ID="Literal34" Text="<%$ Resources:WebResources, ExpressConference_ConferenceDesc%>" runat="server"></asp:Literal>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtDescription_Exp" runat="server" MaxLength="2000" class="altText" style="width:165px; height:100px; padding:0;"
                                      Rows="2" TextMode="MultiLine"  Columns="20" Wrap="true"></asp:TextBox>
                                    <asp:RegularExpressionValidator Enabled="false" ID="RegularExpressionValidator1" ControlToValidate="txtDescription_Exp" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters12%>" ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> <%-- ZD 100263 --%>
                                    <asp:RegularExpressionValidator Enabled="false" ID="RegularExpressionValidator2"
                                         ControlToValidate="txtDescription_Exp" Display="dynamic" runat="server" SetFocusOnError="true"
                                         ErrorMessage="<%$ Resources:WebResources, InvalidCharacters15%>"
                                         ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+?|!`\[\]{}\=@#$%&~]*$"></asp:RegularExpressionValidator>                                                                
                                    <asp:RegularExpressionValidator runat="server" ID="RegularExpressionValidator4" ControlToValidate="txtDescription_Exp"  ValidationExpression="^[\s\S]{0,2000}$"   ErrorMessage="<%$ Resources:WebResources, MaxLimit3%>"
                                                                Display="Dynamic"></asp:RegularExpressionValidator>
                                  </td>
                                </tr>
                              <%--ZD 100574 - Inncrewin Ends--%>
                              <%--ZD 100834 - Starts--%>
                              <tr id="trDetails" runat="server">
                              <td colspan="2" align="left">
                                <table border="0" style="background-color: #2C398D; width: 505px;" cellspacing="1">
                                    <tr style="background-color: #FFFFFF">
                                        <td align="left" style="width: 31%;" valign="top">
                                            <table style="width: 100%;" border="0" cellpadding="3">
                                            <tr>
                                                    <td style="width:150px;">
                                                    </td>
                                                    <td align="left">
                                                        <asp:Label ID="Label4" runat="server" Text="<%$ Resources:WebResources, ExpressConference_Details%>" Style="font-size: 10pt;font-weight:bold" />
                                                    </td>
                                                </tr>
                                            <tr id="trConfDesc" runat="server" > <%--ZD 100704--%>
                                                <td class="blackblodtext" align="left" nowrap="nowrap">
                                                    <asp:Literal ID="Literal35" Text="<%$ Resources:WebResources, ExpressConference_ConferenceDesc%>" runat="server"></asp:Literal>

                                                </td>
                                                <td>
                                                    <asp:TextBox ID="ConferenceDescription" runat="server" MaxLength="2000" class="altText" Height="40"
                                                        Rows="2" TextMode="MultiLine" Width="310PX" Columns="20" Wrap="true"></asp:TextBox>
                                                    <asp:RegularExpressionValidator Enabled="false" ID="regConfDisc" ControlToValidate="ConferenceDescription" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters12%>" ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> <%-- ZD 100263 --%>
                                                    <%--<asp:RegularExpressionValidator Enabled="false" ID="RegularExpressionValidator3"
                                                        ControlToValidate="ConferenceDescription" Display="dynamic" runat="server" SetFocusOnError="true"
                                                        ErrorMessage="<%$ Resources:WebResources, InvalidCharacters15%>"
                                                        ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+?|!`\[\]{}\=@#$%&~]*$"></asp:RegularExpressionValidator>--%><%--@@@@@ 103385--%>
                                                    <%--FB 1888--%>
                                                    <asp:RegularExpressionValidator runat="server" ID="ValConfDesc" ControlToValidate="ConferenceDescription"  ValidationExpression="^[\s\S]{0,2000}$"   ErrorMessage="<%$ Resources:WebResources, MaxLimit3%>"
                                                    Display="Dynamic"></asp:RegularExpressionValidator><%--FB 2508--%>   
                                                </td>
                                            </tr>
                                            <tr id="NONRecurringConferenceDiv3">
                                                <td class="blackblodtext" align="left">
                                                    <asp:Literal ID="Literal36" Text="<%$ Resources:WebResources, ExpressConference_TimeZone%>" runat="server"></asp:Literal><span class="reqfldstarText">*</span>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="lstConferenceTZ" runat="server" Width="210px" DataTextField="timezoneName"
                                                        DataValueField="timezoneID" CssClass="altText">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr id="NONRecurringConferenceDiv9" runat="server">
                                                <td class="blackblodtext">
                                                    <asp:Literal ID="Literal37" Text="<%$ Resources:WebResources, ExpressConference_EnableBufferZ%>" runat="server"></asp:Literal>
                                                </td>
                                                <td>
                                                    <asp:CheckBox ID="chkEnableBuffer" runat="server" Checked="true" />
                                                </td>
                                            </tr>
                                            <tr> <%--FB 2592--%>
                                            <td nowrap="nowrap" class="blackblodtext" align="left" id="Field4" runat="server" valign="top"><asp:Literal ID="Literal38" Text="<%$ Resources:WebResources, ExpressConference_Field4%>" runat="server"></asp:Literal></td>
                                            <td>
                                                <asp:TextBox ID="txtApprover4" runat="server" CssClass="altText"
                                                    Enabled="true"></asp:TextBox>
                                                <a href="" onclick="this.childNodes[0].click();return false;"><img id="Img8" onclick="javascript:getYourOwnEmailList(3,1)" alt="<asp:Literal Text='<%$ Resources:WebResources, ExpressConference_Edit%>' runat='server'></asp:Literal>" src="image/edit.gif" style="cursor:pointer;" title="<asp:Literal Text='<%$ Resources:WebResources, AddressBook%>' runat='server'></asp:Literal>" /> </a> <%--FB 2798--%> <%--ZD 100419--%><%--ZD 100420--%>
                                                <asp:TextBox ID="hdnApprover4" runat="server" BackColor="Transparent" BorderColor="White" TabIndex="-1"
                                                    BorderStyle="None" Width="0px" ForeColor="Black"></asp:TextBox>
                                                <asp:TextBox ID="hdnApproverMail" runat="server" Visible="false"></asp:TextBox>
                                            </td>
                                            </tr>
                                            <tr><%--FB 2359--%>
                                            <td class="blackblodtext"  align="left"><asp:Literal ID="Literal39" Text="<%$ Resources:WebResources, ExpressConference_Requestor%>" runat="server"></asp:Literal></td>
                                            <%--FB 2501 starts--%>
                                            <td align="left">
                                                <asp:TextBox ID="txtApprover7" runat="server" CssClass="altText"></asp:TextBox>
                                                    <a href="" onclick="this.childNodes[0].click();return false;"><img id="Img3" onclick="javascript:getYourOwnEmailList(6,2)" src="image/edit.gif"  alt="<asp:Literal Text='<%$ Resources:WebResources, ExpressConference_Edit%>' runat='server'></asp:Literal>" style="cursor:pointer;" title="<asp:Literal Text='<%$ Resources:WebResources, AddressBook%>' runat='server'></asp:Literal>"/></a> <%--FB 2798--%> <%--ZD 100419--%><%--ZD 100420--%>
                                                    <asp:TextBox ID="hdnApprover7" runat="server" BackColor="Transparent" BorderColor="White" TabIndex="-1"
                                                    BorderStyle="None" Width="0px" ForeColor="Black"></asp:TextBox>
                                                    <asp:TextBox ID="hdnRequestorMail" runat="server" Width="0px" style="display:none"></asp:TextBox>
                                            </td>
                                            <%--FB 2501 ends--%>
                                            </tr>
                                            </table>
                                            </td>
                                    </tr>
                                </table>
                              </td>
                              </tr>
                                <tr id="Advancesetting" runat="server" style="display:none">
                                    <td colspan="2" align="left">
                                        <table border="0" style="background-color: #2C398D; width: 450px;" cellspacing="1">
                                            <tr style="background-color: #FFFFFF">
                                                <td align="left" style="width: 31%;" valign="top">
                                                    <table style="width: 100%;" border="0" cellpadding="3">
                                                        <tr>
                                                            <td style="width:150px;"><%--FB 2592--%>
                                                            </td>
                                                            <td align="left">
                                                              <asp:Label ID="Label2" runat="server" Text="<%$ Resources:WebResources, ExpressConference_AdvancedSettin%>" Style="font-size: 10pt;font-weight:bold" />
                                                            </td>
                                                        </tr>
                                                        
                                                        <%--FB 2583 START--%>
                                                        <tr id="trFECC" runat="server">
                                                            <td id="tdFECC" runat="server" align="left" class="blackblodtext"><asp:Literal ID="Literal40" Text="<%$ Resources:WebResources, ExpressConference_tdFECC%>" runat="server"></asp:Literal></td>
                                                            <td><asp:CheckBox ID="chkFECC" runat="server" /></td>
                                                       </tr>
                                                        <%--FB 2583 END--%>
                                                        <%--ZD 101869 Starts--%>
                                                        <tr id="trLayout" runat="server">
                                                        <td align="left" class="subtitlexxsblueblodtext" id="tdVideoDisplay" runat="server"> <%--ZD 101931--%>
                                                            <asp:Literal ID="Literal95" Text="<%$ Resources:WebResources, ConferenceSetup_VideoDisplay%>" runat="server"></asp:Literal></td>
                                                        <td nowrap="nowrap" id="tdimgVideoDisplay" runat="server"> <%--ZD 101931--%>
                                                        <table>
                                                            <tr>
                                                            <td nowrap="nowrap" style="vertical-align:middle;"><%--ZD 102057--%>
                                                                <asp:Label runat="server" id="lblCodianLO" class="blackblodtext" ></asp:Label><br />
                                                                <asp:Image ID="imgVideoDisplay" runat="server" AlternateText="Video Display" /> <%--ZD 100419--%>
                                                                <asp:image id="imgLayoutMapping6" runat="server" alt="Layout" style="display:none" />
                                                                <asp:image id="imgLayoutMapping7" runat="server" alt="Layout"  style="display:none" />
                                                                <asp:image id="imgLayoutMapping8" runat="server" alt="Layout"  style="display:none" />
                                                            </td>
                                                            <td valign="bottom">
                                                                <button id="Button3" runat="server" name="ConfLayoutSubmit" style="width:65px;" 
                                                                onkeydown="if(event.keyCode == 13){ javascript:managelayout('', '01', '','0');return false;}"  class="altMedium0BlueButtonFormat" 
                                                                onClick="managelayout('', '01', '','1');return false;" type="button"><asp:Literal ID="Literal147" 
                                                                Text="<%$ Resources:WebResources, Change%>" runat="server"></asp:Literal></button> <%--ZD 100875--%>
                                                            </td>
                                                            </tr>
                                                         </table>
                                                        </td>
                                                        </tr>
                                                        <%--ZD 101869 End--%>
														<%--FB 2839 Start--%>
														 <tr id="trMCUProfile1" runat="server"> <%--FB 3063--%>
                                                            <td id="tdMCUProfile" runat="server" colspan="2">
                                                                <div id="div3" runat="server">
                                                                    <table id="Table1" runat="server" width="100%" style="margin-top:-20px">
                                                                        <tr>
                                                                            <td class="subtitlexxsblueblodtext"><asp:Literal ID="Literal41" Text="<%$ Resources:WebResources, ExpressConference_MCUProfile%>" runat="server"></asp:Literal></td> <%--ZD 100298--%>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left">
                                                                                <table width="100%" border="0">
																				    <tr>
                                                                                        <td colspan="2">
                                                                                            <table width="100%" border="0" cellspacing="0px" cellpadding="0px" > <%--FB 2349--%>
                                                                                                <tr>
                                                                                                    <td align="left">
                                                                                                        <asp:Table ID="tblMCUProfile" runat="server" CellPadding="3" width="100%" CellSpacing="2" Visible="true">
                                                                                                        </asp:Table>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </td>
                                                        </tr>
														<%--FB 2839 End--%>
                                                        <%--ZD 104256 Starts--%>
                                                        <tr id="trPoolOrderSelection" runat="server"> 
                                                            <td id="tdPoolOrder" runat="server" colspan="2">
                                                                <div id="div5" runat="server">
                                                                    <table id="Table3" runat="server" width="100%" style="margin-top:-20px">
                                                                        <tr>
                                                                            <td class="subtitlexxsblueblodtext"><asp:Literal ID="Literal145" Text="<%$ Resources:WebResources, PoolOrder%>" runat="server"></asp:Literal></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left">
                                                                                <table width="100%" border="0">
																				    <tr>
                                                                                        <td colspan="2">
                                                                                            <table width="100%" border="0" cellspacing="0px" cellpadding="0px" > <%--FB 2349--%>
                                                                                                <tr>
                                                                                                    <td align="left">
                                                                                                        <asp:Table ID="tblPoolOrder" runat="server" CellPadding="3" width="100%" CellSpacing="2" Visible="true">
                                                                                                        </asp:Table>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <%--ZD 104256 Ends--%>
                                                        <tr id="MCUConnectDisplayRow"  runat="server"> <%--FB 2998--%>
                                                            <td colspan="2">
                                                                <table width="100%" style="margin-top:-20px">     <%--ZD 104256  --%>            
                                                                     <tr>
                                                                        <td class="subtitlexxsblueblodtext" colspan="2">
                                                                            <asp:Label ID="lblMCUConnect" runat="server" Text="<%$ Resources:WebResources, ExpressConference_lblMCUConnect%>"></asp:Label>
                                                                        </td>
                                                                    </tr>                                      
                                                                    <tr >
                                                                        <td  align="left" id="Td3" nowrap="nowrap" class="blackblodtext" width="5%">
                                                                        </td>
                                                                        <td valign="top">
                                                                            <table width="100%" border="0">
                                                                             <tr align="left">
                                                                                 <td id="ConnectCell" runat="server" width="45%" valign="top">
                                                                                    <span class="blackblodtext"><asp:Literal ID="Literal137" Text="<%$ Resources:WebResources, ExpressConference_Connect%>" runat="server"></asp:Literal></span> &nbsp;<asp:TextBox ID="txtMCUConnect"  runat="server" CssClass="altText" Width="50px"
                                                                                    onblur="javascript:if(this.value.trim() =='') this.value='0';"></asp:TextBox> <%--ZD 100528--%>
                                                                                    <asp:ImageButton ID="imgMCUConnect" AlternateText="<%$ Resources:WebResources, ExpressConference_Info%>" src="image/info.png" runat="server" style="cursor:default" OnClientClick="javascript:return false;"/> <%--ZD 100419--%><%-- ZD 102590 --%>
                                                                                    <br />
                                                                                    <%--FB 2998 Validation Part Starts--%>
                                                                                    <%--<asp:CompareValidator  id="cmpNumbers" Enabled="true"  ErrorMessage="MCU Connect time should be less than or equal to Setup Time." display="dynamic"
                                                                                        ControlToCompare="SetupDuration" ControlToValidate="txtMCUConnect" Operator="LessThanEqual" runat="server" Type="Integer"
                                                                                        ValidationGroup="Submit" SetFocusOnError="true"  EnableClientScript="true" ></asp:CompareValidator>--%>
                                                                                   <%--FB 2998 Validation Part Ends--%><%-- ZD 100405 --%>
                                                                                    <%--<asp:RangeValidator id="RangeValidator1" setfocusonerror="true" type="Integer" minimumvalue="-15"
                                                                                        maximumvalue="15" display="Dynamic" controltovalidate="txtMCUConnect" runat="server"
                                                                                        errormessage="MCU connect time is not allowed more than 15 mins."></asp:RangeValidator>--%>
                                                                                    <asp:RegularExpressionValidator id="RegularExpressionValidator17" validationgroup="Submit"
                                                                                        controltovalidate="txtMCUConnect" display="dynamic" runat="server" setfocusonerror="true"
                                                                                        errormessage="<%$ Resources:WebResources, NumericValuesOnly%>" validationexpression="^-{0,1}\d+$"></asp:RegularExpressionValidator>
                                                                                        <asp:CustomValidator id="customMCUPreStart" Enabled="true"  ErrorMessage="<%$ Resources:WebResources, MCUConnectTime%>" Display="Dynamic"
                                                                                        ControlToValidate="txtMCUConnect"  runat="server"  ClientValidationFunction="fnCompareSetup"></asp:CustomValidator><%-- ZD 100433--%>
                                                                                        
                                                                                 </td>
                                                                                 <td id="DisconnectCell" runat="server" width="55%" valign="top">
                                                                                   <span class="blackblodtext"><asp:Literal ID="Literal138" Text="<%$ Resources:WebResources, ExpressConference_Disconnect%>" runat="server"></asp:Literal></span> &nbsp;<asp:TextBox ID="txtMCUDisConnect" runat="server" CssClass="altText" Width="50px" 
                                                                                   onblur="javascript:if(this.value.trim() =='') this.value='0';"></asp:TextBox><%--ZD 100528--%>
                                                                                   <asp:ImageButton ID="imgMCUDisconnect" AlternateText="<%$ Resources:WebResources, ExpressConference_Info%>" src="image/info.png" runat="server" style="cursor:default" OnClientClick="javascript:return false;"/> <%--ZD 100419--%><%-- ZD 102590 --%>
                                                                                    <br /><%-- ZD 100405 --%>
                                                                                    <%--<asp:rangevalidator id="RangeValidator2" setfocusonerror="true" type="Integer" minimumvalue="-15"
                                                                                        maximumvalue="15" display="Dynamic" controltovalidate="txtMCUDisConnect" runat="server"
                                                                                        errormessage="MCU disconnect time is not allowed more than 15 mins."></asp:rangevalidator>--%>
                                                                                    <asp:regularexpressionvalidator id="RegularExpressionValidator20" validationgroup="Submit"
                                                                                        controltovalidate="txtMCUDisConnect" display="dynamic" runat="server" setfocusonerror="true"
                                                                                        errormessage="<%$ Resources:WebResources, NumericValuesOnly%>" validationexpression="^-{0,1}\d+$"></asp:regularexpressionvalidator>
                                                                                    <%--FB 2998 Validation Part Starts--%>
                                                                                    <%--<asp:CompareValidator  id="cmpTeardown" Enabled="true"  ErrorMessage="MCU Disconnect Time should be less than or equal to Tear-Down Time." display="dynamic"
                                                                                        ControlToCompare="TearDownDuration" ControlToValidate="txtMCUDisConnect" Operator="LessThanEqual" runat="server" Type="Integer"
                                                                                         ValidationGroup="Submit" SetFocusOnError="true"  EnableClientScript="true" ></asp:CompareValidator>--%>
                                                                                    <%--FB 2998 Validation Part Ends--%>
                                                                                    <%--ZD 102392--%>
                                                                                    <asp:CustomValidator id="customMCUPreEnd" Enabled="true"  ErrorMessage="<%$ Resources:WebResources, MCUDisConnectTime%>" Display="Dynamic"
                                                                                        ControlToValidate="txtMCUDisConnect"  runat="server"  ClientValidationFunction="fnCompareTear"></asp:CustomValidator><%-- ZD 100433--%>
                                        
                                                                                 </td>
                                                                             </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>                                                                
                                                        <tr id="trAudioConf"><%--FB 2341--%>
                                                            <td colspan="2">
                                                              <div id="divAudioConf" runat="server"><%--FB 2359--%>                                                             
                                                                <table id="tblAudioser" runat="server" width="100%" style="margin-top:-20px" border="0">
                                                                    <tr>
                                                                        <td class="subtitlexxsblueblodtext"><asp:Literal ID="Literal42" Text="<%$ Resources:WebResources, ExpressConference_AudioConferenc%>" runat="server"></asp:Literal><span class="blackblodtext" style="font-size: 8pt"></span></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left">
                                                                            <table width="100%" border="0">
                                                                                <tr>
                                                                                    <%--<td width="16%">
                                                                                    </td>--%>
                                                                                    <td align="left">
                                                                                        <table cellspacing="0" cellpadding="0" border="0">
                                                                                            <tr>
                                                                                                <td align="left" valign="middle" style="width: 6%;">
                                                                                                </td>
                                                                                                <td valign="middle">
                                                                                                    <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                                                                                                    <ContentTemplate>
                                                                                                    <table border="0" width="80%" >
                                                                                                        <tr>
                                                                                                            <td colspan="4" align="left" class="blackblodtext">
                                                                                                                <asp:RadioButtonList runat="server" ID="lstAudioParty" onClick="SelectAudioParty()">
                                                                                                                </asp:RadioButtonList>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td id="td1" class="blackblodtext" align="center" valign="middle">
                                                                                                                <asp:Literal ID="Literal43" Text="<%$ Resources:WebResources, ExpressConference_td1%>" runat="server"></asp:Literal>
                                                                                                            </td>
                                                                                                            <td id="tdlblConfcode" runat="server" class="blackblodtext"
                                                                                                                align="center" valign="middle">
                                                                                                                <asp:Label ID="lblConfCode" runat="server" Text="<%$ Resources:WebResources, ExpressConference_lblConfCode%>"></asp:Label>
                                                                                                            </td>
                                                                                                            <td id="tdlblLeaderPin" runat="server" class="blackblodtext"
                                                                                                                align="center" valign="middle">
                                                                                                                <asp:Label ID="lblLeaderPin" runat="server" Text="<%$ Resources:WebResources, ExpressConference_lblLeaderPin%>"></asp:Label>
                                                                                                            </td>
                                                                                                             <td id="tdlblPartyCode" runat="server" class="blackblodtext"
                                                                                                                align="center" valign="middle">
                                                                                                                <asp:Label ID="lblPartyCode" runat="server" Text="<%$ Resources:WebResources, ParticipantCode%>"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:TextBox ID="txtAudioDialNo" onblur="javascript:clearError(this,'lblError2')"
                                                                                                                    CssClass="altText" Width="101px" runat="server" ></asp:TextBox>
                                                                                                            </td>
                                                                                                            <td id="tdConfCode" runat="server">
                                                                                                                <asp:TextBox ID="txtConfCode" class="altText" onblur="javascript:clearError(this,'lblError3')"
                                                                                                                    runat="server" Width="101px" ></asp:TextBox>
                                                                                                            </td>
                                                                                                            <td id="tdLeaderPin" runat="server">
                                                                                                                <asp:TextBox ID="txtLeaderPin" CssClass="altText" onblur="javascript:clearError(this,'lblError4')"
                                                                                                                    runat="server" Width="101px" ></asp:TextBox>
                                                                                                            </td>
                                                                                                             <td id="tdPartyCode" runat="server">
                                                                                                                <asp:TextBox ID="txtPartyCode" CssClass="altText" onblur="javascript:clearError(this,'lblError4')"
                                                                                                                    runat="server" Width="101px"  ></asp:TextBox>

                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:RegularExpressionValidator ID="RegtxtAudioDialNo" ControlToValidate="txtAudioDialNo"
                                                                                                                    Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, AudioCodeInvalidCharacter%>"
                                                                                                                    ValidationExpression="^[^<>&+]*$" ValidationGroup="Submit"></asp:RegularExpressionValidator><%--@@@@@--%>
                                                                                                                <br />
                                                                                                                <label id="lblError2" runat="server" style="font-weight: normal; color: Red"><%--FB 2592--%>
                                                                                                                </label>
                                                                                                            </td>
                                                                                                            <td id="tdRegConfCode" runat="server">
                                                                                                                <asp:RegularExpressionValidator ID="RegtxtConfCode" ControlToValidate="txtConfCode"
                                                                                                                    Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, AudioCodeInvalidCharacter%>"
                                                                                                                    ValidationExpression="^[^<>&+]*$" ValidationGroup="Submit"></asp:RegularExpressionValidator><%--@@@@@--%> <%--ZD 103540--%>
                                                                                                                <br />
                                                                                                                <label id="lblError3" runat="server" style="font-weight: normal; color: Red"><%--FB 2592--%>
                                                                                                                </label>
                                                                                                            </td>
                                                                                                            <td id="tdRegLeaderPin" runat="server">
                                                                                                                <asp:RegularExpressionValidator ID="RegtxtLeaderPin" ControlToValidate="txtLeaderPin"
                                                                                                                    Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, AudioCodeInvalidCharacter%>"
                                                                                                                    ValidationExpression="^[^<>&+]*$" ValidationGroup="Submit"></asp:RegularExpressionValidator><%--@@@@@--%> <%--ZD 103540--%>
                                                                                                                <br />
                                                                                                                <label id="lblError4" runat="server" style="font-weight: normal; color: Red"><%--FB 2592--%>
                                                                                                                </label>
                                                                                                            </td>
                                                                                                            <td id="tdRegPartyCode" runat="server">
                                                                                                                <asp:RegularExpressionValidator ID="RegtxtPartyCode" ControlToValidate="txtPartyCode"
                                                                                                                    Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, AudioCodeInvalidCharacter%>"
                                                                                                                    ValidationExpression="^[^<>&+]*$" ValidationGroup="Submit"></asp:RegularExpressionValidator><%--@@@@@--%> <%--ZD 103540--%>
                                                                                                                <br />
                                                                                                                <label id="lblErrorParty" runat="server" style="font-weight: normal; color: Red"><%--FB 2592--%>
                                                                                                                </label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                    </ContentTemplate>
                                                                                                    </asp:UpdatePanel>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="display: none;">
                                                                        <td>
                                                                            <img src="image/DoubleLine.jpg" alt="DoubleLine" height="6px" width="100%" /> <%--ZD 100419--%>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                              </div>
                                                            </td>
                                                        </tr>
                                                    </table>

                                                    <asp:TextBox ID="ImageFiles" runat="server" Text=""></asp:TextBox>
                                                    <asp:TextBox ID="ImageFilesBT" runat="server" Text=""></asp:TextBox>
                                                    <asp:TextBox ID="ImagesPath" runat="server" Text=""></asp:TextBox>
                                                    <input id="hdnFamilyLayout" type="hidden" runat="server" value="0" /><%--ZD 101869--%>
													<%--ZD 102057--%>
                                                    <input id="hdnLOSelection" type="hidden" runat="server" value ="0"  name="hdnLOSelection"/> <%--ZD 102057--%>
                                                    <asp:TextBox ID="txtSelectedImage" runat="server" Text="-1"></asp:TextBox> <%--FB 2524--%>
                                                    <script language="javascript" type="text/javascript">
                                                        if (document.getElementById("<%=imgVideoDisplay.ClientID %>") != null) //ZD 104256
                                                        {
                                                            document.getElementById("<%=imgVideoDisplay.ClientID %>").src = document.getElementById("<%=ImagesPath.ClientID %>").value + document.getElementById("<%=txtSelectedImage.ClientID %>").value + ".gif";
                                                            var id = document.getElementById("<%=txtSelectedImage.ClientID %>").value;
                                                            document.getElementById("hdnFamilyLayout").value = 1;
                                                            if (id == 101) {
                                                                document.getElementById("imgVideoDisplay").style.display = '';
                                                                document.getElementById("imgVideoDisplay").src = "image/displaylayout/" + "05" + ".gif";
                                                                document.getElementById("imgLayoutMapping6").style.display = '';
                                                                document.getElementById("imgLayoutMapping6").src = "image/displaylayout/" + "06" + ".gif";
                                                                document.getElementById("imgLayoutMapping7").style.display = '';
                                                                document.getElementById("imgLayoutMapping7").src = "image/displaylayout/" + "07" + ".gif";
                                                                document.getElementById("imgLayoutMapping8").style.display = '';
                                                                document.getElementById("imgLayoutMapping8").src = "image/displaylayout/" + "44" + ".gif";
                                                                document.getElementById("lblCodianLO").innerHTML = Family1;
                                                            }
                                                            else if (id == 102) {
                                                                document.getElementById("imgVideoDisplay").style.display = '';
                                                                document.getElementById("imgVideoDisplay").src = "image/displaylayout/" + "01" + ".gif";
                                                                document.getElementById("imgLayoutMapping6").style.display = 'none';
                                                                document.getElementById("imgLayoutMapping7").style.display = 'none';
                                                                document.getElementById("imgLayoutMapping8").style.display = 'none';
                                                                document.getElementById("lblCodianLO").innerHTML = Family2;
                                                            }
                                                            else if (id == 103) {
                                                                document.getElementById("imgVideoDisplay").style.display = '';
                                                                document.getElementById("imgVideoDisplay").src = "image/displaylayout/" + "02" + ".gif";
                                                                document.getElementById("imgLayoutMapping6").style.display = 'none';
                                                                document.getElementById("imgLayoutMapping7").style.display = 'none';
                                                                document.getElementById("imgLayoutMapping8").style.display = 'none';
                                                                document.getElementById("lblCodianLO").innerHTML = Family3;
                                                            }
                                                            else if (id == 104) {
                                                                document.getElementById("imgVideoDisplay").style.display = '';
                                                                document.getElementById("imgVideoDisplay").src = "image/displaylayout/" + "02" + ".gif";
                                                                document.getElementById("imgLayoutMapping6").style.display = '';
                                                                document.getElementById("imgLayoutMapping6").src = "image/displaylayout/" + "03" + ".gif";
                                                                document.getElementById("imgLayoutMapping7").style.display = '';
                                                                document.getElementById("imgLayoutMapping7").src = "image/displaylayout/" + "04" + ".gif";
                                                                document.getElementById("imgLayoutMapping8").style.display = '';
                                                                document.getElementById("imgLayoutMapping8").src = "image/displaylayout/" + "43" + ".gif";
                                                                document.getElementById("lblCodianLO").innerHTML = Family4;
                                                            }
                                                            else if (id == 105) {
                                                                document.getElementById("imgVideoDisplay").style.display = '';
                                                                document.getElementById("imgVideoDisplay").src = "image/displaylayout/" + "25" + ".gif";
                                                                document.getElementById("imgLayoutMapping6").style.display = 'none';
                                                                document.getElementById("imgLayoutMapping7").style.display = 'none';
                                                                document.getElementById("imgLayoutMapping8").style.display = 'none';
                                                                document.getElementById("lblCodianLO").innerHTML = Family5;
                                                            }
                                                            else if (id == 100) {
                                                                document.getElementById("imgVideoDisplay").style.display = 'none';
                                                                document.getElementById("imgLayoutMapping6").style.display = 'none';
                                                                document.getElementById("imgLayoutMapping7").style.display = 'none';
                                                                document.getElementById("imgLayoutMapping8").style.display = 'none';
                                                                document.getElementById("lblCodianLO").innerHTML = Defaultfamily;
                                                            }
                                                            else {
                                                                document.getElementById("imgVideoDisplay").src = "image/displaylayout/" + id + ".gif";
                                                                document.getElementById("imgVideoDisplay").style.display = '';
                                                                document.getElementById("imgLayoutMapping6").style.display = 'none';
                                                                document.getElementById("imgLayoutMapping7").style.display = 'none';
                                                                document.getElementById("imgLayoutMapping8").style.display = 'none';
                                                                document.getElementById("lblCodianLO").innerHTML = "";
                                                                document.getElementById("hdnFamilyLayout").value = 0;
                                                                //ZD 102057
                                                                if (id == "-1") {
                                                                    document.getElementById("imgVideoDisplay").style.display = 'none';
                                                                    document.getElementById("lblCodianLO").innerHTML = "Auto";
                                                                    document.getElementById("<%=imgVideoDisplay.ClientID %>").src = "";
                                                                }
                                                            }
                                                        }
                                                    </script>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                              <%--ZD 100834 - Starts--%>
                                <tr id="trOptions" runat="server">
                                <td colspan="2" align="left">
                                    <table border="0" style="background-color: #2C398D; width: 505px;" cellspacing="1">
                                        <tr style="background-color: #FFFFFF">
                                            <td align="left" style="width: 31%;" valign="top">
                                                    <table style="width: 100%;" border="0" cellpadding="3">
                                                    <tr>
                                                            <td style="width:150px;"><%--FB 2592--%>
                                                            </td>
                                                            <td align="left">
                                                                    <asp:Label ID="Label1" runat="server" Text="<%$ Resources:WebResources, ExpressConference_Options%>" Style="font-size: 10pt;font-weight:bold" />
                                                            </td>
                                                        </tr>
                                                    <tr>
                                                            <td align="left" nowrap>
                                                              &nbsp;<a href="#" onclick="document.getElementById('aFileUp').click();return false;">
                                                                    <asp:Label ID="aFileUp" runat="server" Text="<%$ Resources:WebResources, ExpressConference_aFileUp%>" onclick="CheckFiles();"
                                                                        Style="font-size: 10pt; color: Blue" /></a>
                                                                <asp:Label ID="lblFileList" runat="server" class="blackblodtext" Style="font-size: 8pt;
                                                                    display: none;"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <table id="tblSplinst" runat="server" width="100%" style="margin-top:-20px">
                                                                    <tr id="trsplinst" runat="server">
                                                                        <td class="subtitlexxsblueblodtext">
                                                                            <asp:Literal ID="Literal44" Text="<%$ Resources:WebResources, ExpressConference_SpecialInstruc%>" runat="server"></asp:Literal> <span class="blackblodtext" style="font-size: 8pt">
                                                                            <asp:Literal ID="Literal45" Text="<%$ Resources:WebResources, ExpressConference_Additional%>" runat="server"></asp:Literal> </span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left">
                                                                            <table width="100%" border="0">
                                                                                <tr>
                                                                                    <td align="left"><%--FB 2592--%>
                                                                                        <asp:Table ID="tblSpecial" width="100%" runat="server" CellPadding="3" CellSpacing="2" Visible="true">
                                                                                        </asp:Table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <%--FB 2592 Starts--%>
                                                        <tr>
                                                            <td colspan="2">
                                                                <table width="100%" border="0" cellspacing="0px" cellpadding="0px" > <%--FB 2349--%>
                                                                    <tr>
                                                                        <td align="left">
                                                                            <asp:Table ID="tblCustomAttribute" runat="server" CellPadding="3" width="100%" CellSpacing="2" Visible="true">
                                                                            </asp:Table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    <tr>
                                                            <td colspan="2" class="subtitlexxsblueblodtext"><asp:Literal ID="Literal46" Text="<%$ Resources:WebResources, ExpressConference_AdditionalOpti%>" runat="server"></asp:Literal>
                                                                <table border="0" cellspacing="0px" cellpadding="0px" width="100%"> <%--FB 2349--%>
                                                                    <tr>
                                                                        <td align="left">
                                                                            <table border="0" width="100%">
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:Table ID="tblHost" HorizontalAlign="Left" runat="server" width="100%" CellPadding="3" CellSpacing="2" Visible="true">
                                                                                        </asp:Table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    
                                                                </table>
                                                            </td>
                                                        </tr>

                                                        <tr id="trConcSupport" runat="server"><%--FB 2341--%><%--ZD 100704--%>
                                                            <td colspan="2">
                                                             <div id="divConcSupport" runat="server"><%--FB 2359--%>
                                                                <table id="tblConcSupport" runat="server" width="100%" style="margin-top:-20px">
                                                                    <tr>
                                                                        <td class="subtitlexxsblueblodtext"><asp:Literal ID="Literal47" Text="<%$ Resources:WebResources, ExpressConference_ConferenceSupp%>" runat="server"></asp:Literal></td><%--FB 3023--%>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left">
                                                                            <table width="100%" border="0">
                                                                                <%--FB 2377 - End--%>
                                                                                <%--FB 2632 - Starts--%>
																					<tr>
                                                                                        <td width="4%">
                                                                                        </td>
                                                                                        <td id="tdOnSiteAVSupport" runat="server" align="left"><%--FB 2670--%>
                                                                                            <input id="chkOnSiteAVSupport" type="checkbox" runat="server" />
                                                                                            <strong style="align: left; color:Black"><asp:Literal ID="Literal48" Text="<%$ Resources:WebResources, ExpressConference_OnSiteAVSuppo%>" runat="server"></asp:Literal></strong> <%-- FB 2827 --%>
                                                                                        </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                        <td width="4%">
                                                                                        </td>
                                                                                        <td id="tdConciergeMonitoring" runat="server" align="left"><%--FB 2670--%>
                                                                                            <input id="chkConciergeMonitoring" type="checkbox" runat="server" />
                                                                                            <strong style="align: left; color:Black"><asp:Literal ID="Literal49" Text="<%$ Resources:WebResources, ExpressConference_CallMonitoring%>" runat="server"></asp:Literal></strong> <%-- FB 2827 --%> <%--FB 3023--%>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td width="4%">
                                                                                        </td>
                                                                                        <td id="tdMeetandGreet" runat="server" align="left"><%--FB 2670--%>
                                                                                            <input id="chkMeetandGreet" type="checkbox" runat="server" />
                                                                                            <strong style="align: left; color:Black"><asp:Literal ID="Literal50" Text="<%$ Resources:WebResources, ExpressConference_MeetandGreet%>" runat="server"></asp:Literal></strong> <%-- FB 2827 --%>
                                                                                        </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                        <td width="4%">
                                                                                        </td>
                                                                                        <td align="left" id="tdDedicatedVNOC" runat="server">
                                                                                            <input id="chkDedicatedVNOCOperator" type="checkbox" runat="server" />
                                                                                            <strong style="align: left; color:Black"><asp:Literal ID="Literal51" Text="<%$ Resources:WebResources, ExpressConference_DedicatedVNOC%>" runat="server"></asp:Literal></strong> <%-- FB 2827 --%>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <%--FB 2632 - End--%>
                                                                                    
                                                                                    <tr>
                                                                                     <td width="4%">
                                                                                        </td>
                                                                                       <td align="left" >
																				            <asp:TextBox ID="txtVNOCOperator" TextMode="MultiLine" runat="server" CssClass="altText" Enabled="true"></asp:TextBox>
                                                                                            <a id="ancimgVNOC" href="#" onclick="this.childNodes[0].click();return false;"><img id="imgVNOC" alt="<%$ Resources:WebResources, ExpressConference_Edit%>" onclick="javascript:getVNOCEmailList()" src="../image/VNOCedit.gif" runat="server" style="cursor:pointer;" title="<%$ Resources:WebResources, SelectVNOCOperator%>" /></a><%-- FB 2783 --%> <%--ZD 100419--%>
                                                                                            <a id="ancimgdeleteVNOC" href="javascript: deleteVNOC();" onmouseover="window.status='';return true;">
                                                                                            <img border="0" id="imgdeleteVNOC" src="image/btn_delete.gif" title="<%$ Resources:WebResources, ExpressConference_Delete%>" alt="<asp:Literal Text='<%$ Resources:WebResources, ExpressConference_Delete%>' runat='server'></asp:Literal>" width="16" height="16" runat="server" /></a> <%--FB 2798--%>
                                                                                            <asp:TextBox ID="hdnVNOCOperator" runat="server" BackColor="Transparent" BorderColor="White"
                                                                                                BorderStyle="None" Width="0px" ForeColor="Black" style="display:none"></asp:TextBox>                                                      
                                                                                         </td>
                                                                            </tr>
                                                                           
                                                                             </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                              </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                              </tr>
                              <%--ZD 100834 - End--%>
                                <tr id="RecurrenceRow">
                                    <td colspan="2" align="left">
                                        <table border="0" style="background-color: #2C398D; width: 450px;" cellspacing="1">
                                            <tr style="background-color: #FFFFFF">
                                                <td align="left" style="width: 31%;" valign="top">
                                                    <table style="width: 100%;" border="0" cellpadding="3">
                                                        <tr>
                                                            <td colspan="2" valign="top">
                                                                <table style="width: 100%;" border="0" cellpadding="3">
                                                                    <tr>
                                                                        <td valign="top" align="left" colspan="2">
                                                                            <span class="blackblodtext"><asp:Literal ID="Literal52" Text="<%$ Resources:WebResources, ExpressConference_RecurringPatte%>" runat="server"></asp:Literal></span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td valign="top" nowrap style="width: 5%;" align="left" class="blackblodtext"> <%--ZD 103929--%>
                                                                            <table cellpadding="3">
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:RadioButton ID="RecurType_1" Text="<%$ Resources:WebResources, CalendarWorkorder_btnDaily%>" OnClick="javascript:return fnShow('1');" GroupName="RecurType" runat="server" CssClass="blackxxxsboldtext" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:RadioButton ID="RecurType_2" Text="<%$ Resources:WebResources, CalendarWorkorder_btnWeekly%>"  OnClick="javascript:return fnShow('2');" GroupName="RecurType" runat="server" CssClass="blackxxxsboldtext" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:RadioButton ID="RecurType_3" Text="<%$ Resources:WebResources, CalendarWorkorder_btnMonthly%>"  OnClick="javascript:return fnShow('3');" GroupName="RecurType" runat="server" CssClass="blackxxxsboldtext" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:RadioButton ID="RecurType_4" Text="<%$ Resources:WebResources, Yearly%>"  OnClick="javascript:return fnShow('4');" GroupName="RecurType" runat="server" CssClass="blackxxxsboldtext" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:RadioButton ID="RecurType_5" Text="<%$ Resources:WebResources, SearchConferenceInputParameters_Custom%>"  OnClick="javascript:return fnShow('5');" GroupName="RecurType" runat="server" CssClass="blackxxxsboldtext" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                               <%-- <asp:RadioButtonList ID="RecurType" runat="server" CssClass="blackblodtext" RepeatDirection="Vertical"
                                                                                    OnClick="javascript:return fnShow();" CellPadding="3">
                                                                                </asp:RadioButtonList>--%>
                                                                        </td>
                                                                        <td align="left" valign="top">
                                                                            <%--Daily Recurring Pattern--%>
                                                                            &nbsp;
                                                                            <asp:Panel ID="Daily" runat="server" HorizontalAlign="left" CssClass="blackblodtext">
                                                                                <asp:RadioButton ID="DEveryDay" runat="server" GroupName="RDaily" CssClass="blackblodtext" /><font
                                                                                    class="blackblodtext"><asp:Literal ID="Literal53" Text="<%$ Resources:WebResources, ExpressConference_Every%>" runat="server"></asp:Literal></font>
                                                                                <asp:TextBox ID="DayGap" CssClass="altText" runat="server" Width="8%" onClick="javaScript: DEveryDay.checked = true;"
                                                                                    onChange="javaScript: summarydaily();"></asp:TextBox>
                                                                                <font class="blackblodtext"><asp:Literal ID="Literal54" Text="<%$ Resources:WebResources, ExpressConference_days%>" runat="server"></asp:Literal></font>
                                                                                <br />
                                                                                <asp:RadioButton ID="DWeekDay" CssClass="blackblodtext" runat="server" GroupName="RDaily"
                                                                                    onClick="javaScript: DayGap.value=''; summarydaily();" /><font class="blackblodtext">
                                                                                    <asp:Literal ID="Literal55" Text="<%$ Resources:WebResources, ExpressConference_EveryWeekday%>" runat="server"></asp:Literal></font>
                                                                            </asp:Panel>
                                                                            <%--Weekly Recurring Pattern--%>
                                                                            <asp:Panel ID="Weekly" runat="server" nowrap Width="360">
                                                                                <font class="blackblodtext"><asp:Literal ID="Literal56" Text="<%$ Resources:WebResources, ExpressConference_RecurEvery%>" runat="server"></asp:Literal> </font>
                                                                                <asp:TextBox ID="WeekGap" runat="server" CssClass="altText" Width="8%"></asp:TextBox>
                                                                                <font class="blackblodtext"><asp:Literal ID="Literal57" Text="<%$ Resources:WebResources, ExpressConference_weekson%>" runat="server"></asp:Literal></font>
                                                                                <asp:CheckBoxList ID="WeekDay" runat="server" CssClass="blackblodtext" RepeatDirection="horizontal"
                                                                                    RepeatColumns="4" CellPadding="2" CellSpacing="3" onClick="javaScript: summaryweekly();">
                                                                                </asp:CheckBoxList>
                                                                            </asp:Panel>
                                                                            <%--Monthly Recurring Pattern--%>
                                                                            <asp:Panel ID="Monthly" runat="server" nowrap Width="370">
                                                                                <asp:RadioButton ID="MEveryMthR1" runat="server" GroupName="GMonthly" onClick="javaScript: MonthGap2.value = ''; summarymonthly();" />
                                                                                <font class="blackblodtext"><asp:Literal ID="Literal58" Text="<%$ Resources:WebResources, ExpressConference_Day%>" runat="server"></asp:Literal></font>
                                                                                <asp:TextBox ID="MonthDayNo" runat="server" CssClass="altText" Width="8%" onClick="javaScript: MEveryMthR1.checked = true;"
                                                                                    onChange="javaScript: summarymonthly();" class="altText"></asp:TextBox>
                                                                                <font class="blackblodtext"><asp:Literal ID="Literal59" Text="<%$ Resources:WebResources, ExpressConference_ofevery%>" runat="server"></asp:Literal></font>
                                                                                <asp:TextBox ID="MonthGap1" runat="server" CssClass="altText" Width="20px" onClick="javaScript: MEveryMthR1.checked = true;"
                                                                                    onChange="javaScript: summarymonthly();"></asp:TextBox>
                                                                                <font class="blackblodtext"><asp:Literal ID="Literal60" Text="<%$ Resources:WebResources, ExpressConference_months%>" runat="server"></asp:Literal></font>
                                                                                <br />
                                                                                <br />
                                                                                <asp:RadioButton ID="MEveryMthR2" runat="server" GroupName="GMonthly" onClick="javaScript: summarymonthly(); MonthDayNo.value = ''; MonthGap1.value = '';" />
                                                                                <font class="blackblodtext"><asp:Literal ID="Literal61" Text="<%$ Resources:WebResources, ExpressConference_The%>" runat="server"></asp:Literal></font>
                                                                                <asp:DropDownList CssClass="altText" runat="server" ID="MonthWeekDayNo" onClick="javaScript: summarymonthly();">
                                                                                </asp:DropDownList>
                                                                                <asp:DropDownList CssClass="altText" runat="server" ID="MonthWeekDay" onClick="javaScript: summarymonthly();">
                                                                                </asp:DropDownList>
                                                                                <font class="blackblodtext"><asp:Literal ID="Literal62" Text="<%$ Resources:WebResources, ExpressConference_ofevery%>" runat="server"></asp:Literal></font>
                                                                                <asp:TextBox ID="MonthGap2" runat="server" CssClass="altText" Width="20px" onClick="javaScript: MEveryMthR2.checked = true;"
                                                                                    onChange="javaScript: summarymonthly();"></asp:TextBox>
                                                                                <font class="blackblodtext"><asp:Literal ID="Literal63" Text="<%$ Resources:WebResources, ExpressConference_months%>" runat="server"></asp:Literal></font></asp:Panel>
                                                                            <%--Yearly Recurring Pattern--%>
                                                                            <asp:Panel ID="Yearly" runat="server" nowrap Width="360">
                                                                                <asp:RadioButton ID="YEveryYr1" runat="server" GroupName="GYearly" onClick="javaScript: summaryyearly();" />
                                                                                <font class="blackblodtext"><asp:Literal ID="Literal64" Text="<%$ Resources:WebResources, ExpressConference_Every%>" runat="server"></asp:Literal></font>
                                                                                <asp:DropDownList CssClass="altText" runat="server" ID="YearMonth1" onClick="javaScript: summaryyearly();">
                                                                                </asp:DropDownList>
                                                                                <asp:TextBox ID="YearMonthDay" runat="server" CssClass="altText" Width="8%" onChange="javaScript: summaryyearly();"
                                                                                    onClick="YEveryYr1.checked = true;"></asp:TextBox>
                                                                                <br />
                                                                                <br />
                                                                                <asp:RadioButton ID="YEveryYr2" runat="server" GroupName="GYearly" onClick="javaScript: summaryyearly(); document.frmSettings2.YearMonthDay.value = '';" />
                                                                                <font class="blackblodtext"><asp:Literal ID="Literal65" Text="<%$ Resources:WebResources, ExpressConference_The%>" runat="server"></asp:Literal> </font>
                                                                                <asp:DropDownList CssClass="altText" runat="server" ID="YearMonthWeekDayNo" onClick="javaScript: summaryyearly();">
                                                                                </asp:DropDownList>
                                                                                <asp:DropDownList CssClass="altText" runat="server" ID="YearMonthWeekDay" onClick="javaScript: summaryyearly();">
                                                                                </asp:DropDownList>
                                                                                <font class="blackblodtext"><asp:Literal ID="Literal66" Text="<%$ Resources:WebResources, ExpressConference_of%>" runat="server"></asp:Literal> </font>
                                                                                <asp:DropDownList CssClass="altText" runat="server" ID="YearMonth2" onClick="javaScript: summaryyearly();">
                                                                                </asp:DropDownList>
                                                                            </asp:Panel>
                                                                            <asp:Panel ID="Custom" runat="server">
                                                                                <table border="0">
                                                                                    <tr>
                                                                                        <td style="width: 30%">
                                                                                            <a href="#" class="name" onclick="return false;"><div id="flatCalendarDisplay" style="float: left; clear: both;"><%--ZD 100639--%>
                                                                                            </div></a>
                                                                                            <br />
                                                                                            <div id="Div2" style="font-size: 80%; text-align: center; padding: 2px">
                                                                                            </div>
                                                                                        </td>
                                                                                        <td style="width: 25%" align="center">
                                                                                            <span class="blackblodtext"><asp:Literal ID="Literal67" Text="<%$ Resources:WebResources, ExpressConference_SelectedDate%>" runat="server"></asp:Literal></span><br />
                                                                                            <asp:ListBox runat="server" ID="CustomDate" Rows="8" CssClass="altSmall0SelectFormat"
                                                                                                onChange="JavaScript: removedate(this);"></asp:ListBox>
                                                                                            <br />
                                                                                            <%--<asp:ImageButton ID="btnsortDates" ImageUrl="~/en/image/sort.png" runat="server" Text="Sort"  OnClientClick="javascript:return SortDates();" />--%>
                                                                                            <asp:Button ID="btnsortDates" CssClass="altMedium0BlueButtonFormat" runat="server" Width="80pt"
                                                                                                Text="<%$ Resources:WebResources, ExpressConference_btnsortDates%>" OnClientClick="javascript:return SortDates();" /> <%--FB 3030--%>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                        </td>
                                                                                        <td>
                                                                                            <span><font class="blackblodtext">* <asp:Literal ID="Literal68" Text="<%$ Resources:WebResources, ExpressConference_clickadateto%>" runat="server"></asp:Literal>
                                                                                             <font class="blackblodtext"></span>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </asp:Panel>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr id="RangeRow" runat="server">
                                                            <td colspan="2">
                                                                <table border="0" width="100%" cellpadding="5">
                                                                    <tr>
                                                                        <td colspan="2">
                                                                            <span class="blackblodtext"><asp:Literal ID="Literal69" Text="<%$ Resources:WebResources, ExpressConference_RangeofRecurr%>" runat="server"></asp:Literal> </span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr valign="top">
                                                                        <td class="blackblodtext" nowrap style="width: 20%;" align="right">
                                                                            <asp:Literal ID="Literal70" Text="<%$ Resources:WebResources, ExpressConference_Start%>" runat="server"></asp:Literal>
                                                                            <asp:TextBox ID="StartDate" Width="70px" Font-Size="9" CssClass="altText" runat="server"></asp:TextBox>
                                                                            <a href="" onkeydown="if(event.keyCode == 13){if(!isIE){document.getElementById('cal_triggerd1').click();return false;}}" onclick="if(isIE){this.childNodes[0].click();return false;}"><img alt="<asp:Literal Text='<%$ Resources:WebResources, DateSelector%>' runat='server'></asp:Literal>" src="image/calendar.gif" border="0" id="cal_triggerd1" style="cursor: pointer;
                                                                                vertical-align: top;" title="<asp:Literal Text='<%$ Resources:WebResources, DateSelector%>' runat='server'></asp:Literal>" onclick="return showCalendar('<%=StartDate.ClientID%>', 'cal_triggerd1', 1, '<%=format %>');" /></a> <%--ZD 100419--%>
                                                                        </td>
                                                                        <td>
                                                                            <table width="100%" border="0">
                                                                                <tr>
                                                                                    <td class="blackblodtext" colspan="2">
                                                                                        <asp:RadioButton ID="EndType" runat="server" GroupName="RangeGroup" onClick="javascript: document.frmSettings2.Occurrence.value=''; document.frmSettings2.EndDate.value='';" />
                                                                                        <asp:Literal ID="Literal71" Text="<%$ Resources:WebResources, ExpressConference_Noenddate%>" runat="server"></asp:Literal>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="blackblodtext" nowrap style="width: 9%;">
                                                                                        <asp:RadioButton ID="REndAfter" runat="server" GroupName="RangeGroup" onClick="javascript: document.frmSettings2.EndDate.value='';" />
                                                                                        <asp:Literal ID="Literal72" Text="<%$ Resources:WebResources, ConferenceSetup_Endafter%>" runat="server"></asp:Literal>
                                                                                    </td>
                                                                                    <td class="blackblodtext" nowrap>
                                                                                        <asp:TextBox ID="Occurrence" CssClass="altText" Width="70px" runat="server" onClick="javascript: document.frmSettings2.REndAfter.checked=true; document.frmSettings2.EndDate.value='';"></asp:TextBox>
                                                                                        <asp:Literal ID="Literal73" Text="<%$ Resources:WebResources, ExpressConference_occurrences%>" runat="server"></asp:Literal>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="blackblodtext">
                                                                                        <asp:RadioButton ID="REndBy" runat="server" GroupName="RangeGroup" onClick="javascript: document.frmSettings2.Occurrence.value='';" />
                                                                                        <asp:Literal ID="Literal74" Text="<%$ Resources:WebResources, ExpressConference_Endby%>" runat="server"></asp:Literal>
                                                                                    </td>
                                                                                    <td nowrap>
                                                                                        <asp:TextBox ID="EndDate" Width="70px" onblur="javascript:CheckDate(this)" onchange="javascript:CheckDate(this)"
                                                                                            CssClass="altText" runat="server" onClick="javascript: document.frmSettings2.REndBy.checked=true; document.frmSettings2.Occurrence.value='';"></asp:TextBox>
                                                                                        <a href="" onkeydown="if(event.keyCode == 13){if(!isIE){document.getElementById('cal_trigger2').click();return false;}}" onclick="if(isIE){this.childNodes[0].click();return false;}"><img alt="<asp:Literal Text='<%$ Resources:WebResources, DateSelector%>' runat='server'></asp:Literal>" src="image/calendar.gif" border="0" id="cal_trigger2" style="cursor: pointer;
                                                                                            vertical-align: top;" title="<asp:Literal Text='<%$ Resources:WebResources, DateSelector%>' runat='server'></asp:Literal>" onclick="return showCalendar('<%=EndDate.ClientID%>', 'cal_trigger2', 1, '<%=format %>');" /></a> <%--ZD 100419--%>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2" class="blackblodtext" style="font-size: 8pt;">
                                                                            <asp:Literal ID="Literal75" Text="<%$ Resources:WebResources, recurNET_NoteMaximumli%>" runat="server"></asp:Literal><%--ZD 101837--%>
                                                                             <span id="spnRecurLimit"> <%=Session["ConfRecurLimit"]%> </span> <asp:Literal Text="<%$ Resources:WebResources, recurNET_NoteMaximumli1%>" runat="server"></asp:Literal>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <%--Below script is to hide Recurrence Div during page load--%>

                <script language="javascript" type="text/javascript">    
                	//FB 2634
                    document.getElementById("regStartTime").controltovalidate = "confStartTime_Text";
                    document.getElementById("reqStartTime").controltovalidate = "confStartTime_Text";
                    document.getElementById("regEndTime").controltovalidate = "confEndTime_Text";
                    document.getElementById("reqEndTime").controltovalidate = "confEndTime_Text";
//                    document.getElementById("regSetupStartTime").controltovalidate = "SetupTime_Text"; 
//                    document.getElementById("reqSetupStartTime").controltovalidate = "SetupTime_Text";
//                    document.getElementById("regTearDownStartDate").controltovalidate = "TeardownTime_Text";
                                
                    if (document.getElementById("<%=Recur.ClientID %>").value != "" ) 
                    {
                        isRecur();
                        AnalyseRecurStr(document.getElementById("<%=Recur.ClientID %>").value);
                        st = calStart(atint[1], atint[2], atint[3]);
                        et = calEnd(st, parseInt(atint[4], 10));
                        document.getElementById("RecurringText").value =  recur_discription(document.getElementById("<%=Recur.ClientID %>").value, et, "Eastern Standard Time", Date(),"<%=Session["timeFormat"].ToString()%>","<%=Session["timezoneDisplay"].ToString()%>");
                    }
                    isRecur();
                    ChangeImmediate();
                    ChangePublic();//FB 2226
                    openAdvancesetting();

                    if ("<%=isInstanceEdit%>" == "Y" )
                    {  
                        document.getElementById("NONRecurringConferenceDiv8").style.display = "none";
                    }
                    
                    if ("<%=timeZone%>" == "0" ) 
                        document.getElementById("NONRecurringConferenceDiv3").style.display = "none";
                             //FB 2634                  
                    if ("<%=client.ToString().ToUpper() %>" == "MOJ")
                    {
                        document.getElementById("StartNowRow").style.display = "none";//FB 2341
                        document.getElementById("trPublic").style.display = "none"; 
                        document.getElementById("trConfType").style.display = "none"; 
                        document.getElementById("ConfStartRow").style.display =  "none"; 
                        document.getElementById("ConfEndRow").style.display =  "none"; 
                        
                    }
                    // FB 2359
                   

                </script>

            </td>
        </tr>
        <tr>
            <td width="100%">
                <table width="100%" border="0">
                <tr style="height:38; width:100%;" valign="middle" class="Bodybackgorund">
                    <td colspan="2" style="width: 100%;">
                        <span style="width: 100%;" class="ExpressHeadingsNumbers">&nbsp;&nbsp;&nbsp;2&nbsp;&nbsp;&nbsp;</span><span
                            class="ExpressHeadings">&nbsp;&nbsp;&nbsp;<asp:Literal ID="Literal76" Text="<%$ Resources:WebResources, ExpressConference_Rooms%>" runat="server"></asp:Literal></span>
                    </td>
                </tr>
                <tr>
                    <td width="50%" valign="top">
                        <table width="100%">
                            <tr>
                                <td align="left">
                                    <asp:UpdatePanel ID="UpdatePanelSelectedRooms" runat="server" RenderMode="Inline"
                                        UpdateMode="Conditional">
                                        <Triggers>
                                        </Triggers>
                                        <ContentTemplate>
                                            <asp:ListBox ID="RoomList" runat="server" Height="150" Width="100%"
                                                CssClass="altText2" SelectionMode="multiple"></asp:ListBox><%--FB 2367--%><%--ZD 100238--%>
                                            <br />
                                            <asp:Button ID="btnRoomSelect" runat="server" Style="display: none" OnClick="btnRoomSelect_Click"
                                                CssClass="dnButtonLong"></asp:Button>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="50%" valign="top">
                        <%-- FB 2525 Starts --%>
                        <div>
                        <asp:UpdatePanel ID="updtguest" runat="server" UpdateMode="Conditional" >
                        <ContentTemplate>
                        <input type="hidden" runat="server" id="hdnGuestRoom" /><%--FB 2426--%>
	                    <input type="hidden" runat="server" id="hdnGuestRoomID" /><%--FB 2426--%>
	                    <input type="hidden" runat="server" id="hdnGuestloc" /><%--FB 2426--%>
                        <input type="hidden" value="0" id="hdnGusetLoc" runat="server" /><%-- ZD 101057--%>
                        <input type="hidden" runat="server" id="hdnROWID" name="hdnROWID" /> <%--ZD 100619--%>
                        <table width="100%">
                            <%-- FB 2426 Starts --%>
                            <tr id="OnFlyRowGuestRoom" runat="server" >
                                <td style="text-align: left" >
                                <div id="divIdRoomList" style="height:150; overflow-y:scroll; overflow-x:hidden; border-left-style:solid; border-left-color:#aaaaaa; border-left-width:thick;" ><%--FB 2448--%>
                                    <asp:DataGrid ID="dgOnflyGuestRoomlist" EnableViewState="true" ShowHeader="false"  runat="server" AutoGenerateColumns="False"
                                        CellPadding="4" GridLines="None" OnEditCommand="dgOnflyGuestRoomlist_Edit" OnDeleteCommand="dgOnflyGuestRoomlist_DeleteCommand"
                                        Width="100%" AllowSorting="True" Style="border-collapse: separate"> <%--ZD 100922--%>
                                        <FooterStyle CssClass="tableBody" Font-Bold="True" />
                                        <Columns>
                                            <asp:BoundColumn DataField="RowUID" Visible="false"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="RoomID" Visible="false"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="RoomName" ItemStyle-CssClass="tableBody" ItemStyle-BackColor="White" ItemStyle-Width="300px" ></asp:BoundColumn><%--ZD 100238--%>
                                            <asp:BoundColumn DataField="ContactName" ItemStyle-CssClass="tableBody" HeaderText="<%$ Resources:WebResources, ExpressConference_GuestRoomIncharge%>"
                                                Visible="false" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="ContactEmail" Visible="false"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="ContactPhoneNo" Visible="false"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="ContactAddress" Visible="false"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="State" Visible="false"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="City" Visible="false"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="ZIP" Visible="false"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="Country" Visible="false"></asp:BoundColumn>
                                            <asp:TemplateColumn ItemStyle-CssClass="tableBody"  ItemStyle-BackColor="White" >
                                                <ItemTemplate><%-- FB 2592 --%>
                                                    <asp:Button ID="btnGuestRoomEdit" CausesValidation="false" CssClass="altLongblueButtonFormat" CommandName="Edit" runat="server" Text="<%$ Resources:WebResources, ExpressConference_Edit%>" />
                                                    <asp:Button ID="btnGuestRoomDelete" CausesValidation="false" CssClass="altLongblueButtonFormat" CommandName="Delete" runat="server" Text="<%$ Resources:WebResources, ExpressConference_Delete%>" />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn DataField="GuestContactPhoneNo" Visible="false"></asp:BoundColumn> <%--ZD 100619--%>
                                            <asp:BoundColumn DataField="DftConnecttype" Visible="false"></asp:BoundColumn> <%--ZD 100619--%>
                                            <asp:BoundColumn DataField="GuestAdmin" Visible="false"></asp:BoundColumn> <%--ZD 100619--%> 
                                            <asp:BoundColumn DataField="GuestAdminID" Visible="false"></asp:BoundColumn> <%--ZD 100619--%> 
                                        </Columns>
                                        <SelectedItemStyle CssClass="tableBody" Font-Bold="True" />
                                        <EditItemStyle CssClass="tableBody" />
                                        <AlternatingItemStyle CssClass="tableBody" />
                                        <ItemStyle CssClass="tableBody" />
                                        <HeaderStyle CssClass="tableBody" Font-Bold="True" />
                                        <PagerStyle CssClass="tableBody" HorizontalAlign="Center" />
                                    </asp:DataGrid>
                                </div>
                                </td>
                            </tr>
                            <tr>
                                <td >
                                    <div id="divGuestPopup" style="display:none" > <%-- ZD 103573 --%>
                                    <ajax:ModalPopupExtender ID="guestLocationPopup" runat="server" TargetControlID="btnGuestLocation2"
                                        PopupControlID="PopupLdapPanel" DropShadow="false" Drag="true" BackgroundCssClass="modalBackground"
                                         BehaviorID="btnGuestLocation2" >
                                    </ajax:ModalPopupExtender>
                                    <asp:Panel ID="PopupLdapPanel" runat="server" Width="98%" Height="98%" HorizontalAlign="Center"
                                        CssClass="treeSelectedNode" ScrollBars="Vertical">
                                        <asp:UpdatePanel ID="updtGuestLocation" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true"  >
                                                    <ContentTemplate>
                                                    <input type="hidden" id="expColStat" runat="server" value="0" />
                                        <div>
                                        <table align="center" cellpadding="3" cellspacing="0" width="98%" style="border-collapse: collapse;
                                            height: 100%;">
                                            <tr>
                                                <td align="center">
                                                    <table width="100%" border="0" cellpadding="3" style="border-collapse: collapse;
                                                        height: 100%;">
                                                        <tr>
                                                            <td colspan="6" style="padding-bottom:20px">
                                                                <h3>
                                                                    <asp:Literal ID="Literal77" Text="<%$ Resources:WebResources, ExpressConference_VideoGuestLoc%>" runat="server"></asp:Literal></h3>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td width="50%">        
                                                                                                                                 
                                                                <table border="0" style="margin-bottom:20px">
                                                                <tr>
                                                                        <td style="width: 1%;">
                                                                        </td>
                                                                        <td align="left" class="blackblodtext" style="width:10%">
                                                                            <asp:Literal ID="Literal78" Text="<%$ Resources:WebResources, ExpressConference_ConnectionType%>" runat="server"></asp:Literal> 
                                                                        </td>
                                                                        <td align="left" style="width:11%">
                                                                                <asp:DropDownList ID="lstGuestConnectionType" Width="187px" CssClass="altSelectFormat"
                                                                                runat="server" DataTextField="Name" DataValueField="ID" OnSelectedIndexChanged="GuestDftConnctType" AutoPostBack="true" >
                                                                            </asp:DropDownList><br />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 1%;">
                                                                        </td>

                                                                        <td align="left" nowrap="nowrap" class="blackblodtext">
                                                                            <asp:Literal ID="Literal79" Text="<%$ Resources:WebResources, ExpressConference_GuestSiteName%>" runat="server"></asp:Literal> <span class="reqfldstarText">*</span>
                                                                        </td>
                                                                        <td align="left">

                                                                            <asp:TextBox ID="txtsiteName" runat="server" CssClass="altText" MaxLength="20" Width="187px" /><br /><%--FB 2523--%><%--FB 2995--%>
                                                                                
                                                                            <asp:RequiredFieldValidator ID="reqRoomName" Enabled="false" runat="server" ControlToValidate="txtsiteName"
                                                                            Display="dynamic" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, ExpressConference_RequiredField%>" ValidationGroup="SubmitG"></asp:RequiredFieldValidator><%--ALLBUGS-111--%>
                                                                                
                                                                            <asp:RegularExpressionValidator ID="regRoomName" Enabled="false" ControlToValidate="txtsiteName"
                                                                            Display="dynamic" runat="server" SetFocusOnError="true" ValidationGroup="SubmitG"
                                                                            ErrorMessage="<%$ Resources:WebResources, InvalidCharacters35%>"
                                                                            ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+?|!`\[\]{}\=@$%&~]*$"></asp:RegularExpressionValidator><%-- ZD 103424 ZD 104000--%> <%--ALLBUGS-111--%>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                        <td align="left" class="blackblodtext" nowrap="nowrap">
                                                                            <asp:Literal ID="Literal80" Text="<%$ Resources:WebResources, ExpressConference_ContactName%>" runat="server"></asp:Literal> <span class="reqfldstarText">*</span>
                                                                        </td>
                                                                        <td align="left" nowrap="nowrap">
                                                                            <asp:TextBox ID="txtApprover5" runat="server" CssClass="altText" Width="187px" />
                                                                            <a href="" onclick="this.childNodes[0].click();return false;"><img id="Img10" onClick="javascript:getYourOwnEmailList(4,0)" alt="<asp:Literal Text='<%$ Resources:WebResources, ExpressConference_Edit%>' runat='server'></asp:Literal>" src="image/edit.gif" style="cursor:pointer;" title="<asp:Literal Text='<%$ Resources:WebResources, ExpressConference_AddressBook%>' runat='server'></asp:Literal>" /></a>
                                                                                <asp:TextBox ID="hdnApprover5" runat="server" BackColor="Transparent" BorderColor="White"
                                                                                BorderStyle="None" Width="0px" ForeColor="Black" Style="display: none"></asp:TextBox>
                                                                            <a href="javascript:deleteAssistant();"  onclick="javascript:document.getElementById('hdnSaveData').value = '1';" onmouseover="window.status='';return true;"><%--ZD 100875--%>
                                                                                <img border="0" src="image/btn_delete.gif" alt="<asp:Literal Text='<%$ Resources:WebResources, ExpressConference_Delete%>' runat='server'></asp:Literal>" width="16" height="16" style="cursor:pointer;" title="<asp:Literal Text='<%$ Resources:WebResources, ExpressConference_Delete%>' runat='server'></asp:Literal>" /></a><br /> <%--FB 2798--%>
                                                                            <asp:RequiredFieldValidator ID="reqcontactName" Enabled="false" runat="server" ControlToValidate="txtApprover5" 
                                                                                Display="dynamic" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, ExpressConference_RequiredField%>" ValidationGroup="SubmitG"></asp:RequiredFieldValidator><%--ALLBUGS-111--%>
                                                                        </td> 
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                        <td align="left" class="blackblodtext" nowrap="nowrap">
                                                                            <asp:Literal ID="Literal81" Text="<%$ Resources:WebResources, ExpressConference_ContactEmail%>" runat="server"></asp:Literal> <span class="reqfldstarText">*</span><%--ZD 101790--%>
                                                                        </td>
                                                                        <td align="left">
                                                                            <asp:TextBox ID="txtEmailId" runat="server" CssClass="altText" Width="187px" /><br />
                                                                            <asp:RequiredFieldValidator ID="reqcontactEmail"  Enabled="false" runat="server" ControlToValidate="txtEmailId" SetFocusOnError="true"
                                                                                ValidationGroup="SubmitG" ErrorMessage="<%$ Resources:WebResources, ExpressConference_RequiredField%>" Display="dynamic"></asp:RequiredFieldValidator><%--ALLBUGS-111--%>
                                                                            <asp:RegularExpressionValidator ID="regTestemail" runat="server" ControlToValidate="txtEmailId"
                                                                                ErrorMessage="<%$ Resources:WebResources, InvalidEmailAddress%>" Display="dynamic"  ValidationExpression="^[a-zA-Z()^][()^\w\.-]*[a-zA-Z0-9()^]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"></asp:RegularExpressionValidator>
                                                                            <asp:RegularExpressionValidator ID="regcontactEmail" ControlToValidate="txtEmailId"
                                                                                Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters%>"
                                                                                ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=:#$%&'~]*$"></asp:RegularExpressionValidator>
                                                                                <asp:TextBox ID="hdnGusetAdmin" runat="server" BackColor="Transparent" BorderColor="White"
                                                                                BorderStyle="None" Width="0px" ForeColor="Black" Style="display: none"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                        <td align="left" class="blackblodtext" nowrap="nowrap">
                                                                            <asp:Literal ID="Literal82" Text="<%$ Resources:WebResources, ExpressConference_ContactPhone%>" runat="server"></asp:Literal>
                                                                        </td>
                                                                        <td align="left">
                                                                            <asp:TextBox ID="txtContactPhone" runat="server" CssClass="altText" Width="187px" />
                                                                                        
                                                                        </td>
                                                                    </tr>
                                                                        <tr>                                                                                
                                                                        <td>
                                                                        </td>
                                                                        <td colspan="2" align="left" >
                                                                            <br /><br />
                                                                            <span class="blackblodtext"><asp:Literal ID="Literal83" Text="<%$ Resources:WebResources, Profiles%>" runat="server"></asp:Literal></span> 
                                                                            <asp:ImageButton ID="imgRoomList" ImageUrl="image/info.png" AlternateText="<%$ Resources:WebResources, ExpressConference_Info%>" runat="server" style="cursor:default" ToolTip="<%$ Resources:WebResources, MaxProfiles%>"  /><br /><%-- ZD 102590 --%>
                                                                            <span class="blackblodtext"><asp:Literal ID="Literal84" Text="<%$ Resources:WebResources, ExpressConference_DialingInforma%>" runat="server"></asp:Literal></span> 
                                                                        </td>                                                                                    
                                                                    </tr>
                                                                    </table>
                                                            </td>

                                                            <td width="50%" valign="top">   
                                                                <table border="0">
                                                                    <tr>
                                                                        <td align="left" class="blackblodtext" valign="top" colspan="2" >
                                                                            <span style="vertical-align:top"><asp:Literal ID="Literal85" Text="<%$ Resources:WebResources, ExpressConference_Address%>" runat="server"></asp:Literal></span>
                                                                            <asp:imagebutton alternatetext="Expand/Collapse" cssclass ="0"  id="ImgbtnAddress" runat="server" imageurl="image/loc/nolines_plus.gif" 
                                                                            height="25" width="25" vspace="0" hspace="0" OnClientClick="javascript:return ExpandAll();" /> 
                                                                            <asp:ImageButton ID="imgAddressList" ImageUrl="image/info.png" AlternateText="<%$ Resources:WebResources, ExpressConference_Info%>" runat="server" ToolTip="<%$ Resources:WebResources, MsgAddressList%>" style="cursor:default"  /><%-- ZD 102590 --%>
                                                                        </td> <td style="width: 5%">
                                                                        </td
                                                                                    
                                                                    </tr>
                                                                    </table>                                                                  
                                                                <table id="tblAddress" border="0" style="padding-bottom:15px; visibility:hidden;">                                                                               
                                                                    <tr>
                                                                        <td style="width: 1%">
                                                                        </td>
                                                                        <td align="left" class="blackblodtext" style="width: 10%">
                                                                            <asp:Literal ID="Literal86" Text="<%$ Resources:WebResources, ExpressConference_Address%>" runat="server"></asp:Literal>
                                                                        </td>
                                                                        <td align="left" style="vertical-align: top; width: 31%">
                                                                            <asp:TextBox ID="txtAddress" runat="server" CssClass="altText" TextMode="MultiLine"
                                                                                Width="183px" />
                                                                            <asp:RegularExpressionValidator ID="reqAddress2" ControlToValidate="txtAddress" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters12%>" ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator><%--ZD 100263--%>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                        <td align="left" class="blackblodtext" nowrap="nowrap">
                                                                            <asp:Literal ID="Literal87" Text="<%$ Resources:WebResources, ExpressConference_StateProvince%>" runat="server"></asp:Literal>
                                                                        </td>
                                                                        <td align="left">
                                                                            <asp:TextBox ID="txtState" runat="server" CssClass="altText" Width="187px" />
                                                                            <asp:RegularExpressionValidator ID="reqState" ControlToValidate="txtState" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters12%>" ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator><%--ZD 100263--%>
                                                                        </td>
                                                                                    
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                        <td align="left" class="blackblodtext">
                                                                            <asp:Literal ID="Literal88" Text="<%$ Resources:WebResources, ExpressConference_City%>" runat="server"></asp:Literal>
                                                                        </td>
                                                                        <td align="left">
                                                                            <asp:TextBox ID="txtCity" runat="server" CssClass="altText" Width="187px" />
                                                                            <asp:RegularExpressionValidator ID="reqCity" ControlToValidate="txtCity" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters12%>" ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator><%--ZD 100263--%>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                        <td align="left" class="blackblodtext" nowrap="nowrap">
                                                                            <asp:Literal ID="Literal89" Text="<%$ Resources:WebResources, ExpressConference_PostalCode%>" runat="server"></asp:Literal>
                                                                        </td>
                                                                        <td align="left">
                                                                            <asp:TextBox ID="txtZipcode" runat="server" CssClass="altText" Width="187px" />
                                                                            <asp:RegularExpressionValidator ID="reqZipcode" ControlToValidate="txtZipcode" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters12%>" ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator><%--ZD 100263--%>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                        <td align="left" class="blackblodtext" nowrap="nowrap">
                                                                            <asp:Literal ID="Literal90" Text="<%$ Resources:WebResources, ExpressConference_SiteCountry%>" runat="server"></asp:Literal>
                                                                        </td>
                                                                        <td align="left">
                                                                            <asp:DropDownList ID="lstCountries" CssClass="altText" runat="server" DataTextField="Name"
                                                                                DataValueField="ID" Width="187px">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                        <td align="left" class="blackblodtext" nowrap="nowrap">
                                                                            <asp:Literal ID="Literal91" Text="<%$ Resources:WebResources, ExpressConference_RoomPhone%>" runat="server"></asp:Literal>
                                                                        </td>
                                                                        <td align="left">
                                                                            <asp:TextBox ID="txtPhone" runat="server" CssClass="altText" Width="187px" />
                                                                            <asp:RegularExpressionValidator ID="reqPhone" ControlToValidate="txtPhone" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters12%>" ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator><%--ZD 100263--%>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                            
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="6">
                                                                <asp:DataGrid runat="server" ID="dgProfiles" AutoGenerateColumns="false" EnableViewState="true" OnItemDataBound="InitializeGuest" OnItemCreated="InitializeGuest"
                                                                    CellSpacing="0" CellPadding="4" GridLines="None" BorderColor="#336699" BorderStyle="solid" BorderWidth="1"  Width="100%" style="border-collapse:separate" >
                                                                <SelectedItemStyle CssClass="tableBody" Font-Bold="True" />
                                                                <EditItemStyle CssClass="tableBody" />
                                                                <AlternatingItemStyle CssClass="tableBody" />
                                                                <ItemStyle CssClass="tableBody" />
                                                                <HeaderStyle CssClass="Bodybackgorund" HorizontalAlign="left" />
                                                                <Columns>
                                                                    <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, ProfileNo%>" HeaderStyle-HorizontalAlign="Center">
                                                                        <ItemStyle HorizontalAlign="center" VerticalAlign="Top" />
                                                                        <HeaderStyle BorderColor="#336699"  CssClass="Bodybackgorund" />
                                                                        <ItemTemplate>                          
                                                                            <asp:Label ID="lblProfileCount" Text="" runat="server" Font-Bold="true"></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, Profiles%>" HeaderStyle-HorizontalAlign="Left">
                                                                    <HeaderStyle CssClass="Bodybackgorund" HorizontalAlign="Left" Height="25" />
                                                                        <ItemTemplate>
                                                                        <table width="100%">
                                                                            <tr>                               
                                                                                <td align="left" class="blackblodtext">
                                                                                    <asp:Literal ID="Literal1" Text="<%$ Resources:WebResources, ExpressConference_Address%>" runat="server"></asp:Literal> <span class="reqfldstarText">*</span> </td><%--ZD 101790--%>
                                                                                <td align="left" nowrap="nowrap" style="width:38%">
                                                                                    <asp:TextBox CssClass="altText"  ID="txtGuestAddress" runat="server" Width="187px" Text='<%# DataBinder.Eval(Container, "DataItem.Address") %>' onchange="CheckIPAddress(this, 0)" ></asp:TextBox><%--ZD 101268--%>                                 
                                                                                    <asp:RequiredFieldValidator ID="reqGuestAddress" Enabled="false" runat="server" ControlToValidate="txtGuestAddress"
                                                                                        Display="dynamic" ErrorMessage="<%$ Resources:WebResources, ExpressConference_RequiredField%>" ValidationGroup="SubmitG"></asp:RequiredFieldValidator><%--ALLBUGS-111--%>
                                                                                    <asp:RegularExpressionValidator ID="regIPAddress" Enabled="false" ControlToValidate="txtGuestAddress" ValidationGroup="SubmitG"
                                                                                        Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidIPAddress%>"
                                                                                        ValidationExpression="^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$"></asp:RegularExpressionValidator>
                                                                                </td>
                                                                                <td align="left" class="blackblodtext" nowrap="nowrap">
                                                                                    <asp:Literal ID="Literal92" Text="<%$ Resources:WebResources, ExpressConference_ConnectionType%>" runat="server"></asp:Literal> 
                                                                                </td>
                                                                                <td align="left" style="width:28%">
                                                                                        <asp:DropDownList ID="lstGuestConnectionType1" Width="187px" CssClass="altSelectFormat" SelectedValue='<%# DataBinder.Eval(Container, "DataItem.ConnectionType") %>'
                                                                                            OnSelectedIndexChanged="ChangeMCU"  AutoPostBack="true" runat="server" DataTextField="Name" DataValueField="ID" >
                                                                                    </asp:DropDownList>
                                                                                    <asp:RequiredFieldValidator ID="reqGuestConnectionType" runat="server" InitialValue="-1" ControlToValidate="lstGuestConnectionType1" ValidationGroup="SubmitG" ErrorMessage="<%$ Resources:WebResources, ExpressConference_RequiredField%>" Display="dynamic"></asp:RequiredFieldValidator><%--ALLBUGS-111--%>
                                                                                </td>
                                                                                </tr>
                                                                                <tr>
                                                                                <td align="left" class="blackblodtext">
                                                                                    <asp:Literal ID="Literal93" Text="<%$ Resources:WebResources, ExpressConference_AddressType%>" runat="server"></asp:Literal> <span style="color:Red">*</span></td>
                                                                                <td align="left">
                                                                                    <asp:DropDownList CssClass="altSelectFormat" ID="lstGuestAddressType" runat="server" Width="187px" DataTextField="Name" DataValueField="ID" SelectedValue='<%# DataBinder.Eval(Container, "DataItem.AddressType") %>' onchange="javascript:fnAddresstypechg(this,0)"></asp:DropDownList> <%--ZD 104068--%>
                                                                                    <asp:RequiredFieldValidator ID="reqGuestAddressType" runat="server" InitialValue="-1" ControlToValidate="lstGuestAddressType" ValidationGroup="SubmitG" ErrorMessage="<%$ Resources:WebResources, ExpressConference_RequiredField%>" Display="dynamic"></asp:RequiredFieldValidator>       <%--ALLBUGS-111--%>                                                                              
                                                                                    <input type="hidden" id="hdnGuestAddrTypeChg" runat="server" value="0" /><%--ZD 104068--%>
                                                                                </td>
                                                                                    <td align="left" class="blackblodtext"><asp:Literal ID="Literal94" Text="<%$ Resources:WebResources, ExpressConference_LineRate%>" runat="server"></asp:Literal><span class="reqfldstarText">*</span>
                                                                                    </td>
                                                                                <td align="left">
                                                                                    <asp:DropDownList CssClass="altSelectFormat" ID="lstGuestLineRate" runat="server" Width="187px" DataTextField="LineRateName" DataValueField="LineRateID" SelectedValue='<%# DataBinder.Eval(Container, "DataItem.MaxLineRate") %>'></asp:DropDownList>
                                                                                    <asp:RequiredFieldValidator ID="reqGuestLineRate" runat="server"  ControlToValidate="lstGuestLineRate" ValidationGroup="SubmitG" ErrorMessage="<%$ Resources:WebResources, ExpressConference_RequiredField%>" Display="dynamic"></asp:RequiredFieldValidator>  <%--ALLBUGS-111--%>
                                                                                </td>
                                                                            </tr>
                                                                                <tr>                               
                                                                                <td align="left" class="blackblodtext">
                                                                                    <asp:Literal ID="LblGstEP" Text="<%$ Resources:WebResources, ExpressConference_AssignedtoMCU%>" runat="server"></asp:Literal> </td>
                                                                                <td align="left">
                                                                                    <asp:DropDownList CssClass="altSelectFormat" ID="lstGuestBridges" runat="server" Width="187px" DataTextField="BridgeName" DataValueField="BridgeID" SelectedValue='<%# DataBinder.Eval(Container, "DataItem.BridgeID") %>'  onchange="javascript:__doPostBack(this.id,'');"  OnSelectedIndexChanged="ChangeMCU" ></asp:DropDownList>
                                                                                    <asp:DropDownList CssClass="altSelectFormat" ID="lstTelnetUsers" Visible="false"  runat="server" onChange="javascript:document.getElementById('hdnChgCallerCallee').value = '1';">  <%--ZD 100815--%><%--ZD 103402--%> 
                                                                                         <asp:ListItem Value="0"  Text="<%$ Resources:WebResources, ExpressConference_Callee%>" ></asp:ListItem> 
                                                                                        <asp:ListItem Value="1" Text="<%$ Resources:WebResources, ExpressConference_Caller%>"></asp:ListItem>
                                                                                    </asp:DropDownList>
                                                                                </td>
                                                                                <td style="display:none">
                                                                                <asp:DropDownList CssClass="altSelectFormat" ID="lstBridgeType" runat="server" DataTextField="BridgeType" DataValueField="BridgeID"  SelectedValue='<%# DataBinder.Eval(Container, "DataItem.BridgeID") %>' ></asp:DropDownList> <%--ZD 100393--%>
                                                                                <asp:TextBox CssClass="altText"  ID="txtRowID" runat="server" Visible="false" value='<%# DataBinder.Eval(Container, "DataItem.RowUID") %>' ></asp:TextBox>                                 
                                                                                </td>
                                                                                <td></td>
                                                                                </tr>
                                                                        </table>
                                                                        </ItemTemplate>
                                                                        <FooterTemplate>
                                                                        </FooterTemplate>
                                                                        <FooterStyle BackColor="beige" />
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, Default%>" ItemStyle-VerticalAlign="Top">
                                                                    <HeaderStyle CssClass="Bodybackgorund" />
                                                                        <ItemTemplate>
                                                                            <asp:RadioButton ID="rdDefault" runat="server" GroupName="DefaultProfile" OnCheckedChanged="SetDftProfile" AutoPostBack="true" onclick="javascript:DataLoading(1);" Checked='<%# DataBinder.Eval(Container, "DataItem.DefaultProfile").Equals("1") %>' /></td>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, ExpressConference_Delete%>" ItemStyle-VerticalAlign="Top">
                                                                    <HeaderStyle CssClass="Bodybackgorund" />
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="chkDelete" runat="server" Checked='<%# DataBinder.Eval(Container, "DataItem.Deleted").Equals("1") %>' />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                </Columns>
                                                                <SelectedItemStyle BackColor="Beige" />
                                                                </asp:DataGrid>  
                                                            </td>  
                                                        </tr>
                                                        <tr align="center" style="height: 50%">
                                                            <td align="center" colspan="6">
                                                            <br />

                                                            <button align="middle" runat="server" id="ClosePUp2" type="button" class="altMedium0BlueButtonFormat" validationgroup="none"  
                                                            onclick="javascript:return fnDisableValidator();"><asp:Literal ID="Literal96" Text="<%$ Resources:WebResources, Cancel%>" runat="server"></asp:Literal></button>
                                                            <input type ="button" ID="btnGuestAddProfile" value="<%$ Resources:WebResources, ExpressConference_AddNewprofile%>" style="width:260px;"  runat="server" onserverclick="fnGuestAddProfile" 
                                                                ValidationGroup="SubmitG" OnClientClick="javascript:return ValidateflyEndpoints();" class="altMedium0BlueButtonFormat" /><%--ALLBUGS-111--%>
                                                            <asp:Button CausesValidation="false" ID="btnGuestLocationSubmit" runat="server" Text="<%$ Resources:WebResources, ExpressConference_btnGuestLocationSubmit%>" ValidationGroup="SubmitG"
                                                                OnClick="fnGuestLocationSubmit" class="altMedium0BlueButtonFormat" OnClientClick="javascript:document.getElementById('hdnSaveData').value = '1';return ValidateflyEndpoints();"/><%--ALLBUGS-111--%>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        </div>
                                        </ContentTemplate> 
                                        <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="btnGuestAddProfile" />
                                        <asp:AsyncPostBackTrigger ControlID="btnGuestLocationSubmit" />
                                        </Triggers>
                                        </asp:UpdatePanel>
                                    </asp:Panel>
                                    </div> <%-- ZD 103573 --%>
                                </td>
                            </tr>
                            <tr style="display:none">
                                <td>
                                    <asp:Button ID="btnGuestLocation2" runat="server" />
                                    <div>
                                        <asp:Button ID="btnUpdtGrid" runat="server" onclick="GusetLocClose" style="display:none"/>
                                    </div>
                                </td>
                            </tr>
                            <%-- FB 2426 Ends --%>
                        </table>
                        </ContentTemplate>
                        </asp:UpdatePanel>
                        </div>
                        <%-- FB 2525 Starts --%>
                    </td>
                </tr>
                  <%--FB 2426 Start--%>
                    <tr valign="top">
                    <td colspan="2">
                    <table border="0" style="width:100%;">
                    <tr>
                         <td style="width:50%;">
                         <%--<input type="image" id="RoomPoupup" value="RoomPoupup" onfocus="this.blur()" validationgroup="Submit" onclick="javascript:return OpenRoom();"  
                                        runat="server" src="~/en/image/PressToSelect.png" /> FB --%><%--FB 2367--%>
                            <button type="button" id="RoomPoupup" class="altMedium0BlueButtonFormat"
                                onclick="javascript:OpenRoomSearchresponse();" runat="server"><asp:Literal ID="Literal97" Text="<%$ Resources:WebResources, ExpressConference_Choose%>" runat="server"></asp:Literal></button><%-- FB 2827 ZD 100922 --%>&nbsp;&nbsp;<asp:Image
                                    ID="Image2" AlternateText="<%$ Resources:WebResources, ExpressConference_Info%>" src="image/info.png" runat="server" style="cursor:default" ToolTip="<%$ Resources:WebResources, TelepresenceRoom%>" /><%--ZD 100419--%><%-- ZD 102590 --%>
                            <input name="addRooms" type="button" id="addRooms" onclick="javascript:AddRooms();"
                                style="display: none;" />
                        </td>
                        <td style="width:50%;">
                        <asp:Button ID="btnGuestLocation" runat="server" Width="58%" Text="<%$ Resources:WebResources, ExpressConference_btnGuestLocation%>"
                                class="altMedium0BlueButtonFormat" OnClientClick="javascript:document.getElementById('hdnSaveData').value = '1';return fnValidator();" /><%-- FB 2827 --%>
                            <asp:Image ID="Image6" AlternateText="<%$ Resources:WebResources, ExpressConference_Info%>" src="image/info.png" style="cursor:default" runat="server" ToolTip="<%$ Resources:WebResources, GuestRoomCreation%>" /> <%--ZD 100419--%><%-- ZD 102590 --%>
                        </td>
                        <td align="right" class="blackblodtext" style="display: none">
                            <a onclick="javascript:goToCal(); return false;" href="#"><asp:Literal ID="Literal98" Text="<%$ Resources:WebResources, ExpressConference_CheckRoomCale%>" runat="server"></asp:Literal></a>
                            <img src="image/calendar.gif" alt="<asp:Literal Text='<%$ Resources:WebResources, DateSelector%>' runat='server'></asp:Literal>" border="0" hspace="0" vspace="0" id="Img2" style="cursor: pointer;
                                vertical-align: bottom;" title="<asp:Literal Text='<%$ Resources:WebResources, DateSelector%>' runat='server'></asp:Literal>" onclick="javascript:goToCal(); return false;" /> <%--ZD 100419--%> 
                        </td>
                        
                        </tr>
                    </table>
                    </td>
                   </tr>
                   <%--FB 2426 End--%>
                </table>
            </td>
        </tr>
        <%--ZD 100834 Starts--%>
        <tr>
        <td id="tdParty" width="100%" valign="top"> <%--ZD 100704--%>
        <br />
            <table id="tbParty" width="100%" cellpadding="0" cellspacing="0">
                <tr style="height: 38" valign="middle" class="Bodybackgorund">
                    <td>
                        <span style="width: 30;" class="ExpressHeadingsNumbers">&nbsp;&nbsp;&nbsp;3&nbsp;&nbsp;&nbsp;</span><span
                            class="ExpressHeadings">&nbsp;&nbsp;&nbsp;<asp:Literal ID="Literal99" Text="<%$ Resources:WebResources, ExpressConference_Participants%>" runat="server"></asp:Literal> </span>
                    </td>
                </tr>
                <tr>
                    <td bordercolor="#0000ff" colspan="4" align="left" width="100%">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                            <% if (enableDetailedExpressForm == 1)
                               { %>
                                <td width="40%" valign="top" align="left">
                                    <iframe align="left" height="150" name="ifrmPartylist" id="ifrmPartylist" 
                                    src="settings2partyNET.aspx?wintype=ifr"  valign="top" style="border: 0px" width="100%"><%--Edited for FF--%>
                                    <p><asp:Literal ID="Literal100" Text="<%$ Resources:WebResources, ExpressConference_goto%>" runat="server"></asp:Literal> <a href="settings2partyNET.aspx?wintype=ifr"><asp:Literal ID="Literal101" Text="<%$ Resources:WebResources, ExpressConference_Participants%>" runat="server"></asp:Literal></a></p><%--ZD 100619--%>
                                    </iframe> 
                                </td>
                                <%}
                               else
                               { %>
                                <td width="40%" valign="top" align="left">
                                    <iframe align="left" height="150" name="ifrmPartylist" id="ifrmPartylist" 
                                    src="Settings2PartyNETExpress.aspx?wintype=ifr" valign="top" style="border: 0px" width="100%"><%--Edited for FF--%>
                                    <p><asp:Literal ID="Literal102" Text="<%$ Resources:WebResources, ExpressConference_goto%>" runat="server"></asp:Literal><a href="settings2partyNET.aspx?wintype=ifr"><asp:Literal ID="Literal103" Text="<%$ Resources:WebResources, ExpressConference_Participants%>" runat="server"></asp:Literal></a></p>
                                    </iframe> 
                                </td>
                                <%} %>
                            </tr>
                        </table>
                        <%--the following 2 controls have been added to confirm the validation on next and previous click under CheckFiles function--%>
                        <asp:TextBox ID="txtTempUser" runat="server" Visible="false" Text="<%$ Resources:WebResources, ExpressConference_txtTempUser%>" TabIndex="-1" BackColor="White" BorderColor="White" BorderStyle="None"></asp:TextBox> <%--ZD 101028--%>
                        <asp:RequiredFieldValidator ID="reqUser" ControlToValidate="txtTempUser" runat="server"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td colspan="9">
                        <table border="0" cellspacing="0" width="100%">
                            <tr>
                                <td width="100%" valign="top" align="left">
                                    <asp:TextBox ID="txtUsersStr" runat="server" Width="0px" ForeColor="transparent"  TabIndex="-1" 
                                        BackColor="transparent" BorderStyle="None" BorderColor="Transparent"></asp:TextBox> <%--ZD 101028--%>
                                    <asp:TextBox ID="txtPartysInfo" runat="server" Width="0px" ForeColor="Black" BackColor="transparent" TabIndex="-1"
                                        BorderStyle="None" BorderColor="Transparent"></asp:TextBox> <%--ZD 101028--%>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                <td id="tdPartyButtons" align="left" valign="bottom" ><%--ZD 100704--%><%--ZD 101267--%>
                    <button type="button" id="btnAddNewParty" class="altMedium0BlueButtonFormat"
                        runat="server" onclick="javascript:document.getElementById('hdnSaveData').value = '1';addNewExpressPartyNET(1);return false;"><asp:Literal ID="Literal104" Text="<%$ Resources:WebResources, New%>" runat="server"></asp:Literal></button><%-- FB 2827 ZD 100922 --%>&nbsp;&nbsp;<asp:Image
                            ID="Image4" src="image/info.png" runat="server" style="cursor:default" ToolTip="<%$ Resources:WebResources, addnewpartymsg%>" /><%-- ZD 102590 --%>
                    <button id="VRMLookup" class="altMedium0BlueButtonFormat"
                        runat="server" onclick="javascript:document.getElementById('hdnSaveData').value = '1';getYourOwnEmailListNET();return false;" style="width:230px" type="button" ><asp:Literal ID="Literal105" Text="<%$ Resources:WebResources, ExpressConference_AddressBook%>" runat="server"></asp:Literal></button><%-- FB 2827 ZD 100666--%>&nbsp;&nbsp;<asp:Image
                            ID="Image3" src="image/info.png" runat="server" style="cursor:default" ToolTip="<%$ Resources:WebResources, Addressbookmsg%>" /><%-- ZD 102590 --%>
                </td>
                </tr>
            </table>
        </td>
        </tr>
        <tr id="trExternalUsers" runat="server" width="100%">
            <td>
            <asp:UpdatePanel id="updtDgUsers" runat="server">
            <ContentTemplate>
            <input type="hidden" name="hdnextusrcnt" id="hdnextusrcnt" runat="server" value="" />
            <input type="hidden" id="hdnAudioId" runat="server" value="" /> <%-- ZD 101253 --%>
            <table width="60%">
            <tr>
                <td colspan="4" class="subtitlexxsblueblodtext"><asp:Literal ID="Literal106" Text="<%$ Resources:WebResources, ExpressConference_ExternalUsers%>" runat="server"></asp:Literal>
                </td>
            </tr>
            <tr>
                <td align="left" >
                    <asp:DataGrid runat="server" OnItemCreated="InitializeUsers" ID="dgUsers" AutoGenerateColumns="false" EnableViewState="true"
                    CellSpacing="0" CellPadding="4" GridLines="None" BorderColor="#336699" BorderStyle="solid" BorderWidth="1"  Width="98%" style="border-collapse:separate" > 
                    <AlternatingItemStyle CssClass="tableBody" />
                    <ItemStyle CssClass="tableBody" />
                    <Columns>
                        <asp:BoundColumn DataField="ID" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="IsTestEquipment" Visible="false"></asp:BoundColumn>
                        <asp:TemplateColumn>
                            <HeaderStyle CssClass="Bodybackgorund" HorizontalAlign="center" Height="15" /><%--100619--%>
                            <ItemTemplate>
                            <table width="100%">
                                <tr>
                                    <td align="left" colspan="6">
                                        <asp:TextBox ID="lblUserID" runat="server" TabIndex="-1" Text='<%# DataBinder.Eval(Container, "DataItem.ID") %>' Width="0" Height="0" BorderColor="transparent" BorderStyle="None" ForeColor="transparent" /> <%--ZD 100619--%>
                                        <asp:Label ID="lblUserName" CssClass="subtitlexxsblueblodtext" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Name") %>'></asp:Label>
                                    </td>
                                    </tr>
                                    <tr>
                                    <td align="left" class="blackblodtext" style="width:15%"><asp:Literal ID="Literal1" Text="<%$ Resources:WebResources, ExpressConference_td1%>" runat="server"></asp:Literal><span class="reqfldstarText">*</span></td>
                                    <td align="left"  style="width:28%">
                                        <asp:TextBox CssClass="altText" ID="txtAddress"  Width="172px" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Address") %>' onchange="CheckIPAddress(this, 1)"></asp:TextBox><%--ZD 101268--%> 
                                        <asp:RequiredFieldValidator ID="reqAddress" Enabled="false" ControlToValidate="txtAddress" ErrorMessage="<%$ Resources:WebResources, ExpressConference_RequiredField%>" runat="server" Display="Dynamic"></asp:RequiredFieldValidator>
                                    </td>
                                    <td colspan="4">
                                        <table id="tbMCUandConn" runat="server" width="100%" border="0">
                                        <tr>
                                            <td align="left" class="blackblodtext"  style="width:36%"><asp:Label ID="LblEPUsers" runat="server" Text="<%$ Resources:WebResources, MCU%>" ></asp:Label><span class="reqfldstarText">*</span></td>                                           
                                            <td align="left">
                                            <asp:DropDownList CssClass="altSelectFormat" ID="lstBridges" runat="server" DataTextField="BridgeName" DataValueField="BridgeID" SelectedValue='<%# DataBinder.Eval(Container, "DataItem.BridgeID") %>' 
                                            onChange="javascript:document.getElementById('hdnSaveData').value = '1';" ></asp:DropDownList>
                                                <asp:DropDownList CssClass="altSelectFormat" Visible="false" ID="lstTelnetUsers" runat="server" onChange="javascript:document.getElementById('hdnChgCallerCallee').value = '1';"> <%--ZD 103402--%> 
                                                <asp:ListItem Value="0"  Text="<%$ Resources:WebResources, ExpressConference_Callee%>" ></asp:ListItem> 
                                                <asp:ListItem Value="1" Text="<%$ Resources:WebResources, ExpressConference_Caller%>"></asp:ListItem>
                                            </asp:DropDownList> 
                                            <asp:Label ID="lblBridgeID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BridgeID") %>' Visible="false"></asp:Label>
                                            </td>
                                        </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="AudioParams1" runat="server">
                                <td align="left" class="blackblodtext"  style="width:20%">
                                            <asp:Literal ID="Literal107" Text="<%$ Resources:WebResources, ExpressConference_Connection%>" runat="server"></asp:Literal>
                                            </td>
                                            <td align="left">
                                            <asp:DropDownList ID="lstConnection" runat="server" CssClass="altSelectFormat" DataTextField="Name" DataValueField="ID" Enabled="false" SelectedValue='<%# DataBinder.Eval(Container, "DataItem.Connection") %>'>
                                            </asp:DropDownList>
                                            </td>
                                    <td align="left" class="blackblodtext"><asp:Literal ID="Literal108" Text="<%$ Resources:WebResources, ExpressConference_AddressType%>" runat="server"></asp:Literal><span class="reqfldstarText">*</span></td>
                                    <td align="left">
                                        <asp:DropDownList CssClass="altSelectFormat" ID="lstAddressType" runat="server" SelectedValue='<%# DataBinder.Eval(Container, "DataItem.AddressType") %>' DataTextField="Name" DataValueField="ID" onchange="javascript:fnAddresstypechg(this,1)"></asp:DropDownList><br /><%--ZD 104068--%>
                                        <asp:RequiredFieldValidator ID="reqAddressType" Enabled="false" ControlToValidate="lstAddressType" InitialValue="-1" ErrorMessage="<%$ Resources:WebResources, ExpressConference_RequiredField%>" runat="server" Display="Dynamic"></asp:RequiredFieldValidator>                                        
                                        <input type="hidden" id="hdnAddrTypeChg" runat="server" value="0" /><%--ZD 104068--%>
                                    </td>
                                </tr>
                                <tr id="AudioParams2" runat="server">
                                <td align="left" class="blackblodtext"><asp:Literal ID="Literal109" Text="<%$ Resources:WebResources, ExpressConference_LineRate%>" runat="server"></asp:Literal><span class="reqfldstarText">*</span></td>
                                    <td align="left">
                                        <asp:DropDownList CssClass="altSelectFormat" ID="lstLineRate" runat="server" DataTextField="LineRateName" DataValueField="LineRateID" SelectedValue='<%# DataBinder.Eval(Container, "DataItem.Bandwidth") %>'></asp:DropDownList>  
                                        <asp:RequiredFieldValidator ID="reqLineRate" Enabled="false" ControlToValidate="lstLineRate" InitialValue="-1" ErrorMessage="<%$ Resources:WebResources, ExpressConference_RequiredField%>" runat="server" Display="Dynamic"></asp:RequiredFieldValidator><%--FB 2444--%>
                                    </td>
                                    <td align="left" class="blackblodtext"><asp:Literal ID="Literal110" Text="<%$ Resources:WebResources, ExpressConference_ConnectionType%>" runat="server"></asp:Literal><span class="reqfldstarText">*</span></td>
                                    <td align="left">
                                        <asp:DropDownList ID="lstConnectionType" CssClass="altSelectFormat" runat="server" DataTextField="Name" DataValueField="ID" SelectedValue='<%# DataBinder.Eval(Container, "DataItem.ConnectionType") %>'></asp:DropDownList><br /><%--Fogbugz case 427--%>
                                        <asp:RequiredFieldValidator ID="reqConnectionType" Enabled="false"  runat="server" ControlToValidate="lstConnectionType" Display="dynamic" InitialValue="-1" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, ExpressConference_RequiredField%>"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr id="Tr1" runat="server"> <%--ZD 103874--%>
                                <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ExpressConference_Equipment%>" runat="server"></asp:Literal></td>
                                    <td align="left">
                                        <asp:DropDownList CssClass="altSelectFormat" ID="lstVideoEquipment" runat="server" DataTextField="VideoEquipmentName" DataValueField="VideoEquipmentID" SelectedValue='<%# DataBinder.Eval(Container, "DataItem.VideoEquipment") %>'></asp:DropDownList>  <%--SelectedValue='<%# DataBinder.Eval(Container, "DataItem.BridgeID") %>'--%>
                                        <asp:RequiredFieldValidator ID="reqEquipment" ControlToValidate="lstVideoEquipment" InitialValue="0" ErrorMessage="Required" runat="server" Display="Dynamic"></asp:RequiredFieldValidator>
                                    </td><%--ZD 101254--%>
                                </tr>
                            </table>
                            </ItemTemplate>
                            <FooterTemplate>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <SelectedItemStyle BackColor="Beige" />
                    </asp:DataGrid>
                    <asp:Label ID="lblNoUsers" runat="server" Text="<%$ Resources:WebResources, ExpressConference_lblNoUsers%>" Visible="False" CssClass="lblError"></asp:Label>                        
                </td>
            </tr>
            <tr style="display:none">
            <td>
             <button id="btnSample" type="button" runat="server" validationgroup="none" style="width:0px;" onserverclick="btnClickSample" onclick="javascript:fnReloadExternalGrid();" ></button>
                </td>
            </tr>
            </table>
            </ContentTemplate>
            </asp:UpdatePanel>
            </td>
        </tr>
        
        <%--ZD 100834 End--%>
        <tr>
                <td>
                    <hr style="height: 3px; border-width: 0; background-color: #3685bb" />
                </td>
        </tr>
        <tr style="display: none;">
            <td>
                <img src="image/DoubleLine.jpg" alt="DoubleLine" height="6px" width="100%" /><%--ZD 100419--%>
            </td>
        </tr>
        <tr style="display: none">
            <td>
                <table id="webConf" runat="server" width="100%" border="0">
                    <tr>
                        <td class="subtitlexxsblueblodtext">
                            <asp:Literal ID="Literal111" Text="<%$ Resources:WebResources, ExpressConference_4WebConferenc%>" runat="server"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%" border="0">
                                <tr>
                                    <td width="62px">
                                    </td>
                                    <td align="left">
                                        <asp:Table ID="tblWeb" runat="server" CellPadding="3" CellSpacing="2" Visible="true">
                                        </asp:Table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr style="display: none">
            <td>
                <img src="image/DoubleLine.jpg" alt="DoubleLine" height="6px" width="100%" /><%--ZD 100419--%>
            </td>
        </tr>
        <tr style="display: none;">
            <td>
                <img src="image/DoubleLine.jpg" alt="DoubleLine" height="6px" width="100%" /> <%--ZD 100419--%>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Table runat="server" Width="100%" CellSpacing="5" CellPadding="2" ID="tblConflict"
                    Style="display: none;">
                    <asp:TableRow ID="TableRow11" runat="server">
                        <asp:TableCell ID="TableCell8" HorizontalAlign="Center" runat="server">
                           <h4><asp:Literal ID="Literal112" Text="<%$ Resources:WebResources, ExpressConference_Resolvethefol%>" runat="server"></asp:Literal></h4>
                        </asp:TableCell></asp:TableRow>
                    <asp:TableRow ID="TableRow12" runat="server">
                        <asp:TableCell ID="TableCell9" HorizontalAlign="Center" runat="server">
                            <asp:DataGrid Width="80%" ID="dgConflict" runat="server" AutoGenerateColumns="False"
                                OnItemDataBound="InitializeConflict">
                                <Columns>
                                    <asp:BoundColumn DataField="formatDate" HeaderText="<%$ Resources:WebResources, ExpressConference_Date%>">
                                        <HeaderStyle CssClass="tableHeader" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="conflict" HeaderText="<%$ Resources:WebResources, ExpressConference_Conflict%>" ItemStyle-Wrap="false">
                                        <HeaderStyle CssClass="tableHeader" />
                                    </asp:BoundColumn>
                                    <asp:TemplateColumn>
                                        <HeaderTemplate>
                                            <asp:Literal ID="Literal1" Text="<%$ Resources:WebResources, ExpressConference_SetupTime%>" runat="server"></asp:Literal>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <mbcbb:ComboBox CssClass="altText" Style="width: 70px" runat="server" ID="conflictSetupTime">
                                                <asp:ListItem Value="01:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="02:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="03:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="04:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="05:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="06:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="07:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="08:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="09:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="10:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="11:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="12:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="01:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="02:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="03:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="04:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="05:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="06:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="07:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="08:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="09:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="10:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="11:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="12:00 AM"></asp:ListItem>
                                            </mbcbb:ComboBox>
                                            <asp:RequiredFieldValidator ID="ReqConflictSetupTime" runat="server" ControlToValidate="conflictSetupTime"
                                                Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, ExpressConference_RequiredField%>"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="RegConflictSetupTime" runat="server" ControlToValidate="conflictSetupTime"
                                                Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, InvalidTime%> (HH:MM AM/PM)" ValidationExpression="[0|1][0-9]:[0-5][0-9] ?[a|A|p|P][M|m]"></asp:RegularExpressionValidator><%--FB 1715--%>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, ExpressConference_StartTime%>">
                                        <HeaderStyle CssClass="tableHeader" />
                                        <ItemTemplate>
                                            <mbcbb:ComboBox CssClass="altText" Style="width: 70px" runat="server" ID="conflictStartTime"
                                                DataValueField='<%# DataBinder.Eval(Container, "DataItem.startHour") %>' DataTextField='<%# DataBinder.Eval(Container, "DataItem.startHour") %>'>
                                                <asp:ListItem Value="01:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="02:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="03:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="04:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="05:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="06:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="07:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="08:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="09:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="10:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="11:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="12:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="01:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="02:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="03:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="04:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="05:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="06:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="07:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="08:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="09:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="10:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="11:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="12:00 AM"></asp:ListItem>
                                            </mbcbb:ComboBox>
                                            <asp:RequiredFieldValidator ID="ReqConflictTime" runat="server" ControlToValidate="conflictStartTime"
                                                Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, ExpressConference_RequiredField%>"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="RegConflictTime" runat="server" ControlToValidate="conflictStartTime"
                                                Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, InvalidTime%> (HH:MM AM/PM)" ValidationExpression="[0|1][0-9]:[0-5][0-9] ?[a|A|p|P][M|m]"></asp:RegularExpressionValidator><%--FB 1715--%>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, ExpressConference_Endtime%>">
                                        <ItemTemplate>
                                            <mbcbb:ComboBox CssClass="altText" Style="width: 70px" runat="server" ID="conflictEndTime">
                                                <asp:ListItem Value="01:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="02:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="03:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="04:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="05:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="06:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="07:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="08:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="09:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="10:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="11:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="12:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="01:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="02:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="03:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="04:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="05:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="06:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="07:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="08:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="09:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="10:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="11:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="12:00 AM"></asp:ListItem>
                                            </mbcbb:ComboBox>
                                            <asp:RequiredFieldValidator ID="ReqConflictEndTime" runat="server" ControlToValidate="conflictEndTime"
                                                Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, ExpressConference_RequiredField%>"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="RegConflictEndTime" runat="server" ControlToValidate="conflictEndTime"
                                                Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, InvalidTime%> (HH:MM AM/PM)" ValidationExpression="[0|1][0-9]:[0-5][0-9] ?[a|A|p|P][M|m]"></asp:RegularExpressionValidator><%--FB 1715--%>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn>
                                        <HeaderTemplate>
                                           <asp:Literal ID="Literal1" Text="<%$ Resources:WebResources, ExpressConference_Teardowntime%>" runat="server"></asp:Literal> 
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <mbcbb:ComboBox CssClass="altText" Style="width: 70px" runat="server" ID="conflictTeardownTime">
                                                <asp:ListItem Value="01:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="02:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="03:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="04:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="05:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="06:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="07:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="08:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="09:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="10:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="11:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="12:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="01:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="02:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="03:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="04:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="05:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="06:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="07:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="08:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="09:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="10:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="11:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="12:00 AM"></asp:ListItem>
                                            </mbcbb:ComboBox>
                                            <asp:RequiredFieldValidator ID="ReqConflictTeardownTime" runat="server" ControlToValidate="conflictTeardownTime"
                                                Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, ExpressConference_RequiredField%>"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="RegConflictTeardownTime" runat="server" ControlToValidate="conflictTeardownTime"
                                                Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, InvalidTime%> (HH:MM AM/PM)" ValidationExpression="[0|1][0-9]:[0-5][0-9] ?[a|A|p|P][M|m]"></asp:RegularExpressionValidator><%--FB 1715--%>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, ExpressConference_Action%>">
                                        <HeaderStyle CssClass="tableHeader" />
                                        <ItemTemplate>
                                            <asp:Button runat="server" ID="btnViewConflict" CssClass="altShortBlueButtonFormat"
                                                Text="<%$ Resources:WebResources, ExpressConference_btnViewConflict%>" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="startHour" Visible="False"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="startMin" Visible="False"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="startSet" Visible="False"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="durationMin" Visible="False"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="startDate" Visible="False"></asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, ExpressConference_Delete%>">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkConflictDelete" runat="server" />
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="tableHeader" />
                                    </asp:TemplateColumn>
                                </Columns>
                                <AlternatingItemStyle CssClass="tablebody" />
                                <ItemStyle CssClass="tableBody" />
                                <HeaderStyle CssClass="tableHeader" />
                            </asp:DataGrid>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow ID="TableRow1" runat="server">
                        <asp:TableCell ID="TableCell1" HorizontalAlign="Center" runat="server">
                            <table width="100%">
                                <tr align="center">
                                    <td align="center">
                                        <input type='submit' name='SoftEdgeTest1' style='width: 0px; display: none' />
                                        <%--<asp:ImageButton ImageUrl="~/en/image/Submit.png" ID="btnConflicts"  runat="server"  Text="Set Custom Instances" OnClick="SetConferenceCustom"/>--%>
                                        <%--ZD 102360--%>
                                        <asp:Button ID="btnConflicts" runat="server" CssClass="altMedium0BlueButtonFormat" Width="150pt" OnClientClick="javascript:document.getElementById('hdnSaveData').value = '1';"
                                            Text="<%$ Resources:WebResources, ExpressConference_btnConflicts%>" OnClick="SetConferenceCustom" /> <%--FB 2592--%> <%--FB 3030--%>
                                    </td>
                                </tr>
                            </table>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </td>
        </tr>
        <tr align="center">
            <td align="center">
                <table width="100%">
                    <tr align="center">
                        <td align="center">
                            <input type='submit' name='SoftEdgeTest1' style='width: 0px; display: none' />
                            <asp:Button ID="btnConfSubmit" Width="195px" CssClass="altMedium0BlueButtonFormat" onfocus="this.blur()"
                                runat="server" Text="<%$ Resources:WebResources, ExpressConference_btnConfSubmit%>" OnClientClick="javascript:fnExternalGridValidator('1'); return Final();"
                                OnClick="EmailDecision" /><%-- FB 2827 --%>
                        </td>
                    </tr>
                    <tr align="right" style="display: none">
                        <%--FB 1830 Email Edit--%>
                        <td align="right" width="1px">
                            <asp:Button ID="btnDummy" BackColor="Transparent" runat="server" OnClick="SetConference" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr id="fileuploadsection">
            <td>
                <ajax:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="aFileUp"
                    PopupControlID="PanloadFiles" DropShadow="false" Drag="true" BackgroundCssClass="modalBackground"
                    CancelControlID="CloseFilePop" BehaviorID="aFileUp">
                </ajax:ModalPopupExtender>
                <asp:Panel ID="PanloadFiles" Width="500px" Height="200px" runat="server" HorizontalAlign="Center"
                    CssClass="treeSelectedNode"><%-- ZD 102275 --%>
                    <table cellpadding="3" cellspacing="0" border="0" width="80%" class="treeSelectedNode">
                        <tr>
                            <td>
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                                    <Triggers>
                                    </Triggers>
                                    <ContentTemplate>
                                        <div align="center" id="Div4" style="width: 600Px; height: 250px; vertical-align: middle;
                                            border: none;" class="treeSelectedNode"> <br /> <%-- ZD 102275 --%>
                                            <table id="tblPanel" cellpadding="3" cellspacing="0" border="0" width="80%">
                                                <tr id="trFile1" runat="server">
                                                    <td align="left" class="blackblodtext" valign="top" nowrap="nowrap">
                                                        <asp:Literal ID="Literal113" Text="<%$ Resources:WebResources, ExpressConference_File1%>" runat="server"></asp:Literal>
                                                    </td>
                                                    <td align="left">
                                                        <table width="100%" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td width="70%"><%--FB 2909 Start--%>
                                                                <table border="0" width="100%" style="border-collapse:collapse">
                                                                    <tr> 
                                                                        <td>
                                                                        <div>
                                                                   <input  type="text" class="file_input_textbox" id="txtFileUpload1" runat="server" readonly="readonly" value='<%$ Resources:WebResources, Nofileselected%>' style="width: 125px" />
                                                                    <div class="file_input_div"><input id="Button2" runat="server" type="button" value="<%$ Resources:WebResources, Browse%>" class="file_input_button" onclick="document.getElementById('FileUpload1').click();return false;" /><%--FB 3055-Filter in Upload Files--%><%--ZD 100420--%>
                                                                      <input type="file"  class="file_input_hidden" id="FileUpload1" tabindex="-1"  contenteditable="false" enableviewstate="true" runat="server" onchange="getfilename(this);fnValidateFileName(this.value,1);"/></div></div>
                                                                      

                                                                        </td>                                                              
                                                                    </tr>
                                                                    <tr>
                                                                        <td>                                                                        
                                                                        <div>
                                                                        <asp:RegularExpressionValidator ID="regFNVal1" ControlToValidate="FileUpload1" Display="dynamic" runat="server" ValidationGroup="Submit1" 
                                                                            SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters46%>" ValidationExpression="^[^<>&#]*$"></asp:RegularExpressionValidator>
                                                                        </div>   
                                                                        </td>
                                                                    </tr>
                                                                </table>                                                              
                                                                                                                                          
                                                                      
                                                                     
                                                                    <%--<input type="file" id="FileUpload1" contenteditable="false" enableviewstate="true"
                                                                        size="50" class="altText" runat="server" />--%><%--FB 2909 End--%>
                                                                    <asp:Label ID="lblUpload1" Text="" Visible="false" runat="server"></asp:Label>
                                                                </td>
                                                                <td width="30%" align="left" valign="top">
                                                                    <%--<asp:ImageButton ID="btnRemove1" ImageUrl="~/en/image/remove.png"  Text="Remove" Visible="false" runat="server" OnCommand="RemoveFile" CommandArgument="1" />--%>
                                                                    <asp:Button ID="btnRemove1" Text="<%$ Resources:WebResources, ExpressConference_btnRemove1%>" CssClass="altMedium0BlueButtonFormat" Width="80px" Visible="false"
                                                                        runat="server" OnCommand="RemoveFile" CommandArgument="1" OnClientClick="javascript:document.getElementById('hdnSaveData').value = '1';" /> <%-- FB 2827 --%>
                                                                    <asp:Label ID="hdnUpload1" Text="" Visible="false" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr  style="height:10px">
                                                </tr>
                                                <tr id="trFile2" runat="server">
                                                    <td align="left" class="blackblodtext" valign="top" nowrap="nowrap">
                                                        <asp:Literal ID="Literal114" Text="<%$ Resources:WebResources, ExpressConference_File2%>" runat="server"></asp:Literal>
                                                    </td>
                                                    <td align="left">
                                                        <table width="100%" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td width="70%"><%--FB 2909 Start--%>
                                                                <table border="0" width="100%" style="border-collapse:collapse">
                                                                    <tr> 
                                                                        <td>
                                                                <div>
                                                                   <input type="text" id="txtFileUpload2" class="file_input_textbox" runat="server" readonly="readonly" value='<%$ Resources:WebResources, Nofileselected%>' style="width: 125px" />
                                                                    <div class="file_input_div"><input type="button" runat="server" value="<%$ Resources:WebResources, Browse%>" class="file_input_button" onclick="document.getElementById('FileUpload2').click();return false;" /><%--FB 3055-Filter in Upload Files--%><%--ZD 100420--%>
                                                                      <input type="file"  class="file_input_hidden" id="FileUpload2" tabindex="-1" contenteditable="false" enableviewstate="true" runat="server" onchange="getfilename(this);fnValidateFileName(this.value,2);"/></div></div>
                                                                    <%--<input type="file" id="FileUpload2" contenteditable="false" enableviewstate="true"
                                                                        size="50" class="altText" runat="server" />--%><%--FB 2909 End--%>
                                                                         </td>                                                              
                                                                    </tr>
                                                                    <tr>
                                                                        <td>                                                                        
                                                                        <div>
                                                                                   <asp:RegularExpressionValidator ID="regFNVal2" ControlToValidate="FileUpload2" Display="dynamic" runat="server" ValidationGroup="Submit1" 
                                                                                    SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters46%>" ValidationExpression="^[^<>&#]*$"></asp:RegularExpressionValidator>
                                                                                  </div>   
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <asp:Label ID="lblUpload2" Text="" Visible="false" runat="server"></asp:Label>
                                                                </td>
                                                                <td width="30%" valign="top">
                                                                    <%--<asp:ImageButton ID="btnRemove2" ImageUrl="~/en/image/remove.png"  Text="Remove" Visible="false" runat="server" OnCommand="RemoveFile" CommandArgument="2" />--%>
                                                                    <asp:Button ID="btnRemove2" Text="<%$ Resources:WebResources, ExpressConference_btnRemove2%>" CssClass="altMedium0BlueButtonFormat" Width="80px" Visible="false"
                                                                        runat="server" OnCommand="RemoveFile" CommandArgument="2" OnClientClick="javascript:document.getElementById('hdnSaveData').value = '1';" /> <%-- FB 2827 --%>
                                                                    <asp:Label ID="hdnUpload2" Text="" Visible="false" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr style="height:10px">
                                                </tr>
                                                <tr id="trFile3" runat="server">
                                                    <td align="left" class="blackblodtext" valign="top" nowrap="nowrap">
                                                        <asp:Literal ID="Literal115" Text="<%$ Resources:WebResources, ExpressConference_File3%>" runat="server"></asp:Literal>
                                                    </td>
                                                    <td align="left">
                                                        <table width="100%" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td width="70%"><%--FB 2909 Start--%>
                                                                <table border="0" width="100%" style="border-collapse:collapse">
                                                                    <tr> 
                                                                        <td>
                                                                        <div>
                                                                   <input type="text" id="txtFileUpload3" class="file_input_textbox" runat="server" readonly="readonly" value='<%$ Resources:WebResources, Nofileselected%>' style="width: 125px" />
                                                                    <div class="file_input_div"><input type="button" runat="server" value="<%$ Resources:WebResources, Browse%>" class="file_input_button" onclick="document.getElementById('FileUpload3').click();return false;" /><%--FB 3055-Filter in Upload Files--%><%--ZD 100420--%>
                                                                      <input type="file"  class="file_input_hidden" id="FileUpload3" tabindex="-1" contenteditable="false" enableviewstate="true" runat="server" onchange="getfilename(this);fnValidateFileName(this.value,3);"/></div></div>
                                                                    <%--<input type="file" id="FileUpload3" contenteditable="false" enableviewstate="true"
                                                                        size="50" class="altText" runat="server" />--%><%--FB 2909 End--%>
                                                                        </td>                                                              
                                                                    </tr>
                                                                    <tr>
                                                                        <td>                                                                        
                                                                        <div>
                                                                        <asp:RegularExpressionValidator ID="regFNVal3" ControlToValidate="FileUpload3" Display="dynamic" runat="server" ValidationGroup="Submit1" 
                                                                        SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters46%>" ValidationExpression="^[^<>&#]*$"></asp:RegularExpressionValidator>
                                                                        </div>   
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <asp:Label ID="lblUpload3" Text="" Visible="false" runat="server"></asp:Label>
                                                                </td>
                                                                <td width="30%" valign="top"><%--ZD 102277 End--%>
                                                                    <%--<asp:ImageButton ID="btnRemove3" ImageUrl="~/en/image/remove.png"  Text="Remove" Visible="false" runat="server" OnCommand="RemoveFile" CommandArgument="3" />--%>
                                                                    <asp:Button ID="btnRemove3" Text="<%$ Resources:WebResources, ExpressConference_btnRemove3%>" Visible="false" CssClass="altMedium0BlueButtonFormat" Width="80px"
                                                                        runat="server" OnCommand="RemoveFile" CommandArgument="3" OnClientClick="javascript:document.getElementById('hdnSaveData').value = '1';" /> <%-- FB 2827 --%>
                                                                    <asp:Label ID="hdnUpload3" Text="" Visible="false" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr><%-- ZD 102275 start--%>
                                                <tr style="height:10px">
                                                </tr>
                                                <tr>
                                                <td></td>
                                                <td colspan ="2" align="left">
                                                 <asp:Literal Text="<%$ Resources:WebResources, ExpressConference_Filesizeexceed%>" runat="server"></asp:Literal>
                                                </td></tr> <%-- ZD 102275 End--%>
                                            </table>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <br />
                                <br />
                                
                                <%--<asp:ImageButton ID="btnUploadFiles" OnClick="UploadFiles" onfocus="this.blur()" ImageUrl="~/en/image/upload.png" runat="server" />--%>
                                <%--<asp:ImageButton ID="CloseFilePop" OnClientClick="CheckUploadedFiles();" onfocus="this.blur()"
                                                ImageUrl="~/en/image/close.png" runat="server" />--%>
                                <%-- ZD 100420 START--%>
                                <%--<asp:Button ID="btnUploadFiles" Width="100" Text="Upload Files" CssClass="altMedium0BlueButtonFormat"
                                    OnClick="UploadFiles" onfocus="this.blur()" runat="server" />--%><%-- FB 2779 FB 2827 --%>
                                <button runat="server" ID="btnUploadFiles" style="width:190px" class="altMedium0BlueButtonFormat" type="button" validationgroup="Submit1" 
                                onserverclick="UploadFiles" onclick="javascript:document.getElementById('hdnSaveData').value = '1';return fnUpload3(document.getElementById('FileUpload1'), document.getElementById('FileUpload2'), document.getElementById('FileUpload3'), this.id, 'Submit1');SubmitRecurrence();" ><asp:Literal ID="Literal116" Text="<%$ Resources:WebResources, ExpressConference_btnUploadFiles%>" runat="server"></asp:Literal></button> <%--ZD 100875--%><%--ZD 100381--%> <%--ZD 102364--%>
                                <%--<asp:Button ID="CloseFilePop" Width="75" Text="Close" CssClass="altMedium0BlueButtonFormat" OnClientClick="CheckUploadedFiles();"
                                    onfocus="this.blur()" runat="server" />--%><%-- FB 2827 --%>
                                <button runat="server" ID="CloseFilePop" style="width:75" class="altMedium0BlueButtonFormat" onclick="javascript:return CheckUploadedFiles();"><asp:Literal ID="Literal117" Text="<%$ Resources:WebResources, ExpressConference_CloseFilePop%>" runat="server"></asp:Literal>  </button><%-- ZD 100420--%>
                               <%-- ZD 100420 END--%> <br /><br />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>

                <script type="text/javascript" language="javascript">

                    function CheckFiles() {


                        if (typeof (Page_ClientValidate) == 'function')
                            if (!Page_ClientValidate()) {
                            DataLoading(0);
                            return false;
                        }


                        if (document.getElementById("FileUpload1")) {
                            if (document.getElementById("FileUpload1").value) {
                                if (confirm("Do you want to upload the browsed file?")) {
                                    document.getElementById("__EVENTTARGET").value = "btnUploadFiles";
                                    WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions("btnUploadFiles", "", true, "", "", false, false));
                                }
                                else {
                                    DataLoading(0);
                                    return false;
                                }
                            }
                        }
                        if (document.getElementById("FileUpload2")) {
                            if (document.getElementById("FileUpload2").value) {
                                if (confirm("Do you want to upload the browsed file?")) {
                                    document.getElementById("__EVENTTARGET").value = "btnUploadFiles";
                                    WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions("btnUploadFiles", "", true, "", "", false, false));
                                }
                                else
                                    DataLoading(0);
                            }
                        }
                        if (document.getElementById("FileUpload3")) {
                            if (document.getElementById("FileUpload3").value) {
                                if (confirm("Do you want to upload the browsed file?")) {
                                    document.getElementById("__EVENTTARGET").value = "btnUploadFiles";
                                    WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions("btnUploadFiles", "", true, "", "", false, false));
                                }
                                else
                                    DataLoading(0);
                            }
                        }

                        return true;
                    }                               
                      
                        changeVMR();//FB 2501
                </script>

            </td>
        </tr>
        <tr id="modalrooms">
            <asp:Button ID="CustomTrigger" runat="server" Style="display: none;" />
            <td>
                <ajax:ModalPopupExtender ID="RoomPopUp" runat="server" TargetControlID="CustomTrigger"
                    Enabled="true" PopupControlID="PopupRoomPanel" DropShadow="false" Drag="true"
                    BackgroundCssClass="modalBackground" CancelControlID="ClosePUpbutton" BehaviorID="RoomPopUp">
                </ajax:ModalPopupExtender>
                <%--FB 1858,1835 Start--%>
                <asp:Panel ID="PopupRoomPanel" Width="60%" Height="60%" runat="server" HorizontalAlign="Center"
                    CssClass="treeSelectedNode" ScrollBars="Auto">
                    <table cellpadding="3" cellspacing="0" border="0" width="100%">
                        <tr>
                            <td valign="top" align="right">
                                <asp:ImageButton ID="ClosePUp" ImageUrl="../image/Close.JPG" runat="server" OnClientClick="javascript:return ProccedClick();"
                                    ToolTip="<%$ Resources:WebResources, Close%>"></asp:ImageButton>
                                <%--FB 1858,1835 End--%>
                                <asp:UpdatePanel ID="UpdatePanelRooms" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                                    <Triggers>
                                    </Triggers>
                                    <ContentTemplate>
                                        <asp:Button ID="refreshrooms" runat="server" OnClick="RefreshRoom" CausesValidation="false"
                                            Style="display: none;" />
                                        <div align="center" id="conftypeDIV" style="width: 100%; height: 100%; border: none;"
                                            class="treeSelectedNode">
                                            <%--FB 1858,1835--%>
                                            <table border="0" cellpadding="3" cellspacing="0" width="100%">
                                                <tr>
                                                    <td align="right" valign="top" width="10%">
                                                    </td>
                                                    <td align="left" class="blackblodtext" valign="top" width="78%">
                                                        <table border="0" style="width: 100%">
                                                            <tr>
                                                                <td valign="top" align="left" width="80">
                                                                    <%-- <asp:ImageButton ID="btnCompare" onfocus="this.blur()"  OnClientClick="javascript:compareselected();" ImageUrl="~/en/image/compare.png" 
                                                        runat="server" />--%>
                                                                    <asp:Button ID="btnCompare" Text="<%$ Resources:WebResources, ExpressConference_btnCompare%>" CssClass="altShortBlueButtonFormat" onfocus="this.blur()"
                                                                        OnClientClick="javascript:compareselected();" runat="server" />
                                                                </td>
                                                                <td nowrap valign="top" align="left" class="blackblodtext">
                                                                    <asp:RadioButtonList ID="rdSelView" runat="server" CssClass="blackblodtext" OnSelectedIndexChanged="rdSelView_SelectedIndexChanged"
                                                                        RepeatDirection="Horizontal" AutoPostBack="True" RepeatLayout="Flow">
                                                                        <asp:ListItem Selected="True" Text="<%$ Resources:WebResources, ExpressConference_LevelView%>" Value="1"></asp:ListItem>
                                                                        <asp:ListItem Value="2" Text="<%$ Resources:WebResources, ExpressConference_ListView%>"></asp:ListItem>
                                                                    </asp:RadioButtonList>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="10%" align="right" valign="top" class="blackblodtext">
                                                        <asp:Literal ID="Literal118" Text="<%$ Resources:WebResources, ExpressConference_Rooms%>" runat="server"></asp:Literal><br><%--FB 2579 End--%>
                                                        <font size="1"><asp:Literal ID="Literal119" Text="<%$ Resources:WebResources, ExpressConference_Clickroomname%>" runat="server"></asp:Literal></font>
                                                    </td>
                                                    <td width="78%" align="left" style="font-weight: bold; font-size: small; color: green;
                                                        font-family: arial" valign="top">
                                                        <asp:Panel ID="pnlLevelView" runat="server" Height="300px" Width="100%" ScrollBars="Auto"
                                                            BorderColor="Blue" BorderStyle="Solid" BorderWidth="1px" HorizontalAlign="Left">
                                                            <%--FB 1858,1835--%>
                                                            <asp:TreeView ID="treeRoomSelection" runat="server" BorderColor="White" Height="95%"
                                                                ShowCheckBoxes="All" onclick="javascript:getRooms(event)" ShowLines="True" Width="94%"
                                                                OnTreeNodeCheckChanged="treeRoomSelection_TreeNodeCheckChanged" OnSelectedNodeChanged="treeRoomSelection_SelectedNodeChanged">
                                                                <NodeStyle CssClass="treeNode" />
                                                                <RootNodeStyle CssClass="treeRootNode" />
                                                                <ParentNodeStyle CssClass="treeParentNode" />
                                                                <LeafNodeStyle CssClass="treeLeafNode" />
                                                            </asp:TreeView>
                                                        </asp:Panel>
                                                        <asp:Panel ID="pnlListView" runat="server" BorderColor="Blue" BorderStyle="Solid"
                                                            BorderWidth="1px" Height="300px" ScrollBars="Auto" Visible="False" Width="100%"
                                                            HorizontalAlign="Left" Direction="LeftToRight" Font-Bold="True" Font-Names="arial"
                                                            Font-Size="Small" ForeColor="Green">
                                                            <%--FB 1858,1835--%>
                                                            <input type="checkbox" id="selectAllCheckBox" runat="server" onclick="CheckBoxListSelect('lstRoomSelection',this);" /><font
                                                                size="2"> <asp:Literal ID="Literal120" Text="<%$ Resources:WebResources, ExpressConference_SelectAll%>" runat="server"></asp:Literal></font>
                                                            <br />
                                                            <asp:CheckBoxList ID="lstRoomSelection" runat="server" Height="95%" Width="95%" Font-Size="Smaller"
                                                                ForeColor="ForestGreen" onclick="javascript:getValues(event)" Font-Names="arial"
                                                                RepeatLayout="Flow">
                                                            </asp:CheckBoxList>
                                                        </asp:Panel>
                                                        <asp:Panel ID="pnlNoData" runat="server" BorderColor="Blue" BorderStyle="Solid" BorderWidth="1px"
                                                            Height="300px" ScrollBars="Auto" Visible="False" Width="100%" HorizontalAlign="Left"
                                                            Direction="LeftToRight" Font-Size="Small">
                                                            <%--FB 1858,1835--%>
                                                            <table>
                                                                <tr align="center">
                                                                    <td>
                                                                        <asp:Literal ID="Literal121" Text="<%$ Resources:WebResources, ExpressConference_YouhavenoRoo%>" runat="server"></asp:Literal>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </asp:Panel>
                                                        <asp:TextBox runat="server" ID="txtTemp" Text="<%$ Resources:WebResources, ExpressConference_txtTemp%>" Visible="false"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Enabled="false" ControlToValidate="txtTemp"
                                                            runat="server"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <br />
                                <%--<asp:ImageButton ID="ClosePUp1" OnClientClick="OpenSetRoom()" ImageUrl="~/en/image/close.png" runat="server" />--%><%--FB 1858,1835--%>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button ID="ClosePUpbutton" Text="<%$ Resources:WebResources, ExpressConference_ClosePUpbutton%>" CssClass="altShortBlueButtonFormat"
                                    OnClientClick="javascript:OpenSetRoom();" runat="server" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr id="modalConfirmation">
            <td>
           
            </td>
        </tr>
        <tr>
            <td>
                
            </td>
        </tr>
    </table>
    
    <%--ZD 101226 UI Modification START--%>
    <div id="divSubModalPopup" runat="server" style="display:none">
                <ajax:ModalPopupExtender ID="SubModalPopupExtender" TargetControlID="adisNone" runat="server"
                    PopupControlID="panSubmit" DropShadow="false" BackgroundCssClass="modalBackground"
                    CancelControlID="showConfMsgImg">
                </ajax:ModalPopupExtender>
                <asp:Panel ID="panSubmit" Width="40%" Height="40%" runat="server" HorizontalAlign="Center"
                    CssClass="treeSelectedNode">
                    <%--FB 1858,1835--%>
                    <asp:UpdatePanel ID="UpdatePanel4" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                        <Triggers>
                        </Triggers>
                        <ContentTemplate>
                            <div align="center" id="Div1" style="width: 100%; height: 100%;" class="treeSelectedNode">
                                <%--FB 1858,1835--%>
                                <table border="0" cellpadding="3" cellspacing="0" width="100%" style="vertical-align: middle;
                                    height: 40%;">
                                    <tr>
                                        <td align="center" style="vertical-align: middle">
                                            <asp:Label ID="showConfMsg" runat="server" CssClass="lblError" Text="<%$ Resources:WebResources, ExpressConference_WorkInprogress%>"></asp:Label>
                                            <br />
                                            <br />
                                            <br />
                                            <br />
                                            <br />
                                            <br /><%-- ZD 100875--%>
                                            <%--<asp:ImageButton ID="showConfMsgImg" ImageUrl="~/en/image/close.png" OnClientClick="javascript:window.location.replace('ExpressConference.aspx?t=n');" runat="server"/>--%>
                                            <asp:Button ID="showConfMsgImg" Text="<%$ Resources:WebResources, Close%>" CssClass="altMedium0BlueButtonFormat"
                                                OnClientClick="javascript:document.getElementById('hdnSaveData').value = '1';window.location.replace('ExpressConference.aspx?t=n'); return false;"
                                                runat="server" />
                                            <br />
                                            <br />
                                            <br />
                                            <br />
                                            <br />
                                            <br />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </asp:Panel>
                <input type="button" id="adisNone" runat="server" style="display: none" />
                </div>
                <%--ZD 101226 UI Modification End--%>
                
    <%--FB 2693 Start--%>
    <div id="divPCdetails" class="rounded-corners" style="left: 425px; position: absolute;
        background-color: White; top: 420px; min-height:200px; z-index: 9999; overflow: hidden;
        border: 0px; width: 700px; display: none;">
        <table width="100%">
            <tr style='height: 25px;'>
                <td colspan="3" style='background-color: #3075AE;' align='center'>
                    <b style='font-size: small; margin-left: 10px; color: White; font-family: Verdana;
                        font-style: normal;'><asp:Literal ID="Literal122" Text="<%$ Resources:WebResources, ExpressConference_INFORMATION%>" runat="server"></asp:Literal></b>
                </td>
             </tr>                        
        </table>
        <div style="height:500px; overflow-y:scroll">
        <table  border="0" cellpadding="0"  cellspacing="0" width="100%" id="Table2">
             <tr>
                <td colspan="3" height="20px;"></td>
             </tr>
            <tr>
                <td colspan="3" >
                    <div id="PCHtmlContent"></div>
                </td>
            </tr>           
        </table>
        </div>
        <table width="100%">
            <tr style="height: 40px;">
                <td align="center" colspan="3">
                    <button id="btnPCCancel" class="altMedium0BlueButtonFormat" style="width:75px">
                        <asp:Literal ID="Literal123" Text="<%$ Resources:WebResources, ExpressConference_CloseFilePop%>" runat="server"></asp:Literal></button> <%-- FB 2827 --%>
                </td>
            </tr>
        </table>                
    </div>
    
    <%--FB 2693 End--%>
        <%--ZD 101815--%>
    <input id="p2pConfirmBtn" type="button" runat="server" style="display:none" />
    <ajax:ModalPopupExtender ID="p2pConfirmPopup" runat="server" TargetControlID="p2pConfirmBtn" PopupControlID="p2pConfirmPanel" 
    DropShadow="false" Drag="true" BackgroundCssClass="modalBackground" CancelControlID="p2pConfirmOk">
    </ajax:ModalPopupExtender>
    <asp:Panel ID="p2pConfirmPanel" Width="50%" Height="25%" runat="server" CssClass="treeSelectedNode" >
    <table width="100%" style="text-align:center">
        <tr>
            <td colspan="2"><br /><br /><b>
            <asp:Literal ID="LitP2PConf" Text="<%$ Resources:WebResources, ExpressConference_P2PConfirmation%>" runat="server"></asp:Literal>
            </b></td>
        </tr>
        <tr>
            <td><br /><br /></td>
        </tr>
        <tr>
            <td>
                <input id="p2pConfirmOk" type="button" class="altMedium0BlueButtonFormat" runat="server" value="<%$ Resources:WebResources, MasterChildReport_cmdConfOk%>" onclick="p2pSetConference('0')" />
            </td>
            <td>
                <input id="p2pConfirmCan" type="button" class="altMedium0BlueButtonFormat" runat="server" style="width:180px" value="<%$ Resources:WebResources, ExpressConference_P2PConfMCU%>" onclick="p2pSetConference('1')" /><%--ZD 103063--%>
            </td>
        </tr>
    </table>
    </asp:Panel>    
	<%--ZD 101815--%>
    </form>

    <script type="text/javascript">
    	

    var prm = Sys.WebForms.PageRequestManager.getInstance(); // ZD 103573
    prm.add_pageLoaded(panelLoaded);
    function panelLoaded(sender, args){
        if(document.getElementById("divGuestPopup") != null)
            document.getElementById("divGuestPopup").style.display = 'block';
    }

     if (document.getElementById('btnRemove1') == null && document.getElementById('btnRemove2') == null && document.getElementById('btnRemove3') == null) 
     {   
       document.getElementById('tblPanel').style.marginLeft = "150px";
     }

        //FB 2620 Starts  FB Icon
        if (document.getElementById("lstVMR") != null) {
           //var lstVMR = document.getElementById("lstVMR").selectedIndex;
        var e =document.getElementById("lstVMR"); //ZD 100753
        var lstVMR=  e.options[e.selectedIndex].value;
            if (lstVMR == 1)
                document.getElementById("divbridge").style.display = "Block";
        }
        //FB 2620 Ends
        
        
	//FB 2634 //ZD 100284
        if (document.getElementById("confStartTime_Text")) 
    {
        var confstarttime_text = document.getElementById("confStartTime_Text");
        confstarttime_text.onblur = function() {
        if (formatTimeNew('confStartTime_Text', 'regStartTime',"<%=Session["timeFormat"]%>"))
                return ChangeEndDate()
        };
    }
    if (document.getElementById("confEndTime_Text")) 
    {
        var confendtime_text = document.getElementById("confEndTime_Text");
        confendtime_text.onblur = function() {
        if (formatTimeNew('confEndTime_Text', 'regEndTime',"<%=Session["timeFormat"]%>"))
                return ChangeEndDate("ET")
        };
    }
        if (document.getElementById("<%=rdSelView.ClientID%>")) {
            if (!document.getElementById("<%=rdSelView.ClientID%>").disabled)
                document.getElementById("<%=rdSelView.ClientID%>").onclick = function()
                { DataLoading(1); };
        }

        function openconflict() {
            if (document.getElementById("dgconflict")) {
                document.getElementById("dgconflict").focus();
            }
        }

        function ChangeDate(inVal) {
            var setupTime = new Date(GetDefaultDate(document.getElementById("confStartDate").value, '<%=format%>') + " "
            + document.getElementById("confStartTime_Text").value);

            var startTime = new Date(GetDefaultDate(document.getElementById("SetupDate").value, '<%=format%>') + " "
            + document.getElementById("SetupTime_Text").value);

            var endTime = new Date(GetDefaultDate(document.getElementById("TearDownDate").value, '<%=format%>') + " "
            + document.getElementById("TeardownTime_Text").value);


            if (inVal == "Set") {
                document.getElementById("SetupDate").value = getCDate(setupTime);
                document.getElementById("SetupTime_Text").value = getCTime(setupTime);
                var startTime = new Date(GetDefaultDate(document.getElementById("SetupDate").value, '<%=format%>') + " "
            + document.getElementById("SetupTime_Text").value);

                startTime = setCDuration(startTime, 60);
                document.getElementById("TearDownDate").value = getCDate(startTime);
                document.getElementById("TeardownTime_Text").value = getCTime(startTime);
                var endTime = new Date(GetDefaultDate(document.getElementById("TearDownDate").value, '<%=format%>') + " "
            + document.getElementById("TeardownTime_Text").value);

                endTime = setCDuration(endTime, 0);
                document.getElementById("confEndDate").value = getCDate(endTime);
                document.getElementById("confEndTime_Text").value = getCTime(endTime);
            }
            else if (inVal == "Start") {
                startTime = setCDuration(startTime, 60);
                document.getElementById("TearDownDate").value = getCDate(startTime);
                document.getElementById("TeardownTime_Text").value = getCTime(startTime);
                var endTime = new Date(GetDefaultDate(document.getElementById("TearDownDate").value, '<%=format%>') + " "
            + document.getElementById("TeardownTime_Text").value);

                endTime = setCDuration(endTime, 0);
                document.getElementById("confEndDate").value = getCDate(endTime);
                document.getElementById("confEndTime_Text").value = getCTime(endTime);
            }
            else if (inVal == "End") {

            }
        }
    </script>

    <script type="text/javascript">
      //SelectAudioParty();//@@@@@
	  fnEnableBuffer();
	  if(document.getElementById("hdnValue").value == "" || document.getElementById("hdnValue").value == "0")
	  {
	    if (document.frmSettings2.EndText)
		    document.frmSettings2.EndText.disabled = true;
	    if (document.frmSettings2.DurText)
		    document.frmSettings2.DurText.disabled = true;

	    document.frmSettings2.RecurValue.value = document.getElementById("Recur").value;
        
        var chkrecurrence = document.getElementById("chkRecurrence");
         
        if(document.getElementById("Recur").value != "" || (chkrecurrence && chkrecurrence.checked == true))
        {
            //var chkrecurrence = document.getElementById("chkRecurrence");
            if(chkrecurrence)
                chkrecurrence.checked = true;
            document.getElementById("hdnRecurValue").value = 'R'
            //FB 2634        
            openRecur();
        
            //initial(); FB 2771
           
            //fnShow();
        }
        
         fnEnableBuffer();
     } 
    
    //FB 2634
    if(document.getElementById("Recur").value == "")
    {
        initial();        
        fnShow();
    }
    
    function OpenRoom()
     {     
            
        if(document.getElementById("refreshrooms"))
            document.getElementById("refreshrooms").click();
                  
        SubmitRecurrence();
        
        var popup2 = $find('RoomPopUp');
        if(popup2)
            popup2.show();
            
        return false;
     }
     //if ("<%=Session["isExpressUserAdv"]%>" == "0") //FB 2394 -LineRate displays to all Express form.
     //document.getElementById("trLineRate").style.display = 'None';


     //ZD 101233 START
    var isSpecialRecur_Session ;
 		
    if(document.getElementById("hdnCrossisSpecialRecur").value != null)
 	    isSpecialRecur_Session = document.getElementById("hdnCrossisSpecialRecur").value;
    else
 	    isSpecialRecur_Session = "<%=Session["isSpecialRecur"] %>";

     
     if(document.getElementById("hdnCrossrecurEnable").value != null)
 	    Enablerecurrence = document.getElementById("hdnCrossrecurEnable").value;
     
     if (Enablerecurrence != null)
         if (Enablerecurrence == "0")
         {
            if(document.getElementById("NONRecurringConferenceDiv8"))
                document.getElementById("NONRecurringConferenceDiv8").style.display = 'None'
            }

     if(document.getElementById("hdnCrossEnableStartMode").value != null)
 	    EnableStartMode = document.getElementById("hdnCrossEnableStartMode").value;

     if (EnableStartMode != null)
         if (EnableStartMode == "0")
         {
           if(document.getElementById('trStartMode') != null && document.getElementById('trStartMode1') != null && document.getElementById('trStartMode2') != null)
            {
            
               document.getElementById('trStartMode').style.display  = 'None';
               document.getElementById('trStartMode1').style.display = 'None';
               document.getElementById('trStartMode2').style.display = 'None';
            }
         }

     
     if(document.getElementById("hdnCrossEnableImmConf").value != null)
 	    EnableImmediateConference = document.getElementById("hdnCrossEnableImmConf").value;
     
     if (EnableImmediateConference != null)
         if (EnableImmediateConference == "0")
         {
            if(document.getElementById("StartNowRow"))
                document.getElementById("StartNowRow").style.display = 'None'
            }

     if(document.getElementById("hdnCrossEnableConfPassword").value != null)
 	    EnableConferencePasswordOrgOption = document.getElementById("hdnCrossEnableConfPassword").value;

    //ALLDEV-826 Starts         
    if(document.getElementById("hdnCrossEnableHostGuestPwd").value != null)
 	    EnableHostGuestPwdOrgOption = document.getElementById("hdnCrossEnableHostGuestPwd").value;
    //ALLDEV-826 Ends

     if (EnableConferencePasswordOrgOption != null)
         if (EnableConferencePasswordOrgOption == "0")
         {
            //ALLDEV-826 Starts
            if (EnableHostGuestPwdOrgOption != null)
                if (EnableHostGuestPwdOrgOption == "0")
                {//ALLDEV-826 Ends
                   if(document.getElementById("trConfPass") != null && document.getElementById("trConfPass1") != null)
                    {
                        document.getElementById("trConfPass").style.display = 'None';
                        document.getElementById("trConfPass1").style.display = 'None';
                    }
                }//ALLDEV-826
         }
    //ALLDEV-826 Starts
    if (EnableHostGuestPwdOrgOption != null)
        if (EnableHostGuestPwdOrgOption == "0")
        {
            if (EnableConferencePasswordOrgOption != null)
             if (EnableConferencePasswordOrgOption == "0")
             {
                if(document.getElementById("trConfPass") != null && document.getElementById("trConfPass1") != null && document.getElementById("trHostPwd") != null && document.getElementById("trCfmHostPwd") != null)
                {   
                    document.getElementById("trConfPass").style.display = 'None';
                    document.getElementById("trConfPass1").style.display = 'None';         
                    document.getElementById("trHostPwd").style.display = 'None';
                    document.getElementById("trCfmHostPwd").style.display = 'None';
                }
            }
        }
    //ALLDEV-826 Ends
     if(document.getElementById("hdnCrossEnableLinerate").value != null)
 	    EnableLinerate = document.getElementById("hdnCrossEnableLinerate").value;
     
     if (EnableLinerate != null)
         if (EnableLinerate == "0")
         {
            if(document.getElementById("trLineRate"))
                document.getElementById("trLineRate").style.display = 'None'
            }

    if(document.getElementById("hdnCrossEnableBufferZone").value != null)
 	    EnableBufferZone = document.getElementById("hdnCrossEnableBufferZone").value;
     
     if (EnableBufferZone != null)
         if (EnableBufferZone == "0")
         {
            if(document.getElementById("SetupRow"))
                document.getElementById("SetupRow").style.display = 'None'
            }
                 
            
     //ZD 101233 END
     
    //FB 1911 - Start
    if ('<%=isEditMode%>' == "1" || "<%=isSpecialRecur %>" == "0"|| isSpecialRecur_Session == "0")//ZD 101233
    {
        if(document.getElementById("SPCell1"))
            document.getElementById("SPCell1").style.display = 'None'
        
        if(document.getElementById("SPCell2"))
            document.getElementById("SPCell2").style.display = 'None'
    }
    
	//if(document.getElementById("hdnValue").value == "" || document.getElementById("hdnValue").value == "0")
	//{
        if(document.getElementById("RecurSpec").value != "")
        {
           showSpecialRecur();
           document.getElementById("RecurText").value = document.getElementById("RecurringText").value;
        }
    //}
    //FB 1911 - End
    
    
    
   
    if(document.getElementById("lstDuration_Container") != null)
        document.getElementById("lstDuration_Container").style.width = "auto";
    
  function OpenRoomSearchresponse() //FB 2367
  {
  
    ChangeTimeFormat("D"); //FB 2588
    var dura = 0;
    //FB 2634
    var dStart = GetDefaultDate(document.getElementById("confStartDate").value,'<%=format%>');
    var dEnd = GetDefaultDate(document.getElementById("confEndDate").value,'<%=format%>');
    //var dSetup = GetDefaultDate(document.getElementById("SetupDate").value,'<%=format%>');
    var isROOMVMR = "0"; //FB 2448
    dStart = dStart + " " + document.getElementById("hdnStartTime").value; //FB 2588
    dEnd = dEnd + " " + document.getElementById("hdnEndTime").value;//FB 2588

    var confType = document.getElementById("lstConferenceType").value;

    //ZD 100085 Starts
    var chkrecurrence = document.getElementById("chkRecurrence");
    if ('<%=isEditMode%>' == "0" && "<%=isSpecialRecur %>" == "1" && document.getElementById("RecurSpec").value != "")
    {
        dura = parseInt(document.getElementById("RecurDurationhr").value,10) * 60;
        confdura = dura + parseInt(document.getElementById("RecurDurationmi").value,10) ;
    
        var entime = setCDuration(dStart, confdura)
        dEnd = GetCurrentdatetime(entime,0,0);
    }
    var cstartdate = new Date(GetDefaultDate(document.getElementById("confStartDate").value,'<%=format%>') + " "
                + document.getElementById("hdnStartTime").value); //FB 2588
         
    var Conncetdura = 0;        
    if(document.getElementById("txtMCUConnect") != null && document.getElementById("txtMCUConnect").value != 0)
       Conncetdura = parseInt(document.getElementById("txtMCUConnect").value,10);
    //ZD 100433 Starts
    var dtConfStart = dStart;  
    if(Conncetdura != 0 )
    {
        var connecttime = setCDuration(cstartdate, -Conncetdura)
        var connecttime1 = getCTime(connecttime);
    	
        dtConfStart = GetCurrentdatetime(connecttime,0,0);
    }
  
    //If MCU Pre End is negative, it will be considered for Room availability, below is the code.
    var disConncetdura = 0;
    if(document.getElementById("txtMCUDisConnect") != null && document.getElementById("txtMCUDisConnect").value < 0)
        disConncetdura = parseInt(document.getElementById("txtMCUDisConnect").value,10);
    
    if(disConncetdura < 0)
    {
        var cenddate = new Date(GetDefaultDate(document.getElementById("confEndDate").value,'<%=format%>') + " "
        + document.getElementById("hdnEndTime").value); //FB 2588
        
        var endtim = setCDuration(cenddate, -disConncetdura); //ZD 100405
        dEnd = GetCurrentdatetime(endtim,0,0);   
    }  
    var setupDur = 0, tearDur = 0;
    if('<%=EnableBufferZone%>' == 1)//FB 2634
    {
        setupDur = parseInt(document.getElementById("SetupDuration").value,10);
        
        if(setupDur > Conncetdura)
        {
            var sttime = setCDuration(cstartdate, -setupDur)
            var sttime1 = getCTime(sttime);
        	
            dtConfStart = GetCurrentdatetime(sttime,0,0);
        }
        //ZD 103865 START
        tearDur = parseInt(document.getElementById("TearDownDuration").value,10);
        
        if(tearDur > disConncetdura)
        {
            var cenddate = new Date(GetDefaultDate(document.getElementById("confEndDate").value,'<%=format%>') + " "
            + document.getElementById("hdnEndTime").value); 
        
//            var endtim = setCDuration(cenddate, -disConncetdura); 
//            dEnd = GetCurrentdatetime(endtim,0,0);   

            var sttime = setCDuration(cenddate, tearDur)
            //var sttime1 = getCTime(sttime);
        	dtConfEnd = GetCurrentdatetime(sttime,0,0);
            dEnd = dtConfEnd;
        }
        
    }
    dStart = dtConfStart;
    //ZD 103865 END
    //ZD 100433 End    
    //ZD 100085 End
    
     //dSetup = dEnd + " " + document.getElementById("SetupTime_Text").value;   //FB 2534
     //dSetup = dSetup + " " + document.getElementById("SetupTime_Text").value;   //FB 2534
     
     //FB 2534 - Starts
     /*if(new Date(dStart).getTime() < new Date().getTime() || new Date(dEnd).getTime() < new Date().getTime() || new Date(dSetup).getTime() < new Date().getTime())
     {
	   alert("Invalid Date/Time");
	   return false;
	 }*/
	 //FB 2534 - End
	 var immediate = "0";//FB 2534
	 if(document.getElementById("chkStartNow").checked)
	 {
	    immediate = "1";
	    //FB 2534 - Starts
	    var currentDate = new Date();
	    dStart = GetCurrentdatetime(currentDate,0,0);
	    //FB 2534 - End
	    if(document.getElementById("lstDuration_Text"))
	    {
	        var dura = document.getElementById("lstDuration_Text").value.split(':');
	        //FB 2534 - Starts
	        //dStart = GetCurrentdatetime(dura[0],dura[1]);
	        dEnd = GetCurrentdatetime(currentDate,dura[0],dura[1]);//FB 2534
	        //FB 2534 - End
	    }
	 }
	 if (document.getElementById("lstVMR")) //FB 2448
     {
        if (document.getElementById("lstVMR").selectedIndex > 0) //FB 2620
            isROOMVMR = "1";
     }
     var cloudConf = 0;
     if(document.getElementById("chkCloudConferencing") != null && document.getElementById("lstVMR") != null)
        if ('<%=isCloudEnabled%>' == 1 && document.getElementById("lstVMR").selectedIndex > 0 && document.getElementById("chkCloudConferencing").checked) //FB 2717
           cloudConf = 1;
    //ZD 100704 Starts 
    //ZD 100522
    if(confType == 9) //ZD 100513
        confType = 2;
	//ZD 103095 Starts
    var isRecurconf = "0";
    if(chkrecurrence && chkrecurrence.checked)
        isRecurconf = "1";
    else if(document.getElementById("hdnIsRecurConference"))
        isRecurconf = document.getElementById("hdnIsRecurConference").value;

    var url = "RoomSearch.aspx?rmsframe=" + document.getElementById("selectedloc").value + "&confID=" + '<%=lblConfID.Text%>'
            + "&stDate=" + dStart + "&enDate=" + dEnd + "&tzone=" + document.getElementById("<%=lstConferenceTZ.ClientID%>").value
            + "&serType=-1&hf=1" + "&isVMR=" + isROOMVMR + "&CloudConf=" + cloudConf + "&immediate=" + immediate + "&frm=frmExpress&ConfType=" + confType + "&isRecurrence=" + isRecurconf; //FB 2448 //FB 2534 //FB 2694//FB 2717 //ZD 100704
	//ZD 103095 End
    if(confType == 7)
        url += "&dedVid=";            
    if(confType == 4)
        url +=  "&isTel=";
   //ZD 100704 End     
   window.open(url, "RoomSearch", "width="+ screen. availWidth +",height=650px,resizable=no,scrollbars=yes,status=no,top=0,left=0"); 
    return false;
 }
 
 
 function GetCurrentdatetime(strDateTime, addHr,addmin) //FB 2367 //FB 2634-Starts
 {
	 var day = strDateTime.getDate();
     var month = strDateTime.getMonth() + 1;
     var year = strDateTime.getFullYear();
	 var hours = parseInt(strDateTime.getHours(),10) + parseInt(addHr,10);
     var minutes = parseInt(strDateTime.getMinutes(),10) + parseInt(addmin,10); //FB 2634-End
	 var suffix = "AM";
	 
     if (hours >= 12)
      {
          suffix = "PM";
          hours = hours - 12;
      }
     
      if (hours == 0)
          hours = 12;
      
      if(minutes < 10)
        minutes = "0" + minutes;
        
      if(month < 10)
        month = "0" + month;
           
      if(day < 10)
         day = "0" + day;
       
       if(hours < 10)
         hours = "0" + hours;
       //FB 2534 - Starts   
       //return  day + "/" + month + "/" + year + " " + hours + ":" + minutes + " " + suffix;
       return  month + "/" + day + "/" + year + " " + hours + ":" + minutes + " " + suffix;
       //FB 2534 - End
 }
 
    </script>

</body>
</html>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
<%--ZD 101388 Commented--%>
<%--<!-- FB 2719 Starts -->
<% if (Session["isExpressUser"].ToString() == "1"){%>
<!-- #INCLUDE FILE="inc/mainbottomNETExp.aspx" -->
<%}else{%>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
<%}%>
<!-- FB 2719 Ends -->--%>
<!--Window Dressing-->

<%--FB 1822--%>
<%--<script type="text/javascript" src="inc/softedge.js"></script>--%>
<script type="text/javascript">
    // FB 2828 Starts
    function setMarqueeWidth() {
        var screenWidth = screen.width - 25;
        if (document.getElementById('martickerDiv') != null)
            document.getElementById('martickerDiv').style.width = screenWidth + 'px';

        if (document.getElementById('marticDiv') != null)
            document.getElementById('marticDiv').style.width = screenWidth + 'px';

        if (document.getElementById('marticker2Div') != null)
            document.getElementById('marticker2Div').style.width = (screenWidth - 15) + 'px';

        if (document.getElementById('martic2Div') != null)
            document.getElementById('martic2Div').style.width = (screenWidth - 15) + 'px';
    }
    setMarqueeWidth();
    // FB 2828 Ends
    
    //FB 2426 Start
    function fnchangetype() {
        fnHideStartNow();//FB 1825
        
        //ZD 100221 Starts
        if (document.getElementById('txtPW1') != null)
            document.getElementById('txtPW1').type = "password";
        if (document.getElementById('txtPW2') != null)
            document.getElementById('txtPW2').type = "password";
        //ZD 100221 Ends
    }
    window.onload = fnchangetype;
    //FB 2426 End

    //FB 2487 - Start
    if (document.getElementById("showConfMsg") != null)
        var obj = document.getElementById("showConfMsg");

    if (obj != null) {
    
        var strInput = obj.innerHTML.toUpperCase();

        if (((strInput.indexOf("SUCCESS") > -1) && !(strInput.indexOf("UNSUCCESS") > -1) && !(strInput.indexOf("ERROR") > -1))
            || ((strInput.indexOf("EXITOSA") > -1) && !(strInput.indexOf("FALLIDA") > -1) && !(strInput.indexOf("ERROR") > -1))
            || ((strInput.indexOf("SUCC�S") > -1 || strInput.indexOf("�TABLIE") > -1) && !(strInput.indexOf("�CHEC") > -1) && !(strInput.indexOf("ERREUR") > -1)) //French            
            ) {
            obj.setAttribute("class", "lblMessage");
            obj.setAttribute("className", "lblMessage");
        }
        else {
            obj.setAttribute("class", "lblError");
            obj.setAttribute("className", "lblError");
        }
    }
    //FB 2487 - End

        


    //FB 2717 Starts
    function fnChkCloudConference() {

        var chkCloud = document.getElementById("chkCloudConferencing");
        if (chkCloud && chkCloud.checked) {
            var e = document.getElementById("lstVMR");
            var VMRType = e.options[e.selectedIndex].value;
            e.value = 3; //ZD 100753
            //document.getElementById("lstVMR").selectedIndex = 3;
            document.getElementById("lstVMR").disabled = true;
            document.getElementById("divbridge").style.display = "none";
        }
        else {
            document.getElementById("lstVMR").disabled = false;
            document.getElementById("lstVMR").selectedIndex = 0;//FB Vidyo_J
        }
        changeVMR();
    }
    //FB 2717 End
    
     //ZD 100890 Start
    function fnchkStatic() {
        var chkStatic = document.getElementById("chkStatic");
        if (document.getElementById("divStaticID") != null) {
            if (chkStatic && chkStatic.checked) {
                document.getElementById("divStaticID").style.display = "block";
            }
            else {
                document.getElementById("divStaticID").style.display = "none";
            }
        }
    }
    fnchkStatic(); //ZD 100969
    //ZD 100890 End
    
    //FB 2998
    if("<%=mcuSetupDisplay %>" == "1")
        document.getElementById("ConnectCell").style.display = "";
    else
        document.getElementById("ConnectCell").style.display = "None";
    if("<%=mcuTearDisplay %>" == "1")
        document.getElementById("DisconnectCell").style.display = "";
    else
        document.getElementById("DisconnectCell").style.display = "None";
</script>
<%---ZD 100428 Start Close the popup window using the esc key--%>
<script language="javascript" type="text/javascript">

   function EscClosePopup() {
           if (document.getElementById('PopupLdapPanel') != null &&
                   document.getElementById('btnGuestLocation2_backgroundElement') != null) {
                   fnDisableValidator();  
               document.getElementById('PopupLdapPanel').style.display = "none";
               document.getElementById('btnGuestLocation2_backgroundElement').style.display = "none";

               document.getElementById('PanloadFiles').style.display = "none";
               document.getElementById('aFileUp_backgroundElement').style.display = "none";

               if (document.getElementById('SubModalPopupExtender_backgroundElement').style.display != "none") {
                   document.getElementById('SubModalPopupExtender_backgroundElement').style.display = "none";
                   document.getElementById('panSubmit').style.display = "none";
                   window.location.replace('ExpressConference.aspx?t=n');
                   return false;
               }
               
           }
   }
   //ZD 100666 ZD 101028 //Commented for AddressBook Focus in IE Browser.
   if (document.getElementById('VRMLookup') != null)
     // document.getElementById('VRMLookup').setAttribute("onblur", "document.getElementById('btnConfSubmit').focus(); document.getElementById('btnConfSubmit').setAttribute('onfocus', '');"); 
        
   if (document.getElementById('btnGuestLocation') != null)
       document.getElementById('btnGuestLocation').setAttribute("onblur", "document.getElementById('txtsiteName').focus();");

   if(document.getElementById('imgVNOC') == null)
       document.getElementById('ancimgVNOC').removeAttribute('href');

   if (document.getElementById('imgdeleteVNOC') == null)
       document.getElementById('ancimgdeleteVNOC').removeAttribute('href');

   function fnHandleWebExPwd() { // ZD PRABU
       if (document.getElementById("chkWebex") != null) {
           if (document.getElementById("chkWebex").checked) {
               if (document.getElementById("txtPW1") != null) {
                   var val = document.getElementById("hdnWebExPwd").value;
                   if (val != "") {
                       document.getElementById("txtPW1").value = val;
                       document.getElementById("txtPW2").value = val;
                       ValidatorEnable(document.getElementById('RequiredFieldValidator7'), false);
                   }
                   else
                       ValidatorEnable(document.getElementById('RequiredFieldValidator7'), true);
               }
           }
       }
   }
  fnHandleWebExPwd();
  fnShowWebExConf(); // ZD 100221 Email

  //ZD 100704 Starts
  if ("<%=EnableExpressConfType %>" == 0) {
      document.getElementById("hdnconftype").value = "2";
      document.getElementById("lstConferenceType").value = "2";
  }
  if ("<%=enableCloudInstallation %>" == 0)
      fnChangeConferenceType();
      
    //ZD 101931 - Start
    function showHideAdvSettings() {

        var AdvSettings = "0";
         var conftype =  document.getElementById("lstConferenceType").value;
         var VMRtype = document.getElementById("lstVMR").value;
         if ("<%=EnableFECC%>" == "1" || "<%=EnableAudioBridges%>" == 1 || "<%=mcuSetupDisplay%>" == "1" || "<%=mcuTearDisplay%>" == "1" || ("<%=EnableProfileSel%>" == "1" && "<%=RMXbrid%>" == true) || ("<%=EnablePoolOrder%>" == "1" && "<%=PoolOrderMCU%>" == true) || '<%=Session["ShowVideoLayout"]%>' == "0" || '<%=Session["ShowVideoLayout"]%>' == "2")  //ZD 104256
         {
              AdvSettings = "1";
              if((conftype == 4  && "<%=EnableAudioBridges%>" == 0) || (conftype == 7  && "<%=EnableAudioBridges%>" == 0))
                  AdvSettings = "0";
              if((VMRtype == 1  && "<%=EnableAudioBridges%>" == 0) || (VMRtype == 3  && "<%=EnableAudioBridges%>" == 0))
                  AdvSettings = "0";
        }
          if (conftype == 4 || conftype == 7 || VMRtype == 1 || VMRtype == 3 || (document.getElementById('chkPCConf') != null && document.getElementById('chkPCConf').checked)) {
            if (document.getElementById("tdMCUProfile") != null)
                document.getElementById("tdMCUProfile").style.display = "none";
            if (document.getElementById("trPoolOrderSelection") != null) //ZD 104256 
                document.getElementById("trPoolOrderSelection").style.display = "none";
               }


        if (AdvSettings == "0") {
            document.getElementById('trAdvancedSettings').style.display = "none";
            document.getElementById('Advancesetting').style.display = "none";
            if (document.getElementById('chkAdvancesetting') != null) {
                if (document.getElementById('chkAdvancesetting').checked == true)
                    document.getElementById('chkAdvancesetting').checked = false;
            }
        }
        else {
            if (document.getElementById("trAdvancedSettings") != null && "<%=enableCloudInstallation %>" == 0 && conftype != 8)
                document.getElementById('trAdvancedSettings').style.display = "";
        }

    }
    //ZD 101931 - End

  function fnChangeConferenceType() {
      var e = document.getElementById("lstVMR");
      var VMRType = e.options[e.selectedIndex].value;
      e = document.getElementById("lstConferenceType");
      var confType = e.options[e.selectedIndex].value;
      
      switch (confType) {
          case "7":
              VMRType = "0";
              if (document.getElementById("lstVMR") != null)//ZD 100381
                  document.getElementById("lstVMR").selectedIndex = VMRType;
              if (document.getElementById("trConfDesc") != null) document.getElementById("trConfDesc").style.display = "";
              if (document.getElementById("trConfTitle") != null) document.getElementById("trConfTitle").style.display = "";
              if (document.getElementById("trPublic") != null && "<%=EnablePublicConf %>" == "1") document.getElementById("trPublic").style.display = "";
              if (document.getElementById("trConcSupport") != null) document.getElementById("trConcSupport").style.display = "";
              if (document.getElementById("trAudioConf") != null && "<%=EnableAudioBridges %>" == "1") document.getElementById("trAudioConf").style.display = "";
              if (document.getElementById("StartNowRow") != null && '<%=EnableImmConf %>' == "1") document.getElementById("StartNowRow").style.display = "";
              if (document.getElementById("tdParty") != null) document.getElementById("tdParty").style.display = "";
              if (document.getElementById("tbParty") != null && '<%=enableDetailedExpressForm %>' == "0") document.getElementById("tbParty").style.width = "50%";
              if (document.getElementById("trExternalUsers") != null) document.getElementById("trExternalUsers").style.display = "none";
              if (document.getElementById("tdPartyButtons") != null) document.getElementById("tdPartyButtons").style.display = "";
              if (document.getElementById("trAdvancedSettings") != null && "<%=enableCloudInstallation %>" == 0 && "<%=AdvSettings%>" == "1") {
                  document.getElementById("trAdvancedSettings").style.display = "";
              }
              //ZD 101755 Start
              if ('<%=EnableBufferZone%>' == "1" && document.getElementById("SetupRow") != null && '<%=Session["EnableSetupTimeDisplay"]%>' == "1")
                  document.getElementById("SetupRow").style.display = "";
              else
                  document.getElementById("SetupRow").style.display = 'None';
              if ('<%=EnableBufferZone%>' == "1" && document.getElementById("TearDownRow") != null && '<%=Session["EnableTeardownTimeDisplay"]%>' == "1")
                  document.getElementById("TearDownRow").style.display = "";
              else
                  document.getElementById("TearDownRow").style.display = 'None';
                //ZD 101755 End 
                  
              if (document.getElementById("StartNowRow") != null && '<%=EnableImmConf %>' == "1") document.getElementById("StartNowRow").style.display = "";
              if ('<%=isEditMode%>' == "0" && "<%=isSpecialRecur %>" == "1") {
                  document.getElementById("SPCell1").style.display = '';
                  document.getElementById("SPCell2").style.display = '';
              }

              if (document.getElementById("chkBJNMeetingID") != null)//ZD 104116
                  document.getElementById("chkBJNMeetingID").checked = false;

              if (document.getElementById("DivBJNMeetingID") != null)//ZD 104116
                  document.getElementById("DivBJNMeetingID").style.display = "none";

              var objList1 = ["TrCTNumericID", "ChkEnableNumericID", "trPCConf", "trConfPass", "trConfPass1", "trBJNMeeting", "trVMR", "MCUConnectDisplayRow", "trNetworkState", "trFECC", "tdMCUProfile", "trStartMode", "trLineRate", "trLayout", "trPoolOrderSelection", "trHostPwd", "trCfmHostPwd"]; //ZD 101869 //ZD 104256 //ALLDEV-826
              for (var i = 0; i < objList1.length; i++) {
                  if (document.getElementById(objList1[i]) != null)
                      document.getElementById(objList1[i]).style.display = "none";

              }

              if (document.getElementById("chkPCConf") != null) document.getElementById("chkPCConf").checked = false;
              if (document.getElementById("chkFECC") != null) document.getElementById("chkFECC").checked = false;
              if (document.getElementById("txtMCUConnect") != null) document.getElementById("txtMCUConnect").value = "0";
              if (document.getElementById("txtMCUDisConnect") != null) document.getElementById("txtMCUDisConnect").value = "0";
              if (document.getElementById("drpNtwkClsfxtn") != null) document.getElementById("drpNtwkClsfxtn").selectedvalue = "0";
              
              if (document.getElementById("hdnConferenceName") != null && document.getElementById("hdnConferenceName").value != "") {
                  document.getElementById("ConferenceName").value = "";
                  document.getElementById("hdnConferenceName").value = "";
              }
              document.getElementById("CreateBy").value = confType; //ZD 100834
              if (EnableTemplatebooking_Session == "1" && confType != 8)
                document.getElementById("trConfTemp").style.display = ""; //ZD 100522
              changeVMR();
              break;
          case "4":
              VMRType = "0";
              if (document.getElementById("lstVMR") != null)//ZD 100381
                  document.getElementById("lstVMR").selectedIndex = VMRType;
              if (document.getElementById("trConfDesc") != null) document.getElementById("trConfDesc").style.display = "";
              if (document.getElementById("trConfTitle") != null) document.getElementById("trConfTitle").style.display = "";
              if (document.getElementById("trPublic") != null && "<%=EnablePublicConf %>" == "1") document.getElementById("trPublic").style.display = "";
              if (document.getElementById("trConcSupport") != null) document.getElementById("trConcSupport").style.display = "";
              if (document.getElementById("trAudioConf") != null && "<%=EnableAudioBridges %>" == "1") document.getElementById("trAudioConf").style.display = "";
              if (document.getElementById("StartNowRow") != null && '<%=EnableImmConf %>' == "1") document.getElementById("StartNowRow").style.display = "";
              //ALLDEV-826 Starts
              if (document.getElementById("trConfPass") != null && "<%=EnableConferencePassword %>" == 1 || "<%=EnableHostGuestPwd %>" == 1) document.getElementById("trConfPass").style.display = "";
              if (document.getElementById("trConfPass1") != null && "<%=EnableConferencePassword %>" == 1 || "<%=EnableHostGuestPwd %>" == 1) document.getElementById("trConfPass1").style.display = "";
              if (document.getElementById("trHostPwd") != null && "<%=EnableHostGuestPwd %>" == 1) document.getElementById("trHostPwd").style.display = "";
              if (document.getElementById("trCfmHostPwd") != null && "<%=EnableHostGuestPwd %>" == 1) document.getElementById("trCfmHostPwd").style.display = "";
              //ALLDEV-826 Ends
              if (document.getElementById("tdParty") != null) document.getElementById("tdParty").style.display = "";
              if (document.getElementById("tbParty") != null && '<%=enableDetailedExpressForm %>' == "0") document.getElementById("tbParty").style.width = "50%";
              if (document.getElementById("trExternalUsers") != null) document.getElementById("trExternalUsers").style.display = "";
              if (document.getElementById("tdPartyButtons") != null) document.getElementById("tdPartyButtons").style.display = "";
              if (document.getElementById("trAudioConf") != null && "<%=EnableAudioBridges %>" == "1") document.getElementById("trAudioConf").style.display = "";
              if (document.getElementById("trAdvancedSettings") != null && "<%=enableCloudInstallation %>" == 0  && "<%=AdvSettings%>" == "1") {
                  document.getElementById("trAdvancedSettings").style.display = "";
              }
              //ZD 101755 Start
              if ('<%=EnableBufferZone%>' == "1" && document.getElementById("SetupRow") != null && '<%=Session["EnableSetupTimeDisplay"]%>' == "1")
                  document.getElementById("SetupRow").style.display = "";
              else
                  document.getElementById("SetupRow").style.display = 'None';
              if ('<%=EnableBufferZone%>' == "1" && document.getElementById("TearDownRow") != null && '<%=Session["EnableTeardownTimeDisplay"]%>' == "1")
                  document.getElementById("TearDownRow").style.display = "";
              else
                  document.getElementById("TearDownRow").style.display = 'None';
              //ZD 101755 End 
              if (document.getElementById("trLineRate") != null && "<%=EnableLinerate %>" == "1")
                  document.getElementById("trLineRate").style.display = "";
              if ('<%=isEditMode%>' == "0" && "<%=isSpecialRecur %>" == "1") {
                  document.getElementById("SPCell1").style.display = '';
                  document.getElementById("SPCell2").style.display = '';
              }

              if (document.getElementById("chkBJNMeetingID") != null)//ZD 104116
                  document.getElementById("chkBJNMeetingID").checked = false;

              if (document.getElementById("DivBJNMeetingID") != null)//ZD 104116
                  document.getElementById("DivBJNMeetingID").style.display = "none";

              var objList1 = ["TrCTNumericID", "ChkEnableNumericID", "trPCConf", "trStartMode", "MCUConnectDisplayRow", "trBJNMeeting", "trVMR", "tdMCUProfile", "trLayout", "trPoolOrderSelection"]; //ZD 101869 //ZD 104256
              for (var i = 0; i < objList1.length; i++) {
                  if (document.getElementById(objList1[i]) != null)
                      document.getElementById(objList1[i]).style.display = "none";
              }

              if (document.getElementById("chkPCConf") != null) document.getElementById("chkPCConf").checked = false;
              if (document.getElementById("txtMCUConnect") != null) document.getElementById("txtMCUConnect").value = 0;
              if (document.getElementById("txtMCUDisConnect") != null) document.getElementById("txtMCUDisConnect").value = 0;
              if (document.getElementById("hdnConferenceName") != null && document.getElementById("hdnConferenceName").value != "") {
                  document.getElementById("ConferenceName").value = "";
                  document.getElementById("hdnConferenceName").value = "";
              }
              document.getElementById("CreateBy").value = confType; //ZD 100834
              if (EnableTemplatebooking_Session == "1" && confType != 8)
                document.getElementById("trConfTemp").style.display = ""; //ZD 100522
              changeVMR();
              break;
          case "8":
              VMRType = "0";
              if (document.getElementById("txtPartysInfo") != null) {
                  document.getElementById("txtPartysInfo").value = "";
              }
              if (document.getElementById("lstVMR") != null)//ZD 100381
                  document.getElementById("lstVMR").selectedIndex = VMRType;
              var objList = ["txtMCUConnect", "txtMCUDisConnect", "drpNtwkClsfxtn", "SetupDuration", "lstStartMode"];
              for (var i = 0; i < objList.length; i++) {
                  if (document.getElementById(objList[i]) != null)
                      document.getElementById(objList[i]).value = "0";
              }
              var objList1 = ["ChkEnableNumericID", "chkPCConf", "chkFECC", "chkOnSiteAVSupport", "chkMeetandGreet", "chkConciergeMonitoring", "chkDedicatedVNOCOperator", "chkAdvancesetting"];
              for (var i = 0; i < objList1.length; i++) {
                  if (document.getElementById(objList1[i]) != null)
                      document.getElementById(objList1[i]).checked = false;
              }
              

              if (document.getElementById("chkBJNMeetingID") != null)//ZD 104116
                  document.getElementById("chkBJNMeetingID").checked = false;

              if (document.getElementById("DivBJNMeetingID") != null)//ZD 104116
                  document.getElementById("DivBJNMeetingID").style.display = "none";

              var objList2 = ["trConfDesc", "trConfTitle", "trConfTemp", "trStartMode", "trPublic", "trCloudConferencing", "trBJNMeeting", "trVMR", "TrCTNumericID", "ChkEnableNumericID", "trConcSupport", "MCUConnectDisplayRow",
                                "trNetworkState", "chkPCConf", "trPCConf", "tdMCUProfile", "trConfPass", "trConfPass1", "trFECC", "trAudioConf",
                                "tdPartyButtons", "StartNowRow", "SetupRow", "trLineRate", "SPCell1", "SPCell2", "recurDIV", "trExternalUsers", "tdParty", "trAdvancedSettings", "Advancesetting", "trPoolOrderSelection", "trHostPwd", "trCfmHostPwd"]; //ZD 104256 //ALLDEV-826
              for (var i = 0; i < objList2.length; i++) {
                  if (document.getElementById(objList2[i]) != null)
                      document.getElementById(objList2[i]).style.display = "none";
              }

              if ((document.getElementById("ConferenceName") != null) && (document.getElementById("ConferenceName").value != "Hotdesking Reservation") && ('<%=ViewState["DConftype"] %>' == "") && (document.getElementById("hdnIsHDBusy").value != "1")) { // ZD 102123 //ALLDEV-807
                  document.getElementById("locstrname").value = "";
                  document.getElementById("selectedloc").value = "";
                  document.getElementById("RoomList").innerHTML = "";
              }
              if (document.getElementById("hdnVNOCOperator") != null) document.getElementById("hdnVNOCOperator").value = "";
              if (document.getElementById("txtVNOCOperator") != null) document.getElementById("txtVNOCOperator").value = "";
              if (document.getElementById("txtNumeridID") != null) document.getElementById("txtNumeridID").value = "";
              if (document.getElementById("ConferenceName") != null) document.getElementById("ConferenceName").value = "Hotdesking Reservation"; //ZD 103095
              if (document.getElementById("ChkEnableNumericID") != null) document.getElementById("ChkEnableNumericID").checked = false;
              if (document.getElementById("hdnConferenceName") != null) document.getElementById("hdnConferenceName").value = document.getElementById("ConferenceName").value;
              //if (document.getElementById("tdParty") != null) document.getElementById("tdParty").style.visibility = "hidden";
              if (document.getElementById("tdParty") != null) document.getElementById("tdParty").style.width = "0";
              if (document.getElementById("hdnSpecRec") != null) document.getElementById("hdnSpecRec").value = "";
              if (document.getElementById("RecurSpec") != null) document.getElementById("RecurSpec").value = "";
              if (document.getElementById("RecurringText") != null) document.getElementById("RecurringText").value = "";
              if (document.getElementById("RecurText") != null) document.getElementById("RecurText").value = "";
              changeVMR();
              visControls();
              //openRecur(); //ZD 102388
              break;
          default: //For Audio only and AudioVideo Conf Type.
              if (document.getElementById("hdnConferenceName") != null && document.getElementById("hdnConferenceName").value != "") {
                  document.getElementById("ConferenceName").value = "";
                  document.getElementById("hdnConferenceName").value = "";
              }
              if (document.getElementById("trLayout") != null) document.getElementById("trLayout").style.display = ""; //ZD 101869
              if (document.getElementById("trConfDesc") != null) document.getElementById("trConfDesc").style.display = "";
              if (document.getElementById("trConfTitle") != null) document.getElementById("trConfTitle").style.display = "";
              //ALLDEV-826 Starts
              if (document.getElementById("trConfPass") != null && "<%=EnableConferencePassword %>" == 1 || "<%=EnableHostGuestPwd %>" == 1) document.getElementById("trConfPass").style.display = "";
              if (document.getElementById("trConfPass1") != null && "<%=EnableConferencePassword %>" == 1 || "<%=EnableHostGuestPwd %>" == 1) document.getElementById("trConfPass1").style.display = "";
              if (document.getElementById("trHostPwd") != null && "<%=EnableHostGuestPwd %>" == 1)  document.getElementById("trHostPwd").style.display = "";
              if (document.getElementById("trCfmHostPwd") != null && "<%=EnableHostGuestPwd %>" == 1)  document.getElementById("trCfmHostPwd").style.display = "";
              //ALLDEV-826 Ends
              if (document.getElementById("trStartMode") != null) document.getElementById("trStartMode").style.display = "";
              if (document.getElementById("trPublic") != null && "<%=EnablePublicConf %>" == "1") document.getElementById("trPublic").style.display = "";
              if (document.getElementById("trCloudConferencing") != null && "<%=isCloudEnabled %>" == "1") document.getElementById("trCloudConferencing").style.display = "";
              if (document.getElementById("TrCTNumericID") != null && "<%=EnableNumericID %>" == "1") document.getElementById("TrCTNumericID").style.display = "";
              if (document.getElementById("ChkEnableNumericID") != null && "<%=EnableNumericID %>" == "1") document.getElementById("ChkEnableNumericID").style.display = "";
              if (document.getElementById("trConcSupport") != null) document.getElementById("trConcSupport").style.display = "";
              if (document.getElementById("trNetworkState") != null && "<%=NetworkSwitching %>" == 2) document.getElementById("trNetworkState").style.display = "";
              if (document.getElementById("chkPCConf") != null && "<%=EnablePCUser %>" == 1) document.getElementById("chkPCConf").style.display = "";
              //ZD 104116 - Start
              if ('<%=Session["EnableBJNIntegration"] %>' == 1 && '<%=Session["BJNDisplay"] %>' == 0)
                  document.getElementById("trPCConf").style.display = "none";
              else {
                  if (document.getElementById("trPCConf") != null && "<%=EnablePCUser %>" == 1 && document.getElementById("chkBJNMeetingID") != null && document.getElementById("chkBJNMeetingID").checked == false)
                      document.getElementById("trPCConf").style.display = "";
              }
              if ('<%=Session["EnableBJNIntegration"] %>' == 1 && '<%=Session["BJNDisplay"] %>' == 1) {
                  document.getElementById("trBJNMeeting").style.display = "block";

                  if ('<%=isEditMode%>' == "0") {
                      if (document.getElementById("chkBJNMeetingID") != null)
                          document.getElementById("chkBJNMeetingID").checked = true;

                      if ('<%=Session["BJNSelectOption"] %>' == 1) {
                          if (document.getElementById("DivBJNMeetingID") != null)
                              document.getElementById("DivBJNMeetingID").style.display = 'block';
                      }
                  }

                  if (document.getElementById("hdnChangeBJNVal") != null) {
                      if (document.getElementById("hdnChangeBJNVal").value == "0") {
                          if (document.getElementById("chkBJNMeetingID") != null)
                              document.getElementById("chkBJNMeetingID").checked = false;
                      }
                  }
                  
              }
              //ZD 104116 - End
              if (document.getElementById("tdMCUProfile") != null) document.getElementById("tdMCUProfile").style.display = "";
              if (document.getElementById("trPoolOrderSelection") != null) document.getElementById("trPoolOrderSelection").style.display = ""; //ZD 104256
              if (document.getElementById("trAudioConf") != null && "<%=EnableAudioBridges %>" == "1") document.getElementById("trAudioConf").style.display = "";
              if (document.getElementById("StartNowRow") != null && '<%=EnableImmConf %>' == "1") document.getElementById("StartNowRow").style.display = "";
              //if (document.getElementById("trExternalUsers") != null && '<%=enableDetailedExpressForm %>' == "1") document.getElementById("trExternalUsers").style.display = "";
              if (document.getElementById("tbParty") != null && '<%=enableDetailedExpressForm %>' == "0") document.getElementById("tbParty").style.width = "50%";
              if (document.getElementById("tdParty") != null) document.getElementById("tdParty").style.display = "";
              if (document.getElementById("tdPartyButtons") != null) document.getElementById("tdPartyButtons").style.display = "";
              if (document.getElementById("trAdvancedSettings") != null && "<%=enableCloudInstallation %>" == 0  && "<%=AdvSettings%>" == "1") {
                  document.getElementById("trAdvancedSettings").style.display = "";
              }
              if (document.getElementById("trLineRate") != null && "<%=EnableLinerate %>" == "1")
                  document.getElementById("trLineRate").style.display = "";
              //ZD 101755 Start
              if ('<%=EnableBufferZone%>' == "1" && document.getElementById("SetupRow") != null && '<%=Session["EnableSetupTimeDisplay"]%>' == "1")
                  document.getElementById("SetupRow").style.display = "";
              else
                  document.getElementById("SetupRow").style.display = 'None';
              if ('<%=EnableBufferZone%>' == "1" && document.getElementById("TearDownRow") != null && '<%=Session["EnableTeardownTimeDisplay"]%>' == "1")
                  document.getElementById("TearDownRow").style.display = "";
              else
                  document.getElementById("TearDownRow").style.display = 'None';
              //ZD 101755 End 
              if ('<%=isEditMode%>' == "0" && "<%=isSpecialRecur %>" == "1") {
                  document.getElementById("SPCell1").style.display = '';
                  document.getElementById("SPCell2").style.display = '';
              }
              //ZD 104116 - Start
              if ('<%=Session["EnableBJNIntegration"] %>' == 1 && '<%=Session["BJNDisplay"] %>' == 0)
                  document.getElementById("trVMR").style.display = "none";
              else {
                  if (('<%=Session["ShowHideVMR"]%>' == 1) && ('<%=Session["EnablePersonaVMR"]%>' == 1 || '<%=Session["EnableRoomVMR"]%>' == 1 || '<%=Session["EnableExternalVMR"]%>' == 1)) {
                      document.getElementById("trVMR").style.display = "";
                  }
              }
              //ZD 104116 - End

              if (document.getElementById("trExternalUsers") != null) {
                  if (VMRType > 0 || (document.getElementById("chkPCConf") != null && document.getElementById("chkPCConf").checked) || (document.getElementById("chkCloudConferencing") != null && document.getElementById("chkCloudConferencing").checked))
                      document.getElementById("trExternalUsers").style.display = "none";
                  else if ('<%=enableDetailedExpressForm %>' == "1")
                      document.getElementById("trExternalUsers").style.display = "";
              }


              document.getElementById("CreateBy").value = confType; //ZD 100834
              if (EnableTemplatebooking_Session == "1" && confType != 8)
                document.getElementById("trConfTemp").style.display = ""; //ZD 100522
              changeVMR();
              break;
      }

  }
  //ZD 100704 End

  //ZD 100834 Starts
  function fnReloadExternalGrid() {
      fnExternalGridValidator("0");
      var confType = document.getElementById("lstConferenceType").value;

      var isPCCOnf;
      if (document.getElementById("chkPCConf") != null)
          isPCCOnf = document.getElementById("chkPCConf").checked;

      var VMRselected = 0;
      if (document.getElementById("lstVMR") != null)
          VMRselected = document.getElementById("lstVMR").selectedIndex;


      var vmrParty = document.getElementById("isVMR");
      if (VMRselected > 0)
          vmrParty.value = "1";
      else
          vmrParty.value = "0";

      document.getElementById("CreateBy").value = confType;

      if (document.getElementById("txtPartysInfo").value.trim() != "")
          document.getElementById("hdnParty").value = document.getElementById("txtPartysInfo").value;

      refreshIframe();

      if (document.getElementById("hdnParty").value != "" && document.getElementById("txtPartysInfo").value.trim() != "")
          document.getElementById("txtPartysInfo").value = document.getElementById("hdnParty").value;

      CheckParty();
  }

  function addExternalParty() {
      var e = document.getElementById("lstVMR");
      var VMRType = e.options[e.selectedIndex].value;
      e = document.getElementById("lstConferenceType");
      var confType = e.options[e.selectedIndex].value;
      if (document.getElementById("trExternalUsers") != null) {
          if ((VMRType > 0 && VMRType != 2)  || confType == 8 || confType == 7 || (document.getElementById("chkPCConf") != null && document.getElementById("chkPCConf").checked) || (document.getElementById("chkCloudConferencing") != null && document.getElementById("chkCloudConferencing").checked))
              document.getElementById("trExternalUsers").style.display = "none";
          else if ('<%=enableDetailedExpressForm %>' == "1")
              document.getElementById("trExternalUsers").style.display = "";
      }
      fnExternalGridValidator("0");
      document.getElementById("btnSample").click();
  }


  function fnExternalGridValidator(enableValidator) {

    //ALLDEV-826 Starts
    if(document.getElementById("ConferencePassword") != null)
    {
        if (document.getElementById("txtHostPwd") != null) 
        {
            var confPwd = "", hostPwd = "", reqValHostPwd = "";
            confPwd = document.getElementById("ConferencePassword");
            hostPwd = document.getElementById("txtHostPwd");
            reqValHostPwd = document.getElementById("reqValHostPwd");

            if (confPwd.value != "" && hostPwd.value == "") {
                ValidatorEnable(reqValHostPwd, true);
            }
            else{
                ValidatorEnable(reqValHostPwd, false);
            }
            if (document.getElementById('lstVMR') != null) {
                if (document.getElementById('lstVMR').value == "2") {                    
                    ValidatorEnable(reqValHostPwd, false);
                }
            }
        }
    }
    //ALLDEV-826 Ends
      if (document.getElementById("dgUsers")) {

          var objMain = document.getElementById("dgUsers");

          var hdnextusrs = document.getElementById("hdnextusrcnt");
          var extusrcnt = 0;
          var controlName = 'dgUsers_ctl';
          var ctlid = '';
          if (hdnextusrs) {
              if (hdnextusrs.value != '')
                  extusrcnt = parseInt(hdnextusrs.value, 10); // ZD 101722
          }
          if (extusrcnt > 0) {
              var conftype = document.getElementById("hdnconftype");
              for (var i = 1; i <= extusrcnt; i++) {
                  ctlid = "";
                  if (i < 10)
                      ctlid = '0' + (i + 1);
                  else
                      ctlid = i + 1;

                  var ReqAddress = document.getElementById(controlName + ctlid + "_" + "reqAddress");
                  var ReqAddressType = document.getElementById((controlName + ctlid + "_" + 'reqAddressType'));
                  var ReqLineRate = document.getElementById((controlName + ctlid + "_" + 'reqLineRate'));
                  var ReqConnectionType = document.getElementById((controlName + ctlid + "_" + 'reqConnectionType'));

                  if (enableValidator == "1") {
                      ValidatorEnable(ReqAddress, true);
                      ValidatorEnable(ReqAddressType, true);
                      ValidatorEnable(ReqLineRate, true);
                      ValidatorEnable(ReqConnectionType, true);
                  }
                  else if (enableValidator == "0") {
                      ValidatorEnable(ReqAddress, false);
                      ValidatorEnable(ReqAddressType, false);
                      ValidatorEnable(ReqLineRate, false);
                      ValidatorEnable(ReqConnectionType, false);
                  }
              }
          }
      }
      return true;
  }

  fnExternalGridValidator("0");

  //ZD 100834 End

  function ExpandAll() {

      var bool = "0";
      if (document.getElementById("ImgbtnAddress").src.indexOf('nolines_minus.gif') > -1)
          bool = "0";
      else
          bool = "1";

      if (bool == "1") {
          document.getElementById('tblAddress').style.visibility = 'visible';
          document.getElementById("ImgbtnAddress").src = "../image/loc/nolines_minus.gif";
      }
      else if (bool == "0") {
          document.getElementById('tblAddress').style.visibility = 'hidden';
          document.getElementById("ImgbtnAddress").src = "../image/loc/nolines_plus.gif";
      }
      document.getElementById("expColStat").value = bool;
      return false;

  }

  function fnSetExpColStatus() {
      if (document.getElementById("expColStat") != null) {
          if (document.getElementById("expColStat")) {
              if (document.getElementById("expColStat").value == "1") {
                  document.getElementById("ImgbtnAddress").src = "../image/loc/nolines_plus.gif";
                  ExpandAll();
              }
          }
      }
  }

  document.onkeydown = function (evt) {
      evt = evt || window.event;
      var keyCode = evt.keyCode;
      if (keyCode == 8) {
          if (document.getElementById("btnCancel") != null) { // backspace
              var str = document.activeElement.type;
              if (!(str == "text" || str == "textarea" || str == "password")) {
                  document.getElementById("btnCancel").click();
                  return false;
              }
          }
          if (document.getElementById("btnGoBack") != null) { // backspace
              var str = document.activeElement.type;
              if (!(str == "text" || str == "textarea" || str == "password")) {
                  document.getElementById("btnGoBack").click();
                  return false;
              }
          }
      }
      //ZD 103872 - Start
      if (keyCode == 13) 
      {                    
            var str = document.activeElement.type;
            if ((str == "text" || str == "textarea" || str == "password"))                                 
                return false;
      }
      //ZD 103872 - End
      fnOnKeyDown(evt);
  };


  if (document.getElementById("topExpDiv") != null)
      document.getElementById("topExpDiv").style.display = 'none';

  //ZD 101500 - Start
  if ('<%= Session["hdModalDiv"]%>' == "none") 
  {
      if (document.getElementById("topExpDiv1") != null)
          document.getElementById("topExpDiv1").style.display = '<%= Session["hdModalDiv"]%>';
  }
  //ZD 101500 - End

  //ZD 100875

   function closeMe(evt) {
       
       var needAlert = false;
       var confirmCheck = true;

       if (document.getElementById("hdnSaveData") != null) {
           if (document.getElementById("hdnSaveData").value == "1")
               confirmCheck = false;
       }
       
       if (confirmCheck) {
           if (document.getElementById("ConferenceName") != null) {
               if (document.getElementById("ConferenceName").value != "")
                   needAlert = true;
           }

           if (document.getElementById("selectedloc") != null) {
               if (document.getElementById("selectedloc").value.trim() != "")
                   needAlert = true;
           }

           if (document.getElementById("txtPartysInfo") != null) {
               if (document.getElementById("txtPartysInfo").value.trim().replace('|','') != "")
                   needAlert = true;
           }
           
       }

       if (needAlert == true)
           return "";
           //return "Are you sure you want to navigate away from this reservation? This meeting has not yet been scheduled and will not be saved.";
   }
   window.onbeforeunload = closeMe;

   if (document.getElementById('btnGuestLocationSubmit') != null && document.getElementById('ClosePUp') != null && document.getElementById('btnGuestLocationSubmit') != null) {
       document.getElementById('ClosePUp').setAttribute("onblur", "document.getElementById('btnGuestAddProfile').focus()");
       document.getElementById('btnGuestAddProfile').setAttribute("onfocus", "");
       document.getElementById('btnGuestAddProfile').setAttribute("onblur", "document.getElementById('btnGuestLocationSubmit').focus()");
       document.getElementById('btnGuestLocationSubmit').setAttribute("onfocus", "");
   }

   // ZD 101444 Starts
   function fnUpdateStartList() {
       if (document.getElementById('confStartTime') != null && document.getElementById('confStartTime_Text') != null)
           if (document.getElementById('confStartTime').innerHTML.toString().indexOf(document.getElementById('confStartTime_Text').value) > -1)
               document.getElementById('confStartTime').value = document.getElementById('confStartTime_Text').value;
   }
   function fnUpdateEndList() {
       if (document.getElementById('confEndTime') != null && document.getElementById('confEndTime_Text') != null)
           if (document.getElementById('confEndTime').innerHTML.toString().indexOf(document.getElementById('confEndTime_Text').value) > -1)
               document.getElementById('confEndTime').value = document.getElementById('confEndTime_Text').value;
   }
   //document.getElementById("confStartTime_Text").setAttribute("onchange", "fnUpdateStartList();");
   //document.getElementById("confEndTime_Text").setAttribute("onchange", "fnUpdateEndList();");
   fnUpdateStartList();
   fnUpdateEndList();
   // ZD 101444 Ends

   //ZD 101562 START
   var EnableTemplatebooking_Session;
   if (document.getElementById("hdntemplatebooking").value != null)
       EnableTemplatebooking_Session = document.getElementById("hdntemplatebooking").value;
   else
       EnableTemplatebooking_Session = '<%=Session["EnableTemplateBooking"] %>';

   confType = document.getElementById("lstConferenceType").value;
   
   if (EnableTemplatebooking_Session == "1" && confType != 8)
       document.getElementById("trConfTemp").style.display = '';
   else
       document.getElementById("trConfTemp").style.display = 'None';
   //ZD 101562 END
//ZD 101869 Starts
   function managelayout(dl, epid, epty, par) {
       if (par == '0') {
           mousedownX = 700;
       }

       change_display_layout_prompt('image/pen.gif', RSManageLayOut, epid, epty, dl, 5, document.getElementById('<%=ImageFiles.ClientID%>').value + '|' + document.getElementById('<%=ImageFilesBT.ClientID%>').value, document.getElementById('<%=ImagesPath.ClientID%>').value);
   }

   function change_display_layout_prompt(promptpicture, prompttitle, epid, epty, dl, rowsize, images, imgpath) {
       var title = new Array();
       title[0] = "Default ";
       title[1] = "Custom ";
       promptbox = document.createElement('div');
       promptbox.setAttribute('id', 'prompt');
       document.getElementsByTagName('body')[0].appendChild(promptbox);
       promptbox = document.getElementById('prompt').style; // FB 2050

       promptbox.position = 'absolute'
       promptbox.top = -155 + mousedownY + 'px'; //FB 1373 start FB 2050
       promptbox.left = mousedownX - 485 + 'px'; // FB 2050
       promptbox.width = rowsize * 125 + 'px'; // FB 2050
       promptbox.border = 'outset 1 #bbbbbb';
       promptbox.height = '400px'; // FB 2050
       promptbox.overflow = 'auto'; //FB 1373 End
       promptbox.backgroundColor = '#FFFFE6'; //ZD 100426

       m = "<table cellspacing='0' cellpadding='0' border='0' width='100%'><tr valign='middle'><td width='22' height='22' style='text-indent:2;' class='tableHeader'>&nbsp;</td><td class='tableHeader'>" + prompttitle + "</td></tr></table>"
       m += "<table cellspacing='2' cellpadding='2' border='0' width='100%' class='promptbox'>";
       imagesary = images.split(":");
       rowNum = parseInt((imagesary.length + rowsize - 2) / rowsize, 10);
       m += "  <tr><td align='right' colspan='" + (rowsize * 2) + "'>"
       //Code Changed for Soft Edge Button
       //m += "    <input type='button' class='altShortBlueButtonFormat' value='Submit' onMouseOver='this.style.border=\"1 outset #dddddd\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='saveOrder(epid);'>"
       //m += "    <input type='button' class='altShortBlueButtonFormat' value='Cancel' onMouseOver='this.style.border=\"1 outset transparent\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='cancelthis();'>"
       m += "    <button id='btnLayoutSubmit' class='altMedium0BlueButtonFormat' style='width:80px' onClick='saveOrder(epid);' tabindex='1'><asp:Literal Text='<%$ Resources:WebResources, Submit%>' runat='server'></asp:Literal></button>" //ZD 101028
       m += "    <button id='btnLayoutCancel' class='altMedium0BlueButtonFormat' style='width:80px' onClick='cancelthis();' tabindex='2'><asp:Literal Text='<%$ Resources:WebResources, Cancel%>' runat='server'></asp:Literal></button>" //ZD 101028
       m += "  </td></tr>"
       m += "	<tr>";
       //Window Dressing
       m += "    <td colspan='" + (rowsize * 2) + "' align='left' class='blackblodtext'><asp:Literal Text='<%$ Resources:WebResources, ManageConference_DisplayLayout%>' runat='server'></asp:Literal></td>"; //FB 2579
       m += "  </tr>"
       m += "  <tr>"
       m += "    <td colspan='" + (rowsize * 2) + "' height='5'></td>";
       m += "  </tr>"
	   //ZD 102057
       m += "  <tr>";
       m += "    <td valign='middle'>";
       m += "      <input type='radio' name='layout' id='layout' value='-1' onClick='epid=-1'>";
       m += "    </td>";
       m += "    <td valign='middle' align='left' class='blackblodtext'>Auto</td>";
       m += "  </tr>";

       //ZD 101869 start
       if (1 == 1) {

           m += "<tr><td colspan='" + (rowsize * 2) + "'>";
           m += "<table border='0'>";
           m += "  <tr>";
           m += "    <td valign='middle' align='left' style='FONT-SIZE: 14px;' class='blackblodtext' colspan='" + (rowsize * 2) + "'>" + CodianMCUType + "</td>";
           m += "  </tr>";

           m += "  <tr>";
		   //ZD 102057
           m += "    <td valign='middle' height='43'>";
           m += "      <input type='radio' name='layout' id='layout' value='100' onClick='epid=100'>";
           m += "    </td>";
           m += "    <td valign='middle' align='left' class='blackblodtext'>" + Defaultfamily + "</td>";
           m += "  </tr>";

           m += "  <tr>";
           m += "    <td  valign='middle'>";
           m += "      <input type='radio' name='layout' id='layout' value='101' onClick='epid=101'>";
           m += "    </td>";
           m += "    <td valign='middle' align='left' class='blackblodtext'>" + Family1 + "</td>";
           m += "    <td valign='middle'>";
           m += "      <img src='" + imgpath + "05" + ".gif' width='57' height='43' alt='Layout'>";
           m += "    </td>";
           m += "    <td></td>";
           m += "    <td valign='middle'>";
           m += "      <img src='" + imgpath + "06" + ".gif' width='57' height='43' alt='Layout'>";
           m += "    </td>";
           m += "    <td></td>";
           m += "    <td valign='middle'>";
           m += "      <img src='" + imgpath + "07" + ".gif' width='57' height='43' alt='Layout'>";
           m += "    </td>";
           m += "    <td></td>";
           m += "    <td valign='middle'>";
           m += "      <img src='" + imgpath + "44" + ".gif' width='57' height='43' alt='Layout'>";
           m += "    </td>";
           m += "  </tr>";

           m += "  <tr>"
           m += "    <td height='1'></td>";
           m += "  </tr>"

           m += "  <tr>";
           m += "    <td  valign='middle'>";
           m += "      <input type='radio' name='layout' id='layout' value='102' onClick='epid=102'>";
           m += "    </td>";
           m += "    <td valign='middle' align='left' class='blackblodtext'>" + Family2 + "</td>";
           m += "    <td valign='middle'>";
           m += "      <img src='" + imgpath + "01" + ".gif' width='57' height='43' alt='Layout'>";
           m += "    </td>";
           m += "  </tr>";

           m += "  <tr>"
           m += "    <td height='1'></td>";
           m += "  </tr>"

           m += "  <tr>";
           m += "    <td  valign='middle'>";
           m += "      <input type='radio' name='layout' id='layout' value='103' onClick='epid=103'>";
           m += "    </td>";
           m += "    <td valign='middle' align='left' class='blackblodtext'>" + Family3 + "</td>";
           m += "    <td valign='middle'>";
           m += "      <img src='" + imgpath + "02" + ".gif' width='57' height='43' alt='Layout'>";
           m += "    </td>";
           m += "  </tr>";

           m += "  <tr>"
           m += "    <td height='1'></td>";
           m += "  </tr>"


           m += "  <tr>";
           m += "    <td valign='middle'>";
           m += "      <input type='radio' name='layout' id='layout' value='104' onClick='epid=104'>";
           m += "    </td>";
           m += "    <td valign='middle' align='left' class='blackblodtext'>" + Family4 + "</td>";
           m += "    </td>";
           m += "    <td valign='middle'>";
           m += "      <img src='" + imgpath + "02" + ".gif' width='57' height='43' alt='Layout' >";
           m += "    </td>";
           m += "    <td></td>";
           m += "    <td valign='middle'>";
           m += "      <img src='" + imgpath + "03" + ".gif' width='57' height='43' alt='Layout'>";
           m += "    </td>";
           m += "    <td></td>";
           m += "    <td valign='middle'>";
           m += "      <img src='" + imgpath + "04" + ".gif' width='57' height='43' alt='Layout'>";
           m += "    </td>";
           m += "    <td></td>";
           m += "    <td valign='middle'>";
           m += "      <img src='" + imgpath + "43" + ".gif' width='57' height='43' alt='Layout'>";
           m += "    </td>";
           m += "  </tr>";

           m += "  <tr>"
           m += "    <td height='1'></td>";
           m += "  </tr>"

           m += "    <tr>";
           m += "    <td valign='middle'>";
           m += "      <input type='radio' name='layout' id='layout' value='105' onClick='epid=105'>";
           m += "    </td>";
           m += "    <td valign='middle' align='left' class='blackblodtext'>" + Family5 + "</td>";
           m += "    </td>";
           m += "    <td valign='middle'>";
           m += "      <img src='" + imgpath + "25" + ".gif' width='57' height='43' alt='Layout'>";
           m += "    </td>";
           m += "  </tr>"

           m += "  <tr>"
           m += "    <td height='1'></td>";
           m += "  </tr>"

           m += "</table>";
           m += " </td></tr>"

           m += "  <tr>"
           m += "    <td colspan='" + (rowsize * 2) + "' height='5'></td>";
           m += "  </tr>"

       }
       //ZD 101869 End
       m += "  <tr>";
       m += "    <td valign='middle' align='left' style='FONT-SIZE: 14px;' class='blackblodtext' colspan='" + (rowsize * 2) + "'>" + AllMCUtypes + "</td>";
       m += "  </tr>";

       imgno = 0;
       for (i = 0; i < rowNum; i++) {
           m += "  <tr>";
           for (j = 0; (j < rowsize) && (imgno < imagesary.length - 1); j++) {


               m += "    <td valign='middle'>";
               m += "      <input type='radio' tabindex='3' name='layout' id='layout' value='" + imagesary[imgno] + "' onClick='epid=" + imagesary[imgno] + ";'>";
               m += "    </td>";
               m += "    <td valign='middle'>";
               m += "      <img src='" + imgpath + imagesary[imgno] + ".gif' width='57' height='43' alt='layout'>"; //ZD 100419
               m += "    </td>";
               imgno++;
           }
           m += "  </tr>";
       }

       m += "  <tr>";
       m += "    <td colspan='" + (rowsize * 2) + "' height='5'></td>";
       m += "  </tr>"
       m += "  <tr><td align='right' colspan='" + (rowsize * 2) + "'>"
       //Code Changed for Soft Edge Button
       //m += "    <input type='button' class='altShortBlueButtonFormat' value='Submit' onMouseOver='this.style.border=\"1 outset #dddddd\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='saveOrder(epid);'>"
       //m += "    <input type='button' class='altShortBlueButtonFormat' value='Cancel' onMouseOver='this.style.border=\"1 outset transparent\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='cancelthis();'>"
       m += "    <button id='btnLayoutSubmit1' class='altMedium0BlueButtonFormat' style='width:80px'  onClick='saveOrder(epid);' tabindex='4'><asp:Literal Text='<%$ Resources:WebResources, Submit%>' runat='server'></asp:Literal></button>" //ZD 101028
       m += "    <button id='btnLayoutCancel1' class='altMedium0BlueButtonFormat' style='width:80px' onClick='cancelthis();' tabindex='5'><asp:Literal Text='<%$ Resources:WebResources, Cancel%>' runat='server'></asp:Literal></button>" //ZD 101028
       m += "  </td></tr>"
       m += "</table>"

       document.getElementById('prompt').innerHTML = m;
       document.getElementById('btnLayoutSubmit').focus();
//       if (document.getElementById('btnLayoutSubmit') != null)
//           document.getElementById('btnLayoutSubmit').setAttribute("onblur", "document.getElementById('btnLayoutCancel').focus(); document.getElementById('btnLayoutCancel').setAttribute('onfocus', '');");
//       if (document.getElementById('btnLayoutCancel') != null)
//           document.getElementById('btnLayoutCancel').setAttribute("onblur", "document.getElementById('layout').focus(); document.getElementById('gprivate').setAttribute('layout', '');");
//       //if (document.getElementById('layout') != null)
//       //  document.getElementById('layout').setAttribute("onblur", "document.getElementById('btnLayoutSubmit1').focus(); document.getElementById('btnLayoutSubmit1').setAttribute('onfocus', '');");
//       if (document.getElementById('btnLayoutSubmit1') != null)
//           document.getElementById('btnLayoutSubmit1').setAttribute("onblur", "document.getElementById('btnLayoutCancel1').focus(); document.getElementById('btnLayoutCancel1').setAttribute('onfocus', '');");
//       if (document.getElementById('btnLayoutCancel1') != null)
//           document.getElementById('btnLayoutCancel1').setAttribute("onblur", "document.getElementById('btnLayoutSubmit').focus(); document.getElementById('btnLayoutSubmit').setAttribute('onfocus', '');");


   }


   function saveOrder(id) {
       if (id < 10 && id != -1) //ZD 102057
           id = "0" + id;
       document.getElementById("<%=txtSelectedImage.ClientID %>").value = id;
       document.getElementById("<%=imgVideoDisplay.ClientID %>").src = document.getElementById("<%=ImagesPath.ClientID %>").value + document.getElementById("<%=txtSelectedImage.ClientID %>").value + ".gif";
       document.getElementById("hdnFamilyLayout").value = 0;
       saveLayout(document.getElementById("<%=txtSelectedImage.ClientID %>").value);
       cancelthis();
   }

   function saveLayout(id) {

       document.getElementById("hdnFamilyLayout").value = 1;
       if (id == 101) {
           document.getElementById("imgVideoDisplay").style.display = '';
           document.getElementById("imgVideoDisplay").src = "image/displaylayout/" + "05" + ".gif";
           document.getElementById("imgLayoutMapping6").style.display = '';
           document.getElementById("imgLayoutMapping6").src = "image/displaylayout/" + "06" + ".gif";
           document.getElementById("imgLayoutMapping7").style.display = '';
           document.getElementById("imgLayoutMapping7").src = "image/displaylayout/" + "07" + ".gif";
           document.getElementById("imgLayoutMapping8").style.display = '';
           document.getElementById("imgLayoutMapping8").src = "image/displaylayout/" + "44" + ".gif";
           document.getElementById("lblCodianLO").innerHTML = Family1;
       }
       else if (id == 102) {
           document.getElementById("imgVideoDisplay").style.display = '';
           document.getElementById("imgVideoDisplay").src = "image/displaylayout/" + "01" + ".gif";
           document.getElementById("imgLayoutMapping6").style.display = 'none';
           document.getElementById("imgLayoutMapping7").style.display = 'none';
           document.getElementById("imgLayoutMapping8").style.display = 'none';
           document.getElementById("lblCodianLO").innerHTML = Family2;
       }
       else if (id == 103) {
           document.getElementById("imgVideoDisplay").style.display = '';
           document.getElementById("imgVideoDisplay").src = "image/displaylayout/" + "02" + ".gif";
           document.getElementById("imgLayoutMapping6").style.display = 'none';
           document.getElementById("imgLayoutMapping7").style.display = 'none';
           document.getElementById("imgLayoutMapping8").style.display = 'none';
           document.getElementById("lblCodianLO").innerHTML = Family3;
       }
       else if (id == 104) {
           document.getElementById("imgVideoDisplay").style.display = '';
           document.getElementById("imgVideoDisplay").src = "image/displaylayout/" + "02" + ".gif";
           document.getElementById("imgLayoutMapping6").style.display = '';
           document.getElementById("imgLayoutMapping6").src = "image/displaylayout/" + "03" + ".gif";
           document.getElementById("imgLayoutMapping7").style.display = '';
           document.getElementById("imgLayoutMapping7").src = "image/displaylayout/" + "04" + ".gif";
           document.getElementById("imgLayoutMapping8").style.display = '';
           document.getElementById("imgLayoutMapping8").src = "image/displaylayout/" + "43" + ".gif";
           document.getElementById("lblCodianLO").innerHTML = Family4;
       }
       else if (id == 105) {
           document.getElementById("imgVideoDisplay").style.display = '';
           document.getElementById("imgVideoDisplay").src = "image/displaylayout/" + "25" + ".gif";
           document.getElementById("imgLayoutMapping6").style.display = 'none';
           document.getElementById("imgLayoutMapping7").style.display = 'none';
           document.getElementById("imgLayoutMapping8").style.display = 'none';
           document.getElementById("lblCodianLO").innerHTML = Family5;
       }
       else if (id == 100) {
           document.getElementById("imgVideoDisplay").style.display = 'none';
           document.getElementById("imgLayoutMapping6").style.display = 'none';
           document.getElementById("imgLayoutMapping7").style.display = 'none';
           document.getElementById("imgLayoutMapping8").style.display = 'none';
           document.getElementById("lblCodianLO").innerHTML = Defaultfamily;
       }
       else {
           document.getElementById("imgVideoDisplay").src = "image/displaylayout/" + id + ".gif";
           document.getElementById("imgVideoDisplay").style.display = '';
           document.getElementById("imgLayoutMapping6").style.display = 'none';
           document.getElementById("imgLayoutMapping7").style.display = 'none';
           document.getElementById("imgLayoutMapping8").style.display = 'none';
           document.getElementById("lblCodianLO").innerHTML = "";
           document.getElementById("hdnFamilyLayout").value = 0;
			//ZD 102057
           if (id == "-1") {
               document.getElementById("imgVideoDisplay").style.display = 'none';
               document.getElementById("lblCodianLO").innerHTML = "Auto";
               document.getElementById("<%=imgVideoDisplay.ClientID %>").src = "";
           }
       }
       return true;
   }
   

   function cancelthis() {
       document.getElementsByTagName("body")[0].removeChild(document.getElementById("prompt"));
       //window.resizeTo(750,450); //FB Case 536 Saima
   }
    //ZD 101869 End
   //ZD 101971 Starts
   if (document.getElementById("errLabel") != null)
       var obj = document.getElementById("errLabel");
   if (obj != null) {

       var strInput = obj.innerHTML.toUpperCase();

       if (strInput.indexOf("NOTE: THE SELECTED VIDEO DISPLAY LAYOUT IS NOT SUPPORTED BY THE MCU IN CONFERENCE") > -1)
           obj.setAttribute("style", "color:red");

       if (strInput.indexOf("SELECCIONADA NO ES COMPATIBLE CON EL MCU") > -1)
           obj.setAttribute("style", "color:red");
   }
   //ZD 101971 Ends
   if ('<%=Session["EnableDetailedExpressForm"]%>' != null) 
   {
       if ('<%=Session["EnableDetailedExpressForm"]%>' == '1') 
       {
           if (document.getElementById('hdndetailexpform') != null)
               document.getElementById('hdndetailexpform').value = "1";
       }
       else 
       {
           if (document.getElementById('hdndetailexpform') != null)
               document.getElementById('hdndetailexpform').value = "0";
       }
   }

   //ALLBUGS-99 START
   if (("<%=EnableFECC%>" == "0" || "<%=EnableFECC%>" == "2") && "<%=EnableAudioBridges%>" == 0 && "<%=mcuSetupDisplay%>" == "0" && "<%=mcuTearDisplay%>" == "0" && "<%=EnableProfileSel%>" == "0" && "<%=EnablePoolOrder%>" == "0" && '<%=Session["ShowVideoLayout"]%>' == "1") {
       document.getElementById('trAdvancedSettings').style.display = "none";
   }

   confType = document.getElementById("lstConferenceType").value;
   if (confType == 4 || confType == 7 || confType == 8) // Hiding the Advanced Settings for the conftype- P2P,Room,Hotdesking
        document.getElementById('trAdvancedSettings').style.display = "none";
   //ALLBUGS-99 END

  
</script>
<%--ZD 100428 END--%>