<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%>
<%--ZD 100147 ZD 100886 End--%>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="ns_MyVRM.en_DashBoard" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"  Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v10.2, Version=10.2.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v10.2, Version=10.2.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>

<%@ Register assembly="DevExpress.Web.ASPxEditors.v10.2" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=8" /> <!-- FB 2050 -->


<!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 


<script type="text/javascript" src="script/errorList.js"></script>
<script type="text/javascript" language="JavaScript" src="../<%=Session["language"] %>/inc/functions.js"></script>
<script type="text/javascript" src="script/mousepos.js"></script> <%-- ZD 102723 --%>
<script type="text/javascript" src="script/showmsg.js"></script> <%-- ZD 102723 --%>
<link rel="stylesheet" type="text/css" media="all" href="css/aqua/theme.css" title="Aqua" />
<script type="text/javascript">

  var servertoday = new Date(parseInt("<%=DateTime.Now.Year%>", 10), parseInt("<%=DateTime.Now.Month%>", 10)-1, parseInt("<%=DateTime.Now.Day%>", 10),
  parseInt("<%=DateTime.Now.Hour%>", 10), parseInt("<%=DateTime.Now.Minute%>", 10), parseInt("<%=DateTime.Now.Second%>", 10));
  //ZD 100604 start
  var img = new Image();
  img.src = "../en/image/wait1.gif";
  //ZD 100604 End 101610 start
  function btnDeleteConference_Click() {
      var msg;
      var btnDelete = document.getElementById('btnDeleteConf');
      var btnLinkButton4 = document.getElementById('LinkButton4'); //GP Fixes
      var drp = document.getElementById("DrpDwnListView");
      var confGrid = document.getElementById("hdngridCount");
      var Confdate = document.getElementById("lblConfDate").innerHTML;
      var Confsetdur = document.getElementById("lblSetupDur").innerHTML;
      if (drp.value == 1 && Confdate != "" && Confsetdur != "" || drp.value == 3 && Confdate != "" && Confsetdur != "") {
          if (!btnLinkButton4.disabled)//GP Fixes
          {
              if ('<%=Application["Client"]%>' == "MOJ")
                  msg = "Are you sure you want to delete this hearing?";
              else
                  msg = DeleteConf; //ZD 101344

              if (confirm(msg)) {
                  DataLoading(1);
                  return true;
              }
              else
                  return false;
          }
      }
      else
          return false;

  }
  function btnTerminateConference_Click()
  {
  var msg;
  var btnDelete = document.getElementById('btnDeleteConf');
  var btnLinkButton4 = document.getElementById('LinkButton4'); //GP Fixes
  var drp = document.getElementById("DrpDwnListView");
  var confGrid = document.getElementById("hdngridCount");
  var Confdate = document.getElementById("lblConfDate").innerHTML;
  var Confsetdur = document.getElementById("lblSetupDur").innerHTML;
  if (drp.value == 2 && Confdate != "" && Confsetdur != "") {
      if (!btnLinkButton4.disabled)//GP Fixes
      {
          if ('<%=Application["Client"]%>' == "MOJ")
              msg = "Are you sure you want to delete this hearing?";
          else
              msg = DeleteConf; //ZD 101344

          if (confirm(msg)) {
              DataLoading(1);
              return true;
          }
          else
              return false;
      }
  }
  else
      return false;

}

   //101610 end
  //Blue Status Project
    function ShowEpID()
    {
        if(arguments[1] != "")
            document.getElementById(arguments[0]).innerHTML = arguments[1];
    }
	//FB 1958 - Start
    function shwHostDetails() 
    {
        document.getElementById("viewHostDetails").style.display = 'block';
        return false;
    }
    //ZD 101597 - Start

    // document.addEventListener("click", handler, true); 
     document.onclick = handler; //ZD 101931

    

    function fnSelectForm() {

        var elementRef = document.getElementById('rdEditForm');
        var inputElementArray = elementRef.getElementsByTagName('input');

        //ZD 102200 Starts
        if ('<%=Session["hasConference"]%>' == '1' && '<%=Session["hasExpConference"]%>' == "1") {
            document.getElementById("PopupFormList").style.display = 'block';
            if (document.getElementById('rdEditForm_1') != null)
                document.getElementById('rdEditForm_1').checked = false;
            if (document.getElementById('rdEditForm_2') != null)
                document.getElementById('rdEditForm_2').checked = false;

            for (var i = 0; i < inputElementArray.length; i++) {
                var inputElement = inputElementArray[i];
                inputElement.checked = false;
            }
            return false; //ZD 102792
        }
        //ZD 102200 Ends
    }

    function fnRedirectForm() {

        var args = fnRedirectForm.arguments;        
        var hdnEditForm = document.getElementById("hdnEditForm");
        hdnEditForm.value = "";

        if (document.getElementById("rdEditForm_0").checked)
            hdnEditForm.value = "1";
        else if (document.getElementById("rdEditForm_1").checked)
            hdnEditForm.value = "2";

        if (hdnEditForm.value != "") {           
            var btnTemp = document.getElementById("Temp");
            if (btnTemp != null)
                btnTemp.click();

            DataLoading(1);
            
            document.getElementById("PopupFormList").style.display = 'None';
        }
        else
            alert("Please select the form to edit a conference.");

        return false;
    }

    function fnDivClose() {
        var hdnEditForm = document.getElementById("hdnEditForm");
        hdnEditForm.value = "";
        document.getElementById("PopupFormList").style.display = 'none';
    }


    function handler(e) {
        var obj = document.getElementById('PopupFormList');
        var isoutofpopup = false; //ZD 104319

        if (obj.style.display != 'none') {
            if (e.target.id != "Temp" && e.target.id != "btnCancel" && e.target.id != "btnOk" && e.target.id.indexOf('rdEditForm') < 0) {
                isoutofpopup = true; //ZD 104319
            }
            //ZD 104319 - start
            if (e.target.htmlFor != undefined) {
                if (e.target.htmlFor.indexOf('rdEditForm') < 0)
                    isoutofpopup = true;
                else
                    isoutofpopup = false;
            }
            if (isoutofpopup == true)
                e.returnValue = false;
            //ZD 104319 - end
        }

    }
    //ZD 101597 - End
    
    function ClosePopUp()
    {
        document.getElementById("viewHostDetails").style.display = 'none';
        return false;
    }
    //FB 1958 - End
    //FB 2441 Starts
    function fnShowHide(arg) {

        if (arg == '1')
            document.getElementById("MuteAllEndpointDiv").style.display = 'block';
        else
            document.getElementById("MuteAllEndpointDiv").style.display = 'none';

        return false;
    }
    //FB 2441 Ends
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">

    <title>Conference Console</title>
    </head>
<body BackColor="#f1f1f1"><%--FB 2837--%>
    <form id="frmDashBoard" runat="server" method="post">
        <input type="hidden" id="helpPage" value="107" runat="server">
        <input type="hidden" id="txtSelectionCount" runat="server">
        <input type="hidden" id="txtSearchType" runat="server" />
        <input type="hidden" id="txtSortBy" runat="server" />
        <input type="hidden" id="txtPublic" runat="server" />
        <input type="hidden" id="txtConferenceSearchType" runat="server" />
        <input type="hidden" id="txtCOMConfigPath" runat="server" value="C:\VRMSchemas_v1.8.3\COMConfig.xml" />
        <input type="hidden" id="txtASPILConfigPath" runat="server" value="C:\VRMSchemas_v1.8.3\" />
        <asp:HiddenField ID="hdnConfLockStatus" runat="server" /> <%--FB 2501 Dec10--%>
        <input type="hidden" id="hdnTimeZoneId" runat="server" /><%--ZD 100602--%>
        <input type="hidden" id="hdnCloudConf" runat="server" />
        <input type="hidden" id="hdnIsVMR" runat="server" />
        <input type="hidden" id="hdnServiceType" runat="server" />
        <input type="hidden" id="hdnConfStart" runat="server" />
        <input type="hidden" id="hdnConfEnd" runat="server" /><%--ZD 100602 End--%>
        <input type="hidden" id="hdnEditForm" runat="server" /> <%--ZD 101597--%>
    <div>
    <%--ZD 101022 start--%>
        <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" >
		    <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		    </Scripts>
	    </asp:ScriptManager> <%--ZD 101022 End--%>
    
     <%--FB 2441 Starts--%>
    <div  id="MuteAllEndpointDiv"  style="left:700px; top:300px; display:none; POSITION: absolute; HEIGHT: 200px;VISIBILITY: visible; Z-INDEX: 3;  width:250px; background-color:#A9D9E8">
     <table>
        <tr>
        <td align="center"><span class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, DashBoard_SelectParticip%>" runat="server"></asp:Literal></span></td>
        </tr>
        </table>
    <div style="HEIGHT: 140px;left:200px;overflow-y: scroll; background-color:#A9D9E8">
       <table>
        <tr>
        <td>
            
        <asp:DataGrid AutoGenerateColumns="false"  ShowHeader="false" GridLines="None" ID="dgMuteALL"  runat ="server">
            <Columns>
            <asp:BoundColumn DataField="ID" Visible="false"></asp:BoundColumn>
            <asp:BoundColumn DataField="type" Visible="false"></asp:BoundColumn>
            <asp:TemplateColumn>
                 <ItemTemplate>
                     <asp:CheckBox runat="server" ID="chk_muteall"/>
                 </ItemTemplate>
            </asp:TemplateColumn>
            <asp:BoundColumn DataField="Name"></asp:BoundColumn>
            </Columns>
        
       </asp:DataGrid>
        </td>
        </tr>
       
          </table>
          </div>
          <table> <tr>
        
           <td align="center">
                     <asp:Button id="BtnSubmit" text="<%$ Resources:WebResources, DashBoard_BtnSubmit%>" cssclass="altShortBlueButtonFormat" onclick="btnMuteAllExcept" runat="server"></asp:Button>
              <asp:Button id="BtnClose" text="<%$ Resources:WebResources, DashBoard_BtnClose%>" cssclass="altShortBlueButtonFormat" runat="server" onclientclick="javascript:return fnShowHide('0');"></asp:Button>
          </td>
          </tr></table>
    </div>
    <%--FB 2441 Ends--%>
        
    </div>
    
            <table id="print_content" width="100%"> 
            <tr>
            <td align="center" colspan="2">
                <h3>
                    <span ID="Field1"><asp:Literal Text="<%$ Resources:WebResources, DashBoard_Field1%>" runat="server"></asp:Literal></span><!-- FB 2570 -->
                    <div id="dataLoadingDIV" name="dataLoadingDIV" align="center" style="display:none">
                        <img border='0' src='image/wait1.gif' alt='Loading..' />
                    </div><%--ZD 100678--%>
                    
            <asp:CheckBox ID="Refreshchk" runat="server" style="display:none" />
                </h3>   
            </td>
            </tr>
            <tr valign="top">
            <td>
            <asp:UpdatePanel ID="RoomsUpdate"   UpdateMode="Conditional"  runat="server" RenderMode="Inline">
                         <ContentTemplate>
                         <input id="hdnCodian" type="hidden" runat="server" value ="0"  name="hdnCodian"/> <%--ZD 101869 start--%>
                         <input id="hdnPolycomMGC" type="hidden" runat="server" value ="0" name="hdnPolycomMGC" />
                         <input id="hdnPolycomRMX" type="hidden" runat="server" value ="0" name="hdnPolycomRMX" /> <%--ZD 101869 End--%>
                         <input type="hidden" id="hdnExpressConf" runat="server" /><%--ZD 101233--%>
                         <asp:Timer ID="Timer1" Interval="30000" OnTick="ChangeCalendarDate" runat="server">
                          </asp:Timer>
                         <input type="hidden" id="hdnLayout" runat="server" />
                         <table width="100%"   style="height:545px;vertical-align:super;">
                         <tr>
                         <td colspan="2" align="center"> 
                         <asp:Label ID="LblError" CssClass="lblError" runat="server" Visible="false"></asp:Label>
                         <asp:Image ID="imgVideoLayout" runat="server" Width="30" Height="30" style="display:none;" AlternateText="Video Layout"/> <%--ZD 100419--%>
                                                              
                         <asp:Table runat="server" ID="tblForceTerminate" Visible="false">
                        <asp:TableRow>
                            <asp:TableCell CssClass="lblError" Width="100%">
                                <b><asp:Literal Text="<%$ Resources:WebResources, DashBoard_WARNINGconfere%>" runat="server"></asp:Literal><br />
			                    <asp:Button ID="Button1" Text="<%$ Resources:WebResources, DashBoard_Button1%>" CssClass="altLongBlueButtonFormat" runat="server" OnClientClick="javascript:DataLoading(1)" OnClick="ForceTerminate" />
			                    &nbsp;&nbsp;&nbsp;&nbsp;
			                    <asp:Button ID="Button2" Text="<%$ Resources:WebResources, DashBoard_Button2%>" CssClass="altLongBlueButtonFormat" runat="server" OnClientClick="javascript:DataLoading(1)" OnClick="HideForceTerminate" />
			                    
                                </b>
                                
                            </asp:TableCell>
                        </asp:TableRow>
                        </asp:Table>
                        
                        <table id="tblExtendTime" style="display:none;" width="700px"><%--Edited for FF--%>
                        <tr>
                        <td>
                                                                 <asp:Label id="Label1" text="<%$ Resources:WebResources, DashBoard_Label1%>" cssclass="blackblodtext" runat="server"></asp:Label>
                                                                <asp:TextBox id="txtExtendedTime" cssclass="altText" runat="server" validationgroup="SubmitTime"></asp:TextBox>
                                                                <asp:Button id="btnExtendEndtime" cssclass="altShortBlueButtonFormat" onclick="ExtendEndtime" validationgroup="SubmitTime" text="<%$ Resources:WebResources, DashBoard_btnExtendEndtime%>" runat="server" onclientclick="javascript:return fnValidateEndTime();"></asp:Button>
                                                                &nbsp;&nbsp;&nbsp;<input id="extTime" runat="server" type="button" class="altShortBlueButtonFormat" value=" Cancel " onclick="javascript:fnextendtime('2');" />
                                                               <br /><asp:Label ID="LblExtendedTimeMsg" style="visibility:hidden" Text="" ForeColor="red"  runat="server"></asp:Label>
                                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ValidationGroup="SubmitTime" ControlToValidate="txtExtendedTime" ErrorMessage="<%$ Resources:WebResources, NumericValuesonly3%>" ValidationExpression="\d+" runat="server" Display="dynamic"></asp:RegularExpressionValidator>
                                                                <asp:RangeValidator ID="RangeValidator1" ValidationGroup="SubmitTime" ControlToValidate="txtExtendedTime" ErrorMessage="<%$ Resources:WebResources, ExtendtimeRange%>" MinimumValue="0" MaximumValue="360" Type="Integer" runat="server" Display="dynamic"></asp:RangeValidator>
                                                            </td>                        
                        </tr>
                        </table>
                         </td>
                         </tr>
                            
            
            <tr valign="top">
                <td style="width:12%;">
                    
                         <input type="hidden" id="hdnlisttype" runat="server" />
                            <table width="276px">
                            <tr>
                            <td>
                            <table width="276px"> 
                            <tr Class="DashBoardHeader">
                            <td style="width:90%;" Class="DashBoardHeader">
                            <asp:Literal Text="<%$ Resources:WebResources, DashBoard_ConferenceFilt%>" runat="server"></asp:Literal>&nbsp;
                            <asp:DropDownList ID="DrpDwnListView" CssClass="altText" runat="server" AutoPostBack="false" onchange="javascript:ChangeViewType()">
                            <asp:ListItem Text="<%$ Resources:WebResources, Ongoing%>" Value="2"></asp:ListItem>
                            <asp:ListItem Text="<%$ Resources:WebResources, OnMCU%>" Value="3"></asp:ListItem><%--ZD 100036--%>
                            <asp:ListItem Text="<%$ Resources:WebResources, Reservations%>" Value="1"></asp:ListItem>
                            </asp:DropDownList>
                                 <font size="1">
                                <br /><asp:Literal Text="<%$ Resources:WebResources, DashBoard_Clickonthehe%>" runat="server"></asp:Literal></font>                    
                                </td>
                          </tr>
                          <tr>
                          <td style="width:70%;">
                          <asp:Panel ID="PanelRooms" runat="server"  Height="540px"  CssClass="DashBackGround"   BorderWidth="1px">
                                 <div align="center" id="conftypeDIV" style="width:100%;" >
                                    <table border="0" cellpadding="3" cellspacing="0" width="100%">
                                         <tr id="ListView">
                            <td width="100%" align="left" style="font-weight: bold; font-size: small; color: green; font-family: arial" valign="middle">
                            
                                <dxwgv:ASPxGridView  AllowSort="true" onhtmlrowcreated="ASPxGridView1_HtmlRowCreated"  ID="grid2"   ClientInstanceName="grid2" runat="server"  KeyFieldName="ConferenceID" Width="100%" EnableRowsCache="True" OnCustomCallback="Grid2_CustomCallback"  OnDataBound="Grid2_DataBound">
                <Columns>
                    <dxwgv:GridViewDataColumn FieldName="ConferenceUniqueID" VisibleIndex="1" Caption="<%$ Resources:WebResources, ReportDetails_ConferenceID%>" />
                    <dxwgv:GridViewDataColumn FieldName="ConferenceName" VisibleIndex="2" Caption="<%$ Resources:WebResources, ConferenceList_tdName%>" />
                    <dxwgv:GridViewDataColumn FieldName="Status" VisibleIndex="3" Caption="<%$ Resources:WebResources, DashBoard_Status%>" />
                     <dxwgv:GridViewDataColumn FieldName="ConfID" Visible="False" />
                    
                                </Columns>
                <Styles>
                        <CommandColumn Paddings-Padding="1"></CommandColumn>
                    </Styles>
                <SettingsBehavior AllowMultiSelection="false" />
                <Settings/><%--Edited for FB 1679--%>
                <SettingsText EmptyDataRow="<%$ Resources:WebResources, NoData%>" HeaderFilterShowAll="false" GroupPanel="<%$ Resources:WebResources, GridViewGroupMsg%>"/>
                <SettingsPager Mode="ShowPager" PageSize="15"  AlwaysShowPager="true" Position="Top" Summary-Text="<%$ Resources:WebResources, GridViewPageText%>">
                </SettingsPager> <%--Edited for FB 1679--%>
                 <Templates>
                     <DataRow>
                        
                         <div style="padding:4px"><%--Edited For FF--%>
                             <table class="templateTable" cellpadding="0"  cellspacing="1" width="100%">
                                 <tr>
                                      <td  class="templateCaption" width="20%"><%# DataBinder.Eval(Container, "DataItem.ConferenceUniqueID")%></td>
                                     <td class="templateCaption" style="width:65%;">
                                     <asp:HyperLink ID="btnViewDetailsDev" NavigateUrl="#" style="cursor:pointer;" runat="server" Text='&nbsp;&nbsp;&nbsp;<%#  DataBinder.Eval(Container, "DataItem.ConferenceName")%>'></asp:HyperLink></td>
                                     <td  id="tdimage" class="templateCaption" runat="server" style="width:15%;"><asp:Image ID="Image1" runat="server"  Height="15px" width="15px" src=""/></td>
                                     <asp:Label ID="lblIsRecur" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.IsRecur")%>'></asp:Label>
                                 </tr>
                             </table>
                         </div>
                     </DataRow>
                 </Templates>
             </dxwgv:ASPxGridView>
             </td>
             </tr>
                                    </table>
                                </div>   
                                
                                
                                </asp:Panel>
                          </td>
                          </tr>             
                         </table>
                        <asp:Button ID="btnRefreshRooms" style="display:none;" runat="server" OnClick="ChangeCalendarDate" />
                    
                </td>
                </tr>
                </table>
                </td>
                <td style="width:88%;" ><%--FB 1958--%>
                <div id="viewHostDetails" runat="server" align="center" style="left:120px; POSITION: absolute; HEIGHT: 350px;VISIBILITY: visible; Z-INDEX: 3; display:none; width:725px"> 
		         <asp:PlaceHolder ID="HostDetailHolder" Runat="server"></asp:PlaceHolder>
                </div> 
                <%--ZD 101597--%>
                <div id="PopupFormList" align="center" style="position: absolute; overflow: hidden;
                    border: 1px; width: 450px; display: none;  top: 300px;left: 510px;height:450px;">
                    <table align="center"class="tableBody" width="80%" cellpadding="5" cellspacing="5">
                        <tr class="tableHeader">
                            <td colspan="2" align="left" class="blackblodtext">
                                <asp:Literal ID="Literal4" Text="<%$ Resources:WebResources, ChooseFormHeading%>" runat="server"></asp:Literal>
                            </td>
                        </tr>
                        <tr >
                            <td align="left" colspan="2" >
                                <asp:RadioButtonList ID="rdEditForm" runat="server" RepeatDirection="Horizontal" RepeatLayout="Table" RepeatColumns="1" CellPadding="3" CellSpacing="3">
                                    <asp:ListItem Text="<%$ Resources:WebResources, LongForm%>" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="<%$ Resources:WebResources, ExpressForm%>" Value="2"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                    <input id="btnOk" runat="server" type="button" class="altMedium0BlueButtonFormat"
                                    onclick="javascript:return fnRedirectForm();" value="<%$ Resources:WebResources, MasterChildReport_cmdConfOk%>" />

                            </td>
                            <td>
                                <input id="btnCancel" runat="server" type="button" class="altMedium0BlueButtonFormat"
                                    onclick="javascript:fnDivClose();" value="<%$ Resources:WebResources, Cancel%>" />
                            </td>
                        </tr>
                    </table>
                </div>
                    <asp:Panel ID="Filters" runat="server"   BackColor="#f1f1f1" Height="594px" BorderColor="#89B5D8" ScrollBars="Auto"><%--FB 2837--%>
                            <table width="100%" id="tblMain" name="tblMain" style="height:454px;vertical-align:super;">
                                        <tr>
                                            <td align="left">
                                                <table width="100%" style="height:545px;vertical-align:super;">
                                                <tr style="height:45px;" >
                                                        <td align="left" Class="DashBoardHeader"  width="90%"><%--FB 2508--%>
                                                            <asp:Label ID="lblConfUniqueID" runat="server" ></asp:Label>
                                                            <asp:Label ID="lblhash"  runat="server" ></asp:Label>
                                                            <asp:Label ID="lblConfName"  runat="server" Font-Bold="true"></asp:Label>
                                                            </td>
                                                            <td align="right" Class="DashBoardHeader"  nowrap>
                                                            <asp:Label ID="LblTimeText"  runat="server" ></asp:Label>
                                                            <asp:Label ID="LblTime"  runat="server" ></asp:Label>
                                                            </td>
                                                        
                                                    </tr>
                                                <tr>
                                                <td colspan="4">                                
                                                    <asp:TextBox ID="lblConfID" runat="server" TabIndex="-1" width="0px" BorderStyle="None" BackColor="transparent"></asp:TextBox>
                                                <table>
                                                    <tr>
                                                    <td width="80%"><%--ZD 100085--%>
                                                    <table style="width:100%; margin-right: 0px;" valign="super" cellspacing="0" >
                                                    <tr>
                                                         <td align="left" class="DashboardText" valign="top" width="120px" nowrap="nowrap"><asp:Literal Text="<%$ Resources:WebResources, DashBoard_Host%>" runat="server"></asp:Literal></td>
                                                         <td style="width:1px; vertical-align:top"><b>:</b>&nbsp;</td>
                                                        <td align="left"  valign="top" nowrap>
                                                            <asp:Label ID="lblConfHost" CssClass="DashboardText"  runat="server" Font-Bold="False"></asp:Label>
								                            <asp:Label ID="hdnConfHost" runat="server" Visible="false"></asp:Label>&nbsp; <%--FB 1958--%>
                                                            <asp:Label ID="hdnconfrequestor" runat="server" Visible="false"></asp:Label>&nbsp; <%--ALLDEV-839--%>
								                            <asp:ImageButton ID="imgHostDetails" runat="server"  ImageUrl="image/FaceSheet.GIF" OnClientClick="javascript:return shwHostDetails()" ToolTip="<%$ Resources:WebResources, ViewDetails%>" style="cursor: pointer;vertical-align:middle;" AlternateText="View Details"/> <%--ZD 100419--%>
								                        </td> 
							                           <td align="left" class="DashboardText" nowrap=""><asp:Literal Text="<%$ Resources:WebResources, DashBoard_LastModifiedB%>" runat="server"></asp:Literal></td>
                                                            <td style="width:1px; vertical-align:top"><b>:</b>&nbsp;</td>
                                                        <td align="left" style="height: 21px;" valign="top"  nowrap>
                                                            <asp:Label ID="lblLastModifiedBy" CssClass="DashboardText" Font-Bold="False" runat="server"></asp:Label>&nbsp;</td>
								                            <asp:Label ID="hdnLastModifiedBy" runat="server" Visible="false"></asp:Label>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" class="DashboardText" valign="top" nowrap=""><asp:Literal Text="<%$ Resources:WebResources, DashBoard_Date%>" runat="server"></asp:Literal></td>
                                                        <td style="width:1px; vertical-align:top"><b>:</b>&nbsp;</td>
                                                        <td align="left" valign="top">
                                                            <asp:Label ID="lblConfDate" CssClass="DashboardText" runat="server" Font-Bold="False"></asp:Label> 
                                                            <asp:Label ID="lblConfTime" CssClass="DashboardText" runat="server" Font-Bold="False"></asp:Label>
                                                            <asp:Label ID="lblTimezone" CssClass="DashboardText" Font-Bold="False" runat="server"></asp:Label>
                                                            
                                                            <asp:TextBox runat=server id="Recur" TabIndex="-1" Text="" TextMode="SingleLine" BorderStyle="None" Width="0px" BackColor="transparent" BorderColor="transparent" ForeColor="White" Height="10"></asp:TextBox>
                                                            
                                                        </td>
                                                        <td align="left" class="DashboardText" valign="top" nowrap=""><asp:Literal Text="<%$ Resources:WebResources, DashBoard_Duration%>" runat="server"></asp:Literal></td>
                                                        <td style="width:1px; vertical-align:top"><b>:</b>&nbsp;</td>
                                                        <td align="left" style="height: 21px;" valign="top"  nowrap>
                                                            <asp:Label ID="lblConfDuration" CssClass="DashboardText" runat="server" Font-Bold="False"></asp:Label>
                                                            <br />
                                                            <asp:Label ID="hdnConfDuration" runat="server" Visible="false"></asp:Label></td>
                                                    </tr>
                                                     
                                                     <%if (!(Application["Client"] == "MOJ"))
                                                       { %>
                                                    <tr runat="server" id="bufferTableCell">
                                                        <td align="left" class="DashboardText" style="height: 10px;" nowrap=""><asp:Literal Text="<%$ Resources:WebResources, DashBoard_ConferenceStar%>" runat="server"></asp:Literal></td>
                                                        <td style="width:1px; vertical-align:top"><b>:</b>&nbsp;</td>
                                                        <td align="left" nowrap>
                                                            <asp:Label ID="lblSetupDur" CssClass="DashboardText" valign="top"  runat="server" Font-Bold="False" nowrap></asp:Label>
                                                         </td>
                                                        <td align="left" class="DashboardText" nowrap=""><asp:Literal Text="<%$ Resources:WebResources, DashBoard_ConferenceEnd%>" runat="server"></asp:Literal></td>
                                                        <td style="width:1px; vertical-align:top"><b>:</b>&nbsp;</td>
                                                        <td align="left"  style="height: 10px;" nowrap>
                                                            <asp:Label ID="lblTearDownDur" CssClass="DashboardText" runat="server" Font-Bold="False"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <%}%>
                                                    <%--ZD 100085 Starts--%>                                                    
                                                    <tr id="trBuffer" runat="server">
                                                        <td align="left" Class="blackblodtext" valign="top" nowrap style="height: 10px">							                        
                                                        <asp:Literal Text="<%$ Resources:WebResources, DashBoard_Setupmins%>" runat="server"></asp:Literal>
                                                        </td><td style="width:1px"><b>:</b>&nbsp;</td>                                                        
                                                        <td align="left" >
                                                            <asp:Label ID="lblSetupTimeinMin" runat="server" Font-Bold="False"></asp:Label>
                                                        </td>
                                                        <td align="left" Class="blackblodtext" valign="top" nowrap>
                                                            <asp:Literal Text="<%$ Resources:WebResources, DashBoard_TearDownmins%>" runat="server"></asp:Literal>
                                                        </td><td style="width:1px"><b>:</b>&nbsp;</td>
                                                        <td align="left" style="height: 10px;">
                                                            <asp:Label ID="lblTearDownTimeinMin" runat="server" Font-Bold="False"></asp:Label>
                                                        </td>
                                                    </tr> 
                                                    <tr id="trMUCPreTime" runat="server">
                                                        <td align="left" Class="blackblodtext" valign="top" nowrap style="height: 10px">
                                                            <asp:Literal Text="<%$ Resources:WebResources, DashBoard_MCUPreStartTi%>" runat="server"></asp:Literal>
                                                        </td><td style="width:1px"><b>:</b>&nbsp;</td>
                                                        <td align="left" >
                                                            <asp:Label ID="lblMCUPreStart" runat="server" Font-Bold="False"></asp:Label></td><td align="left" Class="blackblodtext" valign="top" nowrap>
                                                            <asp:Literal Text="<%$ Resources:WebResources, DashBoard_MCUPreEndTime%>" runat="server"></asp:Literal>
                                                        </td>
                                                        <td style="width:1px"><b>:</b>&nbsp;</td><td align="left" style="height: 10px;">
                                                            <asp:Label ID="lblMCUPreEnd" runat="server" Font-Bold="False"></asp:Label>
                                                       </td>
                                                    </tr>
							                        <%--ZD 100085 End--%>
                                                    <tr>
                                                        <td align="left" class="DashboardText" valign="top" nowrap=""><asp:Literal Text="<%$ Resources:WebResources, DashBoard_Status%>" runat="server"></asp:Literal></td>
                                                        <td style="width:1px; vertical-align:top"><b>:</b>&nbsp;</td>
                                                        <td align="left" valign="top" nowrap>
                                                            <asp:Label ID="lblStatus" CssClass="DashboardText" runat="server" Font-Bold="False"></asp:Label></td>
                                                            <%if (!(Application["Client"] == "MOJ"))
                                                              { %>
                                                            <td align="left" valign="top" class="DashboardText" id="tdType" runat="server" nowrap=""><asp:Literal Text="<%$ Resources:WebResources, DashBoard_tdType%>" runat="server"></asp:Literal></td>
                                                            <td style="width:1px; vertical-align:top"><b>:</b>&nbsp;</td>
                                                            <td align="left" nowrap>
                                                                <asp:Label ID="lblConfType" CssClass="DashboardText" runat="server" Font-Bold="false"></asp:Label>
                                                            </td>
                                                            <%}%>
                                                    </tr>
                                                    <tr id="trPuPw" runat="server">
                                                        <td align="left" class="DashboardText" nowrap="nowrap"><asp:Literal Text="<%$ Resources:WebResources, DashBoard_Public%>" runat="server"></asp:Literal></td>
                                                        <td style="width:1px"><b>:</b>&nbsp;</td>
                                                        <td align="left" nowrap>
                                                            <asp:Label ID="lblPublic" CssClass="DashboardText" runat="server" Font-Bold="False"></asp:Label>
                                                            <asp:Label ID="lblRegistration" runat="server" Font-Bold="False"></asp:Label></td>
                                                        <td align="left" class="DashboardText" nowrap="nowrap">
                                                            <asp:Literal Text="<%$ Resources:WebResources, DashBoard_Password%>" runat="server"></asp:Literal></td>
                                                        <td style="width:1px"><b>:</b>&nbsp;</td>
                                                        <td align="left" style="height: 21px;" nowrap="nowrap">
                                                            <asp:Label CssClass="DashboardText" ID="lblPassword" Font-Bold="False" runat="server"></asp:Label>&nbsp;</td>
                                                    </tr>
                                                    <%--FB 2501 Starts--%>
                                                     <tr><%--FB 2694--%>
                                                        <td id="tdReminders" runat="server" align="left" valign="top" class="DashboardText" nowrap>
                                                            <asp:Literal Text="<%$ Resources:WebResources, DashBoard_tdReminders%>" runat="server"></asp:Literal></td>
                                                        <td id="tdReminders1" runat="server"  style="width:1px; vertical-align:top"><b>:</b>&nbsp;</td>
                                                        <td id="tdReminderSelections" runat="server" align="left" valign="top" nowrap>
                                                            <asp:Label ID="lblReminders" runat="server" Font-Bold="False" class="DashboardText"></asp:Label>&nbsp;</td>
                                                        <td align="left" class="DashboardText" id="trVNOC" runat="server" nowrap> 
                                                            <asp:Literal Text="<%$ Resources:WebResources, DashBoard_trVNOC%>" runat="server"></asp:Literal></td>
                                                        <td id = "tdvnoccol" runat="server" style="width:1px; vertical-align:top"><b>:</b>&nbsp;</td><%--FB 2670--%>
                                                        <td  id="trVNOCoptor" runat="server" align="left" style="height: 21px;" colspan="2" valign="top"  nowrap>
                                                            <asp:Label ID="lblConfVNOC" runat="server" class="DashboardText" style="font-weight:normal;"></asp:Label>&nbsp;</td><%--FB 2837--%>
                                                    </tr>                                                    
                                                    <tr id="trStModeNwStarte" runat="server">
                                                    <%--FB 2595 Starts--%>
                                                    <td align="left" class="DashboardText" valign="top" id="tdStartMode" runat="server" nowrap=""><asp:Literal Text="<%$ Resources:WebResources, DashBoard_tdStartMode%>" runat="server"></asp:Literal></td>
                                                        <td id="tdStartMode1" runat="server" style="width:1px; vertical-align:top"><b>:</b>&nbsp;</td>
                                                        <td  id="tdStartModeSelection" runat="server" align="left" valign="top"  nowrap>
                                                            <asp:Label ID="lblStartMode" runat="server" class="DashboardText" style="font-weight:normal;"></asp:Label>&nbsp;</td><%--FB 2837--%>
                                                            <td align="left" class="DashboardText" valign="top" id="tdSecured" runat="server" nowrap> 
                                                            <asp:Literal Text="<%$ Resources:WebResources, DashBoard_tdSecured%>" runat="server"></asp:Literal></td>
                                                        <td id="tdSecured1" runat="server" style="width:1px; vertical-align:top"><b>:</b>&nbsp;</td>
                                                        <td  id="tdSecuredSelection" runat="server" align="left" style="height: 21px;" colspan="2" valign="top"  nowrap>
                                                            <asp:Label ID="lblSecured" runat="server" class="DashboardText" style="font-weight:normal;"></asp:Label>&nbsp;</td><%--FB 2837--%>
                                                     </tr>
                                                     <%--FB 2501 Ends--%><%--FB 2595 Ends --%>                                                   
                                                    <tr id="trFle" runat="server" >
                                                        <td align="left" Class="DashboardText" nowrap>
                                                            <asp:Literal Text="<%$ Resources:WebResources, DashBoard_Files%>" runat="server"></asp:Literal> </td>
                                                        <td style="width:1px;"><b>:</b>&nbsp;</td>
                                                        <td align="left" colspan="3" nowrap>
                                                            <asp:Label CssClass="DashboardText" ID="lblFiles" runat="server" Font-Bold="False"></asp:Label></td>
                                                    </tr>
                                                    <tr>
                                                        <td id="tdDescription" runat="server" align="left" Class="DashboardText" valign="top" ><%--ZD 100085--%><%--ZD 100397--%>
                                                            Description </td>
                                                        <td style="width:1px; vertical-align:top"><b>:</b>&nbsp;</td>
                                                        <td align="left" colspan="3" valign="top">
                                                            <asp:Label CssClass="DashboardText" ID="lblDescription" runat="server" Font-Bold="False"></asp:Label>&nbsp;</td>
                                                    </tr>                                                     
                                                            </table>
                                                        </td>
                                                        <%--change --%>
                                                        <td align="left" width="250px"  colspan="1" rowspan="8" valign="top" >
                                                             <table style="width:250px; margin-right: 0px;" valign="super" cellspacing="0" border="0">
                                                             <tr>
                                                             <td>
                                                             <asp:Table ID="tblActions" runat="server" BorderStyle="None" BorderWidth="0" CssClass="DashBackGround" CellPadding="0" CellSpacing="0" Width="100%" Height="100%" >
                                    <asp:TableRow ID="TableRow1" runat="server" CssClass="DashBoardHeader" >
                                        <asp:TableCell ID="TableCell1" runat="server" VerticalAlign="Middle" ForeColor="#f1f1f1"  HorizontalAlign=right><asp:Literal ID="Literal2" Text="<%$ Resources:WebResources, MeetingPlanner_Conference%>" runat="server"></asp:Literal></asp:TableCell>
                                        <asp:TableCell ID="TableCell4" runat="server" VerticalAlign="Middle" ForeColor="#f1f1f1" HorizontalAlign=left>&nbsp;<asp:Literal ID="Literal3" Text="<%$ Resources:WebResources, Actions%>" runat="server"></asp:Literal> &nbsp;&nbsp;&nbsp;</asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow ID="TableRow2" runat="server" Height="20px"> <%--ZD 102342 hided btnPDF and moved all right side cell one row upwards --%>
                                       <asp:TableCell ID="ss" runat="server" >
                                           <img style="width:12px" src="image/mute.png" alt="Mute All" /> <%--ZD 100419--%> 
                                            <asp:LinkButton ID="LinkButton1" Font-Size="Smaller" Font-Underline="true" ForeColor="Black" Text="" runat="server" OnClick="MuteEndpointAll" ></asp:LinkButton><%--FB 2530--%> 
                                       </asp:TableCell>
                                        <asp:TableCell ID="TableCell2" runat="server" Visible="false">
                                            <img style="width:12px" src="image/adobe.gif" alt="PDF" /><%-- ZD 100419--%>  <%--ZD 100429--%>
                                            <asp:LinkButton ID="btnPDF" Text="<%$ Resources:WebResources, DashBoard_btnPDF%>" runat="server" Font-Size="Smaller" Enabled="false" Font-Underline="true" ForeColor="Black" OnClick="ExportToPDF" ></asp:LinkButton>
                                        </asp:TableCell>
                                         <asp:TableCell ID="TableCell3" runat="server">
                                              <img src="image/Print.gif" style="width:12px" alt="Print" /><%-- ZD 100419--%>
                                              <%-- code added for FB 1764--%><%--Starts--%>
                                             <a href="#" onclick="javascript:window.print();" style="font-size:smaller; color:Black; text-decoration:underline;"><asp:Literal ID="Literal5" Text="<%$ Resources:WebResources, DashBoard_Print%>" runat="server"></asp:Literal></a>
                                             <%--Ends--%>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow ID="TableRow3" runat="server" Height="20px">
                                        <asp:TableCell ID="TableCell5" runat="server">
                                            <img style="width:12px" src="../image/MonitorMCU/layout_0.gif" alt="Change Layout All" /> <%-- ZD 100419--%><%--ZD 100429--%>
                                            <asp:LinkButton ID="LinkButton2" Font-Size="Smaller" Font-Underline="true" ForeColor="Black" Text="<%$ Resources:WebResources, DashBoard_LinkButton2%>"  runat="server"></asp:LinkButton>
                                            <asp:Button ID="btnLyout"  runat="server" style="display:none;" OnClick="ChangeVideoDisplay" />
                                       </asp:TableCell>
                                       <%-- FB 2152 Starts--%>
                                       <asp:TableCell ID="TableCell6" runat="server">
                                            <img src="image/saveoutlook.bmp" visible="false"  style="width:12px; display:none" alt="Saveoutlook"  /><%-- ZD 100419--%>
                                            <asp:LinkButton ID="btnOutlook" Text="<%$ Resources:WebResources, DashBoard_btnOutlook%>" Font-Size="Smaller" Font-Underline="true" ForeColor="Black" Visible="false" runat="server" OnClientClick="javascript:saveToOutlookCalendar('0','0','1','');return false;"></asp:LinkButton>
                                        
                                            <img border="0" src="../image/MonitorMCU/addUser.gif" id="Img1" width="15px"   alt="Add User"  /><%-- ZD 100419--%> <%--ZD 100429--%>
                                           <asp:LinkButton ID="btnAddEndpoint" Font-Size="Smaller" Font-Underline="true" ForeColor="Black" Text="<%$ Resources:WebResources, DashBoard_btnAddEndpoint%>" runat="server"  OnClick="AddNewEndpoint" ></asp:LinkButton>
                                           
                                           <%--FB 2152 End--%>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow ID="TableRow4" runat="server" Height="20px">
                                        <asp:TableCell ID="TableCell7" runat="server">
                                            <img style="width:15px" src="../image/MonitorMCU/time_1.gif" alt="Extend Time" /><%-- ZD 100419--%> <%--ZD 100429--%>
                                       <asp:LinkButton ID="LinkButton3" Font-Size="Smaller" Font-Underline="true" ForeColor="Black" Text="<%$ Resources:WebResources, DashBoard_LinkButton3%>" runat="server" OnClientClick="javascript:return fnextendtime('1');"></asp:LinkButton>
                                       </asp:TableCell>
                                        <asp:TableCell ID="TableCell8" runat="server">
                                            <img src="image/edit.gif" style="width:12px" alt="Edit" /><%-- ZD 100419--%>
                                            <asp:Button style="display:none;" runat="server" ID="Temp" OnClick="EditConference" /> <%--ZD 101597--%>
                                            <asp:LinkButton ID="btnEdit" Text="<%$ Resources:WebResources, DashBoard_btnEdit%>" Font-Size="Smaller" Font-Underline="true" ForeColor="Black" runat="server" OnClick="EditConference"  ></asp:LinkButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow  runat="server" Height="20px" ID="trED">
                                        <asp:TableCell ID="TableCell11" runat="server">
                                            <img style="width:12px;cursor:pointer;" src="image/btn_delete.gif"  title="Delete All" alt="Delete"  /> <%--FB 2798--%><%-- ZD 100419--%>
                                       <asp:LinkButton ID="btnDeleteConf" Font-Size="Smaller" Font-Underline="true" ForeColor="Black" Text="<%$ Resources:WebResources, DashBoard_btnDeleteConf%>" runat="server" OnClientClick="javascript:return btnDeleteConference_Click();" OnClick="DeleteConference"></asp:LinkButton>
                                       </asp:TableCell>
                                       <%--FB 2441 Starts--%>
                                        <asp:TableCell ID="TableCell12" runat="server">
                                          <asp:LinkButton ID="lnkMuteAllExcept" Visible="false" Text="<%$ Resources:WebResources, DashBoard_lnkMuteAllExcept%>" Font-Size="Smaller" Font-Underline="true" ForeColor="Black" runat="server"  OnClientClick="javascript:return fnShowHide('1')"></asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp;
                                        </asp:TableCell>
                                       <%--FB 2441 Ends--%>
                                       <asp:TableCell ID="tcAEP" runat="server" >
                                           <%-- <img border="0" src="Image/endpointadd.png" id="Img1" width="16" height="16"  />//FB 2152
                                            <asp:LinkButton ID="btnAddEndpoint" Font-Size="Smaller" Font-Underline="true" ForeColor="Black" Text="Add Endpoint" runat="server"  OnClick="AddNewEndpoint" ></asp:LinkButton>--%>
                                         </asp:TableCell>
                                     </asp:TableRow>
                                       <asp:TableRow ID="TableRow6" runat="server" Height="20px">
                                       <asp:TableCell ID="TableCell13" runat="server">
                                            <img style="width:12px" src="../image/MonitorMCU/delete_1.gif" alt="Terminate" /><%-- ZD 100419--%> <%--ZD 100429--%>
                                       <asp:LinkButton ID="LinkButton4" Font-Size="Smaller" Font-Underline="true" ForeColor="Black" Text="<%$ Resources:WebResources, DashBoard_LinkButton4%>" runat="server" OnClientClick="javascript:return btnTerminateConference_Click();" OnClick="DeleteConference"></asp:LinkButton> <%--GP Fixes--%>
                                       </asp:TableCell>
                                        <%--FB 2441 Starts--%>
                                       <asp:TableCell ID="TableCell10" runat="server" >
                                            <asp:LinkButton ID="lnkUnMuteAllParties" Font-Size="Smaller" Visible=false Font-Underline="true" ForeColor="Black" Enabled="true" runat="server" Text="<%$ Resources:WebResources, DashBoard_lnkUnMuteAllParties%>" OnClick="UnMuteAllParties" ></asp:LinkButton>&nbsp;&nbsp;
                                         </asp:TableCell>
                                          <%--FB 2441 Ends--%>
                                       </asp:TableRow>
                                       <asp:TableRow ID="TableRow7" runat="server" Height="20px">
                                       <asp:TableCell ID="TableCell9" runat="server">
                                            <img style="width:12px" src="image/mcusetup.gif" alt="Setup on MCU" /><%-- ZD 100419--%>
                                       <asp:LinkButton ID="LinkButton6" Font-Size="Smaller" Font-Underline="true" ForeColor="Black" Text="<%$ Resources:WebResources, DashBoard_LinkButton6%>" runat="server" OnClientClick="javascript:btnSetupAtMCU_Click();" OnClick="btnSetupAtMCU_Click"></asp:LinkButton>
                                       </asp:TableCell>
                                       
                                       </asp:TableRow>
                                       </asp:Table>
                                       <asp:TextBox ID="tempText" TabIndex="-1" TextMode="multiline" Rows="4" style="display:none;" runat="server" width="0px" Height="0px" ForeColor="black" BackColor="transparent" BorderColor="transparent"></asp:TextBox>
                                                 </td>
                                                 </tr>
                                                 <tr>
                                                 <td align="right">
                                                 <a id="A1" tabindex="-1" href="#"></a>
                                                 </td>
                                                 </tr>
                                                 <tr>
                                                 <td align="right">
                                                 <a id="A2" tabindex="-1" href="#"></a>
                                                 </td>
                                                 </tr>
                                                 <tr>
                                                 <td align="right">
                                                 <a id="A4" tabindex="-1" href="#"></a>
                                                 </td>
                                                 </tr>
                                                 </table>
                                             
                                                        </td>
                                                    </tr>
                                                    
                                                    </table><%--FB 2508--%>

                                                    <table width="100%">
                                                    <tr>
                                                        <td valign="top" align="center">
                                                        <a id="A3" href="#" runat="server" onclick="javascript:opnmanage();"><asp:Literal Text="<%$ Resources:WebResources, DashBoard_A3%>" runat="server"></asp:Literal></a> <%-- FB 1733 --%> <%--FB 2664--%>
                                                        </td>
                                                            
                                                    </tr>
                                                    </table>
                                                </td>
                                                </tr>
                                                    <tr>
                                                        <td align="center" colspan="4">
                <div style="width:100%; height: 323px;" >
                                                               <asp:Panel ID="pnlEndpoint" runat="server" Width="100%">
                                                               
                                        <asp:Table runat="server" ID="tblEndpoints" Width="100%">
                                            <asp:TableRow>
                                                <asp:TableCell HorizontalAlign="center">
                                                                                            
                                                                    <asp:DataGrid runat="server"
                                                                        EnableViewState="true" OnItemDataBound="InitializeEndpoints" ID="dgEndpoints" 
                                                                        AutoGenerateColumns="false" CellSpacing="0" CellPadding="4" GridLines="None" 
                                                                        ShowFooter="true"  OnEditCommand="EditEndpoint"  
                                                                        OnDeleteCommand="DeleteTerminal" OnUpdateCommand="ConnectEndpoint" 
                                                                        OnCancelCommand="MuteEndpoint" BorderColor="blue" BorderStyle="solid" 
                                                                        BorderWidth="1"  Width="99%" AllowPaging="true" OnSortCommand="SortGrid"
                                                                        PagerStyle-HorizontalAlign="Right" AllowSorting="true" OnPageIndexChanged="paging"  PageSize="3">                                                                                  
                                                                         <PagerStyle HorizontalAlign="Left" Mode="NextPrev" NextPageText="<%$ Resources:WebResources, Next%>" PageButtonCount="5" PrevPageText="<%$ Resources:WebResources, Previous%>"/> <%-- FB 1733--%>
                                                                                 
                                                                         <AlternatingItemStyle CssClass="DashBoardGridItem" />
                                                                         <ItemStyle CssClass="DashBoardGridItem"  />
                                                                         <FooterStyle CssClass="DashBoardGridItem" />
                                                                         <HeaderStyle CssClass="DashBoardGridHeader" Height="30px" />
                                                                         <EditItemStyle CssClass="DashBoardGridItem" />
                                                                         <SelectedItemStyle CssClass="DashBoardGridItem"  Font-Bold="True"/>
                                                                       
                                                                        <Columns>
                                                                            <asp:BoundColumn DataField="ID" Visible="false"></asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="type" Visible="false"></asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="displayLayout" Visible="false"></asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="mute" Visible="false"></asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="ImageURL" Visible="false"></asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="Name"  HeaderText="<%$ Resources:WebResources, ManageConference_RmAttendee%>"  HeaderStyle-CssClass="DashBoardGridHeader"  SortExpression="Name" >
                                                                                <HeaderStyle CssClass="DashBoardGridHeader" />
                                                                            </asp:BoundColumn> 
                                                                            <asp:BoundColumn DataField="EndpointName"  HeaderText="<%$ Resources:WebResources, ManageConference_EptName%>"  HeaderStyle-CssClass="DashBoardGridHeader" SortExpression="EndpointName">
                                                                                <HeaderStyle CssClass="DashBoardGridHeader" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn  HeaderText="<%$ Resources:WebResources, ManageConference_EptStatus%>" HeaderStyle-CssClass="DashBoardGridHeader" Visible="false" >
                                                                                <HeaderStyle CssClass="DashBoardGridHeader" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="address"  HeaderText="<%$ Resources:WebResources, EditEndpoint_Address%>" HeaderStyle-CssClass="DashBoardGridHeader">
                                                                                <HeaderStyle CssClass="DashBoardGridHeader" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="addressType"  HeaderText="<%$ Resources:WebResources, ManageConference_AddressType%>" HeaderStyle-CssClass="DashBoardGridHeader">
                                                                                <HeaderStyle CssClass="DashBoardGridHeader" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="connectionType"  HeaderText="<%$ Resources:WebResources, ManageConference_ConnType%>" HeaderStyle-CssClass="DashBoardGridHeader">
                                                                                <HeaderStyle CssClass="DashBoardGridHeader" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="MCUName"  HeaderText="MCU" HeaderStyle-CssClass="DashBoardGridHeader" SortExpression="MCUName">
                                                                                <HeaderStyle CssClass="DashBoardGridHeader" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="BridgeAddress"  HeaderText="<%$ Resources:WebResources, ManageConference_MCUAddress%>" HeaderStyle-CssClass="DashBoardGridHeader">
                                                                                <HeaderStyle CssClass="DashBoardGridHeader" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="BridgeAddressType"  HeaderText="<%$ Resources:WebResources, ManageConference_MCUAddressType%>" HeaderStyle-CssClass="DashBoardGridHeader" Visible="false">
                                                                                <HeaderStyle CssClass="DashBoardGridHeader" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="DefaultProtocol"  HeaderText="<%$ Resources:WebResources, Protocol%>" HeaderStyle-CssClass="DashBoardGridHeader"  Visible="false">
                                                                                <HeaderStyle CssClass="DashBoardGridHeader" />
                                                                            </asp:BoundColumn>
                                                                            <%--FB 2839--%>
                                                                             <asp:BoundColumn DataField="BridgeProfileName" HeaderText="<%$ Resources:WebResources, ManageConference_MCUProfile%>" HeaderStyle-CssClass="DashBoardGridHeader" >
                                                                              <HeaderStyle CssClass="DashBoardGridHeader" />
                                                                            </asp:BoundColumn> 
                                                                             <asp:BoundColumn DataField="BridgePoolOrderName" HeaderText="<%$ Resources:WebResources, PoolOrder%>" HeaderStyle-CssClass="DashBoardGridHeader" > <%--ZD 104256--%>
                                                                           <HeaderStyle CssClass="DashBoardGridHeader" />  <%--ZD 104256--%>
                                                                            </asp:BoundColumn> 
                                                                            <asp:TemplateColumn ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="DashBoardGridHeader" HeaderText="<%$ Resources:WebResources, EndpointActions%>" HeaderStyle-HorizontalAlign="center" FooterStyle-HorizontalAlign="right">
                                                                                <ItemTemplate>
                                                                                    <table cellspacing="5" width="100%" border="0">
                                                                                        <tr width="100%" align="center">
                                                                                            <td>
                                                                                            <asp:Image ID="imgVideoLayout" runat="server" Width="30" Height="30" ImageUrl='<%# DataBinder.Eval(Container, "DataItem.ImageURL") %>' style="display:none;" AlternateText="Video Layout" /> <%--ZD 100419--%>
                                                                                            <asp:Button CssClass="altShortBlueButtonFormat" ID="btnChangeEndpointLayout"  runat="server" Text="<%$ Resources:WebResources, DashBoard_btnChangeEndpointLayout%>" style="display:none;" />
                                                                                            <asp:LinkButton ID="btnCon"  CommandName="Update" Text="<%$ Resources:WebResources, DashBoard_btnCon%>" runat="server"></asp:LinkButton></td>                                                                       
                                                                                            <td><asp:LinkButton ID="btnEdit" Text="<%$ Resources:WebResources, DashBoard_btnEdit%>" CommandName="Edit" runat="server"></asp:LinkButton></td>
                                                                                            <td><asp:LinkButton ID="btnMute" Text="<%$ Resources:WebResources, DashBoard_btnMute%>" Visible="false" CommandName="Cancel" runat="server"></asp:LinkButton></td>
                                                                                            <td><asp:LinkButton ID="btnDelete" Text="<%$ Resources:WebResources, DashBoard_btnDelete%>" CommandName="Delete" runat="server"></asp:LinkButton></td>
                                                                                            <td><asp:LinkButton ID="btnChangeLayout" Text="<%$ Resources:WebResources, DashBoard_btnChangeLayout%>" runat="server" Visible="false"></asp:LinkButton></td>
                                                                                            
                                                                                        </tr>
                                                                                    </table>
                                                                                </ItemTemplate>
                                                                               
                                                                                
                                                                                <HeaderStyle CssClass="DashBoardGridHeader" HorizontalAlign="Center" 
                                                                                    Width="100px" />
                                                                                <ItemStyle HorizontalAlign="Center" Width="100px" />
                                                                             <FooterTemplate > 
                                                                                                <asp:Label ID="lblCountOngoingText" runat="server"  CssClass="DashboardText"></asp:Label>
                                                                                                <asp:Label ID="lblCountonGoing" runat="server"  CssClass="DashboardText"></asp:Label>
                                                                                </FooterTemplate>    
                                                                            </asp:TemplateColumn>
                                                                            <asp:TemplateColumn ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="DashBoardGridHeader"   FooterStyle-HorizontalAlign="Left" FooterStyle-Width="250px">
                                                                                <FooterTemplate > 
                                                                                                <asp:Label ID="lblCountText" runat="server"  CssClass="DashboardText"></asp:Label>
                                                                                                <asp:Label ID="lblCount" runat="server"  CssClass="DashboardText"></asp:Label>
                                                                                </FooterTemplate>
                                                                            </asp:TemplateColumn>
                                                                            <%--FB 1650--%>
                                                                            <asp:BoundColumn DataField="CascadeLinkId" Visible="false"></asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="EndPointStatus" Visible="false"></asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="isMonitorDMA" Visible="false"></asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="BridgeTypeId" Visible="false"></asp:BoundColumn>
                                                                        </Columns>
                                                                        
                                                                    </asp:DataGrid>
                                                                    </asp:TableCell>
                                                                    </asp:TableRow>
                                                                    
                                                                    </asp:Table>
                                                                    <asp:Table runat="server" ID="tblP2PEndpoints" Width="100%" Visible="false">
                                            <asp:TableRow>
                                                <asp:TableCell HorizontalAlign="center" runat="server" Visible="false" ID="refreshCell">
                                                    <table width="90%" border="0"> 
                                                        <tr>
                                                            <td align="right" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, DashBoard_BroadcastMessa%>" runat="server"></asp:Literal></td>
                                                            <td>
                                                                <input type="text" Height="35px" onkeyup="javascript:chkLimit(this,'50');" ID="TxtMessageBoxAll" runat="server" />
                                                                <asp:Button ID="SendMsgAll" CssClass="altShortBlueButtonFormat" Text="<%$ Resources:WebResources, DashBoard_SendMsgAll%>" runat="server"  OnClientClick="javascript:return fnchkValue('TxtMessageBoxAll')" OnClick="BroadCastP2PMsg"  />
                                                            </td>
                                                            <td align="right" class="blackblodtext" style="display:none;">
                                                               <asp:Literal Text="<%$ Resources:WebResources, DashBoard_AutoRefresh30%>" runat="server"></asp:Literal></td>
                                                            <td style="display:none;">
                                                                <asp:CheckBox ID="P2pAutoRef" runat="server" Checked="false" onclick="javascript:UpdateP2PEndpointStatus();" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow>
                                                 <asp:TableCell HorizontalAlign="center">
                                                    <%--Code Modified For FB 1422 - To display Point to point End Point Details - Start--%>
                                                    <asp:DataGrid runat="server" EnableViewState="true" ID="dgP2PEndpoints" AutoGenerateColumns="false" OnCancelCommand="SendP2PMessage" OnItemDataBound="InitializeP2PEndpoints"
                                                          style="border-collapse:separate" CellSpacing="0" CellPadding="4" GridLines="None" BorderColor="blue" BorderStyle="solid" BorderWidth="1"  Width="100%" OnUpdateCommand="ConnectP2PEndpoint" OnPageIndexChanged="paging"  PageSize="3">
                                                                                  
                                                                         <PagerStyle HorizontalAlign="Right" Mode="NextPrev" NextPageText="<" PageButtonCount="5" PrevPageText=">"/>
                                                                                 
                                                                         <AlternatingItemStyle CssClass="DashBoardGridItemAlternating" />
                                                                         <ItemStyle CssClass="DashBoardGridItem"  />
                                                                         <FooterStyle CssClass="DashBoardGridItem" />
                                                                         <HeaderStyle CssClass="DashBoardGridHeader" Height="30px" />
                                                                         <EditItemStyle CssClass="DashBoardGridItem" />
                                                                         <SelectedItemStyle CssClass="DashBoardGridItem"  Font-Bold="True"/>
                                                        <Columns>
                                                            <asp:BoundColumn DataField="ID" Visible="false"></asp:BoundColumn> 
                                                            <asp:BoundColumn DataField="type" Visible="false"></asp:BoundColumn>   
                                                            <asp:BoundColumn DataField="endpointID" Visible="false"></asp:BoundColumn> 
                                                            <asp:BoundColumn DataField="name" HeaderStyle-CssClass="DashBoardGridHeader" Visible="true" HeaderText="<%$ Resources:WebResources, ManageConference_RmAttendee%>"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="EndpointName" HeaderStyle-CssClass="DashBoardGridHeader" Visible="true" HeaderText="<%$ Resources:WebResources, ManageConference_EptName%>"></asp:BoundColumn>                                                             
                                                            <asp:BoundColumn DataField="address" HeaderStyle-CssClass="DashBoardGridHeader" Visible="false"></asp:BoundColumn>                                                           
                                                            <asp:TemplateColumn ItemStyle-HorizontalAlign="center" HeaderStyle-Width="100" ItemStyle-Width="100" HeaderStyle-CssClass="DashBoardGridHeader" HeaderText="<%$ Resources:WebResources, Address%>" HeaderStyle-HorizontalAlign="center" FooterStyle-HorizontalAlign="right">
                                                                <ItemTemplate>
                                                                    <asp:HyperLink style="cursor:hand;" NavigateUrl="#" ID="EptWebsite" runat="server" CssClass="treeRootNode"></asp:HyperLink> 
                                                                </ItemTemplate>                                                               
                                                            </asp:TemplateColumn> 
                                                            <asp:BoundColumn DataField="addressType" HeaderStyle-CssClass="DashBoardGridHeader" Visible="true" HeaderText="<%$ Resources:WebResources, ManageConference_AddressType%>"></asp:BoundColumn> 
                                                            <asp:BoundColumn DataField="DefaultProtocol" HeaderStyle-CssClass="DashBoardGridHeader"  Visible="true" HeaderText="<%$ Resources:WebResources, Protocol%>"></asp:BoundColumn>  
                                                            <asp:BoundColumn DataField="Connect2" HeaderStyle-CssClass="DashBoardGridHeader"  Visible="true" HeaderText="<%$ Resources:WebResources, ManageConference_CallerCallee%>"></asp:BoundColumn>
                                                            <asp:BoundColumn ItemStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="center" ItemStyle-Width="90" HeaderText="<%$ Resources:WebResources, ManageConference_EptStatus%>" HeaderStyle-CssClass="DashBoardGridHeader"></asp:BoundColumn> <%--Edited for Blue Status--%>
                                                            <%-- Window Dressing end--%>
                                                             <asp:TemplateColumn ItemStyle-HorizontalAlign="center" HeaderStyle-Width="170px" ItemStyle-Width="100" HeaderStyle-CssClass="DashBoardGridHeader" HeaderText="<%$ Resources:WebResources, Actions%>" HeaderStyle-HorizontalAlign="center" FooterStyle-HorizontalAlign="right">
                                                                <ItemTemplate>
                                                                    <table cellspacing="5" width="160px" border="0">
                                                                        <tr width="100%" align="center">
                                                                        <td><asp:LinkButton ID="btnConP2P" CommandName="Update" CssClass="treeRootNode" Text="<%$ Resources:WebResources, DashBoard_btnConP2P%>" runat="server"></asp:LinkButton></td>                                                                             
                                                                        <td><asp:HyperLink ID="btnMessage" NavigateUrl="#" CssClass="treeRootNode"  Text="<%$ Resources:WebResources, DashBoard_btnMessage%>" runat="server"></asp:HyperLink></td> 
                                                                        <td><asp:HyperLink ID="EptMonitor" NavigateUrl="#" CssClass="treeRootNode" Text="<%$ Resources:WebResources, DashBoard_EptMonitor%>" runat="server"></asp:HyperLink></td> 
                                                                      </tr>
                                                                        <tr  id="Messagediv" runat="server" style="display:none;z-index:999;cursor:hand" >
                                                                        <td colspan="2" width="120px">
                                                                            <div>
                                                                                <table cellspacing="5"  border="0">
                                                                                    <tr width="100%" align="center">
                                                                                    <td>
                                                                                        <input type="text" Height="35px" onkeyup="javascript:chkLimit(this,'50');" ID="TxtMessageBox" runat="server" />
                                                                                        <asp:Button ID="SendMsg" CssClass="altShortBlueButtonFormat" Text="<%$ Resources:WebResources, DashBoard_SendMsg%>" runat="server"  CommandName="Cancel" />
                                                                                        <input type="button" id="btnClose" onclick="javascript:fnOpenMsg('0')" class="altShortBlueButtonFormat" runat="server" value="<%$ Resources:WebResources, Cancel%>">
                                                                                    </td>
                                                                                    </tr>
                                                                                    </table>
                                                                        </div>
                                                                       </td>                                                                      </tr>
                                                                    </table>
                                                                </ItemTemplate>                                                               
                                                            </asp:TemplateColumn>
                                                            <%--Blue Status Project START--%>
                                                            <asp:BoundColumn DataField="EndPointStatus" Visible="false"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="remoteEndpoint" Visible="false"></asp:BoundColumn>
                                                            <%--Blue Status Project End--%>             
                                                            <asp:BoundColumn DataField="isMonitorDMA" Visible="false"></asp:BoundColumn> <%--FB 2441--%>                                                                                                        
                                                        </Columns>
                                                     </asp:DataGrid>
                                                 <%--Code Modified Fo FB 1422 End--%>      
                                                </asp:TableCell>
                                            </asp:TableRow>
                                        </asp:Table>
                                         <asp:Table runat="server" ID="tblNoEndpoints" Visible="false">
                                            <asp:TableRow>
                                                <asp:TableCell CssClass="lblError"><asp:Literal ID="Literal1" Text="<%$ Resources:WebResources, EndpointList_lblNoEndpoints%>" runat="server"></asp:Literal></asp:TableCell>
                                            </asp:TableRow>
                                            
                                        </asp:Table>
                                                                    
                                        <asp:DropDownList CssClass="altLong0SelectFormat" Visible="false" ID="lstAddressType" runat="server" DataTextField="Name" DataValueField="ID"></asp:DropDownList>
<asp:DropDownList Visible="false" ID="lstProtocol" runat="server" DataTextField="Name" DataValueField="ID">
</asp:DropDownList>

                                    <asp:TextBox ID="ImageFiles" runat="server"  Width="0px" Height="0px" style="display:none"></asp:TextBox>
                                    <asp:TextBox ID="ImageFilesBT" runat="server"  Width="0px" Height="0px" style="display:none"></asp:TextBox>
                                    <asp:TextBox ID="ImagesPath" runat="server"  Width="0px" Height="0px" style="display:none"></asp:TextBox>
                                    <asp:TextBox ID="txtSelectedImage" runat="server" Text="01" Width="0px" Height="0px" style="display:none"></asp:TextBox>
                                    <asp:TextBox ID="txtSelectedImageEP" runat="server" Text="01" Width="0px" Height="0px" style="display:none"></asp:TextBox> <%--Edited for FF--%>
                                    <asp:TextBox ID="txtTempImage" runat="server" Text="01" Width="0px" Height="0px" style="display:none"></asp:TextBox><%--Edited for FF--%>
                                                                   <asp:HiddenField ID="hdngridCount" runat="server" />
                                                                  </asp:Panel>
                                                            </div>
                                                            
                                                        </td>
                                                    </tr>
                                    
                                        

                                        
                                                    </table>
                                                   
                                            </td>
                                        </tr>
                                    </table>
                             
                    <asp:TextBox ID="txtTimeDifference" Visible="false" TabIndex="-1" runat="server"></asp:TextBox>
                    <asp:TextBox ID="txtEndpointType" runat="server" TabIndex="-1" width="0" Height="0" ForeColor="transparent" BackColor="transparent" BorderColor="transparent" style="display:none"></asp:TextBox> <%--Edited for FF--%>
                    <asp:HiddenField ID="hdnConfStatus" runat="server" />
                    <asp:HiddenField ID="hdnConfType" runat="server" />
                    </asp:Panel>
                </td>
                </tr>
                </table>
                
                </ContentTemplate>
                    </asp:UpdatePanel>
            
            </td>
            </tr>
            
        </table>
        <script type="text/javascript">
ChangeViewTypeExtracted();

var prm = Sys.WebForms.PageRequestManager.getInstance();
prm.add_initializeRequest(initializeRequest);

prm.add_endRequest(endRequest);

var postbackElement;
  
  function initializeRequest(sender, args) {
document.body.style.cursor = "wait";
DataLoading(1);
//document.getElementById("btnCompare").disabled = true;



}



function endRequest(sender, args) {document.body.style.cursor = "default";DataLoading(0);
//document.getElementById("btnCompare").disabled =  false;

 
    
}

  
function ChangeViewTypeExtracted()
{
		var lnk = document.getElementById("A3");
		   var confsid = document.getElementById("lblConfID");
		   
if(confsid.value == "")
{
    lnk.disabled =  true;
    lnk.style.color="gray";//FB 2664
    
}

}
function ChangeViewType()
 {
  var tpe = document.getElementById("hdnlisttype");
  var drp = document.getElementById("DrpDwnListView");
   var btn = document.getElementById("btnRefreshRooms");
ChangeViewTypeExtracted();
   
  if(tpe)
  {
        tpe.value = drp.value;
  }
  if(btn)
    btn.click();
 
 }
 
 function checkconf()
 {
    var tpe = document.getElementById("hdnlisttype");
   var btn = document.getElementById("btnRefreshRooms");
   var confsid = document.getElementById("lblConfID");
   
   var args = checkconf.arguments;
   
   if(args)
   {
        if(args[0] != "" && args[0] != "undefined")
        {
           
            confsid.value = args[0];
            tpe.value =args[0];
        }   
   }
  
  if(btn)
    btn.click();
 
 }
 
    function managelayout (dl, epid, epty)
    {
        //Commented for FB 2530 
        //  var drp = document.getElementById("DrpDwnListView");
        //  var lnkButton = document.getElementById("LinkButton2");
        //var drp = document.getElementById("DrpDwnListView");
        //var confGrid = document.getElementById("hdngridCount");//Edited For FF...
        ////Edited For FF...
        // if(drp.value == 2)
        //      {
        //           
        //            if(confGrid.value > 0)
        //            {
        //        change_display_layout_prompt('image/pen.gif', 'Manage Display Layout', epid, epty, dl, 5, document.getElementById('<%=ImageFiles.ClientID%>').value + '|' + document.getElementById('<%=ImageFilesBT.ClientID%>').value, document.getElementById('<%=ImagesPath.ClientID%>').value);
        //     }
        //     }
       change_display_layout_prompt('image/pen.gif', RSManageLayOut, epid, epty, dl, 5, document.getElementById('<%=ImageFiles.ClientID%>').value + '|' + document.getElementById('<%=ImageFilesBT.ClientID%>').value, document.getElementById('<%=ImagesPath.ClientID%>').value);
    }

function change_display_layout_prompt(promptpicture, prompttitle, epid, epty, dl, rowsize, images, imgpath) 
{
    var tempEpid = epid;
	var title = new Array()
	title[0] = "Default ";
	title[1] = "Custom ";
	promptbox = document.createElement('div'); 
	promptbox.setAttribute ('id' , 'prompt');
	document.getElementsByTagName('body')[0].appendChild(promptbox);
	promptbox = eval("document.getElementById('prompt').style");

	promptbox.position = 'absolute'
	//FB 2712 Starts
    promptbox.top = -120+mousedownY + 'px'; //FB 1373 start
	promptbox.left = mousedownX - 500 + 'px'; 
	promptbox.width = rowsize * 100 + 'px';
	promptbox.border = 'outset 1 #bbbbbb' 
	promptbox.height = 400 + 'px';
	//FB 2712 Ends
	promptbox.overflow ='auto'; //FB 1373 End
    //ZD 101869 start
    if (document.getElementById("hdnCodian").value == "1")
         images = "01:02:03:04:05:06:07:08:09:10:11:12:13:14:15:16:17:18:19:20:21:22:23:24:25:26:27:28:29:30:31:32:33:34:35:36:37:38:39:40:41:42:43:44:45:46:47:48:49:50:51:52:53:54:55:56:57:58:59:60:61:62:63:";
    else if(document.getElementById("hdnPolycomMGC").value == "1" || document.getElementById("hdnPolycomRMX").value == "1")
         images = "01:02:03:04:05:06:12:13:14:15:16:17:18:19:20:24:25:33:60:61:62:63:";
    else 
         images = "01:02:03:04:05:06:07:08:09:10:11:12:13:14:15:16:17:18:19:20:21:22:23:24:25:26:27:28:29:30:31:32:33:34:35:36:37:38:39:40:41:42:43:44:45:46:47:48:49:50:51:52:53:54:55:56:57:58:59:60:61:62:63:";
     
        

    //ZD 101869 End

	m = "<table cellspacing='0' cellpadding='0' border='0' width='100%'><tr valign='middle'><td width='22' height='22' style='text-indent:2;' class='titlebar'><img src='" + promptpicture + "' height='18' width='18' alt='Layout'></td><td class='titlebar'>" + prompttitle + "</td></tr></table>"  //ZD 100419
	m += "<table cellspacing='2' cellpadding='2' border='0' width='100%' class='promptbox'>";
	imagesary = images.split(":");
	rowNum = parseInt( (imagesary.length + rowsize - 2) / rowsize, 10 );
	m += "  <tr><td align='right' colspan='" + (rowsize * 2) + "'>"
	m += "    <input type='button' class='altMedium0BlueButtonFormat' value='<asp:Literal Text='<%$ Resources:WebResources, Submit%>' runat='server'></asp:Literal>' onMouseOver='this.style.border=\"1 outset #dddddd\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='saveOrder(\"" + dl + "\", epid, \"" + epty + "\",\"" + tempEpid + "\");'>"
	m += "    <input type='button' class='altMedium0BlueButtonFormat' value='<asp:Literal Text='<%$ Resources:WebResources, Cancel%>' runat='server'></asp:Literal>' onMouseOver='this.style.border=\"1 outset transparent\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='cancelthis();'>"
	m += "  </td></tr>"
	m += "	<tr>";
	m += "    <td colspan='" + (rowsize * 2) + "' align='left'><b><asp:Literal Text='<%$ Resources:WebResources, ManageConference_DisplayLayout%>' runat='server'></asp:Literal></b></td>";
	m += "  </tr>"
	m += "  <tr>"
	m += "    <td colspan='" + (rowsize * 2) + "' height='5'></td>";
	m += "  </tr>"


    //ZD 101869 start
        if (document.getElementById("hdnCodian").value == "1") {

            m += "<tr><td colspan='" + (rowsize * 2) + "'>";
            m += "<table>";
            m += "  <tr>";
            m += "    <td valign='middle'>";
            m += "      <input type='radio' name='layout' id='layout' value='100' onClick='epid=100'>";
            m += "    </td>";
            m += "    <td valign='middle' align='left' class='blackblodtext'>" + Defaultfamily + "</td>";
            m += "  </tr>";

            m += "  <tr>";
            m += "    <td  valign='middle'>";
            m += "      <input type='radio' name='layout' id='layout' value='101' onClick='epid=101'>";
            m += "    </td>";
            m += "    <td valign='middle' align='left' class='blackblodtext'>" + Family1 + "</td>";
            m += "    <td valign='middle'>";
            m += "      <img src='" + imgpath + "05"+ ".gif' width='57' height='43' alt='Layout'>";
            m += "    </td>";
            m += "    <td></td>";
            m += "    <td valign='middle'>";
            m += "      <img src='" + imgpath + "06" + ".gif' width='57' height='43' alt='Layout'>";
            m += "    </td>";
            m += "    <td></td>";
            m += "    <td valign='middle'>";
            m += "      <img src='" + imgpath + "07" + ".gif' width='57' height='43' alt='Layout'>";
            m += "    </td>";
            m += "    <td></td>";
            m += "    <td valign='middle'>";
            m += "      <img src='" + imgpath + "44" + ".gif' width='57' height='43' alt='Layout'>";
            m += "    </td>";
            m += "  </tr>";

            m += "  <tr>"
            m += "    <td height='1'></td>";
            m += "  </tr>"

            m += "  <tr>";
            m += "    <td  valign='middle'>";
            m += "      <input type='radio' name='layout' id='layout' value='102' onClick='epid=102'>";
            m += "    </td>";
            m += "    <td valign='middle' align='left' class='blackblodtext'>" + Family2 + "</td>";
            m += "    <td valign='middle'>";
            m += "      <img src='" + imgpath + "01" + ".gif' width='57' height='43' alt='Layout'>"; 
            m += "    </td>";
            m += "  </tr>";

            m += "  <tr>"
            m += "    <td height='1'></td>";
            m += "  </tr>"

            m += "  <tr>";
            m += "    <td  valign='middle'>";
            m += "      <input type='radio' name='layout' id='layout' value='103' onClick='epid=103'>";
            m += "    </td>";
            m += "    <td valign='middle' align='left' class='blackblodtext'>" + Family3 + "</td>";
            m += "    <td valign='middle'>";
            m += "      <img src='" + imgpath + "02" + ".gif' width='57' height='43' alt='Layout'>";
            m += "    </td>";
            m += "  </tr>";

            m += "  <tr>"
            m += "    <td height='1'></td>";
            m += "  </tr>"


            m += "  <tr>";
            m += "    <td valign='middle'>";
            m += "      <input type='radio' name='layout' id='layout' value='104' onClick='epid=104'>";
            m += "    </td>";
            m += "    <td valign='middle' align='left' class='blackblodtext'>" + Family4 + "</td>";
            m += "    </td>";
            m += "    <td valign='middle'>";
            m += "      <img src='" + imgpath + "02" + ".gif' width='57' height='43' alt='Layout' >";
            m += "    </td>";
            m += "    <td></td>";
            m += "    <td valign='middle'>";
            m += "      <img src='" + imgpath + "03" + ".gif' width='57' height='43' alt='Layout'>";
            m += "    </td>";
            m += "    <td></td>";
            m += "    <td valign='middle'>";
            m += "      <img src='" + imgpath + "04" + ".gif' width='57' height='43' alt='Layout'>";
            m += "    </td>";
            m += "    <td></td>";
            m += "    <td valign='middle'>";
            m += "      <img src='" + imgpath + "43" + ".gif' width='57' height='43' alt='Layout'>";
            m += "    </td>";
            m += "  </tr>";

            m += "  <tr>"
            m += "    <td height='1'></td>";
            m += "  </tr>"

            m += "    <tr>";
            m += "    <td valign='middle'>";
            m += "      <input type='radio' name='layout' id='layout' value='105' onClick='epid=105'>";
            m += "    </td>";
            m += "    <td valign='middle' align='left' class='blackblodtext'>" + Family5 + "</td>";
            m += "    </td>";
            m += "    <td valign='middle'>";
            m += "      <img src='" + imgpath + "25" + ".gif' width='57' height='43' alt='Layout'>";
            m += "    </td>";
            m += "  </tr>"

            m += "  <tr>"
            m += "    <td height='1'></td>";
            m += "  </tr>"

            m += "</table>";
            m += " </td></tr>"

            m += "  <tr>"
            m += "    <td colspan='" + (rowsize * 2) + "' height='5'></td>";
            m += "  </tr>"

        }
        //ZD 101869 End


	imgno = 0;
	for (i = 0; i < rowNum; i++) 
	{
		m += "  <tr>";
		for (j = 0; (j < rowsize) && (imgno < imagesary.length-1); j++) {
			
		
			m += "    <td valign='middle'>";
			m += "      <input type='radio' name='layout' id='layout' value='" + imagesary[imgno] + "' onClick='epid=" + imagesary[imgno] + ";'>";
			m += "    </td>";
			m += "    <td valign='middle'>";
			m += "      <img src='" + imgpath + imagesary[imgno] + ".gif' width='57' height='43' alt='Layout'>"; //ZD 100419//ZD 101869 
			m += "    </td>";
			imgno ++;
		}
		m += "  </tr>";
	}
    
	m += "  <tr>";
	m += "    <td colspan='" + (rowsize * 2) + "' height='5'></td>";
	m += "  </tr>"
	m += "  <tr><td align='right' colspan='" + (rowsize * 2) + "'>"
	m += "    <input type='button' class='altMedium0BlueButtonFormat' value='<asp:Literal Text='<%$ Resources:WebResources, Submit%>' runat='server'></asp:Literal>' onMouseOver='this.style.border=\"1 outset #dddddd\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='saveOrder(\"" + dl + "\", epid, \"" + epty + "\",\"" + tempEpid + "\");'>"
	m += "    <input type='button' class='altMedium0BlueButtonFormat' value='<asp:Literal Text='<%$ Resources:WebResources, Cancel%>' runat='server'></asp:Literal>' onMouseOver='this.style.border=\"1 outset transparent\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='cancelthis();'>"
	m += "  </td></tr>"
	m += "</table>" 
	
	document.getElementById('prompt').innerHTML = m;
} 

function pdfReport() 
    {
    
        var loc = document.location.href; 
        loc = loc.substring(0,loc.indexOf("Dashboard.aspx"));
         
        var htmlString = document.getElementById("tblMain").innerHTML;
        //remove Actions table from PDF
        var toBeRemoved = document.getElementById("tblActions");
        if (toBeRemoved != null)
            htmlString = htmlString.replace(toBeRemoved.innerHTML, "");
        //Remove Expand Collapse checkbox
        toBeRemoved = document.getElementById("tdExpandCollapse");
        
        if (toBeRemoved != null)
            htmlString = htmlString.replace(toBeRemoved.parentNode.innerHTML, "");
        //replace all doube " with single '        
        htmlString = htmlString.replace(new RegExp("\"","g"), "'");
        loc = "http://localhost" + loc.replace(loc.substring(0, loc.indexOf("/", 8)), ""); 
         if("<%=Session["ImageURL"]%>" == "")
             {
               htmlString = htmlString.replace(new RegExp("image/","g"), loc + "image/");
              htmlString = "<html><link rel='stylesheet' type='text/css' href='" + loc + "mirror/styles/main.css' /><body><center><table><tr><td><img src='" + loc + "mirror/image/lobbytop1600.jpg' width='100%' height='72' alt='LobbyTop'></td></tr></table>" + htmlString + "</center></body></html>"; //ZD 100419
             }
        else
           {
             var url = "<%=Session["ImageURL"]%>";
             htmlString = htmlString.replace(new RegExp("image/","g"), url + "/en/image/");
             htmlString = "<html><link rel='stylesheet' type='text/css' href='" + url + "/en/mirror/styles/main.css' /><body><center><table><tr><td><img src='" + url + "/en/mirror/image/lobbytop1600.jpg' width='100%' height='72' alt='LobbyTop'></td></tr></table>" + htmlString + "</center></body></html>";//ZD 100419
           }
        if (document.getElementById("tempText") != null)
        {
            document.getElementById("tempText").value = "";
            document.getElementById("tempText").value = htmlString;
        }
         return true;
    }
    
    function saveOrder(objid, id, epty, epid) 
{
    if (id < 10)
        id = "0" + id;
	
	if (epty=="")
    {
        document.getElementById("<%=txtTempImage.ClientID %>").value=document.getElementById("<%=txtSelectedImage.ClientID %>").value;
        document.getElementById("<%=txtSelectedImage.ClientID %>").value = id;
        document.getElementById("hdnLayout").value="5";
    }
    else
        if (epty != "1")
        {
	        document.getElementById("<%=txtTempImage.ClientID %>").value=document.getElementById("<%=txtSelectedImageEP.ClientID %>").value;
            document.getElementById("<%=txtSelectedImageEP.ClientID %>").value = id;
	        document.getElementById("<%=txtEndpointType.ClientID %>").value=epty + "," + epid;
        document.getElementById("hdnLayout").value="6";
        }
        
     var btn = document.getElementById("btnLyout"); 
  
  if(btn)
    btn.click();
 
       
	document.getElementById(objid).src = document.getElementById("<%=ImagesPath.ClientID %>").value + document.getElementById("<%=txtSelectedImage.ClientID %>").value + ".gif";
	cancelthis();
} 


function cancelthis()
{
	document.getElementsByTagName("body")[0].removeChild(document.getElementById("prompt"));
}

function CheckEndpoint()
{
    if (document.getElementById("<%=imgVideoLayout.ClientID %>") != null)
        document.getElementById("<%=imgVideoLayout.ClientID %>").src = document.getElementById("<%=ImagesPath.ClientID %>").value + document.getElementById("<%=txtSelectedImage.ClientID %>").value + ".gif";    
}

function fnValidateEndTime()
{
		if ( (document.getElementById("txtExtendedTime").value == "") ) {
				document.getElementById("LblExtendedTimeMsg").style.visibility = "visible";
				document.getElementById("LblExtendedTimeMsg").innerText = "Required";
				return (false);		
		}
		return true;
}


//Code added for P2P
function fnchkValue()
{
        var args = fnchkValue.arguments;
        var txtt
        if(args)
        {
            if(args[0])      
              txtt = document.getElementById(args[0]);
            
            if(txtt)
            {
                if(txtt.value == "")
                {
                    alert(DashMsg)
                    return false;
                }
            }
         }
            
        return true;
}

function fnOpenEpt()
{
    var args = fnOpenEpt.arguments;
    
    if(args[0])
    {
        window.open("http://"+args[0], "EndPointDetails", "width=900,height=800,resizable=yes,scrollbars=yes,status=no");
        return false;
    }
}

function fnOpenRemote()
{
    var args = fnOpenRemote.arguments;
    
    if(args[0])
    {
        window.open("http://"+ args[0] + "/a_tvmon.htm", "Monitor", "width=650,height=350,resizable=yes,scrollbars=yes,status=no");
        return false;
    }
}

function fnOpenMsg()
{

    var args = fnOpenMsg.arguments;
    var rw;
    if(args[0])
      rw = document.getElementById(args[0]);
    
    if(rw)
    {      
        if(args[1] == "1")
         rw.style.display = "block";
        else if(args[1] == "0")
             rw.style.display = "none"; 
    }
}


function fnextendtime()
{
    var args = fnextendtime.arguments;
    var tblext = document.getElementById("tblExtendTime");
    var drp = document.getElementById("DrpDwnListView");
    var lnkButton = document.getElementById("LinkButton3");
    var confGrid = document.getElementById("hdngridCount");//Edited For FF...
   //Edited For FF...
      if(drp.value == 2)
      {
           
            if(confGrid.value > 0)
            {
            
            if(tblext)
            {
                if(args[0] == "1")
                   tblext.style.display = 'block';
                else
                   tblext.style.display = 'none';
            }
            }
        }
    
    return false;
}


function saveToOutlookCalendar (iscustomrecur, isrecr, f, instInfo) 
  {
	// outlook do not support custom recur. Maybe LN can, but current can not test.
	var confid = document.getElementById("<%=lblConfID.ClientID%>").value;
	instanceInfo = "";
	//instInfo = document.getElementById("Recur").value;
	//alert(f + " : " + instInfo);
	if (parseInt(iscustomrecur)) {
		getCustomRecur(confid);
	} else {
		//if (parseInt(isrecr))
		//	getRecur(confid);
		//else
			setConfInCalendar(f, confid);
	}
  }
  
  function getCustomRecur(cid)
{
	if (cid != "") {
		if (ifrmPreloading != null)
			ifrmPreloading.window.location.href = "dispatcher/conferencedispatcher.asp?cmd=GetInstances&mode=21&frm=preload&cid=" + cid;
	}
}
function getRecur(cid)
{
//alert("in getrecur" + cid);
	if (cid != "") {
		if (ifrmPreloading != null)
			ifrmPreloading.window.location.href = "dispatcher/conferencedispatcher.asp?cmd=GetInstances&mode=22&frm=preload&cid=" + cid;
	}
}
function setConfInCalendar(f, instInfo)
{

    var confid = document.getElementById("<%=lblConfID.ClientID%>").value;
    if (confid == "")
        confid = document.getElementById("<%=lblConfUniqueID.ClientID%>").innerHTML;
	//url = "saveconfincal.asp?f=" + f + "&ii=" + confid;
	//alert(f);
	//Code Changed for FB 1410 - Start
//	url = "SetSessionOutXml.aspx?tp=saveconfincal.asp&ii=" + instInfo + "&f=" + f;
	url = "GetSessionOutXml.aspx?ii=" + instInfo + "&f=" + f;
	//Code Changed for FB 1410 - End
//	alert(url);
	if (!window.winrtc) {	// has not yet been defined
		winrtc = window.open(url, "", "width=600,height=350,top=0,left=0,resizable=no,scrollbars=no,status=no");
		winrtc.focus();
	} else {	// has been defined
	    if (!winrtc.closed) {     // still open
	    	winrtc.close();
	        winrtc = window.open(url, "", "width=600,height=350,top=0,left=0,resizable=no,scrollbars=no,status=no");
			winrtc.focus();
		} else {
	        winrtc = window.open(url, "", "width=600,height=350,top=0,left=0,resizable=no,scrollbars=no,status=no");
	        winrtc.focus();
		}
	}
}

function opnmanage()
{

var confid = document.getElementById("<%=lblConfID.ClientID%>").value;
		   
if(confid == "")
{
    return false;
    
}

url = "ManageConference.aspx?confid="+confid+"&t=";
window.location.href = url;

}

function ViewBridgeDetails(bid)
{
    url = "BridgeDetailsViewOnly.aspx?hf=1&bid=" + bid;
    window.open(url, "BrdigeDetails", "width=900,height=800,resizable=yes,scrollbars=yes,status=no");
    return false;
}

function viewEndpoint(endpointId)
        {
	        url = "dispatcher/admindispatcher.asp?eid=" + endpointId + "&cmd=GetEndpoint&ed=1&wintype=pop";

	        if (!window.winrtc) {	// has not yet been defined
		        winrtc = window.open(url, "", "width=450,height=310,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
		        winrtc.focus();
	        } else { // has been defined
	            if (!winrtc.closed) {     // still open
	    	        winrtc.close();
	                winrtc = window.open(url, "", "width=450,height=310,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
			        winrtc.focus();
		        } else {
	                winrtc = window.open(url, "", "width=450,height=310,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
	                winrtc.focus();
		        }
	        }
        }
        
      if(document.getElementById("hdnlisttype").value == 2)
       {
            
            var tim = '30000';
            setTimeout("DataLoading(1);__doPostBack('btnRefreshRooms', '')", tim);            
       } 
       
       function DataLoading(val)
    {
        if (val=="1")
            document.getElementById("dataLoadingDIV").style.display = 'block'; //ZD 100678
		//document.getElementById("dataLoadingDIV").innerHTML="<b><font color='#FF0000' size='2'>Refreshing ...</font></b>&nbsp;&nbsp;&nbsp;&nbsp;<img border='0' src='image/wait1.gif'  alt='Loading..'>";// ZD 100419
        else
            document.getElementById("dataLoadingDIV").style.display = 'none'; //ZD 100678
    }
    
 function btnSetupAtMCU_Click()
  {
  var lnkButton6 = document.getElementById('LinkButton6');//
  var confGrid = document.getElementById("hdngridCount");//Edited For FF...
  var drp = document.getElementById("DrpDwnListView");
  if(drp.value == 2)
      {
           
            if(confGrid.value > 0)
            {  
    if (confirm(SetUponbridge))
    {
        return true;
    }
    else
        return false;
        }
        }
  
  }
       
    function CustomEditAlert()
{
    var msg = "Press OK to proceed "
    var act;
    
//    if ("<%=isCustomEdit%>" == "Y" )
//    {
//        act = confirm(msg);
//        if(!act)
            DataLoading(0);
        //return act;    
//    }   
    //ZD 101597
    if(parseInt('<%=Session["admin"]%>') > 0)
        return fnSelectForm();
    else
        return true;
}  

//ZD 100428 START- Close the popup window using the esc key

    document.onkeydown = function (event) {
           if (document.getElementById('viewHostDetails') != null)
                if (document.getElementById('viewHostDetails').style.display != 'none')
                    ClosePopUp();
                else if(document.getElementById('prompt') != null)
                    cancelthis();
                else if (document.getElementById('btnClose') != null)
                    document.getElementById('btnClose').click();
        //ZD 101597
        if (event.keyCode == 27) {
            var obj = document.getElementById('PopupFormList');

            if (obj != null && obj.style.display != 'none')
                obj.style.display = 'none';
        }
    }
    //ZD 100428 END
</script>


</script>
</form>
</body>
</html>

<%--commented for FB1764--%>
<%--<script language="javascript">
function Clickheretoprint()
{ 
  var disp_setting="toolbar=yes,location=no,directories=yes,menubar=yes,"; 
      disp_setting+="scrollbars=yes,width=300, height=250, left=100, top=25"; 
  var content_vlue = document.getElementById("print_content").innerHTML; 
  
  var docprint=window.open("","",disp_setting); 
   docprint.document.open(); 
   docprint.document.write('<html><head><title>Inel Power System</title>'); 
   docprint.document.write('</head><body onLoad="self.print()"><center>');          
   docprint.document.write(content_vlue);          
   docprint.document.write('</center></body></html>'); 
   docprint.document.close(); 
   docprint.focus(); 
}


</script>
--%>

<script type="text/javascript" src="inc/softedge.js"></script>
<% if (Request.QueryString["hf"] == null)
   { %>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" --><% }
   else
   {
       if (!Request.QueryString["hf"].ToString().Equals("1"))
       {    
%>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" --> 
<%}
   } %>