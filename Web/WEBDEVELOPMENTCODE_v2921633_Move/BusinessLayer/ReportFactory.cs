//ZD 100147 Start
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End ZD 100886
using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Data;  //Report Fixes
using System.IO;
using log4net;
using myVRM.DataLayer;
using NHibernate.Criterion;
using System.Globalization; //FB 2410


namespace myVRM.BusinessLayer
{
	/// <summary>
	/// Data Layer Logic for loading/saving Reports(Templates/Schedule)
	/// </summary>
	public class ReportFactory
	{
		private static log4net.ILog m_log;
		private static bool m_bRunningScheduleReport = false;
		private int m_iCurrentPage = 0;
		private int m_iPageSize = 50;

        private myVRM.DataLayer.ReportDAO m_rptDAO;
        private myVRM.DataLayer.userDAO m_usrDAO;
        private IReportScheduleDao m_SchedDao;
        private IReportTemplateDao m_TempDao;
        private IReportInputItemsDao m_inItemsDao;
        IUserDao m_vrmUserDAO;

        //Report fixes start
        private string configpath = "";
        private string starttime = "";
        private string endtime = "";
        private string timezone = "";
        private string roomslist = "";
        private string reportType = "";
        private ns_SqlHelper.SqlHelper m_rptLayer = null;
        private int languageid = 0;

        //Report fixes end

        //FB 1750
        private string codeType = "";
        private string entityCode = "";
        private string tableName = "";
        private string export = "";
        internal string destDirectory = ""; //FB 2363
        private string fileName = "";
        private ArrayList entityList = null;
        
        private orgDAO m_OrgDAO;    //Organization Module Fixes
        private IOrgSettingsDAO m_IOrgSettingsDAO;
        private OrgData orgInfo;
        private const int defaultOrgId = 11;  //Default organization
        private int organizationID = 0;
        private myVRMException myvrmEx; //FB 1881

        //FB 1969 - Start
        private string email = "";
        private string firstname = "";
        private string lastname = "";
        private string fromdate = "";
        private string todate = "";
        private string SortBy = "";
        private string reportUserID = "";
        private ns_SqlHelper.SqlHelper m_usrLayer = null;
        //FB 1969 - End

        //FB 2155 - Starts
        private int loginId = 0;
        private String OrgID = "";
        private int selectType = 0;
        private String usrRoleId = "", usrCrossAccess = "", usrAdmin = "";
        //FB 2155 - End
        //FB 2343 Start
        private IReportMonthlyDaysDAO m_IReportMonthlyDaysDAO;
        private IReportWeeklyDaysDAO m_IReportWeeklyDaysDAO;
        //FB 2343 End
        //FB 2558
        private String dtFormat = "";
        private String tFormat = "";
		//FB 2410 Starts
        private IBatchReportSettingsDAO m_IBatchReportSettingsDAO; //FB 2410
        private IBatchReportDatesDAO m_IBatchReportDatesDAO; //FB 2410
        private emailFactory m_vrmEmailFact; //FB 2410
		//FB 2410 Ends
		/// <summary>
		/// construct report factory with session reference
		/// </summary>
		public ReportFactory(ref vrmDataObject obj)
		{
			try
			{
			    m_rptDAO = new ReportDAO(obj.ConfigPath,obj.log);
                m_usrDAO = new userDAO(obj.ConfigPath, obj.log);

                m_SchedDao   = m_rptDAO.GetReportScheduleDao();
                m_TempDao    = m_rptDAO.GetReportTemplateDao();
                m_inItemsDao = m_rptDAO.GetReportInputItemsDao();
                m_vrmUserDAO = m_usrDAO.GetUserDao();

                m_OrgDAO = new orgDAO(obj.ConfigPath, obj.log); //Organization Module Fixes
                m_IOrgSettingsDAO = m_OrgDAO.GetOrgSettingsDao();

                if (configpath.Trim() == "")//FB 2363
                    configpath = obj.ConfigPath;

                m_rptLayer = new ns_SqlHelper.SqlHelper(configpath); //FB 2047
                //FB 2343 Start
                m_IReportMonthlyDaysDAO = m_rptDAO.GetReportMonthlyDaysDao();
                m_IReportWeeklyDaysDAO = m_rptDAO.GetReportWeeklyDaysDao();
                //FB 2343 End
                m_IBatchReportSettingsDAO = m_rptDAO.GetBatchReportSettingsDao(); //FB 2410
                m_IBatchReportDatesDAO = m_rptDAO.GetBatchReportDatesDao(); //FB 2410
                
                m_log = obj.log;
			}
			catch (Exception e)
			{
				m_log.Error("sytemException",e);
				throw e;
			}
		}
		
		/// <summary>
		/// Get All Report templates
		/// </summary>
		/// <returns>Complete list of report templates</returns>
		public bool GetReportTypeList(ref vrmDataObject obj)
		{
			IList ReportTypes = new ArrayList();
			bool bRet = true;			
			try
			{
                ReportTypes = m_TempDao.GetActive();
//    new ICriterion[]
  //   { Expression.Eq("BlogId", 0) }, new Order[]
    // { Order.Desc("PublishedDate") }, 0, 25);

				if(ReportTypes.Count > 0)
				{
					obj.outXml  = "<ReportTypes>";

					foreach(ReportTemplate rt in ReportTypes)
					{
						obj.outXml += "<Report>";
						obj.outXml += "<ID>"          +	rt.ID.ToString()                   + "</ID>";		
						obj.outXml += "<Title>"       +	rt.templateTitle.ToString()        + "</Title>";		
						obj.outXml += "<Description>" + rt.templateDescription.ToString()  + "</Description>";		
						obj.outXml += "</Report>";
					}

                    obj.outXml += "</ReportTypes>";
                }
                else
                {
                    //throw new myVRMException("ERROR, could not get report type list");//FB 1881
                    m_log.Error("ERROR, could not get report type list");//FB 1881
                    throw new myVRMException();
                }

			}		
			catch (myVRMException e)
			{
				m_log.Error("vrmException",e);
				return false;
			}
			catch (Exception e)
			{
				m_log.Error("sytemException",e);
                return false;
			}

			return bRet;
		}	

		public bool GetConferenceReportList(ref vrmDataObject obj)
		{ 
			bool bRet = true;	
		
			try
			{
				// Load the string into xml parser and retrieve the tag values.
				XmlDocument xd = new XmlDocument();
				xd.LoadXml(obj.inXml);
							
				XmlNode node;
                string dateFrom = string.Empty;

                node = xd.SelectSingleNode("//login/dateFrom");
                if (node.InnerXml.Trim().Length > 0)
                    dateFrom = node.InnerXml.Trim();

                string dateTo = string.Empty;
                node = xd.SelectSingleNode("//login/dateTo");
				if(node.InnerXml.Trim().Length > 0)
            	    dateTo = node.InnerXml.Trim();

                int userid = 0;
                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out userid);

                vrmUser myVrmUser = m_vrmUserDAO.GetByUserId(userid);
           
				DateTime a_from, a_to;
                if (dateFrom.Length == 0)
                {
                    a_from = new DateTime(0);
                }
                else
                {
                    dateFrom += " 00:00:00 AM";
                    a_from = Convert.ToDateTime(dateFrom);
                    timeZone.changeToGMTTime(myVrmUser.TimeZone, ref  a_from);

                }

                if (dateTo.Length == 0)
                {
                    a_to = new DateTime(0);
                }
                else
                {
                    dateTo += " 23:59:59 PM";
                    a_to = Convert.ToDateTime(dateTo);
                    timeZone.changeToGMTTime(myVrmUser.TimeZone, ref a_to);
                }
               
                int tzID = myVrmUser.TimeZone;
				//Create a new node.
				obj.outXml  = "<reports>";
				obj.outXml += "<dateFrom>" + dateFrom + "</dateFrom>";
				obj.outXml += "<dateTo>" + dateTo +  "</dateTo>";
							  
                IList Schedule = m_SchedDao.GetSubmitBetween(a_from, a_to);
    
	           	foreach(ReportSchedule RS in Schedule)
				{
                    vrmUser owner = m_vrmUserDAO.GetByUserId(RS.userId);
	    			obj.outXml += "<report>";					
						
					DateTime submitDate   = RS.SubmitDate;                                                                                                                                                                                                                                                                    
					DateTime completeDate = RS.CompleteDate;

                    timeZone.userPreferedTime(tzID, ref submitDate);
                    timeZone.userPreferedTime(tzID, ref completeDate);

					obj.outXml += "<ID>"           + RS.ID.ToString()           + "</ID>";		
					obj.outXml += "<name>"         + RS.reportTitle.ToString()  + "</name>";		
					obj.outXml += "<submitDate>"   + submitDate.ToString()   + "</submitDate>";		
					obj.outXml += "<completeDate>" + completeDate.ToString() + "</completeDate>";		
					obj.outXml += "<description>"  + RS.reportDescription.ToString() +
			    			"</description>";							
					obj.outXml += "<status>"       + RS.status.ToString()   + "</status>" ;		
						
					obj.outXml += "<creator>"      +
                            owner.LastName + ", " + owner.FirstName + "</creator>";
					obj.outXml += "<public>1</public>";
					obj.outXml += "</report>";					
					

				}
                obj.outXml += "</reports>";
				
	
			} 
			catch (myVRMException e)
			{
				m_log.Error("vrmException",e);
				return false;
			}
			catch (Exception e)
			{
				m_log.Error("sytemException",e);
                return false;
			}

			return bRet;
		}
	
		public bool runReport()
		{
			
			DateTime reportDate = System.DateTime.Now;
			DateTime nullDate   = new System.DateTime(0);
			List<ReportSchedule>reportList;
			ArrayList results    = new ArrayList();
			ArrayList columnList = new ArrayList();			
			string outXml;

			m_bRunningScheduleReport = true;
			
			try
			{
                timeZone.changeToGMTTime(11, ref reportDate);
                DateTime a_from = Convert.ToDateTime(reportDate);

                reportList = m_SchedDao.GetSubmitBetween(a_from, nullDate);
    

				if (reportList.Count == 0 )
				{
					m_log.Debug("No reports...");
				}
				else
				{
					
					foreach( ReportSchedule RS in reportList)
					{			
						outXml = string.Empty;
						vrmDataObject obj = new vrmDataObject();
						obj.inXml = RS.inXml;

						if ( GenerateReport(ref obj))
						{	
							RS.status = 1;
							RS.CompleteDate = reportDate;
							RS.report = obj.outXml;
							
					
						}
						else
						{
							m_log.Error("Data error SCHEDULED:(GenerateReport)");
							return (false);
						}
                        m_SchedDao.SaveOrUpdateList(reportList);
            		}
				}
	
			}
			catch (myVRMException e)
			{
				m_log.Error("vrmException",e);
                return false;
			}
			catch (Exception e)
			{
				m_log.Error("sytemException",e);
                return false;
			}

			m_bRunningScheduleReport = false;
			return true;
		}

		public bool GenerateReport(ref vrmDataObject obj)
		{ 
			string filterString = "";
			string execQuery    = "";
			string rcQuery      = "";
	
			ArrayList columnList = new ArrayList();
			XmlNode node;
            IList results;
            List<ReportSchedule> schedData = new List<ReportSchedule>() ;
		
			ReportTemplate template = new ReportTemplate();
			 
			bool bScheduleReport = false;
			try

			{
			
				XmlDocument xd = new XmlDocument();
				xd.LoadXml(obj.inXml);

                int tID = 0;
				node = xd.SelectSingleNode("//generateReport/tID");
                if (tID != null)
                    int.TryParse(node.InnerXml.Trim(), out tID);

				node = xd.SelectSingleNode("//generateReport/userID");
				string userID = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//generateReport/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;
                int.TryParse(orgid, out organizationID);  //Organization Module Fixes

                string pageNo = "";
				node = xd.SelectSingleNode("//generateReport/pageNo");
                if (node != null)
                    pageNo = node.InnerXml.Trim();
				if(pageNo.Length == 0)
					m_iCurrentPage = 1;
				else
					m_iCurrentPage = int.Parse(pageNo.ToString());;

				node = xd.SelectSingleNode("//generateReport/ScheduleReport/dateFrom");
				string SchedDateFrom = node.InnerXml.Trim();
				
				node = xd.SelectSingleNode("//generateReport/ScheduleReport/dateTo");
				string SchedDateTo = node.InnerXml.Trim();
				
				node = xd.SelectSingleNode("//generateReport/ScheduleReport/name");
				string SchedName = node.InnerXml.Trim();

				node = xd.SelectSingleNode("//generateReport/ScheduleReport/description");
				string SchedDescription = node.InnerXml.Trim();
	
				node = xd.SelectSingleNode("//generateReport/ScheduleReport/recurrance");
				string SchedRecurrance = node.InnerXml.Trim();
	
				node = xd.SelectSingleNode("//generateReport/ScheduleReport/emailNotify");
				string SchedEmailNotify = node.InnerXml.Trim();
		
				node = xd.SelectSingleNode("//generateReport/ScheduleReport/emailAddress");
				string SchedEmailAddress = node.InnerXml.Trim();

                template = m_TempDao.GetById(tID);
				if ( template.ID > 0 )
				{
					
					ArrayList tokenList = new ArrayList();
					GetColumnList(ref columnList,template.columnHeadings);
					if (!extractInputToken(ref tokenList,obj.inXml))
					{
						m_log.Error("Data read error (extractInputToken)");
						return (false);
					}

                    ReportInputItem item = 
					    m_inItemsDao.GetById(template.InputID);

					filterString += item.inputClause;
			
					if (!replaceToken(tokenList, ref filterString))
					{
						m_log.Error("Data read error (replaceToken)");
						return (false);
					}
                    
                    vrmUser myVrmUser =
                        m_vrmUserDAO.GetByUserId(int.Parse(userID));

                    if (!GetVRMQueryFilter(myVrmUser, ref filterString, false))
					{
						throw new myVRMException(309);
					}
		
					filterString += " " + template.filter;
		
					execQuery = template.query + " " + template.tables + " " +
										filterString + " " + template.totalquery +
										" " + template.orderby ;
				
					rcQuery  = "Select count(*) " + template.tables + " " +
										filterString + " " +  template.totalquery;
					
					if(SchedDateFrom.Length > 0 && !m_bRunningScheduleReport)
						bScheduleReport = true;
					else
						bScheduleReport = false;

					if(bScheduleReport)
					{
						ReportSchedule SchedRecord = new ReportSchedule();
				
						string [] split = SchedDateFrom.Split(new Char [] {',', ';'});

						foreach (string sDate in split) 
						{
							System.DateTime reportDate = System.DateTime.Parse(sDate);
                            timeZone.changeToGMTTime(myVrmUser.TimeZone, ref  reportDate);

                            SchedRecord.userId      = template.userId;
							SchedRecord.SubmitDate  = reportDate;
							SchedRecord.reportTitle = SchedName;
							SchedRecord.timeZone    = 26;
							SchedRecord.inXml       = obj.inXml;
							SchedRecord.status      = 0;
							SchedRecord.notifyEmail = SchedEmailAddress;
                            SchedRecord.tID = tID;
							SchedRecord.deleted     = 0;

							SchedRecord.SubmitDate    = reportDate;
							SchedRecord.CompleteDate  = System.DateTime.Parse("01/01/1970");

							SchedRecord.reportDescription 	= SchedDescription;

							schedData.Add(SchedRecord);
							
						}
                        int iSuccess = m_SchedDao.SaveOrUpdateList(schedData).Count;
                        if (iSuccess > 0)
						{
							obj.outXml = "<success>1</success>";
							return (true);
						}
						else
						{
							m_log.Error("Unable add schedule record(s)");
							obj.outXml =  "<error>";
							obj.outXml += "<errorCode>100</errorCode>";
							obj.outXml += "<message> Unable add schedule record(s)\n  </message>";
							obj.outXml += "<level>S</level>";
							obj.outXml += "</error>";
							return (false);
						}						
					}
					else
					{
						long rowCount = 0;

                        results = m_SchedDao.execQuery(rcQuery); 
                        	
			            foreach(long iObject in results)
				        {
                            rowCount += int.Parse(iObject.ToString());
				        }

                        if(m_iCurrentPage > 0)
                            m_SchedDao.pageNo(m_iCurrentPage);
                        if (m_iPageSize > 0)
                            m_SchedDao.pageSize(m_iPageSize);

						results = m_SchedDao.execQuery(execQuery);
                        if(results.Count > 0 )
						{	
							string outXml = string.Empty;
							if(CreateReport(ref results,ref columnList, ref outXml, rowCount))
							{
								obj.outXml = outXml;
								return true;
							}
							else
							{
								m_log.Error("Unable to create report");
								return (false);
							}				
						}
						else
						{

                            obj.outXml = "<ConfReport>";
                            
                            obj.outXml += "<Paging>";
                            obj.outXml += "<pageEnable>0</pageEnable>";
                            obj.outXml += "<canAll>0</canAll>";
                            obj.outXml += "<pageNo>0</pageNo>";
                            obj.outXml += "<totalPages>0</totalPages>";
                            obj.outXml += "<totalNumber>0</totalNumber>";
                            obj.outXml += "</Paging>";

                            obj.outXml += "</ConfReport>";
                            return (true);
						}	
					}
				}
				else
				{
					m_log.Error("Data read error (Generate Report)");
					return (false);
				}	
		
			} 
			catch (myVRMException e)
			{
				m_log.Error("vrmException: " + e.Message);
				//obj.outXml = e.FetchErrorMsg();//FB 1881
                obj.outXml = ""; //FB 1881
                return false;
			}
			catch (Exception e)
			{
				m_log.Error("sytemException",e);
                return false;
			}
		}

		public bool GetInputParameters(ref vrmDataObject obj)
		{ 
			try
			{
				IList itemList = new ArrayList();
				// Load the string into xml parser and retrieve the tag values.
				XmlDocument xd = new XmlDocument();
				xd.LoadXml(obj.inXml);
							
				XmlNode node;

				node = xd.SelectSingleNode("//login/userID");
				string userID = node.InnerXml.Trim();

                int tID = 0;
				node = xd.SelectSingleNode("//login/tID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out tID);

				obj.outXml  = "<ReportParams>";
				obj.outXml += "<Report>";

				ReportTemplate template = new ReportTemplate();
                template = m_TempDao.GetById(tID);

				if (template.ID == 0 )
				{
					m_log.Error("Data read error (GetReportByID)");
					return (false);
				}		
				obj.outXml += "<ID>"          + template.ID          + "</ID>";
				obj.outXml += "<Title>"       + template.templateTitle  + "</Title>";
				obj.outXml += "<Description>" + template.templateDescription + "</Description>";
				obj.outXml += "</Report>";

				obj.outXml += "<Params>";
		
				int Tid = template.InputID;
                ReportInputItem IT = m_inItemsDao.GetById(Tid);

			
				if(IT.inputID == Tid)
				{
					ArrayList tokenList = new ArrayList();
					extractInputTokenList(IT.inputClause,ref tokenList);

					for(int j = 0; j < tokenList.Count; j ++)
					{
						obj.outXml += "<Param>";					
						obj.outXml += "<Value>" + tokenList[j].ToString() + "</Value>";		
						obj.outXml += "</Param>";
					}
					obj.outXml += "<Param>";					
					obj.outXml += "<Value>selPublic</Value>";		
					obj.outXml += "</Param>";
				}
                else
                {
                    m_log.Error("Data read error (GetInputItems)");
                    return (false);
                }		

				obj.outXml += "</Params>";
				obj.outXml += "</ReportParams>";
	
			} 
			catch (myVRMException e)
			{
				m_log.Error("vrmException",e);
                return false;
			}
			catch (Exception e)
			{
				m_log.Error("sytemException",e);
                return false;
			}
			return true;
		}
		private bool extractInputToken(ref ArrayList tokenList, string inXml)
		{
			try 
			{
				// Load the string into xml parser and retrieve the tag values.
				XmlDocument xd = new XmlDocument();
				xd.LoadXml(inXml);
				XmlNode node;
				
				XmlElement root = xd.DocumentElement;
				XmlNodeList elemList = root.GetElementsByTagName("Param");

				for (int i=0; i < elemList.Count; i++)
				{   

					TokenObject token = new TokenObject();

					node = elemList[i].SelectSingleNode("ID");
					token.tokenName = node.InnerXml;
					
					node = elemList[i].SelectSingleNode("Value");
					token.tokenValue = node.InnerXml;

					tokenList.Add(token);
				}
				
			}
			catch (myVRMException e)
			{
				m_log.Error("vrmException",e);
                return false;
			}
			catch (Exception e)
			{
				m_log.Error("sytemException",e);
                return false;
			}

			return true;
		}
		private bool replaceToken(ArrayList tokenList, ref string filterString)
		{
			try 
			{
				string timeZone  = "26";
				TokenObject token;
				for (int i=0; i < tokenList.Count; i++)
				{   
					token = (TokenObject)tokenList[i];
					token.tokenValue.Trim();
	
					string temp = "";
					if(token.tokenName.Trim() == "selPublic")
					{
						if( token.tokenValue != "2" && token.tokenValue.Length > 0)
						{
							temp = filterString + " AND C.isPublic = " + token.tokenValue + " ";
						}
					}
					else
					{
						string findToken = "<<" + token.tokenName.Trim() + ">>";
						if( filterString.IndexOf(findToken) >= 0)
						{
							if(token.tokenValue.Length < 1)
								return false;
							else
								temp = filterString.Replace(findToken, token.tokenValue);					
						}
					}
					if(temp.Length > 0 )
					{
						temp = temp.Replace("@tz", timeZone);					
						filterString = temp;
					}
					
				}
				
			}
			catch (myVRMException e)
			{
				m_log.Error("vrmException",e);
                return false;
			}
			catch (Exception e)
			{
				m_log.Error("sytemException",e);
                return false;
			}

			return true;
		}

		private bool extractInputTokenList(string inputItem, ref ArrayList tokenList)
		{
			int x = -1;
			try
			{
				while((x = inputItem.IndexOf("<<") ) >= 0)
				{
					x += 2;
					int j = inputItem.IndexOf(">>");
					string token = inputItem.Substring(x, j - x);
					tokenList.Add(token);
					j += 2;
					if(j >= inputItem.Length)
						break;
					string temp = inputItem.Substring(j, inputItem.Length - j);
					inputItem = temp;
				}
			}
			catch (myVRMException e)
			{
				m_log.Error("vrmException",e);
                return false;
			}
			catch (Exception e)
			{
				m_log.Error("sytemException",e);
                return false;
			}
			return true;
		}
				
		private bool CreateReport(ref IList results,ref ArrayList columnList, ref string outXml, long rowCount)
		{
			try
			{
				 outXml = "<ConfReport>";
	
				int iStart = 0;
				int iFinish = m_iPageSize;

				if(iFinish < results.Count)
				{
					iStart = (m_iCurrentPage - 1) * m_iPageSize; 
					if(iStart < 0)
						iStart = 0;

					iFinish = m_iCurrentPage * m_iPageSize;
				
				}

				if(iFinish > results.Count)
					iFinish = results.Count;

				int numPages = (int)( rowCount / m_iPageSize);

				if( (numPages * m_iPageSize) != rowCount )
					numPages ++;

				outXml += "<Paging>";
				if(numPages > 1)
					outXml += "<pageEnable>1</pageEnable>";
				else
					outXml += "<pageEnable>0</pageEnable>";

				outXml += "<canAll>0</canAll>";
				outXml += "<pageNo>"      + m_iCurrentPage.ToString() + "</pageNo>";
				outXml += "<totalPages>"  + numPages.ToString()       + "</totalPages>";
				outXml += "<totalNumber>" + rowCount.ToString()       + "</totalNumber>";
				outXml += "</Paging>";


				outXml += "<Header>";
				int idx;
				int iRowNumIdx = -1;

				for(int j=0; j< columnList.Count; j ++)
				{
					idx = j;
					string displayValue = columnList[j].ToString();
					if(displayValue == "RowNum")
					{
						iRowNumIdx = j;
					}
					else
					{
						idx ++;
						outXml += "<Title>";
						outXml += "<ID>" + idx.ToString() +"</ID>";
						outXml += "<DisplayValue>" + displayValue + "</DisplayValue>";							
						outXml += "</Title>";
					}
				}
				outXml += "</Header>";
				outXml += "<Rows>";

				foreach(object[] arrobject in results)
				{	
					outXml += "<Row>";
					for(int j=0; j < columnList.Count; j++)
					{
						string outString = arrobject[j].ToString();
						scrubData(columnList[j].ToString(), ref outString);
						outXml += "<Column>";
						outXml += "<Value>"  + outString + "</Value>";
						outXml += "</Column>";							
					}
					outXml += "</Row>";
				}
				outXml += "</Rows>";
				outXml += "</ConfReport>";
			
			}
			catch (myVRMException e)
			{
				m_log.Error("vrmException",e);
                return false;
			}
			catch (Exception e)
			{
				m_log.Error("sytemException",e);
                return false;
			}
			return true;
		}

		public bool DeleteConferenceReport(ref vrmDataObject obj)
		{ 
			try
			{
				// Load the string into xml parser and retrieve the tag values.
				XmlDocument xd = new XmlDocument();
				xd.LoadXml(obj.inXml);
							
				XmlNode node;

				node = xd.SelectSingleNode("//login/userID");
				string userID = node.InnerXml.Trim();

				node = xd.SelectSingleNode("//login/delReport/reportID");
				string sID = node.InnerXml.Trim();

                int tID = int.Parse(sID);
                ReportSchedule rpt = m_SchedDao.GetById(tID);
                if(rpt.ID == tID)
                {
                    rpt.deleted = 1;
                    m_SchedDao.SaveOrUpdate(rpt);
                 	obj.outXml = "<success>1</success>";
					return true;
				}
				else
				{
                    //FB 1881 start
					//obj.outXml = "<error><level>E</level><message>Cannot Delete Report</message></error>";
                    myvrmEx = new myVRMException(473);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    //FB 1881 end
					return false;
				}
			}
			catch (myVRMException e)
			{
				m_log.Error("vrmException",e);
                return false;
			}
			catch (Exception e)
			{
				m_log.Error("sytemException",e);
                return false;
			}
		}

		public bool GetOldScheduledReport(ref vrmDataObject obj)
		{ 
			string rID = string.Empty;
			try
			{
				// Load the string into xml parser and retrieve the tag values.
				XmlDocument xd = new XmlDocument();
				xd.LoadXml(obj.inXml);
							
				XmlNode node;
				
				
				node = xd.SelectSingleNode("//getOldScheduledReport/rID");
				rID = node.InnerXml.Trim();
                int tID = int.Parse(rID);

				node = xd.SelectSingleNode("//getOldScheduledReport/pageNo");
				string pageNo = node.InnerXml.Trim();

				node = xd.SelectSingleNode("//getOldScheduledReport/userID");
				string userid = node.InnerXml.Trim();

                ReportSchedule sched = m_SchedDao.GetById(tID);
	
				if (sched.ID != tID)
				{
					m_log.Error("Data read error (GetConferenceReportList)");
					//FB 1881 start
                    //obj.outXml = "<error><level>E</level><message>Report does not exist!</message></error>";
                    myvrmEx = new myVRMException(474);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    //FB 1881 end
					return (false);
				}
				else
					obj.outXml = sched.report; 

			}
			catch (myVRMException e)
			{
				m_log.Error("vrmException",e);
                return false;
			}
			catch (Exception e)
			{
				m_log.Error("sytemException",e);
                return false;
			}
			return true;
		}
		
		private bool GetColumnList(ref ArrayList columnList, string template)
		{
			int x, j;
			try
			{
				x = 0;
				while((j = template.IndexOf(",") ) >= 0)
				{
					string column = template.Substring(x, j);
					columnList.Add(column);
					x = j + 1;
					if(x >= template.Length)
						break;
					string temp = template.Substring(x, template.Length - x);
					template = temp;
					x = 0;
				}
				if(template.Length > 0 )
					columnList.Add(template);
				
			}
			catch (Exception e)
			{
				m_log.Error("GetColumnList",e);
                return false;
			}
			return true;
		}
        private bool GetVRMQueryFilter(vrmUser user, ref string query, bool bPublic)
        {
            try
            {
                int a_bMultiDepartments = 0;
                string addRef = " AND (";

                if (bPublic)
                    addRef += " C.isPublic = 1 OR ";

                if (user.userid == 0)
                    return false;

                if (organizationID < 11)    //Organization Module
                    organizationID = defaultOrgId;

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                a_bMultiDepartments = 1;
                if(orgInfo != null)
                    a_bMultiDepartments = orgInfo.MultipleDepartments;  //Organization Module

                if (!user.isSuperAdmin() && !user.isAdmin())
                {
                    // if this is an admin, if multi depts are set then
                    // apply dept level security otherwise default to 
                    // stand alone

                    if (a_bMultiDepartments != 0)
                    {
                        query += addRef + " C.userid IN(";
                        query += " SELECT DA.approverid FROM myVRM.DataLayer.vrmDeptApprover DA";
                        query += " WHERE DA.approverid = '" + user.userid.ToString() + "'))";
                    }
                    else
                    {
                        // this is an ordinary user (lowest privilages)
                        query += addRef + " UserID = '" + user.userid.ToString() + "')";
                    }

                }
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("GetQueryFilter", e);
                return false;
            }
	    }	
		private bool scrubData(string column, ref string field)
		{
			try
			{
				string test = column.Trim().ToLower();
				if(test.CompareTo("actual minutes") == 0 )
				{
                    int ifield = int.Parse(field) / 2;
					field = ifield.ToString();
				}
			}
			catch (Exception e)
			{
				m_log.Error("GetColumnList",e);
                return false;
			}
			return true;
		}
		
		/// <summary>
		/// Make sure we clean up session etc.
		/// </summary>
		public void Dispose()
		{
		}

        //Report Fixes Start

        #region ExtractInXML

        private void ExtractInXML(string rptInXML)
        {
            XmlDocument xd = new XmlDocument();
            xd.LoadXml(rptInXML);

            XmlNode node;
            node = xd.SelectSingleNode("//report/configpath");
            if (node != null)
            {
                if (node.InnerXml.Trim().Length > 0)
                    configpath = node.InnerXml.Trim();
            }

            DateTime SDate = DateTime.Now;
            node = xd.SelectSingleNode("//report/starttime");
            if (node != null)
            {
                if (node.InnerXml.Trim().Length > 0)
                    starttime = node.InnerXml.Trim();
            }

            node = xd.SelectSingleNode("//report/endtime");
            if (node != null)
            {
                if (node.InnerXml.Trim().Length > 0)
                    endtime = node.InnerXml.Trim();
            }

            if (starttime == "" || endtime == "")
            {
                starttime = SDate.ToShortDateString() + " 00:00:00 AM";
                endtime = SDate.ToShortDateString() + " 23:59:59 PM";
            }

            node = xd.SelectSingleNode("//report/timezone");
            if (node != null)
            {
                if (node.InnerXml.Trim().Length > 0)
                    timezone = node.InnerXml.Trim();
            }
            if (timezone == "")
                timezone = "26";

            node = xd.SelectSingleNode("//report/roomslist");
            if (node != null)
            {
                if (node.InnerXml.Trim().Length > 0)
                    roomslist = node.InnerXml.Trim();
            }

            node = xd.SelectSingleNode("//report/reportType");
            if (node != null)
            {
                if (node.InnerXml.Trim().Length > 0)
                    reportType = node.InnerXml.Trim();
            }

            node = xd.SelectSingleNode("//report/organizationID"); //Organization Module Fixes
            string orgid = "";
            if (node != null)
                orgid = node.InnerXml.Trim();
            organizationID = defaultOrgId;
            int.TryParse(orgid, out organizationID);  //Organization Module Fixes

            //ZD 100288 Starts
            node = xd.SelectSingleNode("//report/languageid"); 
            string langid = "";
            if (node != null)
                langid = node.InnerXml.Trim();
            languageid = 1; //Default language id
            int.TryParse(langid, out languageid);  
            //ZD 100288 Ends

            // FB 1750 - Start
            node = xd.SelectSingleNode("//report/codeType");
            if (node != null)
            {
                if (node.InnerXml.Trim().Length > 0)
                    codeType = node.InnerXml.Trim();
            }

            node = xd.SelectSingleNode("//report/entityCode");
            if (node != null)
            {
                if (node.InnerXml.Trim().Length > 0)
                    entityCode = node.InnerXml.Trim();
            }

            node = xd.SelectSingleNode("//report/tableName");
            if (node != null)
            {
                if (node.InnerXml.Trim().Length > 0)
                    tableName = node.InnerXml.Trim();
            }

            node = xd.SelectSingleNode("//report/export");
            if (node != null)
            {
                if (node.InnerXml.Trim().Length > 0)
                    export = node.InnerXml.Trim();
            }

            node = xd.SelectSingleNode("//report/Destination");
            if (node != null)
            {
                if (node.InnerXml.Trim().Length > 0)
                    destDirectory = node.InnerXml.Trim();
            }

            node = xd.SelectSingleNode("//report/fileName");
            if (node != null)
            {
                if (node.InnerXml.Trim().Length > 0)
                    fileName = node.InnerXml.Trim();
            }
            

            node = xd.SelectSingleNode("//report/entityList");
            if (node != null)
            {
                if (node.InnerXml.Trim().Length > 0)
                {
                    String[] strELArr = node.InnerXml.Split(',');
                    entityList = new ArrayList();
                    foreach (String sl in strELArr)
                        entityList.Add(sl);
                }
            }
                        
            // FB 1750 - End
            //FB 2588
            node = xd.SelectSingleNode("//report/DateFormat");
            if (node != null)
            {
                if (node.InnerXml.Trim().Length > 0)
                    dtFormat = node.InnerXml.Trim();
            }

            node = xd.SelectSingleNode("//report/TimeFormat");
            if (node != null)
            {
                if (node.InnerXml.Trim().Length > 0)
                    tFormat = node.InnerXml.Trim();
            }
            
        }
        #endregion

        #region ConferenceReports
        /// <summary>
        /// CalendarReport
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        //<report> - Inxml for all sql reports
        //    <configpath></configpath>
        //    <starttime></starttime>
        //    <endtime></endtime>
        //    <timezone></timezone>
        //    <roomslist></roomslist>
        //</report>
        public bool ConferenceReports(ref vrmDataObject obj)
        {
            bool bRet = true;
            string stmt = "";
            MemoryStream ms = null;
            StreamReader sr = null;
            try
            {
                if (obj.inXml == "")
                {
                    obj.outXml = "<error>Invalid inXml.</error>";
                    return false;
                }

                ExtractInXML(obj.inXml);

                if (configpath == "")
                {
                    obj.outXml = "<error>Invalid config path.</error>";
                    return false;
                }

                switch (reportType)
                {
                    case "CAL":
                        //stmt = CalendarReport();                        
                        break;
                    case "CL":
                        //stmt = ContactList();
                        break;
                    case "PRI":
                        //stmt = PRIAllocation();
                        break;
                    case "RAL":
                       // stmt = ResourceAllocationReport();
                        break;
                    case "DS":
                       //stmt = DailyScheduleReport();
                        break;
                    case "AU":
                       // stmt = AggregateUsageReport();
                        break;
                    case "UR":
                       // stmt = UsageByRoom();
                        break;
                    // FB 1750
                    case "CD":
                        stmt = SelectCalendarDetails();
                        break;
                    case "EC":
                        stmt = GetEntityCodes();
                        break;
                    case "CR":
                        stmt = CreateRoomStructure(obj.inXml);
                        break;
                    case "GU": //FB 2363
                        stmt = GetUsers();
                        break;
                    case "GE": //FB 2363
                        stmt = GetErrorEvents();
                        break;
                }

                if (m_rptDAO != null)
                    m_rptLayer = new ns_SqlHelper.SqlHelper(configpath);

                DataSet ds = null;
                String strXml = "";

                if (stmt != "")
                    ds = m_rptLayer.ExecuteDataSet(stmt);

                if (ds != null)
                {
                    if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        if ((reportType == "CD" || reportType == "GU" || reportType == "GE") && export == "1")//FB 2363
                        {
                            myVRM.ExcelXmlWriter.ExcelUtil exlUtil = new myVRM.ExcelXmlWriter.ExcelUtil();
                            DirectoryInfo dInfo = new DirectoryInfo(destDirectory);

                            if (!dInfo.Exists)
                            {
                                dInfo.Create();
                            }

                            destDirectory = destDirectory + "\\" + fileName;
							//ZD 100288 Starts
                            String strTranText = "";
                            strTranText = "Select * from Launguage_Translation_Text where LanguageID ="+languageid;// +strTranText;

                            DataSet ds1 = null;
                            if (strTranText != "")
                                ds1 = m_rptLayer.ExecuteDataSet(strTranText);
                            Hashtable tranTable = new Hashtable();

                            foreach (DataRow rr in ds1.Tables[0].Rows)
                            {
                                if (!tranTable.ContainsKey(rr["Text"].ToString().ToLower()))
                                    tranTable.Add(rr["Text"].ToString().ToLower(), rr["TranslatedText"]);
                            }
                            
                            if (reportType == "CD")
                                exlUtil.CreateXLFile(entityList, destDirectory, ds,tranTable);
							//ZD 100288 Ends
                            else if (reportType == "GU") //FB 2363
                            {
                                Hashtable loglist = new Hashtable();
                                loglist.Add("UserReport", "User Report");
                                ds.Tables[0].TableName = "UserReport";
                                exlUtil.CreateXLFile(loglist, destDirectory, ds);
                            }
                            else if (reportType == "GE") //FB 2363
                            {
                                Hashtable loglist = new Hashtable();
                                loglist.Add("ErrorReport", "Error Report");
                                ds.Tables[0].TableName = "ErrorReport";
                                exlUtil.CreateXLFile(loglist, destDirectory, ds);
                            }
                        }

                        ms = new MemoryStream();
                        ds.WriteXml(ms);// extract string from MemoryStream
                        ms.Position = 0;
                        sr = new StreamReader(ms, System.Text.Encoding.UTF8);
                        strXml = sr.ReadToEnd();
                        sr.Close();
                        ms.Close();
                    }
                    if (strXml == "") //FB 2045
                        strXml = "<report>No details</report>";

                    obj.outXml = strXml;

                    //FB 1750 
                    if (reportType == "CR")
                        DeleteTmpUtilizationTable();
                }
            }
            catch (Exception ex)
            {
                m_log.Error("sytemException", ex);
                //obj.outXml = "<error>" + ex.Message + "</error>"; //FB 1881
                obj.outXml = "";//FB 1881
                return false;
            }
            finally
            {
                if (sr != null)
                    sr.Close();
                sr = null;

                if (ms != null)
                    ms.Close();
                ms = null;
            }
            return bRet;
        }
        #endregion

        #region CalendarReport
        /// <summary>
        /// CalendarReport
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public String CalendarReport()
        {
            string stmt = "";
            //Added for FB 1511 START
            string stmtCus = "";
            string stmtCus1 = "";
            //Added for FB 1511 END
            try
            {
                //Calendar Report Query Part
                //Added for FB 1511 START
                stmtCus = "select DisplayTitle from dept_CustomAttr_D where deleted=0 and status=0 and orgId='" + organizationID.ToString() + "'";
                if (m_rptDAO != null)
                    m_rptLayer = new ns_SqlHelper.SqlHelper(configpath);

                DataSet ds = null;
                ds = m_rptLayer.ExecuteDataSet(stmtCus);
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count == 0)
                    {

                        stmt = "SELECT dbo.changeTime(" + timezone + ", c.confdate) AS startdate, ";
                        stmt += "dbo.changeTime(" + timezone + ",dateadd(minute,c.duration, c.confdate)) as enddate, ";
                        stmt += "c.duration, l.name AS Room, c.externalname AS Title,";
                        stmt += "case when CHARINDEX('H',UserIDRefText) > 0 then UD.LastName else U.LastName end as LastName,";//ZD 103878
                        stmt += "case when CHARINDEX('H',UserIDRefText) > 0 then UD.firstname else U.firstname end as firstname, ";
                        stmt += "u.Email, l3.name AS State, l2.name AS City, c.confnumname AS ConfNum, "; //ZD 100288
                        stmt += "case c.conftype when 1 then 'Video' when 2 then 'Audio/Video' when 3 then 'Immediate' "; //ZD 100528
                        stmt += "when 4 then 'Point to Point' when 5 then 'Template' when 6 then 'AudioOnly' ";
                        stmt += "when 7 then 'RoomOnly' when 8 then 'Hotdesking' when 11 then 'Phantom' end AS meetType, ";//FB 2694
                        stmt += "case cu.connectionType when 0 then 'Outbound' else 'Inbound' end AS connType, ";
                        stmt += "case cu.defVideoProtocol when 2 then 'ISDN' else 'IP' end AS vidProtocol, ";
                        stmt += "case c.Status when 0 then 'Confirmed' else 'Pending' end AS 'Status', ";
                        stmt += "c.description FROM conf_conference_d c";
                        stmt += " left outer join (select LastName, firstname, UserID from usr_list_d) U on c.owner = u.userid "; //ZD 103878
                        stmt += " left outer join (select LastName, firstname, id from Usr_Deletelist_D) UD on c.owner = UD.id ";
                        stmt += ", conf_room_d cu, loc_room_d l, loc_tier2_d l2, loc_tier3_d l3 ";
                        stmt += "WHERE c.orgId='" + organizationID.ToString() +"' and (c.confid = cu.confid AND c.instanceid = cu.instanceid) ";
                        stmt += "AND (c.deleted = 0) ";
                        //stmt += "AND c.owner = u.userid "; //ZD 103878
                        stmt += "AND l.roomid = cu.roomid ";
                        stmt += "AND l2.id = l.l2locationid ";
                        stmt += "And l3.id = l.l3locationid ";
                        stmt += "AND c.confdate BETWEEN dbo.changeTOGMTtime(" + timezone + ",'" + starttime + "') ";
                        stmt += "AND dbo.changeTOGMTtime(" + timezone + ",'" + endtime + "') ";
                        if (roomslist != "")
                            stmt += "AND cu.roomid IN (" + roomslist + ") ";
                        stmt += "ORDER BY dbo.changeTOGMTtime(" + timezone + ",c.confdate), Title, Room; ";

                        //Meeting Summary Query Part
                        stmt += "SELECT distinct c.externalname AS Title,  c.confnumname AS ConfNum, c.conftype, ";
                        stmt += "case c.conftype when 1 then 'Video' when 2 then 'Audio/Video' "; //ZD 100528
                        stmt += "when 3 then 'Immediate' when 4 then 'Point to Point' ";
                        stmt += "when 5 then 'Template' when 6 then 'AudioOnly' ";
                        stmt += "when 7 then 'RoomOnly' when 8 then 'Hotdesking' when 11 then 'Phantom' end AS meetType, ";//FB 2694
                        stmt += "case c.Status when 0 then 'Confirmed' else 'Pending' end AS 'Status' ";
                        stmt += "FROM conf_conference_d c, conf_room_d cu, loc_room_d l ";
                        stmt += "WHERE c.orgId='" + organizationID.ToString() + "' and (c.confid = cu.confid AND c.instanceid = cu.instanceid) ";
                        stmt += "AND l.roomid = cu.roomid  ";
                        stmt += "AND c.confdate BETWEEN dbo.changeTOGMTtime(" + timezone + ",'" + starttime + "') ";
                        stmt += "AND dbo.changeTOGMTtime(" + timezone + ",'" + endtime + "')  ";
                        if (roomslist != "")
                            stmt += "AND cu.roomid IN (" + roomslist + ")";
                    }
                    else if (ds.Tables[0].Rows.Count > 0)
                    {
                        stmtCus = "";

                        for (Int32 i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            stmtCus += " isnull([" + ds.Tables[0].Rows[i].ItemArray[0].ToString() + "],'N/A') as [" + ds.Tables[0].Rows[i].ItemArray[0].ToString() + "]";
                            stmtCus1 += " [" + ds.Tables[0].Rows[i].ItemArray[0].ToString() + "]";
                            if (i < ds.Tables[0].Rows.Count - 1)
                            {
                                stmtCus += ",";
                                stmtCus1 += ",";
                            }
                        }
                        
                        stmt = "SELECT startdate, enddate, duration,Room, Title, lastname,firstname,";
                        stmt += " Email,State,City,ConfNum,meetType,connType,vidProtocol,Status,description,"; //ZD 100288
                        stmt += " " + stmtCus + "";
                        stmt += " FROM";
                        stmt += " (";
                        stmt += " SELECT y.startdate, y.enddate, y.duration, y.Room, y.Title, y.lastname,y.firstname,";
                        stmt += " y.Email, y.State,y.City,y.ConfNum,y.confid,y.meetType,y.connType,y.vidProtocol,y.Status,y.description,";//ZD 100288
                        stmt += " isnull(z.selectedValue,'') as selectedValue,isnull(z.Displaytitle,'') as Displaytitle";
                        stmt += " from";
                        stmt += " (";
                        stmt += " SELECT dbo.changeTime(" + timezone + ", c.confdate) AS startdate,";
                        stmt += " dbo.changeTime(" + timezone + ",dateadd(minute,c.duration, c.confdate)) as enddate,";
                        stmt += " c.duration as duration, l.name AS Room, c.externalname AS Title,";
                        stmt += " case when CHARINDEX('H',UserIDRefText) > 0 then UD.LastName else U.LastName end as LastName,";//ZD 103878
                        stmt += " case when CHARINDEX('H',UserIDRefText) > 0 then UD.firstname else U.firstname end as firstname, ";                        
                        stmt += " u.Email as email, l3.name AS State,";//ZD 100288
                        stmt += " l2.name AS City, c.confnumname AS ConfNum,";
                        stmt += " c.confid as confid,";
                        stmt += " case c.conftype when 1 then 'Video' when 2 then 'Audio/Video' when 3 then 'Immediate' when 4 then 'Point to Point'"; //ZD 100528
                        stmt += " when 5 then 'Template' when 6 then 'AudioOnly' when 7 then 'RoomOnly' when 8 then 'Hotdesking' when 11 then 'Phantom' end AS meetType,";//FB 2694
                        stmt += " case cu.connectionType when 0 then 'Outbound' else 'Inbound' end AS connType,";
                        stmt += " case cu.defVideoProtocol when 2 then 'ISDN' else 'IP' end AS vidProtocol,";
                        stmt += " case c.Status when 0 then 'Confirmed' else 'Pending' end AS 'Status',";
                        stmt += " c.description as description FROM conf_conference_d c ";
                        stmt += " left outer join (select LastName, firstname, UserID, Email from usr_list_d) U on c.owner = u.userid "; //ZD 103878 //104606
                        stmt += " left outer join (select LastName, firstname, id from Usr_Deletelist_D) UD on c.owner = UD.id ";
                        stmt += ", conf_room_d cu, loc_room_d l, loc_tier2_d l2, loc_tier3_d l3";
                        stmt += " WHERE c.orgId='" + organizationID.ToString() + "' and (c.confid = cu.confid AND c.instanceid = cu.instanceid) AND (c.deleted = 0) AND c.owner = u.userid AND";
                        stmt += " l.roomid = cu.roomid AND l2.id = l.l2locationid And l3.id = l.l3locationid AND c.confdate";
                        stmt += " BETWEEN dbo.changeTOGMTtime(" + timezone + ",'" + starttime + "') AND dbo.changeTOGMTtime(" + timezone + ",'" + endtime + "')";
                        if (roomslist != "")
                            stmt += " AND cu.roomid IN (" + roomslist + ")";
                        stmt += " )y";
                        stmt += " left outer join (select m.confid as custconfid,";
                        stmt += " case ltrim(rtrim(m.selectedValue))";
                        stmt += " when '0'  then 'No'";
                        stmt += " when '1'  then 'Yes' ";
                        stmt += " when '' then 'N/A'";
                        stmt += " else ltrim(rtrim(m.selectedValue)) ";
                        stmt += " end AS selectedValue,";
                        stmt += " d.Displaytitle as Displaytitle from Conf_CustomAttr_D m,";
                        stmt += " dept_CustomAttr_D d where m.CustomAttributeID = d.CustomAttributeID AND";
                        stmt += " d.deleted=0 And d.Status=0 )z on z.custconfid = y.confid";
                        stmt += " ) AS TableToBePivoted";
                        stmt += " PIVOT";
                        stmt += " (";
                        stmt += " max(selectedValue)";
                        stmt += " FOR Displaytitle IN (" + stmtCus1 + ")";
                        stmt += " ) AS PivotedTable ORDER BY startdate,Title, Room;";

                        //Meeting Summary Query Part
                        stmt += "SELECT distinct c.externalname AS [Meeting Title],  c.confnumname AS ConfNum, c.conftype, ";
                        stmt += "case c.conftype when 1 then 'Video' when 2 then 'Audio/Video' "; //ZD 100528
                        stmt += "when 3 then 'Immediate' when 4 then 'Point to Point' ";
                        stmt += "when 5 then 'Template' when 6 then 'AudioOnly' ";
                        stmt += "when 7 then 'RoomOnly' when 8 then 'Hotdesking' when 11 then 'Phantom' end AS meetType, ";//FB 2694
                        stmt += "case c.Status when 0 then 'Confirmed' else 'Pending' end AS 'Status' ";
                        stmt += "FROM conf_conference_d c, conf_room_d cu, loc_room_d l ";
                        stmt += "WHERE c.orgId='" + organizationID.ToString() + "' and (c.confid = cu.confid AND c.instanceid = cu.instanceid) ";
                        stmt += "AND l.roomid = cu.roomid  ";
                        stmt += "AND c.confdate BETWEEN dbo.changeTOGMTtime(" + timezone + ",'" + starttime + "') ";
                        stmt += "AND dbo.changeTOGMTtime(" + timezone + ",'" + endtime + "')  ";
                        if (roomslist != "")
                            stmt += "AND cu.roomid IN (" + roomslist + ")";

                    }
                    //Edited for FB 1511 END
                }
                //Added for FB 1511 End
                return stmt;

            }
            catch (Exception ex)
            {
                m_log.Error("sytemException", ex);
                return "";
            }
        }

        public String CalendarReport(ref String stmt, String starttime, String endtime, String organizationID, String roomslist,String dtFormat)//FB 2588
        {
            //string stmt = "";
            //Added for FB 1511 START
            string stmtCus = "";
            string stmtCus1 = "";
            string stmtConcierge = "";//FB 2632
            //Added for FB 1511 END
            try
            {
                //Calendar Report Query Part
                //Added for FB 1511 START
                stmtCus = "select DisplayTitle from dept_CustomAttr_D where deleted=0 and status=0 and orgId='" + organizationID.ToString() + "'";
                if (m_rptDAO != null)
                    m_rptLayer = new ns_SqlHelper.SqlHelper(configpath);

                DataSet ds = null;
                ds = m_rptLayer.ExecuteDataSet(stmtCus);
                if (ds != null)
                {

                    stmtCus = "";
                    stmtCus1 = "";

                    for (Int32 i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        stmtCus += " isnull([" + ds.Tables[0].Rows[i].ItemArray[0].ToString() + "],'N/A') as [" + ds.Tables[0].Rows[i].ItemArray[0].ToString() + "]";
                        stmtCus1 += " [" + ds.Tables[0].Rows[i].ItemArray[0].ToString() + "]";
                        if (i < ds.Tables[0].Rows.Count - 1)
                        {
                            stmtCus += ",";
                            stmtCus1 += ",";
                        }
                    }

                    if (stmtCus1 == "")
                        stmtCus1 = "NR";

                    stmtConcierge = "when 0 then 'No' when 1 then 'Yes' when '' then 'N/A' end AS";//FB 2632

                    stmt = "SELECT startdate as [Date], startdate as [Start], enddate as [End], Duration as [Duration (mins)],Room, Title as [Meeting Title], lastname as [Last Name],firstname as [First Name],";
                    stmt += " Email,State,City,ConfNum as [Conference #],meetType as [Meeting Type],connType as [Connection Type],vidProtocol as Protocol,Status,description as Remarks,";//ZD 100288
                    if (stmtCus != "")
                        stmt += " " + stmtCus + "";
                    stmt += " ,case OnSiteAVSupport " + stmtConcierge + " [On-Site A/V Support]";//FB 2632
                    stmt += " ,case MeetandGreet " + stmtConcierge + " [Meet and Greet]";
                    stmt += " ,case ConciergeMonitoring " + stmtConcierge + " [Call Monitoring]";//FB 3023
                    stmt += " ,case DedicatedVNOCOperator " + stmtConcierge + " [Dedicated VNOC Operator]";
                    stmt += " FROM";
                    stmt += " (";
                    stmt += " SELECT y.startdate, y.enddate, y.duration, y.Room, y.Title, y.lastname,y.firstname,";
                    stmt += " y.Email, y.State,y.City,y.ConfNum,y.confid,y.meetType,y.connType,y.vidProtocol,y.Status,y.description,";//ZD 100288
                    stmt += " isnull(z.selectedValue,'') as selectedValue,isnull(z.Displaytitle,'') as Displaytitle,";
                    stmt += " y.OnSiteAVSupport,y.MeetandGreet,y.ConciergeMonitoring, y.DedicatedVNOCOperator";//FB 2632
                    stmt += " from";
                    stmt += " (";

                    if (dtFormat == "HHmmZ")//FB 2588
                        stmt += " SELECT c.confdate AS startdate,dateadd(minute,c.duration, c.confdate) as enddate,";
                    else
                        stmt += " SELECT dbo.changeTime(" + timezone + ", c.confdate) AS startdate,dbo.changeTime(" + timezone + ",dateadd(minute,c.duration, c.confdate)) as enddate,";

                    stmt += " c.duration as duration, l.name AS Room, c.externalname AS Title, ";
                    stmt += "case when CHARINDEX('H',UserIDRefText) > 0 then UD.LastName else U.LastName end as LastName,";//ZD 103878
                    stmt += "case when CHARINDEX('H',UserIDRefText) > 0 then UD.firstname else U.firstname end as firstname, ";
                    stmt += " u.Email as email, l3.name AS State,";//ZD 100288
                    stmt += " l2.name AS City, c.confnumname AS ConfNum,";
                    stmt += " c.confid as confid,";
                    stmt += " case c.conftype when 1 then 'Video' when 2 then 'Audio/Video' when 3 then 'Immediate' when 4 then 'Point to Point'"; //ZD 100528
                    stmt += " when 5 then 'Template' when 6 then 'AudioOnly' when 7 then 'RoomOnly' when 8 then 'Hotdesking' when 11 then 'Phantom' end AS meetType,";//FB 2694
                    stmt += " case cu.connectionType when 0 then 'Outbound' else 'Inbound' end AS connType,";
                    stmt += " case cu.defVideoProtocol when 2 then 'ISDN' else 'IP' end AS vidProtocol,";
                    stmt += " case c.Status when 0 then 'Confirmed' else 'Pending' end AS 'Status',";
                    stmt += " c.OnSiteAVSupport,c.MeetandGreet,c.ConciergeMonitoring, c.DedicatedVNOCOperator,";//FB 2632
                    stmt += " c.description as description FROM conf_conference_d c";
                    stmt += " left outer join (select LastName, firstname, UserID, Email from usr_list_d) U on c.owner = u.userid "; //ZD 103878 //ZD 104606
                    stmt += " left outer join (select LastName, firstname, id from Usr_Deletelist_D) UD on c.owner = UD.id ";
                    stmt += ", conf_room_d cu, loc_room_d l, loc_tier2_d l2, loc_tier3_d l3";
                    stmt += " WHERE c.orgId='" + organizationID.ToString() + "' and (c.confid = cu.confid AND c.instanceid = cu.instanceid) AND (c.deleted = 0) AND c.isHDBusy = 0 AND c.owner = u.userid AND"; //ALLDEV-807
                    stmt += " l.roomid = cu.roomid AND l2.id = l.l2locationid And l3.id = l.l3locationid AND c.confdate";
                    if (dtFormat == "HHmmZ")//FB 2588
                        stmt += " BETWEEN '" + starttime + "' AND '" + endtime + "'";
                    else
                        stmt += " BETWEEN dbo.changeTOGMTtime(" + timezone + ",'" + starttime + "') AND dbo.changeTOGMTtime(" + timezone + ",'" + endtime + "')";

                    if (roomslist != "")
                        stmt += " AND cu.roomid IN (" + roomslist + ")";
                    stmt += " )y";
                    stmt += " left outer join (select m.confid as custconfid,";
                    stmt += " case ltrim(rtrim(m.selectedValue))";
                    stmt += " when '0'  then 'No'";
                    stmt += " when '1'  then 'Yes' ";
                    stmt += " when '' then 'N/A'";
                    stmt += " else ltrim(rtrim(m.selectedValue)) ";
                    stmt += " end AS selectedValue,";
                    stmt += " d.Displaytitle as Displaytitle from Conf_CustomAttr_D m,";
                    stmt += " dept_CustomAttr_D d where m.CustomAttributeID = d.CustomAttributeID AND";
                    stmt += " d.deleted=0 And d.Status=0 )z on z.custconfid = y.confid";
                    stmt += " ) AS TableToBePivoted";
                    stmt += " PIVOT";
                    stmt += " (";
                    stmt += " max(selectedValue)";
                    stmt += " FOR Displaytitle IN (" + stmtCus1 + ")";
                    stmt += " ) AS PivotedTable ";

                    //ZD 101835
                    stmt += " Union All " + stmt.Replace("conf_conference_d", "Archive_conf_conference_d").Replace("conf_room_d", "Archive_conf_room_d").Replace("Conf_CustomAttr_D", "Archive_Conf_CustomAttr_D");
                    stmt += " ORDER BY startdate,Title, Room";
                    
                    stmt += ";";

                    String stmtSub = "";
                    //Meeting Summary Query Part
                    stmtSub += "SELECT distinct c.externalname AS [Meeting Title],  c.confnumname AS ConfNum, c.conftype, ";
                    stmtSub += "case c.conftype when 1 then 'Video' when 2 then 'Audio/Video' "; //ZD 100528
                    stmtSub += "when 3 then 'Immediate' when 4 then 'Point to Point' ";
                    stmtSub += "when 5 then 'Template' when 6 then 'AudioOnly' ";
                    stmtSub += "when 7 then 'RoomOnly' when 8 then 'Hotdesking' when 11 then 'Phantom' end AS meetType, ";//FB 2694
                    stmtSub += "case c.Status when 0 then 'Confirmed' else 'Pending' end AS 'Status' ";
                    stmtSub += "FROM conf_conference_d c, conf_room_d cu, loc_room_d l ";
                    stmtSub += "WHERE c.orgId='" + organizationID.ToString() + "' and (c.confid = cu.confid AND c.instanceid = cu.instanceid) ";
                    stmtSub += "AND l.roomid = cu.roomid  ";
                    if (dtFormat == "HHmmZ")//FB 2588
                        stmtSub += "AND c.confdate BETWEEN '" + starttime + "' AND '" + endtime + "'";
                    else
                        stmtSub += "AND c.confdate BETWEEN dbo.changeTOGMTtime(" + timezone + ",'" + starttime + "') AND dbo.changeTOGMTtime(" + timezone + ",'" + endtime + "') ";

                    if (roomslist != "")
                        stmtSub += "AND cu.roomid IN (" + roomslist + ")";

                    stmtSub += " Union All " + stmtSub.Replace("conf_conference_d", "Archive_conf_conference_d").Replace("conf_room_d", "Archive_conf_room_d");
                    stmt += stmtSub;
                }
                //Edited for FB 1511 END
                return stmt;
            }
            catch (Exception ex)
            {
                m_log.Error("sytemException", ex);
                return "";
            }
        }
        #endregion

        #region ContactList
        /// <summary>
        /// ContactList
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public String ContactList(ref String stmt, String organizationID)
        {
            //string stmt = "";
            try
            {
                stmt = "SELECT l3.name AS 'Tier1', l2.name AS 'Tier2', l.name AS [Room Name],";
                stmt += " case ep.addressType when 1 then 'IP Address' when 2 then 'H323 ID' when 3 then 'E.164' when 2 then 'ISDN Phone number' when 3 then 'MPI' end AS [Address Type],";
                stmt += " case l.videoAvailable when 1 then 'Audio-only' when 2 then 'Audio/Video' when 0 then 'None' end AS [Room Type],";//ZD 100288 //ZD 100528
                stmt += " case ep.addressType when 1 then ep.address when 2 then ep.address when 3 then ep.address when 4 then ep.address else '' end AS 'Address',"; //'VoIP #'
                stmt += " l.RoomPhone AS 'Room Phone #', u.lastname + '' +  u.firstname as [Contact Name], "; //ISDN #
                stmt += " u.Email as Email,u.WorkPhone as [Office Phone #],u.CellPhone as [Cell #], ep.name AS 'Endpoint Name'";//ZD 100288
                stmt += " FROM usr_list_d U,(select *, (select top 1 AssistantId from Loc_Assistant_D la where la.RoomId = r.RoomID order by la.ID) as Assistant1 from loc_room_d r) l";  //ALLDEV-807
                stmt += " , loc_tier2_d l2, loc_tier3_d l3, ept_list_d ep, gen_videoequipment_s ve ";
                //Code Added for Organization Module
                stmt += " WHERE U.companyId=" + organizationID.ToString() + " and l.Assistant1 = u.userid  AND l2.id = l.l2locationid AND l3.id = l.l3locationid AND ep.videoequipmentid = ve.veid AND "; //ALLDEV-807
                stmt += " (ep.endpointid = l.endpointid AND ep.isDefault = 1)";
                stmt += " ORDER BY State, City, [Room Name]";
                return stmt;

            }
            catch (Exception ex)
            {
                m_log.Error("sytemException", ex);
                return "";
            }
        }
       
        #endregion

        #region PRIAllocation
        /// <summary>
        /// PRIAllocation
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public String PRIAllocation(ref String stmt, String starttime, String endtime, String organizationID, String dtFormat)//FB 2588
        {
            //string stmt = "";
            //Added for FB 1511 START
            string stmtCus = "";
            string stmtCus1 = "";
            string stmtConcierge = "";//FB 2632
            //Added for FB 1511 END
            try
            {
                //Added for FB 1511 START
                stmtCus = "select DisplayTitle from dept_CustomAttr_D where deleted=0 and status=0 and orgId='"+ organizationID.ToString() +"'";
                if (m_rptDAO != null)
                    m_rptLayer = new ns_SqlHelper.SqlHelper(configpath);

                DataSet ds = null;
                ds = m_rptLayer.ExecuteDataSet(stmtCus);
                if (ds != null)
                {

                    stmtCus = "";

                    for (Int32 i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        stmtCus += " isnull([" + ds.Tables[0].Rows[i].ItemArray[0].ToString() + "],'N/A') as [" + ds.Tables[0].Rows[i].ItemArray[0].ToString() + "]";
                        stmtCus1 += " [" + ds.Tables[0].Rows[i].ItemArray[0].ToString() + "]";
                        if (i < ds.Tables[0].Rows.Count - 1)
                        {
                            stmtCus += ",";
                            stmtCus1 += ",";
                        }
                    }

                    if (stmtCus1 == "")
                        stmtCus1 = "NR";

                    stmtConcierge = "when 0 then 'No' when 1 then 'Yes' when '' then 'N/A' end AS";//FB 2632

                    stmt = "SELECT startdate as [Date], enddate, PRI, Title as [Meeting Title]";
                    if (stmtCus != "")
                        stmt += " ," + stmtCus + "";
                    stmt += " ,case OnSiteAVSupport " + stmtConcierge + " [On-Site A/V Support]";//FB 2632
                    stmt += " ,case MeetandGreet " + stmtConcierge + " [Meet and Greet]";
                    stmt += " ,case ConciergeMonitoring " + stmtConcierge + " [Call Monitoring]";//FB 3023
                    stmt += " ,case DedicatedVNOCOperator " + stmtConcierge + " [Dedicated VNOC Operator]";
                    stmt += " FROM";
                    stmt += " (";
                    stmt += " SELECT y.startdate, y.enddate, y.Title, y.PRI,y.confid";
                    stmt += " , isnull(z.selectedValue,'') as selectedValue,isnull(z.Displaytitle,'') as Displaytitle,";
                    stmt += " y.OnSiteAVSupport,y.MeetandGreet,y.ConciergeMonitoring, y.DedicatedVNOCOperator";//FB 2632
                    stmt += " from";
                    stmt += " (";
                    if(dtFormat == "HHmmZ") //FB 2588
                        stmt += " select c.confdate as startdate, dateadd(minute,c.duration, c.confdate) as enddate,";
                    else
                        stmt += " select dbo.changeTime(" + timezone + ", c.confdate) AS startdate,dbo.changeTime(" + timezone + ",dateadd(minute,c.duration, c.confdate)) as enddate,";

                    stmt += " c.externalname AS Title"; //, ca.selectedValue as selectedval";
                    stmt += " , cr.mcuservicename AS PRI,c.confid as confid,";
                    stmt += " c.OnSiteAVSupport,c.MeetandGreet,c.ConciergeMonitoring, c.DedicatedVNOCOperator";//FB 2632
                    stmt += " FROM conf_conference_d c, conf_room_d cr"; //, conf_customattr_d ca";
                    stmt += " WHERE c.orgId='" + organizationID.ToString() + "' and (c.confid = cr.confid AND c.instanceid = cr.instanceid)";
                    //stmt += " AND (c.confid = ca.confid AND c.instanceid = ca.instanceid)";
                    if (dtFormat == "HHmmZ") //FB 2588
                        stmt += " AND c.confdate BETWEEN '" + starttime + "' AND '" + endtime + "' ";
                    else
                        stmt += " AND c.confdate BETWEEN dbo.changeTOGMTtime(" + timezone + ",'" + starttime + "') AND dbo.changeTOGMTtime(" + timezone + ",'" + endtime + "') ";

                    stmt += " AND cr.addressType = 4";

                    stmt += " )y";
                    stmt += " left outer join (select m.confid as custconfid,";
                    stmt += " case ltrim(rtrim(m.selectedValue))";
                    stmt += " when '0'  then 'No'";
                    stmt += " when '1'  then 'Yes' ";
                    stmt += " when '' then 'N/A'";
                    stmt += " else ltrim(rtrim(m.selectedValue)) ";
                    stmt += " end AS selectedValue,";
                    stmt += " d.Displaytitle as Displaytitle from Conf_CustomAttr_D m,";
                    stmt += " dept_CustomAttr_D d where m.CustomAttributeID = d.CustomAttributeID AND";
                    stmt += " d.deleted=0 And d.Status=0 )z on z.custconfid = y.confid";

                    stmt += " ) AS TableToBePivoted";
                    stmt += " PIVOT";
                    stmt += " (";
                    stmt += " max(selectedValue)";
                    stmt += " FOR Displaytitle IN (" + stmtCus1 + ")";
                    stmt += " ) AS PivotedTable ";

                    //ZD 101835
                    stmt += " union all " + stmt.Replace("conf_conference_d", "Archive_Conf_Conference_D").Replace("conf_room_d", "Archive_Conf_Room_D").Replace("Conf_CustomAttr_D", "Archive_Conf_CustomAttr_D");

                    stmt += " ORDER BY startdate, PRI, Title;";
                }
                //Added for FB 1511 End
                return stmt;
            }
            catch (Exception ex)
            {
                m_log.Error("sytemException", ex);
                return "";
            }
        }
        #endregion

        #region ResourceAllocationReport
        /// <summary>
        /// ResourceAllocationReport
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public String ResourceAllocationReport(ref String stmt, String starttime, String endtime, String organizationID, String dtFormat) //FB 2588
        {
            //string stmt = "";
            //Added for FB 1511 START
            string stmtCus = "";
            string stmtCus1 = "";
            string stmtConcierge = "";//FB 2632
            //Added for FB 1511 END
            try
            {
                //Added for FB 1511 START
                stmtCus = "select DisplayTitle from dept_CustomAttr_D where deleted=0 and status=0 and orgId='" + organizationID.ToString() +"'";
                if (m_rptDAO != null)
                    m_rptLayer = new ns_SqlHelper.SqlHelper(configpath);

                DataSet ds = null;
                ds = m_rptLayer.ExecuteDataSet(stmtCus);
                if (ds != null)
                {
                    stmtCus = "";

                    for (Int32 i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        stmtCus += " isnull([" + ds.Tables[0].Rows[i].ItemArray[0].ToString() + "],'N/A') as [" + ds.Tables[0].Rows[i].ItemArray[0].ToString() + "]";
                        stmtCus1 += " [" + ds.Tables[0].Rows[i].ItemArray[0].ToString() + "]";
                        if (i < ds.Tables[0].Rows.Count - 1)
                        {
                            stmtCus += ",";
                            stmtCus1 += ",";
                        }
                    }

                    if (stmtCus1 == "")
                        stmtCus1 = "NR";

                    stmtConcierge = "when 0 then 'No' when 1 then 'Yes' when '' then 'N/A' end AS";//FB 2632

                    stmt = "SELECT startdate as [Date], startdate as [Start], enddate as [End], enddate,Room,Title as [Meeting Title]";
                    if (stmtCus != "")
                        stmt += " ," + stmtCus + "";
                    stmt += " ,case OnSiteAVSupport " + stmtConcierge + " [On-Site A/V Support]";//FB 2632
                    stmt += " ,case MeetandGreet " + stmtConcierge + " [Meet and Greet]";
                    stmt += " ,case ConciergeMonitoring " + stmtConcierge + " [Call Monitoring]";//FB 3023
                    stmt += " ,case DedicatedVNOCOperator " + stmtConcierge + " [Dedicated VNOC Operator]";
                    stmt += " FROM";
                    stmt += " (";
                    stmt += " SELECT y.startdate, y.enddate, y.Room, y.Title";
                    stmt += " ,isnull(z.selectedValue,'') as selectedValue,isnull(z.Displaytitle,'') as Displaytitle,";
                    stmt += " y.OnSiteAVSupport,y.MeetandGreet,y.ConciergeMonitoring, y.DedicatedVNOCOperator";//FB 2632
                    stmt += " from";
                    stmt += " (";
                    if (dtFormat == "HHmmZ") //FB 2588
                        stmt += " SELECT  c.confdate AS startdate, dateadd(minute,c.duration,c.confdate) as enddate,";
                    else
                        stmt += " SELECT dbo.changeTime(" + timezone + ", c.confdate) AS startdate, dbo.changeTime(" + timezone + ",dateadd(minute,c.duration,c.confdate)) as enddate,";

                    stmt += " l.name AS Room, c.OnSiteAVSupport,c.MeetandGreet,c.ConciergeMonitoring, c.DedicatedVNOCOperator,";//FB 2632
                    stmt += " c.externalname AS Title,c.confid as confid FROM conf_conference_d c, conf_room_d cr, loc_room_d l";
                    stmt += " WHERE  c.orgId='" + organizationID.ToString() + "' and (c.confid = cr.confid AND c.instanceid = cr.instanceid) AND l.roomid = cr.roomid AND c.Deleted = 0";
                    if (dtFormat == "HHmmZ") //FB 2588
                        stmt += " AND c.confdate BETWEEN '" + starttime + "' AND '" + endtime + "'";
                    else
                        stmt += "AND c.isHDBusy =0 AND c.confdate BETWEEN dbo.changeTOGMTtime(" + timezone + ",'" + starttime + "') AND dbo.changeTOGMTtime(" + timezone + ",'" + endtime + "')"; //ALLDEV-807

                    stmt += " )y";
                    stmt += " left outer join (select m.confid as custconfid,";
                    stmt += " case ltrim(rtrim(m.selectedValue))";
                    stmt += " when '0'  then 'No'";
                    stmt += " when '1'  then 'Yes' ";
                    stmt += " when '' then 'N/A'";
                    stmt += " else ltrim(rtrim(m.selectedValue)) ";
                    stmt += " end AS selectedValue,";
                    stmt += " d.Displaytitle as Displaytitle from Conf_CustomAttr_D m,";
                    stmt += " dept_CustomAttr_D d where m.CustomAttributeID = d.CustomAttributeID AND";
                    stmt += " d.deleted=0 And d.Status=0 )z on z.custconfid = y.confid";
                    stmt += " ) AS TableToBePivoted";
                    stmt += " PIVOT";
                    stmt += " (";
                    stmt += " max(selectedValue)";
                    stmt += " FOR Displaytitle IN (" + stmtCus1 + ")";
                    stmt += " ) AS PivotedTable ";

                    //ZD 101835
                    stmt += " union all " + stmt.Replace("conf_conference_d", "Archive_Conf_Conference_D").Replace("conf_room_d", "Archive_Conf_Room_D").Replace("Conf_CustomAttr_D", "Archive_Conf_CustomAttr_D");

                    stmt += " ORDER BY startdate, Title, Room;";
                }
                //Added for FB 1511 End
                return stmt;
            }
            catch (Exception ex)
            {
                m_log.Error("sytemException", ex);
                return "";
            }
        }
        public String ResourceAllocationReport1()
        {
            string stmt = "";
            try
            {
                stmt = "SELECT dbo.changeTime(" + timezone + ", c.confdate) AS startdate, ";
                stmt += " dbo.changeTime(" + timezone + ",dateadd(minute,c.duration,c.confdate)) as enddate,";
                stmt += " l.name AS Room, c.externalname AS Title FROM conf_conference_d c, conf_room_d cr, loc_room_d l ";
                //Code Added for Organization Module
                stmt += " WHERE  c.orgId=" + organizationID.ToString() + " and (c.confid = cr.confid AND c.instanceid = cr.instanceid) AND l.roomid = cr.roomid AND c.Deleted = 0 AND";
                stmt += " c.confdate BETWEEN dbo.changeTOGMTtime(" + timezone + ",'" + starttime + "') AND dbo.changeTOGMTtime(" + timezone + ",'" + endtime + "')";
                stmt += " ORDER BY dbo.changeTOGMTtime(" + timezone + ",c.confdate), Title, Room";
                return stmt;
            }
            catch (Exception ex)
            {
                m_log.Error("sytemException", ex);
                return "";
            }
        }
        #endregion

        #region DailyScheduleReport
        /// <summary>
        /// DailyScheduleReport
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public String DailyScheduleReport(ref String stmt, String starttime, String endtime, String userId, String organizationID, String dtFormat)//FB 2588
        {
            //string stmt = "";
            //Added for FB 1511 START
            string stmtCus = "";
            string stmtCus1 = "";
            string stmtConcierge="";//FB 2632
            //Added for FB 1511 END
            try
            {
                //Added for FB 1511 START
                stmtCus = "select DisplayTitle from dept_CustomAttr_D where deleted=0 and status=0 and orgId='"+ organizationID.ToString() +"'";
                if (m_rptDAO != null)
                    m_rptLayer = new ns_SqlHelper.SqlHelper(configpath);

                DataSet ds = null;
                ds = m_rptLayer.ExecuteDataSet(stmtCus);
                if (ds != null)
                {
                    stmtCus = "";
                    stmtCus1 = "";

                    for (Int32 i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        stmtCus += " isnull([" + ds.Tables[0].Rows[i].ItemArray[0].ToString() + "],'N/A') as [" + ds.Tables[0].Rows[i].ItemArray[0].ToString() + "]";
                        stmtCus1 += " [" + ds.Tables[0].Rows[i].ItemArray[0].ToString() + "]";
                        if (i < ds.Tables[0].Rows.Count - 1)
                        {
                            stmtCus += ",";
                            stmtCus1 += ",";
                        }
                    }

                    if (stmtCus1 == "")
                        stmtCus1 = "NR";

                    stmtConcierge = "when 0 then 'No' when 1 then 'Yes' when '' then 'N/A' end AS";//FB 2632

                    stmt = "SELECT startdate as [Date], startdate as Start, enddate as [End], Room, title as [Meeting Title], lastname as [Last Name],";
                    stmt += " confNum as [Conference #],Remarks,TimeZone as [Time Zone]"; //FB 2205-s
                    if (stmtCus != "")
                        stmt += " , " + stmtCus + "";
                    stmt += " ,case OnSiteAVSupport " + stmtConcierge + " [On-Site A/V Support]";//FB 2632
                    stmt += " ,case MeetandGreet " + stmtConcierge + " [Meet and Greet]";
                    stmt += " ,case ConciergeMonitoring " + stmtConcierge + " [Call Monitoring]";//FB 3023
                    stmt += " ,case DedicatedVNOCOperator " + stmtConcierge + " [Dedicated VNOC Operator]";
                    stmt += " FROM";
                    stmt += " (";
                    stmt += " SELECT y.startdate, y.enddate, y.Room, y.title, y.lastname,y.confNum, y.TimeZone, y.duration,y.Remarks,y.confType as [Type],";
                    stmt += " y.OnSiteAVSupport,y.MeetandGreet,y.ConciergeMonitoring, y.DedicatedVNOCOperator,";//FB 2632
                    stmt += " isnull(z.selectedValue,'N/A') as selectedValue,isnull(z.Displaytitle,'N/A') as Displaytitle from";
                    if (dtFormat == "HHmmZ") //FB 2588
                        stmt += " (SELECT c.confdate as startdate, dateadd(minute,c.duration, c.confdate) as enddate";
                    else
                        stmt += " (SELECT dbo.changeTime(" + timezone + ", c.confdate) AS startdate, dbo.changeTime(" + timezone + ",dateadd(minute,c.duration, c.confdate)) as enddate";

                    stmt += " , l.name AS Room, c.externalname AS title, case when CHARINDEX('H',UserIDRefText) > 0 then UD.LastName else U.LastName end as lastname,"; //ZD 103878
                    stmt += " c.confnumname AS confNum, t.TimeZone AS TimeZone, c.duration AS duration, c.description AS Remarks,c.confid,";
                    stmt += " CASE c.conftype	when 1 then 'Y'	when 2 then 'Y'	when 6 then 'Y'	else 'N' END AS 'confType',";
                    stmt += " c.OnSiteAVSupport,c.MeetandGreet,c.ConciergeMonitoring, c.DedicatedVNOCOperator";//FB 2632
                    stmt += " FROM conf_conference_d C left outer join (select LastName,  UserID from usr_list_d) U on c.owner = u.userid left outer join (select LastName,  id from Usr_Deletelist_D) UD on c.owner = UD.id "; //ZD 103878
                    stmt += " , conf_room_d cr, gen_timezone_s t, loc_room_d l";
                    stmt += " WHERE c.orgId='" + organizationID.ToString() + "' and (c.confid = cr.confid AND c.instanceid = cr.instanceid)";
                    stmt += "  AND c.deleted=0 and c.isHDBusy = 0 AND t.timezoneid = '" + timezone + "'"; //FB 2205 //FB 2468(t.timezoneid = c.timezone)//ZD 100263 //ALLDEV-807
                    stmt += " AND l.roomid = cr.roomid AND confdate BETWEEN";
                    if (dtFormat == "HHmmZ") //FB 2588
                        stmt += "'" + starttime + "' AND '" + endtime + "'";
                    else
                        stmt += " dbo.changeTOGMTtime(" + timezone + ", '" + starttime + "') AND dbo.changeTOGMTtime(" + timezone + ", '" + endtime + "')";

                    stmt += " AND c.deleted = 0 )y";                    
                    stmt += " left outer join (select m.confid as custconfid,";
                    stmt += " case ltrim(rtrim(m.selectedValue)) when '0'  then 'No' when '1'  then 'Yes' when '' then 'N/A' else ltrim(rtrim(m.selectedValue)) end AS selectedValue,";
                    stmt += " d.Displaytitle as Displaytitle from Conf_CustomAttr_D m,";
                    stmt += " dept_CustomAttr_D d where m.CustomAttributeID = d.CustomAttributeID AND";
                    stmt += " d.deleted=0 And d.Status=0 )z on z.custconfid = y.confid";
                    stmt += " ) AS TableToBePivoted";
                    stmt += " PIVOT";
                    stmt += " ( max(selectedValue) FOR Displaytitle IN (" + stmtCus1 + ") ) AS PivotedTable ";
                    //ZD 101835
                    stmt += " union all " + stmt.Replace("conf_conference_d", "Archive_Conf_Conference_D").Replace("conf_room_d", "Archive_Conf_Room_D").Replace("Conf_CustomAttr_D", "Archive_Conf_CustomAttr_D");

                    stmt += " ORDER BY startdate, confNum;";                                        

                }
                //Added for FB 1511 End

                return stmt;
            }
            catch (Exception ex)
            {
                m_log.Error("sytemException", ex);
                return "";
            }
        }
       
        #endregion

        #region AggregateUsageReport
        /// <summary>
        /// AggregateUsageReport
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>

        public String AggregateUsageReport(ref String stmt, String starttime, String endtime, String organizationID, String dtFormat)//FB 2588
        {
           //string stmt = "";
            try
            {
                //FB 2343 - Starts
                /*stmt = "Select ";
                stmt += " isnull(sum(AudioOnly),0) as 'Total Audio Conferences', isnull(sum(AudioVideo),0)  as 'Total Audio-Video Conferences',";
                stmt += " isnull(sum([Point to Point]),0) as 'Total Point-to-Point Conferences', isnull(sum(RoomOnly),0)  as 'Total Room Conferences',";*/

                stmt = "Select";
                stmt += " isnull(sum(AudioOnly),0) as 'Total Audio Conferences',";
                if (dtFormat == "HHmmZ")//FB 2588
                    stmt += " convert ( decimal(5,2),(convert(float,(isnull(sum(AudioOnly1),0)))/convert(float,datediff(minute, '" + starttime + "', '" + endtime + "')))*100)";
                else
                    stmt += " convert ( decimal(5,2),(convert(float,(isnull(sum(AudioOnly1),0)))/convert(float,datediff(minute, dbo.changeTOGMTtime(" + timezone + ", '" + starttime + "') , dbo.changeTOGMTtime(" + timezone + ",  '" + endtime + "'))))*100)";
                stmt += " as 'Percentage of Total Audio Conferences',"; //FB 2343(% to Percentage)

                stmt += " isnull(sum(AudioVideo),0)  as 'Total Audio/Video Conferences',";
                if (dtFormat == "HHmmZ")//FB 2588
                    stmt += " convert ( decimal(5,2),(convert(float,(isnull(sum(AudioVideo1),0)))/convert(float,datediff(minute, '" + starttime + "','" + endtime + "')))*100)";
                else
                    stmt += " convert ( decimal(5,2),(convert(float,(isnull(sum(AudioVideo1),0)))/convert(float,datediff(minute, dbo.changeTOGMTtime(" + timezone + ", '" + starttime + "') , dbo.changeTOGMTtime(" + timezone + ",  '" + endtime + "'))))*100)";

                stmt += " as 'Percentage of Total Audio/Video Conferences',"; //ZD 100528

                stmt += " isnull(sum([Point to Point]),0) as 'Total Point-to-Point Conferences',";
                if (dtFormat == "HHmmZ")//FB 2588
                    stmt += " convert ( decimal(5,2),(convert(float,(isnull(sum([Point to Point1]),0)))/convert(float,datediff(minute,'" + starttime + "', '" + endtime + "')))*100)";
                else
                    stmt += " convert ( decimal(5,2),(convert(float,(isnull(sum([Point to Point1]),0)))/convert(float,datediff(minute, dbo.changeTOGMTtime(" + timezone + ", '" + starttime + "') , dbo.changeTOGMTtime(" + timezone + ",  '" + endtime + "'))))*100)";

                stmt += " as 'Percentage of Total Point-to-Point Conferences',";

                stmt += " isnull(sum(RoomOnly),0)  as 'Total Room Conferences',";
                if (dtFormat == "HHmmZ")//FB 2588
                    stmt += " convert ( decimal(5,2),(convert(float,(isnull(sum(RoomOnly),0)))/convert(float,datediff(minute, '" + starttime + "', '" + endtime + "')))*100)";
                else
                    stmt += " convert ( decimal(5,2),(convert(float,(isnull(sum(RoomOnly),0)))/convert(float,datediff(minute, dbo.changeTOGMTtime(" + timezone + ", '" + starttime + "') , dbo.changeTOGMTtime(" + timezone + ",  '" + endtime + "'))))*100)";

                stmt += " as 'Percentage of Total Room Conferences',";

                stmt += " Count(*)  as 'Total Conferences', case when (sum(duration) > 0) then convert ( decimal(5,2),(convert(float, sum(duration))/60)) else 0 end as 'Total Duration(hrs)',"; //ZD 100528

                stmt += " (Select count(*) as cnt from (Select c.confid, cu1.userid From (Select distinct confid, InstanceId  from Conf_Conference_d ";
                stmt += " where orgId='" + organizationID.ToString() + "'  AND confdate BETWEEN ";

                if (dtFormat == "HHmmZ")//FB 2588
                    stmt += "'" + starttime + "' AND '" + endtime + "'";
                else
                    stmt += " dbo.changeTOGMTtime(" + timezone + ", '" + starttime + "') AND dbo.changeTOGMTtime(" + timezone + ",  '" + endtime + "')";

                stmt += " And deleted=0) c,(Select  distinct confid, UserId, InstanceId from Conf_User_D)cu1 where c.confid = cu1.confid ";
                stmt += " And c.instanceid= cu1.instanceid)x) as [Total Participants]";

                stmt += " from (SELECT c.confid,c.duration, ";
                stmt += " case when conftype=6 and (c.duration>0) then c.duration else 0 end as 'AudioOnly1', ";
                stmt += " case when conftype=2 and (c.duration>0) then c.duration else 0 end as 'AudioVideo1', ";
                stmt += " case when conftype=4 and (c.duration>0) then c.duration else 0 end as 'Point to Point1', ";
                stmt += " case when conftype=7 and (c.duration>0) then c.duration else 0 end as 'RoomOnly1', ";
                stmt += " case when conftype=8 and (c.duration>0) then c.duration else 0 end as 'Hotdesking1', ";//FB 2694
                if (dtFormat == "HHmmZ")//FB 2588
                    stmt += " datediff(minute, '" + starttime + "' ,'" + endtime + "') as 'dates', ";
                else
                    stmt += " datediff(minute, dbo.changeTOGMTtime(" + timezone + ", '" + starttime + "') , dbo.changeTOGMTtime(" + timezone + ",  '" + endtime + "')) as 'dates', ";

                stmt += " case c.conftype when 2 then 1 else 0 end as 'AudioVideo',";
                stmt += " case c.conftype when 4 then 1 else 0 end as 'Point to Point', case c.conftype when 6 then 1 else 0 end as 'AudioOnly',";
                stmt += " case c.conftype when 7 then 1 else 0 end as 'RoomOnly',";
                stmt += " case c.conftype when 8 then 1 else 0 end as 'Hotdesking'";//FB 2694
                //Organization module
                stmt += " FROM Conf_Conference_d c WHERE (c.deleted = 0) and c.orgId=" + organizationID.ToString() + "  AND ";
                if (dtFormat == "HHmmZ") //FB 2588
                    stmt += " c.confdate BETWEEN '" + starttime + "' AND '" + endtime + "')d group by d.dates";
                else
                    stmt += " c.confdate BETWEEN dbo.changeTOGMTtime(" + timezone + ", '" + starttime + "') AND dbo.changeTOGMTtime(" + timezone + ",  '" + endtime + "') )d group by d.dates";

                //ZD 101835
                stmt += " Union All " + stmt.Replace("Conf_Conference_d", "Archive_Conf_Conference_D").Replace("Conf_User_D", "Archive_Conf_User_D");

                //FB 2343 - End
                return stmt;
            }
            catch (Exception ex)
            {
                m_log.Error("sytemException", ex);
                return "";
            }
        }
        #endregion

        #region UsageByRoom
        /// <summary>
        /// UsageByRoom
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>

        public String UsageByRoom(ref String stmt, String starttime, String endtime, String organizationID, String dtFormat)//FB 2588
        {
            //string stmt = "";
            try
            {
                //FB 2343
                //stmt = "Select  c.[name] as [Room Name], isnull(d.duration,0) as [Total Minutes]  from (select sum(a.duration) as  duration,";
                //Code Added for Organization Module
                stmt = "Select  c.[name] as [Room Name], isnull(d.duration,0) as [Total Minutes],";
                if (dtFormat == "HHmmZ")//FB 2588
                    stmt += " convert ( decimal(5,2),(convert(float,isnull(d.duration,0))/convert(float,datediff(minute,'" + starttime + "','" + endtime + "')))*100)";
                else
                    stmt += " convert ( decimal(5,2),(convert(float,isnull(d.duration,0))/convert(float,datediff(minute, dbo.changeTOGMTtime(" + timezone + ", '" + starttime + "') , dbo.changeTOGMTtime(" + timezone + ",  '" + endtime + "'))))*100)";

                stmt += " as [Percentage of Usage]  from (select sum(a.duration) as  duration,"; //FB 2343
                stmt += " b.roomid from conf_conference_d as a, conf_room_d b where a.orgId='" + organizationID.ToString() + "'  AND  a.confid = b.confid and a.instanceid = b.instanceid ";
                if (dtFormat == "HHmmZ")//FB 2588
                    stmt += " AND a.confdate BETWEEN '" + starttime + "' AND '" + endtime + "'";
                else
                    stmt += " AND a.confdate BETWEEN dbo.changeTOGMTtime(" + timezone + ", '" + starttime + "') AND dbo.changeTOGMTtime(" + timezone + ",  '" + endtime + "')";

                stmt += " and a.deleted = 0 and a.isHDBusy=0 group by b.roomid) as d right outer join  loc_room_d c on d.roomid = c.roomid where c.disabled  != 1 and c.orgid = '" + organizationID.ToString() + "'"; //ALLDEV-807

                //ZD 101835
                stmt += " Union All " + stmt.Replace("conf_conference_d", "Archive_conf_conference_d").Replace("conf_room_d", "Archive_conf_room_d");

                stmt += " order by  [Total Minutes] desc"; //order by  c.[name]";  //FB 2343 

                return stmt;

            }
            catch (Exception ex)
            {
                m_log.Error("sytemException", ex);
                return "";
            }
        }
        #endregion
        
        //Report Fixes End

        //Added for Graphical Report START

        #region GetTotalUsageMonth
        public bool GetTotalUsageMonth(ref vrmDataObject obj)
        {
            bool bRet = true;
            string sYst = "";
            string sLWkS = "";
            string sLWkE = "";
            string sLMt = "";
            string sTWkS = "";
            string sTWkE = "";
            string sYtoDS = "";
            string sYtoDE = "";
            string sCusFm = "";
            string sCusTo = "";
            string strSQL = "";
            string sDtFormat = "";
            string sUsrId = "";
            Int32 sCurYr = DateTime.Now.Year;
            m_rptLayer = new ns_SqlHelper.SqlHelper();
            DataSet ds = new DataSet();
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;

                node = xd.SelectSingleNode("//GetTotalUsageMonth/Yesterday");
                if (node.InnerText != "")
                    sYst = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//GetTotalUsageMonth/LastWeek/DateFrom");
                if (node.InnerText != "")
                    sLWkS = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//GetTotalUsageMonth/LastWeek/DateTo");
                if (node.InnerText != "")
                    sLWkE = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//GetTotalUsageMonth/LastMonth");
                if (node.InnerText != "")
                    sLMt = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//GetTotalUsageMonth/ThisWeek/DateFrom");
                if (node.InnerText != "")
                    sTWkS = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//GetTotalUsageMonth/ThisWeek/DateTo");
                if (node.InnerText != "")
                    sTWkE = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//GetTotalUsageMonth/YearToDate/DateFrom");
                if (node.InnerText != "")
                    sYtoDS = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//GetTotalUsageMonth/YearToDate/DateTo");
                if (node.InnerText != "")
                    sYtoDE = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//GetTotalUsageMonth/CustomDate/DateFrom");
                if (node.InnerText != "")
                    sCusFm = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//GetTotalUsageMonth/CustomDate/DateTo");
                if (node.InnerText != "")
                    sCusTo = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//GetTotalUsageMonth/DateFormat");
                if (node.InnerText != "")
                    sDtFormat = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//GetTotalUsageMonth/UserID");
                if (node.InnerText != "")
                    sUsrId = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//GetTotalUsageMonth/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;
                int.TryParse(orgid, out organizationID);  //Organization Module Fixes
        

                m_rptLayer.OpenConnection();



                if (sYst != "" && sUsrId != "")
                {
                    strSQL = "select M,sum(conftype1) as conftype1, sum(conftype2) as conftype2, sum(conftype3) as conftype3,";
                    strSQL += " sum(conftype4) as conftype4, sum(confTypeCnt1) as confTypeCnt1, sum(confTypeCnt2) as confTypeCnt2,";
                    strSQL += " sum(confTypeCnt3) as confTypeCnt3, sum(confTypeCnt4) as confTypeCnt4 from";
                    strSQL += " (";
                    strSQL += " select conftype as conftype1,0 as conftype2,0 as conftype3,0 as conftype4,";
                    if (sDtFormat.Equals("dd/MM/yyyy"))
                        strSQL += " convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),103) M,";
                    else
                        strSQL += " convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),101) M,";
                    strSQL += " count(*) as confTypeCnt1, 0 as confTypeCnt2, 0 as confTypeCnt3, 0 as confTypeCnt4";
                    //Code Added for Organization Module
                    strSQL += " from Conf_Conference_D a where a.orgId=" + organizationID.ToString() + " and Conftype =6 and deleted = 0";
                    strSQL += " and dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) >= '" + sYst + " 00:00:00'";
                    strSQL += " and dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) <= '" + sYst + " 23:59:00'";
                    if (sDtFormat.Equals("dd/MM/yyyy"))
                        strSQL += " group by conftype, convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),103)";
                    else
                        strSQL += " group by conftype, convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),101)";
                    strSQL += " Union";
                    strSQL += " select 0 as conftype1,0 as conftype2,0 as conftype3,conftype as conftype4,";
                    if (sDtFormat.Equals("dd/MM/yyyy"))
                        strSQL += " convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),103) M,";
                    else
                        strSQL += " convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),101) M,";
                    strSQL += " 0 as confTypeCnt1, 0 as confTypeCnt2, 0 as confTypeCnt3, count(*) as confTypeCnt4";
                    //Code Added for Organization Module
                    strSQL += " from Conf_Conference_D a where a.orgId=" + organizationID.ToString() + " and Conftype =4 and deleted = 0";
                    strSQL += " and dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) >= '" + sYst + " 00:00:00'";
                    strSQL += " and dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) <= '" + sYst + " 23:59:00'";
                    if (sDtFormat.Equals("dd/MM/yyyy"))
                        strSQL += " group by conftype, convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),103)";
                    else
                        strSQL += " group by conftype, convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),101)";
                    strSQL += " Union";
                    strSQL += " select 0 as conftype1,conftype as conftype2,0 as conftype3,0 as conftype4,";
                    if (sDtFormat.Equals("dd/MM/yyyy"))
                        strSQL += " convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),103) M,";
                    else
                        strSQL += " convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),101) M,";
                    strSQL += " 0 as confTypeCnt1, count(*) as confTypeCnt2, 0 as confTypeCnt3, 0 as confTypeCnt4";
                    //Code Added for Organization Module
                    strSQL += " from Conf_Conference_D a where a.orgId=" + organizationID.ToString() + " and Conftype =2 and deleted = 0";
                    strSQL += " and dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) >= '" + sYst + " 00:00:00'";
                    strSQL += " and dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) <= '" + sYst + " 23:59:00'";
                    if (sDtFormat.Equals("dd/MM/yyyy"))
                        strSQL += " group by conftype, convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),103)";
                    else
                        strSQL += " group by conftype, convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),101)";
                    strSQL += " Union";
                    strSQL += " select 0 as conftype1,0 as conftype2,conftype as conftype3,0 as conftype4,";
                    if (sDtFormat.Equals("dd/MM/yyyy"))
                        strSQL += " convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),103) M,";
                    else
                        strSQL += " convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),101) M,";
                    strSQL += " 0 as confTypeCnt1, 0 as confTypeCnt2, count(*) as confTypeCnt3, 0 as confTypeCnt4";
                    //Code Added for Organization Module
                    strSQL += " from Conf_Conference_D a where a.orgId=" + organizationID.ToString() + " and Conftype =7 and deleted = 0";
                    strSQL += " and dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) >= '" + sYst + " 00:00:00'";
                    strSQL += " and dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) <= '" + sYst + " 23:59:00'";
                    if (sDtFormat.Equals("dd/MM/yyyy"))
                        strSQL += " group by conftype, convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),103)";
                    else
                        strSQL += " group by conftype, convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),101)";
                    strSQL += " ) as a";
                    strSQL += " group by M order by M";
                }
                else if (sLWkS != "" && sLWkE != "" && sUsrId != "")
                {
                    strSQL = "select M, sum(conftype1) as conftype1, sum(conftype2) as conftype2, sum(conftype3) as conftype3,";
                    strSQL += " sum(conftype4) as conftype4, sum(confTypeCnt1) as confTypeCnt1, sum(confTypeCnt2) as confTypeCnt2,";
                    strSQL += " sum(confTypeCnt3) as confTypeCnt3, sum(confTypeCnt4) as confTypeCnt4 from";
                    strSQL += " (";
                    strSQL += " select conftype as conftype1,0 as conftype2,0 as conftype3,0 as conftype4,";
                    if (sDtFormat.Equals("dd/MM/yyyy"))
                        strSQL += " convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),103) M,";
                    else
                        strSQL += " convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),101) M,";
                    strSQL += " count(*) as confTypeCnt1, 0 as confTypeCnt2, 0 as confTypeCnt3, 0 as confTypeCnt4";
                    //Code Added for Organization Module
                    strSQL += " from Conf_Conference_D a where a.orgId=" + organizationID.ToString() + " and Conftype =6 and deleted = 0";
                    strSQL += " and dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) >= '" + sLWkS + " 00:00:00'";
                    strSQL += " and dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) <= '" + sLWkE + " 23:59:00'";
                    if (sDtFormat.Equals("dd/MM/yyyy"))
                        strSQL += " group by conftype, convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),103)";
                    else
                        strSQL += " group by conftype, convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),101)";
                    strSQL += " Union";
                    strSQL += " select 0 as conftype1,0 as conftype2,0 as conftype3,conftype as conftype4,";
                    if (sDtFormat.Equals("dd/MM/yyyy"))
                        strSQL += " convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),103) M,";
                    else
                        strSQL += " convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),101) M,";
                    strSQL += " 0 as confTypeCnt1, 0 as confTypeCnt2, 0 as confTypeCnt3, count(*) as confTypeCnt4";
                    //Code Added for Organization Module
                    strSQL += " from Conf_Conference_D a where a.orgId=" + organizationID.ToString() + " and Conftype =4 and deleted = 0";
                    strSQL += " and dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) >= '" + sLWkS + " 00:00:00'";
                    strSQL += " and dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) <= '" + sLWkE + " 23:59:00'";
                    if (sDtFormat.Equals("dd/MM/yyyy"))
                        strSQL += " group by conftype, convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),103)";
                    else
                        strSQL += " group by conftype, convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),101)";
                    strSQL += " Union";
                    strSQL += " select 0 as conftype1,conftype as conftype2,0 as conftype3,0 as conftype4,";
                    if (sDtFormat.Equals("dd/MM/yyyy"))
                        strSQL += " convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),103) M,";
                    else
                        strSQL += " convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),101) M,";
                    strSQL += " 0 as confTypeCnt1, count(*) as confTypeCnt2, 0 as confTypeCnt3, 0 as confTypeCnt4";
                    //Code Added for Organization Module
                    strSQL += " from Conf_Conference_D a where a.orgId=" + organizationID.ToString() + " and Conftype =2 and deleted = 0";
                    strSQL += " and dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) >= '" + sLWkS + " 00:00:00'";
                    strSQL += " and dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) <= '" + sLWkE + " 23:59:00'";
                    if (sDtFormat.Equals("dd/MM/yyyy"))
                        strSQL += " group by conftype, convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),103)";
                    else
                        strSQL += " group by conftype, convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),101)";
                    strSQL += " Union";
                    strSQL += " select 0 as conftype1,0 as conftype2,conftype as conftype3,0 as conftype4,";
                    if (sDtFormat.Equals("dd/MM/yyyy"))
                        strSQL += " convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),103) M,";
                    else
                        strSQL += " convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),101) M,";
                    strSQL += " 0 as confTypeCnt1, 0 as confTypeCnt2, count(*) as confTypeCnt3, 0 as confTypeCnt4";
                    //Code Added for Organization Module
                    strSQL += " from Conf_Conference_D a where a.orgId=" + organizationID.ToString() + " and Conftype =7 and deleted = 0";
                    strSQL += " and dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) >= '" + sLWkS + " 00:00:00'";
                    strSQL += " and dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) <= '" + sLWkE + " 23:59:00'";
                    if (sDtFormat.Equals("dd/MM/yyyy"))
                        strSQL += " group by conftype, convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),103)";
                    else
                        strSQL += " group by conftype, convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),101)";
                    strSQL += " ) as a";
                    strSQL += " group by M order by M";
                }
                else if (sLMt != "" && sUsrId != "")
                {
                    strSQL = "select M,sum(conftype1) as conftype1, sum(conftype2) as conftype2, sum(conftype3) as conftype3,";
                    strSQL += " sum(conftype4) as conftype4, sum(confTypeCnt1) as confTypeCnt1, sum(confTypeCnt2) as confTypeCnt2,";
                    strSQL += " sum(confTypeCnt3) as confTypeCnt3, sum(confTypeCnt4) as confTypeCnt4 from";
                    strSQL += " (";
                    strSQL += " select conftype as conftype1,0 as conftype2,0 as conftype3,0 as conftype4,";

                    strSQL += " (left(datename(month,confdate),3)) M,";
                    strSQL += " count(*) as confTypeCnt1, 0 as confTypeCnt2, 0 as confTypeCnt3, 0 as confTypeCnt4";
                    //Code Added for Organization Module
                    strSQL += " from Conf_Conference_D a where a.orgId=" + organizationID.ToString() + " and Conftype =6 and deleted = 0";
                    strSQL += " and month(confdate) = " + sLMt + " and year(confdate)=" + sCurYr + "";
                    strSQL += " group by conftype, (left(datename(month,confdate),3))";

                    strSQL += " Union";
                    strSQL += " select 0 as conftype1,0 as conftype2,0 as conftype3,conftype as conftype4,";

                    strSQL += " (left(datename(month,confdate),3)) M,";
                    strSQL += " 0 as confTypeCnt1, 0 as confTypeCnt2, 0 as confTypeCnt3, count(*) as confTypeCnt4";
                    //Code Added for Organization Module
                    strSQL += " from Conf_Conference_D a where a.orgId=" + organizationID.ToString() + " and Conftype =4 and deleted = 0";
                    strSQL += " and month(confdate) = " + sLMt + " and year(confdate)=" + sCurYr + "";

                    strSQL += " group by conftype, (left(datename(month,confdate),3))";
                    strSQL += " Union";
                    strSQL += " select 0 as conftype1,conftype as conftype2,0 as conftype3,0 as conftype4,";

                    strSQL += " (left(datename(month,confdate),3)) M,";
                    strSQL += " 0 as confTypeCnt1, count(*) as confTypeCnt2, 0 as confTypeCnt3, 0 as confTypeCnt4";
                    //Code Added for Organization Module
                    strSQL += " from Conf_Conference_D a where a.orgId=" + organizationID.ToString() + " and Conftype =2 and deleted = 0";
                    strSQL += " and month(confdate) = " + sLMt + " and year(confdate)=" + sCurYr + "";
                    strSQL += " group by conftype, (left(datename(month,confdate),3))";
                    strSQL += " Union";
                    strSQL += " select 0 as conftype1,0 as conftype2,conftype as conftype3,0 as conftype4,";

                    strSQL += " (left(datename(month,confdate),3)) M,";
                    strSQL += " 0 as confTypeCnt1, 0 as confTypeCnt2, count(*) as confTypeCnt3, 0 as confTypeCnt4";
                    //Code Added for Organization Module
                    strSQL += " from Conf_Conference_D a where a.orgId=" + organizationID.ToString() + " and Conftype =7 and deleted = 0";
                    strSQL += " and month(confdate) = " + sLMt + " and year(confdate)=" + sCurYr + "";

                    strSQL += " group by conftype, (left(datename(month,confdate),3))";
                    strSQL += " ) as a";
                    strSQL += " group by M order by M";
                }
                else if (sTWkS != "" && sTWkE != "" && sUsrId != "")
                {
                    strSQL = "select M,sum(conftype1) as conftype1, sum(conftype2) as conftype2, sum(conftype3) as conftype3,";
                    strSQL += " sum(conftype4) as conftype4, sum(confTypeCnt1) as confTypeCnt1, sum(confTypeCnt2) as confTypeCnt2,";
                    strSQL += " sum(confTypeCnt3) as confTypeCnt3, sum(confTypeCnt4) as confTypeCnt4 from";
                    strSQL += " (";
                    strSQL += " select conftype as conftype1,0 as conftype2,0 as conftype3,0 as conftype4,";
                    if (sDtFormat.Equals("dd/MM/yyyy"))
                        strSQL += " convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),103) M,";
                    else
                        strSQL += " convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),101) M,";
                    strSQL += " count(*) as confTypeCnt1, 0 as confTypeCnt2, 0 as confTypeCnt3, 0 as confTypeCnt4";
                    //Code Added for Organization Module
                    strSQL += " from Conf_Conference_D a where a.orgId=" + organizationID.ToString() + " and Conftype =6 and deleted = 0";
                    strSQL += " and dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) >= '" + sTWkS + " 00:00:00'";
                    strSQL += " and dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) <= '" + sTWkE + " 23:59:00'";
                    if (sDtFormat.Equals("dd/MM/yyyy"))
                        strSQL += " group by conftype, convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),103)";
                    else
                        strSQL += " group by conftype, convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),101)";
                    strSQL += " Union";
                    strSQL += " select 0 as conftype1,0 as conftype2,0 as conftype3,conftype as conftype4,";
                    if (sDtFormat.Equals("dd/MM/yyyy"))
                        strSQL += " convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),103) M,";
                    else
                        strSQL += " convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),101) M,";
                    strSQL += " 0 as confTypeCnt1, 0 as confTypeCnt2, 0 as confTypeCnt3, count(*) as confTypeCnt4";
                    //Code Added for Organization Module
                    strSQL += " from Conf_Conference_D a where a.orgId=" + organizationID.ToString() + " and Conftype =4 and deleted = 0";
                    strSQL += " and dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) >= '" + sTWkS + " 00:00:00'";
                    strSQL += " and dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) <= '" + sTWkE + " 23:59:00'";
                    if (sDtFormat.Equals("dd/MM/yyyy"))
                        strSQL += " group by conftype, convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),103)";
                    else
                        strSQL += " group by conftype, convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),101)";
                    strSQL += " Union";
                    strSQL += " select 0 as conftype1,conftype as conftype2,0 as conftype3,0 as conftype4,";
                    if (sDtFormat.Equals("dd/MM/yyyy"))
                        strSQL += " convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),103) M,";
                    else
                        strSQL += " convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),101) M,";
                    strSQL += " 0 as confTypeCnt1, count(*) as confTypeCnt2, 0 as confTypeCnt3, 0 as confTypeCnt4";
                    //Code Added for Organization Module
                    strSQL += " from Conf_Conference_D a where a.orgId=" + organizationID.ToString() + " and Conftype =2 and deleted = 0";
                    strSQL += " and dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) >= '" + sTWkS + " 00:00:00'";
                    strSQL += " and dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) <= '" + sTWkE + " 23:59:00'";
                    if (sDtFormat.Equals("dd/MM/yyyy"))
                        strSQL += " group by conftype, convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),103)";
                    else
                        strSQL += " group by conftype, convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),101)";
                    strSQL += " Union";
                    strSQL += " select 0 as conftype1,0 as conftype2,conftype as conftype3,0 as conftype4,";
                    if (sDtFormat.Equals("dd/MM/yyyy"))
                        strSQL += " convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),103) M,";
                    else
                        strSQL += " convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),101) M,";
                    strSQL += " 0 as confTypeCnt1, 0 as confTypeCnt2, count(*) as confTypeCnt3, 0 as confTypeCnt4";
                    //Code Added for Organization Module
                    strSQL += " from Conf_Conference_D a where a.orgId=" + organizationID.ToString() + " and Conftype =7 and deleted = 0";
                    strSQL += " and dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) >= '" + sTWkS + " 00:00:00'";
                    strSQL += " and dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) <= '" + sTWkE + " 23:59:00'";
                    if (sDtFormat.Equals("dd/MM/yyyy"))
                        strSQL += " group by conftype, convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),103)";
                    else
                        strSQL += " group by conftype, convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),101)";
                    strSQL += " ) as a";
                    strSQL += " group by M order by M";
                }
                else if (sYtoDS != "" && sYtoDE != "" && sUsrId != "")
                {
                    strSQL = "select M2,M,max(conftype1) as conftype1, max(conftype2) as conftype2, max(conftype3) as conftype3,";
                    strSQL += " max(conftype4) as conftype4, sum(confTypeCnt1) as confTypeCnt1, sum(confTypeCnt2) as confTypeCnt2,";
                    strSQL += " sum(confTypeCnt3) as confTypeCnt3, sum(confTypeCnt4) as confTypeCnt4 from";
                    strSQL += " (";
                    strSQL += "select M2,M1,M,sum(conftype1) as conftype1, sum(conftype2) as conftype2, sum(conftype3) as conftype3,";
                    strSQL += " sum(conftype4) as conftype4, sum(confTypeCnt1) as confTypeCnt1, sum(confTypeCnt2) as confTypeCnt2,";
                    strSQL += " sum(confTypeCnt3) as confTypeCnt3, sum(confTypeCnt4) as confTypeCnt4 from";
                    strSQL += " (";
                    strSQL += " select conftype as conftype1,0 as conftype2,0 as conftype3,0 as conftype4,";
                    if (sDtFormat.Equals("dd/MM/yyyy"))
                        strSQL += " convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),103) M1,";
                    else
                        strSQL += " convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),101) M1,";
                    strSQL += " (left(datename(month,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate)),3)) M,";
                    strSQL += " DATEPART(mm,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate)) M2,";
                    strSQL += " count(*) as confTypeCnt1, 0 as confTypeCnt2, 0 as confTypeCnt3, 0 as confTypeCnt4";
                    //Code Added for Organization Module
                    strSQL += " from Conf_Conference_D a where a.orgId=" + organizationID.ToString() + " and Conftype =6 and deleted = 0";
                    strSQL += " and dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) >= '" + sYtoDS + " 00:00:00'";
                    strSQL += " and dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) <= '" + sYtoDE + " 23:59:00'";
                    if (sDtFormat.Equals("dd/MM/yyyy"))
                        strSQL += " group by conftype, convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),103)";
                    else
                        strSQL += " group by conftype, convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),101)";
                    strSQL += ", (left(datename(month,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate)),3))";
                    strSQL += ", DATEPART(mm,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate)) ";
                    strSQL += " Union";
                    strSQL += " select 0 as conftype1,0 as conftype2,0 as conftype3,conftype as conftype4,";
                    if (sDtFormat.Equals("dd/MM/yyyy"))
                        strSQL += " convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),103) M1,";
                    else
                        strSQL += " convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),101) M1,";
                    strSQL += " (left(datename(month,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate)),3)) M,";
                    strSQL += " DATEPART(mm,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate)) M2,";
                    strSQL += " 0 as confTypeCnt1, 0 as confTypeCnt2, 0 as confTypeCnt3, count(*) as confTypeCnt4";
                    //Code Added for Organization Module
                    strSQL += " from Conf_Conference_D a where a.orgId=" + organizationID.ToString() + " and Conftype =4 and deleted = 0";
                    strSQL += " and dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) >= '" + sYtoDS + " 00:00:00'";
                    strSQL += " and dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) <= '" + sYtoDE + " 23:59:00'";
                    if (sDtFormat.Equals("dd/MM/yyyy"))
                        strSQL += " group by conftype, convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),103)";
                    else
                        strSQL += " group by conftype, convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),101)";
                    strSQL += ", (left(datename(month,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate)),3))";
                    strSQL += ", DATEPART(mm,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate)) ";
                    strSQL += " Union";
                    strSQL += " select 0 as conftype1,conftype as conftype2,0 as conftype3,0 as conftype4,";
                    if (sDtFormat.Equals("dd/MM/yyyy"))
                        strSQL += " convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),103) M1,";
                    else
                        strSQL += " convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),101) M1,";
                    strSQL += " (left(datename(month,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate)),3)) M,";
                    strSQL += " DATEPART(mm,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate)) M2,";
                    strSQL += " 0 as confTypeCnt1, count(*) as confTypeCnt2, 0 as confTypeCnt3, 0 as confTypeCnt4";
                    //Code Added for Organization Module
                    strSQL += " from Conf_Conference_D a where a.orgId=" + organizationID.ToString() + " and Conftype =2 and deleted = 0";
                    strSQL += " and dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) >= '" + sYtoDS + " 00:00:00'";
                    strSQL += " and dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) <= '" + sYtoDE + " 23:59:00'";
                    if (sDtFormat.Equals("dd/MM/yyyy"))
                        strSQL += " group by conftype, convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),103)";
                    else
                        strSQL += " group by conftype, convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),101)";
                    strSQL += ", (left(datename(month,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate)),3))";
                    strSQL += ", DATEPART(mm,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate)) ";
                    strSQL += " Union";
                    strSQL += " select 0 as conftype1,0 as conftype2,conftype as conftype3,0 as conftype4,";
                    if (sDtFormat.Equals("dd/MM/yyyy"))
                        strSQL += " convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),103) M1,";
                    else
                        strSQL += " convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),101) M1,";
                    strSQL += " (left(datename(month,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate)),3)) M,";
                    strSQL += " DATEPART(mm,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate)) M2,";
                    strSQL += " 0 as confTypeCnt1, 0 as confTypeCnt2, count(*) as confTypeCnt3, 0 as confTypeCnt4";
                    //Code Added for Organization Module
                    strSQL += " from Conf_Conference_D a where a.orgId=" + organizationID.ToString() + " and Conftype =7 and deleted = 0";
                    strSQL += " and dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) >= '" + sYtoDS + " 00:00:00'";
                    strSQL += " and dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) <= '" + sYtoDE + " 23:59:00'";
                    if (sDtFormat.Equals("dd/MM/yyyy"))
                        strSQL += " group by conftype, convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),103)";
                    else
                        strSQL += " group by conftype, convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),101)";
                    strSQL += ", (left(datename(month,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate)),3))";
                    strSQL += ", DATEPART(mm,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate)) ";
                    strSQL += " ) as a";
                    strSQL += " group by M2,M,M1) as b group by M,M2 order by M2";
                }
                else if (sCusFm != "" && sCusTo != "" && sUsrId != "")
                {
                    strSQL = "select M,sum(conftype1) as conftype1, sum(conftype2) as conftype2, sum(conftype3) as conftype3,";
                    strSQL += " sum(conftype4) as conftype4, sum(confTypeCnt1) as confTypeCnt1, sum(confTypeCnt2) as confTypeCnt2,";
                    strSQL += " sum(confTypeCnt3) as confTypeCnt3, sum(confTypeCnt4) as confTypeCnt4 from";
                    strSQL += " (";
                    strSQL += " select conftype as conftype1,0 as conftype2,0 as conftype3,0 as conftype4,";
                    if (sDtFormat.Equals("dd/MM/yyyy"))
                        strSQL += " convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),103) M,";
                    else
                        strSQL += " convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),101) M,";
                    strSQL += " count(*) as confTypeCnt1, 0 as confTypeCnt2, 0 as confTypeCnt3, 0 as confTypeCnt4";
                    //Code Added for Organization Module
                    strSQL += " from Conf_Conference_D a where a.orgId=" + organizationID.ToString() + " and Conftype =6 and deleted = 0";
                    strSQL += " and dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) >= '" + sCusFm + " 00:00:00'";
                    strSQL += " and dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) <= '" + sCusTo + " 23:59:00'";
                    if (sDtFormat.Equals("dd/MM/yyyy"))
                        strSQL += " group by conftype, convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),103)";
                    else
                        strSQL += " group by conftype, convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),101)";
                    strSQL += " Union";
                    strSQL += " select 0 as conftype1,0 as conftype2,0 as conftype3,conftype as conftype4,";
                    if (sDtFormat.Equals("dd/MM/yyyy"))
                        strSQL += " convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),103) M,";
                    else
                        strSQL += " convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),101) M,";
                    strSQL += " 0 as confTypeCnt1, 0 as confTypeCnt2, 0 as confTypeCnt3, count(*) as confTypeCnt4";
                    //Code Added for Organization Module
                    strSQL += " from Conf_Conference_D a where a.orgId=" + organizationID.ToString() + " and Conftype =4 and deleted = 0";
                    strSQL += " and dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) >= '" + sCusFm + " 00:00:00'";
                    strSQL += " and dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) <= '" + sCusTo + " 23:59:00'";
                    if (sDtFormat.Equals("dd/MM/yyyy"))
                        strSQL += " group by conftype, convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),103)";
                    else
                        strSQL += " group by conftype, convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),101)";
                    strSQL += " Union";
                    strSQL += " select 0 as conftype1,conftype as conftype2,0 as conftype3,0 as conftype4,";
                    if (sDtFormat.Equals("dd/MM/yyyy"))
                        strSQL += " convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),103) M,";
                    else
                        strSQL += " convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),101) M,";
                    strSQL += " 0 as confTypeCnt1, count(*) as confTypeCnt2, 0 as confTypeCnt3, 0 as confTypeCnt4";
                    //Code Added for Organization Module
                    strSQL += " from Conf_Conference_D a where a.orgId=" + organizationID.ToString() + " and Conftype =2 and deleted = 0";
                    strSQL += " and dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) >= '" + sCusFm + " 00:00:00'";
                    strSQL += " and dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) <= '" + sCusTo + " 23:59:00'";
                    if (sDtFormat.Equals("dd/MM/yyyy"))
                        strSQL += " group by conftype, convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),103)";
                    else
                        strSQL += " group by conftype, convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),101)";
                    strSQL += " Union";
                    strSQL += " select 0 as conftype1,0 as conftype2,conftype as conftype3,0 as conftype4,";
                    if (sDtFormat.Equals("dd/MM/yyyy"))
                        strSQL += " convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),103) M,";
                    else
                        strSQL += " convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),101) M,";
                    strSQL += " 0 as confTypeCnt1, 0 as confTypeCnt2, count(*) as confTypeCnt3, 0 as confTypeCnt4";
                    //Code Added for Organization Module
                    strSQL += " from Conf_Conference_D a where a.orgId=" + organizationID.ToString() + " and Conftype =7 and deleted = 0";
                    strSQL += " and dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) >= '" + sCusFm + " 00:00:00'";
                    strSQL += " and dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) <= '" + sCusTo + " 23:59:00'";
                    if (sDtFormat.Equals("dd/MM/yyyy"))
                        strSQL += " group by conftype, convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),103)";
                    else
                        strSQL += " group by conftype, convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),101)";
                    strSQL += " ) as a";
                    if (sDtFormat.Equals("dd/MM/yyyy"))
                        strSQL += " group by M order by convert(datetime,M,103)";
                    else
                        strSQL += " group by M order by convert(datetime,M,101)";
                }

                if (strSQL != "")
                    ds = m_rptLayer.ExecuteDataSet(strSQL);

                m_rptLayer.CloseConnection();


                if (ds != null)
                {
                    //if (ds.Tables[0].Rows.Count > 0)
                    //{
                    //DataSet dsX = ds; // export dataset to xml 
                    MemoryStream ms = new MemoryStream();
                    ds.WriteXml(ms);// extract string from MemoryStream
                    ms.Position = 0;
                    StreamReader sr = new StreamReader(ms, System.Text.Encoding.UTF8);
                    String strXml = sr.ReadToEnd();
                    obj.outXml = strXml;
                    sr.Close();
                    ms.Close();
                    //}
                }



            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
            return bRet;
        }
        #endregion

        #region GetSpecificUsageMonth
        public bool GetSpecificUsageMonth(ref vrmDataObject obj)
        {
            bool bRet = true;
            string sYst = "";
            string sLWkS = "";
            string sLWkE = "";
            string sLMt = "";
            string sTWkS = "";
            string sTWkE = "";
            string sYtoDS = "";
            string sYtoDE = "";
            string sCusFm = "";
            string sCusTo = "";
            string sConfTyp = "";
            string strSQL = "";
            string sDtFormat = "";
            string sUsrId = "";
            Int32 sCurYr = DateTime.Now.Year;
            m_rptLayer = new ns_SqlHelper.SqlHelper();
            DataSet ds = new DataSet();
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;

                node = xd.SelectSingleNode("//GetSpecificUsageMonth/Yesterday");
                if (node.InnerText != "")
                    sYst = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//GetSpecificUsageMonth/LastWeek/DateFrom");
                if (node.InnerText != "")
                    sLWkS = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//GetSpecificUsageMonth/LastWeek/DateTo");
                if (node.InnerText != "")
                    sLWkE = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//GetSpecificUsageMonth/LastMonth");
                if (node.InnerText != "")
                    sLMt = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//GetSpecificUsageMonth/ThisWeek/DateFrom");
                if (node.InnerText != "")
                    sTWkS = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//GetSpecificUsageMonth/ThisWeek/DateTo");
                if (node.InnerText != "")
                    sTWkE = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//GetSpecificUsageMonth/YearToDate/DateFrom");
                if (node.InnerText != "")
                    sYtoDS = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//GetSpecificUsageMonth/YearToDate/DateTo");
                if (node.InnerText != "")
                    sYtoDE = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//GetSpecificUsageMonth/CustomDate/DateFrom");
                if (node.InnerText != "")
                    sCusFm = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//GetSpecificUsageMonth/CustomDate/DateTo");
                if (node.InnerText != "")
                    sCusTo = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//GetSpecificUsageMonth/DateFormat");
                if (node.InnerText != "")
                    sDtFormat = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//GetSpecificUsageMonth/UserID");
                if (node.InnerText != "")
                    sUsrId = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//GetSpecificUsageMonth/ConferenceType");
                if (node.InnerText != "")
                    sConfTyp = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//GetSpecificUsageMonth/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;
                int.TryParse(orgid, out organizationID);  //Organization Module Fixes

                m_rptLayer.OpenConnection();

                if (sYst != "" && sConfTyp != "" && sUsrId != "")
                {
                    if (sDtFormat.Equals("dd/MM/yyyy"))
                        strSQL = "select convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),103) as D,count(*) as confTypeCnt1 from Conf_Conference_D";
                    else
                        strSQL = "select convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),101) as D,count(*) as confTypeCnt1 from Conf_Conference_D";
                    //Code Added for Organization Module
                    strSQL += " where orgId=" + organizationID.ToString() + " and Conftype=" + sConfTyp + " and dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) >= '" + sYst + " 00:00:00' and dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) <= '" + sYst + " 23:59:00' and deleted = 0";

                    if (sDtFormat.Equals("dd/MM/yyyy"))
                        strSQL += " group by convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),103)";
                    else
                        strSQL += " group by convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),101)";

                }
                else if (sLWkS != "" && sLWkE != "" && sConfTyp != "" && sUsrId != "")
                {
                    if (sDtFormat.Equals("dd/MM/yyyy"))
                        strSQL = "select convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),103) as D,count(*) as confTypeCnt1 from Conf_Conference_D";
                    else
                        strSQL = "select convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),101) as D,count(*) as confTypeCnt1 from Conf_Conference_D";
                    //Code Added for Organization Module
                    strSQL += " where orgId=" + organizationID.ToString() + " and Conftype=" + sConfTyp + " and dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) >= '" + sLWkS + " 00:00:00' and dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) <= '" + sLWkE + " 23:59:00' and deleted = 0";

                    if (sDtFormat.Equals("dd/MM/yyyy"))
                        strSQL += " group by convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),103)";
                    else
                        strSQL += " group by convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),101)";

                }
                else if (sLMt != "" && sConfTyp != "" && sUsrId != "")
                {
                    strSQL = "select (left(datename(month,confdate),3)) as M, count(*) as confTypeCnt1,month(confdate) as M1";

                    //Code Added for Organization Module
                    strSQL += " from Conf_Conference_D where orgId=" + organizationID.ToString() + " and Conftype = " + sConfTyp + " and deleted = 0 and Month(confdate) ='" + sLMt + "' and year(confdate) = '" + sCurYr + "'  group by datename(month,confdate),month(confdate)";
                    strSQL += " order by M1";
                }
                else if (sTWkS != "" && sTWkE != "" && sConfTyp != "" && sUsrId != "")
                {
                    if (sDtFormat.Equals("dd/MM/yyyy"))
                        strSQL = "select convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),103) as D,count(*) as confTypeCnt1 from Conf_Conference_D";
                    else
                        strSQL = "select convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),101) as D,count(*) as confTypeCnt1 from Conf_Conference_D";
                    //Code Added for Organization Module
                    strSQL += " where orgId=" + organizationID.ToString() + " and Conftype=" + sConfTyp + " and dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) >= '" + sTWkS + " 00:00:00' and dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) <= '" + sTWkE + " 23:59:00' and deleted = 0";

                    if (sDtFormat.Equals("dd/MM/yyyy"))
                        strSQL += " group by convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),103)";
                    else
                        strSQL += " group by convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),101)";
                }
                else if (sYtoDS != "" && sYtoDE != "" && sConfTyp != "" && sUsrId != "")
                {
                    strSQL = "select D,sum(confTypeCnt1) as confTypeCnt1,M2 from ( ";
                    if (sDtFormat.Equals("dd/MM/yyyy"))
                        strSQL += "select (left(datename(month,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate)),3)) D,DATEPART(mm,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate)) M2 ,convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),103) as M1 ,count(*) as confTypeCnt1 from Conf_Conference_D";
                    else
                        strSQL += "select (left(datename(month,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate)),3)) D,DATEPART(mm,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate)) M2 ,convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),101) as M1 ,count(*) as confTypeCnt1 from Conf_Conference_D";
                    strSQL += " where orgId=" + organizationID.ToString() + " and Conftype=" + sConfTyp + " and dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) >= '" + sYtoDS + " 00:00:00' and dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) <= '" + sYtoDE + " 23:59:00' and deleted = 0";

                    if (sDtFormat.Equals("dd/MM/yyyy"))
                        strSQL += " group by (left(datename(month,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate)),3)),DATEPART(mm,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate)), convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),103)) as b ";
                    else
                        strSQL += " group by (left(datename(month,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate)),3)),DATEPART(mm,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate)), convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),101)) as b ";

                    strSQL += " group by D,M2 order by M2,D";

                }
                else if (sCusFm != "" && sCusTo != "" && sConfTyp != "" && sUsrId != "")
                {
                    if (sDtFormat.Equals("dd/MM/yyyy"))
                        strSQL = "select convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),103) as D,count(*) as confTypeCnt1  from Conf_Conference_D";
                    else
                        strSQL = "select convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),101) as D,count(*) as confTypeCnt1 from Conf_Conference_D";
                    strSQL += " where orgId=" + organizationID.ToString() + " and  Conftype=" + sConfTyp + " and dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) >= '" + sCusFm + " 00:00:00' and dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) <= '" + sCusTo + " 23:59:00' and deleted = 0";

                    if (sDtFormat.Equals("dd/MM/yyyy"))
                        strSQL += " group by convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),103) order by convert(datetime,convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),103),103)";

                    else
                        strSQL += " group by convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),101) order by convert(datetime,convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),101),101)";

                }

                if (strSQL != "")
                    ds = m_rptLayer.ExecuteDataSet(strSQL);

                m_rptLayer.CloseConnection();


                if (ds != null)
                {
                    //if (ds.Tables[0].Rows.Count > 0)
                    //{
                    //DataSet dsX = ds; // export dataset to xml 
                    MemoryStream ms = new MemoryStream();
                    ds.WriteXml(ms);// extract string from MemoryStream
                    ms.Position = 0;
                    StreamReader sr = new StreamReader(ms, System.Text.Encoding.UTF8);
                    String strXml = sr.ReadToEnd();
                    obj.outXml = strXml;
                    sr.Close();
                    ms.Close();
                    //}
                }



            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
            return bRet;
        }
        #endregion

        #region GetLocationsUsage

        public bool GetLocationsUsage(ref vrmDataObject obj)
        {
            bool bRet = true;
            string sYst = "";
            string sLWkS = "";
            string sLWkE = "";
            string sLMt = "";
            string sTWkS = "";
            string sTWkE = "";
            string sYtoDS = "";
            string sYtoDE = "";
            string sCusFm = "";
            string sCusTo = "";
            string sCountry = "";
            string sZipCode = "";
            string strSQL = "";
            string sDtFormat = "";
            string sUsrId = "";
            string sState = "";
            string roomslist = "";
            
            Int32 sCurDate = DateTime.Now.Year;
            m_rptLayer = new ns_SqlHelper.SqlHelper();
            DataSet ds = new DataSet();
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;

                node = xd.SelectSingleNode("//GetLocationsUsage/Yesterday");
                if (node.InnerText != "")
                    sYst = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//GetLocationsUsage/LastWeek/DateFrom");
                if (node.InnerText != "")
                    sLWkS = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//GetLocationsUsage/LastWeek/DateTo");
                if (node.InnerText != "")
                    sLWkE = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//GetLocationsUsage/LastMonth");
                if (node.InnerText != "")
                    sLMt = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//GetLocationsUsage/ThisWeek/DateFrom");
                if (node.InnerText != "")
                    sTWkS = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//GetLocationsUsage/ThisWeek/DateTo");
                if (node.InnerText != "")
                    sTWkE = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//GetLocationsUsage/YearToDate/DateFrom");
                if (node.InnerText != "")
                    sYtoDS = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//GetLocationsUsage/YearToDate/DateTo");
                if (node.InnerText != "")
                    sYtoDE = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//GetLocationsUsage/CustomDate/DateFrom");
                if (node.InnerText != "")
                    sCusFm = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//GetLocationsUsage/CustomDate/DateTo");
                if (node.InnerText != "")
                    sCusTo = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//GetLocationsUsage/DateFormat");
                if (node.InnerText != "")
                    sDtFormat = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//GetLocationsUsage/UserID");
                if (node.InnerText != "")
                    sUsrId = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//GetLocationsUsage/Country");
                if (node.InnerText != "")
                    sCountry = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//GetLocationsUsage/State");
                if (node.InnerText != "")
                    sState = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//GetLocationsUsage/Zipcode");
                if (node.InnerText != "")
                    sZipCode = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//GetLocationsUsage/roomslist");                
                if (node != null && node.InnerText != "")
                    roomslist = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//GetLocationsUsage/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;
                int.TryParse(orgid, out organizationID);  //Organization Module Fixes


                m_rptLayer.OpenConnection();

                if (sYst != "")
                {
                    if (sDtFormat.Equals("dd/MM/yyyy"))
                        strSQL = "select convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),103) as D, c.Name as RoomName, count(*) as cnt";
                    else
                        strSQL = "select convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),101) as D, c.Name as RoomName, count(*) as cnt";
                    strSQL += " from Conf_Conference_D a, Conf_Room_D b, Loc_Room_D c";
                    //Code Added for Organization Module
                    strSQL += " where a.orgId=" + organizationID.ToString() + " and a.ConfID = b.ConfID and b.RoomID = c.RoomID and a.deleted = 0";
                    // Added for Location Filter
                    if (roomslist != "")
                        strSQL += " AND c.roomid IN (" + roomslist + ") ";
                    if (sZipCode != "")
                        strSQL += " and c.ZipCode like '% " + sZipCode + "%'";
                    if (sCountry != "")
                        strSQL += " and c.Country = " + sCountry + "";
                    if (sState != "")
                        strSQL += " and c.State = " + sState + "";
                    strSQL += " and dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) >= '" + sYst + " 00:00:00' and dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) <= '" + sYst + " 23:59:00'";

                    if (sDtFormat.Equals("dd/MM/yyyy"))
                        strSQL += " group by convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),103),c.Name order by convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),103),c.Name";

                    else
                        strSQL += " group by convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),101),c.Name order by convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),101),c.Name";

                }
                else if (sLWkS != "" && sLWkE != "")
                {
                    if (sDtFormat.Equals("dd/MM/yyyy"))
                        strSQL = "select convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),103) as D, c.Name as RoomName, count(*) as cnt";
                    else
                        strSQL = "select convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),101) as D, c.Name as RoomName, count(*) as cnt";
                    strSQL += " from Conf_Conference_D a, Conf_Room_D b, Loc_Room_D c";
                    strSQL += " where a.orgId=" + organizationID.ToString() + " and  a.ConfID = b.ConfID and b.RoomID = c.RoomID and a.deleted = 0";
                    // Added for Location Filter
                    if (roomslist != "")
                        strSQL += " AND c.roomid IN (" + roomslist + ") ";
                    if (sZipCode != "")
                        strSQL += " and c.ZipCode like '% " + sZipCode + "%'";
                    if (sCountry != "")
                        strSQL += " and c.Country = " + sCountry + "";
                    if (sState != "")
                        strSQL += " and c.State = " + sState + "";
                    strSQL += " and dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) >= '" + sLWkS + " 00:00:00' and dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) <= '" + sLWkE + " 23:59:00'";

                    if (sDtFormat.Equals("dd/MM/yyyy"))
                        strSQL += " group by convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),103),c.Name order by convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),103),c.Name";

                    else
                        strSQL += " group by convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),101),c.Name order by convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),101),c.Name";

                }
                else if (sLMt != "")
                {
                    strSQL = "select (left(datename(month,confdate),3)) as D, c.Name as RoomName, count(*) as cnt";
                    strSQL += " from Conf_Conference_D a, Conf_Room_D b, Loc_Room_D c";
                    strSQL += " where a.orgId=" + organizationID.ToString() + " and  a.ConfID = b.ConfID and b.RoomID = c.RoomID and a.deleted = 0";
                    strSQL += " and month(confdate)=" + sLMt + " and year(confdate)=" + sCurDate + "";
                    // Added for Location Filter
                    if (roomslist != "")
                        strSQL += " AND c.roomid IN (" + roomslist + ") ";
                    if (sZipCode != "")
                        strSQL += " and c.ZipCode like '%" + sZipCode + "%'";
                    if (sCountry != "")
                        strSQL += " and c.Country = " + sCountry + "";
                    if (sState != "")
                        strSQL += " and c.State = " + sState + "";
                    strSQL += " group by datename(month,confdate),c.Name";
                }
                else if (sTWkS != "" && sTWkE != "")
                {
                    if (sDtFormat.Equals("dd/MM/yyyy"))
                        strSQL = "select convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),103) as D, c.Name as RoomName, count(*) as cnt";
                    else
                        strSQL = "select convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),101) as D, c.Name as RoomName, count(*) as cnt";
                    strSQL += " from Conf_Conference_D a, Conf_Room_D b, Loc_Room_D c";
                    strSQL += " where a.orgId=" + organizationID.ToString() + " and  a.ConfID = b.ConfID and b.RoomID = c.RoomID and a.deleted = 0";
                    // Added for Location Filter
                    if (roomslist != "")
                        strSQL += " AND c.roomid IN (" + roomslist + ") ";
                    if (sZipCode != "")
                        strSQL += " and c.ZipCode like '% " + sZipCode + "%'";
                    if (sCountry != "")
                        strSQL += " and c.Country = " + sCountry + "";
                    if (sState != "")
                        strSQL += " and c.State = " + sState + "";
                    strSQL += " and dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) >= '" + sTWkS + " 00:00:00' and dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) <= '" + sTWkE + " 23:59:00'";

                    if (sDtFormat.Equals("dd/MM/yyyy"))
                        strSQL += " group by convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),103),c.Name order by convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),103),c.Name";

                    else
                        strSQL += " group by convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),101),c.Name order by convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),101),c.Name";
                }
                else if (sYtoDS != "" && sYtoDE != "")
                {
                    strSQL = "select D, sum(cnt) as cnt,RoomName,M2 from (";
                    if (sDtFormat.Equals("dd/MM/yyyy"))
                        strSQL += "select (left(datename(month,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate)),3)) D,DATEPART(mm,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate)) M2 ,convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),103) as M1, c.Name as RoomName, count(*) as cnt";
                    else
                        strSQL += "select (left(datename(month,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate)),3)) D,DATEPART(mm,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate)) M2 ,convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),101) as M1, c.Name as RoomName, count(*) as cnt";
                    strSQL += " from Conf_Conference_D a, Conf_Room_D b, Loc_Room_D c";
                    //Code Added for Organization Module
                    strSQL += " where a.orgId=" + organizationID.ToString() + " and  a.ConfID = b.ConfID and b.RoomID = c.RoomID and a.deleted = 0";
                    // Added for Location Filter
                    if (roomslist != "")
                        strSQL += " AND c.roomid IN (" + roomslist + ") ";
                    if (sZipCode != "")
                        strSQL += " and c.ZipCode like '% " + sZipCode + "%'";
                    if (sCountry != "")
                        strSQL += " and c.Country = " + sCountry + "";
                    if (sState != "")
                        strSQL += " and c.State = " + sState + "";
                    strSQL += " and dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) >= '" + sYtoDS + " 00:00:00' and dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) <= '" + sYtoDE + " 23:59:00'";

                    if (sDtFormat.Equals("dd/MM/yyyy"))
                        strSQL += " group by (left(datename(month,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate)),3)),DATEPART(mm,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate)), convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),103), c.Name) as b ";
                    else
                        strSQL += " group by (left(datename(month,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate)),3)),DATEPART(mm,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate)), convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),101), c.Name) as b ";

                    strSQL += " group by RoomName,D,M2 order by M2,D";
                }
                else if (sCusFm != "" && sCusTo != "")
                {
                    if (sDtFormat.Equals("dd/MM/yyyy"))
                        strSQL = "select convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),103) as D, c.Name as RoomName, count(*) as cnt";
                    else
                        strSQL = "select convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),101) as D, c.Name as RoomName, count(*) as cnt";
                    strSQL += " from Conf_Conference_D a, Conf_Room_D b, Loc_Room_D c";
                    strSQL += " where a.orgId=" + organizationID.ToString() + " and   a.ConfID = b.ConfID and b.RoomID = c.RoomID and a.deleted = 0";
                    // Added for Location Filter
                    if (roomslist != "")
                        strSQL += " AND c.roomid IN (" + roomslist + ") ";
                    if (sZipCode != "")
                        strSQL += " and c.ZipCode like '% " + sZipCode + "%'";
                    if (sCountry != "")
                        strSQL += " and c.Country = " + sCountry + "";
                    if (sState != "")
                        strSQL += " and c.State = " + sState + "";
                    strSQL += " and dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) >= '" + sCusFm + " 00:00:00' and dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) <= '" + sCusTo + " 23:59:00'";

                    if (sDtFormat.Equals("dd/MM/yyyy"))
                        strSQL += " group by convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),103),c.Name order by convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),103),c.Name";

                    else
                        strSQL += " group by convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),101),c.Name order by convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate),101),c.Name";
                }


                if (strSQL != "")
                    ds = m_rptLayer.ExecuteDataSet(strSQL);

                m_rptLayer.CloseConnection();


                if (ds != null)
                {
                    //if (ds.Tables[0].Rows.Count > 0)
                    //{
                    //DataSet dsX = ds; // export dataset to xml 
                    MemoryStream ms = new MemoryStream();
                    ds.WriteXml(ms);// extract string from MemoryStream
                    ms.Position = 0;
                    StreamReader sr = new StreamReader(ms, System.Text.Encoding.UTF8);
                    String strXml = sr.ReadToEnd();
                    obj.outXml = strXml;
                    sr.Close();
                    ms.Close();
                    //}
                }
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
            return bRet;
        }
        #endregion

        #region GetMCUUsage
        public bool GetMCUUsage(ref vrmDataObject obj)
        {
            bool bRet = true;
            string sYst = "";
            string sLWkS = "";
            string sLWkE = "";
            string sLMt = "";
            string sTWkS = "";
            string sTWkE = "";
            string sYtoDS = "";
            string sYtoDE = "";
            string sCusFm = "";
            string sCusTo = "";
            string sBridgeId = "";
            string strSQL = "";
            string sDtFormat = "";
            string sUsrId = "";
            Int32 sCurYr = DateTime.Now.Year;
            m_rptLayer = new ns_SqlHelper.SqlHelper();
            DataSet ds = new DataSet();
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;

                node = xd.SelectSingleNode("//GetMCUUsage/Yesterday");
                if (node.InnerText != "")
                    sYst = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//GetMCUUsage/LastWeek/DateFrom");
                if (node.InnerText != "")
                    sLWkS = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//GetMCUUsage/LastWeek/DateTo");
                if (node.InnerText != "")
                    sLWkE = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//GetMCUUsage/LastMonth");
                if (node.InnerText != "")
                    sLMt = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//GetMCUUsage/ThisWeek/DateFrom");
                if (node.InnerText != "")
                    sTWkS = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//GetMCUUsage/ThisWeek/DateTo");
                if (node.InnerText != "")
                    sTWkE = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//GetMCUUsage/YearToDate/DateFrom");
                if (node.InnerText != "")
                    sYtoDS = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//GetMCUUsage/YearToDate/DateTo");
                if (node.InnerText != "")
                    sYtoDE = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//GetMCUUsage/CustomDate/DateFrom");
                if (node.InnerText != "")
                    sCusFm = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//GetMCUUsage/CustomDate/DateTo");
                if (node.InnerText != "")
                    sCusTo = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//GetMCUUsage/DateFormat");
                if (node.InnerText != "")
                    sDtFormat = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//GetMCUUsage/UserID");
                if (node.InnerText != "")
                    sUsrId = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//GetMCUUsage/BridgeID");
                if (node.InnerText != "")
                    sBridgeId = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//GetMCUUsage/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;
                int.TryParse(orgid, out organizationID);  //Organization Module Fixes


                m_rptLayer.OpenConnection();

                if (sBridgeId != "")
                {
                    if (sYst != "" && sUsrId != "")
                    {
                        if (sDtFormat.Equals("dd/MM/yyyy"))
                            strSQL = "select distinct convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate),103) as d1, videoprotocoltype, count(*) as mcucount";
                        else
                            strSQL = "select distinct convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate),101) as d1,  videoprotocoltype, count(*) as mcucount";

                        //Code Added for Organization Module
                        strSQL += " from conf_room_d a,(select distinct confid from COnf_COnference_D where orgId=" + organizationID.ToString() + " and";

                        strSQL += " dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) >= '" + sYst + " 00:00:00'";

                        strSQL += " and dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) <= '" + sYst + " 23:59:59' and  conftype  in(4,2,6)";

                        strSQL += " and deleted=0) c , gen_videoprotocol_s b";
                        strSQL += " where a.Confid = c.Confid  and a.defVideoProtocol = b.videoprotocolid and a.bridgeid = " + sBridgeId + "";

                        strSQL += " and dbo.GMTtouserPreferedtime(11,a.startdate) >= '" + sYst + " 00:00:00'";
                        strSQL += " and dbo.GMTtouserPreferedtime(11,a.startdate) <= '" + sYst + " 23:59:00'";
                        if (sDtFormat.Equals("dd/MM/yyyy"))
                        {
                            strSQL += " group by convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate),103),videoprotocoltype";
                            strSQL += " order by convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate),103),videoprotocoltype";
                        }
                        else
                        {
                            strSQL += " group by convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate),101),videoprotocoltype";
                            strSQL += " order by convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate),101),videoprotocoltype";

                        }

                    }
                    else if (sLWkS != "" && sLWkE != "" && sUsrId != "")
                    {
                        if (sDtFormat.Equals("dd/MM/yyyy"))
                            strSQL = "select distinct convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate),103) as d1, videoprotocoltype, count(*) as mcucount";
                        else
                            strSQL = "select distinct convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate),101) as d1, videoprotocoltype, count(*) as mcucount";

                        //Code Added for Organization Module
                        strSQL += " from conf_room_d a,(select distinct confid from COnf_COnference_D where orgId=" + organizationID.ToString() + " and";

                        strSQL += " dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) >= '" + sLWkS + " 00:00:00'";

                        strSQL += " and dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) <= '" + sLWkE + " 23:59:59' and  conftype  in(4,2,6)";

                        strSQL += " and deleted=0) c , gen_videoprotocol_s b";
                        strSQL += " where a.Confid = c.Confid  and a.defVideoProtocol = b.videoprotocolid and a.bridgeid = " + sBridgeId + "";

                        strSQL += " and dbo.GMTtouserPreferedtime(11,a.startdate) >= '" + sLWkS + " 00:00:00'";
                        strSQL += " and dbo.GMTtouserPreferedtime(11,a.startdate) <= '" + sLWkE + " 23:59:00'";
                        if (sDtFormat.Equals("dd/MM/yyyy"))
                        {
                            strSQL += " group by convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate),103),videoprotocoltype";
                            strSQL += " order by convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate),103),videoprotocoltype";
                        }
                        else
                        {
                            strSQL += " group by convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate),101),videoprotocoltype";
                            strSQL += " order by convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate),101),videoprotocoltype";

                        }

                    }
                    else if (sLMt != "" && sUsrId != "")
                    {
                        strSQL = "select distinct videoprotocoltype, count(*) as mcucount,(left(datename(month,startdate),3)) as d1";
                        //Code Added for Organization Module
                        strSQL += " from conf_room_d a,(select distinct confid from COnf_COnference_D where orgId=" + organizationID.ToString() + " and";
                        strSQL += " month(confdate) = " + sLMt + " and year(confdate)= " + sCurYr + " and  conftype  in(4,2,6) and deleted=0) c ,";
                        strSQL += " gen_videoprotocol_s b";
                        strSQL += " where a.Confid = c.Confid  and a.defVideoProtocol = b.videoprotocolid and a.bridgeid = " + sBridgeId + "";
                        strSQL += " and month(a.startdate) = " + sLMt + " and year(a.startdate)= " + sCurYr + "";
                        strSQL += " group by (left(datename(month,startdate),3)),videoprotocoltype";
                        strSQL += " order by (left(datename(month,startdate),3)),videoprotocoltype";

                    }
                    else if (sTWkS != "" && sTWkE != "" && sUsrId != "")
                    {
                        if (sDtFormat.Equals("dd/MM/yyyy"))
                            strSQL = "select distinct convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate),103) as d1, videoprotocoltype, count(*) as mcucount";
                        else
                            strSQL = "select distinct convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate),101) as d1, videoprotocoltype, count(*) as mcucount";

                        //Code Added for Organization Module
                        strSQL += " from conf_room_d a,(select distinct confid from COnf_COnference_D where orgId=" + organizationID.ToString() + " and";

                        strSQL += " dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) >= '" + sTWkS + " 00:00:00'";

                        strSQL += " and dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) <= '" + sTWkE + " 23:59:59' and  conftype  in(4,2,6)";

                        strSQL += " and deleted=0) c , gen_videoprotocol_s b";
                        strSQL += " where a.Confid = c.Confid  and a.defVideoProtocol = b.videoprotocolid and a.bridgeid = " + sBridgeId + "";

                        strSQL += " and dbo.GMTtouserPreferedtime(11,a.startdate) >= '" + sTWkS + " 00:00:00'";
                        strSQL += " and dbo.GMTtouserPreferedtime(11,a.startdate) <= '" + sTWkE + " 23:59:00'";
                        if (sDtFormat.Equals("dd/MM/yyyy"))
                        {
                            strSQL += " group by convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate),103),videoprotocoltype";
                            strSQL += " order by convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate),103),videoprotocoltype";
                        }
                        else
                        {
                            strSQL += " group by convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate),101),videoprotocoltype";
                            strSQL += " order by convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate),101),videoprotocoltype";

                        }
                    }
                    else if (sYtoDS != "" && sYtoDE != "" && sUsrId != "")
                    {
                        strSQL = "select d1,videoprotocoltype,sum(mcucount) as mcucount,M2 from (";
                        if (sDtFormat.Equals("dd/MM/yyyy"))
                            strSQL += "select distinct (left(datename(month,dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate)),3)) d1, videoprotocoltype, count(*) as mcucount,DATEPART(mm,dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate)) M2 ,convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate),103) as D";
                        else
                            strSQL += "select distinct (left(datename(month,dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate)),3)) d1, videoprotocoltype, count(*) as mcucount,DATEPART(mm,dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate)) M2 ,convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate),101) as D";

                        //Code Added for Organization Module
                        strSQL += " from conf_room_d a,(select distinct confid from COnf_COnference_D where orgId=" + organizationID.ToString() + " and";

                        strSQL += " dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) >= '" + sYtoDS + " 00:00:00'";

                        strSQL += " and dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) <= '" + sYtoDE + " 23:59:59' and  conftype  in(4,2,6)";

                        strSQL += " and deleted=0) c , gen_videoprotocol_s b";
                        strSQL += " where a.Confid = c.Confid  and a.defVideoProtocol = b.videoprotocolid and a.bridgeid = " + sBridgeId + "";

                        strSQL += " and dbo.GMTtouserPreferedtime(11,a.startdate) >= '" + sYtoDS + " 00:00:00'";
                        strSQL += " and dbo.GMTtouserPreferedtime(11,a.startdate) <= '" + sYtoDE + " 23:59:00'";
                        if (sDtFormat.Equals("dd/MM/yyyy"))
                        {
                            strSQL += " group by (left(datename(month,dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate)),3)),DATEPART(mm,dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate)), convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate),103),videoprotocoltype) as z ";
                            strSQL += " group by videoprotocoltype,d1,M2 order by M2,d1,videoprotocoltype";
                        }
                        else
                        {
                            strSQL += " group by (left(datename(month,dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate)),3)),DATEPART(mm,dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate)), convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate),101),videoprotocoltype) as z ";
                            strSQL += " group by videoprotocoltype,d1,M2 order by M2,d1,videoprotocoltype";

                        }
                    }
                    else if (sCusFm != "" && sCusTo != "" && sUsrId != "")
                    {
                        if (sDtFormat.Equals("dd/MM/yyyy"))
                            strSQL = "select distinct convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate),103) as d1, videoprotocoltype, count(*) as mcucount";
                        else
                            strSQL = "select distinct convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate),101) as d1, videoprotocoltype, count(*) as mcucount";

                        //Code Added for Organization Module
                        strSQL += " from conf_room_d a,(select distinct confid from COnf_COnference_D where orgId=" + organizationID.ToString() + " and";

                        strSQL += " dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) >= '" + sCusFm + " 00:00:00'";

                        strSQL += " and dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) <= '" + sCusTo + " 23:59:59' and  conftype  in(4,2,6)";

                        strSQL += " and deleted=0) c , gen_videoprotocol_s b";
                        strSQL += " where a.Confid = c.Confid  and a.defVideoProtocol = b.videoprotocolid and a.bridgeid = " + sBridgeId + "";

                        strSQL += " and dbo.GMTtouserPreferedtime(11,a.startdate) >= '" + sCusFm + " 00:00:00'";
                        strSQL += " and dbo.GMTtouserPreferedtime(11,a.startdate) <= '" + sCusTo + " 23:59:00'";
                        if (sDtFormat.Equals("dd/MM/yyyy"))
                        {
                            strSQL += " group by convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate),103),videoprotocoltype";
                            strSQL += " order by convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate),103),videoprotocoltype";
                        }
                        else
                        {
                            strSQL += " group by convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate),101),videoprotocoltype";
                            strSQL += " order by convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate),101),videoprotocoltype";

                        }
                    }
                }
                else
                {
                    //FB 1920 start
                    String bridgeQry = " b.bridgename ";
                    if (organizationID != 11)
                        bridgeQry = " case when b.isPublic = 1 then b.bridgename + '(*)' else b.bridgename end ";
                    
                    if (sYst != "" && sUsrId != "")
                    {
                        if (sDtFormat.Equals("dd/MM/yyyy"))
                            strSQL = "select distinct convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate),103) as d1, " + bridgeQry + " as BridgeName, count(*) as mcucount";
                        else
                            strSQL = "select distinct  " + bridgeQry + " as BridgeName, count(*) as mcucount,convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate),101) as d1";
                        //Code Added for Organization Module
                        strSQL += " from conf_room_d a,mcu_list_d b,(select distinct confid from COnf_COnference_D where orgId=" + organizationID.ToString() + " and";
                        strSQL += " dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) >= '" + sYst + " 00:00:00' and dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) <= '" + sYst + " 23:59:59' and  conftype  in(4,2,6)";
                        strSQL += " and deleted=0) c where b.deleted=0 and a.Confid = c.Confid";
                        strSQL += " and a.bridgeid=b.bridgeid and dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate) >= '" + sYst + " 00:00:00'";
                        strSQL += " and dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate) <= '" + sYst + " 23:59:00'";
                        if (sDtFormat.Equals("dd/MM/yyyy"))
                        {
                            strSQL += " group by convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate),103), " + bridgeQry ;
                            strSQL += " order by convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate),103), " + bridgeQry ;
                        }
                        else
                        {
                            strSQL += " group by convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate),101), " + bridgeQry;
                            strSQL += " order by convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate),101), " + bridgeQry;

                        }
                    }
                    else if (sLWkS != "" && sLWkE != "" && sUsrId != "")
                    {
                        if (sDtFormat.Equals("dd/MM/yyyy"))
                            strSQL = "select distinct convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate),103) as d1,  " + bridgeQry + " as BridgeName, count(*) as mcucount";
                        else
                            strSQL = "select distinct convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate),101) as d1,  " + bridgeQry +" as BridgeName, count(*) as mcucount";
                        //Code Added for Organization Module
                        strSQL += " from conf_room_d a,mcu_list_d b,(select distinct confid from COnf_COnference_D where orgId=" + organizationID.ToString() + " and";
                        strSQL += " dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) >= '" + sLWkS + " 00:00:00' and dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) <= '" + sLWkE + " 23:59:59' and  conftype  in(4,2,6)";
                        strSQL += " and deleted=0) c where b.deleted=0 and a.Confid = c.Confid";
                        strSQL += " and a.bridgeid=b.bridgeid and dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate) >= '" + sLWkS + " 00:00:00'";
                        strSQL += " and dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate) <= '" + sLWkE + " 23:59:00'";
                        if (sDtFormat.Equals("dd/MM/yyyy"))
                        {
                            strSQL += " group by convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate),103), " + bridgeQry;
                            strSQL += " order by convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate),103), " + bridgeQry;
                        }
                        else
                        {
                            strSQL += " group by convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate),101), " + bridgeQry;
                            strSQL += " order by convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate),101), " + bridgeQry;

                        }

                    }
                    else if (sLMt != "" && sUsrId != "")
                    {

                        strSQL = "select distinct (left(datename(month,a.startdate),3)) as d1,  " + bridgeQry +" as BridgeName, count(*) as mcucount";
                        //Code Added for Organization Module
                        strSQL += " from conf_room_d a,mcu_list_d b,(select distinct confid from COnf_COnference_D where orgId=" + organizationID.ToString() + " and";
                        strSQL += " month(confdate) = " + sLMt + " and year(confdate) = " + sCurYr + " and  conftype  in(4,2,6)";
                        strSQL += " and deleted=0) c where b.deleted=0 and a.Confid = c.Confid";
                        strSQL += " and a.bridgeid=b.bridgeid and month(a.startdate) = " + sLMt + " and year(a.startdate) = " + sCurYr + "";
                        strSQL += " group by (left(datename(month,a.startdate),3)), " + bridgeQry;
                        strSQL += " order by (left(datename(month,a.startdate),3)), " + bridgeQry;

                    }
                    else if (sTWkS != "" && sTWkE != "" && sUsrId != "")
                    {
                        if (sDtFormat.Equals("dd/MM/yyyy"))
                            strSQL = "select distinct convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate),103) as d1,  " + bridgeQry +" as BridgeName, count(*) as mcucount";
                        else
                            strSQL = "select distinct convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate),101) as d1,  " + bridgeQry +" as BridgeName, count(*) as mcucount";
                        //Code Added for Organization Module
                        strSQL += " from conf_room_d a,mcu_list_d b,(select distinct confid from COnf_COnference_D where orgId=" + organizationID.ToString() + " and";
                        strSQL += " dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) >= '" + sTWkS + " 00:00:00' and dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) <= '" + sTWkE + " 23:59:59' and  conftype  in(4,2,6)";
                        strSQL += " and deleted=0) c where b.deleted=0 and a.Confid = c.Confid";
                        strSQL += " and a.bridgeid=b.bridgeid and dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate) >= '" + sTWkS + " 00:00:00'";
                        strSQL += " and dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate) <= '" + sTWkE + " 23:59:00'";
                        if (sDtFormat.Equals("dd/MM/yyyy"))
                        {
                            strSQL += " group by convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate),103), " + bridgeQry;
                            strSQL += " order by convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate),103), " + bridgeQry;
                        }
                        else
                        {
                            strSQL += " group by convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate),101), " + bridgeQry;
                            strSQL += " order by convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate),101), " + bridgeQry;

                        }
                    }
                    else if (sYtoDS != "" && sYtoDE != "" && sUsrId != "")
                    {
                        strSQL = "select d1,BridgeName, sum(mcucount) as mcucount,m2 from(";
                        if (sDtFormat.Equals("dd/MM/yyyy"))
                            strSQL += "select distinct (left(datename(month,dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate)),3)) d1,  " + bridgeQry +" as BridgeName, count(*) as mcucount,DATEPART(mm,dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate)) M2 ,convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate),103) as D";
                        else
                            strSQL += "select distinct (left(datename(month,dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate)),3)) d1,  " + bridgeQry +" as BridgeName, count(*) as mcucount,DATEPART(mm,dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate)) M2 ,convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate),101) as D";

                        //Code Added for Organization Module
                        strSQL += " from conf_room_d a,mcu_list_d b,(select distinct confid from COnf_COnference_D where orgId=" + organizationID.ToString() + " and";
                        strSQL += " dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) >= '" + sYtoDS + " 00:00:00' and dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) <= '" + sYtoDE + " 23:59:59' and  conftype  in(4,2,6)";
                        strSQL += " and deleted=0) c, gen_videoprotocol_s d where b.deleted=0 and a.Confid = c.Confid and a.defVideoProtocol = d.videoprotocolid";
                        strSQL += " and a.bridgeid=b.bridgeid and dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate) >= '" + sYtoDS + " 00:00:00'";
                        strSQL += " and dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate) <= '" + sYtoDE + " 23:59:00'";
                        if (sDtFormat.Equals("dd/MM/yyyy"))
                        {
                            strSQL += " group by (left(datename(month,dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate)),3)),DATEPART(mm,dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate)), convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate),103), " + bridgeQry +" ) as z ";
                            strSQL += " group by BridgeName,d1,M2 order by M2,d1,BridgeName";
                        }
                        else
                        {
                            strSQL += " group by (left(datename(month,dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate)),3)),DATEPART(mm,dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate)), convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate),101), " + bridgeQry +" ) as z ";
                            strSQL += " group by BridgeName,d1,M2 order by M2,d1,BridgeName";

                        }
                    }
                    else if (sCusFm != "" && sCusTo != "" && sUsrId != "")
                    {
                        if (sDtFormat.Equals("dd/MM/yyyy"))
                            strSQL = "select convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate),103) as d1,  " + bridgeQry +" as BridgeName, count(*) as mcucount";
                        else
                            strSQL = "select  " + bridgeQry +" as BridgeName, count(*) as mcucount,convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate),101) as d1";
                        //Code Added for Organization Module
                        strSQL += " from conf_room_d a,mcu_list_d b,(select distinct confid from COnf_COnference_D where orgId=" + organizationID.ToString() + " and";
                        strSQL += " dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) >= '" + sCusFm + " 00:00:00' and dbo.GMTtouserPreferedtime(" + sUsrId + ",confdate) <= '" + sCusTo + " 23:59:59' and  conftype  in(4,2,6)";
                        strSQL += " and deleted=0) c where b.deleted=0 and a.Confid = c.Confid";
                        strSQL += " and a.bridgeid=b.bridgeid and dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate) >= '" + sCusFm + " 00:00:00'";
                        strSQL += " and dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate) <= '" + sCusTo + " 23:59:00'";
                        if (sDtFormat.Equals("dd/MM/yyyy"))
                        {
                            strSQL += " group by convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate),103), " + bridgeQry;
                            strSQL += " order by convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate),103), " + bridgeQry;
                        }
                        else
                        {
                            strSQL += " group by convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate),101), " + bridgeQry;
                            strSQL += " order by convert(char,dbo.GMTtouserPreferedtime(" + sUsrId + ",a.startdate),101), " + bridgeQry;
                        }
                    }
                }

                //FB 1920 end
                //case when admin = 13 then bridgename + '*' else bridgename end as brdname //FB 1920
                if (strSQL != "")
                    ds = m_rptLayer.ExecuteDataSet(strSQL);

                m_rptLayer.CloseConnection();


                if (ds != null)
                {
                    //if (ds.Tables[0].Rows.Count > 0)
                    //{
                    //DataSet dsX = ds; // export dataset to xml 
                    MemoryStream ms = new MemoryStream();
                    ds.WriteXml(ms);// extract string from MemoryStream
                    ms.Position = 0;
                    StreamReader sr = new StreamReader(ms, System.Text.Encoding.UTF8);
                    String strXml = sr.ReadToEnd();
                    obj.outXml = strXml;
                    sr.Close();
                    ms.Close();
                    //}
                }



            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
            return bRet;
        }
        #endregion

        //Added for Graphical Reports END

        //FB 1750 - Start

        #region SelectCalendarDetails
        /// <summary>        
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>

        private String SelectCalendarDetails()
        {
            string stmt = "";
            try
            {
                stmt = "SelectCalendarDetails @timezone='" + timezone + "'";
                if (starttime.Trim() != "")
                    stmt += ",@startTime='" + Convert.ToDateTime(starttime) + "'";

                if (endtime.Trim() != "")
                    stmt += ",@endTime='" + Convert.ToDateTime(endtime) + "'";

                if (codeType != "")
                {
                    stmt += ",@codeType =" + Convert.ToInt32(codeType) + "";

                    if (entityCode != "")
                        stmt += ",@specificEntityCode='" + entityCode + "'";
                }

                if (tableName != "")
                    stmt += ",@tableName='" + tableName + "'";

                if(dtFormat != "")
                    stmt += ",@dtFormat='" + dtFormat + "'";

                if (tFormat != "")
                    stmt += ",@tformat='" + tFormat + "'";

                return stmt;
            }
            catch (Exception ex)
            {
                m_log.Error("sytemException", ex);
                return "";
            }
        }
		//FB 2410
        private void SelectCalendarDetails(ref StringBuilder strSQL, String startDate, String endDate)
        {
            try
            {
                strSQL.Append("SelectCalendarDetails @timezone='" + timezone + "'");
                if (startDate.Trim() != "")
                    strSQL.Append(",@startTime='" + Convert.ToDateTime(startDate) + "'");

                if (endDate.Trim() != "")
                    strSQL.Append(",@endTime='" + Convert.ToDateTime(endDate) + "'");

                strSQL.Append(",@codeType =1");
            }
            catch (Exception ex)
            {
                m_log.Error("sytemException", ex);
            }
        }

        #endregion

        #region GetEntityCodes

        private String GetEntityCodes()
        {
            string stmt = "";
            try
            {
                stmt = "GetEntityCodes";             

                return stmt;
            }
            catch (Exception ex)
            {
                m_log.Error("sytemException", ex);
                return "";
            }
        }

        #endregion

        #region CreateRoomSqlTable

        private String CreateRoomStructure(String inXML)
        {
            string stmt = "";
            XmlDocument xmlDstDoc = new XmlDocument();
            DataTable roomTable = null;
            
            DataSet ds = null;
            try
            {
                xmlDstDoc.LoadXml(inXML);
                ds = new DataSet();
                ds.ReadXml(new XmlNodeReader(xmlDstDoc.SelectSingleNode("//report/rooms")));

                if (ds.Tables.Count > 1)
                    roomTable = ds.Tables[1];

                if (roomTable != null)
                {
                    if (roomTable.Rows.Count > 0)
                    {                     
                        string connectionString = string.Empty;
                        connectionString = GetConnectionString(configpath);

                        if (CreateRoomSqlTable(tableName))
                        {
                            using (System.Data.SqlClient.SqlBulkCopy sbc = new System.Data.SqlClient.SqlBulkCopy(connectionString))
                            {
                                sbc.DestinationTableName = "[" + tableName + "]";
                                sbc.BatchSize = roomTable.Rows.Count;
                                sbc.ColumnMappings.Add("RoomID", "RoomID");
                                sbc.WriteToServer(roomTable);
                                sbc.Close();
                            }
                        }
                        else
                            stmt = "Error in Room Selection";
                    }
                }

                return stmt;
            }
            catch (Exception ex)
            {
                m_log.Error("sytemException", ex);
                return "";
            }
        }

        #endregion

        #region CreateRoomSqlTable

        private Boolean CreateRoomSqlTable(String tableName)
        {
            Boolean isTableCreated = true;
            String sqlInsert = "";            
            try
            {
                sqlInsert = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[" + tableName + "]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)";
                sqlInsert += " drop Table [dbo].[" + tableName + "]";
                sqlInsert += " Create Table [dbo].[" + tableName + "]([RoomID] [int] NOT NULL)";

                if (m_rptLayer == null)
                    m_rptLayer = new ns_SqlHelper.SqlHelper(configpath);

                m_rptLayer.OpenConnection();

                m_rptLayer.ExecuteNonQuery(sqlInsert);

            }
            catch
            {
                isTableCreated = false;
            }
            finally
            {
                if (m_rptLayer != null)
                    m_rptLayer.CloseConnection();
            }
            return isTableCreated;
        }

        #endregion

        #region GetConnectionString

        private String GetConnectionString(String path)
        {

            string _connectionString = string.Empty;
            CommandType _commandType;
            try
            {
                string configPath = path;
                _commandType = CommandType.Text;

                if (_connectionString == string.Empty)
                {
                    string xml = configPath + "app.config.xml";

                    XmlTextReader textReader = new XmlTextReader(xml);
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(textReader);

                    XmlNodeList NodeList = xmlDoc.GetElementsByTagName("session-factory");

                    foreach (XmlNode xnode in NodeList)
                    {
                        foreach (XmlNode xnod in xnode.ChildNodes)
                        {
                            if (xnod.LocalName == "property")
                            {
                                if (xnod.Attributes["name"].Value == "connection.connection_string")
                                {
                                    _connectionString = xnod.InnerText;
                                    break;
                                }
                            }
                        }
                    }
                }
                return _connectionString;
            }
            catch (Exception ex)
            {
                m_log.Error("sytemException", ex);
                return "";
            }
        }

        #endregion

        #region DeleteTmpUtilizationTable

        private Boolean DeleteTmpUtilizationTable()
        {
            string stmt = "";
            try
            {
                Boolean isTableCreated = true;
                String sqlInsert = "";
                try
                {
                    sqlInsert = " drop Table [dbo].[" + tableName + "]";

                    if (m_rptLayer == null)
                        m_rptLayer = new ns_SqlHelper.SqlHelper(configpath);

                    m_rptLayer.OpenConnection();

                    m_rptLayer.ExecuteNonQuery(sqlInsert);
                }
                catch
                {
                    isTableCreated = false;
                }
                finally
                {
                    if (m_rptLayer != null)
                        m_rptLayer.CloseConnection();
                }
                return isTableCreated;

            }
            catch (Exception ex)
            {
                m_log.Error("sytemException", ex);
                return false;
            }
        }

        #endregion

        //FB 1750 - End

        //FB 1969 - Start

        #region FetchUserLogin
        /// <summary>        
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>

        public bool FetchUserLogin(ref vrmDataObject obj)
        {
            string stmt = "";
            string userID = "";
            bool fet = true;
            MemoryStream ms = null;
            StreamReader sr = null;
            List<ICriterion> criterionList = new List<ICriterion>();
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                reportUserID = "11";

                node = xd.SelectSingleNode("//FetchAllUsers/Users/UserID");
                if (node != null)
                    userID = node.InnerXml.Trim();

                if (userID != "")
                    reportUserID = userID;

                node = xd.SelectSingleNode("//FetchAllUsers/Users/organizationID");

                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;
                int.TryParse(orgid, out organizationID);

                node = xd.SelectSingleNode("//FetchAllUsers/Users/FirstName");
                if (node != null)
                    firstname = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//FetchAllUsers/Users/LastName");
                if (node != null)
                    lastname = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//FetchAllUsers/Users/Email");
                if (node != null)
                    email = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//FetchAllUsers/Users/FromDate");
                if (node != null)
                    fromdate = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//FetchAllUsers/Users/ToDate");
                if (node != null)
                    todate = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//FetchAllUsers/Users/sortBy");
                if (node != null)
                    SortBy = node.InnerXml.Trim();


                stmt = UserDetails();

                if (m_usrDAO != null)
                    m_usrLayer = new ns_SqlHelper.SqlHelper(configpath);

                m_usrLayer.CloseConnection();

                DataSet ds = null;
                String strXml = "";

                if (stmt != "")
                {
                    ds = m_usrLayer.ExecuteDataSet(stmt);
                }
                else
                {
                    DataTable tt = new DataTable();
                }

                if (ds != null)
                {
                    if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        ms = new MemoryStream();
                        ds.WriteXml(ms);
                        ms.Position = 0;
                        sr = new StreamReader(ms, System.Text.Encoding.UTF8);
                        strXml = sr.ReadToEnd();
                        sr.Close();
                        ms.Close();
                    }
                    obj.outXml = strXml;
                }
            }
            catch (Exception ex)
            {
                m_log.Error("sytemException", ex);
                fet = false;
            }

            finally
            {
                if (sr != null)
                    sr.Close();
                sr = null;

                if (ms != null)
                    ms.Close();
                ms = null;
            }

            return fet;
        }

        #endregion

        #region UserDetails
        /// <summary>
        /// UserDetails
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>

        public String UserDetails()
        {
            string stmt = "";
            string whereClause = "";
            try
            {
                if (email != "")
                {
                    whereClause += " and u.email like '%" + email + "%'";
                }

                if (firstname != "")
                {
                    whereClause += " and u.firstname like '%" + firstname + "%'";
                }

                if (lastname != "")
                {
                    whereClause += " and u.lastname like '%" + lastname + "%'";
                }

                if ((fromdate != "") && (todate != ""))
                {
                    whereClause += " and (l.logindate between '" + fromdate + " 00:00" + "' and '" + todate + " 23:59" + "') ";
                }
                //FB 2513 starts
                //stmt = "select u.firstname as FirstName,u.lastname as LastName,u.email as Email,dbo.GMTToUserPreferedTime(" + reportUserID + ",dbo.changeTOGMTtime(" + sysSettings.TimeZone + ",l.logindate)) as LoginDateTime ";
                stmt = "select u.firstname as FirstName,u.lastname as LastName,u.email as Email,dbo.GMTToUserPreferedTime(" + reportUserID + ",l.logindate) as LoginDateTime ";
                //FB 2513 ends
                stmt += " from sys_LoginAudit_d L, usr_list_d U where u.userid = l.userid " + whereClause + ""; //FB 3000
                if (SortBy == "1") // sort by first name
                {
                    stmt += " ORDER BY u.firstname desc";
                }
                else if (SortBy == "2") // sort by last name
                {
                    stmt += " ORDER BY u.lastname desc";
                }
                else if (SortBy == "3") // sort by login date/time
                {
                    stmt += " ORDER BY l.logindate desc";
                }
                else  // defalt sort by order id 
                {
                    stmt += "order by u.email, l.logindate desc";
                }


                return stmt;


            }
            catch (Exception ex)
            {
                m_log.Error("sytemException", ex);
                return "";
            }
        }
        #endregion

        //FB 1969 - End

        #region GetUsageReports
        public bool GetUsageReports(ref vrmDataObject obj)
        {
            bool bRet = true;
            String startDate = "";
            String endDate = "";
            String dtFormat = "";
            String tFormat = "";
            String userId = "";
            String rptType = "";
            String inputType = "";
            String inputValue = "";
            StringBuilder strSQL = null;
            Int32 sCurYr = DateTime.Now.Year;
            Int32 workingHours = 0;
            String strParam = "", pivotInput = "", selectedYear = "";
            //FB 2501 Starts
            String ConfName="";
            String ConfUniqueID="";
            String HostID="";
            String RequestorID="";
            String EndpointID="";
            String LocationID = "";
            String VNOCoperator = "";
            String CallURL = "";
            String QueryType = "";
            //FB 2501 Ends
			//FB 2410 - Start
            String ReportName = "";
            MemoryStream ms = null;
            StreamReader sr = null;
			//FB 2410 - End

            m_rptLayer = new ns_SqlHelper.SqlHelper();
            DataSet ds = new DataSet();
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//GetUsageReports/DateFrom");
                if (node != null && node.InnerText != "")
                    startDate = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//GetUsageReports/DateTo");
                if (node != null && node.InnerText != "")
                    endDate = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//GetUsageReports/DateFormat");
                if (node != null && node.InnerText != "")
                    dtFormat = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//GetUsageReports/TimeFormat"); //FB 2588
                if (node != null && node.InnerText != "")
                    tFormat = node.InnerXml.Trim();
                if (tFormat == "HHmmZ")
                    dtFormat = "HHmmZ";

                node = xd.SelectSingleNode("//GetUsageReports/UserID");
                if (node != null && node.InnerText != "")
                    userId = node.InnerXml.Trim();

                // CONF - Conf Usage , LOC - Locations, MCU - MCU
                node = xd.SelectSingleNode("//GetUsageReports/ReportType");
                if (node != null && node.InnerText != "")
                    rptType = node.InnerXml.Trim();

                // ALL-Conf All, CA-Conf Audio, CV-Conf Video, CR-Conf Room, CP-Conf pt to pt, 
                //RM - Room Id's, CS - Country/State, ZP - Zip, MCU
                node = xd.SelectSingleNode("//GetUsageReports/InputType");
                if (node != null && node.InnerText != "")
                    inputType = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//GetUsageReports/InputValue");
                if (node != null && node.InnerText != "")
                    inputValue = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//GetUsageReports/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;
                int.TryParse(orgid, out organizationID);

                node = xd.SelectSingleNode("//GetUsageReports/timezone");
                if (node != null)
                {
                    if (node.InnerXml.Trim().Length > 0)
                        timezone = node.InnerXml.Trim();
                }

                if (timezone == "" || timezone == "0") //ZD 101708
                    timezone = "26";

                //FB 2343
                node = xd.SelectSingleNode("//GetUsageReports/WorkingHours");
                if (node != null)
                    int.TryParse(node.InnerText, out workingHours);

                node = xd.SelectSingleNode("//GetUsageReports/SelectedYear");
                if (node != null)
                    selectedYear = node.InnerText;

                node = xd.SelectSingleNode("//GetUsageReports/strParam");
                if (node != null)
                    strParam = node.InnerText;

                node = xd.SelectSingleNode("//GetUsageReports/pivotInput");
                if (node != null)
                    pivotInput = node.InnerText;
                //FB 2501 starts
                node=xd.SelectSingleNode("//GetUsageReports/ConfTitle");
                if(node!=null)
                    ConfName=node.InnerText;
                node=xd.SelectSingleNode("//GetUsageReports/UniqueID");
                if(node!=null)
                    ConfUniqueID=node.InnerText;
                node=xd.SelectSingleNode("//GetUsageReports/OwnerID");
                if(node!=null)
                    HostID=node.InnerText;
                node=xd.SelectSingleNode("//GetUsageReports/RequestorID");
                if(node!=null)
                    RequestorID=node.InnerText;
                node=xd.SelectSingleNode("//GetUsageReports/EndpointID");
                if(node!=null)
                    EndpointID=node.InnerText;
                node = xd.SelectSingleNode("//GetUsageReports/LocationID");
                if (node != null)
                    LocationID = node.InnerText;
                node = xd.SelectSingleNode("//GetUsageReports/VNOCoperator");
                if(node!=null)
                    VNOCoperator = node.InnerText;
                node = xd.SelectSingleNode("//GetUsageReports/CallURL");
                if (node != null)
                    CallURL = node.InnerText;
                node=xd.SelectSingleNode("//GetUsageReports/QueryType");
                if (node != null)
                    QueryType = node.InnerText;
                //FB 2501 ends
                //FB 2410 start
                //export = "0";
                node = xd.SelectSingleNode("//GetUsageReports/Export");
                if (node != null)
                {
                    if (node.InnerXml.Trim().Length > 0)
                        export = node.InnerXml.Trim();
                }

                if (export == "2" && (rptType == "WUR" || rptType == "MUR"))
                {
                    GetUsageParams(ref strParam, ref pivotInput, ref selectedYear, startDate, endDate, workingHours, obj, organizationID.ToString(), userId, rptType);
                }

                node = xd.SelectSingleNode("//GetUsageReports/FileName");
                if (node != null)
                {
                    if (node.InnerXml.Trim().Length > 0)
                    {
                        fileName = node.InnerXml.Trim();
                    }
                }

                String emailAddress = "";
                node = xd.SelectSingleNode("//GetUsageReports/EmailAddress");
                if (node != null)
                {
                    if (node.InnerXml.Trim().Length > 0)
                        emailAddress = node.InnerXml.Trim();
                }

                Int32 id = 0;
                node = xd.SelectSingleNode("//GetUsageReports/ID");
                if (node != null)
                {
                    if (node.InnerXml.Trim().Length > 0)
                        id = Convert.ToInt32(node.InnerXml.Trim());
                }

                String jobName = "";
                node = xd.SelectSingleNode("//GetUsageReports/JobName");
                if (node != null)
                {
                    if (node.InnerXml.Trim().Length > 0)
                        jobName = node.InnerXml.Trim();
                }
                node = xd.SelectSingleNode("//GetUsageReports/ReportName");
                if (node != null)
                {
                    if (node.InnerXml.Trim().Length > 0)
                        ReportName = node.InnerXml.Trim();
                }
                //FB 2410 end
                //ALLDEV-498
                string delUserID = "";
                node = xd.SelectSingleNode("//GetUsageReports/DelUserID");
                if (node != null)
                {
                    if (node.InnerXml.Trim().Length > 0)
                        delUserID = node.InnerXml.Trim();
                }

                m_rptLayer.OpenConnection();
                strSQL = new StringBuilder();
                String strState = "";
                String rmId = "";
                String country = "";
                String zip = "";

                String stment = " Select a.roleid, a.admin, b.crossaccess FROM Usr_List_D a, Usr_Roles_D b where a.roleid = b.roleid AND userid = '" + userId + "'";

                DataSet dss = null;
                dss = m_rptLayer.ExecuteDataSet(stment);

                if (dss.Tables[0].Rows.Count > 0)
                {
                    usrRoleId = Convert.ToString(dss.Tables[0].Rows[0]["roleid"]);
                    usrAdmin = Convert.ToString(dss.Tables[0].Rows[0]["admin"]);
                    usrCrossAccess = Convert.ToString(dss.Tables[0].Rows[0]["crossaccess"]);
                }

                switch (rptType)
                {
                    case "CONF":
                        String confType = inputValue;
                        GetConferenceUsage(ref strSQL, startDate, endDate, userId, orgid, dtFormat, confType);
                        break;
                    case "LOC":

                        switch (inputType)
                        {
                            case "RM": rmId = inputValue; break;
                            case "CS": country = inputValue; break;
                            case "ZP": zip = inputValue; break;
                        }
                        GetRoomsUsage(ref strSQL, startDate, endDate, userId, orgid, dtFormat, rmId, country, zip);
                        break;
                    case "MCU":
                        String mcuName = inputValue;
                        GetMCUUsageReport(ref strSQL, startDate, endDate, userId, orgid, mcuName);
                        break;
                    case "DS":
                        DailyScheduleReport(ref strState, startDate, endDate, userId, orgid, dtFormat); //FB 2588
                        strSQL.Append(strState);
                        break;
                    case "PRI":
                        PRIAllocation(ref strState, startDate, endDate, orgid, dtFormat); //FB 2588
                        strSQL.Append(strState);
                        break;
                    case "RA":
                        ResourceAllocationReport(ref strState, startDate, endDate, orgid, dtFormat); //FB 2588
                        strSQL.Append(strState);
                        break;
                    case "CL":
                        ContactList(ref strState, orgid);
                        strSQL.Append(strState);
                        break;
                    case "AU":
                        AggregateUsageReport(ref strState, startDate, endDate, orgid, dtFormat); //FB 2588
                        strSQL.Append(strState);
                        break;
                    case "UR":
                        UsageByRoom(ref strState, startDate, endDate, orgid, dtFormat); //FB 2588
                        strSQL.Append(strState);
                        break;
                    case "WUR": //FB 2343
                        WeeklyUsagebyRoom(ref strSQL, strParam, pivotInput, workingHours, selectedYear, orgid);
                        break;
                    case "MUR":
                        MonthlyUsagebyRoom(ref strSQL, strParam, pivotInput, workingHours, selectedYear, orgid);
                        break;
                    case "CR":
                        if (inputValue != "" && rmId == "")
                            rmId = inputValue;
                        CalendarReport(ref strState, startDate, endDate, orgid, rmId, dtFormat);//FB 2588
                        strSQL.Append(strState);
                        break;
                    case "PERS"://FB 2155
                        String statusType = inputValue;
                        PersonalReport(ref strSQL, startDate, endDate, userId, orgid, dtFormat, statusType);
                        break;
                    case "USR":
                        UserReports(ref strSQL, userId, dtFormat, inputType, orgid);
                        break;
                    case "RR":
                        RoomReports(ref strSQL, inputType, orgid);
                        break;
                    case "ER":
                        EptReports(ref strSQL, orgid);
                        break;
                    case "MR":
                        MCUReports(ref strSQL, orgid);
                        break;
                    case "DSR":
                        RoomConfReports(ref strSQL, inputType, startDate, endDate, orgid, dtFormat);//FB 2588
                        break;
                    case "MY":
                    case "WY":
                        GetWorkingYears(ref strSQL, orgid, selectedYear);
                        break;
                    case "ES": //FB 2363
                        GetExchangeEvents(ref strSQL, startDate, endDate, userId, inputType, dtFormat);//FB 2588
                        break;
                    //FB 2501 Starts
                    case "CDD":
                        GetConferenceDetails(ref strSQL, startDate, endDate, timezone, userId, orgid, ConfName, HostID, RequestorID
                            , EndpointID, inputValue, LocationID, VNOCoperator, CallURL, QueryType, dtFormat); //FB 2588
                        break;
                    case "CUS":
                        GetConferenceUniqueID(ref strSQL, ConfUniqueID, orgid, userId, dtFormat);//FB 2588
                        break;
                    //FB 2501 Ends
                    //FB 2501 CDR Reports Starts
                    case "CDR":
                        String mcuId = inputValue;
                        GetCDRReports(ref strSQL, startDate, endDate, timezone, userId, orgid, mcuId, dtFormat);//FB 2588
                        break;
                    //FB 2501 CDR Reports Ends
                    //FB 2501 Ends
                    case "CD": //2410

                        String st = GetEntityCodes();
                        if (m_rptDAO != null)
                            m_rptLayer = new ns_SqlHelper.SqlHelper(configpath);
                        DataSet ds1 = null;

                        if (st != "")
                            ds1 = m_rptLayer.ExecuteDataSet(st);

                        if (ds1.Tables.Count > 0)
                        {
                            entityList = null;

                            foreach (DataRow row in ds1.Tables[0].Rows)
                            {
                                if (entityList == null)
                                    entityList = new ArrayList();

                                entityList.Add(row["EntityCode"].ToString());
                            }

                            if (!entityList.Contains("NA"))
                                entityList.Add("NA");

                            if (!entityList.Contains("ISDN"))
                                entityList.Add("ISDN");

                            if (!entityList.Contains("ALL"))
                                entityList.Add("ALL");
                        }

                        SelectCalendarDetails(ref strSQL, startDate, endDate);
                        break;
					//ZD 100456
                    case "DIR":
                        GetRooms(ref strSQL, orgid);
                        break;
                    case "DIU":
                        GetUsers(ref strSQL, orgid);
                        break;
                    case "DIE":
                        GetEndpoints(ref strSQL, orgid);
                        break;
                    case "DIM":
                        GetMCUList(ref strSQL, orgid);
                        break;
                    case "RH": //ZD 100902
                        GetReservationHistory(ref strSQL, ConfUniqueID, userId);
                        break;
                    case "CUR": //ZD 102468
                        CustomOptionReports(ref strSQL, userId, dtFormat, inputType, orgid, startDate, endDate);
                        break;
                    case "AUR": //ALLDEV-498
                        GetUserAssignedReports(ref strSQL, delUserID);
                        break;
                }

                if (strSQL.ToString() != "")
                    ds = m_rptLayer.ExecuteDataSet(strSQL.ToString());
				//ZD 100456
                if (rptType == "DIU" || rptType == "DIE" || rptType == "DIM")
                {
                    cryptography.Crypto crypto = new cryptography.Crypto();

                    if (ds != null && ds.Tables.Count > 0)
                    {
                        for (Int32 i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            if (rptType == "DIU")
                            {
                                if (!string.IsNullOrEmpty(ds.Tables[0].Rows[i]["Initial Password"].ToString()))
                                    ds.Tables[0].Rows[i]["Initial Password"] = crypto.decrypt(ds.Tables[0].Rows[i]["Initial Password"].ToString().Trim());
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(ds.Tables[0].Rows[i]["Password"].ToString()) && ds.Tables[0].Rows[i]["Password"].ToString() != "") //ZD 100877
                                    ds.Tables[0].Rows[i]["Password"] = crypto.decrypt(ds.Tables[0].Rows[i]["Password"].ToString().Trim());
                            }
                        }
                    }
                }

                m_rptLayer.CloseConnection();

				 if (export == "2")
                {
                    myVRM.ExcelXmlWriter.ExcelUtil exlUtil = new myVRM.ExcelXmlWriter.ExcelUtil();
                    DirectoryInfo dInfo = new DirectoryInfo(destDirectory);

                    if (!dInfo.Exists)
                        dInfo.Create();

                    String destFile = destDirectory + "\\" + fileName;

                    Hashtable loglist = new Hashtable();
                    loglist.Add("BatchReport", "Batch Report");
                    ds.Tables[0].TableName = "BatchReport";
                    if (rptType == "CD")
                        exlUtil.CreateXLFile(entityList, destFile, ds, null);//ZD 100288
                    else
                        exlUtil.CreateXLFile(loglist, destFile, ds);

                    m_vrmEmailFact = new emailFactory(ref obj);
                    if (m_vrmEmailFact.SendBatchreport(emailAddress,organizationID, ref destFile, jobName, startDate, endDate, ReportName)) //FB 2410
                    {
                        BatchReportDates btchRptDate = m_IBatchReportDatesDAO.GetById(id);

                        btchRptDate.Sent = 1;
                        btchRptDate.SentTime = DateTime.Now;
                        m_IBatchReportDatesDAO.Update(btchRptDate);
                    }
                    
                } //FB 2410 End
                if (ds != null)
                {
                    ms = new MemoryStream();
                    ds.WriteXml(ms);// extract string from MemoryStream
                    ms.Position = 0;
                    sr = new StreamReader(ms, System.Text.Encoding.UTF8);
                    String strXml = sr.ReadToEnd();
                    obj.outXml = strXml;
                    sr.Close();
                    ms.Close();
                }

                //ALLDEV-821 - Start
                String oXML = "";               

                if (obj.outXml == "<NewDataSet />")
                    oXML = "<NewDataSet>";

                for (Int32 i = 0; i < ds.Tables.Count; i++) 
                {
                    if (ds.Tables[i].Rows.Count <= 0)
                    {
                        oXML += "<Table";
                        if (i > 0)
                            oXML += i + ">";
                        else
                            oXML += ">";
                    
                        for (int c = 0; c < ds.Tables[i].Columns.Count; c++)
                        {
                            oXML += "<" + ds.Tables[i].Columns[c].ToString().Replace(" ", "_x0020_").Replace("#", "_x0023_").Replace("(", "_x0028_").Replace(")", "_x0029_").Replace("<", "_x003C_").Replace("/", "_x002F_") + " />"; //_x0020_  --> Space , _x0023_  --> #//FB 2272
                        }

                        oXML += "</Table";
                        if (i > 0)
                            oXML += i + ">";
                        else
                            oXML += ">";
                    }
                }

                if (obj.outXml == "<NewDataSet />")
                {
                    oXML += "</NewDataSet>";
                    obj.outXml = oXML;
                }
                else if (oXML != "")
                    obj.outXml = obj.outXml.Replace("</NewDataSet>", oXML + "</NewDataSet>");
                //ALLDEV-821 - End

            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
            finally
            {
                if (sr != null)
                    sr.Close();
                sr = null;

                if (ms != null)
                    ms.Close();
                ms = null;
            }
            return bRet;
        }
        #endregion

        #region GetConferenceUsage 

        private void GetConferenceUsage(ref StringBuilder strSQL, String startDate, String endDate, String userId, String orgid, String dtFormat, String confType)
        {
            try
            {
                strSQL.Append("select Category, SeriesName, Category as Category1,ReportsRange  from (");
                strSQL.Append("select case conftype when 6 then 'Audio Conferences' when 2 then 'Video Conferences' ");
                strSQL.Append(" when 7 then 'Room Conferences' when 8 then 'Hotdesking' when 4 then 'Point-to- Point' end as SeriesName,");//FB 2694

                if (dtFormat.Equals("dd/MM/yyyy"))
                    strSQL.Append(" convert(char,dbo.GMTtouserPreferedtime(" + userId + ",confdate),103) Category,");
                else if (dtFormat.Equals("HHmmZ"))//FB 2588
                    strSQL.Append(" convert(char,confdate,101) Category,");
                else
                    strSQL.Append(" convert(char,dbo.GMTtouserPreferedtime(" + userId + ",confdate),101) Category,");

                strSQL.Append( " count(*) as ReportsRange from Conf_Conference_D a  where a.orgId=" + orgid + " and deleted = 0");

                if (confType != "")
                    strSQL.Append( " and ConfType = " + confType);

                strSQL.Append( " and dbo.GMTtouserPreferedtime(" + userId + ",confdate) >= '" + startDate + "'");
                strSQL.Append( " and dbo.GMTtouserPreferedtime(" + userId + ",confdate) <= '" + endDate + "'");
                if (dtFormat.Equals("dd/MM/yyyy"))
                    strSQL.Append( " group by conftype,convert(char,dbo.GMTtouserPreferedtime(" + userId + ",confdate),103)");
                else if (dtFormat.Equals("HHmmZ"))//FB 2588
                    strSQL.Append(" group by conftype,convert(char,confdate,101)");
                else
                    strSQL.Append( " group by conftype,convert(char,dbo.GMTtouserPreferedtime(" + userId + ",confdate),101)");

                 strSQL.Append( ") as xx ");
                
                //ZD 101835
                strSQL.Append(" Union All " + strSQL.ToString().Replace("Conf_Conference_D","Archive_Conf_Conference_D"));

                strSQL.Append( " order by 3");
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
            }
        }
        #endregion

        #region GetRoomsUsage 

        private void GetRoomsUsage(ref StringBuilder strSQL, String startDate, String endDate, String userId, String orgid, String dtFormat,
            String rmID, String country, String zip)
        {
            try
            {
               strSQL.Append( " select "); 
                if (dtFormat.Equals("dd/MM/yyyy"))
                    strSQL.Append("convert(char,dbo.GMTtouserPreferedtime(" + userId + ",confdate),103) as Category1, convert(char,dbo.GMTtouserPreferedtime(" + userId + ",confdate),103) as Category  ");
                else if (dtFormat.Equals("HHmmZ")) //FB 2588
                    strSQL.Append("convert(char,confdate,101) as Category1, convert(char,confdate,101) as Category  ");
                else
                    strSQL.Append(" convert(char,dbo.GMTtouserPreferedtime(" + userId + ",confdate),101) as Category1, convert(char,dbo.GMTtouserPreferedtime(" + userId + ",confdate),101) as Category ");
                
                strSQL.Append(" ,c.Name as SeriesName, count(*) as ReportsRange ");

                strSQL.Append( " from Conf_Conference_D a, Conf_Room_D b, Loc_Room_D c where a.orgId=" + orgid + " and a.ConfID = b.ConfID ");
                strSQL.Append( " and b.RoomID = c.RoomID and a.deleted = 0 ");
                
                if (rmID != "")
                    strSQL.Append( " and c.roomid IN (" + rmID + ") ");

                if (country != "")
                    strSQL.Append( " and c.Country = " + country.Split('|')[0] + " and c.State =  " + country.Split('|')[1]);

                if (zip != "")
                    strSQL.Append( " and c.ZipCode like '%" + zip.Trim() + "%'");

                strSQL.Append( " and dbo.GMTtouserPreferedtime(" + userId + ",confdate) >= '" + startDate +"'");
                strSQL.Append( " and dbo.GMTtouserPreferedtime(" + userId + ",confdate) <= '" + endDate + "'");
                
                if (dtFormat.Equals("dd/MM/yyyy"))
                    strSQL.Append(" group by convert(char,dbo.GMTtouserPreferedtime(" + userId + ",confdate),103),convert(char,dbo.GMTtouserPreferedtime(" + userId + ",confdate),103),c.Name ");
                else if (dtFormat.Equals("HHmmZ")) //FB 2588
                    strSQL.Append(" group by convert(char,confdate,101),convert(char,confdate,101),c.Name ");
                else
                    strSQL.Append(" group by convert(char,dbo.GMTtouserPreferedtime(" + userId + ",confdate),101),convert(char,dbo.GMTtouserPreferedtime(" + userId + ",confdate),101),c.Name ");

                //ZD 101835                
                strSQL.Append(" Union All " + strSQL.ToString().Replace("Conf_Conference_D", "Archive_Conf_Conference_D").Replace("Conf_Room_D","Archive_Conf_Room_D"));

                strSQL.Append( " order by 1,3");

            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
            }
        }
        #endregion

        #region GetMCUUsageReport 

        private void GetMCUUsageReport(ref StringBuilder strSQL, String startDate, String endDate, String userId, String orgid, String mucName)
        {
            Int32 sCurYr1 = DateTime.Now.Year;//FB 2343
            try
            {
                if (mucName == "")
                {
                    strSQL.Append("select distinct (left(datename(month,c.confdate),3)) as Category,");
                    strSQL.Append(" datepart(month, dbo.GMTtouserPreferedtime(" + userId + ",c.confdate)) as d1,");
                    strSQL.Append("  b.bridgename as SeriesName, count(*) as ReportsRange ");
                    strSQL.Append(" from conf_room_d a, mcu_list_d b, Conf_Conference_D c ");
                    strSQL.Append(" where c.deleted = 0 and b.deleted=0 and a.Confid = c.Confid  and a.bridgeid=b.bridgeid and c.orgid = " + orgid + " and  conftype  in(4,2,6)");
                    strSQL.Append(" and dbo.GMTtouserPreferedtime(" + userId + ",confdate) >= '" + startDate + "'");
                    strSQL.Append(" and dbo.GMTtouserPreferedtime(" + userId + ",confdate) <= '" + endDate + "'");
                    strSQL.Append(" group by datepart(month, dbo.GMTtouserPreferedtime(" + userId + ",c.confdate)), (left(datename(month,c.confdate),3)),  b.bridgename  ");

                    //ZD 101835                   
                    strSQL.Append(" Union All " + strSQL.ToString().Replace("Conf_Conference_D", "Archive_Conf_Conference_D").Replace("conf_room_d", "Archive_Conf_Room_D"));

                    strSQL.Append(" order by datepart(month, dbo.GMTtouserPreferedtime(" + userId + ",c.confdate)),  b.bridgename ");
                }
                else
                {   
                    strSQL.Append(" select distinct (left(datename(month,dbo.GMTtouserPreferedtime(" + userId + ",c.confdate)),3)) as Category,");
                    strSQL.Append(" videoprotocoltype as SeriesName, count(*) as ReportsRange, ");
                    strSQL.Append(" datepart(month, dbo.GMTtouserPreferedtime(" + userId + ",c.confdate)) as d1 ");
                    strSQL.Append(" from conf_room_d a, Conf_Conference_D c, gen_videoprotocol_s b ");
                    strSQL.Append(" where a.Confid = c.Confid and c.deleted = 0  and a.defVideoProtocol = b.videoprotocolid and a.bridgeid = " + mucName);
                    strSQL.Append(" and  conftype  in (4,2,6) and c.orgid = " + orgid);
                    strSQL.Append(" and month(a.startdate) = 3 and year(a.startdate)= " + sCurYr1 + " ");//FB 2343
                    strSQL.Append(" group by (left(datename(month,dbo.GMTtouserPreferedtime(" + userId + ",c.confdate)),3)),");
                    strSQL.Append(" datepart(month, dbo.GMTtouserPreferedtime(" + userId + ",c.confdate)) ,videoprotocoltype ");
                    //ZD 101835                    
                    strSQL.Append(" Union All " + strSQL.ToString().Replace("Conf_Conference_D", "Archive_Conf_Conference_D").Replace("conf_room_d", "Archive_Conf_Room_D"));

                    strSQL.Append(" order by datepart(month,dbo.GMTtouserPreferedtime(" + userId + ",c.confdate)),videoprotocoltype");
                }
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
            }
        }
        #endregion

        #region PersonalReport 
        /// <summary>
        /// PersonalReport - FB 2155
        /// </summary>
        /// <param name="stmt"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="userId"></param>
        /// <param name="orgid"></param>
        /// <param name="sDtFormat"></param>
        /// <param name="statusType"></param>
        private void PersonalReport(ref StringBuilder stmt, String startDate, String endDate, String userId, String orgid, String sDtFormat, String statusType)
        {
            try
            {
                stmt.Append("select distinct C.confnumname as 'Conference #',");

                if (sDtFormat.Equals("dd/MM/yyyy"))
                    stmt.Append(" convert(char,dbo.GMTtouserPreferedtime(" + userId + ", C.confdate),103) 'Conference Date',");
                else if (sDtFormat.Equals("HHmmZ"))
                    stmt.Append(" convert(char,C.confdate,101) 'Conference Date',");
                else
                    stmt.Append(" convert(char,dbo.GMTtouserPreferedtime(" + userId + ", C.confdate),101) 'Conference Date',");

                stmt.Append(" C.ExternalName as 'Meeting Title', case when CHARINDEX('H',UserIDRefText) > 0 then UD.firstname + ' ' +  UD.lastname  else U.firstname + ' ' +  U.lastname  end as 'Host', C.Duration as 'Duration (mins)',"); //ZD 100528 //ZD 103878
                stmt.Append(" case C.conftype when 6 then 'Audio' when 2 then 'Video'");
                stmt.Append(" when 7 then 'Room' when 8 then 'Hotdesking' when 4 then 'Point-to- Point' end as 'Type',");//FB 2694
                stmt.Append(" case C.[public] when 0 then 'No' when 1 then 'Yes' end as 'Public' from Conf_Conference_D C");

                stmt.Append(" left outer join (select LastName,firstname,  UserID from usr_list_d) U on c.owner = u.userid ");
                stmt.Append(" left outer join (select LastName,firstname,  id from Usr_Deletelist_D) UD on c.owner = UD.id ");

                stmt.Append(" left outer join Conf_User_D CU on C.Confid = CU.Confid and C.instanceid = CU.instanceid");

                stmt.Append(" left outer join Conf_Approval_D CA on C.confid = CA.confid  and C.instanceid = CA.instanceid where ");
                if (usrAdmin != "2")
                    stmt.Append(" (C.[public] = 1 or C.owner = '" + userId + "' or (CA.approverid = '" + userId + "' and CA.decision = 0) or CU.userid = '" + userId + "') and"); //ZD 100263
                stmt.Append(" dbo.GMTtouserPreferedtime(" + userId + ",C.confdate) >= '" + startDate + "'");
                stmt.Append(" and dbo.GMTtouserPreferedtime(" + userId + ",C.confdate) <= '" + endDate + "'");
                stmt.Append(" and (C.orgId =" + orgid + ")");

                if (statusType != "")
                {
                    if (statusType == "2")//Constraint added for ongoing conference
                    {
                        stmt.Append(" and C.confdate >= dbo.changeToGMTTime(" + sysSettings.TimeZone + ", DATEADD(mi, -C.duration, getdate()))");
                        stmt.Append(" and C.confdate <= dbo.changeToGMTTime(" + sysSettings.TimeZone + ", DATEADD(mi, 6, getdate()))");
                    }
                    else if (statusType == "4" || statusType == "7" || statusType == "8") //ZD 100036
                        stmt.Append(" and C.confdate > dbo.changeToGMTTime(" + sysSettings.TimeZone + ", DATEADD(mi, 6, getdate()))");
                }

                if (statusType != "")
                {
                    switch (statusType)
                    {
                        case "2":
                            stmt.Append(" and C.deleted = 0 and C.status != 1");
                            break;
                        case "3":
                            stmt.Append(" and C.status = 1 and C.deleted = 0 and C.confdate > dbo.changeToGMTTime(" + sysSettings.TimeZone + ", getdate())");
                            break;
                        case "4":
                            stmt.Append(" and C.status = 0 and C.deleted = 0");
                            break;
                        case "5":
                            stmt.Append(" and C.deleted = 2 ");
                            break;
                        case "6":
                            stmt.Append(" and C.deleted = 1 ");
                            break;
                        case "7":
                            stmt.Append(" and C.status = 0 and C.[public] = 1 and C.deleted = 0");
                            break;
                        case "8":
                            stmt.Append(" and C.status = 6 and C.deleted = 0");
                            break;
                    }
                 }
                //ZD 101835
                stmt.Append(" Union All " + stmt.ToString().Replace("Conf_Conference_D", "Archive_Conf_Conference_D").Replace("Conf_User_D", "Archive_Conf_User_D")
                    .Replace("Conf_Approval_D", "Archive_Conf_Approval_D"));

                stmt.Append(" order by 2, 6 ");
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
            }
        }
        #endregion
        
        #region UserReports
        /// <summary>
        /// UserReports
        /// </summary>
        /// <param name="stmt"></param>
        /// <param name="loginId"></param>
        /// <param name="sDtFormat"></param>
        /// <param name="usrType"></param>
        /// <param name="orgID"></param>
        private void UserReports(ref StringBuilder stmt, String loginId, String sDtFormat, String usrType, String orgID)
        {
            try
            {
                stmt.Append(" Select FirstName as 'First Name', LastName as 'Last Name', isnull(WorkPhone, '') as 'Contact No',Email, b.roleName as 'Role', ");
                if (sDtFormat.Equals("dd/MM/yyyy"))
                    stmt.Append(" convert(char,dbo.GMTtouserPreferedtime(" + loginId + ",AccountExpiry),103) 'Account Expiry' ");
                else if (sDtFormat == "HHmmZ")//FB 2588
                    stmt.Append(" convert(char,AccountExpiry,101) 'Account Expiry' ");
                else
                    stmt.Append(" convert(char,dbo.GMTtouserPreferedtime(" + loginId + ",AccountExpiry),101) 'Account Expiry' ");
				//ZD 102043 Starts
                if (usrType == "SU")
                {
                    if (sDtFormat.Equals("dd/MM/yyyy"))
                        stmt.Append(" ,convert(char,dbo.GMTtouserPreferedtime(" + loginId + ",c.InitTime),103) 'Created Date' ");
                    else if (sDtFormat == "HHmmZ")
                        stmt.Append(" ,convert(char,c.InitTime,101) 'Created Date' ");
                    else
                        stmt.Append(" ,convert(char,dbo.GMTtouserPreferedtime(" + loginId + ",c.InitTime),101) 'Created Date' ");

                    stmt.Append(" , c.ScheduledMinutes as 'Scheduled Minutes' ");
                }
				//ZD 102043 End
                String stmet = stmt.ToString();
                if (usrType != "ALL")
                    stmt.Append(" FROM Usr_Roles_D b , ");
				//ZD 102043 Starts
                if (usrType == "ACU" || usrType == "BL" || usrType == "SU")
                {
                    if (usrType == "SU")
                        stmt.Append(" Acc_Balance_D c, ");
					//ZD 102043 End
                    stmt.Append(" Usr_List_D a ");
                    stmt.Append(" WHERE deleted = 0 AND a.RoleID = b.RoleID ");
                    if (usrType == "ACU")
                        stmt.Append(" AND LockStatus != 3");//Active Users
                    if (usrRoleId == "2") //Org Admin
                         stmt.Append(" AND a.Roleid !=3 ");
                    if (usrType == "BL")
                        stmt.Append(" AND LockStatus = 3"); //Blocked Users
                    if (usrType == "SU")//ZD 102043
                        stmt.Append(" AND a.UserID = c.UserID AND c.ScheduledMinutes > 0 ");  //Scheduled Usage Users
                    stmt.Append(" AND companyId = " + orgID);

                }
                else if (usrType == "IA")
                {
                    stmt.Append(" Usr_Inactive_D a ");
                    stmt.Append(" WHERE deleted = 1 AND a.RoleID = b.RoleID "); //Inactive Users
                    if (usrRoleId == "2")
                        stmt.Append(" AND a.Roleid !=3 ");
                    stmt.Append(" AND companyId = " + orgID);
                }
                else if (usrType == "ALL")                                      //ALL Users
                {
                    stmt.Append(" , 'Status' = case when a.lockstatus = 3 then 'Blocked' when a.lockstatus = 0 then 'Active' end ");
                    stmt.Append(" FROM Usr_Roles_D b , Usr_List_D a  ");
                    stmt.Append(" WHERE deleted = 0 AND a.RoleID = b.RoleID ");
                    if (usrRoleId == "2")
                        stmt.Append(" AND a.Roleid !=3 ");
                    stmt.Append(" AND a.companyId = " + orgID);
                    stmt.Append(" UNION ALL");
                    stmt.Append(stmet.ToString());
                    stmt.Append(" , 'Status' = case when a.userid > 0 then 'Inactive' end ");
                    stmt.Append(" FROM Usr_Roles_D b , Usr_Inactive_D a  ");
                    stmt.Append(" WHERE deleted = 1 AND a.RoleID = b.RoleID ");
                    if (usrRoleId == "2")
                        stmt.Append(" AND a.Roleid !=3 ");
                    stmt.Append(" AND a.companyId = " + orgID);
                }
                stmt.Append(" order by FirstName ");
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);

            }
        }

        #endregion

        #region RoomReports
        /// <summary>
        /// RoomReports
        /// </summary>
        /// <param name="stmt"></param>
        /// <param name="roomType"></param>
        /// <param name="orgid"></param>
        /// <returns></returns>
        private void RoomReports(ref StringBuilder stmt, String roomType, String orgId)
        {
            try
            {
                //FB 2529 starts
                stmt.Append(" Select distinct g.Name as 'Top Tier', f.Name as 'Middle Tier',  a.Name as 'Room Name',  'Media' = case when b.[MediaType] is NULL then '' ");
                stmt.Append(" when  b.[MediaType] = 'Audio-only' then 'Audio' when  b.[MediaType] = 'Audio/Video' then 'Video' end, "); //ZD 100528
                stmt.Append(" 'Endpoint' = case when c.[Name] is NULL then '' else c.Name end, u1.FirstName + ' '+ u1.LastName as 'Admin', ");
                stmt.Append(" ISNULL(u.FirstName,'') as 'Approver 1', e.TimeZone  as 'Time Zone' FROM");
                stmt.Append("(select *, (select top 1 AssistantId from Loc_Assistant_D la where la.RoomId = r.RoomID order by la.ID) as Assistant1 from loc_room_d r)  a "); //ALLDEV-807
                //Loc_Room_D a "); //ZD 101429
                stmt.Append(" Left Outer Join Gen_MediaType_S b on b.ID = a.VideoAvailable ");
                stmt.Append(" Left Outer Join  Ept_List_D c on  c.endpointid = a.endpointid and c.deleted = 0,");// AND a.orgId = " + orgId + " AND "); //ZD 103113
                stmt.Append(" Gen_TimeZone_S e,  usr_List_d d, Loc_Tier2_D f, Loc_Tier3_D g,");//,Loc_Approver_D l  
            
                stmt.Append("(SELECT t.roomid, t.approverid FROM (SELECT ROW_NUMBER() OVER (PARTITION BY roomid ORDER BY roomid) AS RowNo,");
                stmt.Append(" roomid, approverid FROM Loc_Approver_D)t WHERE t.RowNo=1) as tt");
                stmt.Append(" inner join Usr_List_D u on u.UserID =tt.approverid ");
                stmt.Append(" right outer join Loc_Room_D r on  tt.roomid=r.RoomID ");
                stmt.Append(" inner join Usr_List_D u1 on r.Assistant=u1.UserID ");
                stmt.Append(" WHERE a.l2locationid = f.id and f.l3locationid  = g.id AND d.userid = a.assistant ");
                stmt.Append(" AND e.TimezoneID = a.TimezoneID AND a.orgId = '" + orgId + "' and a.RoomID=r.RoomID AND ");
                //FB 2529 ends
                if (roomType == "ACR")
                    stmt.Append(" a.Disabled = 0 ");  //Activated Rooms
                else if (roomType == "DE")
                    stmt.Append(" a.Disabled = 1 AND  a.roomId <> 11");  //Deactivated Rooms
                stmt.Append(" order by g.Name, f.Name, a.Name");
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);

            }
        }

        #endregion

        #region EptReports
        /// <summary>
        /// EptReports
        /// </summary>
        /// <param name="stmt"></param>
        /// <param name="orgID"></param>
        /// <returns></returns>
        private void EptReports(ref StringBuilder stmt, String orgID)
        {
            try
            {
                stmt.Append(" Select a.Name as 'Endpoint Name', 'Address Type' = case when a.[addressType] is NULL then '' else b.name end,");
                stmt.Append(" 'Address' = case when a.MultiCodecAddress like '%�%' then REPLACE(a.MultiCodecAddress,'�',',') when a.[isTelePresence]=1 then a.[MultiCodecAddress] when a.[isTelePresence]=0 then a.[address] end,"); //FB 2577
                stmt.Append(" 'Model' = case when a.[videoequipmentid] is NULL then '' else c.VEname end,  'Protocol' = case when a.[Protocol] is NULL ");
                stmt.Append(" then '' else d.VideoProtocolType end,  'MCU' = case when a.[BridgeID] is NULL then '' else e.BridgeName end, ");
                stmt.Append(" 'Dialing Option' = case when a.[connectionType] is NULL then 'N/A' else f.DialingOption end , a.ProfileName as 'Default Profile' ");
                stmt.Append(" FROM Ept_List_D a ");
                stmt.Append(" Left Outer Join Gen_AddressType_S b on b.id =  a.addressType ");
                stmt.Append(" Left Outer Join Gen_VideoEquipment_S c on c.VEid =  a.videoequipmentid "); //FB 2521
                stmt.Append(" Left Outer Join Gen_VideoProtocol_S d on d.VideoProtocolId =  a.Protocol ");
                stmt.Append(" Left Outer Join Mcu_List_D e on e.BridgeID = a.BridgeID ");
                stmt.Append(" Left Outer Join Gen_DialingOption_S f on f.ID = a.connectionType ");
                stmt.Append(" WHERE a.deleted = 0 AND a.isdefault = 1 AND a.orgId = " + orgID);
                stmt.Append(" order by a.Name ");
            
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);

            }
        }

        #endregion

        #region MCUReports
        /// <summary>
        /// MCUReports
        /// </summary>
        /// <param name="stmt"></param>
        /// <param name="orgID"></param>
        /// <returns></returns>
        private void MCUReports(ref StringBuilder stmt, String orgID)
        {
            try
            {
                stmt.Append("Select BridgeName as 'Bridge Name', c.name as 'Type', a.SoftwareVer as 'Firmware Version',");
                stmt.Append(" a.BridgeAddress as 'Address', b.TimeZone as 'Time Zone', e.FirstName +' '+ e.LastName as 'Admin', ");
                stmt.Append(" isnull(PApprover,'') as 'Approver 1' FROM Mcu_List_D a  ");//ZD 101429
                stmt.Append(" left outer join  (select MCUid,f.FirstName +' '+ f.LastName as PApprover  from Mcu_Approver_D d , usr_List_d f ");
                stmt.Append(" where d.approverID = f.userid) as x on x.MCUid = a.BridgeID,  Gen_TimeZone_S b, MCU_Vendor_S c, usr_list_d e  ");
                stmt.Append(" WHERE b.TimezoneID = a.timezone AND e.userid = a.Admin AND c.id = a.BridgeTypeId  AND a.deleted = 0 AND a.orgId = '" + orgID + "'");
                stmt.Append(" order by BridgeName ");
            }

            catch (Exception e)
            {
                m_log.Error("sytemException", e);

            }
        }

        #endregion

        #region RoomConfReports
        /// <summary>
        /// RoomConfReports
        /// </summary>
        /// <param name="stmt"></param>
        /// <param name="roomConfType"></param>
        /// <param name="starttime"></param>
        /// <param name="endtime"></param>
        /// <param name="orgID"></param>
        private void RoomConfReports(ref StringBuilder stmt, String roomConfType, String startDate, String endDate, String orgID, String dtFormat)//FB 2588
        {
            try
            {
                if (roomConfType == "RSC" || roomConfType == "RAC")
                {
                    stmt.Append("select [Room Name], SUM([Total Minutes]) as [Total Minutes]  from ( ");
                    StringBuilder stmtSub = new StringBuilder();
                    stmtSub.Append(" Select  c.[name] as [Room Name], isnull(d.duration,0) as [Total Minutes]  from ");
                    stmtSub.Append(" (select sum(a.duration) as  duration, b.roomid from conf_conference_d as a, conf_room_d b  ");
                    stmtSub.Append(" where a.isHDBusy = 0 and a.orgId=" + orgID + " AND a.confid = b.confid and a.instanceid = b.instanceid   "); //ALLDEV-807

                    if (roomConfType == "RSC")
                        stmtSub.Append(" AND  a.deleted != 1 ");
                    else
                    {
                        stmtSub.Append(" and (( (a.confdate >= dbo.changeToGMTTime(" + sysSettings.TimeZone + ", DATEADD(mi, -a.duration, getdate()))");
                        stmtSub.Append(" and a.confdate <= dbo.changeToGMTTime(" + sysSettings.TimeZone + ", DATEADD(mi, 6, getdate()))) )");
                        stmtSub.Append(" OR a.deleted = 2 OR a.Status = 7)");
                    }
                    if(dtFormat == "HHmmZ") //FB 2588
                        stmtSub.Append(" AND a.confdate BETWEEN '" + startDate + "' AND '" + endDate + "' group by b.roomid) ");
                    else
                        stmtSub.Append(" AND a.confdate BETWEEN dbo.changeTOGMTtime(" + timezone + ", '" + startDate + "') AND dbo.changeTOGMTtime(" + timezone + ",  '" + endDate + "') group by b.roomid) ");

                    stmtSub.Append(" as d right outer join  loc_room_d c on d.roomid = c.roomid where c.disabled  != 1 AND d.duration > 0 and c.orgid = " + orgID);

                    //ZD 101835
                    stmtSub.Append(" Union All " + stmtSub.ToString().Replace("conf_conference_d", "Archive_conf_conference_d").Replace("conf_room_d", "Archive_conf_room_d"));


                    stmtSub.Append(" ) as x group by  [Room Name] order by  [Room Name] ");

                    stmt.Append(stmtSub.ToString());
                }
                
                if (roomConfType == "RCO")
                {
                    stmt.Append("declare @fromDate nvarchar(40), @toDate nvarchar(40) "); //FB 2205
                    stmt.Append("declare @confid int, @confnumname int ");
                    stmt.Append(" set @fromDate = '" + startDate + " '");
                    stmt.Append(" set @toDate = '" + endDate + " '");

                    //ZD 101835
                    StringBuilder stmtSub = new StringBuilder();

                    stmtSub.Append(" select  c.[Name] as [Room Name], sum(a.Duration) as [Duration (mins)] "); //ZD 103840 //104169 
                    stmtSub.Append(" from conf_conference_d a, conf_room_d b, loc_room_d c ");
                    stmtSub.Append(" where a.Confnumname = b.Confuid and b.RoomID = c.RoomID ");
                    stmtSub.Append(" and (CAST(FLOOR(CAST(a.ConfDate AS float)) AS datetime) BETWEEN @fromDate AND @toDate) ");
                    stmtSub.Append(" AND (a.status = 5 OR a.status = 7 OR a.status = 3) AND a.Deleted = 0 AND  a.isHDBusy = 0 and c.orgid = " + orgID); //ALLDEV-807
                    stmtSub.Append(" group by c.[Name] ");

                    //ZD 101835
                    stmtSub.Append(" Union All " + stmtSub.ToString().Replace("conf_conference_d", "Archive_conf_conference_d").Replace("conf_room_d", "Archive_conf_room_d"));
                    stmt.Append(stmtSub.ToString());

                    stmt.Append(" order by c.[Name] ");
                }
            }

            catch (Exception e)
            {
                m_log.Error("sytemException", e);

            }
        }

        #endregion

		//FB 2047 - Start

        #region GetMCCReport
        public bool GetMCCReport(ref vrmDataObject obj)
        {
            bool bRet = true;
            String dtFormat = "";
            String userId = "";
            StringBuilder strSQL = null;
            StringBuilder strSQL1 = null;
            StringBuilder strSQL2 = null; //FB 2593
            Int32 sCurYr = DateTime.Now.Year;
            //m_rptLayer = new ns_SqlHelper.SqlHelper();
            DataSet ds = new DataSet();
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//MCCReport/DateFormat");
                if (node.InnerText != "")
                    dtFormat = node.InnerXml.Trim();
                //FB 2588
                node = xd.SelectSingleNode("//MCCReport/TimeFormat");
                if (node.InnerText != "")
                    tFormat = node.InnerXml.Trim();
    
                string type = "";
                node = xd.SelectSingleNode("//MCCReport/Type");
                if (node.InnerText != "")
                    type = node.InnerXml.Trim();

                string startDate = "";
                node = xd.SelectSingleNode("//MCCReport/StartDate");
                if (node.InnerText != "")
                    startDate = node.InnerXml.Trim();

                string endDate = "";
                node = xd.SelectSingleNode("//MCCReport/EndDate");
                if (node.InnerText != "")
                    endDate = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//MCCReport/UserID");
                if (node.InnerText != "")
                    userId = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//MCCReport/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;
                int.TryParse(orgid, out organizationID);

                node = xd.SelectSingleNode("//MCCReport/timezone");
                if (node != null)
                {
                    if (node.InnerXml.Trim().Length > 0)
                        timezone = node.InnerXml.Trim();
                }

                if (timezone == "")
                    timezone = "26";

                String confSelect = "", confFilter = ""; //FB 3066

                node = xd.SelectSingleNode("//MCCReport/Params/Conference/Query/Select");
                if (node != null)
                    confSelect =  node.InnerXml.Trim();
                //confSelect = confSelect + "," + node.InnerXml.Trim();

                node = xd.SelectSingleNode("//MCCReport/Params/Conference/Query/Filter");
                if (node != null)
                    confFilter = node.InnerXml.Trim();

                String strorgid = "", departmentid = "", rmInchargeSelect = "", mcuInchargeSelect = "", woInchargeSelect = "";
                String attendeesFilter = "", roomsPartyFilter = "", endPointsSelect = "", mcuSelect = "", confSpeedSelect = "";
                String conProtocolSelect = "", confWOSelect = "";

                GetRightParams(node, xd, ref strorgid, ref departmentid, ref rmInchargeSelect, ref mcuInchargeSelect, ref woInchargeSelect,
                    ref attendeesFilter, ref roomsPartyFilter, ref endPointsSelect, ref mcuSelect, ref confSpeedSelect,
                    ref conProtocolSelect, ref confWOSelect);

                String hostSelect = "", hostFilter = "";
                Boolean isHost = false;

                node = xd.SelectSingleNode("//MCCReport/Params/Host/Query/Select");
                if (node != null)
                {
                    hostSelect = node.InnerXml.Trim();
                    isHost = true;
                }
                node = xd.SelectSingleNode("//MCCReport/Params/Host/Query/Filter");
                if (node != null)
                    hostFilter = node.InnerXml.Trim();

                String partSelect = "", partFilter = "";
                Boolean isPart = false;

                node = xd.SelectSingleNode("//MCCReport/Params/Participant/Query/Select");
                if (node != null)
                {
                    isPart = true;
                    partSelect = node.InnerXml.Trim();
                }

                node = xd.SelectSingleNode("//MCCReport/Params/Participant/Query/Filter");
                if (node != null)
                    partFilter = node.InnerXml.Trim();

                String scheduledFilter = "";

                node = xd.SelectSingleNode("//MCCReport/Params/Scheduled/Query/Select");
                if (node != null)
                    confSelect = confSelect + "," + node.InnerXml.Trim();

                node = xd.SelectSingleNode("//MCCReport/Params/Scheduled/Query/Filter");
                if (node != null)
                    scheduledFilter = node.InnerXml.Trim();

                String confTypeFilter = "";
                node = xd.SelectSingleNode("//MCCReport/Params/ConfType/Query/Filter");
                if (node != null)
                    confTypeFilter = node.InnerXml.Trim();

                String confBehaviorFilter = "";
                node = xd.SelectSingleNode("//MCCReport/Params/ConfBehavior/Query/Filter");
                if (node != null)
                    confBehaviorFilter = node.InnerXml.Trim();

                // ZD 102835 Start
                String confTimeSelect = "", woTimeSelect = "";
                node = xd.SelectSingleNode("//MCCReport/Params/ConfTime/Query/Select");
                if (node != null)
                {
                    confSelect = confSelect + "," + node.InnerXml.Trim();
                    woTimeSelect = "," + node.InnerXml.Trim();
                }

                String confDurationSelect = "", woDurationSelect = "";
                node = xd.SelectSingleNode("//MCCReport/Params/ConfDuration/Query/Select");
                if (node != null)
                {
                    confSelect = confSelect + "," + node.InnerXml.Trim();
                    woDurationSelect = "," + node.InnerXml.Trim();

                }
                // ZD 102835 End

                String userCommon = "";
                node = xd.SelectSingleNode("//MCCReport/Params/UserCommon/Query/Select");
                if (node != null)
                    userCommon = node.InnerXml.Trim();

                String isExchange = "";
                node = xd.SelectSingleNode("//MCCReport/Params/Exchange/Query/Select");
                if (node != null)
                    isExchange = node.InnerXml.Trim();

                String isDomino = "";
                node = xd.SelectSingleNode("//MCCReport/Params/Domino/Query/Select");
                if (node != null)
                    isDomino = node.InnerXml.Trim();

                String roomType = "";
                node = xd.SelectSingleNode("//MCCReport/Params/RoomType/Query/Filter");
                if (node != null)
                    roomType = node.InnerXml.Trim();

                String rmImmediate = "";
                node = xd.SelectSingleNode("//MCCReport/Params/Immediate/Query/Select");
                if (node != null)
                    rmImmediate = node.InnerXml.Trim();

                String subConfSelect = "";
                node = xd.SelectSingleNode("//MCCReport/Params/SubConfSelect/Query/Select");
                if (node != null)
                    subConfSelect = node.InnerXml.Trim();

                String eptDetails = "";
                node = xd.SelectSingleNode("//MCCReport/Params/EptDetails/Query/Select");
                if (node != null)
                    eptDetails = node.InnerXml.Trim();

                String mcuDetails = "";
                node = xd.SelectSingleNode("//MCCReport/Params/MCUDetails/Query/Select");
                if (node != null)
                    mcuDetails = node.InnerXml.Trim();

                String woDetails = "";
                node = xd.SelectSingleNode("//MCCReport/Params/WODetails/Query/Select");
                if (node != null)
                    woDetails = node.InnerXml.Trim();

                String rmAsset = "";
                node = xd.SelectSingleNode("//MCCReport/Params/RmAsset/Query/Select");
                if (node != null)
                    rmAsset = node.InnerXml.Trim();

               String eptColDetails = "";
                node = xd.SelectSingleNode("//MCCReport/Params/Endpoint/Query/Select");
                if (node != null)
                    eptColDetails = node.InnerXml.Trim();

                 String immediate = "";
                 node = xd.SelectSingleNode("//MCCReport/Params/Immediate/Query/Select");
                if (node != null)
                    immediate = node.InnerXml.Trim();

                 String tierInfo = "";
                 node = xd.SelectSingleNode("//MCCReport/Params/Tier/Query/Select");
                if (node != null)
                    tierInfo = node.InnerXml.Trim();
				//FB 2654	
                String concierge = "";
                node = xd.SelectSingleNode("//MCCReport/Params/Confcierge/Query/Select");
                if (node != null)
                    concierge = node.InnerXml.Trim();
				//FB 2593 Starts
                String cdr = "";
                node = xd.SelectSingleNode("//MCCReport/Params/CDR/Query/Select");
                if (node != null)
                    cdr = node.InnerXml.Trim();

                String isSecondTable = "";
                node = xd.SelectSingleNode("//MCCReport/Params/SecondTable/Query/Select");
                if (node != null)
                    isSecondTable = node.InnerXml.Trim();

                String mcu = "";
                node = xd.SelectSingleNode("//MCCReport/Params/MCU/Query/Select");
                if (node != null)
                    mcu = node.InnerXml.Trim();
                
                String room = "";
                node = xd.SelectSingleNode("//MCCReport/Params/Room/Query/Select");
                if (node != null)
                    room = node.InnerXml.Trim();

                String endPoints = "";
                node = xd.SelectSingleNode("//MCCReport/Params/EndPoints/Query/Select");
                if (node != null)
                    endPoints = node.InnerXml.Trim();
				//FB 2593 End
                //ZD 101950
                String confVMR = "";
                node = xd.SelectSingleNode("//MCCReport/Params/ConfVMR/Query/Filter");
                if (node != null)
                    confVMR = node.InnerXml.Trim();
                //ZD 104686
                String customOptions = "";
                node = xd.SelectSingleNode("//MCCReport/Params/CustomOptions/Query/Filter");
                if (node != null)
                    customOptions = node.InnerXml.Trim();

                //ZD 102835 Start
                String workOrders = "";
                node = xd.SelectSingleNode("//MCCReport/Params/WorkOrders/Query/Filter");
                if (node != null)
                    workOrders = node.InnerXml.Trim();

                String Personnel = "";
                node = xd.SelectSingleNode("//MCCReport/Params/Personnel/Query/Select");
                if (node != null)
                    Personnel = node.InnerXml.Trim();
				//ZD 102835 End

                strSQL = new StringBuilder();
                strSQL1 = new StringBuilder();
                strSQL2 = new StringBuilder(); //FB 2593
                
                if (type == "1")
                    GetConferenceReport(ref strSQL, ref strSQL1, confSelect, confFilter, hostSelect, hostFilter, isHost, partSelect, partSelect, isPart,
                     scheduledFilter, confTypeFilter, confBehaviorFilter, confTimeSelect, confDurationSelect, strorgid, departmentid, rmInchargeSelect,
                     mcuInchargeSelect, woInchargeSelect, attendeesFilter, roomsPartyFilter, endPointsSelect, mcuSelect, confSpeedSelect,
                     conProtocolSelect, confWOSelect, startDate, endDate, timezone, concierge, confVMR, orgid, customOptions);//FB 2654 // ZD 102835 //ZD 104686
                else if (type == "2")//FB 2593 Starts
                    GetUserConferenceReport(ref strSQL, ref strSQL1, userCommon, departmentid, hostSelect, confTypeFilter, partSelect, isPart, isDomino,
                    isExchange, rmInchargeSelect, mcuInchargeSelect, woInchargeSelect, startDate, endDate, timezone,strorgid);
                else if (type == "3") 
                    GetRoomConferenceReport(ref strSQL, ref strSQL1, type, userCommon, hostSelect, roomType, rmImmediate, subConfSelect, eptDetails, mcuDetails
                    , confBehaviorFilter, woDetails, scheduledFilter, confTypeFilter, rmAsset, rmInchargeSelect, departmentid, startDate, endDate
                    , timezone, immediate, tierInfo, eptColDetails, strorgid);
                else if (type == "4" || type == "5")
                    GetEptAndMCUConferenceReport(ref strSQL, ref strSQL1, type, userCommon, hostSelect, roomType, rmImmediate, subConfSelect, eptDetails, mcuDetails
                    , confBehaviorFilter, woDetails, scheduledFilter, confTypeFilter, rmAsset, rmInchargeSelect, departmentid, startDate, endDate
                    , timezone, immediate, tierInfo, eptColDetails, isSecondTable, mcu, room, endPoints, strorgid);
                else if (type == "6")
                {
                    if (cdr == "C")
                        GetCDRScheduledReports(ref strSQL, ref strSQL1, ref strSQL2, startDate, endDate, timezone, userId, dtFormat, mcu, strorgid);
                    else //ZD 101348
                        GetVMRCDRScheduledReports(ref strSQL, ref strSQL1, ref strSQL2, startDate, endDate, timezone, userId, dtFormat, mcu, strorgid);
                    //GetVMRCDRReports(ref strSQL, ref strSQL1, startDate, endDate, timezone, userId, dtFormat, mcu, strorgid); //FB 3045

                }
				//FB 2593 End
                else if (type == "7") // ZD 102835
                {

                    GetWorkOrderConferenceReport(ref strSQL, confSelect, workOrders, hostSelect,
                        scheduledFilter, confTypeFilter, confBehaviorFilter,
                        woTimeSelect, woDurationSelect, departmentid,
                        startDate, endDate, timezone, userCommon, tierInfo, Personnel, orgid);

                }
                m_rptLayer.OpenConnection();
                
                if (strSQL1.ToString() != "")
                    strSQL.Append("; " + strSQL1.ToString());
                //FB 2593
                if (strSQL2.ToString() != "")
                    strSQL.Append("; " + strSQL2.ToString());
                //DataSet dspart = new DataSet();
                if (strSQL.ToString() != "")
                {
                    ds = m_rptLayer.ExecuteDataSet(strSQL.ToString());

                    //if (strSQL1.ToString() != "")
                    //    dspart = m_rptLayer.ExecuteDataSet(strSQL1.ToString());
                }

                m_rptLayer.CloseConnection();

                if (ds != null)
                {
					//ZD 104686	
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        for (int i = 0; i < ds.Tables[0].Columns.Count; i++)
                        {
                            ds.Tables[0].Columns[i].ReadOnly = false;
                            if (string.IsNullOrEmpty(row[i].ToString()) && ds.Tables[0].Columns[i].DataType.ToString() == "System.String")
                                row[i] = "-";
                        }
                    }

                    MemoryStream ms = new MemoryStream();
                    ds.WriteXml(ms);// extract string from MemoryStream
                    ms.Position = 0;
                    StreamReader sr = new StreamReader(ms, System.Text.Encoding.UTF8);
                    String strXml = sr.ReadToEnd();
                    obj.outXml = strXml;
                    //obj.outXml = "<Tables><Table1>";
                    //obj.outXml += strXml;
                    //obj.outXml += "</Table1>";
                    sr.Close();
                    ms.Close();

                    //if (dspart != null)
                    //{
                    //    ms = new MemoryStream();
                    //    dspart.WriteXml(ms);// extract string from MemoryStream
                    //    ms.Position = 0;
                    //    StreamReader sr1 = new StreamReader(ms, System.Text.Encoding.UTF8);
                    //    String strPartXml = sr1.ReadToEnd();
                    //    obj.outXml += "<Table2>";
                    //    obj.outXml += strPartXml;
                    //    obj.outXml += "</Table2>";
                    //    sr.Close();
                    //    ms.Close();
                    //}
                    //obj.outXml = "</Tables>";
                }

                if (ds.Tables[0].Rows.Count <= 0)
                {
                    String oXML = "";
                    oXML = "<NewDataSet><Table>";
                    for (int c = 0; c < ds.Tables[0].Columns.Count; c++)
                    {
                        oXML += "<" + ds.Tables[0].Columns[c].ToString().Replace(" ", "_x0020_").Replace("#", "_x0023_").Replace("/","_x002F_")
                            .Replace("(","_x0028_").Replace(")","_x0029_") + " />"; //_x0020_  --> Space , _x0023_  --> # //FB 2654
                    }
                    oXML += "</Table></NewDataSet>";

                    obj.outXml = oXML;
                }
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
            return bRet;
        }
        #endregion

        #region GetRightParams

        private void GetRightParams(XmlNode node, XmlDocument xd, ref String strorgid, ref String departmentid, ref String rmInchargeSelect, ref String mcuInchargeSelect,
        ref String woInchargeSelect, ref String attendeesFilter, ref String roomsPartyFilter, ref String endPointsSelect, ref String mcuSelect,
        ref String confSpeedSelect, ref String conProtocolSelect,ref String confWOSelect)
        {
            try
            {
                node = xd.SelectSingleNode("//MCCReport/Params/Conference/Query/OrgFilter/Org/Query/Filter");
                if (node != null)
                    strorgid = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//MCCReport/Params/Conference/Query/OrgFilter/Dept/Query/Filter");
                if (node != null)
                    departmentid = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//MCCReport/Params/Conference/Query/OrgFilter/RmIncharge/Query/Select");
                if (node != null)
                    rmInchargeSelect = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//MCCReport/Params/Conference/Query/OrgFilter/MCUIncharge/Query/Select");
                if (node != null)
                    mcuInchargeSelect = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//MCCReport/Params/Conference/Query/OrgFilter/WOIncharge/Query/Select");
                if (node != null)
                    woInchargeSelect = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//MCCReport/Params/Conference/Query/OrgFilter/Attendees/Query/Select");
                if (node != null)
                    attendeesFilter = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//MCCReport/Params/Conference/Query/OrgFilter/RoomsParty/Query/Filter");
                if (node != null)
                    roomsPartyFilter = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//MCCReport/Params/Conference/Query/OrgFilter/EndPoints/Query/Select");
                if (node != null)
                    endPointsSelect = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//MCCReport/Params/Conference/Query/OrgFilter/MCU/Query/Select");
                if (node != null)
                    mcuSelect = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//MCCReport/Params/Conference/Query/OrgFilter/ConfSpeed/Query/Select");
                if (node != null)
                    confSpeedSelect = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//MCCReport/Params/Conference/Query/OrgFilter/ConProtocol/Query/Select");
                if (node != null)
                    conProtocolSelect = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//MCCReport/Params/Conference/Query/OrgFilter/ConfWO/Query/Select");
                if (node != null)
                    confWOSelect = node.InnerXml.Trim();

            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
            }
        }

        #endregion

        #region GetConferenceReport

        private void GetConferenceReport(ref StringBuilder strSQL, ref StringBuilder strSQL1, String confSelect, String confFilter, String hostSelect, String hostFilter,
        Boolean isHost, String partSelect, String partFilter, Boolean isPart, String scheduledFilter, String confTypeFilter, String confBehaviorFilter,
        String confTimeSelect, String confDurationSelect, String strorgid, String departmentid, String rmInchargeSelect, String mcuInchargeSelect,
        String woInchargeSelect, String attendeesFilter, String roomsPartyFilter, String endPointsSelect, String mcuSelect,
        String confSpeedSelect, String conProtocolSelect, String confWOSelect, String startDate, String endDate, String tmZone,
        String concierge, string confVMR, string orgid, string customOptions) //FB 2654 //ZD 101950 // ZD 102835 //ZD 104686
        {
            try
            {
				//ZD 104686
                string stmtCus = "", stmtCus1 = "";
                if (customOptions != "")
                {
                    stmtCus = "select DisplayTitle from dept_CustomAttr_D where deleted=0 and status=0 and orgId='" + orgid.ToString() + "'";
                    stmtCus += " and CustomAttributeID in(" + customOptions + ")";

                    if (m_rptDAO != null)
                        m_rptLayer = new ns_SqlHelper.SqlHelper(configpath);

                    DataSet ds = null;
                    ds = m_rptLayer.ExecuteDataSet(stmtCus);
                    if (ds != null)
                    {
                        stmtCus = "";
                        stmtCus1 = "";

                        for (Int32 i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            stmtCus += " isnull([" + ds.Tables[0].Rows[i].ItemArray[0].ToString() + "],'N/A') as [" + ds.Tables[0].Rows[i].ItemArray[0].ToString() + "]";
                            stmtCus1 += " [" + ds.Tables[0].Rows[i].ItemArray[0].ToString() + "]";
                            if (i < ds.Tables[0].Rows.Count - 1)
                            {
                                stmtCus += ",";
                                stmtCus1 += ",";
                            }
                        }
                    }
                }

                //ZD 104686
                if (customOptions != "" && stmtCus1 != "")
                {
                    strSQL.Append(" select * from (");
                }

                strSQL.Append(" select distinct a.confnumname[ConfUID],  a.Confdate,(select StandardName from Gen_TimeZone_S where Timezoneid =" + tmZone + " ) as tzone"); //FB 3066 // ZD 102835
                if (isPart)
                    strSQL1.Append("select distinct a.ConfID");

                //dbo.changeTime(" + tmZone + ", ConfTime) [Start Time],
                if(tFormat != "HHmmZ") //FB 2588
                    confSelect = confSelect.Replace("ConfTime [Start Time]", "dbo.changeTime(" + tmZone + ", ConfTime) [Start Time]");

                if (confSelect != "")//for Participant table
                    strSQL.Append(", " + confSelect);

                if (strorgid != "")
                    strSQL.Append(", (select o.OrgName from org_list_d o where a.orgid = o.orgid) as Organization");

                if (departmentid != "")
                {
                    strSQL.Append(" ,Stuff ((SELECT distinct N', ' + d1.departmentname from  conf_room_D b1, Loc_Department_D c1, dept_list_d d1 where d1.Departmentid = c1.Departmentid ");
                    strSQL.Append(" and b1.Confid = a.confid and b1.RoomID = c1.RoomID and d1.Departmentid in (" + departmentid + ")");
                    strSQL.Append(" FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N'') as [Department Name] ");
                }

                if (isHost)
                {
					//ZD 103878
                    if (hostSelect != "")
                    {
                        hostSelect = hostSelect.Replace("h.FirstName", "case when CHARINDEX('H',a.UserIDRefText) > 0 then hd.FirstName else h.FirstName end ");
                        hostSelect = hostSelect.Replace("h.LastName", "case when CHARINDEX('H',a.UserIDRefText) > 0 then hd.LastName else h.LastName end ");
                        hostSelect = hostSelect.Replace("h.Email", "case when CHARINDEX('H',a.UserIDRefText) > 0 then hd.Email else h.Email end ");
                        hostSelect = hostSelect.Replace("hr.roleName", "case when CHARINDEX('H',a.UserIDRefText) > 0 then hdr.roleName else hr.roleName end ");
                    }
                    strSQL.Append(", " + hostSelect);
                }

                if (isPart)//for Participant table
                    strSQL1.Append(", " + partSelect);

                if (isPart && attendeesFilter != "")
                    strSQL1.Append(", isnull(p.[Company Relationship],'-') as [Company Relationship]");

                //if (!isPart && roomsPartyFilter != "")
                //    strSQL.Append(", p.LastName as [Part. Name], p.[Company Relationship]");


                strSQL.Append(", case Status when 7 then 'Completed' when 0 then 'Scheduled' when 9 then 'Deleted' ");
                strSQL.Append(" when 3 then 'Terminated' when 1 then 'Pending' when 6 then 'On MCU' End as Status "); //ZD 100036

                //Room in Conference CHAR(13)+CHAR(10)
                strSQL.Append(" ,isnull(Stuff ((SELECT distinct  N', ' + d.name + '|' + cast(r.HostRoom as varchar(5)) + '|' + cast(r.NoofAttendee as varchar(5)) from conf_room_d r, loc_room_d d where r.Roomid = d.Roomid and r.Confid = a.Confid and r.instanceID = a.instanceid ");
                strSQL.Append(" FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N''),'-') as [Rooms In Conf]  ");

                strSQL.Append(" ,isnull(Stuff ((SELECT distinct  N', ' + d.name from conf_room_d r, loc_room_d d where r.Roomid = d.Roomid and r.Confid = a.Confid and r.instanceID = a.instanceid ");
                strSQL.Append(" FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N''),'-') as [Rooms In Conf.]  ");
                //ZD 104686
                strSQL.Append(" ,isnull(Stuff ((SELECT distinct  N'\n ' + cast(r.NoofAttendee as varchar(5)) from conf_room_d r, loc_room_d d where r.Roomid = d.Roomid and r.Confid = a.Confid  and r.instanceID = a.instanceid "); //ZD 104686
                strSQL.Append(" FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N''),'') as [# of Attendees]  ");
                //ZD 103216
                strSQL.Append(" , isnull((select d.Name from conf_room_d r, loc_room_d d where r.Roomid = d.Roomid and r.Confid = a.Confid and r.instanceID = a.instanceid and r.HostRoom =1 ),'') as [Host Room] ");
                strSQL.Append(" , isnull((select SUM(r.NoofAttendee) from conf_room_d r where r.Confid = a.Confid and r.instanceID = a.instanceid and r.HostRoom =0 ),0) as [Total Travel Avoided] ");

                //EndPoints in Conference
                if (endPointsSelect != "")
                {
                    strSQL.Append(" ,isnull(Stuff ((SELECT distinct N', ' + e.name + '-' + e.address  ");
                    strSQL.Append(" from conf_conference_d c ,conf_room_d r, loc_room_d l, Ept_List_D e ");
                    strSQL.Append(" where c.confid = a.confid and c.instanceid = a.instanceid and c.conftype not in (7) and c.confid = r.confid and r.roomid = l.roomid and l.endpointid = e.endpointid ");
                    strSQL.Append(" and (e.profileid = 0 or e.isdefault = 1) FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N''),'-') as [Endpoints in Conf] ");
                }

                //MCU in Conference
                if (mcuSelect != "")
                {
                    strSQL.Append(" ,isnull(Stuff ((SELECT distinct N', ' + m.bridgename + '-' + m.BridgeAddress from conf_conference_d c,");
                    strSQL.Append(" (select distinct bridgeid, confid, instanceid from (select bridgeid, confid, instanceid from conf_room_d ");
                    strSQL.Append(" union all select bridgeid, confid, instanceid from conf_user_d ) as b) as bb, mcu_list_D m");
                    strSQL.Append(" where c.confid = a.confid and c.instanceid = a.instanceid and ");
                    strSQL.Append(" c.conftype not in (7) and c.confid = bb.confid and c.instanceid = bb.instanceid and bb.bridgeid = m.bridgeid ");
                    strSQL.Append(" FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N''),'-') as [MCU Used]");
                }

                if (confSpeedSelect != "")
                    strSQL.Append(",isnull([Conf Speed],'-') [Conf Speed]");

                if (conProtocolSelect != "")
                    strSQL.Append(",isnull([Conf Protocol],'-') [Conf Protocol]");

                //WO in Conference
                if (confWOSelect != "")
                {
                    strSQL.Append(" ,isnull(Stuff ((SELECT N', ' + Name FROM inv_workorder_d where type = 1 and confid = a.confid and instanceid = a.instanceid"); // ZD 102835
                    strSQL.Append(" FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N''),'-') as [AV WO],");
                    strSQL.Append(" isnull(Stuff ((SELECT N', ' + Name FROM inv_workorder_d where type = 2 and confid = a.confid and instanceid = a.instanceid"); // ZD 102835
                    strSQL.Append(" FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N''),'-') as [CAT WO],");
                    strSQL.Append(" isnull(Stuff ((SELECT N', ' + Name FROM inv_workorder_d where type = 3 and confid = a.confid and instanceid = a.instanceid"); // ZD 102835
                    strSQL.Append(" FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N''),'-') as [HK WO]");
                }

                strSQL.Append(" , case conftype when 6 then 'Audio' when 2 then 'Video' when 7 then 'Room' when 8 then 'Hotdesking' when 4 then 'Pt-to-Pt' End as [Conference Type] ");//FB 2694

                if (confBehaviorFilter != "")
                    strSQL.Append(" , case recuring when 0 then 'Single' when 1 then 'Recurring' End as [Occurrence]"); //ZD 100288

                if (confTimeSelect != "")
                    strSQL.Append(", " + confTimeSelect);

                if (confDurationSelect != "")
                    strSQL.Append(", " + confDurationSelect);

                if (woInchargeSelect != "")
                {
                    strSQL.Append(" ,isnull(Stuff ((SELECT distinct N', ' + ui.firstname + ' ' + ui.Lastname from Inv_WorkOrder_D I, usr_list_D ui where I.adminid = ui.userid and I.Confid = a.confid and I.instanceid = a.instanceid"); // ZD 102835
                    strSQL.Append(" FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N''),'-') as [WO Person-in-Charge]");
                }

                if (rmInchargeSelect.IndexOf('1') >= 0)
                {
                    strSQL.Append(" ,isnull(stuff( (SELECT distinct N', ' + a.Lastname from ");
                    strSQL.Append(" (select (select lastname from usr_list_d u1 where u1.userid = l1.Assistant) as lastname ");
                    strSQL.Append(" from Loc_Room_D l1, conf_room_d c1 where c1.roomid =l1.roomid and c1.confid = a.confid and c1.instanceid = a.instanceid) as a  ");
                    strSQL.Append(" FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N''),'-') as [Rm.Asst.in-Charge] ");
                }

                if (rmInchargeSelect.IndexOf('2') >= 0)
                {
                    strSQL.Append(" ,isnull(stuff( (SELECT distinct N', ' + x.Lastname from ");
                    strSQL.Append(" (select lastname from  ");
                    strSQL.Append(" (select ROW_NUMBER() over (PARTITION BY l1.roomid order by id asc) as 'rowNum', (select lastname from usr_list_d u1 where u1.userid = l1.approverid) as lastname ");
                    strSQL.Append(" from Loc_Approver_D l1, conf_room_d c1 where c1.roomid =l1.roomid and c1.confid = a.confid and c1.instanceid = a.instanceid) as a where rownum = 1) as x ");
                    strSQL.Append(" FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N''),'-') as [Rm.Pri.Appr.Name] ");
                }

                if (rmInchargeSelect.IndexOf('3') >= 0)
                {
                    strSQL.Append(" ,isnull(stuff( (SELECT distinct N', ' + x.Lastname from ");
                    strSQL.Append(" (select lastname from  ");
                    strSQL.Append(" (select ROW_NUMBER() over (PARTITION BY l1.roomid order by id asc) as 'rowNum', (select lastname from usr_list_d u1 where u1.userid = l1.approverid) as lastname ");
                    strSQL.Append(" from Loc_Approver_D l1, conf_room_d c1 where c1.roomid =l1.roomid and c1.confid = a.confid and c1.instanceid = a.instanceid) as a where rownum = 2) as x ");
                    strSQL.Append(" FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N''),'-') as [Rm.Sec.Appr.Name 1] ");
                }

                if (rmInchargeSelect.IndexOf('4') >= 0)
                {
                    strSQL.Append(" ,isnull(stuff( (SELECT distinct N', ' + x.Lastname from ");
                    strSQL.Append(" (select lastname from  ");
                    strSQL.Append(" (select ROW_NUMBER() over (PARTITION BY l1.roomid order by id asc) as 'rowNum', (select lastname from usr_list_d u1 where u1.userid = l1.approverid) as lastname ");
                    strSQL.Append(" from Loc_Approver_D l1, conf_room_d c1 where c1.roomid =l1.roomid and c1.confid = a.confid and c1.instanceid = a.instanceid) as a where rownum = 3) as x ");
                    strSQL.Append(" FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N''),'-') as [Rm.Sec.Appr.Name 2] ");
                }

                if (mcuInchargeSelect.IndexOf('1') >= 0)
                {
                    strSQL.Append(" ,isnull(stuff( (SELECT distinct N', ' + a.Lastname from ");
                    strSQL.Append(" (select (select lastname from usr_list_d u1 where u1.userid = l1.Admin) as lastname ");
                    strSQL.Append(" from mcu_list_d l1, conf_room_d c1 where c1.bridgeid =l1.bridgeid and c1.confid = a.confid and c1.instanceid = a.instanceid) as a  ");
                    strSQL.Append(" FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N''),'-') as [MCU Admin] ");
                }

                if (mcuInchargeSelect.IndexOf('2') >= 0)
                {
                    strSQL.Append(" ,isnull(stuff( (SELECT distinct N', ' + x.Lastname from (select lastname from  ");
                    strSQL.Append(" (select ROW_NUMBER() over (PARTITION BY l1.mcuid order by id asc) as 'rowNum', (select lastname from usr_list_d u1 where u1.userid = l1.approverid) as lastname ");
                    strSQL.Append(" from Mcu_Approver_D l1, conf_room_d c1 where c1.bridgeid =l1.mcuid and c1.confid = a.confid and c1.instanceid = a.instanceid) as a where rownum = 1) as x ");
                    strSQL.Append(" FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N''),'-') as [MCU.Pri.Appr.Name] ");
                }

                if (mcuInchargeSelect.IndexOf('3') >= 0)
                {
                    strSQL.Append(" ,isnull(stuff( (SELECT distinct N', ' + x.Lastname from (select lastname from  ");
                    strSQL.Append(" (select ROW_NUMBER() over (PARTITION BY l1.mcuid order by id asc) as 'rowNum', (select lastname from usr_list_d u1 where u1.userid = l1.approverid) as lastname ");
                    strSQL.Append(" from Mcu_Approver_D l1, conf_room_d c1 where c1.bridgeid =l1.mcuid and c1.confid = a.confid and c1.instanceid = a.instanceid) as a where rownum = 2) as x ");
                    strSQL.Append(" FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N''),'-') as [MCU.Sec.Appr.Name 1] ");
                }

                if (mcuInchargeSelect.IndexOf('4') >= 0)
                {
                    strSQL.Append(" ,isnull(stuff( (SELECT distinct N', ' + x.Lastname from (select lastname from  ");
                    strSQL.Append(" (select ROW_NUMBER() over (PARTITION BY l1.mcuid order by id asc) as 'rowNum', (select lastname from usr_list_d u1 where u1.userid = l1.approverid) as lastname ");
                    strSQL.Append(" from Mcu_Approver_D l1, conf_room_d c1 where c1.bridgeid =l1.mcuid and c1.confid = a.confid and c1.instanceid = a.instanceid) as a where rownum = 3) as x ");
                    strSQL.Append(" FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N''),'-') as [MCU.Sec.Appr.Name 2] ");
                }
                //FB 2654
                if (concierge != "")
                {
                    String[] conciergeAry = concierge.Split(',');
                    String temp = "";
                    for (Int32 i = 0; i < conciergeAry.Length; i++)
                    {
                        if (conciergeAry[i] == "O")
                            temp = "case when OnSiteAVSupport = 0 then 0 else a.Duration end as [On-Site A/V Support(Mins)]";
                        else if (conciergeAry[i] == "M")
                            temp = "case when MeetandGreet = 0 then 0 else a.Duration end as [Meet and Greet(Mins)]";
                        else if (conciergeAry[i] == "C")
                            temp = "case when ConciergeMonitoring = 0 then 0 else a.Duration end as [Call Monitoring(Mins)]";//FB 3023
                        else if (conciergeAry[i] == "D")
                            temp = "case when DedicatedVNOCOperator = 0 then 0 else a.Duration end as [Dedicated VNOC Operator(Mins)]";

                        strSQL.Append(", " + temp);
                    }
                }

                //ZD 101950
                if (confVMR != "")
                    strSQL.Append(",case when isVMR  = 1 then 'Personal' when isVMR = 2 then 'Room' when isVMR = 3 then 'External' end as [VMR Type]");

                strSQL.Append(", a.ConfID,1 as userid, 1 as roomid, 1 as endpointid, 1 as bridgeid  ,rd.PartyName as [Room Name],rd.NoofAttendee as [# of Attendees.] "); //FB 3066 //ZD 104686
				//ZD 104686
                if (customOptions != "" && stmtCus1 != "")
                    strSQL.Append(" ,selectedValue,Displaytitle ");

                strSQL.Append(" from conf_conference_d a left outer join Conf_Room_D rd on a.confid = rd.ConfID and a.instanceid = rd.instanceID ");//ZD 104686

                if (isPart)
                    strSQL1.Append(" from conf_conference_d a "); //for Participant table

                if (isHost)
                {
                    //ZD 103878
                    strSQL.Append(" left outer join (select LastName,firstname, UserID,roleid,Email from  usr_list_d) h left outer join Usr_Roles_D hr on h.roleid = hr.roleid on h.UserID = a.owner ");
                    strSQL.Append(" left outer join (select LastName,firstname, id,roleid,Email from  Usr_Deletelist_D) hd left outer join Usr_Roles_D hdr on hd.roleid = hdr.roleid on hd.id = a.owner ");
                }
                if (conProtocolSelect != "" || confSpeedSelect != "")
                {
                    strSQL.Append(" left outer join ( select distinct LIneRateType as [Conf Speed], VideoProtocolType as [Conf Protocol], confid ");
                    strSQL.Append(" from conf_room_D a, Gen_LineRate_S s, Gen_VideoProtocol_S g");
                    strSQL.Append(" where g.VideoProtocolID = a.DefVideoProtocol and a.DefLineRate = s.LineRateID ) as sp ");
                    strSQL.Append(" on a.confid = sp.confid and a.conftype not in (7)");
                }

                if (isPart || roomsPartyFilter != "" || attendeesFilter != "") //participants & internal /External
                {
                    StringBuilder strTemp = new StringBuilder();
                    strTemp.Append(" left outer join (");

                    strTemp.Append(" select confid,instanceid,firstname,lastname,Email,p.userid,Attended,dbo.changeTime(" + tmZone + ", MeetingSigninTime)[MeetingSigninTime] "); //102131
                    strTemp.Append(" from conf_user_d p, usr_list_d b left outer join usr_roles_d r on b.roleid = r.roleid");
                    strTemp.Append(" where p.userid = b.userid ");
                    //internal /External
                    if (roomsPartyFilter != "")
                        strTemp.Append(" and " + roomsPartyFilter);

                    if (attendeesFilter == "" || attendeesFilter.IndexOf('2') >= 0)
                    {
                        strTemp.Append(" Union all");
                        strTemp.Append(" select confid,instanceid,firstname,lastname,Email,p.userid,Attended,dbo.changeTime(" + tmZone + ", MeetingSigninTime)[MeetingSigninTime] ");//102131
                        strTemp.Append(" from conf_user_d p, Usr_GuestList_D b left outer join usr_roles_d r on b.roleid = r.roleid");
                        strTemp.Append(" where p.userid = b.userid  ");
                        //internal /External
                        if (roomsPartyFilter != "")
                            strTemp.Append(" and " + roomsPartyFilter);
                    }

                    strTemp.Append(" ) as p on a.Confid = p.Confid and p.instanceid = a.instanceid ");

                    strSQL.Append(strTemp.ToString());
                    if (isPart)
                        strSQL1.Append(strTemp.ToString()); //for Participant table
                }

                //strSQL.Append(" , Gen_TimeZone_S gen ");
                strSQL.Append(" left outer join conf_bridge_d br on a.confid= br.confid and a.instanceid = br.instanceid "); //FB 2870

                //ZD 104686
                if (customOptions != "" && stmtCus1 != "")
                {
                    strSQL.Append(" left outer join (select m.confid as custconfid ");
                    strSQL.Append(" , case ltrim(rtrim(m.selectedValue)) when '0'  then 'No' when '1'  then 'Yes'  when '' then 'N/A' else ltrim(rtrim(m.selectedValue))  end AS selectedValue ");
                    strSQL.Append(" , d.Displaytitle as Displaytitle from Conf_CustomAttr_D m, dept_CustomAttr_D d  ");
                    strSQL.Append(" where m.CustomAttributeID = d.CustomAttributeID AND d.deleted=0 And d.Status=0 and d.CustomAttributeID in (" + customOptions + ") ");
                    strSQL.Append(" )z on z.custconfid = a.confid  ");
                }

                if (departmentid != "")
                    strSQL.Append(" , conf_room_D b, Loc_Department_D c ");

                

                //Common where condition  Start
                //strSQL.Append(" where a.confid not in(11) and a.Timezone = gen.Timezoneid ");
                strSQL.Append(" where a.confid not in(11) "); //FB 2870
                //ZD 103878
                //if (strorgid != "")
                //    strSQL.Append(" and a." + strorgid);

                if (departmentid != "")
                    strSQL.Append(" and a.Confid = b.Confid and a.instanceid = b.instanceid and b.Roomid = c.Roomid and c.Departmentid in (" + departmentid + ")");

                if (isHost)
                    strSQL.Append(" and h.userid = a.owner ");

                if (scheduledFilter != "")
                    strSQL.Append(" and " + scheduledFilter);

                if (confBehaviorFilter != "")
                    strSQL.Append(" and " + confBehaviorFilter);

                if (confTypeFilter != "")
                    strSQL.Append(" and " + confTypeFilter);
                //ZD 101950
                if (confVMR != "")
                    strSQL.Append(" and isVMR in (" + confVMR + " )");

                if (startDate != "" && endDate != "")
                {
                    strSQL.Append(" AND a.confdate BETWEEN dbo.changeTOGMTtime(" + tmZone + ",'" + startDate + "') ");
                    strSQL.Append(" AND dbo.changeTOGMTtime(" + tmZone + ",'" + endDate + "') ");
                }
                //ZD 104686
                if (customOptions != "" && stmtCus1 != "")
                {
                    strSQL.Append(" ) AS TableToBePivoted ");
                    strSQL.Append(" PIVOT ( max(selectedValue) FOR Displaytitle IN (" + stmtCus1 + ")");
                    strSQL.Append(" ) AS PivotedTable  ");
                }

                //ZD 101835
                strSQL.Append(" Union All " + strSQL.ToString().Replace("conf_room_d", "Archive_conf_room_d").Replace("conf_conference_d", "Archive_conf_conference_d").Replace("conf_user_d", "Archive_conf_user_d").Replace("conf_bridge_d", "Archive_conf_bridge_d"));
                strSQL.Append(" Order by Confdate");
                if (isPart)
                {
                    strSQL1.Append(" Union All " + strSQL1.ToString().Replace("conf_room_d", "Archive_conf_room_d").Replace("conf_conference_d", "Archive_conf_conference_d").Replace("conf_user_d", "Archive_conf_user_d").Replace("conf_bridge_d", "Archive_conf_bridge_d"));
                    strSQL1.Append(" Order by confid");
                }

                // ZD 102835 Start
                if (confWOSelect != "")
                {
                    strSQL1.Append(" ; ");
                    strSQL1.Append(" select a.confnumname [workorder_conferenceid], wo.type, wo.name [WO Name], case wo.Type when 1 then ila.Name when 2 then ilc.name when 3 then ilk.Name end as [Inventory Item] ");
                    strSQL1.Append(" ,wi.quantity [Quantity],wi.deliveryCost[Delivery Cost], wi.serviceCharge[Service Charge], wo.Comment[Comment], r.Name as Room ");
                    strSQL1.Append(" ,(select FirstName + ' ' + LastName from Usr_List_D uu where uu.UserID = wo.AdminId) as [WO Person-in-Charge] ");
                    strSQL1.Append(" from conf_conference_d a, usr_list_d h left outer join Usr_Roles_D hr on h.roleid = hr.roleid ");
                    strSQL1.Append(" ,Inv_WorkOrder_D wo, Inv_WorkItem_D wi ");
                    strSQL1.Append(" left outer join Inv_ItemList_AV_D ILA on wi.ItemID = ila.ID ");
                    strSQL1.Append(" left outer join Inv_Menu_D ILC on wi.ItemID =ilc.id ");
                    strSQL1.Append(" left outer join Inv_ItemList_HK_D ILK on wi.ItemID =ILK.ID ");
                    strSQL1.Append(" , Loc_Room_D r ");
                    strSQL1.Append(" where a.confid not in(11) and a.owner = h.UserID ");
                    strSQL1.Append(" and a.confid = wo.ConfID and a.instanceid = wo.InstanceID and wo.Type in (1,2,3) ");
                    strSQL1.Append(" and wo.RoomID = r.RoomID and wo.ID = wi.WorkOrderID ");
                    // AND a.confdate BETWEEN dbo.changeTOGMTtime(26,'01/01/2012')  AND dbo.changeTOGMTtime(26,'03/09/2015')
                    if (startDate != "" && endDate != "")
                    {
                        strSQL1.Append(" and a.confdate BETWEEN dbo.changeTOGMTtime(" + tmZone + ",'" + startDate + "') ");
                        strSQL1.Append(" and dbo.changeTOGMTtime(" + tmZone + ",'" + endDate + "') ");
                    }
                    if (orgid != "11")
                        strSQL1.Append(" and a.orgid = " + orgid);
                    strSQL1.Append(" order by [workorder_conferenceid] ");
                }
                // ZD 102835 End
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
            }
        }

        #endregion

        #region GetUserConferenceReport

        private void GetUserConferenceReport(ref StringBuilder strSQL, ref StringBuilder strSQL1, String userCommon, String departmentid, String hostSelect
          , String confTypeFilter, String partSelect, Boolean isPart, String isDomino, String isExchange, String rmInchargeSelect, String mcuInchargeSelect, String woInchargeSelect
            , String startDate, String endDate, String tmZone, String strorgid) //FB 2593
        {
            try
            {
                strSQL.Append(" select distinct a.userid, 1 as ConfID, 1 as bridgeid,1 as roomid, 1 as endpointid ");//FB 3066

                userCommon = userCommon.Replace("d.StandardName as [Time Zone]", "d.TimeZoneDiff + ' ' + d.StandardName as [Time Zone]");

                if (userCommon != "")
                    strSQL.Append("," + userCommon);
                else
                    strSQL.Append(", a.FirstName as [First Name], a.LastName as [Last Name]"); //FB 2808

                //strSQL.Append(", a.LastName, a.FirstName, a.Email, a.AlternativeEmail, c.RoleName, a.timezone, d.StandardName ");
                //strSQL.Append(", a.AccountExpiry, e.Timeremaining ");

                if (departmentid != "")
                {
                    strSQL.Append(",Stuff ((SELECT distinct N', ' + d1.departmentname from  Usr_Dept_D c1, dept_list_d d1 where d1.Departmentid = c1.Departmentid and d1.Departmentid in (" + departmentid + ")");
                    strSQL.Append(" and c1.userid = a.userid FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N'') as [Department Name]  ");
                }

                if (isExchange != "")
                    strSQL.Append(" , case enableExchange when 1 then 'Yes' else 'No' end as Exchange ");

                if (isDomino != "")
                    strSQL.Append(" , case enableDomino when 1 then 'Yes' else 'No' end as Domino ");

                strSQL.Append(" from usr_list_d a, Usr_Dept_D b, Usr_Roles_D c, Gen_TimeZone_S d, Acc_Balance_D e ");
                strSQL.Append(" where a.userid = b.userid and a.Roleid = c.Roleid and a.Timezone = d.Timezoneid  ");
                strSQL.Append(" and a.userid = e.userid and a.deleted = 0 ");
                if (departmentid != "")
                    strSQL.Append(" and b.departmentid in (" + departmentid + ")");

                //FB 2593
                if (strorgid != "")
                {
                    strorgid = strorgid.Replace("OrgID", "companyid");
                    strSQL.Append(" and a." + strorgid);
                }
                //if (isExchange != "")
                //    strSQL.Append(" and enableExchange = 1");

                //if (isDomino != "")
                //    strSQL.Append(" and enableDomino = 1");

                //if (userCommon != "")
                //    strSQL.Append(" order by a.LastName ");

                //Second Table
                strSQL1.Append(" select distinct a.confdate as [Date of Conf.], a.userid, ConfNumname as [Conf ID], ExternalName as [Conf.Title] "); //ZD 100288

                if (hostSelect != "")
                    strSQL1.Append(", " + hostSelect);
                //strSQL1.Append(" ,h.LastName as HostName ");

                strSQL1.Append(" , case conftype when 6 then 'Audio' when 2 then 'Video' when 7 then 'Room' when 8 then 'Hotdesking' when 4 then 'Pt-to-Pt' End as [Conference Type],a.Attended as [Attended Conference],dbo.changeTime(" + tmZone + ", a.MeetingSigninTime) as [Date/Time Signed-In] ");//FB 2694 //ZD 102131

                //ALLDEC-807 Starts
                //if (rmInchargeSelect.IndexOf('1') >= 0)
                //{
                //    strSQL1.Append(" ,isnull(stuff( (SELECT distinct N', ' + a.Lastname from ");
                //    strSQL1.Append(" (select (select lastname from usr_list_d u1 where u1.userid = l1.Assistant) as lastname ");
                //    strSQL1.Append(" from Loc_Room_D l1, conf_room_d c1 where c1.roomid =l1.roomid and c1.confid = a.confid and c1.instanceid = a.instanceid) as a  ");
                //    strSQL1.Append(" FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N''),'-') as [Rm.Asst.in-Charge] ");
                //}
                if (rmInchargeSelect.IndexOf('1') >= 0)
                {
                    strSQL1.Append(" isnull(stuff( (SELECT distinct N', ' + x.Lastname from  (select lastname from   (select ROW_NUMBER() over (PARTITION BY l1.roomid order by id asc) as 'rowNum', ");
                    strSQL1.Append(" ((select lastname from usr_list_d u1 where u1.userid = l1.Assistant1) as lastname  from ");
                    strSQL1.Append(" (select roomid,(select top 1 AssistantId from Loc_Assistant_D la where la.RoomId = r.RoomID order by la.ID) as Assistant1 from loc_room_d r ) l1, conf_room_d ");
                    strSQL1.Append(" c1 where c1.roomid =l1.roomid and c1.confid = a.confid and c1.instanceid = a.instanceid) as a where rownum = 1) as x ");
                    strSQL1.Append(" FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N''),'-') as [Rm.Asst.in-Charge]   ");
                }
                //ALLDEC-807 Ends
               
                if (rmInchargeSelect.IndexOf('2') >= 0)
                {
                    strSQL1.Append(" ,isnull(stuff( (SELECT distinct N', ' + x.Lastname from ");
                    strSQL1.Append(" (select lastname from  ");
                    strSQL1.Append(" (select ROW_NUMBER() over (PARTITION BY l1.roomid order by id asc) as 'rowNum', (select lastname from usr_list_d u1 where u1.userid = l1.approverid) as lastname ");
                    strSQL1.Append(" from Loc_Approver_D l1, conf_room_d c1 where c1.roomid =l1.roomid and c1.confid = a.confid and c1.instanceid = a.instanceid) as a where rownum = 1) as x ");
                    strSQL1.Append(" FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N''),'-') as [Rm.Pri.Appr.Name] ");
                }

                if (rmInchargeSelect.IndexOf('3') >= 0)
                {
                    strSQL1.Append(" ,isnull(stuff( (SELECT distinct N', ' + x.Lastname from ");
                    strSQL1.Append(" (select lastname from  ");
                    strSQL1.Append(" (select ROW_NUMBER() over (PARTITION BY l1.roomid order by id asc) as 'rowNum', (select lastname from usr_list_d u1 where u1.userid = l1.approverid) as lastname ");
                    strSQL1.Append(" from Loc_Approver_D l1, conf_room_d c1 where c1.roomid =l1.roomid and c1.confid = a.confid and c1.instanceid = a.instanceid) as a where rownum = 2) as x ");
                    strSQL1.Append(" FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N''),'-') as [Rm.Sec.Appr.Name 1] ");
                }

                if (rmInchargeSelect.IndexOf('4') >= 0)
                {
                    strSQL1.Append(" ,isnull(stuff( (SELECT distinct N', ' + x.Lastname from ");
                    strSQL1.Append(" (select lastname from  ");
                    strSQL1.Append(" (select ROW_NUMBER() over (PARTITION BY l1.roomid order by id asc) as 'rowNum', (select lastname from usr_list_d u1 where u1.userid = l1.approverid) as lastname ");
                    strSQL1.Append(" from Loc_Approver_D l1, conf_room_d c1 where c1.roomid =l1.roomid and c1.confid = a.confid and c1.instanceid = a.instanceid) as a where rownum = 3) as x ");
                    strSQL1.Append(" FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N''),'-') as [Rm.Sec.Appr.Name 2] ");
                }

                if (mcuInchargeSelect.IndexOf('1') >= 0)
                {
                    strSQL1.Append(" ,isnull(stuff( (SELECT distinct N', ' + a.Lastname from ");
                    strSQL1.Append(" (select (select lastname from usr_list_d u1 where u1.userid = l1.Admin) as lastname ");
                    strSQL1.Append(" from mcu_list_d l1, conf_room_d c1 where c1.bridgeid =l1.bridgeid and c1.confid = a.confid and c1.instanceid = a.instanceid) as a  ");
                    strSQL1.Append(" FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N''),'-') as [MCU Admin] ");
                }

                if (mcuInchargeSelect.IndexOf('2') >= 0)
                {
                    strSQL1.Append(" ,isnull(stuff( (SELECT distinct N', ' + x.Lastname from (select lastname from  ");
                    strSQL1.Append(" (select ROW_NUMBER() over (PARTITION BY l1.mcuid order by id asc) as 'rowNum', (select lastname from usr_list_d u1 where u1.userid = l1.approverid) as lastname ");
                    strSQL1.Append(" from Mcu_Approver_D l1, conf_room_d c1 where c1.bridgeid =l1.mcuid and c1.confid = a.confid and c1.instanceid = a.instanceid) as a where rownum = 1) as x ");
                    strSQL1.Append(" FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N''),'-') as [MCU.Pri.Appr.Name] ");
                }

                if (mcuInchargeSelect.IndexOf('3') >= 0)
                {
                    strSQL1.Append(" ,isnull(stuff( (SELECT distinct N', ' + x.Lastname from (select lastname from  ");
                    strSQL1.Append(" (select ROW_NUMBER() over (PARTITION BY l1.mcuid order by id asc) as 'rowNum', (select lastname from usr_list_d u1 where u1.userid = l1.approverid) as lastname ");
                    strSQL1.Append(" from Mcu_Approver_D l1, conf_room_d c1 where c1.bridgeid =l1.mcuid and c1.confid = a.confid and c1.instanceid = a.instanceid) as a where rownum = 2) as x ");
                    strSQL1.Append(" FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N''),'-') as [MCU.Sec.Appr.Name 1] ");
                }

                if (mcuInchargeSelect.IndexOf('4') >= 0)
                {
                    strSQL1.Append(" ,isnull(stuff( (SELECT distinct N', ' + x.Lastname from (select lastname from  ");
                    strSQL1.Append(" (select ROW_NUMBER() over (PARTITION BY l1.mcuid order by id asc) as 'rowNum', (select lastname from usr_list_d u1 where u1.userid = l1.approverid) as lastname ");
                    strSQL1.Append(" from Mcu_Approver_D l1, conf_room_d c1 where c1.bridgeid =l1.mcuid and c1.confid = a.confid and c1.instanceid = a.instanceid) as a where rownum = 3) as x ");
                    strSQL1.Append(" FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N''),'-') as [MCU.Sec.Appr.Name 2] ");
                }

                strSQL1.Append(" from( ");
                strSQL1.Append(" select a.confdate, a.confid, a.instanceid, ConfNumname, ExternalName, a.owner as host, a.owner as Userid,conftype, ");
                strSQL1.Append(" cu.Attended,cu.MeetingSigninTime");
                strSQL1.Append(" from conf_conference_d a ,conf_user_d cu where a.confid= cu.confid and a.instanceid =cu.InstanceId and a.userid = cu.UserID and");//ZD 102131
                strSQL1.Append(" a.owner in (select distinct a.userid from usr_list_d a, Usr_Dept_D b where a.userid = b.userid) and a.confid not in (11)");
                if (confTypeFilter != "")
                    strSQL1.Append(" and " + confTypeFilter);
                strSQL1.Append(" union all  ");
                strSQL1.Append(" select a.confdate, a.confid, a.instanceid, ConfNumname, ExternalName, a.owner as host, p.userid as Userid ,conftype,Attended,MeetingSigninTime ");//ZD 102131
                strSQL1.Append(" from conf_conference_d a  ");

                StringBuilder strTemp = new StringBuilder();

                strTemp.Append(" left outer join (  ");
                strTemp.Append(" select confid,instanceid,firstname,lastname,Email,rolename,p.userid,Attended,MeetingSigninTime"); //'Staff' as [Company Relationship]   "); ZD 102131
                strTemp.Append(" from conf_user_d p, usr_list_d b left outer join usr_roles_d r on b.roleid = r.roleid where p.userid = b.userid   ");
                
                strTemp.Append(" Union all");

                strTemp.Append(" select confid,instanceid,firstname,lastname,Email,rolename,p.userid,Attended,MeetingSigninTime"); //'Guest' as [Company Relationship] "); //ZD 102131
                strTemp.Append(" from conf_user_d p, Usr_GuestList_D b left outer join usr_roles_d r on b.roleid = r.roleid");
                strTemp.Append(" where p.userid = b.userid  ");

                strTemp.Append(" ) as p on a.Confid = p.Confid and p.instanceid = a.instanceid   ");

                strSQL1.Append(strTemp.ToString());
                
                strSQL1.Append(" where p.userid in (select distinct a.userid from usr_list_d a, Usr_Dept_D b where a.userid = b.userid) ");
                if (confTypeFilter != "")
                    strSQL1.Append(" and " + confTypeFilter);

                strSQL1.Append(" ) as a , usr_list_d h where a.host = h.userid  ");
                
                if (startDate != "" && endDate != "")
                {
                    strSQL1.Append(" AND a.confdate BETWEEN dbo.changeTOGMTtime(" + tmZone + ",'" + startDate + "') ");
                    strSQL1.Append(" AND dbo.changeTOGMTtime(" + tmZone + ",'" + endDate + "') ");
                }
                //ZD 101835
                strSQL1.Append(" Union All " + strSQL1.ToString().Replace("conf_room_d", "Archive_conf_room_d").Replace("conf_conference_d", "Archive_conf_conference_d").Replace("conf_user_d", "Archive_conf_user_d").Replace("conf_bridge_d", "Archive_conf_bridge_d"));

                strSQL1.Append(" order by a.confdate ");

            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
            }
        }

        #endregion

        #region GetRoomConferenceReport

        private void GetRoomConferenceReport(ref StringBuilder strSQL, ref StringBuilder strSQL1, String type, String userCommon, String hostSelect, String roomType,
            String rmImmediate, String subConfSelect, String eptDetails, String mcuDetails, String confBehaviorFilter, String woDetails, String scheduledFilter,
            String confTypeFilter, String rmAsset, String rmInchargeSelect, String departmentid, String startDate, String endDate, String tmZone,
            String immediate, String tierInfo, String eptcolDetails, String strorgid)
        {
            try
            {
                if(type == "4" || type == "5")
                    strSQL.Append(" select distinct 1, 1 as ConfID, 1 as userid");//FB 3066
                else
                    strSQL.Append(" select distinct roomid, 1 as ConfID, 1 as userid ");//FB 3066

                if (type == "3")
                    strSQL.Append(",1 as endpointid, 1 as bridgeid"); //FB 3066

                if (userCommon != "" && type == "3")
                    strSQL.Append(", " + userCommon);

                if (type == "4")
                    strSQL.Append(",endpointid, 1 as bridgeid"); //FB 3066

                if (type == "4" && eptcolDetails == "")
                    strSQL.Append(",[Default Profile Name]");

                if (type == "5")
                    strSQL.Append(",bridgeid, 1 as endpointid"); //FB 3066

                if (type == "5" && eptcolDetails.IndexOf("MCU Assignment") < 0)
                    strSQL.Append(",[MCU Assignment]");

                if (eptcolDetails != "")
                    strSQL.Append(", " + eptcolDetails);

                if (userCommon != "" && (type == "4" || type == "5")) //For using again in this line for give to preference for Endpoint Columns
                    strSQL.Append(", " + userCommon);

                strSQL.Append(" from ( select distinct a.roomid, a.[Name], (select [Name] from Loc_Tier3_D l3 where l3.id = a.l3LocationID) as [Top Tier] ");
                strSQL.Append(" , (select [Name] from Loc_Tier2_D l2 where l2.id = a.l2LocationID) as [Middle Tier] ");
                strSQL.Append(" , RoomPhone, Capacity, Address1, Address2, RoomFloor, RoomNumber, City ");
                strSQL.Append(" , (select State from Gen_State_S s where s.stateid = a.State) as State ");
                strSQL.Append(" , (select CountryName from Gen_Country_S c where c.Countryid = a.country) as Country ");
                strSQL.Append(" , ZipCode, (select StandardName from Gen_TimeZone_S g where g.timezoneid = a.timezoneid) as TimeZone ");

                strSQL.Append(" ,(select lastname from (select ROW_NUMBER() over (PARTITION BY l1.roomid order by id asc) as 'rowNum',  ");
                strSQL.Append(" (select lastname from usr_list_d u1 where u1.userid = l1.approverid) as lastname ");
                strSQL.Append(" from Loc_Approver_D l1 where l1.roomid = a.roomid) as a where rownum = 1) as [Approver 1] ");

                strSQL.Append(" ,(select lastname from (select ROW_NUMBER() over (PARTITION BY l1.roomid order by id asc) as 'rowNum',  ");
                strSQL.Append(" (select lastname from usr_list_d u1 where u1.userid = l1.approverid) as lastname ");
                strSQL.Append(" from Loc_Approver_D l1 where l1.roomid = a.roomid) as a where rownum = 2) as [Approver 2] ");

                strSQL.Append(" ,(select lastname from (select ROW_NUMBER() over (PARTITION BY l1.roomid order by id asc) as 'rowNum',  ");
                strSQL.Append(" (select lastname from usr_list_d u1 where u1.userid = l1.approverid) as lastname ");
                strSQL.Append(" from Loc_Approver_D l1 where l1.roomid = a.roomid) as a where rownum = 3) as [Approver 3] ");

                if(type == "4")
                    strSQL.Append(" , ep.Name as [Endpoint Name], ep.profileName as [Default Profile Name], ep.endpointid ,'' as [MCU Assignment]");
                
                if (type == "5")
                    strSQL.Append(",(select BridgeName from mcu_list_d m where m.bridgeid = ep.bridgeid) as [MCU Assignment], ep.Name as [Endpoint Name], ep.profileName as [Default Profile Name], ep.endpointid,ep.bridgeid");
                
                if (eptcolDetails != "")
                {
                    strSQL.Append(" , (select count(*) from ept_list_d el where el.endpointid = a.endpointid) as [Number of Profiles] ");
                    strSQL.Append(" ,  (select name from Gen_AddressType_S s where ep.addresstype = s.id) as [Address Type] , ep.Address, ");
                    strSQL.Append(" (select VEname from Gen_VideoEquipment_S v where ep.videoequipmentid = v.veid) as Model");
                    strSQL.Append(" ,(select LInerateType from Gen_LineRate_S v where ep.linerateid = v.linerateid) as [Preferred Bandwidth]");
                    
                    strSQL.Append(" ,(select DialingOption from Gen_DialingOption_S do where do.id = ep.connectiontype) as [Pre. Dialing Option]");
                    strSQL.Append(" ,(select VideoProtocolType from Gen_VideoProtocol_S v where v.videoprotocolid= ep.protocol ) as [Default Protocol]");
                    strSQL.Append(" ,endptURL as [Web Access URL] , case outsidenetwork when 1 then 'Outside' else '' end as [Network Location]");
                    strSQL.Append(" , case TelnetAPI when 1 then 'Yes' else 'No' end as [Telnet Enabled],ExchangeID as [Email ID], APIPortNO as [API Port] ");
                    strSQL.Append(" , case Calendarinvite when 1 then 'Yes' else 'No' end as [iCal Invite]");
                }

                strSQL.Append(" from loc_room_D a ");

                if (type == "4" || type == "5")
                    strSQL.Append(" left outer join ept_list_d ep on ep.endpointid = a.endpointid and ep.[name] <> '' and (profileid = 0 or isdefault = 1) ");

                if(type == "4") //FB 2593
                    strSQL.Append(", Loc_Department_D c where a.roomid = c.roomid   ");
                else
                    strSQL.Append(" where 1 = 1  "); //FB 2808

                if (roomType != "")
                    strSQL.Append(" and " + roomType);

                if (departmentid != "" && type == "4") //FB 2593
                    strSQL.Append(" and c.departmentid in (" + departmentid + ") ");

                if (tierInfo != "")
                    strSQL.Append(" and L3LocationID in (" + tierInfo + ")");

                //FB 2593
                if (strorgid != "")
                    strSQL.Append(" and a." + strorgid);

                strSQL.Append(" and a.Roomid not in (11) and a.disabled = 0 and a.extroom = 0 and a.isvmr = 0) as a "); //FB 2593

                if (type == "4" || type == "5") 
                    strSQL.Append(" where [Default Profile Name] <> '' ");

                //Second Table
                strSQL1.Append(" select distinct b.roomid,b.bridgeid, ConfNumName as [Conf ID], ExternalName as [Conf.Title],b.endpointid "); //ZD 100288

                if (confBehaviorFilter != "")
                    strSQL1.Append(" , case recuring when 0 then 'Single' when 1 then 'Recurring' End as [Single/Recuring]"); //ZD 100288

                if (eptDetails != "")
                {
                    strSQL1.Append(" ,Stuff ((SELECT distinct N', ' + e.name ");
                    if (eptDetails.IndexOf("2") >= 0)
                        strSQL1.Append(" + ' > ' + s.[name] ");
                    if (eptDetails.IndexOf("3") >= 0)
                        strSQL1.Append(" + ' - ' + r.ipisdnaddress ");
                    if (eptDetails.IndexOf("4") >= 0)
                        strSQL1.Append(" + ' - '+ do.DialingOption  ");
                    strSQL1.Append(" from conf_conference_d c ,conf_room_d r, loc_room_d l, Ept_List_D e, Gen_AddressType_S s , Gen_DialingOption_S do   ");
                    strSQL1.Append(" where c.confid = a.confid and c.instanceid = a.instanceid  and c.conftype not in (7) and c.confid = r.confid and r.roomid = l.roomid   ");
                    strSQL1.Append(" and l.endpointid = e.endpointid  and r.addresstype = s.id and do.id = r.connectiontype and e.isdefault = 1  ");
                    strSQL1.Append(" FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N'') as [Endpoints in Conf");
                    strSQL1.Append(" > Name");
                    if (eptDetails.IndexOf("2") >= 0)
                        strSQL1.Append(" - IP ");
                    if (eptDetails.IndexOf("3") >= 0)
                        strSQL1.Append(" - Add ");
                    if (eptDetails.IndexOf("4") >= 0)
                        strSQL1.Append(" - Dial.Opt. ");
                    strSQL1.Append("]");
                }

                if (mcuDetails.IndexOf("2") >= 0)
                {
                    strSQL1.Append(" ,Stuff ((SELECT distinct N', ' + m.BridgeName  ");
                    strSQL1.Append(" from conf_conference_d c ,conf_room_d r, loc_room_d l, Ept_List_D e, mcu_list_D m  ");
                    strSQL1.Append(" where c.confid = a.confid and c.instanceid = a.instanceid  and c.conftype not in (7) and c.confid = r.confid   ");
                    strSQL1.Append(" and r.roomid = l.roomid and l.endpointid = e.endpointid  and e.bridgeid = m.bridgeid and e.isdefault = 1  ");
                    strSQL1.Append(" FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N'') as [Default MCU]   ");
                }

                if (mcuDetails.IndexOf("1") >= 0 || mcuDetails.IndexOf("3") >= 0)
                {
                    strSQL1.Append(" ,Stuff ((SELECT distinct N', ' + m.bridgename ");
                    if (mcuDetails.IndexOf("3") >= 0)
                        strSQL1.Append(" + ' > '+ bridgeIPISDNAddress ");
                    strSQL1.Append(" from conf_conference_d c, (select distinct bridgeid, confid,instanceid,bridgeIPISDNAddress from (select bridgeid, confid, instanceid, bridgeIPISDNAddress from conf_room_d    ");
                    strSQL1.Append(" union all select bridgeid, confid, instanceid, bridgeIPISDNAddress from conf_user_d ) as b) as bb, mcu_list_D m   ");
                    strSQL1.Append(" where c.confid = a.confid and c.instanceid = a.instanceid  and  c.conftype not in (7) and c.confid = bb.confid     ");
                    strSQL1.Append(" and c.instanceid = bb.instanceid and bb.bridgeid = m.bridgeid  ");
                    strSQL1.Append(" FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N'') as [MCU Used in Conf");
                    if (mcuDetails.IndexOf("3") >= 0)
                        strSQL1.Append(" > Address");
                    strSQL1.Append("  ]");
                }

                if (woDetails.IndexOf("1") >= 0)
                {
                    strSQL1.Append(" ,Stuff ((SELECT N', ' + Name FROM inv_workorder_d where type = 1 and deleted = 0 and confid = a.confid and instanceid = a.instanceid ");
                    strSQL1.Append(" FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N'') as [A/V WO]  ");
                }

                if (woDetails.IndexOf("2") >= 0)
                {
                    strSQL1.Append(" ,Stuff ((SELECT N', ' + Name FROM inv_workorder_d where type = 2 and deleted = 0 and confid = a.confid and instanceid = a.instanceid ");
                    strSQL1.Append(" FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N'') as [CAT WO]  ");
                }
                if (woDetails.IndexOf("3") >= 0)
                {
                    strSQL1.Append(" ,Stuff ((SELECT N', ' + Name FROM inv_workorder_d where type = 3 and deleted = 0 and confid = a.confid and instanceid = a.instanceid ");
                    strSQL1.Append(" FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N'') as [HK WO]  ");
                }
                strSQL1.Append(" from conf_conference_d a , conf_room_d b where a.confid not in (11) and a.confid = b.confid and a.instanceid = b.instanceid ");

                if (confBehaviorFilter != "")
                    strSQL1.Append(" and " + confBehaviorFilter);

                if (scheduledFilter != "" && immediate != "")
                    strSQL1.Append(" and (" + scheduledFilter + " or immediate = 1)");
                else if (scheduledFilter != "" && immediate == "")
                    strSQL1.Append(" and " + scheduledFilter);
                else if (scheduledFilter == "" && immediate != "")
                    strSQL1.Append(" and immediate = 1");

                if (confTypeFilter != "")
                    strSQL1.Append(" and " + confTypeFilter);

                //strSQL1.Append(" and (Status in (7,0,9,3,1) or immediate = 1)   ");

                if (startDate != "" && endDate != "")
                {
                    strSQL1.Append(" AND a.confdate BETWEEN dbo.changeTOGMTtime(" + tmZone + ",'" + startDate + "') ");
                    strSQL1.Append(" AND dbo.changeTOGMTtime(" + tmZone + ",'" + endDate + "') ");
                }

                //ZD 101835
                strSQL1.Append(" Union All " + strSQL1.ToString().Replace("conf_room_d", "Archive_conf_room_d").Replace("conf_conference_d", "Archive_conf_conference_d").Replace("conf_user_d", "Archive_conf_user_d").Replace("conf_bridge_d", "Archive_conf_bridge_d"));

            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
            }
        }



        #endregion

        //FB 2047 - End
        
        //FB 2343 Start
        #region GetMonthlydays
        /// <summary>
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetMonthlydays(ref vrmDataObject obj)
        {
            int userid = 0, currentYear = 0;
            StringBuilder OutXml = new StringBuilder();
            
            try
            {
                obj.outXml = "";
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node = null;

                node = xd.SelectSingleNode("//GetMonthlydays/userID");
                if (node != null)
                    int.TryParse(node.InnerText, out userid);

                node = xd.SelectSingleNode("//GetMonthlydays/organizationID");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out organizationID);
                if (organizationID < defaultOrgId)
                {
                    myvrmEx = new myVRMException(423);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//GetMonthlydays/currentYear");
                if (node != null)
                    int.TryParse(node.InnerText, out currentYear);
                
                OutXml.Append("<GetMonthlydays>");

                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("OrgId", organizationID));
                criterionList.Add(Expression.Eq("CurrentYear", currentYear));
                
                m_IReportMonthlyDaysDAO.addOrderBy(Order.Asc("ID"));
                List<ReportMonthlyDays> RptMonthDays = m_IReportMonthlyDaysDAO.GetByCriteria(criterionList);
                m_IReportMonthlyDaysDAO.clearOrderBy();

                if (RptMonthDays.Count > 0)
                {
                    OutXml.Append("<MonthlyTypes>");

                    for (int i = 0; i < RptMonthDays.Count; i++)
                    {
                        OutXml.Append("<MonthlyType>");
                        OutXml.Append("<ID>" + RptMonthDays[i].ID.ToString() + "</ID>");
                        OutXml.Append("<MonthName>" + RptMonthDays[i].MonthName + "</MonthName>");
                        OutXml.Append("<MonthWorkingDays>" + RptMonthDays[i].MonthWorkingDays.ToString() + "</MonthWorkingDays>");
                        OutXml.Append("<MonthStartDate>" + RptMonthDays[i].MonthStartDate + "</MonthStartDate>");
                        OutXml.Append("<MonthEndDate>" + RptMonthDays[i].MonthEndDate + "</MonthEndDate>");
                        OutXml.Append("<Year>" + RptMonthDays[i].CurrentYear.ToString() + "</Year>");
                        OutXml.Append("</MonthlyType>");
                    }
                    OutXml.Append("</MonthlyTypes>");
                }

                OutXml.Append("</GetMonthlydays>");
                obj.outXml = OutXml.ToString();

                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                return false;
            }
        }
        #endregion

        #region SetMonthlydays
        /// <summary>
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SetMonthlydays(ref vrmDataObject obj)
        {
            int userid = 0, currentYear = 0;
            StringBuilder OutXml = new StringBuilder();

            try
            {
                obj.outXml = "";
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node = null;

                node = xd.SelectSingleNode("//SetMonthlyDays/userID");
                if (node != null)
                    int.TryParse(node.InnerText, out userid);

                node = xd.SelectSingleNode("//SetMonthlyDays/organizationID");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out organizationID);
                if (organizationID < defaultOrgId)
                {
                    myvrmEx = new myVRMException(423);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }
                                
                XmlNodeList NodeList = xd.GetElementsByTagName("MonthDetails");
                foreach (XmlNode Node in NodeList)
                {
                    XmlElement element = (XmlElement)Node;
                    int MonthNo = int.Parse(element.GetElementsByTagName("MonthNo")[0].InnerText);
                    string MonthName = element.GetElementsByTagName("MonthName")[0].InnerText;
                    int MonthWorkingDays = int.Parse(element.GetElementsByTagName("MonthWorkingDays")[0].InnerText);
                    currentYear = int.Parse(element.GetElementsByTagName("CurrentYear")[0].InnerText);

                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("OrgId", organizationID));
                    criterionList.Add(Expression.Eq("ID", MonthNo));
                    criterionList.Add(Expression.Eq("MonthName", MonthName));
                    criterionList.Add(Expression.Eq("CurrentYear", currentYear));

                    List<ReportMonthlyDays> RptMonthDays = m_IReportMonthlyDaysDAO.GetByCriteria(criterionList);
                    if (RptMonthDays.Count > 0)
                    {
                        for (int i = 0; i < RptMonthDays.Count; i++)
                        {
                            RptMonthDays[i].MonthWorkingDays = MonthWorkingDays;
                            m_IReportMonthlyDaysDAO.Update(RptMonthDays[i]);
                        }
                    }
                }
                obj.outXml = "<Success>1</Success>";
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                return false;
            }
        }
        #endregion

        #region GetWeeklydays
        /// <summary>
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetWeeklydays(ref vrmDataObject obj)
        {
            int userid = 0, currentYear = 0;
            StringBuilder OutXml = new StringBuilder();

            try
            {
                obj.outXml = "";
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node = null;

                node = xd.SelectSingleNode("//GetWeeklydays/userID");
                if (node != null)
                    int.TryParse(node.InnerText, out userid);

                node = xd.SelectSingleNode("//GetWeeklydays/organizationID");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out organizationID);
                if (organizationID < defaultOrgId)
                {
                    myvrmEx = new myVRMException(423);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//GetWeeklydays/currentYear");
                if (node != null)
                    int.TryParse(node.InnerText, out currentYear);

                OutXml.Append("<GetWeeklydays>");

                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("OrgId", organizationID));
                criterionList.Add(Expression.Eq("CurrentYear", currentYear));

                m_IReportMonthlyDaysDAO.addOrderBy(Order.Asc("ID"));
                List<ReportWeeklyDays> RptWeekDays = m_IReportWeeklyDaysDAO.GetByCriteria(criterionList);
                m_IReportWeeklyDaysDAO.clearOrderBy();

                if (RptWeekDays.Count > 0)
                {
                    OutXml.Append("<WeeklyTypes>");

                    for (int i = 0; i < RptWeekDays.Count; i++)
                    {
                        OutXml.Append("<WeeklyType>");
                        OutXml.Append("<ID>" + RptWeekDays[i].ID.ToString() + "</ID>");
                        OutXml.Append("<WeekNumber>" + RptWeekDays[i].WeekNumber + "</WeekNumber>");
                        OutXml.Append("<WeekWorkingDays>" + RptWeekDays[i].WeekWorkingDays.ToString() + "</WeekWorkingDays>");
                        OutXml.Append("<WeekStartDate>" + RptWeekDays[i].WeekStartDate + "</WeekStartDate>");
                        OutXml.Append("<WeekEndDate>" + RptWeekDays[i].WeekEndDate + "</WeekEndDate>");
                        OutXml.Append("<Year>" + RptWeekDays[i].CurrentYear.ToString() + "</Year>");
                        OutXml.Append("</WeeklyType>");
                    }
                    OutXml.Append("</WeeklyTypes>");
                }

                OutXml.Append("</GetWeeklydays>");
                obj.outXml = OutXml.ToString();

                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                return false;
            }
        }
        #endregion

        #region SetWeeklydays
        /// <summary>
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SetWeeklydays(ref vrmDataObject obj)
        {
            int userid = 0;
            StringBuilder OutXml = new StringBuilder();

            try
            {
                obj.outXml = "";
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node = null;
                int WeekNo = 0, WeekWorkingDays = 0, CurrentYear = 0 ;
                String WeekName = "";
                List<ICriterion> criterionList = new List<ICriterion>();

                node = xd.SelectSingleNode("//SetWeeklydays/userID");
                if (node != null)
                    int.TryParse(node.InnerText, out userid);

                node = xd.SelectSingleNode("//SetWeeklydays/organizationID");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out organizationID);
                if (organizationID < defaultOrgId)
                {
                    myvrmEx = new myVRMException(423);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                XmlNodeList NodeList = xd.GetElementsByTagName("WeekDetails");
                foreach (XmlNode Node in NodeList)
                {
                    WeekNo = 0; WeekWorkingDays = 0; CurrentYear = 0;
                    XmlElement element = (XmlElement)Node;
                    WeekNo = int.Parse(element.GetElementsByTagName("WeekNo")[0].InnerText);
                    WeekName = element.GetElementsByTagName("Weeks")[0].InnerText;
                    WeekWorkingDays = int.Parse(element.GetElementsByTagName("WeekWorkingDays")[0].InnerText);
                    CurrentYear = int.Parse(element.GetElementsByTagName("CurrentYear")[0].InnerText);

                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("OrgId", organizationID));
                    criterionList.Add(Expression.Eq("ID", WeekNo));
                    criterionList.Add(Expression.Eq("WeekNumber", WeekName));
                    criterionList.Add(Expression.Eq("CurrentYear", CurrentYear));
                    List<ReportWeeklyDays> RptWeekDays = m_IReportWeeklyDaysDAO.GetByCriteria(criterionList);
                    if (RptWeekDays.Count > 0)
                    {
                        for (int i = 0; i < RptWeekDays.Count; i++)
                        {
                            RptWeekDays[i].WeekWorkingDays = WeekWorkingDays;
                            m_IReportWeeklyDaysDAO.Update(RptWeekDays[i]);
                        }
                    }
                }
                obj.outXml = "<Success>1</Success>";
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                return false;
            }
        }
        #endregion

        #region Setdays
        /// <summary>
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool Setdays(ref vrmDataObject obj)
        {
            int userid = 0, Year=-1;
            StringBuilder OutXml = new StringBuilder();

            try
            {
                obj.outXml = "";
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node = null;

                node = xd.SelectSingleNode("//SetDays/userID");
                if (node != null)
                    int.TryParse(node.InnerText, out userid);

                node = xd.SelectSingleNode("//SetDays/organizationID");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out organizationID);
                if (organizationID < defaultOrgId)
                {
                    myvrmEx = new myVRMException(423);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//SetDays/Year");
                if (node != null)
                    int.TryParse(node.InnerText, out Year);

                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("OrgId", organizationID));
                criterionList.Add(Expression.Eq("CurrentYear", Year));
                List<ReportWeeklyDays> RptWeekDays = m_IReportWeeklyDaysDAO.GetByCriteria(criterionList);
                List<ReportMonthlyDays> RptmonthDays = m_IReportMonthlyDaysDAO.GetByCriteria(criterionList);
                if (RptmonthDays.Count <= 0)
                {
                    XmlNodeList NodeList1 = xd.GetElementsByTagName("MonthDetails");
                    foreach (XmlNode Node1 in NodeList1)
                    {
                        XmlElement element1 = (XmlElement)Node1;
                        int MonthNo = int.Parse(element1.GetElementsByTagName("MonthNo")[0].InnerText);
                        string MonthName = element1.GetElementsByTagName("MonthName")[0].InnerText;
                        int MonthWorkingDays = int.Parse(element1.GetElementsByTagName("MonthWorkingDays")[0].InnerText);
                        DateTime StartMonthDate = DateTime.Parse(element1.GetElementsByTagName("MonthStartDate")[0].InnerText);
                        DateTime EndMonthDate = DateTime.Parse(element1.GetElementsByTagName("MonthEndDate")[0].InnerText);
                        int year = int.Parse(element1.GetElementsByTagName("Year")[0].InnerText);
                        ReportMonthlyDays rptmonth = new ReportMonthlyDays();
                        rptmonth.ID = MonthNo;
                        rptmonth.OrgId = organizationID;
                        rptmonth.MonthName = MonthName;
                        rptmonth.MonthWorkingDays = MonthWorkingDays;
                        rptmonth.MonthStartDate = StartMonthDate;
                        rptmonth.MonthEndDate = EndMonthDate;
                        rptmonth.CurrentYear = year;
                        m_IReportMonthlyDaysDAO.Save(rptmonth);
                    }
                }
                if (RptWeekDays.Count <= 0)
                {
                    XmlNodeList NodeList = xd.GetElementsByTagName("WeekDetails");
                    foreach (XmlNode Node in NodeList)
                    {
                        XmlElement element = (XmlElement)Node;
                        int WeekNo = int.Parse(element.GetElementsByTagName("WeekNo")[0].InnerText);
                        string WeekName = element.GetElementsByTagName("Weeks")[0].InnerText;
                        int WeekWorkingDays = int.Parse(element.GetElementsByTagName("WeekWorkingDays")[0].InnerText);
                        DateTime StartWeekDate = DateTime.Parse(element.GetElementsByTagName("WeekStartDate")[0].InnerText);
                        DateTime EndWeekDate = DateTime.Parse(element.GetElementsByTagName("WeekEndDate")[0].InnerText);
                        int year = int.Parse(element.GetElementsByTagName("Year")[0].InnerText);
                        ReportWeeklyDays rptweek = new ReportWeeklyDays();
                        rptweek.ID = WeekNo;
                        rptweek.OrgId = organizationID;
                        rptweek.WeekNumber = WeekName;
                        rptweek.WeekWorkingDays = WeekWorkingDays;
                        rptweek.WeekStartDate = StartWeekDate;
                        rptweek.WeekEndDate = EndWeekDate;
                        rptweek.CurrentYear = year;
                        m_IReportWeeklyDaysDAO.Save(rptweek);
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                return false;
            }
        }
        #endregion
        //FB 2343 End

        //FB 2343 - Starts New Reports 
        #region WeeklyUsagebyRoom
        /// <summary>
        /// WeeklyUsagebyRoom
        /// </summary>
        /// <param name="stmt"></param>
        /// <param name="starttime"></param>
        /// <param name="endtime"></param>
        /// <param name="organizationID"></param>
        /// <returns></returns>
        private void WeeklyUsagebyRoom(ref StringBuilder stmt, String strParam, String pivotInput, Int32 WorkingHours, String selectedYear, String organizationID)
        {
            //id in (" + selectedYear.Split(';')[0] + ") and
            try
            {
                stmt.Append(" Select * from Rpt_week_D where  CurrentYear in(" + selectedYear + ") and orgid =" + organizationID + " order by CurrentYear,ID;");

                stmt.Append(" Select " + strParam + " from (");
                //ZD 101835
                StringBuilder stmtSub = new StringBuilder();
                stmtSub.Append(" select c.Name ");
                stmtSub.Append(", 'Week'+ DATENAME(WEEK,confdate) + cast(year(confdate) as varchar(4)) wk ");
                stmtSub.Append(", 'Week'+ DATENAME(Week,confdate) + cast(year(confdate) as varchar(4)) + '1' as wk1 ");
                stmtSub.Append(", WeekNumber + cast(year(confdate) as varchar(4)) + '2' as wk2 ");
                stmtSub.Append(", 'Week'+ DATENAME(Week,confdate) + cast(year(confdate) as varchar(4)) + '3' as wk3  ");
                stmtSub.Append(", count(*) as cnt, convert ( decimal(5,2),(convert(float, sum(a.duration))/60)) as duration ");
                stmtSub.Append(", WeekWorkingDays * " + WorkingHours + " as Availablehours ");
                stmtSub.Append(", case when WeekWorkingDays = 0 then WeekWorkingDays else ");
                stmtSub.Append(" round(convert ( decimal(5,0),(100 * convert ( decimal(5,2),(convert(float, sum(a.duration))/60)))/convert(float,WeekWorkingDays * " + WorkingHours + " )), 0) end as usage ");
                stmtSub.Append(" from loc_room_d c  left outer join (");
                stmtSub.Append(" select b.roomid, confdate,a.duration,(select Weeknumber from Rpt_Week_D where orgid =" + organizationID + " and Currentyear = year(a.confdate) and id = month(a.confdate)) as Weeknumber  ");
                stmtSub.Append(",(select WeekWorkingDays from Rpt_Week_D where orgid =" + organizationID + " and Currentyear = year(a.confdate) and id = month(a.confdate)) as WeekWorkingDays ");
                stmtSub.Append(" from conf_conference_d a, conf_room_D b ");
                stmtSub.Append(" where a.confid = b.confid and a.instanceid = b.instanceid and a.orgid = " + organizationID + " and a.deleted = 0  ");
                stmtSub.Append(" and year(confdate) in (" + selectedYear + ") ");
                stmtSub.Append(" ) a  on a.roomid = c.roomid where c.disabled = 0 and c.orgid = " + organizationID);
                stmtSub.Append(" group by c.Roomid,c.Name ");
                stmtSub.Append(", DATENAME(Week,confdate) + cast(year(confdate) as varchar(4)) , DATENAME(Week,confdate) + cast(year(confdate) as varchar(4)) + '1'");
                stmtSub.Append(", DATENAME(Week,confdate) + cast(year(confdate) as varchar(4)) + '2', DATENAME(Week,confdate) + cast(year(confdate) as varchar(4)) + '3'");
                stmtSub.Append(", WeekWorkingDays, Weeknumber + cast(year(confdate) as varchar(4)) + '2', a.confdate");

                stmtSub.Append(" Union All " + stmtSub.ToString().Replace("conf_conference_d", "Archive_conf_conference_d").Replace("conf_room_D", "Archive_conf_room_D"));
                stmt.Append(stmtSub.ToString());
                
                stmt.Append(" ) as P ");
                stmt.Append(" Pivot (max(cnt) for wk in ( " + pivotInput.Split(';')[0] + " )) as pvt");

                stmt.Append(" Pivot (max(duration) for wk1 in (" + pivotInput.Split(';')[1] + "))as pvt1");

                stmt.Append(" Pivot (max(Availablehours) for wk2 in (" + pivotInput.Split(';')[2] + "))as pvt2");

                stmt.Append(" Pivot (max(usage) for wk3 in (" + pivotInput.Split(';')[3] + " ))as pvt3");

                stmt.Append(" group by  Name ");
                stmt.Append(" order by Name");

            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
            }

        }
        #endregion

        #region MonthlyUsagebyRoom
        /// <summary>
        /// MonthlyUsagebyRoom
        /// </summary>
        /// <param name="stmt"></param>
        /// <param name="starttime"></param>
        /// <param name="endtime"></param>
        /// <param name="organizationID"></param>
        /// <returns></returns>
        private void MonthlyUsagebyRoom(ref StringBuilder stmt, String strParam, String pivotInput, Int32 WorkingHours, String selectedYear, String organizationID)
        {
            try
            {//ID in (" + selectedYear.Split(';')[0] + ") and
                stmt.Append(" Select * from Rpt_Month_D where CurrentYear in(" + selectedYear + ")  and orgid =" + organizationID + " order by CurrentYear,ID;");
                stmt.Append(" Select " + strParam + " ");
                stmt.Append("  from (");

                //ZD 101835
                StringBuilder stmtSub = new StringBuilder();
                stmtSub.Append(" select c.Roomid,c.Name");
                stmtSub.Append(", DATENAME(month,confdate) + cast(year(confdate) as varchar(4)) yr ");
                stmtSub.Append(", DATENAME(month,confdate) + cast(year(confdate) as varchar(4)) + '1' as yr1 ");
                stmtSub.Append(", MonthName + cast(year(confdate) as varchar(4)) + '2' as yr2");
                stmtSub.Append(", DATENAME(month,confdate) + cast(year(confdate) as varchar(4)) + '3' as yr3 ");
                stmtSub.Append(", count(*) as cnt");
                //stmtSub.Append(", sum(a.duration)/60 as duration");
                stmtSub.Append(", convert ( decimal(5,2),(convert(float, sum(a.duration))/60)) as duration ");
                stmtSub.Append(", monthworkingdays * " + WorkingHours + "  as Availablehours");
                stmtSub.Append(", convert ( decimal(5),(convert(decimal(5),(isnull(sum(a.duration)/60,0)))/convert(decimal(5),monthworkingdays*" + WorkingHours + " ))*100) as usage");

                stmtSub.Append(" from loc_room_d c ");
                stmtSub.Append(" left outer join (select b.roomid, confdate,a.duration,(select MonthName from Rpt_Month_D where orgid =" + organizationID + " and Currentyear = year(a.confdate) and id = month(a.confdate)) as MonthName ");
                stmtSub.Append(" ,(select monthworkingdays from Rpt_Month_D where orgid =" + organizationID + " and Currentyear = year(a.confdate) and id = month(a.confdate)) as monthworkingdays ");
                stmtSub.Append(" from conf_conference_d a, conf_room_D b ");

                stmtSub.Append(" where a.confid = b.confid and a.instanceid = b.instanceid and a.orgid = " + organizationID + " and a.deleted = 0");
                stmtSub.Append(" and year(confdate) in (" + selectedYear + ")");
                stmtSub.Append("  ) a on a.roomid = c.roomid where c.disabled = 0 and c.orgid = " + organizationID + " ");

                stmtSub.Append(" group by c.Roomid,c.Name ");
                stmtSub.Append(", DATENAME(month,confdate) + cast(year(confdate) as varchar(4)) , DATENAME(month,confdate) + cast(year(confdate) as varchar(4)) + '1'");
                stmtSub.Append(", DATENAME(month,confdate) + cast(year(confdate) as varchar(4)) + '2', DATENAME(month,confdate) + cast(year(confdate) as varchar(4)) + '3'");
                stmtSub.Append(", MonthWorkingDays, MonthName + cast(year(confdate) as varchar(4)) + '2'");
                stmtSub.Append(", MonthWorkingDays");
                stmtSub.Append(", MonthName + '2'");
                stmtSub.Append(" Union All " + stmtSub.ToString().Replace("conf_conference_d", "Archive_conf_conference_d").Replace("conf_room_D", "Archive_conf_room_D"));
                
                stmt.Append(stmtSub.ToString());

                stmt.Append(" ) as P");
                stmt.Append(" Pivot (max(cnt) for yr in ( " + pivotInput.Split(';')[0] + " )) as pvt");

                stmt.Append(" Pivot (max(duration) for yr1 in (" + pivotInput.Split(';')[1] + "))as pvt1");

                stmt.Append(" Pivot (max(Availablehours) for yr2 in (" + pivotInput.Split(';')[2] + "))as pvt2");

                stmt.Append(" Pivot (max(usage) for yr3 in (" + pivotInput.Split(';')[3] + " ))as pvt3");

                stmt.Append(" group by  Name ,Roomid ");
                stmt.Append(" order by Name");

            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
            }

        }
        #endregion

        #region GetWorkingYears

        private void GetWorkingYears(ref StringBuilder strSQL, String orgid, String selectedYear)
        {
            try
            {
                strSQL.Append(" SELECT DISTINCT CurrentYear FROM dbo.Rpt_Month_D where orgid =" + orgid + " ORDER BY CurrentYear "); //To Fill dropdownlist for month
                strSQL.Append("; SELECT * FROM dbo.Rpt_Month_D where orgid =" + orgid); //To find workingdays for month
                if (selectedYear != "")
                    strSQL.Append(" and currentyear in ("+ selectedYear +")");
                strSQL.Append(" ORDER BY CurrentYear ");
                strSQL.Append("; SELECT DISTINCT CurrentYear FROM dbo.Rpt_Week_D where orgid =" + orgid + "ORDER BY CurrentYear ");
                strSQL.Append("; SELECT * FROM dbo.Rpt_Week_D where orgid =" + orgid);
                if (selectedYear != "")
                    strSQL.Append(" and currentyear in (" + selectedYear + ")");
                strSQL.Append(" ORDER BY CurrentYear ");
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
            }
        }
        #endregion

        //FB 2343 - End  

        //FB 2363 - Start
        #region GetExchangeEvents
        private void GetExchangeEvents(ref StringBuilder strSQL, String startDate, String endDate, String tmZone, String inputType, String dtFormat)
        {
            try
            {
                strSQL.Append("SELECT isnull(TaskType,'') [Task Type],isnull(RequestID,'') AS [Trans Id],isnull(TypeID,'') [Type ID],Customerid as [Customer Id],confnumname as [Conference Id]"); //FB 2363
                strSQL.Append(" ,(SELECT CustomerName FROM dbo.Org_Settings_D s WHERE s.CustomerID = a.CustomerID AND s.orgid = a.orgid) [Issued By]");//FB 2518               
                if(dtFormat == "HHmmZ")
                    strSQL.Append(" , dbo.changeToGMTTime("+ sysSettings.TimeZone +", EventTime) [Issued On]");
                else
                    strSQL.Append(" , dbo.userPreferedTime(" + tmZone + ",dbo.changeToGMTTime(" + sysSettings.TimeZone + ", EventTime)) [Issued On]");

                if (dtFormat == "HHmmZ")
                    strSQL.Append(" ,Status, dbo.userPreferedTime(" + tmZone + ",dbo.changeToGMTTime(" + sysSettings.TimeZone + ", StatusDate)) [Status Date]");
                else
                    strSQL.Append(" ,Status, dbo.changeToGMTTime(" + sysSettings.TimeZone + ", StatusDate) [Status Date]");

                strSQL.Append(" ,StatusMessage [Status Message],RequestID");
                //strSQL.Append(" ,isnull(RequestXML,'') as RequestXML, isnull(RespondXML,'') as RespondXML");
                strSQL.Append(" , (SELECT StandardName FROM dbo.Gen_TimeZone_S WHERE TimeZoneID = (select timezone from usr_list_d where userid = '"+ tmZone +"')) AS tzone");
                strSQL.Append(" FROM [ES_Event_D] a where 1 = 1 ");

                if (startDate != "" && endDate != "")
                {
                    if (dtFormat == "HHmmZ")
                        strSQL.Append(" AND a.EventTime BETWEEN dbo.changeTOGMTtime(" + sysSettings.TimeZone + ",'" + startDate + "') AND dbo.changeTOGMTtime(" + sysSettings.TimeZone + ",'" + endDate + "')");
                    else
                    {
                        strSQL.Append(" AND a.EventTime BETWEEN dbo.userPreferedTime(" + tmZone + ",dbo.changeTOGMTtime(" + sysSettings.TimeZone + ",'" + startDate + "'))");
                        strSQL.Append(" AND dbo.userPreferedTime(" + tmZone + ",dbo.changeTOGMTtime(" + sysSettings.TimeZone + ",'" + endDate + "')) ");
                    }
                }

                if (inputType != "")
                    strSQL.Append(" AND Status in(" + inputType + ")");

                strSQL.Append(" ORDER BY EventTime desc ");
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
            }
        }
        #endregion

        #region GetUsers
        private String GetUsers()
        {
            StringBuilder strSQL = new StringBuilder();
            try
            {
                /*
                //"ConferenceReports"
                inXML = "<report>";
                inXML += "<configpath>" + Application["MyVRMServer_ConfigPath"].ToString() + "</configpath>";
                inXML += "<reportType>GU</reportType>";
                inXML += "<export>1</export>";
                inXML += "<Destination>" + Server.MapPath(".") + "\\Upload" + "</Destination>";                  
                inXML += "<fileName>UserReport</fileName>";
                inXML += "</report>";
                */

                strSQL.Append("SELECT Email AS [User ID], Firstname +' '+ LastName AS [Username], Email, WorkPhone as [Work Phone]");//ZD 102706
                strSQL.Append(" ,CellPhone as [Cell Phone],Stuff ((SELECT distinct N', ' + d1.departmentname from  Usr_Dept_D c1, dept_list_d d1 where d1.Departmentid = c1.Departmentid ");
                strSQL.Append(" and c1.userid = a.userid FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N'') as [Department Name]");
                strSQL.Append(" FROM dbo.Usr_List_D a");
                strSQL.Append(" ORDER BY FirstName");

                return strSQL.ToString();
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                return "";
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return "";
            }
        }
        #endregion

        #region GetErrorEvents
        private String GetErrorEvents()
        {
            StringBuilder strSQL = new StringBuilder();
            try
            {
                strSQL.Append("select CustomerName as [Customer Name], ConfNumName as [myVRM Conference ID]");
                strSQL.Append(" ,RequestID as [Request ID], TypeID as [Type of Request], StatusDate as [Status Date/Time]");
                strSQL.Append(" , StatusMessage as [Exception Received]");
                strSQL.Append(" from es_event_d a, Sys_ESSettings_D b ");
                strSQL.Append(" where a.CustomerID = b.CustomerID and Status = 'E' and orgid=" + organizationID.ToString());
                strSQL.Append(" Order By ConfNumName");

                return strSQL.ToString();
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                return "";
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return "";
            }
        }
        #endregion

        //FB 2363 - End

        //FB 2363 - Starts
        #region UpdateCancelEvents
        /// <summary>
        /// UpdateCancelEvents
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool UpdateCancelEvents(ref vrmDataObject obj)
        {
            bool bRet = true;
            XmlNode node = null;
            String stmt = "", requestIds = "", userName = "";
            //status = "X";

            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                
                node = xd.SelectSingleNode("//UpdateCancelEvents/RequestId");
                if (node != null)
                    requestIds = node.InnerText.ToString().Trim();

                node = xd.SelectSingleNode("//UpdateCancelEvents/userName");
                if (node != null)
                    userName = node.InnerText.ToString().Trim();

                try
                {
                    stmt = "Update ES_Event_D Set [Status] = 'X', StatusDate='" + DateTime.Now + "', StatusMessage = StatusMessage + '. This Event Canceled by " + userName
                        + "' where RequestId in ( " + requestIds + " ) ";

                    if (m_rptLayer == null)
                        m_rptLayer = new ns_SqlHelper.SqlHelper();

                    m_rptLayer.OpenConnection();
                    m_rptLayer.ExecuteNonQuery(stmt);
                }
                catch
                {
                    bRet = false;
                }
                finally
                {
                    if (m_rptLayer != null)
                        m_rptLayer.CloseConnection();
                }
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                bRet = false;
            }
            return bRet;
        }
        #endregion

        //FB 2501 starts
        # region GetConferenceDetails

        private void GetConferenceDetails(ref StringBuilder strSQL, String startDate, String endDate, String tmZone, String userID
            , String orgID, String ConfName, String HostID, String RequestorID, String EndpointID, String BridgeID, String LocationID
            , String VNOCoperator, String CallURL, String QueryType, String dtFormat)
        {

            StringBuilder strSQLParams = new StringBuilder();
            try
            {
                //ZD 103840
                strSQL.Append("SELECT distinct a.confnumname as  [Conference #],a.externalname as [Meeting Title],"); //ZD 100288
                if(dtFormat == "HHmmZ")//FB 2588
                    strSQL.Append("a.confdate as [Conference Start], tz.TimeZone as TimezoneName, ");
                else
                    strSQL.Append("dbo.GMTToUserPreferedTime(" + userID + ",a.confdate)AS [Conference Start], tz.TimeZone as TimezoneName, ");
                strSQL.Append("a.duration as [Duration (mins)],t.StandardName as [Conference Time Zone],"); //ZD 104169
                strSQL.Append("case when CHARINDEX('H',UserIDRefText) > 0 then ud.firstname + ' ' +  ud.lastname  else uh.firstname + ' ' +  uh.lastname  end as [Host],"); //ZD 103878
                strSQL.Append("case when CHARINDEX('R',UserIDRefText) > 0 then urd.firstname + ' ' +  urd.lastname  else ur.firstname + ' ' +  ur.lastname  end as [Requester],");
                //uh.firstname + ' '+ uh.lastname as [Host], ur.firstname +' '+ ur.lastname as [Requester], ");
                //FB 2694 
                //strSQL.Append("isnull(uo.firstname + ' '+ uo.lastname, '') as [VNOC Operator], "); vnocname
                strSQL.Append("isnull(vnocname,'') as [VNOC Operator], ");
                strSQL.Append("case a.conftype ");
                strSQL.Append("when 2 then 'Audio/Video' ");//ZD 100528
                strSQL.Append("when 4 then 'Point to Point' "); 
                strSQL.Append("when 6 then 'AudioOnly' ");
                strSQL.Append("when 7 then 'RoomOnly' ");
                strSQL.Append("when 8 then 'Hotdesking' ");//FB 2694
                strSQL.Append("when 10 then 'VMR' End AS [Meet Type], ");

                strSQL.Append("case a.Status ");
                strSQL.Append("when 0 then 'Scheduled' ");
                strSQL.Append("when 1 then 'Pending' ");
                strSQL.Append("when 3 then 'Terminated' ");
                strSQL.Append("when 5 then 'Ongoing' ");
                strSQL.Append("when 7 then 'Completed' ");
                strSQL.Append("when 6 then 'On MCU' "); //ZD 100036
                strSQL.Append("when 9 then 'Deleted' End as [Status], ");    
	            
                strSQL.Append("isnull(Stuff ((SELECT N', ' + m.bridgename  from conf_conference_d c, (select distinct bridgeid, confid, ");
                strSQL.Append("instanceid from (select bridgeid, confid, instanceid from conf_room_d union all select bridgeid, confid, ");
                strSQL.Append("instanceid from conf_user_d ) as b) as bb, mcu_list_D m where c.confid = a.confid and c.instanceid = a.instanceid ");
                strSQL.Append("and c.confid = bb.confid and c.instanceid = bb.instanceid and bb.bridgeid = m.bridgeid ");
                strSQL.Append("FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N''),'') as [MCU], ");
    
                strSQL.Append("isnull(Stuff (( SELECT N', ' + d.name  from conf_room_d r, loc_room_d d where r.Roomid = d.Roomid and r.extroom = 0 ");
                strSQL.Append("and r.Confid = a.Confid and r.instanceid = a.instanceid FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N''),'') as [Room], ");

                //FB 2577 Start
               
                //strSQL.Append("isnull(Stuff ((SELECT N'; ' + e.address  from conf_conference_d c , conf_room_d r, loc_room_d l, Ept_List_D e ");
                //strSQL.Append("where c.confid = a.confid and c.instanceid = a.instanceid and c.confid = r.confid and c.instanceid = r.instanceID and r.roomid = l.roomid and ");
                //strSQL.Append("l.endpointid = e.endpointid and e.extendpoint=0 and (e.profileid = 0 or e.isdefault = 1) FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N''),'') as [Call URI], ");

                strSQL.Append("isnull(Stuff ((Select N'; ' + case when e.MultiCodecAddress like '%�%' then REPLACE(r.MultiCodecAddress,'�',',') ");
                strSQL.Append(" when e.[isTelePresence]=1 then r.[MultiCodecAddress] when e.[isTelePresence]=0 then r.[ipisdnaddress] end from conf_conference_d c , conf_room_d r, loc_room_d l, "); //FB 2955
                strSQL.Append(" Ept_List_D e where c.confid = a.confid and c.instanceid = a.instanceid and c.confid = r.confid and c.instanceid = r.instanceID and r.roomid = l.roomid  and c.isHDBusy =0"); //ALLDEV-807
                strSQL.Append(" and l.endpointid = e.endpointid and e.extendpoint=0 and (e.profileid = 0 or e.isdefault = 1) FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N''),'') as [Call URI],");

                //FB 2577 End

                strSQL.Append("isnull(Stuff (( SELECT N', ' + d.name  from conf_room_d r, loc_room_d d where r.Roomid = d.Roomid and r.extroom = 1 and r.Confid = a.Confid and ");
                strSQL.Append("r.instanceid = a.instanceid FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N''),'') as [Guest Room], ");            
               
                strSQL.Append("isnull(Stuff ((SELECT N'; ' + e.address  from conf_conference_d c , conf_room_d r, loc_room_d l, ");
                strSQL.Append("Ept_List_D e where c.confid = a.confid and c.instanceid = a.instanceid and c.confid = r.confid and c.instanceid = r.instanceID and ");
                strSQL.Append("r.roomid = l.roomid and l.endpointid = e.endpointid and e.extendpoint=1 and (e.profileid = 0 or e.isdefault = 1) ");
                strSQL.Append("FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N''),'') as [Guest Endpoint Address], ");

                strSQL.Append("isnull(Stuff ((Select N', ' + ug.FirstName+ ' '+ ug.lastname from conf_user_d cu, (select distinct userid, firstname, lastname  from ");
                strSQL.Append("(select userid,firstname,lastname from usr_list_d union all select userid, firstname, lastname from usr_guestlist_d) as b) as ug ");
                strSQL.Append("where cu.userid = ug.userid and cu.Invitee = 1 and cu.confid = a.confid and cu.instanceid=a.instanceid ");
                strSQL.Append("FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N''),'') as [Username], ");//ZD 102706

                strSQL.Append("isnull(Stuff ((Select N', ' + cu.ipaddress from conf_user_d cu, (select distinct userid  from (select userid from usr_list_d ");
                strSQL.Append("union all select userid from usr_guestlist_d) as b) as ug where cu.userid = ug.userid and cu.Invitee = 1 and cu.confid = a.confid and cu.instanceid=a.instanceid  ");
                strSQL.Append("FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N''),'') as [User Endpoint] ");

                strSQL.Append("FROM dbo.conf_conference_d a ");
                //strSQL.Append("Left Outer Join usr_list_d uh on a.owner = uh.userid ");
                strSQL.Append(" left outer join (select LastName,firstname,  UserID from usr_list_d) uh on a.owner = uh.userid ");
                strSQL.Append(" left outer join (select LastName,firstname,  id from Usr_Deletelist_D) ud on a.owner = ud.id ");
                //strSQL.Append("Left Outer Join usr_list_d ur on a.userid=ur.userid ");
                strSQL.Append(" left outer join (select LastName,firstname,  UserID from usr_list_d) ur on a.userid = ur.userid ");
                strSQL.Append(" left outer join (select LastName,firstname,  id from Usr_Deletelist_D) urd on a.userid = urd.id ");

                strSQL.Append("Left Outer Join Gen_TimeZone_S t on t.timezoneid=a.timezone ");
                strSQL.Append("left outer join (select TimeZone, TimeZoneID from Gen_TimeZone_S ) tz on tz.TimeZoneID in (select timezone from Usr_List_D where userid = " + userID + ") "); //ZD 103840
                //FB 2694
                strSQL.Append("left outer join (select isnull(vu.firstname + ' '+ vu.lastname, '') as vnocname,v.confid,v.vnocid,v.instanceid from conf_vnocoperator_d v, usr_list_d vu ");
                strSQL.Append("where v.vnocid = vu.userid ) as v on a.confid = v.confid and a.instanceid = v.instanceid ");                 
                //strSQL.Append("Left Outer Join usr_list_d uo on a.ConfVNOC = uo.userid ");
                strSQL.Append("Left outer join conf_room_d cr on a.confid=cr.confid AND a.instanceid = cr.instanceid ");
                strSQL.Append("Left outer join ept_list_d e on e.endpointid=cr.endpointid ");
                strSQL.Append("Left outer join mcu_list_d d on d.bridgeid = cr.bridgeid ");
                
                strSQL.Append("where ");
                if(dtFormat == "HHmmZ")//FB 2588
                    strSQL.Append(" a.confdate >= '" + startDate + "' and a.confdate <= '" + endDate + "'");
                else
                    strSQL.Append(" dbo.GMTtouserPreferedtime(" + userID + ", a.confdate) >= '" + startDate + "' and dbo.GMTtouserPreferedtime(" + userID + ", a.confdate) <= '" + endDate + "'");

                strSQL.Append(" AND a.permanent = 0 AND a.isHDBusy = 0 AND a.orgid=" + orgID); //ZD 100522    //ALLDEV-807            

                if (ConfName.ToString() != "")
                {
                    if(strSQLParams.ToString() != "")
                        strSQLParams.Append(QueryType + " a.externalname like '%" + ConfName + "%' " );
                    else
                        strSQLParams.Append(" a.externalname like '%" + ConfName + "%' ");
                }
                if (HostID == "0") HostID = ""; //ZD  101708
                if (HostID != "")
                {
                    if (strSQLParams.ToString() != "")
                        strSQLParams.Append(QueryType + " a.owner = '" + HostID + "' ");//ZD 100263_SQL
                    else
                        strSQLParams.Append(" a.owner = '" + HostID + "' ");//ZD 100263_SQL
                }
                if (RequestorID == "0") RequestorID = ""; //ZD  101708
                if (RequestorID != "")
                {
                    if (strSQLParams.ToString() != "")
                        strSQLParams.Append(QueryType + " a.userid = '" + RequestorID + "' ");//ZD 100263_SQL
                    else
                        strSQLParams.Append(" a.userid = '" + RequestorID + "' ");//ZD 100263_SQL
                }
                //FB 2670
                if (VNOCoperator == "0") VNOCoperator = ""; //ZD  101708
                if (VNOCoperator != "")
                {
                    if (strSQLParams.ToString() != "")
                        strSQLParams.Append(QueryType + " v.vnocid in (" + VNOCoperator + ")");
                    else
                        strSQLParams.Append(" v.vnocid in (" + VNOCoperator + ")");
                }
                if (CallURL != "")
                {
                    if (strSQLParams.ToString() != "")
                        strSQLParams.Append(QueryType + " e.address ='" + CallURL + "'");
                    else
                        strSQLParams.Append(" e.address ='" + CallURL + "' OR e.MultiCodecAddress like '%" + CallURL + "%'"); //FB 2577 
                }
                if (tmZone == "") //ZD 101708
                    tmZone = "-1";
                if (tmZone != "-1")
                {
                    if (strSQLParams.ToString() != "")
                        strSQLParams.Append(QueryType + " a.timezone= '" + tmZone + "'");//ZD 100263_SQL
                    else
                        strSQLParams.Append(" a.timezone= '" + tmZone + "' ");//ZD 100263_SQL
                }
                if (BridgeID == "" || BridgeID == "0")//ZD 101708
                    BridgeID = "-1";
                if (BridgeID != "-1")
                {
                    if (strSQLParams.ToString() != "")
                        strSQLParams.Append(QueryType + " cr.bridgeid ='" + BridgeID + "' ");//ZD 100263_SQL
                    else
                        strSQLParams.Append(" cr.bridgeid ='" + BridgeID + "' ");//ZD 100263_SQL
                }
                if (EndpointID == "" || EndpointID == "0")//ZD 101708
                    EndpointID = "-1";
                if (EndpointID != "-1")
                {
                    if (strSQLParams.ToString() != "")
                        strSQLParams.Append(QueryType + " cr.endpointid='" + EndpointID + "' ");//ZD 100263_SQL
                    else
                        strSQLParams.Append(" cr.endpointid='" + EndpointID + "' ");//ZD 100263_SQL
                }
                if (LocationID != "")
                {
                    if (strSQLParams.ToString() != "")
                        strSQLParams.Append(QueryType + " cr.roomid in (" + LocationID + ")");
                    else
                        strSQLParams.Append("  cr.roomid in (" + LocationID + ")");
                }
                if (strSQLParams.ToString().Trim() != "")
                {
                    strSQL.Append("and (");
                    strSQL.Append(strSQLParams.ToString() + " ) ");
                }

                strSQL.Append(" Union All " + strSQL.ToString().Replace("conf_conference_d", "Archive_conf_conference_d").Replace("conf_room_d", "Archive_conf_room_d").Replace("conf_user_d", "Archive_conf_user_d")
                   .Replace("conf_vnocoperator_d", "Archive_conf_vnocoperator_d"));

                strSQL.Append(" Order by a.confnumname desc");
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
            }
           
        }
        #endregion
        # region GetConferenceUniqueID
        private void GetConferenceUniqueID(ref StringBuilder strSQL, String ConfUniqueID, String orgid, String userID, String dtFormat)
        {
            try
            {
                //ZD 103840 - Start
                strSQL.Append("SELECT a.confnumname as  [Conference #],a.externalname as [Meeting Title],"); //ZD 100288
                if (dtFormat == "HHmmZ")//FB 2588
                    strSQL.Append("a.confdate as [Conference Start], tz.TimeZone as TimezoneName, ");
                else
                    strSQL.Append("dbo.GMTToUserPreferedTime(" + userID + ",a.confdate)AS [Conference Start], tz.TimeZone as TimezoneName, ");
                strSQL.Append("a.duration as [Duration (mins)],t.StandardName as [Conference Time Zone],"); //ZD 104169
                strSQL.Append("case when CHARINDEX('H',UserIDRefText) > 0 then ud.firstname + ' ' +  ud.lastname  else uh.firstname + ' ' +  uh.lastname  end as [Host],"); //ZD 103878
                strSQL.Append("case when CHARINDEX('R',UserIDRefText) > 0 then urd.firstname + ' ' +  urd.lastname  else ur.firstname + ' ' +  ur.lastname  end as [Requester],");
                //uh.firstname + ' '+ uh.lastname as [Host], ur.firstname +' '+ ur.lastname as [Requester], ");
                //FB 2670 //ZD 103840 - End
                //strSQL.Append("isnull(uo.firstname + ' '+ uo.lastname, '') as [VNOC Operator], "); 
                strSQL.Append("vnocname as [VNOC Operator], "); 
                strSQL.Append("case a.conftype ");
                strSQL.Append("when 2 then 'Audio/Video' "); //ZD 100528
                strSQL.Append("when 4 then 'Point to Point' "); 
                strSQL.Append("when 6 then 'AudioOnly' ");
                strSQL.Append("when 7 then 'RoomOnly' ");
                strSQL.Append("when 8 then 'Hotdesking' ");//FB 2694
                strSQL.Append("when 10 then 'VMR' End AS [Meet Type], ");

                strSQL.Append("case a.Status ");
                strSQL.Append("when 0 then 'Scheduled' ");
                strSQL.Append("when 1 then 'Pending' ");
                strSQL.Append("when 3 then 'Terminated' ");
                strSQL.Append("when 5 then 'Ongoing' ");
                strSQL.Append("when 7 then 'Completed' ");
                strSQL.Append("when 6 then 'On MCU' "); //ZD 100036
                strSQL.Append("when 9 then 'Deleted' End as [Status], "); 

                strSQL.Append("isnull(Stuff ((SELECT N', ' + m.bridgename  from conf_conference_d c, (select distinct bridgeid, confid, ");
                strSQL.Append("instanceid from (select bridgeid, confid, instanceid from conf_room_d union all select bridgeid, confid, ");
                strSQL.Append("instanceid from conf_user_d ) as b) as bb, mcu_list_D m where c.confid = a.confid and c.instanceid = a.instanceid ");
                strSQL.Append("and c.confid = bb.confid and c.instanceid = bb.instanceid and bb.bridgeid = m.bridgeid ");
                strSQL.Append("FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N''),'') as [MCU], ");
    
                strSQL.Append("isnull(Stuff (( SELECT N', ' + d.name  from conf_room_d r, loc_room_d d where r.Roomid = d.Roomid and r.extroom = 0 ");
                strSQL.Append("and r.Confid = a.Confid and r.instanceid = a.instanceid FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N''),'') as [Room], ");
                                  
                strSQL.Append("isnull(Stuff ((SELECT N'; ' + e.address  from conf_conference_d c , conf_room_d r, loc_room_d l, Ept_List_D e ");
                strSQL.Append("where c.confid = a.confid and c.instanceid = a.instanceid and c.confid = r.confid and c.instanceid = r.instanceID and r.roomid = l.roomid and ");
                strSQL.Append("l.endpointid = e.endpointid and e.extendpoint=0 and (e.profileid = 0 or e.isdefault = 1) FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N''),'') as [Call URI], ");

                strSQL.Append("isnull(Stuff (( SELECT N', ' + d.name  from conf_room_d r, loc_room_d d where r.Roomid = d.Roomid and r.extroom = 1 and r.Confid = a.Confid and ");
                strSQL.Append("r.instanceid = a.instanceid FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N''),'') as [Guest Room], ");
                
                strSQL.Append("isnull(Stuff ((SELECT N'; ' + e.address  from conf_conference_d c , conf_room_d r, loc_room_d l, ");
                strSQL.Append("Ept_List_D e where c.confid = a.confid and c.instanceid = a.instanceid and c.confid = r.confid and c.instanceid = r.instanceID and ");
                strSQL.Append("r.roomid = l.roomid and l.endpointid = e.endpointid and e.extendpoint=1 and (e.profileid = 0 or e.isdefault = 1) ");
                strSQL.Append("FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N''),'') as [Guest Endpoint Address], ");

                strSQL.Append("isnull(Stuff ((Select N', ' + ug.FirstName+ ' '+ ug.lastname from conf_user_d cu, (select distinct userid, firstname, lastname  from ");
                strSQL.Append("(select userid,firstname,lastname from usr_list_d union all select userid, firstname, lastname from usr_guestlist_d) as b) as ug ");
                strSQL.Append("where cu.userid = ug.userid and cu.Invitee = 1 and cu.confid = a.confid and cu.instanceid=a.instanceid ");
                strSQL.Append("FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N''),'') as [Username], ");//ZD 102706

                strSQL.Append("isnull(Stuff ((Select N', ' + cu.ipaddress from conf_user_d cu, (select distinct userid  from (select userid from usr_list_d ");
                strSQL.Append("union all select userid from usr_guestlist_d) as b) as ug where cu.userid = ug.userid and cu.Invitee = 1 and cu.confid = a.confid and cu.instanceid=a.instanceid  ");
                strSQL.Append("FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N''),'') as [User Endpoint] ");      

                strSQL.Append("FROM dbo.Conf_Conference_D a ");
                strSQL.Append(" left outer join (select LastName,firstname,  UserID from usr_list_d) uh on a.owner = uh.userid ");
                strSQL.Append(" left outer join (select LastName,firstname,  id from Usr_Deletelist_D) ud on a.owner = ud.id ");

                //strSQL.Append("Left Outer Join usr_list_d uh on a.owner = uh.userid ");
                strSQL.Append(" left outer join (select LastName,firstname,  UserID from usr_list_d) ur on a.userid = ur.userid ");
                strSQL.Append(" left outer join (select LastName,firstname,  id from Usr_Deletelist_D) urd on a.userid = urd.id ");

                //strSQL.Append("Left Outer Join usr_list_d ur on a.userid=ur.userid ");
                
                strSQL.Append("Left Outer Join Gen_TimeZone_S t on t.timezoneid=a.timezone ");
                strSQL.Append("left outer join (select TimeZone, TimeZoneID from Gen_TimeZone_S ) tz on tz.TimeZoneID in (select timezone from Usr_List_D where userid = " + userID + ") "); //ZD 103840
                //FB 2670
                strSQL.Append("left outer join (select isnull(vu.firstname + ' '+ vu.lastname, '') as vnocname,v.confid from conf_vnocoperator_d v, usr_list_d vu ");
                strSQL.Append("where v.vnocid = vu.userid) as v on a.confid = v.confid ");
                //strSQL.Append("Left Outer Join usr_list_d uo on a.ConfVNOC = uo.userid ");

                strSQL.Append("where 1=1 ");
                if (ConfUniqueID != "")
                    strSQL.Append(" and a.confnumname= '" + ConfUniqueID + "' "); //ZD 100263_SQL
                strSQL.Append(" and a.orgid=" + orgid);

                strSQL.Append(" Union All " + strSQL.ToString().Replace("Conf_Conference_D", "Archive_Conf_Conference_D").Replace("conf_user_d", "Archive_conf_user_d")
                    .Replace("conf_vnocoperator_d", "Archive_conf_vnocoperator_d").Replace("conf_room_d", "Archive_conf_room_d"));

                strSQL.Append(" Order by a.confnumname desc");
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
            }
        }
        #endregion
        //FB 2501 Ends

        # region GetCDRReports
        /// <summary>
        /// GetCDRReports
        /// </summary>
        /// <param name="strSQL"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="timezone"></param>
        /// <param name="orgid"></param>
        /// <param name="mcuId"></param>
        private void GetCDRReports(ref StringBuilder strSQL, String startDate, String endDate, String timezone
            , String userId, String orgID, String mcuId,String dtFormat)//FB 2588 // ref StringBuilder CDRSQL, String Title, int confnumname,int eventtype, String eventdescription)
        {
            try
            {

                strSQL.Append(" Select ");
                if(dtFormat =="HHmmZ")//FB 2588
                    strSQL.Append(" eventdate as [Conference Start],");
                else
                    strSQL.Append(" dbo.GMTToUserPreferedTime(" + userId + ", eventdate) as [Conference Start],");

                strSQL.Append(" confnumname as [Conference #], Title as 'Conference Title', eventtype as 'Type',eventdescription as 'Message' FROM Conf_CDR_logs_D ");
                strSQL.Append(" Where MCUid = '" + mcuId + "' AND orgid = '" + orgID + "' AND eventdate BETWEEN dbo.changeTOGMTtime(" + timezone + ",'" + startDate + "')");
                strSQL.Append(" AND dbo.changeTOGMTtime(" + timezone + ", '" + endDate + "') ");
                //strSQL.Append(" select top 1(MCUName), MCUType, MCUaddress from Conf_CDR_logs_D where mcuid = " + mcuId + " and orgId = '" + orgid + "' ");

                strSQL.Append(" Select a.BridgeName as MCUName, b.Name as MCUType, a.BridgeAddress as MCUaddress from Mcu_List_D a, MCU_Vendor_S b ");
                strSQL.Append(" Where a.BridgeId = " + mcuId + " and OrgId = '" + orgID + "' and a.BridgetypeId = b.Id  ");
                
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
            }
        }
        #endregion

        //Commented for FB 2569
        // FB 2501 p2p callMonitor start
        //#region GetEventLog
        ///// <summary>
        ///// GetEventLog 
        ///// </summary>
        ///// <param name="obj"></param>
        ///// <returns></returns>
        //public bool GetEventLog(ref vrmDataObject obj)
        //{
        //    bool bRet = true;
        //    StringBuilder strSQL = new StringBuilder();
        //    int userid = 0, confid = 0, instanceid = 0;
        //    myVRMException myVRMEx = new myVRMException();
        //    List<ICriterion> criterionList = new List<ICriterion>();
        //    String confids = "";
        //    DataSet ds = new DataSet();
        //    try
        //    {
        //        XmlDocument xd = new XmlDocument();
        //        xd.LoadXml(obj.inXml);
        //        XmlNode node = null;
        //        node = xd.SelectSingleNode("//login/userID");
        //        if (node != null)
        //            int.TryParse(node.InnerText.Trim(), out userid);
        //        else
        //        {
        //            myVRMEx = new myVRMException(201);
        //            obj.outXml = myVRMEx.FetchErrorMsg();
        //            return false;
        //        }
        //        if (userid <= 0)
        //        {
        //            myVRMEx = new myVRMException(422);
        //            obj.outXml = myVRMEx.FetchErrorMsg();
        //            return false;
        //        }
        //        node = xd.SelectSingleNode("//login/organizationID");
        //        string orgid = "";
        //        if (node != null)
        //            orgid = node.InnerText.Trim();
        //        organizationID = defaultOrgId;
        //        int.TryParse(orgid, out organizationID);
        //        if (organizationID < defaultOrgId)
        //        {
        //            myVRMEx = new myVRMException(423);
        //            obj.outXml = myVRMEx.FetchErrorMsg();
        //            return false;
        //        }
        //        node = xd.SelectSingleNode("//login/confID");
        //        if (node != null)
        //            confids = node.InnerText.Trim();
        //        if (confids.Length <= 0)
        //        {
        //            myVRMEx = new myVRMException(422);
        //            obj.outXml = myVRMEx.FetchErrorMsg();
        //            return false;
        //        }
        //        CConfID ConfMode = new CConfID(confids);
        //        confid = ConfMode.ID;
        //        instanceid = ConfMode.instance;
        //        if (instanceid == 0)
        //            instanceid = 1;

        //        int AlertType = 0;
        //        node = xd.SelectSingleNode("//login/AlertType");
        //        if (node != null)
        //            int.TryParse(node.InnerText.Trim(), out AlertType);

        //        strSQL.Append(" Select ConfID as [ConfID], InstanceID as [InstanceID], AlertTypeID as 'AlertType', Timestamp as [UpdatedDateTime], ");
        //        if(AlertType == 4) //FB 2501 Dec7
        //            strSQL.Append(" Message as [Message] FROM Conf_P2PAlert_D ");
        //        if (AlertType == 1)//FB 2501 Dec7
        //            strSQL.Append(" Message as [Message] FROM Conf_Alerts_D ");
        //        strSQL.Append(" Where ConfID = " + confid + " AND InstanceID = " + instanceid + " AND AlertTypeID = " + AlertType);
                
        //        if (strSQL.ToString() != "")
        //            ds = m_rptLayer.ExecuteDataSet(strSQL.ToString());

        //        m_rptLayer.CloseConnection();

        //        if (ds != null)
        //        {
        //            MemoryStream ms = new MemoryStream();
        //            ds.WriteXml(ms);
        //            ms.Position = 0;
        //            StreamReader sr = new StreamReader(ms, System.Text.Encoding.UTF8);
        //            String strXml = sr.ReadToEnd();
        //            obj.outXml = strXml;
        //            sr.Close();
        //            ms.Close();
        //        }

        //        if (ds.Tables[0].Rows.Count <= 0)
        //        {
        //            String oXML = "";
        //            oXML = "<NewDataSet>";
        //            for (int c = 0; c < ds.Tables[0].Columns.Count; c++)
        //            {
        //                oXML += "<" + ds.Tables[0].Columns[c].ToString().Replace(" ", "_x0020_").Replace("#", "_x0023_").Replace("(", "_x0028_").Replace(")", "_x0029_").Replace("<", "_x003C_").Replace("/", "_x002F_") + " />"; //_x0020_  --> Space , _x0023_  --> #//FB 2272
        //            }
        //            oXML += "</NewDataSet>";

        //            obj.outXml = oXML;
        //        }

        //    }
        //    catch (myVRMException e)
        //    {
        //        m_log.Error("vrmException", e);
        //        obj.outXml = e.FetchErrorMsg();
        //        bRet = false;
        //    }
        //    catch (Exception e)
        //    {
        //        m_log.Error("sytemException", e);
        //        obj.outXml = "";
        //        bRet = false;
        //    }
        //    return bRet;
        //}
        //#endregion
        // FB 2501 p2p callMonitor End




		//FB 2410 - Start
        #region SetBatchReportConfig
        /// <summary>
        /// SaveReportConfig
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SetBatchReportConfig(ref vrmDataObject obj)
        {
            bool bRet = true;
            XmlNode node = null;
            String stmt = "", requestIds = "", userName = "",Reportname="";
            BatchReportSettings rptSettings = null;
            BatchReportDates rptDates = null;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                Int32 id = 0;
                node = xd.SelectSingleNode("//SetBatchReportConfig/ID");
                if (node != null && node.InnerText != "")
                    id = int.Parse(node.InnerText.ToString().Trim());

                if (id == -1)
                {
                    rptSettings = new BatchReportSettings();

                    int rptID = 0;

                    string rptst = "SELECT max(br.id) FROM myVRM.DataLayer.BatchReportSettings br";
                    IList result = m_IBatchReportSettingsDAO.execQuery(rptst);
                    
                    if (result[0] != null)
                        int.TryParse(result[0].ToString(), out rptID);
                    rptID = rptID + 1;

                    rptSettings.ID = rptID;
                }
                else
                    rptSettings = m_IBatchReportSettingsDAO.GetById(id);

                node = xd.SelectSingleNode("//SetBatchReportConfig/DateFrom");
                if (node != null && node.InnerText != "")
                    rptSettings.DateFrom = Convert.ToDateTime(node.InnerText.ToString().Trim());
                else//ZD 101344
                    rptSettings.DateFrom = DateTime.Now;

                node = xd.SelectSingleNode("//SetBatchReportConfig/DateTo");
                if (node != null && node.InnerText != "")
                    rptSettings.DateTo = Convert.ToDateTime(node.InnerText.ToString().Trim());
                else //ZD 101344
                    rptSettings.DateTo = DateTime.Now;

                node = xd.SelectSingleNode("//SetBatchReportConfig/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerText.Trim();
                organizationID = defaultOrgId;
                int.TryParse(orgid, out organizationID);

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (organizationID < defaultOrgId)
                {
                    myvrmEx = new myVRMException(423);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                rptSettings.OrgId = organizationID;

                node = xd.SelectSingleNode("//SetBatchReportConfig/JobName");
                if (node != null && node.InnerText != "")
                    rptSettings.JobName = node.InnerText.ToString().Trim();

                node = xd.SelectSingleNode("//SetBatchReportConfig/UserID");
                if (node != null && node.InnerText != "")
                    rptSettings.CreatedUser = int.Parse(node.InnerText.ToString().Trim());

                node = xd.SelectSingleNode("//SetBatchReportConfig/RptStartDate");
                if (node != null && node.InnerText != "")
                    rptSettings.RptStartDate = Convert.ToDateTime(node.InnerText.ToString().Trim());
                else
                    rptSettings.RptStartDate = Convert.ToDateTime("1/1/1753");

                node = xd.SelectSingleNode("//SetBatchReportConfig/RptEndDate");
                if (node != null && node.InnerText != "")
                    rptSettings.RptEndDate = Convert.ToDateTime(node.InnerText.ToString().Trim());
                else
                    rptSettings.RptEndDate = Convert.ToDateTime("1/1/1753");

                node = xd.SelectSingleNode("//SetBatchReportConfig/ReportType");
                if (node != null && node.InnerText != "")
                    rptSettings.ReportType = node.InnerText.ToString().Trim();

                node = xd.SelectSingleNode("//SetBatchReportConfig/InputType");
                if (node != null && node.InnerText != "")
                    rptSettings.InputType = node.InnerText.ToString().Trim();

                node = xd.SelectSingleNode("//SetBatchReportConfig/InputValue");
                if (node != null && node.InnerText != "")
                    rptSettings.InputValue = node.InnerText.ToString().Trim();

                node = xd.SelectSingleNode("//SetBatchReportConfig/FrequencyType");
                if (node != null && node.InnerText != "")
                    rptSettings.FrequencyType = int.Parse(node.InnerText.ToString().Trim());

                node = xd.SelectSingleNode("//SetBatchReportConfig/EmailAddress");
                if (node != null && node.InnerText != "")
                    rptSettings.EmailAddress = node.InnerText.ToString().Trim();

                node = xd.SelectSingleNode("//SetBatchReportConfig/WorkingHours");
                if (node != null && node.InnerText != "")
                    rptSettings.WorkingHours = int.Parse(node.InnerText.ToString().Trim());

                node = xd.SelectSingleNode("//SetBatchReportConfig/ReportName");
                if (node != null && node.InnerText != "")
                    rptSettings.ReportName = node.InnerText.ToString().Trim();
                //ZD 101708

                if (rptSettings.ReportType == "CDD")
                {
                    node = xd.SelectSingleNode("//SetBatchReportConfig/InputParams/Operator");
                    if (node != null && node.InnerText != "")
                        rptSettings.Operator = node.InnerText.ToString().Trim();
                    else
                        rptSettings.Operator = "";

                    node = xd.SelectSingleNode("//SetBatchReportConfig/InputParams/ConfTitle");
                    if (node != null && node.InnerText != "")
                        rptSettings.ConfTitle = node.InnerText.ToString().Trim();
                    else
                        rptSettings.ConfTitle = "";

                    node = xd.SelectSingleNode("//SetBatchReportConfig/InputParams/HostID");
                    if (node != null && node.InnerText != "")
                        rptSettings.HostID = Int32.Parse(node.InnerText.ToString().Trim());
                    else
                        rptSettings.HostID = 0;

                    node = xd.SelectSingleNode("//SetBatchReportConfig/InputParams/RequestorID");
                    if (node != null && node.InnerText != "")
                        rptSettings.RequestorID = Int32.Parse(node.InnerText.ToString().Trim());
                    else
                        rptSettings.RequestorID = 0;

                    node = xd.SelectSingleNode("//SetBatchReportConfig/InputParams/VNOCID");
                    if (node != null && node.InnerText != "")
                        rptSettings.VNOCID = Int32.Parse(node.InnerText.ToString().Trim());
                    else
                        rptSettings.VNOCID = 0;

                    node = xd.SelectSingleNode("//SetBatchReportConfig/InputParams/CallURI");
                    if (node != null && node.InnerText != "")
                        rptSettings.CallURI = node.InnerText.ToString().Trim();
                    else
                        rptSettings.CallURI = "";

                    node = xd.SelectSingleNode("//SetBatchReportConfig/InputParams/Timezone");
                    if (node != null && node.InnerText != "")
                        rptSettings.Timezone = Int32.Parse(node.InnerText.ToString().Trim());

                    node = xd.SelectSingleNode("//SetBatchReportConfig/InputParams/MCUID");
                    if (node != null && node.InnerText != "")
                        rptSettings.MCUID = Int32.Parse(node.InnerText.ToString().Trim());
                    else
                        rptSettings.MCUID = 0;

                    node = xd.SelectSingleNode("//SetBatchReportConfig/InputParams/EndpointID");
                    if (node != null && node.InnerText != "")
                        rptSettings.EndpointID = Int32.Parse(node.InnerText.ToString().Trim());
                    else
                        rptSettings.EndpointID = 0;

                    node = xd.SelectSingleNode("//SetBatchReportConfig/InputParams/RoomIDs");
                    if (node != null && node.InnerText != "")
                        rptSettings.RoomIDs = node.InnerText.ToString().Trim();
                    else
                        rptSettings.RoomIDs = "";
                }

                // FB 2410 Starts
                 bool bJobName = false;
                 if (id == -1) //FB 2869
                 {
                     string rptst1 = "SELECT br.JobName FROM myVRM.DataLayer.BatchReportSettings br";
                     IList result1 = m_IBatchReportSettingsDAO.execQuery(rptst1);
                     if (result1 != null)
                     {
                         if (result1.Count > 0)
                         {
                             for (int i = 0; i < result1.Count; i++)
                             {
                                 if (result1[i].ToString() == rptSettings.JobName) //  "Jobname already exist";
                                 {
                                     bJobName = true;
                                     break;
                                 }
                             }
                         }
                     }
                 }
                if (bJobName == false)
                {
                    m_IBatchReportSettingsDAO.SaveOrUpdate(rptSettings);

                    XmlNodeList rptDateList = node.SelectNodes("//SetBatchReportConfig/ReportDates/ReportDate");

                    if (rptDateList != null && rptDateList.Count > 0)
                    {
                        List<BatchReportDates> btchRptDates = new List<BatchReportDates>();
                        List<ICriterion> criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("ReportID", rptSettings.ID));
                        criterionList.Add(Expression.Eq("Sent", 0));
                        btchRptDates = m_IBatchReportDatesDAO.GetByCriteria(criterionList);

                        foreach (BatchReportDates btchRptDate in btchRptDates)
                            m_IBatchReportDatesDAO.Delete(btchRptDate);

                        for (int i = 0; i < rptDateList.Count; i++)
                        {
                            rptDates = new BatchReportDates();
                            rptDates.ReportID = rptSettings.ID;
                            rptDates.FrequencyType = rptSettings.FrequencyType;

                            if (rptDateList[i].InnerText != "")
                                rptDates.ReportDate = Convert.ToDateTime(rptDateList[i].InnerText);

                            rptDates.SentTime = Convert.ToDateTime("1/1/1753");

                            m_IBatchReportDatesDAO.SaveOrUpdate(rptDates);
                        }
                    }
                    bRet = true;
                }
                else
                {
                    myvrmEx = new myVRMException(671);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }  
                //FB 2410 Ends


               

            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region GetAllBatchReports
        public bool GetAllBatchReports(ref vrmDataObject obj)
        {
            StringBuilder outXml = new StringBuilder();
            try
            {
                obj.outXml = "";
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node = null;

                node = xd.SelectSingleNode("//GetAllBatchReports/organizationID");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out organizationID);
                if (organizationID < defaultOrgId)
                {
                    myvrmEx = new myVRMException(423);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                outXml.Append("<GetAllBatchReports>");

                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("OrgId", organizationID));
                m_IBatchReportSettingsDAO.addOrderBy(Order.Asc("JobName"));
                List<BatchReportSettings> lstBatchRpts = m_IBatchReportSettingsDAO.GetByCriteria(criterionList);
                m_IBatchReportSettingsDAO.clearOrderBy();

                if (lstBatchRpts.Count > 0)
                {
                    outXml.Append("<Reports>");

                    for (int i = 0; i < lstBatchRpts.Count; i++)
                    {
                        outXml.Append("<Report>");
                        outXml.Append("<ID>" + lstBatchRpts[i].ID.ToString() + "</ID>");
                        outXml.Append("<OrgID>" + lstBatchRpts[i].OrgId.ToString() + "</OrgID>");
                        outXml.Append("<JobName>" + lstBatchRpts[i].JobName + "</JobName>");
                        outXml.Append("<ReportType>" + lstBatchRpts[i].ReportType + "</ReportType>");
                        outXml.Append("<RptStartDate>" + lstBatchRpts[i].RptStartDate.ToShortDateString() + "</RptStartDate>");
                        outXml.Append("<RptEndDate>" + lstBatchRpts[i].RptEndDate.ToShortDateString() + "</RptEndDate>");
                        outXml.Append("<InputType>" + lstBatchRpts[i].InputType + "</InputType>");
                        outXml.Append("<InputValue>" + lstBatchRpts[i].InputValue + "</InputValue>");
                        outXml.Append("<EmailAddress>" + lstBatchRpts[i].EmailAddress + "</EmailAddress>");
                        outXml.Append("<FrequencyType>" + lstBatchRpts[i].FrequencyType.ToString() + "</FrequencyType>");
                        outXml.Append("<DateFrom>" + lstBatchRpts[i].DateFrom.ToShortDateString() + "</DateFrom>");
                        outXml.Append("<DateTo>" + lstBatchRpts[i].DateTo.ToShortDateString() + "</DateTo>");
                        outXml.Append("<ReportName>"+ lstBatchRpts[i].ReportName.ToString()+"</ReportName>");
                        //ZD 101708
                        outXml.Append("<Operator>" + lstBatchRpts[i].Operator + "</Operator>");
                        outXml.Append("<ConfTitle>" + lstBatchRpts[i].ConfTitle + "</ConfTitle>");
                        outXml.Append("<HostID>" + lstBatchRpts[i].HostID + "</HostID>");
                        vrmUser usrDetail;
                        string hostName = "";
                        if (lstBatchRpts[i].HostID > 0)
                        {
                            usrDetail = m_vrmUserDAO.GetByUserId(lstBatchRpts[i].HostID);
                            hostName = usrDetail.FirstName + " " + usrDetail.LastName;
                        }
                        outXml.Append("<HostName>" + hostName + "</HostName>");

                        outXml.Append("<RequestorID>" + lstBatchRpts[i].RequestorID + "</RequestorID>");
                        string requstorName = "";
                        if (lstBatchRpts[i].RequestorID > 0)
                        {
                            usrDetail = m_vrmUserDAO.GetByUserId(lstBatchRpts[i].RequestorID);
                            requstorName = usrDetail.FirstName + " " + usrDetail.LastName;
                        }
                        outXml.Append("<RequestorName>" + requstorName + "</RequestorName>");

                        outXml.Append("<VNOCID>" + lstBatchRpts[i].VNOCID + "</VNOCID>");
                        string vnocName = "";
                        if (lstBatchRpts[i].VNOCID > 0)
                        {
                            usrDetail = m_vrmUserDAO.GetByUserId(lstBatchRpts[i].VNOCID);
                            vnocName = usrDetail.FirstName + " " + usrDetail.LastName;
                        }
                        outXml.Append("<VNOCName>" + vnocName + "</VNOCName>");

                        outXml.Append("<CallURI>" + lstBatchRpts[i].CallURI + "</CallURI>");
                            outXml.Append("<Timezone>" + lstBatchRpts[i].Timezone + "</Timezone>");

                            outXml.Append("<MCUID>" + lstBatchRpts[i].MCUID + "</MCUID>");

                            outXml.Append("<EndpointID>" + lstBatchRpts[i].EndpointID+ "</EndpointID>");

                        outXml.Append("<RoomIDs>" + lstBatchRpts[i].RoomIDs + "</RoomIDs>");

                        List<BatchReportDates> btchRptDates = new List<BatchReportDates>();
                        criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("ReportID", Convert.ToInt32(lstBatchRpts[i].ID.ToString())));
                        btchRptDates = m_IBatchReportDatesDAO.GetByCriteria(criterionList);
                        String rptDates = "";
                        
                        foreach (BatchReportDates btchRptDate in btchRptDates)
                        {
                            if (rptDates == "")
                                rptDates = btchRptDate.ReportDate.ToShortDateString();
                            else
                                rptDates = rptDates + "," + btchRptDate.ReportDate.ToShortDateString();
                        }
                        outXml.Append("<ReportDates>" + rptDates + "</ReportDates>");

                        outXml.Append("</Report>");
                    }
                    outXml.Append("</Reports>");
                }

                outXml.Append("</GetAllBatchReports>");
                obj.outXml = outXml.ToString();

                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                return false;
            }
        }
        #endregion

        #region GenerateBatchReport
        public bool GenerateBatchReport(ref vrmDataObject obj)
        {
            StringBuilder outXml = new StringBuilder();
            try
            {
                

                obj.outXml = "";
                String inXML = "";

                inXML = obj.inXml;
                XmlDocument xdi = new XmlDocument();
                xdi.LoadXml(inXML);
                XmlNode node;

                node = xdi.SelectSingleNode("//report/configpath");
                if (node != null)
                    configpath = node.InnerText;

                node = xdi.SelectSingleNode("//report/Destination");
                if (node != null)
                    destDirectory = node.InnerText;

                sysSettings.Init(configpath, true); //ALLBUGS-61 Added to Reset the site session in Remainder Service

                GetActiveBatchReports(ref outXml, obj.inXml);

                XmlDocument xd = new XmlDocument();
                xd.LoadXml(outXml.ToString());

                XmlNodeList rptList = xd.SelectNodes("//GetActiveBatchReports/Reports/Report");
                StringBuilder InXML = null;
                if (rptList != null && rptList.Count > 0)
                {
                    for (int i = 0; i < rptList.Count; i++)
                    {
                        //ReportTime = DateTime.Parse(DateTime.Now.ToString("MM/dd/yyyy")) ;
                        //ReportTime = DateTime.Parse(DateTime.Now.ToString("MM/dd/yyyy") + " " + DateTime.Now.ToString("hh:mm tt"));
                        //ReportSec =  DateTime.Parse(DateTime.Now.ToString("hh:mm ss"));
                        InXML = new StringBuilder();
                        InXML.Append("<GetUsageReports>");
                        InXML.Append("<ID>" + rptList[i]["ID"].InnerText + "</ID>");
                        InXML.Append("<OrgID>" + rptList[i]["OrgID"].InnerText + "</OrgID>");
                        InXML.Append("<JobName>" + rptList[i]["JobName"].InnerText + "</JobName>");
                        InXML.Append("<DateFrom>" + rptList[i]["DateFrom"].InnerText + "</DateFrom>");
                        InXML.Append("<DateTo>" + rptList[i]["DateTo"].InnerText + "</DateTo>");
                        InXML.Append("<DateFormat>MM/dd/yyyy</DateFormat>");
                        InXML.Append("<UserID>" + rptList[i]["userid"].InnerText + "</UserID>");
                        InXML.Append("<ReportType>" + rptList[i]["ReportType"].InnerText + "</ReportType>");
                        InXML.Append("<InputType>" + rptList[i]["InputType"].InnerText + "</InputType>");
                        InXML.Append("<InputValue>" + rptList[i]["InputValue"].InnerText + "</InputValue>");
                        //InXML.Append("<timezone>" + tmzone + "</timezone>");
                        //InXML.Append("<WorkingHours>" + workingHours + "</WorkingHours>");
                        //InXML.Append("<SelectedYear>" + SelectedYears + "</SelectedYear>");
                        //InXML.Append("<strParam>" + strParam + "</strParam>");
                        //InXML.Append("<pivotInput>" + pivotInput + "</pivotInput>");
                        InXML.Append("<organizationID>" + rptList[i]["organizationID"].InnerText + "</organizationID>");
                        InXML.Append("<Export>2</Export>");
                        InXML.Append("<FileName>" + rptList[i]["JobName"].InnerText.Trim()+" "+DateTime.Now.ToString("dd_MMM_yyyy_hh_mm_ss") + ".xls </FileName>"); 
                        InXML.Append("<EmailAddress>" + rptList[i]["EmailAddress"].InnerText +"</EmailAddress>");
                        InXML.Append("<WorkingHours>" + rptList[i]["WorkingHours"].InnerText + "</WorkingHours>");
                        InXML.Append("<ReportName>" + rptList[i]["ReportName"].InnerText + "</ReportName>");
                        //ZD 101708
                        InXML.Append("<QueryType>" + rptList[i]["Operator"].InnerText + "</QueryType>");
                        InXML.Append("<timezone>" + rptList[i]["Timezone"].InnerText + "</timezone>");
                        InXML.Append("<ConfTitle>" + rptList[i]["ConfTitle"].InnerText.Replace("'", "''") + "</ConfTitle>");
                        InXML.Append("<OwnerID>" + rptList[i]["HostID"].InnerText + "</OwnerID>");
                        InXML.Append("<RequestorID>" + rptList[i]["RequestorID"].InnerText + "</RequestorID>");
                        InXML.Append("<VNOCoperator>" + rptList[i]["VNOCID"].InnerText + "</VNOCoperator>");
                        InXML.Append("<CallURL>" + rptList[i]["CallURI"].InnerText.Replace("'", "''") + "</CallURL>");
                        InXML.Append("<EndpointID>" + rptList[i]["EndpointID"].InnerText + "</EndpointID>");
                        InXML.Append("<LocationID>" + rptList[i]["RoomIDs"].InnerText + "</LocationID>");

                        InXML.Append("</GetUsageReports>");

                        obj.inXml = InXML.ToString();

                        Boolean rptVal = false;
                        rptVal = GetUsageReports(ref obj);

                       if (rptVal)
                           continue;
                       else
                           return rptVal;
                    }
                }

                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                return false;
            }
        }
        #endregion
        
        #region GetActiveBatchReports
        private void GetActiveBatchReports(ref StringBuilder outXml, String inXml)
        {
            try
            {
                outXml.Append("<GetActiveBatchReports>");

                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("Sent", 0));
                criterionList.Add(Expression.Eq("ReportDate", Convert.ToDateTime(DateTime.Now.ToShortDateString()))); //
                List<BatchReportDates> lstBatchRpts = m_IBatchReportDatesDAO.GetByCriteria(criterionList);
               // BatchReportSettings btchRptSettings = null;

                if (lstBatchRpts.Count > 0)
                {
                    outXml.Append("<Reports>");

                    for (int i = 0; i < lstBatchRpts.Count; i++) //FB 2410
                    {
                        BatchReportSettings btchRptSettings = m_IBatchReportSettingsDAO.GetById(lstBatchRpts[i].ReportID);

                        outXml.Append("<Report>");
                        outXml.Append("<ID>" + lstBatchRpts[i].ID + "</ID>");
                        outXml.Append("<OrgID>"+btchRptSettings.OrgId+"</OrgID>");
                        outXml.Append("<JobName>" + btchRptSettings.JobName + "</JobName>");
                        outXml.Append("<ReportDate>" + lstBatchRpts[i].ReportDate + "</ReportDate>");
                        outXml.Append("<ReportType>" + btchRptSettings.ReportType + "</ReportType>");
                        outXml.Append("<InputType>" + btchRptSettings.InputType + "</InputType>");
                        outXml.Append("<InputValue>" + btchRptSettings.InputValue + "</InputValue>");
                        outXml.Append("<EmailAddress>" + btchRptSettings.EmailAddress + "</EmailAddress>");
                        outXml.Append("<DateFrom>" + btchRptSettings.DateFrom.ToString() + "</DateFrom>");
                        outXml.Append("<DateTo>" + btchRptSettings.DateTo.ToString() + "</DateTo>");
                        outXml.Append("<organizationID>" + btchRptSettings.OrgId + "</organizationID>");
                        outXml.Append("<userid>" + btchRptSettings.CreatedUser + "</userid>");
                        outXml.Append("<WorkingHours>" + btchRptSettings.WorkingHours + "</WorkingHours>");
                        outXml.Append("<ReportName>"+btchRptSettings.ReportName+"</ReportName>");

                        //ZD 101708
                        outXml.Append("<Operator>" + btchRptSettings.Operator + "</Operator>");
                        outXml.Append("<ConfTitle>" + btchRptSettings.ConfTitle + "</ConfTitle>");
                        outXml.Append("<HostID>" + btchRptSettings.HostID + "</HostID>");
                        outXml.Append("<RequestorID>" + btchRptSettings.RequestorID + "</RequestorID>");
                        outXml.Append("<VNOCID>" + btchRptSettings.VNOCID + "</VNOCID>");
                        outXml.Append("<CallURI>" + btchRptSettings.CallURI + "</CallURI>");
                        outXml.Append("<Timezone>" + btchRptSettings.Timezone+ "</Timezone>");
                        outXml.Append("<MCUID>" + btchRptSettings.MCUID + "</MCUID>");
                        outXml.Append("<EndpointID>" + btchRptSettings.EndpointID + "</EndpointID>");
                        outXml.Append("<RoomIDs>" + btchRptSettings.RoomIDs + "</RoomIDs>");

                        outXml.Append("</Report>");
                    }
                    outXml.Append("</Reports>");
                }

                outXml.Append("</GetActiveBatchReports>");
                
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                outXml.Append("");
            }
        }
        #endregion

        #region GetUsageParams
        private void GetUsageParams(ref String strParam, ref String pivotInput, ref String SelectedYears, String sDate, String eDate, Int32 workingHours
            , vrmDataObject obj1, String organizationID, String UserID, String rptType)
        {

            StringBuilder inXML = new StringBuilder();
            String outXML1 = "";
            String startFrom1 = "", endTo1 = "";
            DateTime startDate = new DateTime();
            DateTime endDate = new DateTime();
            DataRow[] detailRow = null;
            String strFilter = "";
            Int32 wkgDays = 0;
            String monthName = "";
            String pivotParam = "", pivotParam1 = "", pivotParam2 = "", pivotParam3 = "";
            int startYear = 0, endYear = 0, yearCount = 0;
            int startWeek = 0, endWeek = 0;
            strParam = "Name ";
            XmlDocument xmlDoc;
            DataSet ds1 = new DataSet();
            Int32 monthweekCount = 0;

            DateTime dFrom = new DateTime();
            DateTime dTo = new DateTime();

            dFrom = Convert.ToDateTime(sDate);
            dTo = Convert.ToDateTime(eDate);

            if (ds1 == null || ds1.Tables.Count == 0)
            {
                inXML = new StringBuilder();
                xmlDoc = new XmlDocument();
                inXML.Append("<GetUsageReports>");
                inXML.Append("<organizationID>" + organizationID + "</organizationID>");
                inXML.Append("<UserID>" + UserID + "</UserID>");
                inXML.Append("<ReportType>MY</ReportType>");
                inXML.Append("</GetUsageReports>");

                obj1.inXml = inXML.ToString();
                GetUsageReports(ref obj1);

                outXML1 = obj1.outXml;
                if (outXML1.IndexOf("<error>") < 0)
                {
                    xmlDoc.LoadXml(outXML1);
                    ds1.ReadXml(new XmlNodeReader(xmlDoc));
                }
            }
            DataTable detailsTable1 = ds1.Tables[0];
            String ss = detailsTable1.Rows[0][0].ToString();
            if (ss == "")
            {
                //rptt = false;
            }
            else
            {
                if (rptType == "WUR")
                {
                    startWeek = CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(dFrom, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
                    endWeek = CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(dTo, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);

                    Int32 wknumber = 0, ii = 0;

                    startYear = Convert.ToInt32(dFrom.Year);
                    endYear = Convert.ToInt32(dTo.Year);

                    for (int k = startYear; k <= endYear; k++)
                    {
                        if (k == startYear)
                            SelectedYears = startYear.ToString();
                        else
                            SelectedYears += "," + k.ToString();
                        yearCount = yearCount + 1;
                    }
                    monthweekCount = yearCount * 52; //Calculating week count for selected years

                    if (yearCount > 1)
                        ii = (52 * (yearCount - 1) - startWeek) + endWeek;
                    else
                        ii = endWeek - startWeek;

                    wknumber = startWeek;

                    int selectYear = startYear;

                    detailRow = null;
                    strFilter = "";
                    wkgDays = 0;
                    if (ds1 == null || ds1.Tables.Count == 0)
                    {
                        inXML = new StringBuilder();
                        xmlDoc = new XmlDocument();
                        inXML.Append("<GetUsageReports>");
                        inXML.Append("<organizationID>" + organizationID + "</organizationID>");
                        inXML.Append("<UserID>" + UserID + "</UserID>");
                        inXML.Append("<ReportType>WY</ReportType>");
                        inXML.Append("<SelectedYear>" + SelectedYears + "</SelectedYear>");
                        inXML.Append("</GetUsageReports>");

                        obj1.inXml = inXML.ToString();
                        GetUsageReports(ref obj1);
                        outXML1 = obj1.outXml;

                        if (outXML1.IndexOf("<error>") < 0)
                        {
                            xmlDoc.LoadXml(outXML1);
                            ds1.ReadXml(new XmlNodeReader(xmlDoc));
                        }
                    }
                    for (int d = 0; d <= ii; d++)
                    {
                        strFilter = "id ='" + wknumber + "' and CurrentYear ='" + selectYear + "'";
                        detailRow = ds1.Tables[3].Select(strFilter);

                        if (detailRow != null && detailRow.Length > 0)
                        {
                            wkgDays = Convert.ToInt32(detailRow[0]["WeekWorkingDays"].ToString()) * workingHours;
                        }

                        strParam += ", isnull(Max([Week" + wknumber + selectYear + "]),0) as [Confs" + wknumber + "], isnull(max([Week" + wknumber + selectYear + "1]),0) as [Total Hours" + wknumber + "]";
                        strParam += ", isnull(Max([Week" + wknumber + selectYear + "2])," + wkgDays + ") as [Avail.Hours" + wknumber + "]";
                        strParam += ", isnull(Max([Week" + wknumber + selectYear + "3]),0) as [Usage" + wknumber + "]";

                        if (d == 0)
                            pivotParam = "[Week" + wknumber + selectYear + "]";
                        else
                            pivotParam += ",[Week" + wknumber + selectYear + "]";

                        if (d == 0)
                            pivotParam1 = "[Week" + wknumber + selectYear + "1]";
                        else
                            pivotParam1 += ",[Week" + wknumber + selectYear + "1]";

                        if (d == 0)
                            pivotParam2 = "[Week" + wknumber + selectYear + "2]";
                        else
                            pivotParam2 += ",[Week" + wknumber + selectYear + "2]";

                        if (d == 0)
                            pivotParam3 = "[Week" + wknumber + selectYear + "3]";
                        else
                            pivotParam3 += ",[Week" + wknumber + selectYear + "3]";

                        wknumber = wknumber + 1;
                        if (wknumber == 52)
                        {
                            selectYear = selectYear + 1;
                            wknumber = 1;
                        }
                    }
                    pivotInput = pivotParam + ";" + pivotParam1 + ";" + pivotParam2 + ";" + pivotParam3;

                }
                else
                {
                    startFrom1 = dFrom.Month + "/01/" + dFrom.Year;
                    endTo1 = dTo.Month + "/01/" + dTo.Year;

                    startDate = Convert.ToDateTime(startFrom1);
                    endDate = Convert.ToDateTime(endTo1);

                    startYear = startDate.Year;
                    endYear = endDate.Year;

                    for (int k = startYear; k <= endYear; k++)
                    {
                        if (k == startYear)
                            SelectedYears = startYear.ToString();
                        else
                            SelectedYears += "," + k.ToString();
                        yearCount = yearCount + 1;
                    }

                    monthweekCount = yearCount * 12; //Calculating month count for selected years

                    detailRow = null;
                    strFilter = "";
                    wkgDays = 0;
                    if (ds1 == null || ds1.Tables.Count == 0)
                    {
                        inXML = new StringBuilder();
                        xmlDoc = new XmlDocument();
                        inXML.Append("<GetUsageReports>");
                        inXML.Append("<organizationID>" + organizationID + "</organizationID>");
                        inXML.Append("<UserID>" + UserID + "</UserID>");
                        inXML.Append("<ReportType>MY</ReportType>");
                        inXML.Append("<SelectedYear>" + SelectedYears + "</SelectedYear>");
                        inXML.Append("</GetUsageReports>");

                        obj1.inXml = inXML.ToString();
                        GetUsageReports(ref obj1);
                        outXML1 = obj1.outXml;

                        if (outXML1.IndexOf("<error>") < 0)
                        {
                            xmlDoc.LoadXml(outXML1);
                            ds1.ReadXml(new XmlNodeReader(xmlDoc));
                        }
                    }

                    for (DateTime dd = startDate; dd <= endDate; dd = dd.AddMonths(1))
                    {

                        strFilter = "id ='" + dd.Month + "' and CurrentYear ='" + dd.Year + "'";
                        detailRow = ds1.Tables[1].Select(strFilter);

                        if (detailRow != null && detailRow.Length > 0)
                        {
                            wkgDays = Convert.ToInt32(detailRow[0]["MonthWorkingDays"].ToString()) * workingHours;
                        }

                        monthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(dd.Month);

                        strParam += ", isnull(Max([" + monthName + dd.Year + "]),0) as [Confs" + dd.Month + "], isnull(max([" + monthName + dd.Year + "1]),0) as [Total Hours" + dd.Month + "]";
                        strParam += ", isnull(Max([" + monthName + dd.Year + "2])," + wkgDays + ") as [Avail.Hours" + dd.Month + "]";
                        strParam += ", isnull(Max([" + monthName + dd.Year + "3]),0) as [Usage" + dd.Month + "]";

                        if (dd == startDate)
                            pivotParam = "[" + monthName + dd.Year + "]";
                        else
                            pivotParam += ",[" + monthName + dd.Year + "]";

                        if (dd == startDate)
                            pivotParam1 = "[" + monthName + dd.Year + "1]";
                        else
                            pivotParam1 += ",[" + monthName + dd.Year + "1]";

                        if (dd == startDate)
                            pivotParam2 = "[" + monthName + dd.Year + "2]";
                        else
                            pivotParam2 += ",[" + monthName + dd.Year + "2]";

                        if (dd == startDate)
                            pivotParam3 = "[" + monthName + dd.Year + "3]";
                        else
                            pivotParam3 += ",[" + monthName + dd.Year + "3]";
                    }
                    pivotInput = pivotParam + ";" + pivotParam1 + ";" + pivotParam2 + ";" + pivotParam3;
                }
            }
        }
        #endregion
        //FB 2410 end
        

        #region DeleteBatchReport
        public bool DeleteBatchReport(ref vrmDataObject obj)
        {
            XmlDocument xd = new XmlDocument();
            xd.LoadXml(obj.inXml);
            XmlNode node;

            Int32 id = 0;
            node = xd.SelectSingleNode("//delBatchReport/JobID");
            if (node != null && node.InnerText != "")
                id = int.Parse(node.InnerText.ToString().Trim());

            string deleteJobName = " ";
            string delJobDate = " ";

            deleteJobName = "Delete from BatchRpt_Settings_D where id='" + id + "'";
            m_rptLayer.OpenConnection();
            m_rptLayer.ExecuteNonQuery(deleteJobName);
            m_rptLayer.CloseConnection();

            delJobDate = "Delete from BatchRpt_Dates_D where ReportID='" + id + "'";
            m_rptLayer.OpenConnection();
            m_rptLayer.ExecuteNonQuery(delJobDate);
            m_rptLayer.CloseConnection();
            
            return true;
        }
        #endregion

		//FB 2593 Starts
        
        #region GetCDRScheduledReports

        private void GetCDRScheduledReports(ref StringBuilder strSQL, ref StringBuilder strSQL1, ref StringBuilder strSQL2, string startDate, string endDate, string timezone
            , string userId, string dtFormat, string mcu, string strorgid)
        {
            StringBuilder strTemp = new StringBuilder();
            StringBuilder strTemp1 = new StringBuilder();
            try
            {
                strSQL.Append(" select 1 as userid, 1 as roomid, 1 as endpointid, a.bridgeid as bridgeid, '' as ConfID,'' as PartyGUID, a.BridgeName as [MCU Name], b.orgname as [Org Name] , ");//FB 3066
                strSQL.Append(" case a.EnableCDR when 1 then isnull(cast(a.LastDateTimeCDRPoll as varchar(100)), 'N/A') when 0 then 'N/A' end as [Next Poll in Mins] ");
                strSQL.Append(" from Mcu_List_D a, Org_list_D b where a.deleted = 0 and a.orgId = b.orgId ");
                //FB 3045
                if(strorgid != "")
                    strSQL.Append(" and (b." + strorgid + " or a.ispublic = 1)");

                if (mcu != "")
                    strSQL.Append(" and a.BridgeID in (" + mcu + ")");

                strSQL1.Append(" select '' as PartyGUID,* from ( ");
                strSQL1.Append(" Select distinct MCUId as bridgeid, a.ConfNumname as ConfID, a.ConfNumname as [myVRM ConfID],cast(a.MCUConfID as varchar(100)) as [MCU ConfID],  a.Title, [Host], ");
                if (dtFormat == "HHmmZ")
                    strSQL1.Append(" ScheduledStart as [Scheduled Start], ScheduledEnd as [Scheduled End],");
                else
                    strSQL1.Append(" dbo.changeTime(" + timezone + ", ScheduledStart) as [Scheduled Start],dbo.changeTime(" + timezone + ", ScheduledEnd) as [Scheduled End], a.Duration, "); //FB 2944

                strSQL1.Append(" t.StandardName as TimeZone"); //FB 2944
                strSQL1.Append(", (select cast(min(dbo.changeTime(" + timezone + ", EndpointDatetime)) as varchar(100)) from  Conf_CDREndpoints_D c where a.confnumname = c.confnumname and EndpointConnectStatus = 1) as [Connect Time]");
                strSQL1.Append(", (select cast(max(dbo.changeTime(" + timezone + ", EndpointDatetime)) as varchar(100)) from  Conf_CDREndpoints_D c where a.confnumname = c.confnumname and EndpointConnectStatus = 2) as [Disconnect Time]");
                strSQL1.Append(",(select top 1 c.DisconnectReason from Conf_CDREndpoints_D c where EndpointDatetime = (select max(EndpointDatetime) ");
                strSQL1.Append(" from Conf_CDREndpoints_D r where r.confnumname = c.confnumname and EndpointConnectStatus = 2) ");
                strSQL1.Append(" and a.confnumname = c.confnumname ) as [Disconnect Reason]");
                strSQL1.Append(" from Conf_CDR_logs_D a, Conf_CDREndpoints_D b, Gen_TimeZone_S t, Org_list_D o ");
                strSQL1.Append(" Where a.conftimezone = t.timezoneid and a.orgid = o.orgid and a.confnumname = b.confnumname ");

                if (strorgid != "")
                    strTemp.Append(" and o." + strorgid);

                if (mcu != "")
                    strTemp.Append(" and a.mcuid in (" + mcu + ")");

                if (startDate != "" && endDate != "")
                {
                    strTemp.Append(" AND a.ScheduledStart >= dbo.changeTOGMTtime(" + timezone + ",'" + startDate + "')");
                    strTemp.Append(" AND a.ScheduledEnd <= dbo.changeTOGMTtime(" + timezone + ", '" + endDate + "') ");
                }

                strSQL1.Append(strTemp.ToString());

                //From Conference table
                strSQL1.Append(" Union ");

                strSQL1.Append("  Select distinct bridgeID, a.ConfNumname as ConfID, a.ConfNumname as [myVRM ConfID], ");
                strSQL1.Append(" cast(a.ConfNumname as varchar(100)) as [MCU ConfID], a.externalname as [Meeting Title], a.HostName as [Host],  dbo.changeTime(a.timezone, a.confdate) as [Scheduled Start],");
                strSQL1.Append(" dbo.changeTime(a.timezone, DATEADD(minute, a.duration, a.confdate)) as [Scheduled End], a.Duration, ");
                strSQL1.Append(" t.StandardName as TimeZone, 'N/A' as [Connect Time],  'N/A' as [Disconnect Time],  'N/A' as [Disconnect Reason] ");
                strSQL1.Append(" from Conf_Conference_D a, Conf_Bridge_D b,  Gen_TimeZone_S t, Org_list_D o ");
                strSQL1.Append(" Where a.permanent = 0  AND a.isHDBusy = 0 and a.isVMR = 0 and a.timezone = t.timezoneid and a.orgid = o.orgid and a.confid = b.Confid and a.instanceid = b.instanceid and a.deleted = 0 ");//ZD 101348 //ALLDEV-807
                if (strorgid != "")
                    strSQL1.Append(" and o." + strorgid);
				//FB 2944 Starts
                strSQL1.Append(" and  a.confnumname not in( select confnumname from Conf_CDR_logs_D "); //FB 2944

                if (mcu != "")
                {
                    strSQL1.Append(" where McuId in (" + mcu + ") ) ");  //FB 2944
                    strSQL1.Append(" and b.BridgeID in (" + mcu + ") ");
                }
                else
                    strSQL1.Append(" ) ");
				//FB 2944 End
                if (startDate != "" && endDate != "")
                {
                    strSQL1.Append(" AND a.confdate >= dbo.changeTOGMTtime(" + timezone + ",'" + startDate + "') ");
                    strSQL1.Append(" AND DATEADD(minute, a.duration, a.confdate) <= dbo.changeTOGMTtime(" + timezone + ", '" + endDate + "') ");
                    strSQL1.Append(" AND DATEADD(minute, a.duration, a.confdate) < dbo.changeTOGMTtime(" + sysSettings.TimeZone + ", GETDATE()) ");
                }
                //ZD 101835
                strSQL1.Append(" Union All ");
                strSQL1.Append("  Select distinct bridgeID, a.ConfNumname as ConfID, a.ConfNumname as [myVRM ConfID], ");
                strSQL1.Append(" cast(a.ConfNumname as varchar(100)) as [MCU ConfID], a.externalname as [Meeting Title], a.HostName as [Host],  dbo.changeTime(a.timezone, a.confdate) as [Scheduled Start],");
                strSQL1.Append(" dbo.changeTime(a.timezone, DATEADD(minute, a.duration, a.confdate)) as [Scheduled End], a.Duration, ");
                strSQL1.Append(" t.StandardName as TimeZone, 'N/A' as [Connect Time],  'N/A' as [Disconnect Time],  'N/A' as [Disconnect Reason] ");
                strSQL1.Append(" from Archive_Conf_Conference_D a, Archive_Conf_Bridge_D b,  Gen_TimeZone_S t, Org_list_D o ");
                strSQL1.Append(" Where a.permanent = 0 AND a.isHDBusy = 0 and a.isVMR = 0 and a.timezone = t.timezoneid and a.orgid = o.orgid and a.confid = b.Confid and a.instanceid = b.instanceid and a.deleted = 0 ");//ZD 101348 //ALLDEV-807
                if (strorgid != "")
                    strSQL1.Append(" and o." + strorgid);                
                strSQL1.Append(" and  a.confnumname not in( select confnumname from Conf_CDR_logs_D "); 

                if (mcu != "")
                {
                    strSQL1.Append(" where McuId in (" + mcu + ") ) ");
                    strSQL1.Append(" and b.BridgeID in (" + mcu + ") ");
                }
                else
                    strSQL1.Append(" ) ");
                
                if (startDate != "" && endDate != "")
                {
                    strSQL1.Append(" AND a.confdate >= dbo.changeTOGMTtime(" + timezone + ",'" + startDate + "') ");
                    strSQL1.Append(" AND DATEADD(minute, a.duration, a.confdate) <= dbo.changeTOGMTtime(" + timezone + ", '" + endDate + "') ");
                    strSQL1.Append(" AND DATEADD(minute, a.duration, a.confdate) < dbo.changeTOGMTtime(" + sysSettings.TimeZone + ", GETDATE()) ");
                }

                strSQL1.Append(" ) as x ");
                strSQL1.Append(" order by [myVRM ConfID] ");

                StringBuilder strSQL3 = new StringBuilder();

                strSQL3.Append(" select (cast(A.PartyGUID as varchar(100)) +'-'+ cast(A.ConfNumname as varchar(10))) as PartyGUID, bridgeid, a.ConfNumname as ConfID, A.EndpointName as [Room], A.Endpointaddress as [Endpoint],");
                strSQL3.Append(" case a.ConnectionType when 1 then 'Dial-In' when 2 then 'Dial-Out' end as [Connection Type],"); //ZD 100288
                strSQL3.Append(" (cast(min(dbo.changeTime(" + timezone + ", A.EndpointDateTime))as varchar(100)) ) as [Connect Time], (cast(max(dbo.changeTime(" + timezone + ", B.EndpointDateTime))as varchar(100)) ) as [Disconnect Time]");//FB 2944
                strSQL3.Append(" , (select top 1 disconnectreason from Conf_CDREndpoints_D c where EndpointDatetime ");
                strSQL3.Append(" = (select max(EndpointDatetime) from Conf_CDREndpoints_D d where c.PartyGUID = d.PartyGUID and c.ConfNumname = d.ConfNumname ");
                strSQL3.Append(" and Endpointconnectstatus = 2) and c.ConfNumname = a.ConfNumname and c.PartyGUID = a.PartyGUID) as [Disconnect Reason]");
                strSQL3.Append(" from (select * from Conf_CDREndpoints_D where Endpointconnectstatus = 1 ) A ");
                strSQL3.Append(" inner join (select * from Conf_CDREndpoints_D where Endpointconnectstatus = 2 ) B ");
                strSQL3.Append(" on A.PartyGUID = B.PartyGUID and A.confnumname = B.confnumname");
                strSQL3.Append(" , (select confnumname, mcuid as bridgeid from Conf_CDR_logs_D a where 1 = 1 ");

                if (strorgid != "")
                    strSQL3.Append(" and a." + strorgid);

                if (mcu != "")
                    strSQL3.Append(" and mcuid in (" + mcu + ")");

                if (startDate != "" && endDate != "")
                {
                    strSQL3.Append(" AND a.ScheduledStart >= dbo.changeTOGMTtime(" + timezone + ",'" + startDate + "')");
                    strSQL3.Append(" AND a.ScheduledEnd <= dbo.changeTOGMTtime(" + timezone + ", '" + endDate + "') ");
                }
                strSQL3.Append(" ) as c where a.confnumname = c.confnumname ");
                strSQL3.Append(" group by A.PartyGUID,A.Confnumname,A.ConfID, bridgeid, A.Endpointname,A.Endpointaddress,case a.ConnectionType when 1 then 'Dial-In' when 2 then 'Dial-Out' end ");

                //From Conference table
                strSQL3.Append(" Union ");

                StringBuilder strSub = new StringBuilder();

                strSub.Append(" select (cast(b.endpointid as varchar(100)) +'-'+ cast(A.ConfNumname as varchar(10))) as [PartyGUID], c.bridgeid, A.ConfNumname as ConfID, b.PartyName as [Room], b.ipisdnaddress as [Endpoint], ");
                strSub.Append(" case b.connectiontype when 1 then 'Dial-In' when 2 then 'Dial-Out' end as [Connection Type],"); //ZD 100288
                strSub.Append(" 'N/A' as [Connect Time],  'N/A' as [Disconnect Time],  'N/A' as [Disconnect Reason] ");
                strSub.Append(" from Conf_Conference_D a, Conf_Room_D b,  Conf_Bridge_D c  where a.permanent = 0 AND a.isHDBusy = 0 and a.isVMR = 0 and  a.deleted = 0 and ");//ZD 101348 //ALLDEV-807
                strSub.Append(" a.confid = b.ConfID and a.instanceid = b.instanceID and ");
                strSub.Append(" a.confid = c.ConfID and a.instanceid = c.instanceID ");
                if (strorgid != "") //FB 2944
                    strSub.Append(" and a." + strorgid);
				//FB 2944
                strSub.Append(" and  a.confnumname not in( select confnumname from Conf_CDR_logs_D  ");

                if (mcu != "")
                {
                    strSub.Append(" where McuId in (" + mcu + ") ) ");
                    strSub.Append(" and b.BridgeID in (" + mcu + ") ");
                }
                else
                    strSub.Append(" ) ");


                if (startDate != "" && endDate != "")
                {
                    strSub.Append(" AND a.confdate >= dbo.changeTOGMTtime(" + timezone + ",'" + startDate + "') ");
                    strSub.Append(" AND DATEADD(minute, a.duration, a.confdate) <= dbo.changeTOGMTtime(" + timezone + ", '" + endDate + "') ");
                }
                strSub.Append(" union ");
                strSub.Append(" select (cast(b.userid as varchar(100)) +'-'+ cast(A.ConfNumname as varchar(10))) as [PartyGUID], c.bridgeid, A.ConfNumname as ConfID, b.PartyName as [Room], b.ipisdnaddress as [Endpoint], ");
                strSub.Append(" case b.connectiontype when 1 then 'Dial-In' when 2 then 'Dial-Out' end as [Connection Type],"); //ZD 100288
                strSub.Append(" 'N/A' as [Connect Time],  'N/A' as [Disconnect Time],  'N/A' as [Disconnect Reason] ");
                strSub.Append(" from Conf_Conference_D a, Conf_User_D b, Conf_Bridge_D c where a.permanent = 0 AND a.isHDBusy = 0 and a.isVMR = 0 and b.invitee = 1 and a.deleted = 0 and "); //FB 2944 //ZD 101348 //ALLDEV-807
                strSub.Append(" a.confid = b.ConfID and a.instanceid = b.instanceID and ");
                strSub.Append(" a.confid = c.ConfID and a.instanceid = c.instanceID ");
                if (strorgid != "") //FB 2944
                    strSub.Append(" and a." + strorgid);
				//FB 2944 Starts
                strSub.Append(" and  a.confnumname not in( select confnumname from Conf_CDR_logs_D ");

                if (mcu != "")
                {
                    strSub.Append(" where McuId in (" + mcu + ") ) ");
                    strSub.Append(" and b.BridgeID in (" + mcu + ") ");
                }
                else
                    strSub.Append(" ) ");
				//FB 2944 End
                if (startDate != "" && endDate != "")
                {
                    strSub.Append(" AND a.confdate >= dbo.changeTOGMTtime(" + timezone + ",'" + startDate + "') ");
                    strSub.Append(" AND DATEADD(minute, a.duration, a.confdate) <= dbo.changeTOGMTtime(" + timezone + ", '" + endDate + "') ");
                    strSub.Append(" AND DATEADD(minute, a.duration, a.confdate) < dbo.changeTOGMTtime(" + sysSettings.TimeZone + ", GETDATE()) ");
                }

                strSQL3.Append(strSub.ToString() + " Union All " + strSub.ToString().Replace("Conf_Conference_D", "Archive_Conf_Conference_D")
                    .Replace("Conf_User_D", "Archive_Conf_User_D").Replace("Conf_Bridge_D", "Archive_Conf_Bridge_D").Replace("Conf_Room_D","Archive_Conf_Room_D"));

                strSQL3.Append(";");
                    
                strSQL3.Append(" select (cast(A.PartyGUID as varchar(100)) +'-'+ cast(A.ConfNumname as varchar(10))) as PartyGUID, 0 as bridgeid, 0 as ConfID, A.EndpointName as [Room]"); //FB 2944
                strSQL3.Append(" , dbo.changeTime(" + timezone + ",A.EndpointDatetime) as [Connect Time],dbo.changeTime(" + timezone + ",A.EndpointDatetime) as [Disconnect Time],  Endpointconnectstatus, DisconnectReason as [Disconnect Reason] ");
                strSQL3.Append(" from Conf_CDREndpoints_D A where A.confid in (select confid from Conf_CDR_logs_D a where 1 = 1 ");
                if (strorgid != "") //FB 2944
                    strSQL3.Append(" and a." + strorgid);
                if (mcu != "")
                    strSQL3.Append(" and mcuid in (" + mcu + ")");

                if (startDate != "" && endDate != "")
                {
                    strSQL3.Append(" AND a.ScheduledStart >= dbo.changeTOGMTtime(" + timezone + ",'" + startDate + "')");
                    strSQL3.Append(" AND a.ScheduledEnd <= dbo.changeTOGMTtime(" + timezone + ", '" + endDate + "') ");
                }
                strSQL3.Append(" ) ");
                strSQL3.Append(" order by Confid, instanceid, Endpointname, [Connect Time], EndpointDateTime, PartyGUID "); //FB 2944

                strSQL2.Append(strSQL3.ToString());
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
            }
        }

        #endregion

        //GetVMRCDRReports(ref strSQL, ref strSQL1, startDate, endDate, timezone, userId, dtFormat, mcu);
        #region GetVMRCDRReports - Not Used //ZD 101348

        private void GetVMRCDRReports(ref StringBuilder strSQL, ref StringBuilder strSQL1, String startDate, String endDate, String timezone
            , String userId, String dtFormat, String mcu, String strorgid)
        {
            try
            {
                strSQL.Append(" select 1 as userid, 1 as roomid, 1 as endpointid, '' as ConfID,'' as PartyGUID,a.bridgeid as bridgeid , a.BridgeName as [MCU Name], b.orgname as [Org Name] , case a.EnableCDR when 1 then isnull(cast(a.LastDateTimeCDRPoll as varchar(100)), 'N/A') when 0 then 'N/A' end as [Next Poll in Mins] "); //FB 2944 //FB 3066
                strSQL.Append(" from Mcu_List_D a, Org_list_D b where a.deleted = 0 and a.orgId = b.orgId ");
                //FB 3045
                if (strorgid != "")
                    strSQL.Append(" and (b." + strorgid + " or a.ispublic = 1)");

                if (mcu != "")
                    strSQL.Append(" and a.BridgeID in (" + mcu + ")");

                //strSQL.Append(" select distinct a.mcuid as bridgeid, a.Mcuname as [MCU Name], o.OrgName as [Org Name] ");
                //strSQL.Append(" from VMR_CDR_Logs_D a, Org_list_d o where a.OrgID = O.Orgid ");

                strSQL1.Append(" select 1 as userid, 1 as roomid, 1 as endpointid, '' as ConfID,'' as PartyGUID,a.mcuid as bridgeid, VMRAddress as VMR, StartDate as Date, TotalMinActual as [Total Min Actual],");//FB 3066
                strSQL1.Append(" TotalPortMinActual as [Total Port Min Actual] from VMR_CDR_Logs_D a where 1=1 ");

                StringBuilder strTemp = new StringBuilder();                                                            
                if (mcu != "")
                    strTemp.Append(" and a.mcuid in (" + mcu + ")");

                if (startDate != "" && endDate != "")
                {
                    strTemp.Append(" AND a.StartDate between dbo.changeTOGMTtime(" + timezone + ",'" + startDate + "')");
                    strTemp.Append(" and dbo.changeTOGMTtime(" + timezone + ", '" + endDate + "') ");
                }

                if (strTemp.ToString() != "")
                {
                    //strSQL.Append(strTemp.ToString());
                    strSQL1.Append(strTemp.ToString());
                }
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
            }
        }
        #endregion

        #region GetMCUConferenceReport
		//FB 2593 Starts
        private void GetEptAndMCUConferenceReport(ref StringBuilder strSQL, ref StringBuilder strSQL1, String type, String userCommon, String hostSelect, String roomType,
            String rmImmediate, String subConfSelect, String eptDetails, String mcuDetails, String confBehaviorFilter, String woDetails, String scheduledFilter,
            String confTypeFilter, String rmAsset, String rmInchargeSelect, String departmentid, String startDate, String endDate, String tmZone,
            String immediate, String tierInfo, String eptcolDetails, String isSecondTable, String mcu, String room, String endPoints, String strorgid)
        {
            try
            {
                strSQL.Append(" select distinct 1, 1 as ConfID, 1 as userid, 1 as roomid "); //FB 3066

                if (type == "4")
                    strSQL.Append(",cast(isnull(bridgeid,'') as varchar(5)) + '-' + cast(isnull(endpointid,'') as varchar(5)) + '-' + cast(isnull(roomid,'') as varchar(5)) as endpointid, 1 as bridgeid"); //FB 3066

                eptcolDetails = eptcolDetails.Replace("[MCU Assignment],","");

                if (type == "5")
                    strSQL.Append(",[MCU Assignment]");

                if (type == "4" && eptcolDetails == "")
                    strSQL.Append(",[Endpoint Name]");

                if (type == "5")
                    strSQL.Append(",cast(isnull(bridgeid,'') as varchar(5)) + '-' + cast(isnull(endpointid,'') as varchar(5)) + '-' + cast(isnull(roomid,'') as varchar(5)) as bridgeid, 1 as endpointid"); //FB 3066

                if (eptcolDetails != "")
                    strSQL.Append(", " + eptcolDetails);

                if (tierInfo != "")
                    strSQL.Append(" ," + tierInfo);

                if (userCommon != "" && (type == "4" || type == "5")) //For using again in this line for give to preference for Endpoint Columns
                    strSQL.Append(", " + userCommon);

                if(userCommon == "")
                    strSQL.Append(", [Name] as [Room Name] ");

                strSQL.Append(" from ( select distinct a.roomid, a.[Name] ");

                strSQL.Append(" , Tier1, Tier2, RoomPhone, Capacity, Address1, Address2, RoomFloor, RoomNumber, City ");
                strSQL.Append(" , (select State from Gen_State_S s where s.stateid = a.State) as State ");
                strSQL.Append(" , (select CountryName from Gen_Country_S c where c.Countryid = a.country) as Country ");
                strSQL.Append(" , ZipCode, (select StandardName from Gen_TimeZone_S g where g.timezoneid = a.timezoneid) as TimeZone ");

                strSQL.Append(" ,(select lastname from (select ROW_NUMBER() over (PARTITION BY l1.roomid order by id asc) as 'rowNum',  ");
                strSQL.Append(" (select lastname from usr_list_d u1 where u1.userid = l1.approverid) as lastname ");
                strSQL.Append(" from Loc_Approver_D l1 where l1.roomid = a.roomid) as a where rownum = 1) as [Approver 1] ");

                strSQL.Append(" ,(select lastname from (select ROW_NUMBER() over (PARTITION BY l1.roomid order by id asc) as 'rowNum',  ");
                strSQL.Append(" (select lastname from usr_list_d u1 where u1.userid = l1.approverid) as lastname ");
                strSQL.Append(" from Loc_Approver_D l1 where l1.roomid = a.roomid) as a where rownum = 2) as [Approver 2] ");

                strSQL.Append(" ,(select lastname from (select ROW_NUMBER() over (PARTITION BY l1.roomid order by id asc) as 'rowNum',  ");
                strSQL.Append(" (select lastname from usr_list_d u1 where u1.userid = l1.approverid) as lastname ");
                strSQL.Append(" from Loc_Approver_D l1 where l1.roomid = a.roomid) as a where rownum = 3) as [Approver 3] ");

                if (type == "4")
                    strSQL.Append(" , ep.Name as [Endpoint Name], ep.profileName as [Default Profile Name], ep.endpointid ,'' as [MCU Assignment],ep.bridgeid");

                if (type == "5")
                    strSQL.Append(",BridgeName as [MCU Assignment], ep.Name as [Endpoint Name], ep.profileName as [Default Profile Name], ep.endpointid,ep.bridgeid");

                if (eptcolDetails != "")
                {
                    strSQL.Append(" , (select count(*) from ept_list_d el where el.endpointid = a.endpointid) as [Number of Profiles] ");
                    strSQL.Append(" ,  (select name from Gen_AddressType_S s where ep.addresstype = s.id) as [Address Type] , ep.Address, ");
                    strSQL.Append(" (select VEname from Gen_VideoEquipment_S v where ep.videoequipmentid = v.veid) as Model");
                    strSQL.Append(" ,(select LInerateType from Gen_LineRate_S v where ep.linerateid = v.linerateid) as [Preferred Bandwidth]");

                    strSQL.Append(" ,(select DialingOption from Gen_DialingOption_S do where do.id = ep.connectiontype) as [Pre. Dialing Option]");
                    strSQL.Append(" ,(select VideoProtocolType from Gen_VideoProtocol_S v where v.videoprotocolid= ep.protocol ) as [Default Protocol]");
                    strSQL.Append(" ,endptURL as [Web Access URL] , case outsidenetwork when 1 then 'Outside' else '' end as [Network Location]");
                    strSQL.Append(" , case TelnetAPI when 1 then 'Yes' else 'No' end as [Telnet Enabled],ExchangeID as [Email ID], ep.APIPortNO as [API Port] ");
                    strSQL.Append(" , case Calendarinvite when 1 then 'Yes' else 'No' end as [iCal Invite]");
                }

                if (type == "4")
                {
                    strSQL.Append(" from ept_list_d ep ");
                    strSQL.Append(" left outer join mcu_list_d m ");
                    strSQL.Append("  on m.Bridgeid = ep.bridgeid "); //for left outer join of endpoint table
                    strSQL.Append(" left outer join (select a.*, t1.Name as Tier1, t2.name as Tier2 from loc_room_D a , Loc_Tier3_D t1, Loc_Tier2_D t2 ");
                    strSQL.Append("  where a.disabled = 0 and a.extroom = 0 and a.isvmr = 0 and a.L3LocationId = t1.id and a.L2LocationId = t2.id ");
                    if (room != "")
                        strSQL.Append(" and a.Roomid in(" + room + ")");

                    strSQL.Append(" ) a ");

                    strSQL.Append(" on ep.endpointid = a.endpointid");
                    strSQL.Append(" where  ep.[name] <> '' and (profileid = 0 or isdefault = 1) and extendpoint = 0 ");
                    if (endPoints != "")
                        strSQL.Append(" and ep.endpointid in(" + endPoints + ")");

                    if (strorgid != "")
                        strSQL.Append(" and ep." + strorgid );
                }

                if (type == "5")
                {
                    strSQL.Append(" from mcu_list_d m ");
                    strSQL.Append(" left outer join ept_list_d ep ");
                    strSQL.Append(" left outer join (select a.*, t1.Name as Tier1, t2.name as Tier2 from loc_room_D a , Loc_Tier3_D t1, Loc_Tier2_D t2 ");
                    strSQL.Append("  where a.L3LocationId = t1.id and a.L2LocationId = t2.id) a ");
                    strSQL.Append(" on ep.endpointid = a.endpointid");

                    if (room != "")
                        strSQL.Append(" and a.Roomid in(" + room + ")");

                    strSQL.Append("  on m.Bridgeid = ep.bridgeid "); //for left outer join of endpoint table
                    strSQL.Append(" and  ep.[name] <> '' and (profileid = 0 or isdefault = 1) and extendpoint = 0 ");
                    if (endPoints != "")
                        strSQL.Append(" and ep.endpointid in(" + endPoints + ")");

                    if (strorgid != "")
                        strSQL.Append(" where (m." + strorgid + " or m.[ispublic]=1 )");

                    if (mcu != "")
                        strSQL.Append(" and m.bridgeid in (" + mcu + ")");

                }

                strSQL.Append(" ) as a ");
                strSQL.Append(" where [Default Profile Name] <> '' and a.Name <>'' ");

                
                
                if(type == "5")
                    strSQL.Append(" order by  [MCU Assignment]");
                else
                    strSQL.Append(" order by  [Endpoint Name]");

                //Second Table
                if (isSecondTable == "1")
                {
                    strSQL1.Append(" select distinct b.roomid, cast(isnull(b.bridgeid,'') as varchar(5)) + '-' + cast(isnull(b.endpointid,'') as varchar(5)) + '-' + cast(isnull(b.roomid,'') as varchar(5)) as bridgeid");
                    strSQL1.Append(", ConfNumName as [Conf ID], ExternalName as [Conf.Title]"); //ZD 100288
                    strSQL1.Append(", cast(isnull(b.bridgeid,'') as varchar(5)) + '-' + cast(isnull(b.endpointid,'') as varchar(5)) + '-' + cast(isnull(b.roomid,'') as varchar(5)) as endpointid ");

                    if (confBehaviorFilter != "")
                        strSQL1.Append(" , case recuring when 0 then 'Single' when 1 then 'Recurring' End as [Single/Recuring]");//ZD 100288

                    if (eptDetails != "")
                    {
                        strSQL1.Append(" ,Stuff ((SELECT distinct N', ' + e.name ");
                        if (eptDetails.IndexOf("2") >= 0)
                            strSQL1.Append(" + ' > ' + s.[name] ");
                        if (eptDetails.IndexOf("3") >= 0)
                            strSQL1.Append(" + ' - ' + r.ipisdnaddress ");
                        if (eptDetails.IndexOf("4") >= 0)
                            strSQL1.Append(" + ' - '+ do.DialingOption  ");
                        strSQL1.Append(" from conf_conference_d c ,conf_room_d r, loc_room_d l, Ept_List_D e, Gen_AddressType_S s , Gen_DialingOption_S do   ");
                        strSQL1.Append(" where c.confid = a.confid and c.instanceid = a.instanceid  and c.conftype not in (7) and c.confid = r.confid and r.roomid = l.roomid   ");
                        strSQL1.Append(" and l.endpointid = e.endpointid  and r.addresstype = s.id and do.id = r.connectiontype and e.isdefault = 1  ");
                        strSQL1.Append(" FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N'') as [Endpoints in Conf");
                        strSQL1.Append(" > Name");
                        if (eptDetails.IndexOf("2") >= 0)
                            strSQL1.Append(" - IP ");
                        if (eptDetails.IndexOf("3") >= 0)
                            strSQL1.Append(" - Add ");
                        if (eptDetails.IndexOf("4") >= 0)
                            strSQL1.Append(" - Dial.Opt. ");
                        strSQL1.Append("]");
                    }

                    if (mcuDetails.IndexOf("2") >= 0)
                    {
                        strSQL1.Append(" ,Stuff ((SELECT distinct N', ' + m.BridgeName  ");
                        strSQL1.Append(" from conf_conference_d c ,conf_room_d r, loc_room_d l, Ept_List_D e, mcu_list_D m  ");
                        strSQL1.Append(" where c.confid = a.confid and c.instanceid = a.instanceid  and c.conftype not in (7) and c.confid = r.confid   ");
                        strSQL1.Append(" and r.roomid = l.roomid and l.endpointid = e.endpointid  and e.bridgeid = m.bridgeid and e.isdefault = 1  ");
                        strSQL1.Append(" FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N'') as [Default MCU]   ");
                    }

                    if (mcuDetails.IndexOf("1") >= 0 || mcuDetails.IndexOf("3") >= 0)
                    {
                        strSQL1.Append(" ,Stuff ((SELECT distinct N', ' + m.bridgename ");
                        if (mcuDetails.IndexOf("3") >= 0)
                            strSQL1.Append(" + ' > '+ bridgeIPISDNAddress ");
                        strSQL1.Append(" from conf_conference_d c, (select distinct bridgeid, confid,instanceid,bridgeIPISDNAddress from (select bridgeid, confid, instanceid, bridgeIPISDNAddress from conf_room_d    ");
                        strSQL1.Append(" union all select bridgeid, confid, instanceid, bridgeIPISDNAddress from conf_user_d ) as b) as bb, mcu_list_D m   ");
                        strSQL1.Append(" where c.confid = a.confid and c.instanceid = a.instanceid  and  c.conftype not in (7) and c.confid = bb.confid     ");
                        strSQL1.Append(" and c.instanceid = bb.instanceid and bb.bridgeid = m.bridgeid  ");
                        strSQL1.Append(" FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N'') as [MCU Used in Conf");
                        if (mcuDetails.IndexOf("3") >= 0)
                            strSQL1.Append(" > Address");
                        strSQL1.Append("  ]");
                    }

                    if (woDetails.IndexOf("1") >= 0)
                    {
                        strSQL1.Append(" ,Stuff ((SELECT N', ' + Name FROM inv_workorder_d where type = 1 and deleted = 0 and confid = a.confid and instanceid = a.instanceid ");
                        strSQL1.Append(" FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N'') as [A/V WO]  ");
                    }

                    if (woDetails.IndexOf("2") >= 0)
                    {
                        strSQL1.Append(" ,Stuff ((SELECT N', ' + Name FROM inv_workorder_d where type = 2 and deleted = 0 and confid = a.confid and instanceid = a.instanceid ");
                        strSQL1.Append(" FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N'') as [CAT WO]  ");
                    }
                    if (woDetails.IndexOf("3") >= 0)
                    {
                        strSQL1.Append(" ,Stuff ((SELECT N', ' + Name FROM inv_workorder_d where type = 3 and deleted = 0 and confid = a.confid and instanceid = a.instanceid ");
                        strSQL1.Append(" FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N'') as [HK WO]  ");
                    }
                    strSQL1.Append(" from conf_conference_d a , conf_room_d b where a.confid not in (11) and a.confid = b.confid and a.instanceid = b.instanceid ");

                    if (confBehaviorFilter != "")
                        strSQL1.Append(" and " + confBehaviorFilter);

                    if (scheduledFilter != "" && immediate != "")
                        strSQL1.Append(" and (" + scheduledFilter + " or immediate = 1)");
                    else if (scheduledFilter != "" && immediate == "")
                        strSQL1.Append(" and " + scheduledFilter);
                    else if (scheduledFilter == "" && immediate != "")
                        strSQL1.Append(" and immediate = 1");

                    if (confTypeFilter != "")
                        strSQL1.Append(" and " + confTypeFilter);

                    //strSQL1.Append(" and (Status in (7,0,9,3,1) or immediate = 1)   ");

                    if (startDate != "" && endDate != "")
                    {
                        strSQL1.Append(" AND a.confdate BETWEEN dbo.changeTOGMTtime(" + tmZone + ",'" + startDate + "') ");
                        strSQL1.Append(" AND dbo.changeTOGMTtime(" + tmZone + ",'" + endDate + "') ");
                    }

                    //ZD 101835
                    strSQL1.Append(" Union All " + strSQL1.Replace("conf_room_d", "Archive_conf_room_d").Replace("conf_conference_d", "Archive_conf_conference_d").Replace("conf_user_d", "Archive_conf_user_d").Replace("conf_bridge_d", "Archive_conf_bridge_d"));

                }
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
            }
        }


		//FB 2593 End
        #endregion
		//ZD 100456
        #region GetRooms

        private void GetRooms(ref StringBuilder strQuery, String orgid)
        {
            try
            {
                strQuery.Append("select a.roomid as id, a.Name as [Room Name], c.Name as [Tier One], b.Name as [Tier Two], a.RoomFloor as [Floor]");
                strQuery.Append(" , a.RoomNumber [Room #], a.RoomPhone as [Room Phone], ");
                //rma.Email as [Room Administrator]");// rma.FirstName + ' ' + rma.LastName as [Room Administrator]"); //ALLDEV-807
                strQuery.Append(" ISNULL(Stuff ((SELECT distinct N'| ' + u1.Email from  Loc_Assistant_D asl, Usr_List_D u1 where asl.AssistantId = u1.UserID and asl.RoomId=a.RoomID FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N''),'') as [Room Administrator]"); //ALLDEV-807
                strQuery.Append(" , case a.ProjectorAvailable when 1 then 'Yes' else 'No' end as [Projector Available]");
                strQuery.Append(" , case a.videoAvailable when 1 then 'Audio-only' when 2 then 'Audio/Video' when 0 then 'None' end as [Media (None Audio-only Audio-Video)]"); //ZD 100528
                strQuery.Append(" , e.name as [Endpoint Name], t.TimeZoneDiff + ' ' + t.TimeZone [Time Zone]");//ZD 102398
                strQuery.Append(" , ISNULL(Stuff ((SELECT distinct N', ' + d1.departmentname from  Loc_Department_D c1, dept_list_d d1 where d1.Departmentid = c1.Departmentid ");
                strQuery.Append(" and c1.RoomId = a.RoomID FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N''),'') as [Department]");
                strQuery.Append(" ,isnull(Capacity,0) as [Maximum Capacity],case iControl when 1 then 'Yes' else 'No' end as [iControl Room] ");                           
                strQuery.Append(" , isnull(a.Address1,'') [Street Address 1], isnull(a.address2,'') [Street Address 2], isnull(a.City,'') as City, isnull(GC.CountryName,'') as Country, isnull(GS.StateCode,'') [State/Province], isnull(a.Zipcode,'') [Postal Code], isnull(RoomQueue,'') as [Room Queue]"); //ZD 104091
                strQuery.Append(" , isnull(roomimage,'') as [Room Image], isnull(MapImage1,'') as [Map 1], isnull(MapImage2,'') as [Map 2] , isnull(MiscImage1,'') as [Misc. 1], isnull(MiscImage2,'') as [Misc. 2]");
                strQuery.Append(" , ISNULL(Stuff ((SELECT distinct N'| ' + u1.Email from  Loc_Approver_D a1, Usr_List_D u1 ");
                strQuery.Append(" where a1.approverid = u1.UserID and a1.roomid=a.RoomID FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N''),'') as Approvers");
                strQuery.Append(" from Loc_Room_D a left outer join Ept_List_D e on a.endpointid = e.endpointId and e.profileId = 1");
                strQuery.Append(" left outer join Gen_Country_S GC on GC.CountryID = a.Country");//ZD 104091
                strQuery.Append(" left outer join Gen_State_S GS on GS.StateID = a.State,");
                //strQuery.Append(" left outer join Usr_List_D rmA on  a.Assistant = rmA.UserID , 
                strQuery.Append("Loc_Tier2_D b, Loc_Tier3_D c, Gen_TimeZone_S t, Org_List_D o");
                strQuery.Append(" where a.L2LocationId = b.Id and b.L3LocationId = c.Id");
                strQuery.Append(" and a.TimezoneID = t.TimeZoneID  and a.orgId = o.orgId");
                strQuery.Append(" and a.Disabled = 0 and a.IsVMR = 0 and a.Extroom = 0 and a.RoomCategory in (1,6) ");//(1,3,4,6) "); 
                if (orgid != "")
                    strQuery.Append(" and a.orgId = " + orgid);
                strQuery.Append(" order by a.Name");
                strQuery.Append(" ; "); //ALLDEV-821 - Start
                strQuery.Append("select a.roomid as id, a.Name as [Room Name], c.Name as [Tier One], b.Name as [Tier Two], a.RoomFloor as [Floor]");
                strQuery.Append(" , a.RoomNumber [Room #], a.RoomPhone as [Room Phone],");
                //rma.Email as [Room Administrator]");//ALLDEV-807
                strQuery.Append(" ISNULL(Stuff ((SELECT distinct N'| ' + u1.Email from  Loc_Assistant_D asl, Usr_List_D u1 where asl.AssistantId = u1.UserID and asl.RoomId=a.RoomID FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N''),'') as [Room Administrator]"); //ALLDEV-807
                strQuery.Append(" , case a.videoAvailable when 1 then 'Audio-only' when 2 then 'Audio/Video' when 0 then 'None' end as [Media (None Audio-only Audio-Video)]"); 
                strQuery.Append(" , e.name as [Endpoint Name], t.TimeZoneDiff + ' ' + t.TimeZone [Time Zone]");
                strQuery.Append(" , ISNULL(Stuff ((SELECT distinct N', ' + d1.departmentname from  Loc_Department_D c1, dept_list_d d1 where d1.Departmentid = c1.Departmentid ");
                strQuery.Append(" and c1.RoomId = a.RoomID FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N''),'') as [Department]");
                strQuery.Append(" ,isnull(Capacity,0) as [Maximum Capacity]");                           
                strQuery.Append(" , isnull(a.Address1,'') [Street Address 1], isnull(a.address2,'') [Street Address 2], isnull(a.City,'') as City, isnull(GC.CountryName,'') as Country, isnull(GS.StateCode,'') [State/Province], isnull(a.Zipcode,'') [Postal Code], isnull(RoomQueue,'') as [Room Queue]"); 
                strQuery.Append(" , isnull(roomimage,'') as [Room Image], isnull(MapImage1,'') as [Map 1], isnull(MapImage2,'') as [Map 2] ");
                strQuery.Append(" , ISNULL(Stuff ((SELECT distinct N'| ' + u1.Email from  Loc_Approver_D a1, Usr_List_D u1 ");
                strQuery.Append(" where a1.approverid = u1.UserID and a1.roomid=a.RoomID FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N''),'') as Approvers");
                strQuery.Append(" from Loc_Room_D a left outer join Ept_List_D e on a.endpointid = e.endpointId and e.profileId = 1");
                strQuery.Append(" left outer join Gen_Country_S GC on GC.CountryID = a.Country");
                strQuery.Append(" left outer join Gen_State_S GS on GS.StateID = a.State,");
                //strQuery.Append(" left outer join Usr_List_D rmA on  a.Assistant = rmA.UserID ,");
                strQuery.Append(" Loc_Tier2_D b, Loc_Tier3_D c, Gen_TimeZone_S t, Org_List_D o");
                strQuery.Append(" where a.L2LocationId = b.Id and b.L3LocationId = c.Id");
                strQuery.Append(" and a.TimezoneID = t.TimeZoneID  and a.orgId = o.orgId");
                strQuery.Append(" and a.Disabled = 0 and a.IsVMR = 0 and a.Extroom = 0 and a.RoomCategory in (4) ");
                if (orgid != "")
                    strQuery.Append(" and a.orgId = " + orgid);
                strQuery.Append(" order by a.Name");
                //ALLDEV-821 - End
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
            }
        }

        #endregion

        #region GetUsers

        private void GetUsers(ref StringBuilder strQuery, String orgid)
        {
            try
            {
                strQuery.Append("select a.userid as id, a.FirstName as [First Name], a.LastName as [Last Name]");
                strQuery.Append(" , case a.enableExchange when 1 then 'Yes' else 'No' end as [Outlook], case a.enableDomino when 1 then 'Yes' else 'No' end as [IBM Notes Forms]"); //102743
                strQuery.Append(" , isnull(a.Password,'') as [Initial Password], a.Email as [Email]");
                strQuery.Append(" , t.TimeZoneDiff + ' ' + t.TimeZone as [Time Zone], r.roleName as [User Role (User/Admin/Super Admin etc)]");//ZD 102398
                strQuery.Append(" , isnull(m.BridgeName,'') as [Assigned MCU (only in case of multiple)]");
                strQuery.Append(" , isnull(BJNUserEmail,'') as [Blue Jeans User Name], a.login as [AD/LDAP Login], a.UserDomain as [AD/LDAP Domain] ,  isnull(SL.Name,'') as [System Location] "); //ZD 104821 //ZD 104021 //ZD 104850
                strQuery.Append(" from Usr_List_D a left outer join Ept_List_D e left outer join Mcu_List_D m on e.BridgeID = m.BridgeID ");
                strQuery.Append(" left outer join Gen_SystemLocation_D SL on SL.SysLocId = e.SysLocationId and e.bridgeid = SL.MCUId "); //ZD 104821
                strQuery.Append("  on a.endpointId = e.endpointId, Gen_TimeZone_S t, Usr_Roles_D r, Org_List_D o");
                strQuery.Append(" where a.userid not in(11) and a.timezone = t.TimeZoneID and a.roleID = r.roleID and a.Audioaddon = 0 and a.companyId = o.orgId ");
                if (orgid != "")
                    strQuery.Append(" and a.companyId = " + orgid);
                strQuery.Append(" order by a.FirstName");
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
            }
        }

        #endregion

        #region GetEndpoints

        private void GetEndpoints(ref StringBuilder strQuery, String orgid)
        {
            try
            {
                strQuery.Append("select a.endpointId as id,a.profileId as pid, a.name as [Name], case a.ProfileType when 0 then 'Both' when 1 then 'MCU' when 2 then 'Point-To-Point' end as [Profile Type]");
                strQuery.Append(" , em.ManufacturerName as Manufacturer, v.DisplayName as [Model], a.[Address], s.name as [Address Type], isnull([Password],'') as Password");
                strQuery.Append(" , l.LineRateType as [Preferred Bandwidth], do.DialingOption as [Preferred Dialing Option], m.BridgeAddress as [MCU Assignment]");
                strQuery.Append(" , case outsidenetwork when 1 then 'Yes' else 'No' end as [Located Outside the Network],isnull(SL.Name,'') as [System Location]"); //ZD 104821
                strQuery.Append(" from ept_list_d a left outer join Mcu_List_D m on a.bridgeid = m.BridgeID left outer join Gen_SystemLocation_D SL on a.SysLocationId = SL.SysLocId and Sl.MCUId = a.bridgeid, Gen_AddressType_S s, Gen_LineRate_S l, Gen_VideoEquipment_S v"); //ZD 104821
                strQuery.Append(" , Gen_DialingOption_S do, Org_List_D o, Gen_EquipmentManufacturer_s em");
                strQuery.Append(" where a.addresstype = s.id and a.linerateid = l.linerateid");
                strQuery.Append(" and a.Extendpoint = 0 and a.deleted = 0 and a.videoequipmentid = v.veid and do.id = a.connectiontype and a.orgId = o.orgId and a.ManufacturerID = em.MID");
                if (orgid != "")                    
                    strQuery.Append(" and a.orgId = " + orgid);
                strQuery.Append(" order by a.name, a.profileId");

            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
            }
        }

        #endregion

        #region GetMCUList

        private void GetMCUList(ref StringBuilder strQuery, String orgid)
        {
            try
            {
                strQuery.Append("select a.BridgeID as id, a.BridgeName as [Name], a.BridgeLogin as [Login], isnull(a.BridgePassword,'') as [Password]");
                strQuery.Append(" , v.name as [Vendor Type], t.TimeZoneDiff + ' ' + t.TimeZone as [Time Zone], a.BridgeAddress as [Control Port IP Address]"); //ZD 102398
                strQuery.Append(" , a.SoftwareVer as [Firmware Version]");
                strQuery.Append(" from Mcu_List_D a, MCU_Vendor_S v, Gen_TimeZone_S t, Org_List_D o");
                strQuery.Append(" where a.BridgetypeId = v.id and a.timezone = t.TimeZoneID and a.orgId = o.orgId and a.deleted = 0 ");
                if (orgid != "")
                    strQuery.Append(" and a.orgId = " + orgid);
                strQuery.Append(" order by a.BridgeName");
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
            }
        }

        #endregion
		//ZD 100902
        #region GetReservationHistory

        private void GetReservationHistory(ref StringBuilder strQuery, String ConfUniqueID, String userId)
        {
            try
            {
                strQuery.Append("select confnumname [Meeting ID], AuditSummary as [Description] from Conf_AuditSummary_D a where 1 = 1 ");
                strQuery.Append(" and a.LanguageId = (select [Language] from Usr_List_D where UserID = "+ userId +") "); //ZD 102754

                if (ConfUniqueID.Trim() != "")
                    strQuery.Append(" and confnumname = " + ConfUniqueID);
                else
                    strQuery.Append(" and confnumname in (select top 1 confnumname from Conf_AuditSummary_D where userid=  " + userId + " order by CreatedDate desc) ");

                strQuery.Append(" order by CreatedDate ");
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
            }
        }

        #endregion
		//ZD 101348 Starts
        private void GetVMRmyVRMCDRReports(ref StringBuilder strSQL, ref StringBuilder strSQL1, String startDate, String endDate, String timezone
            , String userId, String dtFormat, String mcu, String strorgid)
        {
            try
            {
                strSQL.Append(" select 1 as userid, 1 as roomid, 1 as endpointid, '' as ConfID,'' as PartyGUID,a.bridgeid as bridgeid , ");
                strSQL.Append(" a.BridgeName as [MCU Name], b.orgname as [Org Name] , case a.EnableCDR when 1 then ");
                strSQL.Append(" isnull(cast(a.LastDateTimeCDRPoll as varchar(100)), 'N/A') when 0 then 'N/A' end as [Next Poll in Mins] "); //FB 2944 //FB 3066
                strSQL.Append(" from Mcu_List_D a, Org_list_D b where a.deleted = 0 and a.orgId = b.orgId ");
                //FB 3045
                if (strorgid != "")
                    strSQL.Append(" and (b." + strorgid + " or a.ispublic = 1)");

                if (mcu != "")
                    strSQL.Append(" and a.BridgeID in (" + mcu + ")");

                strSQL1.Append(" select 1 as userid, 1 as roomid, 1 as endpointid, '' as ConfID,'' as PartyGUID,a.mcuid as bridgeid, VMRAddress as VMR, StartDate as Date, TotalMinActual as [Total Min Actual],");//FB 3066
                strSQL1.Append(" TotalPortMinActual as [Total Port Min Actual] from VMR_CDR_Logs_D a where 1=1 ");

                StringBuilder strTemp = new StringBuilder();
                if (mcu != "")
                    strTemp.Append(" and a.mcuid in (" + mcu + ")");

                if (startDate != "" && endDate != "")
                {
                    strTemp.Append(" AND a.StartDate between dbo.changeTOGMTtime(" + timezone + ",'" + startDate + "')");
                    strTemp.Append(" and dbo.changeTOGMTtime(" + timezone + ", '" + endDate + "') ");
                }

                if (strTemp.ToString() != "")
                {
                    //strSQL.Append(strTemp.ToString());
                    strSQL1.Append(strTemp.ToString());
                }
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
            }
        }

        private void GetVMRCDRScheduledReports(ref StringBuilder strSQL, ref StringBuilder strSQL1, ref StringBuilder strSQL2, string startDate, string endDate, string timezone
            , string userId, string dtFormat, string mcu, string strorgid)
        {
            StringBuilder strTemp = new StringBuilder();
            StringBuilder strTemp1 = new StringBuilder();
            try
            {
                strSQL.Append(" select 1 as userid, 1 as roomid, 1 as endpointid, a.bridgeid as bridgeid, '' as ConfID,'' as PartyGUID, a.BridgeName as [MCU Name], b.orgname as [Org Name] , ");//FB 3066
                strSQL.Append(" case a.EnableCDR when 1 then isnull(cast(a.LastDateTimeCDRPoll as varchar(100)), 'N/A') when 0 then 'N/A' end as [Next Poll in Mins] ");
                strSQL.Append(" from Mcu_List_D a, Org_list_D b where a.deleted = 0 and a.orgId = b.orgId ");
                //FB 3045
                if (strorgid != "")
                    strSQL.Append(" and (b." + strorgid + " or a.ispublic = 1)");

                if (mcu != "")
                    strSQL.Append(" and a.BridgeID in (" + mcu + ")");

                strSQL1.Append(" select '' as PartyGUID,* from ( ");
                strSQL1.Append(" Select distinct MCUId as bridgeid, a.ConfNumname as ConfID, a.ConfNumname as [myVRM ConfID],cast(a.MCUConfID as varchar(100)) as [MCU ConfID],  a.Title, [Host], ");
                if (dtFormat == "HHmmZ")
                    strSQL1.Append(" ScheduledStart as [Scheduled Start], ScheduledEnd as [Scheduled End],");
                else
                    strSQL1.Append(" dbo.changeTime(" + timezone + ", ScheduledStart) as [Scheduled Start], case a.Duration when 0 then 'N/A' else cast(dbo.changeTime(" + timezone + ", ScheduledEnd)  as varchar(40))  end  as [Scheduled End], case a.Duration when 0 then 'N/A' else cast(a.Duration  as varchar(max)) end  as [Duration (mins)],  "); //FB 2944 //ZD 101348

                strSQL1.Append(" t.StandardName as TimeZone"); //FB 2944
                strSQL1.Append(", (select cast(min(dbo.changeTime(" + timezone + ", EndpointDatetime)) as varchar(100)) from  Conf_VMRCDREndpoints_D c where a.confnumname = c.confnumname and EndpointConnectStatus = 1) as [Connect Time]");
                strSQL1.Append(", (select cast(max(dbo.changeTime(" + timezone + ", EndpointDatetime)) as varchar(100)) from  Conf_VMRCDREndpoints_D c where a.confnumname = c.confnumname and EndpointConnectStatus = 2) as [Disconnect Time]");
                strSQL1.Append(",(select top 1 c.DisconnectReason from Conf_VMRCDREndpoints_D c where EndpointDatetime = (select max(EndpointDatetime) ");
                strSQL1.Append(" from Conf_VMRCDREndpoints_D r where r.confnumname = c.confnumname and EndpointConnectStatus = 2) ");
                strSQL1.Append(" and a.confnumname = c.confnumname ) as [Disconnect Reason]");
                strSQL1.Append(" from Conf_VMRCDR_logs_D a, Conf_VMRCDREndpoints_D b, Gen_TimeZone_S t, Org_list_D o ");
                strSQL1.Append(" Where a.conftimezone = t.timezoneid and a.orgid = o.orgid and a.confnumname = b.confnumname ");

                if (strorgid != "")
                    strTemp.Append(" and o." + strorgid);

                if (mcu != "")
                    strTemp.Append(" and a.mcuid in (" + mcu + ")");

                if (startDate != "" && endDate != "")
                {
                    strTemp.Append(" AND a.ScheduledStart >= dbo.changeTOGMTtime(" + timezone + ",'" + startDate + "')");
                    strTemp.Append(" AND a.ScheduledEnd <= dbo.changeTOGMTtime(" + timezone + ", '" + endDate + "') ");
                }

                strSQL1.Append(strTemp.ToString());

                //From Conference table
                strSQL1.Append(" Union ");

                StringBuilder strsub = new StringBuilder();

                strsub.Append("  Select distinct bridgeID, a.ConfNumname as ConfID, a.ConfNumname as [myVRM ConfID], ");
                strsub.Append(" cast(a.ConfNumname as varchar(100)) as [MCU ConfID], a.externalname as [Meeting Title], a.HostName as [Host],  dbo.changeTime(a.timezone, a.confdate) as [Scheduled Start],");
                strsub.Append(" cast(dbo.changeTime(a.timezone, DATEADD(minute, a.duration, a.confdate)) as varchar(40)) as [Scheduled End], case a.Duration when 0 then 'N/A' else cast(a.Duration  as varchar(max)) end  as [Duration (mins)], "); //ZD 101348
                strsub.Append(" t.StandardName as TimeZone, 'N/A' as [Connect Time],  'N/A' as [Disconnect Time],  'N/A' as [Disconnect Reason] ");
                strsub.Append(" from Conf_Conference_D a, Conf_Bridge_D b,  Gen_TimeZone_S t, Org_list_D o ");
                strsub.Append(" Where a.Permanent = 1 AND a.isHDBusy = 0 and a.timezone = t.timezoneid and a.orgid = o.orgid and a.confid = b.Confid and a.instanceid = b.instanceid and a.deleted = 0 "); //ALLDEV-807
                if (strorgid != "")
                    strsub.Append(" and o." + strorgid);
                //FB 2944 Starts
                strsub.Append(" and  a.confnumname not in( select confnumname from Conf_VMRCDR_logs_D "); //FB 2944
                if (mcu != "")
                {
                    strsub.Append(" where McuId in (" + mcu + ") ) ");  //FB 2944
                    strsub.Append(" and b.BridgeID in (" + mcu + ") ");
                }
                else
                    strsub.Append(" ) ");
                //FB 2944 End
                if (startDate != "" && endDate != "")
                {
                    strsub.Append(" AND a.confdate >= dbo.changeTOGMTtime(" + timezone + ",'" + startDate + "') ");
                    strsub.Append(" AND DATEADD(minute, a.duration, a.confdate) <= dbo.changeTOGMTtime(" + timezone + ", '" + endDate + "') ");
                    strsub.Append(" AND DATEADD(minute, a.duration, a.confdate) < dbo.changeTOGMTtime(" + sysSettings.TimeZone + ", GETDATE()) ");
                }

                strSQL1.Append(strsub + " Union All " + strsub.ToString().Replace("Conf_Conference_D", "Archive_Conf_Conference_D").Replace("Conf_Bridge_D", "Archive_Conf_Bridge_D"));

                strSQL1.Append(" ) as x ");
                strSQL1.Append(" order by [myVRM ConfID] ");

                StringBuilder strSQL3 = new StringBuilder();

                strSQL3.Append(" select (cast(A.PartyGUID as varchar(100)) +'-'+ cast(A.ConfNumname as varchar(10))) as PartyGUID, bridgeid, a.ConfNumname as ConfID, A.EndpointName as [Room], A.Endpointaddress as [Endpoint],");
                strSQL3.Append(" case a.ConnectionType when 1 then 'Dial-In' when 2 then 'Dial-Out' end as [Connection Type],"); //ZD 100288
                strSQL3.Append(" (cast(min(dbo.changeTime(" + timezone + ", A.EndpointDateTime))as varchar(100)) ) as [Connect Time], (cast(max(dbo.changeTime(" + timezone + ", B.EndpointDateTime))as varchar(100)) ) as [Disconnect Time]");//FB 2944
                strSQL3.Append(" , (select top 1 disconnectreason from Conf_VMRCDREndpoints_D c where EndpointDatetime ");
                strSQL3.Append(" = (select max(EndpointDatetime) from Conf_VMRCDREndpoints_D d where c.PartyGUID = d.PartyGUID and c.ConfNumname = d.ConfNumname ");
                strSQL3.Append(" and Endpointconnectstatus = 2) and c.ConfNumname = a.ConfNumname and c.PartyGUID = a.PartyGUID) as [Disconnect Reason]");
                strSQL3.Append(" from (select * from Conf_VMRCDREndpoints_D where Endpointconnectstatus = 1 ) A ");
                strSQL3.Append(" inner join (select * from Conf_VMRCDREndpoints_D where Endpointconnectstatus = 2 ) B ");
                strSQL3.Append(" on A.PartyGUID = B.PartyGUID and A.confnumname = B.confnumname");
                strSQL3.Append(" , (select confnumname, mcuid as bridgeid from Conf_VMRCDR_logs_D a where 1 = 1 ");

                if (strorgid != "")
                    strSQL3.Append(" and a." + strorgid);

                if (mcu != "")
                    strSQL3.Append(" and mcuid in (" + mcu + ")");

                if (startDate != "" && endDate != "")
                {
                    strSQL3.Append(" AND a.ScheduledStart >= dbo.changeTOGMTtime(" + timezone + ",'" + startDate + "')");
                    strSQL3.Append(" AND a.ScheduledEnd <= dbo.changeTOGMTtime(" + timezone + ", '" + endDate + "') ");
                }
                strSQL3.Append(" ) as c where a.confnumname = c.confnumname ");
                strSQL3.Append(" group by A.PartyGUID,A.Confnumname,A.ConfID, bridgeid, A.Endpointname,A.Endpointaddress,case a.ConnectionType when 1 then 'Dial-In' when 2 then 'Dial-Out' end ");

                //From Conference table
                strSQL3.Append(" Union ");

                strsub = new StringBuilder();

                strsub.Append(" select (cast(b.endpointid as varchar(100)) +'-'+ cast(A.ConfNumname as varchar(10))) as [PartyGUID], c.bridgeid, A.ConfNumname as ConfID, b.PartyName as [Room], b.ipisdnaddress as [Endpoint], ");
                strsub.Append(" case b.connectiontype when 1 then 'Dial-In' when 2 then 'Dial-Out' end as [Connection Type],"); //ZD 100288
                strsub.Append(" 'N/A' as [Connect Time],  'N/A' as [Disconnect Time],  'N/A' as [Disconnect Reason] ");
                strsub.Append(" from Conf_Conference_D a, Conf_Room_D b,  Conf_Bridge_D c  where a.isVMR > 0 and a.deleted = 0 and ");
                strsub.Append(" a.confid = b.ConfID and a.instanceid = b.instanceID and ");
                strsub.Append(" a.confid = c.ConfID and a.instanceid = c.instanceID ");
                if (strorgid != "") //FB 2944
                    strsub.Append(" and a." + strorgid);
                //FB 2944
                strsub.Append(" and  a.confnumname not in( select confnumname from Conf_VMRCDR_logs_D  ");

                if (mcu != "")
                {
                    strsub.Append(" where McuId in (" + mcu + ") ) ");
                    strsub.Append(" and b.BridgeID in (" + mcu + ") ");
                }
                else
                    strsub.Append(" ) ");


                if (startDate != "" && endDate != "")
                {
                    strsub.Append(" AND a.confdate >= dbo.changeTOGMTtime(" + timezone + ",'" + startDate + "') ");
                    strsub.Append(" AND DATEADD(minute, a.duration, a.confdate) <= dbo.changeTOGMTtime(" + timezone + ", '" + endDate + "') ");
                }
                strsub.Append(" union ");
                strsub.Append(" select (cast(b.userid as varchar(100)) +'-'+ cast(A.ConfNumname as varchar(10))) as [PartyGUID], c.bridgeid, A.ConfNumname as ConfID, b.PartyName as [Room], b.ipisdnaddress as [Endpoint], ");
                strsub.Append(" case b.connectiontype when 1 then 'Dial-In' when 2 then 'Dial-Out' end as [Connection Type],"); //ZD 100288
                strsub.Append(" 'N/A' as [Connect Time],  'N/A' as [Disconnect Time],  'N/A' as [Disconnect Reason] ");
                strsub.Append(" from Conf_Conference_D a, Conf_User_D b, Conf_Bridge_D c where a.isVMR > 0 and b.invitee = 1 and a.deleted = 0 and "); //FB 2944
                strsub.Append(" a.confid = b.ConfID and a.instanceid = b.instanceID and ");
                strsub.Append(" a.confid = c.ConfID and a.instanceid = c.instanceID ");
                if (strorgid != "") //FB 2944
                    strsub.Append(" and a." + strorgid);
                //FB 2944 Starts
                strsub.Append(" and  a.confnumname not in( select confnumname from Conf_VMRCDR_logs_D ");

                if (mcu != "")
                {
                    strsub.Append(" where McuId in (" + mcu + ") ) ");
                    strsub.Append(" and b.BridgeID in (" + mcu + ") ");
                }
                else
                    strsub.Append(" ) ");
                //FB 2944 End
                if (startDate != "" && endDate != "")
                {
                    strsub.Append(" AND a.confdate >= dbo.changeTOGMTtime(" + timezone + ",'" + startDate + "') ");
                    strsub.Append(" AND DATEADD(minute, a.duration, a.confdate) <= dbo.changeTOGMTtime(" + timezone + ", '" + endDate + "') ");
                    strsub.Append(" AND DATEADD(minute, a.duration, a.confdate) < dbo.changeTOGMTtime(" + sysSettings.TimeZone + ", GETDATE()) ");
                }
                strSQL3.Append(strsub + " Union All " + strsub.ToString().Replace("Conf_Conference_D", "Archive_Conf_Conference_D")
                    .Replace("Conf_Bridge_D", "Archive_Conf_Bridge_D").Replace("Conf_User_D","Archive_Conf_User_D"));

                strSQL3.Append(";");

                strSQL3.Append(" select (cast(A.PartyGUID as varchar(100)) +'-'+ cast(A.ConfNumname as varchar(10))) as PartyGUID, 0 as bridgeid, 0 as ConfID, A.EndpointName as [Room]"); //FB 2944
                strSQL3.Append(" , dbo.changeTime(" + timezone + ",A.EndpointDatetime) as [Connect Time],dbo.changeTime(" + timezone + ",A.EndpointDatetime) as [Disconnect Time],  Endpointconnectstatus, DisconnectReason as [Disconnect Reason] ");
                strSQL3.Append(" from Conf_VMRCDREndpoints_D A where A.confid in (select confid from Conf_VMRCDR_logs_D a where 1 = 1 ");
                if (strorgid != "") //FB 2944
                    strSQL3.Append(" and a." + strorgid);
                if (mcu != "")
                    strSQL3.Append(" and mcuid in (" + mcu + ")");

                if (startDate != "" && endDate != "")
                {
                    strSQL3.Append(" AND a.ScheduledStart >= dbo.changeTOGMTtime(" + timezone + ",'" + startDate + "')");
                    strSQL3.Append(" AND a.ScheduledEnd <= dbo.changeTOGMTtime(" + timezone + ", '" + endDate + "') ");
                }
                strSQL3.Append(" ) ");
                strSQL3.Append(" order by Confid, instanceid, Endpointname, [Connect Time], EndpointDateTime, PartyGUID "); //FB 2944

                strSQL2.Append(strSQL3.ToString());
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
            }
        }
        //ZD 101348 End

        //ZD 102468 - Start
        #region CustomOptionReports
        /// <summary>
        /// CustomOptionReports
        /// </summary>
        /// <param name="stmt"></param>
        /// <param name="loginId"></param>
        /// <param name="sDtFormat"></param>
        /// <param name="usrType"></param>
        /// <param name="orgID"></param>
        private void CustomOptionReports(ref StringBuilder strSQL, String userId, String sDtFormat, String inputType, String orgID, String startDate, String endDate)
        {
            try
            {
                //ZD 103840              
                strSQL.Append("SELECT  confNum as [Conference #], title as [Meeting Title], ConfStart as [Conference Start], TimezoneName, duration as [Duration (mins)], timezone as [Conference Time Zone], "); //ZD 104169
                strSQL.Append("host as [Host], requestor as [Requester], Status, [Meeting Type], [Custom Options], [Custom Option Value]");

                strSQL.Append(" FROM ( SELECT  c.confnumname AS confNum, c.externalname AS title, tz.TimeZone as TimezoneName, ");

                if (sDtFormat.Equals("dd/MM/yyyy"))
                    strSQL.Append(" dbo.GMTtouserPreferedtime(" + userId + ", c.confdate) AS ConfStart,");
                else if (sDtFormat.Equals("HHmmZ"))
                    strSQL.Append(" c.confdate AS ConfStart,");
                else
                    strSQL.Append(" dbo.GMTtouserPreferedtime(" + userId + ", c.confdate) AS ConfStart,");
 
                strSQL.Append("c.duration as duration, t.StandardName as timezone,");
                strSQL.Append("case when CHARINDEX('H',UserIDRefText) > 0 then ud.firstname + ' ' +  ud.lastname  else uh.firstname + ' ' +  uh.lastname  end as [Host],"); //ZD 103878
                strSQL.Append("case when CHARINDEX('R',UserIDRefText) > 0 then urd.firstname + ' ' +  urd.lastname  else ur.firstname + ' ' +  ur.lastname  end as [Requestor],");
                //uh.firstname + ' '+ uh.lastname as host, ");
                //strSQL.Append("ur.firstname +' '+ ur.lastname as requestor, ");

                strSQL.Append("  c.confid, case when dateadd(minute, c.duration + c.teardowntimeinmin, c.confdate) <= dbo.changeToGMTTime(" + sysSettings.TimeZone + ", DATEADD(mi, 6, getdate()))  and c.Status in (0,1,7) then 'Completed'  when  c.Status = 9 then 'Deleted' ");                
                strSQL.Append(" when c.Status = 3 then 'Terminated' when c.Status = 1 then 'Pending' when c.Status = 6 then 'On MCU' ");
                                
                strSQL.Append(" when (c.confdate >= dbo.changeToGMTTime(" + sysSettings.TimeZone + ", DATEADD(mi, -c.duration, getdate()))");
                strSQL.Append(" and c.confdate <= dbo.changeToGMTTime(" + sysSettings.TimeZone + ", DATEADD(mi, 6, getdate()))");
                strSQL.Append(" and c.Status in (0,5) and c.permanent = 0 AND c.isHDBusy = 0) then 'Ongoing' "); //ALLDEV-807
                strSQL.Append(" when  c.Status = 0 then 'Scheduled' End as Status, ");

                
                strSQL.Append(" case conftype when 6 then 'Audio' when 2 then 'Video' when 7 then 'Room' when 8 then 'Hotdesking' when 4 then 'Pt-to-Pt' End as [Meeting Type], ");
                strSQL.Append(" d.DisplayTitle as [Custom Options],  b.SelectedValue as [Custom Option Value] ");
                strSQL.Append("FROM conf_conference_d c ");
                //strSQL.Append("Left Outer Join usr_list_d uh on c.owner = uh.userid ");
                strSQL.Append(" left outer join (select LastName,firstname,  UserID from usr_list_d) uh on c.owner = uh.userid ");
                strSQL.Append(" left outer join (select LastName,firstname,  id from Usr_Deletelist_D) ud on c.owner = ud.id ");

                //strSQL.Append("Left Outer Join usr_list_d ur on c.userid=ur.userid ");
                strSQL.Append(" left outer join (select LastName,firstname,  UserID from usr_list_d) ur on c.userid = ur.userid ");
                strSQL.Append(" left outer join (select LastName,firstname,  id from Usr_Deletelist_D) urd on c.userid = urd.id ");

                strSQL.Append("Left Outer Join Gen_TimeZone_S t on t.timezoneid=c.timezone ");
                strSQL.Append("left outer join (select TimeZone, TimeZoneID from Gen_TimeZone_S ) tz on tz.TimeZoneID in (select timezone from Usr_List_D where userid = " + userId + ") "); //ZD 103840
                strSQL.Append(" ,Conf_CustomAttr_D b, dept_CustomAttr_D d ");
                strSQL.Append("WHERE c.orgId=" + orgID + "  AND  c.deleted = 0 and c.confid = b.ConfId and c.instanceid = b.instanceid and b.CustomAttributeId = d.CustomAttributeId and b.CustomAttributeId = " + inputType);
        
                if (dtFormat == "HHmmZ") 
                    strSQL.Append(" AND c.confdate BETWEEN '" + startDate + "' AND '" + endDate + "' ");
                else
                    strSQL.Append(" AND c.confdate BETWEEN dbo.changeTOGMTtime(" + timezone + ", '" + startDate + "') AND dbo.changeTOGMTtime(" + timezone + ",  '" + endDate + "')");

                strSQL.Append(") as y order by ConfStart");
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);

            }
        }

        #endregion
        //ZD 102468 - End

        // ZD 102835 Start


        private void GetWorkOrderConferenceReport(ref StringBuilder strSQL, String confSelect,String workOrders, String hostSelect,
            String scheduledFilter, String confTypeFilter, String confBehaviorFilter,
            String woTimeSelect, String woDurationSelect, String departmentid,
            String startDate, String endDate, String tmZone, String userCommon, String tierInfo, String Personnel, string orgid)
        {
            try
            {
                strSQL.Append("select confnumname [ConfID]");

                if (confSelect.IndexOf('1') > -1)
                    strSQL.Append(", confnumname [Conf ID]");

                if (confSelect.IndexOf('2') > -1)
                    strSQL.Append(", externalname [Conference Title] ");

                if (confSelect.IndexOf('3') > -1)
                    strSQL.Append(" ,(select orgname from Org_List_D where orgId = a.orgId) as [Organization] ");

                if (scheduledFilter != "")
                {
                    strSQL.Append(", case a.Status when 7 then 'Completed' when 0 then 'Scheduled' when 9 then 'Deleted' when 3 then 'Terminated' ");
                    strSQL.Append("when 1 then 'Pending' when 6 then 'On MCU' End as [Status] ");
                }

                if (confBehaviorFilter != "")
                    strSQL.Append(", case recuring when 0 then 'Single' when 1 then 'Recurring' End as [Occurrence] ");

                if (confTypeFilter != "")
                {
                    strSQL.Append(", case conftype when 6 then 'Audio' when 2 then 'Video' when 7 then 'Room' when 8 then 'Hotdesking' ");
                    strSQL.Append("when 4 then 'Pt-to-Pt' End as [Conference Type] ");
                }


                if (departmentid.IndexOf('d') > -1) // Department
                {
                    strSQL.Append(" ,isnull(Stuff ((SELECT distinct N', ' + d1.departmentname from Loc_Department_D c1, dept_list_d d1 where ");
                    strSQL.Append(" r.RoomID = c1.RoomID  and d1.Departmentid = c1.Departmentid ");
                    strSQL.Append(" FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N''),'-') as [Department Name] ");
                }

                //strSQL.Append(" , isnull((select departmentName from Dept_List_D ");
                //strSQL.Append(" where departmentId in (select departmentId from Loc_Department_D where RoomId in (r.RoomID))),'-') as [Dept. Name] ");

                if (userCommon != "")
                {
                    //strSQL.Append(",r.Name ");
                    strSQL.Append("," + userCommon);
                }

                if (tierInfo != "")
                {
                    strSQL.Append("," + tierInfo);
                    //strSQL.Append(", (select [Name] from Loc_Tier3_D l3 where l3.id = r.l3LocationID) as [Top Tier] ");
                    //strSQL.Append(", (select [Name] from Loc_Tier2_D l2 where l2.id = r.l2LocationID) as [Middle Tier] ");
                }


                if (departmentid.IndexOf('h') > -1) // Host (Room Host)
                {
                    strSQL.Append(", (select firstname + ' ' + lastname from usr_list_d u1 where u1.userid = r.AdminID) as [Room Host] ");
                }

                if (hostSelect != "")
                {
                    //ZD 103878
                    if (hostSelect != "")
                    {
                        hostSelect = hostSelect.Replace("h.FirstName", "case when CHARINDEX('H',a.UserIDRefText) > 0 then hd.FirstName else h.FirstName end ");
                        hostSelect = hostSelect.Replace("h.LastName", "case when CHARINDEX('H',a.UserIDRefText) > 0 then hd.LastName else h.LastName end ");
                        hostSelect = hostSelect.Replace("h.Email", "case when CHARINDEX('H',a.UserIDRefText) > 0 then hd.Email else h.Email end ");
                        hostSelect = hostSelect.Replace("hr.roleName", "case when CHARINDEX('H',a.UserIDRefText) > 0 then hdr.roleName else hr.roleName end ");
                    }
                    strSQL.Append("," + hostSelect);
                }
                //strSQL.Append(",h.LastName +',' + h.FirstName as [Host Name] ");
                //strSQL.Append(", h.Email as [Host Email] ");
                //strSQL.Append(", hr.roleName as [Role] ");

                //if(confSelect != "")
                //    strSQL.Append(confSelect);

                if (woTimeSelect != "")
                {
                    if (tFormat != "HHmmZ")
                        woTimeSelect = woTimeSelect.Replace("ConfTime [Start Time]", "dbo.changeTime(" + tmZone + ", ConfTime) [Start Time]");
                    strSQL.Append(woTimeSelect);
                }

                if (woDurationSelect != "")
                    strSQL.Append(woDurationSelect);

                //strSQL.Append(",a.Duration as [Duration (Mins)] ");

                //strSQL.Append(",ConfDate as [Date of Conf.] ");

                //strSQL.Append(", ConfTime [Start Time] ");

                // WORK ORDER DETAILS

                if (workOrders != "")
                    strSQL.Append(",case wo.Type when 1 then 'AV' when 2 then 'Catering' when 3 then 'Facilities' End as [WO Type] ");

                if (workOrders != "")
                {
                    strSQL.Append(" , wo.Name[WO Name] , case wo.Type when 1 then ila.Name when 2 then ilc.name when 3 then ilk.Name end as [Inventory Item] ");
                    strSQL.Append(" , wi.quantity[Quantity],wi.deliveryCost[Delivery Cost], wi.serviceCharge[Service Charge], wo.Comment[Comment] ");
                }

                if (Personnel.IndexOf('1') > -1 || Personnel.IndexOf('3') > -1)
                {
                    //strSQL.Append(",isnull(Stuff ((SELECT distinct N', ' + ui.Firstname + ' ' + ui.Lastname from Inv_Category_D c, Inv_WorkOrder_D I, usr_list_D ui ");
                    //strSQL.Append("where I.deleted = 0 and I.adminid = ui.userid and c.AdminID = i.AdminId and I.Confid = a.confid and I.instanceid = a.instanceid and c.Type = i.Type ");
                    //strSQL.Append("FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N''),'-') as [WO Admin-in-Charge] ");
                    strSQL.Append(" ,(select firstname + ' ' + LastName from Usr_List_D uu, Inv_Category_D cc ");
                    strSQL.Append(" where uu.UserID = cc.AdminID and cc.ID = wo.CatID and wo.ConfID = a.confid) as [WO Admin-in-Charge] ");
                }

                if (Personnel.IndexOf('2') > -1 || Personnel.IndexOf('3') > -1)
                    strSQL.Append(" , (select firstname + ' ' + LastName from Usr_List_D uu where uu.UserID = wo.AdminId) as [WO Person-in-Charge] ");


                //strSQL.Append(" , isnull((select departmentName from Dept_List_D ");
                //strSQL.Append(" where departmentId in (select departmentId from Loc_Department_D where RoomId in (r.RoomID))),'-') as [Dept. Name] ");

                //strSQL.Append(",h.LastName +',' + h.FirstName as [Host Name] ");
                //strSQL.Append(", h.Email as [Host Email] ");
                //strSQL.Append(", hr.roleName as [Role] ");

                //if(confSelect != "")
                //    strSQL.Append(confSelect);

                //strSQL.Append(",a.Duration as [Duration (Mins)] ");

                //strSQL.Append(",ConfDate as [Date of Conf.] ");

                //strSQL.Append(", ConfTime [Start Time] ");

                // WORK ORDER DETAILS


                strSQL.Append("from conf_conference_d a ");
                //ZD 103878
                //, usr_list_d h left outer join Usr_Roles_D hr on h.roleid = hr.roleid ");
                strSQL.Append(" left outer join (select LastName,firstname, UserID,roleid,Email from  usr_list_d) h left outer join Usr_Roles_D hr on h.roleid = hr.roleid on h.UserID = a.owner ");
                strSQL.Append(" left outer join (select LastName,firstname, id,roleid,Email from  Usr_Deletelist_D) hd left outer join Usr_Roles_D hdr on hd.roleid = hdr.roleid on hd.id = a.owner ");

                strSQL.Append(", Inv_WorkOrder_D wo, Loc_Room_D r ");

                // WORK ORDER DETAILS
                if (workOrders != "")
                {
                    strSQL.Append(" , Inv_WorkItem_D wi ");
                    strSQL.Append(" left outer join Inv_ItemList_AV_D ILA on wi.ItemID = ila.ID ");
                    strSQL.Append(" left outer join Inv_Menu_D ILC on wi.ItemID =ilc.id ");
                    strSQL.Append(" left outer join Inv_ItemList_HK_D ILK on wi.ItemID =ILK.ID ");
                }

                strSQL.Append(" where a.confid not in(11) and a.owner = h.UserID ");
                strSQL.Append(" and a.confid = wo.ConfID and a.instanceid = wo.InstanceID ");
                if(workOrders != "")
                    strSQL.Append(" and wo.Type in (" + workOrders + ") ");
                strSQL.Append(" and wo.RoomID = r.RoomID ");

                // For Conference Type FIlter
                if (confTypeFilter != "")
	                strSQL.Append(" and " + confTypeFilter);
                // For Status
                if (scheduledFilter != "")
                {
                    scheduledFilter = scheduledFilter.ToLower().Replace("status", "a.status");
                    strSQL.Append(" and " + scheduledFilter);
                }
                // For Conference single/recurring FIlter
                if (confBehaviorFilter != "")
	                strSQL.Append(" and " + confBehaviorFilter);

                if (startDate != "" && endDate != "")
                {
                    strSQL.Append(" AND a.confdate BETWEEN dbo.changeTOGMTtime(" + tmZone + ",'" + startDate + "') ");
                    strSQL.Append(" AND dbo.changeTOGMTtime(" + tmZone + ",'" + endDate + "') ");
                }

                // WORK ORDER DETAILS
                if (workOrders != "")
                    strSQL.Append(" and wo.ID = wi.WorkOrderID ");

                if (orgid != "11")
                    strSQL.Append(" and a.orgid = " + orgid);
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
            }
        }
        // ZD 102835 End

        //ZD 102909 - Start
        #region GetEntityCodes
        public bool GetEntityCodes(ref vrmDataObject obj)
        {
            bool bRet = true;
            StringBuilder strSQL = new StringBuilder();
            DataSet ds = new DataSet();
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                organizationID = defaultOrgId;
                node = xd.SelectSingleNode("//GetEntityCodes/organizationID");                
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out organizationID);

                languageid = 1; //Default language id
                node = xd.SelectSingleNode("//GetEntityCodes/languageid");                
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out languageid);

                strSQL.Append("select CAO.OptionID, CAO.Caption from Dept_CustomAttr_D CA, Dept_CustomAttr_Option_D CAO ");
                strSQL.Append(" where CA.CustomAttributeId = CAO.CustomAttributeId and CA.FieldController = 'Entity Code' ");
                strSQL.Append(" and CA.orgid = " + organizationID + " and CAO.Languageid =" + languageid);

                if (strSQL.ToString() != "")
                    ds = m_rptLayer.ExecuteDataSet(strSQL.ToString());

                m_rptLayer.CloseConnection();

                if (ds != null)
                {
                    MemoryStream ms = new MemoryStream();
                    ds.WriteXml(ms);
                    ms.Position = 0;
                    StreamReader sr = new StreamReader(ms, System.Text.Encoding.UTF8);
                    String strXml = sr.ReadToEnd();
                    obj.outXml = strXml;

                    sr.Close();
                    ms.Close();
                }

                if (ds.Tables[0].Rows.Count <= 0)
                {
                    String oXML = "";
                    oXML = "<NewDataSet><Table>"; //ZD 103181 start
                    for (int c = 0; c < ds.Tables[0].Columns.Count; c++)
                    {
                        oXML += "<" + ds.Tables[0].Columns[c].ToString().Replace(" ", "_x0020_").Replace("#", "_x0023_").Replace("/", "_x002F_")
                            .Replace("(", "_x0028_").Replace(")", "_x0029_") + " />"; //_x0020_  --> Space , _x0023_  --> # //FB 2654
                    }
                    oXML += "</Table></NewDataSet>"; //ZD 103181 End

                    obj.outXml = oXML;
                }
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
            return bRet;
        }
        #endregion
        //ZD 102909 - End

        #region GetUserAssignedReports
        public void GetUserAssignedReports(ref StringBuilder strSQL, string userid)
        {
            try
            {
                // 8 Tables
                // Conference
                strSQL.Append(" Select 0 SNO, 'Conference Details' [Conference Name], '' [Conference ID], '' [Time Zone],'' [Conference Date],'Yes' as Host,'' as Requester, '' as Participant");
                strSQL.Append(" union");
                strSQL.Append(" select  ROW_NUMBER() Over (Order by A.[Conference ID]) As SNO");
                strSQL.Append(" , isnull(A.[Conference Name],isnull(B.[Conference Name],C.[Conference Name])) [Conference Name] ");
                strSQL.Append(" , isnull(A.[Conference ID], isnull(B.[Conference ID],C.[Conference ID])) [Conference ID] ");
                strSQL.Append(" , isnull(A.[Time Zone], isnull(B.[Time Zone],C.[Time Zone])) [Time Zone]");
                strSQL.Append(" , isnull(A.[Conference Date],isnull(B.[Conference Date],C.[Conference Date])) [Conference Date]");
                strSQL.Append(" , isnull(A.Host,'') as Host, isnull(B.Requester,'') Requester, isnull(C.Participant,'') Participant from (");
                strSQL.Append(" (Select vc.externalname [Conference Name], vc.confnumname [Conference ID], t.timezone [Time Zone],dbo.changeTime(vc.timezone,vc.confdate) [Conference Date],'Yes' as Host,'' as Requester, '' as Participant");
                strSQL.Append(" from Conf_Conference_D vc , Gen_TimeZone_S t");
                strSQL.Append(" WHERE vc.timezone = t.TimeZoneID and vc.deleted=0 and (vc.owner= " + userid + " or vc.userid = " + userid + ") and vc.status in (0,1,5,6) and vc.confdate >= GETDATE()) as AA");
                strSQL.Append(" full outer join ");
                strSQL.Append(" (Select vc.externalname [Conference Name], vc.confnumname [Conference ID], t.timezone [Time Zone],dbo.changeTime(vc.timezone,vc.confdate) [Conference Date],'Yes' as Host,'' as Requester, '' as Participant");
                strSQL.Append(" from Conf_Conference_D vc , Gen_TimeZone_S t");
                strSQL.Append(" WHERE vc.timezone = t.TimeZoneID and vc.deleted=0 and vc.owner= " + userid + " and vc.status in (0,1,5,6) and vc.confdate >= GETDATE()) as A");
                strSQL.Append(" on AA.[Conference ID] = A.[Conference ID] full join ");
                strSQL.Append(" (Select vc.externalname [Conference Name], vc.confnumname [Conference ID], t.timezone [Time Zone],dbo.changeTime(vc.timezone,vc.confdate)  [Conference Date],'' as Host,'Yes' as Requester, '' as Participant");
                strSQL.Append(" from Conf_Conference_D vc , Gen_TimeZone_S t");
                strSQL.Append(" WHERE vc.timezone = t.TimeZoneID and vc.deleted=0 and vc.userid= " + userid + " and vc.status in (0,1,5,6) and vc.confdate >= GETDATE()) B");
                strSQL.Append(" on AA.[Conference ID] = b.[Conference ID]");
                strSQL.Append(" full join");
                strSQL.Append(" (SELECT DISTINCT vc.externalname [Conference Name], vc.confnumname [Conference ID], t.timezone [Time Zone],dbo.changeTime(vc.timezone,vc.confdate)  [Conference Date],'' as Host,'' as Requester, 'Yes' as Participant");
                strSQL.Append(" FROM Conf_Conference_D vc, conf_user_D cu, Gen_TimeZone_S t");
                strSQL.Append(" WHERE vc.confid=cu.confid and vc.timezone = t.TimeZoneID and vc.deleted=0 and vc.status in (0,1,5,6) and vc.confdate >= GETDATE() and cu.userid = " + userid + " ) C");
                strSQL.Append(" on AA.[Conference ID] = C.[Conference ID])");
                strSQL.Append(" order by SNO");

                // System Approver
                strSQL.Append(";");
                strSQL.Append("select * from (");
                strSQL.Append(" select 0 SNO,'System Approver' as Name,'Yes' as Approver union all");
                strSQL.Append(" SELECT 1, u.LastName + ' ' + u.FirstName as [Name],  'Yes' as Approver  FROM  Sys_Approver_D A , Usr_List_D u ");
                strSQL.Append(" WHERE a.approverid = u.UserID and approverid = " + userid);
                strSQL.Append(" ) as x ");

                // Room
                strSQL.Append(";");
                strSQL.Append("select 0 SNO, 'Room Details' [Room Name],'' as Administrator,'' as Approver");
                strSQL.Append(" union");
                strSQL.Append(" select ROW_NUMBER() Over (Order by A.[Room Name]) As SNO , isnull(A.[Room Name], B.[Room Name]) as [Room Name], isnull(A.Administrator,'') as Administrator, isnull(B.Approver,'') from (");
                strSQL.Append(" (SELECT Name as [Room Name], 'Yes' as Administrator,'' as Approver FROM Loc_Room_D R , Loc_Assistant_D A WHERE R.RoomID = A.RoomId and Disabled = 0 and A.AssistantId = " + userid + ") as A");
                strSQL.Append(" full join");
                strSQL.Append(" (Select loc.Name [Room Name],'' as Administrator, 'Yes' as Approver from Loc_Approver_D la, Loc_Room_D loc");
                strSQL.Append(" where loc.Disabled =0  and la.roomid = loc.RoomID and la.approverid = " + userid + ") B ");
                strSQL.Append(" on A.[Room Name] = B.[Room Name] ");
                strSQL.Append(" )   order by SNO ");

                // MCU
                strSQL.Append(";");
                strSQL.Append(" select 0 SNO, 'MCU Details' [Bridge Name],'Yes' as Administrator,'' as Approver ");
                strSQL.Append(" union");
                strSQL.Append(" select ROW_NUMBER() Over (Order by A.[Bridge Name]) As SNO, isnull(A.[Bridge Name],B.[Bridge Name]) [Bridge Name], isnull(B.Administrator,'') as Administrator, isnull(A.Approver,'') as Approver from (");
                strSQL.Append(" (Select mc.BridgeName as [Bridge Name] ,'' as Administrator,'Yes' as Approver from MCU_Approver_D ma, Mcu_List_D mc ");
                strSQL.Append(" where mc.deleted =0  and ma.mcuid = mc.BridgeID and ma.approverid =" + userid + " ) as A");
                strSQL.Append(" full join  ");
                strSQL.Append(" (SELECT BridgeName [Bridge Name],'Yes' as Administrator,'' as Approver FROM MCU_List_D M WHERE Deleted = 0 and Admin = " + userid + " ) as B");
                strSQL.Append(" on A.[Bridge Name] = b.[Bridge Name]");
                strSQL.Append(" )  order by SNO");

                //Group
                strSQL.Append(";");
                strSQL.Append(" select 0 SNO,'Group Details' [Group Name], '' as [Public], 'Yes' as Administrator,'' as Participant union ");
                strSQL.Append(" select ROW_NUMBER() Over (Order by A.[Group Name]) As SNO, isnull(A.[Group Name], B.[Group Name]) [Group Name],isnull(A.[Public],B.[Public]) [Public], isnull(B.Administrator,'') Administrator, isnull(A.Participant,'') Participant from (");
                strSQL.Append(" (SELECT D.Name as [Group Name], case when D.Private = 0 then 'Yes' else 'No' end as [Public], 'Yes' as [Participant], '' as Administrator FROM  Grp_Participant_D A, Grp_Detail_D D");
                strSQL.Append(" WHERE A.GroupID = D.GroupId and UserID = " + userid + " ) as A");
                strSQL.Append(" full join");
                strSQL.Append(" (SELECT Name  as [Group Name], case when A.Private = 0 then 'Yes' else 'No' end as [Public], '' as [Participant], 'Yes' as Administrator FROM  Grp_Detail_D A WHERE Owner = " + userid + ") as B");
                strSQL.Append(" on A.[Group Name] = b.[Group Name]");
                strSQL.Append(" ) order by SNO");

                //Template
                strSQL.Append(";");

                strSQL.Append(" select 0 SNO, 'Template Details' as [Template Name], '' as [Public], 'Yes' as Administrator ,'' as Participant union ");
                strSQL.Append(" select ROW_NUMBER() Over (Order by A.[Template Name]) As SNO, isnull(A.[Template Name],B.[Template Name]) [Template Name] ,isnull(A.[Public],B.[Public]) [Public], isnull(A.Administrator,'') Administrator, isnull(B.Participant,'') Participant from (");
                strSQL.Append(" (SELECT A.TmpName as [Template Name],'' as Participant, case when A.TmpPublic = 1 then 'Yes' else 'No' end as [Public], 'Yes' as Administrator FROM  Tmp_List_D A ");
                strSQL.Append(" WHERE TmpOwner = " + userid + " ) as A");
                strSQL.Append(" full join");
                strSQL.Append(" (SELECT t.TmpName as [Template Name],'Yes' as Participant, case when T.TmpPublic = 1 then 'Yes' else 'No' end as [Public], '' as Administrator FROM  Tmp_Participant_D A , Tmp_List_D t ");
                strSQL.Append(" WHERE A.TmpID = t.TmpID and UserID = " + userid + ") as B");
                strSQL.Append(" on A.[Template Name] = b.[Template Name]");
                strSQL.Append(" ) order by SNO");

                //WO
                strSQL.Append(";");
                strSQL.Append("Select 0 as SNO,'Work Order Details' [Conference Name], '' [Conference ID], '' [Work Order Name], '' [Conference Date],'Yes' as Administrator ");
                strSQL.Append(" Union All");
                strSQL.Append(" SELECT  ROW_NUMBER() Over (Order by c.externalname) As SNO, ISNULL(c.externalname,'') as [Conference Name],ISNULL(c.confnumname,'') as [Conference ID], w.Name [Work Order Name], isnull(ConferenceDate,'') as [Conference Date],'Yes' as Administrator ");
                strSQL.Append(" FROM Inv_WorkOrder_D W left outer join (select confnumname, externalname, confid, dbo.changeTime(c.timezone,c.confdate) as ConferenceDate ");
                strSQL.Append(" from Conf_Conference_D c , Gen_TimeZone_S t where c.timezone = t.TimeZoneID) as c on w.ConfID = c.confid ");
                strSQL.Append(" WHERE AdminID = " + userid);
                strSQL.Append(" order by 1");

                strSQL.Append(";");
                strSQL.Append("Select 0 as SNO, 'Inventory Sets' Name,'A' [Inventory Type] ");
                strSQL.Append(" Union All");
                strSQL.Append(" SELECT ROW_NUMBER() Over (Order by Name) As SNO,Name, case type when 1 then 'Audiovisual' when 2 then 'Catering' else 'Facility' end as [Inventory Type] ");
                strSQL.Append(" FROM Inv_Category_D C ");
                strSQL.Append(" WHERE AdminID = " + userid);
                strSQL.Append(" order by 1");
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
            }
        }
         #endregion
    }
}
