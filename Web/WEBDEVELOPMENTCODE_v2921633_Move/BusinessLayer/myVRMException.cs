//ZD 100147 Start
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End ZD 100886
using System;
using System.Text;
using System.Collections;
using NHibernate;
using NHibernate.Criterion;

using myVRM.DataLayer;
 
namespace myVRM.BusinessLayer
{
	/// <summary>
	/// Summary description for application level exceptions. 
	/// </summary>
	public class myVRMException: ApplicationException
	{
		public myVRMException(){}
		public myVRMException(int errID)	
			: base(GetMessage(errID)){}
		public myVRMException(string message)
			: base(message){initMessage(message);}
		public myVRMException(string message,Exception innerEx)
			: base(message,innerEx){initMessage(message);}
		
		private static string m_message;
		private static string m_description;
		private static int    m_errorID;    
		private static string m_errorLevel;
		private static string GetMessage(int ID)
		{
			vrmError er = new vrmError();
			 
			try
			{
				vrmErrorDAO.GetError(ref er, ID);
				//FB 2659 Starts
                if (ID <= 0 && sysSettings.EnableCloudInstallation > 0)
                {
                    er.ID = -1;
                    er.Message = "Success";
                    er.Description = "Operation Successful";
                    er.Level = "S";
 
                }
				//FB 2659 End
			}
			catch (Exception e)
			{
				  throw e;
			}
			m_errorID     = er.ID;
			m_message     = er.Message;
			m_description = er.Description;
			m_errorLevel  = er.Level;

            return m_message; //FB 2027 SetConference
		}

		public string FetchErrorMsg()
		{
			string errorMsg = "<error>";
			errorMsg += "<errorCode>" + m_errorID.ToString() + "</errorCode>";
            errorMsg += "<message>" + m_message + "</message>"; //FB 1881
            errorMsg += "<Description>" + m_description + "</Description>"; //FB 1881
			errorMsg += "<level>" + m_errorLevel + "</level>";
			errorMsg += "</error>";

			return (errorMsg);
		}
        // ZD 103954 Start
        public string FetchErrorMsg(int passwordrule)
        {
            string errorMsg = "<error>";
            errorMsg += "<errorCode>" + m_errorID.ToString() + "</errorCode>";
            errorMsg += "<message>" + m_message + "</message>"; 
            errorMsg += "<Description>" + m_description + "</Description>"; 
            errorMsg += "<level>" + m_errorLevel + "</level>";
            errorMsg += "<passwordrule>" + passwordrule + "</passwordrule>"; 
            errorMsg += "</error>";

            return (errorMsg);
        }
        // ZD 103954 End
        //FB 2027 - SetConference start
        public string FetchErrorMsg(string addtnlMessage)
        {
            string errorMsg = "<error>";
            errorMsg += "<errorCode>" + m_errorID.ToString() + "</errorCode>";
            errorMsg += "<message>" + m_message + " \n" + addtnlMessage + "</message>";
            errorMsg += "<Description>" + m_description + "</Description>";
            errorMsg += "<level>" + m_errorLevel + "</level>";
            errorMsg += "</error>";

            return (errorMsg);
        }
        //FB 2027 - SetConference end
		private void initMessage(string message)
		{
			m_message    = message; 
			m_errorID    = 200 ;
			m_errorLevel = "S";
		}
		public static string toXml(string message)
		{
			string errorMsg = "<error>";
			errorMsg += "<errorCode>200</errorCode>";
			errorMsg += "<message>" + message + "</message>";
			errorMsg += "<level>S</level>";
			errorMsg += "</error>";

			return (errorMsg);

		}
        public static string toXml(string message, int error)
        {
            string errorMsg = "<error>";
            errorMsg += "<errorCode>" + error.ToString() + "</errorCode>";
            errorMsg += "<message>" + message + "</message>";
            errorMsg += "<level>S</level>";
            errorMsg += "</error>";

            return (errorMsg);

        }
         
	}
}
