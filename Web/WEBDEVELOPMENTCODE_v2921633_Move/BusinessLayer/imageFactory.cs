﻿//ZD 100147 Start
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End ZD 100886
using System;
using System.Diagnostics;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.ComponentModel;
using System.Xml;
using System.Threading;
using System.Text.RegularExpressions;
using System.IO;

using NHibernate;
using NHibernate.Criterion;

using log4net;

using myVRM.DataLayer;

namespace myVRM.BusinessLayer
{
    public class imageFactory
    {
        #region Private Members

        private myVRMException myVRMEx;
        private static log4net.ILog m_log;
        private string m_configPath;
        private const int defaultOrgId = 11;
        internal int organizationID = 0;

        private ISecBadgeDao m_ISecBadgeDao;
        private secBadgeDao m_SecBadgeDao;

        #endregion

        #region Constructor
        public imageFactory(ref vrmDataObject obj)
        {
            try
            {
                m_log = obj.log;
                m_configPath = obj.ConfigPath;
                m_SecBadgeDao = new secBadgeDao(m_configPath, m_log);

                m_ISecBadgeDao = m_SecBadgeDao.GetSecImageDao();

            }
            catch (Exception e)
            {
                m_log.Error("imageFactory", e);
                throw e;
            }
        }
        #endregion

        #region GetAllSecImages
        /// <summary>
        /// GetAllSecImages
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        // InXML
        //<GetAllSecImages>
        //<userid></userid>
        //<organizationID></organizationID>
        //</GetAllSecImages>
        public bool GetAllSecImages(ref vrmDataObject obj)
        {
            organizationID = 0;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;
                obj.outXml = "";

                node = xd.SelectSingleNode("//GetAllSecImages/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();

                Int32.TryParse(orgid, out organizationID);

                if (organizationID < defaultOrgId)
                {
                    myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("OrgId", organizationID));
                m_ISecBadgeDao.addOrderBy(Order.Asc("BadgeId"));
                List<vrmSecurityImage> tmpImages = m_ISecBadgeDao.GetByCriteria(criterionList);

                byte[] tempImg = null;
                string tempImgStr = "";

                obj.outXml = "<GetAllSecImages>";
                obj.outXml += "<organizationID>" + orgid + "</organizationID>";
                if (tmpImages != null)
                {
                    foreach (vrmSecurityImage imgObj in tmpImages)
                    {
                        obj.outXml += "<badge>";
                        tempImg = imgObj.BadgeImage;
                        tempImgStr = ConvertByteArrToBase64(tempImg);
                        obj.outXml += "<badgeimage>" + tempImgStr + "</badgeimage>";
                        obj.outXml += "<badgeid>" + imgObj.BadgeId + "</badgeid>";
                        obj.outXml += "<badgename>" + imgObj.ImgName + "</badgename>";
                        obj.outXml += "<textaxis>" + imgObj.TextAxis + "</textaxis>";
                        obj.outXml += "<barcodeaxis>" + imgObj.BarCodeAxis + "</barcodeaxis>";
                        obj.outXml += "<photoaxis>" + imgObj.PhotoAxis + "</photoaxis>";
                        obj.outXml += "</badge>";
                    }
                }
                obj.outXml += "</GetAllSecImages>";
                return true;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = ""; 
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("GetAllSecImages", e);
                obj.outXml = ""; 
                return false;
            }
        }
        #endregion

        #region GetSecImages
        /// <summary>
        /// GetSecImages
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        // InXML
        //<GetSecImages>
        //<userid></userid>
        //<organizationID></organizationID>
        //</GetSecImages>
        public bool GetSecImages(ref vrmDataObject obj)
        {
            organizationID = 0;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;
                obj.outXml = "";

                node = xd.SelectSingleNode("//GetSecImages/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();

                Int32.TryParse(orgid, out organizationID);

                if (organizationID < defaultOrgId)
                {
                    myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("OrgId", organizationID));
                m_ISecBadgeDao.addOrderBy(Order.Asc("BadgeId"));
                List<vrmSecurityImage> tmpImages = m_ISecBadgeDao.GetByCriteria(criterionList);

                obj.outXml = "<GetSecImages>";
                if (tmpImages != null)
                {
                    foreach (vrmSecurityImage imgObj in tmpImages)
                    {
                        obj.outXml += "<badge>";
                        obj.outXml += "<badgeid>" + imgObj.BadgeId + "</badgeid>";
                        obj.outXml += "<badgename>" + imgObj.ImgName.Trim() + "</badgename>";
                        obj.outXml += "</badge>";
                    }
                }
                obj.outXml += "</GetSecImages>";
                return true;
            }
            catch (myVRMException e)
            {
                m_log.Error("GetSecImages", e);
                obj.outXml = "";
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("GetSecImages", e);
                obj.outXml = "";
                return false;
            }
        }
        #endregion

        #region GetSecurityImage
        /// <summary>
        /// GetSecurityImage
        /// </summary>
        /// <param name="imageid"></param>
        /// <returns></returns>
        public bool GetSecurityImage(ref vrmDataObject obj)
        {
            organizationID = 0;
            int secbadgeid = 0;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;
                obj.outXml = "";

                node = xd.SelectSingleNode("//GetSecurityImage/badgeid");
                string badgeid = "";
                if (node != null)
                    badgeid = node.InnerXml.Trim();

                Int32.TryParse(badgeid, out secbadgeid);

                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("BadgeId", secbadgeid));
                List<vrmSecurityImage> tmpImages = m_ISecBadgeDao.GetByCriteria(criterionList);

                byte[] tempImg = null;
                string tempImgStr = "";
                obj.outXml = "<GetSecurityImage>";
                if (tmpImages != null)
                {
                    if (tmpImages.Count > 0)
                    {
                        obj.outXml += "<badge>";
                        tempImg = tmpImages[0].BadgeImage;
                        tempImgStr = ConvertByteArrToBase64(tempImg);
                        obj.outXml += "<badgeimage>" + tempImgStr + "</badgeimage>";
                        obj.outXml += "<badgename>" + tmpImages[0].ImgName + "</badgename>";
                        obj.outXml += "<textaxis>" + tmpImages[0].TextAxis + "</textaxis>";
                        obj.outXml += "<barcodeaxis>" + tmpImages[0].BarCodeAxis + "</barcodeaxis>";
                        obj.outXml += "<photoaxis>" + tmpImages[0].PhotoAxis + "</photoaxis>";
                        obj.outXml += "</badge>";
                    }

                    tempImg = null;
                    tempImgStr = "";
                }
                obj.outXml += "</GetSecurityImage>";
                return true;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = "";
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("GetSecurityImage", e);
                obj.outXml = "";
                return false;
            }
        }
        #endregion

        #region GetSecImage

        internal vrmSecurityImage GetSecImage(int imageid)
        {
            vrmSecurityImage imObj = new vrmSecurityImage();
            string imgDt = "";
            try
            {
                imObj = m_ISecBadgeDao.GetByBadgeId(imageid);
                if (imObj != null)
                    imgDt = Convert.ToBase64String(imObj.BadgeImage);

                return imObj;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException in GetSecImage", e);
                return imObj;
            }
        }

        #endregion

        #region ConvertByteArrToBase64
        /// <summary>
        /// Converting the bytearray into base64 string.
        /// </summary>
        /// <param name="imageData"></param>
        /// <returns></returns>
        internal String ConvertByteArrToBase64(Byte[] imageData)
        {
            string imageString = "";
            try
            {
                if (imageData == null)
                    return "";

                if (imageData.Length <= 0)
                    return "";

                imageString = Convert.ToBase64String(imageData);
            }
            catch (Exception e)
            {
                m_log.Error("ConvertByteArrToBase64", e);
            }
            return imageString;
        }
        #endregion

        #region ConvertBase64ToByteArray
        /// <summary>
        /// Converting base64 string into byte array
        /// </summary>
        /// <param name="ImagePath"></param>
        /// <returns></returns>
        internal byte[] ConvertBase64ToByteArray(String imageData)
        {
            byte[] bytThumb = null;
            try
            {
                if (imageData == null)
                    return null;

                if (imageData.Trim() == "")
                    return null;

                bytThumb = new Byte[imageData.Length];
                bytThumb = Convert.FromBase64String(imageData);
            }
            catch (Exception e)
            {
                m_log.Error("vrmException", e);
            }
            return bytThumb;
        }
        #endregion

        #region SetSecurityImage
        /// <summary>
        /// SetSecurityImage
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SetSecurityImage(ref vrmDataObject obj)
        {
            organizationID = 0;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;
                obj.outXml = "";
                int dispOrder = 0;

                node = xd.SelectSingleNode("//SetSecurityImage/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();

                Int32.TryParse(orgid, out organizationID);
                if (organizationID < defaultOrgId)
                {
                    myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                byte[] tempImg = null;
                string badgename = "",textaxis="",photoaxis="",barcodeaxis="",badgeid="";
                int imageid = 0;
                string attrImage = "";

                node = xd.SelectSingleNode("//SetSecurityImage/badgeid");
                if (node != null)
                {
                    badgeid = node.InnerText.Trim();
                    Int32.TryParse(badgeid, out imageid);
                }

                node = xd.SelectSingleNode("//SetSecurityImage/badgeimage");
                if (node != null)
                {
                    attrImage = node.InnerText;
                    tempImg = ConvertBase64ToByteArray(attrImage);
                }

                node = xd.SelectSingleNode("//SetSecurityImage/textaxis");
                textaxis = node.InnerText;

                node = xd.SelectSingleNode("//SetSecurityImage/barcodeaxis");
                barcodeaxis = node.InnerText;

                node = xd.SelectSingleNode("//SetSecurityImage/photoaxis");
                photoaxis = node.InnerText;

                vrmSecurityImage imObj = null;
                if (badgeid == "new")
                {
                    if (tempImg == null)
                    {
                        myVRMEx = new myVRMException(422);
                        obj.outXml = myVRMEx.FetchErrorMsg();
                        return false;
                    }
                    if (tempImg.Length <= 0)
                    {
                        myVRMEx = new myVRMException(422);
                        obj.outXml = myVRMEx.FetchErrorMsg();
                        return false;
                    }

                    imObj = new vrmSecurityImage();
                    imObj.BadgeImage = tempImg;
                    imObj.TextAxis = textaxis;
                    imObj.BarCodeAxis = barcodeaxis;
                    imObj.PhotoAxis = photoaxis;
                    imObj.OrgId = organizationID;

                    string imgQry = "SELECT max(vi.DisplayOrder) + 1 FROM myVRM.DataLayer.vrmSecurityImage vi where vi.OrgId=" + organizationID;
                    IList Result = m_ISecBadgeDao.execQuery(imgQry);
                    if (Result != null)
                    {
                        if (Result.Count > 0)
                        {
                            if (Result[0] != null)
                            {
                                if (Result[0].ToString().Trim() != "")
                                    Int32.TryParse(Result[0].ToString(), out dispOrder);
                            }
                        }
                    }
                    if (dispOrder <= 0)
                        dispOrder = 1;

                    badgename = "Security_" + dispOrder;
                    imObj.ImgName = badgename;
                    imObj.DisplayOrder = dispOrder;
                    m_ISecBadgeDao.Save(imObj);
                }
                else
                {
                    imObj = m_ISecBadgeDao.GetById(imageid);
                    if (imObj == null)
                    {
                        myVRMEx = new myVRMException(422);
                        obj.outXml = myVRMEx.FetchErrorMsg();
                        return false;
                    }
                    imObj.TextAxis = textaxis;
                    imObj.BarCodeAxis = barcodeaxis;
                    imObj.PhotoAxis = photoaxis;
                    m_ISecBadgeDao.Update(imObj);
                }
                obj.outXml = "<SetSecurityImage><Id>" + imObj.BadgeId + "</Id><ImageName>" + imObj.ImgName + "</ImageName></SetSecurityImage>";
                return true;
            }
            catch (myVRMException e)
            {
                m_log.Error("SetSecurityImage", e);
                obj.outXml = "";
                return false;
            }

            catch (Exception e)
            {
                m_log.Error("SetSecurityImage", e);
                obj.outXml = ""; 
                return false;
            }
        }
        #endregion

        #region DeleteSecurityImage
        /// <summary>
        /// DeleteSecurityImage
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        // InXML
        //<DeleteSecurityImage>
        //<badgeid>...</badgeid>
        //</DeleteSecurityImage>
        public bool DeleteSecurityImage(ref vrmDataObject obj)
        {
            int imageid = 0;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;
                obj.outXml = "";

                node = xd.SelectSingleNode("//DeleteSecurityImage/badgeid");
                string imgid = "";
                if (node != null)
                    imgid = node.InnerXml.Trim();

                Int32.TryParse(imgid, out imageid);

                vrmSecurityImage imgObj = m_ISecBadgeDao.GetByBadgeId(imageid);
                if (imgObj == null)
                {
                    myVRMEx = new myVRMException(401);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                m_ISecBadgeDao.Delete(imgObj);

                obj.outXml = "<success>1</success>";
                return true;
            }
            catch (myVRMException e)
            {
                m_log.Error("DeleteSecurityImage", e);
                obj.outXml = "";
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("DeleteSecurityImage", e);
                obj.outXml = ""; 
                return false;
            }
        }

        internal bool DeleteSecurityImage(int imageid)
        {
            //myVRMException myVRMEx;
            try
            {
                vrmSecurityImage imgObj = m_ISecBadgeDao.GetByBadgeId(imageid);
                if (imgObj == null)
                {
                    return false;
                }

                m_ISecBadgeDao.Delete(imgObj);

                return true;
            }
            catch (myVRMException e)
            {
                m_log.Error("DeleteSecurityImage", e);
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("DeleteSecurityImage", e);
                return false;
            }
        }
        #endregion
    
    }
}
