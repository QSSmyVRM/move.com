﻿//ZD 100147 Start
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886 End
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Configuration;
using System.Collections.Specialized;
using System.IO;


namespace MyvrmIntegratedEM7
{
    public partial class EM7PollService : ServiceBase
    {
        System.Timers.Timer EM7PollTimer = new System.Timers.Timer();
        public EM7PollService()
        {
            InitializeComponent();
        }

        #region OnStart
        protected override void OnStart(string[] args)
        {
            double EM7OrgPollInterval = 3000;
            double EM7EndPointPollInterval = 30000;
            try
            {
                DateTime startDate = DateTime.Parse(DateTime.Now.AddDays(1).ToShortDateString() + " " + "12:00:00 AM");
                DateTime curDate = DateTime.Now;
                EM7OrgPollInterval = startDate.Subtract(curDate).TotalMilliseconds;
                EM7Poll.EM7Poll EM7OrgPollService = new EM7Poll.EM7Poll();
                EM7OrgPollService.EM7OrgPollStart();
                EM7PollTimer.Elapsed += new System.Timers.ElapsedEventHandler(EM7OrgPollInterval_Elapsed);
                EM7PollTimer.Interval = EM7OrgPollInterval;
                EM7PollTimer.AutoReset = true;
                EM7PollTimer.Start();

                EM7Poll.EM7Poll EM7EndPointPollService = new EM7Poll.EM7Poll();
                EM7EndPointPollService.EM7EndPointPollStart();
                EM7PollTimer.Elapsed += new System.Timers.ElapsedEventHandler(EM7EndPointPollInterval_Elapsed);
                EM7PollTimer.Interval = EM7EndPointPollInterval;
                EM7PollTimer.AutoReset = true;
                EM7PollTimer.Start();

            }
            catch (Exception)
            {

            }

        }
        #endregion

        #region OnStop
        protected override void OnStop()
        {
            EM7PollTimer.Enabled = false;
            EM7PollTimer.AutoReset = false;
            EM7PollTimer.Stop();
        }
        #endregion

        #region EM7OrgPollInterval_Elapsed
        private void EM7OrgPollInterval_Elapsed(object sender, EventArgs e)
        {
            double EM7PollInterval = 24*60*60*1000;
            try
            {
                EM7PollTimer.Stop();
                EM7Poll.EM7Poll EM7OrgPollService = new EM7Poll.EM7Poll();
                EM7OrgPollService.EM7OrgPollStart();

                EM7PollTimer.Interval = EM7PollInterval;
                EM7PollTimer.AutoReset = true;
                EM7PollTimer.Start();

            }
            catch (Exception)
            {
            }

           

        }
        #endregion

        #region EM7EndPointPollInterval_Elapsed
        private void EM7EndPointPollInterval_Elapsed(object sender, EventArgs e)
        {
            double EM7PollInterval = 1*60*60*1000;
            try
            {
                EM7PollTimer.Stop();
                EM7Poll.EM7Poll EM7EndPointPollService = new EM7Poll.EM7Poll();
                EM7EndPointPollService.EM7EndPointPollStart();

                EM7PollTimer.Interval = EM7PollInterval;
                EM7PollTimer.AutoReset = true;
                EM7PollTimer.Start();

            }
            catch (Exception)
            {
            }

            
        }
        #endregion
    }
}
