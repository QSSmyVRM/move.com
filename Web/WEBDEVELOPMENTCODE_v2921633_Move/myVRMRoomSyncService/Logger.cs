﻿//ZD 100147 Start
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886 End
namespace NS_LOGGER
{
    #region References
    using System;
    using System.IO;
    using System.Threading;
    using System.Diagnostics;
    #endregion

    class Log
    {
        private static string logFilePath = "C:\\VRMSchemas_v18\\RTCLog.log";
        private static bool debugEnabled = true;

        public Log(NS_MESSENGER.ConfigParams configParams)
        {

            logFilePath = configParams.logFilePath;
            debugEnabled = configParams.debugEnabled;
        }


        public void Exception(int errorCode, string message)
        {
            message = ReplaceInvalidChars(message);

            //int severity = FetchSeverityLevel(errorCode);
            int severity = 0; //hardcoded for now

            StackFrame CallStack = new StackFrame(1, true);
            string file = CallStack.GetFileName();
            string method = CallStack.GetMethod().Name;
            int line = CallStack.GetFileLineNumber();

            string logRecord = "<EXCEPTION>";
            logRecord += "<MESSAGE>" + message + "</MESSAGE>";
            logRecord += "<SEVERITY>" + severity.ToString() + "</SEVERITY>";
            logRecord += "<FILE>" + file + "</FILE>";
            logRecord += "<METHOD>" + method + "</METHOD>";
            logRecord += "<LINE>" + line.ToString() + "</LINE>";
            logRecord += "<TIMESTAMP>" + DateTime.Now.ToString() + "</TIMESTAMP>";
            logRecord += "</EXCEPTION>";

            WriteToLogFile(logRecord);
            LoggerDLL(errorCode, severity, file, method, line, message);
        }

        public void Trace(string message)
        {
            if (debugEnabled)
            {
                StackFrame CallStack = new StackFrame(1, true);
                string file = CallStack.GetFileName();
                string method = CallStack.GetMethod().Name;
                int line = CallStack.GetFileLineNumber();

                WriteToLogFile(message);
                LoggerDLL(9999, 9, file, method, line, message);
            }
        }

        public string FetchComMsg(int errorCode, string level, string message)
        {
            string errorMsg = "<error>";
            errorMsg += "<errorCode>" + errorCode.ToString() + "</errorCode>";
            errorMsg += "<message>" + message + "</message>";
            errorMsg += "<level>" + level + "</level>";
            errorMsg += "</error>";

            return (errorMsg);
        }

        private void WriteToLogFile(string logRecord)
        {

            Console.WriteLine(logRecord);
            StreamWriter sw;
            String newLogfilepath = "";

            try
            {
                string sYear = DateTime.Now.Year.ToString();
                string sMonth = DateTime.Now.Month.ToString();
                string sDay = DateTime.Now.Day.ToString();

                string lgName = sYear + sMonth + sDay;

                newLogfilepath = logFilePath;

                newLogfilepath = newLogfilepath + "_" + lgName + ".log";

                if (!File.Exists(newLogfilepath))
                {
                    // file doesnot exist . hence, create a new log file.
                    sw = File.CreateText(newLogfilepath);
                    sw.Flush();
                    sw.Close();
                }
                else
                {
                    // check if exisiting log file size is greater than 50 MB
                    FileInfo fi = new FileInfo(newLogfilepath);
                    if (fi.Length > 50000000)
                    {
                        // delete the log file						
                        File.Delete(logFilePath);

                        // create a new log file 
                        sw = File.CreateText(newLogfilepath);
                        sw.Flush();
                        sw.Close();
                    }
                }

                // write the log record.
                sw = File.AppendText(newLogfilepath);
                sw.WriteLine(logRecord);
                sw.Flush();
                sw.Close();
            }
            catch (Exception)
            {
                // do nothing
            }
        }

        private string ReplaceInvalidChars(string input)
        {
            input = input.Replace("<", "&lt;");
            input = input.Replace(">", "&gt;");
            input = input.Replace("&", "&amp;");
            return (input);
        }

        private void LoggerDLL(int errorCode, int severity, string file, string function, int line, string message)
        {
            /*
                        try 
                        {
                            Logger log = new Logger(NS_CONFIG.Config.SYSTEM_GLOBAL_CONFIG_FILE);
			
                            // Logging using Logger.LogMessage(Log log)
                            // Create a Log object
                            Logging.Log logmsg = new Logging.Log();
                            logmsg.ModuleName	= "ConfSetup";
                            logmsg.ModuleSystemID	= "";
                            logmsg.ModuleErrorCode = errorCode;
                            logmsg.Severity	= severity;
                            logmsg.File		= file;
                            logmsg.Function	= function ;
                            logmsg.Line		= line;
                            logmsg.Message	= message;
                            logmsg.Timestamp = DateTime.Now.ToUniversalTime(); // GMT 
                            if ( !log.LogMessage(logmsg) )
                                Console.WriteLine("Error thrown by Logger : " + log.ErrorMessage);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine ("Logger problem" + e.Message);
                        }
            */
        }

    }


}