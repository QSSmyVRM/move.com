﻿//ZD 100147 Start
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End //ZD 100886
/*** FB 1838 Service manager changes ***/

#region References
using System;
using System.Runtime.InteropServices;
using System.Threading;
using System.Net;
using System.IO;
using System.Text;
using System.Security.Cryptography.X509Certificates;
using msxml4_Net;
using System.Data;
using System.Xml;
using System.Collections;
using System.ComponentModel;
using System.Web;
using myVRM.DataLayer;
using System.Collections.Generic;
using System.ServiceProcess;

#endregion

namespace myVRMservice
{
    public partial class myVRMService : ServiceBase
    {
        //For ZD 101063  - Changing static variables to non-static varibles
        private DataTable _dtConfEPTs = null;
        String dirPth = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
        String MyVRMServer_ConfigPath = "";
        String COM_ConfigPath = "";
        String RTC_ConfigPath = "";
        ASPIL.VRMServer myvrmCom = new ASPIL.VRMServer();
        //static web_com_v18_Net.Com com = null; //FB 2027
        VRMRTC.VRMRTC obj = null;
        Thread launchThrd = null;
        Thread updateThrd = null;
        Thread updateMessageThrd = null;//FB 2486
        Thread FetchSwitchingCallsThrd = null;//FB 2595
        Thread UpdateCompletedCallsThrd = null; //ZD #100093 & ZD #100085
        System.Timers.Timer timerActivation = new System.Timers.Timer();
        System.Timers.Timer timerLaunchandUpdt = new System.Timers.Timer();
        System.Timers.Timer timerP2PLaunchUpdt = new System.Timers.Timer();
        System.Timers.Timer timerP2PTerminate = new System.Timers.Timer();
        System.Timers.Timer timerFetchSwitchingCalls = new System.Timers.Timer();//FB 2595
        System.Timers.Timer timerUpdateCompletedCalls = new System.Timers.Timer(); //ZD #100093 & ZD #100085
        System.Timers.Timer timerRPRMPush = new System.Timers.Timer();
        System.Timers.Timer timerHDBusyConf = new System.Timers.Timer(); //ALLDEV-807
        static System.Timers.Timer timerAutoPurgelogs = new System.Timers.Timer();//ZD 104846
        NS_CONFIG.Config config = null;
        NS_MESSENGER.ConfigParams configParams = null;
        string configPath = "";
        string errMsg = "";
        NS_LOGGER.Log log = null;
        bool ret = false;
        System.Globalization.CultureInfo globalCultureInfo = new System.Globalization.CultureInfo("en-US",true);//FB 2545
        static int purgeDuration = 0;//ZD 104846
       

        public myVRMService()
        {
            MyVRMServer_ConfigPath = dirPth + "\\VRMSchemas\\";
            COM_ConfigPath = dirPth + "\\VRMSchemas\\COMConfig.xml";
            RTC_ConfigPath = dirPth + "\\VRMSchemas\\VRMRTCConfig.xml";
            configPath = dirPth + "\\VRMMaintServiceConfig.xml";

            InitializeComponent();

            
        }
      
        #region OnStart
        protected override void OnStart(string[] args)
        {

            double conflauch = 30000;
            double P2Pterminate = 5000;
            double RPRMLaunch = 10000;
            double PurgeLogsInterval = 24 * 60 * 60 * 1000; //ZD 104846 //24 Hours
            try
            {

                config = new NS_CONFIG.Config();
                configParams = new NS_MESSENGER.ConfigParams();
                ret = config.Initialize(configPath, ref configParams, ref errMsg, MyVRMServer_ConfigPath, RTC_ConfigPath);
                log = new NS_LOGGER.Log(configParams);

                log.Trace("**** Starting Myvrm Service ****" + DateTime.Now.ToLocalTime());
                log.Trace("Various Configs COM:" + COM_ConfigPath + " RTC:" + RTC_ConfigPath + " ASPIL:" + MyVRMServer_ConfigPath);
                log.Trace("Site URL: " + configParams.siteUrl);
                log.Trace("ActivationTimer:" + configParams.activationTimer);

                timerActivation.Elapsed += new System.Timers.ElapsedEventHandler(timerActivation_Elapsed);
                timerActivation.Interval = configParams.activationTimer;
                timerActivation.Enabled = true;
                timerActivation.AutoReset = true;
                timerActivation.Start();

                timerLaunchandUpdt.Elapsed += new System.Timers.ElapsedEventHandler(timerLaunchandUpdt_Elapsed);
                timerLaunchandUpdt.Interval = conflauch + 2000;
                timerLaunchandUpdt.Enabled = true;
                timerLaunchandUpdt.AutoReset = true;
                timerLaunchandUpdt.Start();
                log.Trace("Timer for AudioVideo calls started...." + DateTime.Now.ToLocalTime());

                timerP2PLaunchUpdt.Elapsed += new System.Timers.ElapsedEventHandler(timerP2PLaunchUpdt_Elapsed);
                timerP2PLaunchUpdt.Interval = conflauch;
                timerP2PLaunchUpdt.Enabled = true;
                timerP2PLaunchUpdt.AutoReset = true;
                timerP2PLaunchUpdt.Start();
                log.Trace("Timer for P2P calls started...." + DateTime.Now.ToLocalTime());

                timerP2PTerminate.Elapsed += new System.Timers.ElapsedEventHandler(timerP2PTerminate_Elapsed);
                timerP2PTerminate.Interval = P2Pterminate;
                timerP2PTerminate.Enabled = true;
                timerP2PTerminate.AutoReset = true;
                timerP2PTerminate.Start();
                log.Trace("P2P timer event started...." + DateTime.Now.ToLocalTime());

                
                //FB 2595 Start

                timerFetchSwitchingCalls.Elapsed += new System.Timers.ElapsedEventHandler(timerFetchSwitchingCalls_Elapsed);
                timerFetchSwitchingCalls.Interval = conflauch + 1000;
                timerFetchSwitchingCalls.Enabled = true;
                timerFetchSwitchingCalls.AutoReset = true;
                timerFetchSwitchingCalls.Start();
                log.Trace("Timer for Updating calls for switching started...." + DateTime.Now.ToLocalTime());

                //FB 2595 End

                //ZD #100093 & ZD #100085 Start

                //timerUpdateCompletedCalls.Elapsed += new System.Timers.ElapsedEventHandler(timerCompletedCalls_Elapsed);
                //timerUpdateCompletedCalls.Interval = conflauch + 10000;
               // timerUpdateCompletedCalls.Enabled = true;
                //timerUpdateCompletedCalls.AutoReset = true;
               // timerUpdateCompletedCalls.Start();
                //log.Trace("Timer for Updating calls for Completed started...." + DateTime.Now.ToLocalTime());

                //ZD #100093 & ZD #100085 End

                //ZD 102600
                timerRPRMPush.Elapsed += new System.Timers.ElapsedEventHandler(timerRPRMPush_Elapsed);
                timerRPRMPush.Interval = RPRMLaunch;
                timerRPRMPush.Enabled = true;
                timerRPRMPush.AutoReset = true;
                timerRPRMPush.Start();
                log.Trace("Timer for Updating calls for switching started...." + DateTime.Now.ToLocalTime());

                //ALLDEV-807 Starts
                
                DateTime startDate = DateTime.Parse(DateTime.Now.AddDays(1).ToShortDateString() + " " + "12:01:00 AM");
                DateTime curDate = DateTime.Now;
                Double timeInterval = startDate.Subtract(curDate).TotalMilliseconds;

                timerHDBusyConf.Elapsed += new System.Timers.ElapsedEventHandler(timerHDBusyConf_Elapsed);
                timerHDBusyConf.Interval = timeInterval;
                timerHDBusyConf.Start();
                log.Trace("Timer for Updating HD Busy Calls started...." + DateTime.Now.ToLocalTime());
                //ALLDEV-807 Ends

                //ZD 104846 start
                timerAutoPurgelogs.Elapsed += new System.Timers.ElapsedEventHandler(timerAutoPurgelogs_Elapsed);
                timerAutoPurgelogs.Interval = PurgeLogsInterval;
                timerAutoPurgelogs.Enabled = true;
                timerAutoPurgelogs.Start();

                //ZD 104846 End


                log.Trace("Exits Myvrm service cycle...");
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + ex.ToString());

            }
        }
        #endregion

        #region timerLaunchandUpdt_Elapsed
        void timerLaunchandUpdt_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {

            double conflauch = 32000;
            try
            {
                timerLaunchandUpdt.Stop();
                log.Trace("Entering timerLaunchandUpdt_Elapsed block...." + DateTime.Now.ToLocalTime());

                GetConferences();

                if (launchThrd != null)
                    while (launchThrd.IsAlive)
                        launchThrd.Join();//To Terminate current thread

                if (updateThrd != null)
                    while (updateThrd.IsAlive)
                        updateThrd.Join();//To Terminate current thread

                timerLaunchandUpdt.Interval = conflauch;
                timerLaunchandUpdt.Enabled = true;
                timerLaunchandUpdt.AutoReset = true;
                //timerLaunchandUpdt.Start();
                log.Trace("Timer event timerLaunchandUpdt started...." + DateTime.Now.ToLocalTime());


            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + ex.ToString());

            }
        }
        #endregion

        #region timerRPRMPush_Elapsed 
        /// <summary>
        ///  ZD 102600
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void timerRPRMPush_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            
            try
            {
                Thread.CurrentThread.CurrentCulture = globalCultureInfo;
                timerRPRMPush.Stop();
                log.Trace("Entering timerLaunchandUpdt_Elapsed block...." + DateTime.Now.ToLocalTime());                
                GetRPRMConferences();
                timerRPRMPush.Interval = 10000;
                timerRPRMPush.Enabled = true;
                timerRPRMPush.AutoReset = true;
                timerRPRMPush.Start();
                log.Trace("Timer event timerLaunchandUpdt started...." + DateTime.Now.ToLocalTime());

            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + ex.ToString());

            }
        }
        #endregion

        #region timerP2PLaunchUpdt_Elapsed
        void timerP2PLaunchUpdt_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            double conflauch = 30000;
            try
            {
                //timerP2PCalls.Stop();
                log.Trace("Entering timerP2PLaunchUpdt_Elapsed block...." + DateTime.Now.ToLocalTime());
                GetP2PConferences();

                if (launchThrd != null)
                    while (launchThrd.IsAlive)
                        launchThrd.Join();//To Terminate current thread

                if (updateThrd != null)
                    while (updateThrd.IsAlive)
                        updateThrd.Join();//To Terminate current thread

                timerP2PLaunchUpdt.Interval = conflauch;
                timerP2PLaunchUpdt.Enabled = true;
                timerP2PLaunchUpdt.AutoReset = true;
                timerP2PLaunchUpdt.Start();
                log.Trace("Timer event timerP2PLaunchUpdt started...." + DateTime.Now.ToLocalTime());

            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + ex.ToString());

            }
        }
        #endregion

        #region timerP2PTerminate_Elapsed
        void timerP2PTerminate_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            double P2Pterminate = 5000;
            try
            {
                timerP2PTerminate.Stop();

                if (obj == null)//FB 2363 start
                    obj = new VRMRTC.VRMRTC();


                obj.Operations(RTC_ConfigPath, "TerminateCompletedP2PConfs", "<Admin>11</Admin>");

                timerP2PTerminate.Interval = P2Pterminate;
                timerP2PTerminate.Enabled = true;
                timerP2PTerminate.AutoReset = true;
                timerP2PTerminate.Start();

            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);

            }
        }
        #endregion

        #region timerActivation_Elapsed
        void timerActivation_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                CheckActivation();

            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + ex.ToString());

            }
        }
        #endregion

        //ALLDEV-807 Strats
        #region timerHDBusyConf_Elapsed
        /// <summary>
        ///  ZD 102600
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void timerHDBusyConf_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {

            try
            {
                //Thread.CurrentThread.CurrentCulture = globalCultureInfo;
                timerHDBusyConf.Stop();
                timerHDBusyConf.AutoReset = false;
                log.Trace("Entering timerHDBusyConf_Elapsed block...." + DateTime.Now.ToLocalTime());
                ScheduleHDBusyConferences();
                //System.Threading.Thread.Sleep(5000);
                timerHDBusyConf.Interval = 24 * 60 * 60 * 1000;
                timerHDBusyConf.AutoReset = true;
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + ex.ToString());

            }
        }
        #endregion
        //ALLDEV-807 Emds

        //FB 2595 Start

        #region timerFetchSwitchingCalls_Elapsed
        void timerFetchSwitchingCalls_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            double Switchopt = 30000;
            try
            {
                log.Trace("Entering timerFetchSecuredCalls_Elapsed block...." + DateTime.Now.ToLocalTime());

                GetSwitchingConferences();

                if (FetchSwitchingCallsThrd != null)
                    while (FetchSwitchingCallsThrd.IsAlive)
                        FetchSwitchingCallsThrd.Join();//To Terminate current thread

                timerFetchSwitchingCalls.Interval = Switchopt;
                timerFetchSwitchingCalls.Enabled = true;
                timerFetchSwitchingCalls.AutoReset = true;
                timerFetchSwitchingCalls.Start();
                log.Trace("Timer event timerFetchSecuredCalls started...." + DateTime.Now.ToLocalTime());

            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + ex.ToString());

            }
        }
        #endregion

        //FB 2595 End

        #region GetSSLPage
        public bool GetSSLPage(string url)
        {
            try
            {
                ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(ValidateServerCertificate);

                WebRequest req = WebRequest.Create(url);
                req.Timeout = 100000;//GP Timeout Issue
                WebResponse result = req.GetResponse();
                Stream ReceiveStream = result.GetResponseStream();
                Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
                StreamReader sr = new StreamReader(ReceiveStream, encode);
                Console.WriteLine("\r\nResponse stream received");
                Char[] read = new Char[10];//GP
                //int count = sr.Read(read, 0, 256);//GP
                int count = sr.Read(read, 0, 10);//GP

                Console.WriteLine("HTML...\r\n");
                //GP START
                //while (count > 0)
                //{
                //    String str = new String(read, 0, count);
                //    Console.Write(str);
                //    count = sr.Read(read, 0, 256);
                //}

                if (count > 0)
                {
                    String str = new String(read, 0, count);
                    Console.Write(str);
                }
                //GP END
                Console.WriteLine("");

                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine("\r\nThe request URI could not be found or was malformed");
                return false;
            }
        }
        #endregion

        #region CheckActivation
        public void CheckActivation()
        {

            String receive = "";
            try
            {
                if (ret)
                {
                    //GetSSLPage(configParams.siteUrl); //GP Issue
                    try
                    {
                        string siteURL2 = "";
                        //siteURL2 = configParams.siteUrl.Replace("NewRepeatOps.aspx", "ServiceResponse.aspx");

                        XMLHTTP xmlHttp = new XMLHTTP();
                        xmlHttp.open("POST", configParams.siteUrl, false, null, null);
                        xmlHttp.send("hello");

                        if (xmlHttp.status == 200)
                        {
                            receive = xmlHttp.responseText;

                            // Local config file loaded successfully.
                            log.Trace("**** Starting Check Activation Cycle****");//GP
                            log.Trace("Date/Time: " + DateTime.Now.ToString("F"));
                            log.Trace("SiteURL : " + configParams.siteUrl);
                            log.Trace("Response : " + receive.Remove(10));
                            log.Trace("-----");
                            ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(ValidateServerCertificate);

                            WebRequest req = WebRequest.Create(configParams.siteUrl);
                            req.Timeout = 60000;
                            WebResponse result = req.GetResponse();
                            Stream ReceiveStream = result.GetResponseStream();
                            Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
                            StreamReader sr = new StreamReader(ReceiveStream, encode);
                            Char[] read = new Char[10];
                            int count = sr.Read(read, 0, 10);
                        }


                    }
                    catch (Exception e)
                    {
                        receive = e.Message;
                    }
                }
                else
                {
                    Console.WriteLine("Failure in reading from config file.");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

        }
        #endregion

        //FB 2363
        #region GenerateESUserReport
        /// <summary>
        /// GenerateESUserReport
        /// </summary>
        /// <returns></returns>
        private bool GenerateESUserReport()
        {
            try
            {
                String inXML = "", OutXML = "";

                inXML = "<report>";
                inXML += "<configpath>" + MyVRMServer_ConfigPath + "</configpath>";
                inXML += "<reportType>GU</reportType>";
                inXML += "<export>1</export>";
                inXML += "<Destination>" + configParams.reportFilePath + "</Destination>";
                inXML += "<fileName>UserReport.xls</fileName>";
                inXML += "</report>";


                if (myvrmCom == null)
                    myvrmCom = new ASPIL.VRMServer();
                OutXML = myvrmCom.Operations(MyVRMServer_ConfigPath, "GenerateESUserReport", inXML);
                if (OutXML.IndexOf("<error>") >= 0)
                    log.Trace("Error in Generate User Report: " + OutXML);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
            return true;
        }
        #endregion

        #region GetP2PConferences
        /// <summary>
        /// GetP2PConferences
        /// </summary>
        /// <param name="conftype"></param>
        /// conftype = 4 for p2p calls
        private void GetP2PConferences()
        {
            try
            {
                log.Trace("Into GetP2PConferences... " + DateTime.Now.ToLocalTime());
                if(myvrmCom == null)
                    myvrmCom = new ASPIL.VRMServer();

                CreateEPTDataTable();

                String searchConfInXML = "<SearchConference><UserID>11</UserID><ConferenceID/><ConferenceUniqueID/><StatusFilter><ConferenceStatus>0</ConferenceStatus><ConferenceStatus>6</ConferenceStatus><ConferenceStatus>5</ConferenceStatus></StatusFilter><TypeFilter><ConfType>4</ConfType></TypeFilter></SearchConference>"; //FB 2434
                String searchConfOutXML = myvrmCom.Operations(MyVRMServer_ConfigPath, "GetOngoing", searchConfInXML);
                
                GetP2PConfs(searchConfOutXML);
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + ex.ToString());

            }
        }
        #endregion

        #region GetConferences
        /// <summary>
        /// GetConferences
        /// </summary>
        /// <param name="conftype"></param>
        /// if conftype = 2 video; 4 p2p; 6 audio; 7 room; 1 all
        /// need to code for launching audio calls
        private void GetConferences()
        {
            try
            {
                log.Trace("Into GetConferences... " + DateTime.Now.ToLocalTime());
                if (myvrmCom == null)
                    myvrmCom = new ASPIL.VRMServer();

                CreateEPTDataTable();
                String searchConfInXML = "<SearchConference><UserID>11</UserID><ConferenceID/><ConferenceUniqueID/><StatusFilter><ConferenceStatus>0</ConferenceStatus><ConferenceStatus>6</ConferenceStatus><ConferenceStatus>5</ConferenceStatus></StatusFilter><TypeFilter><ConfType>2</ConfType></TypeFilter></SearchConference>"; //FB 2434
                String searchConfOutXML = myvrmCom.Operations(MyVRMServer_ConfigPath, "GetOngoing", searchConfInXML);
                log.Trace("searchConfOutXML" + searchConfOutXML);

                GetConfs(searchConfOutXML);
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + ex.ToString());
            }
        }

        //ZD 102600
        private void GetRPRMConferences() 
        {
            String searchConfInXML = "";
            String searchConfOutXML = "";

            string inXML = "";
            string outXML = "";
            try
            {
                log.Trace("Into GetConferences... " + DateTime.Now.ToLocalTime());
                if (myvrmCom == null)
                    myvrmCom = new ASPIL.VRMServer();
                searchConfInXML = "<SearchConference><UserID>11</UserID><ConferenceID/><ConferenceUniqueID/><StatusFilter><ConferenceStatus>0</ConferenceStatus><ConferenceStatus>6</ConferenceStatus></StatusFilter><TypeFilter><ConfType>2</ConfType></TypeFilter></SearchConference>"; //FB 2434
                searchConfOutXML = myvrmCom.Operations(MyVRMServer_ConfigPath, "GetRPRMConfforLaunch", searchConfInXML);
                log.Trace("searchConfOutXML" + searchConfOutXML);

                try
                {
                    log.Trace("Into GetRPRMConf:" + DateTime.Now.ToLocalTime());
                    obj = new VRMRTC.VRMRTC();

                    //FB 2027 start
                    //com = new web_com_v18_Net.ComClass();
                    if (myvrmCom == null)
                        myvrmCom = new ASPIL.VRMServer();
                    //FB 2027 end

                    if (searchConfOutXML.IndexOf("<error>") > 0)
                        return;
                    if (_dtConfEPTs == null)
                        CreateEPTDataTable();
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(searchConfOutXML);
                    XmlNodeList nodes = xmldoc.SelectNodes("//SearchConference/Conferences/Conference"); //ZD 102723
                    if (nodes != null)
                    {
                        log.Trace("RPRM Conferences:" + nodes.Count.ToString() + "... Time: " + DateTime.Now.ToLocalTime());

                        for (int cnfcnt = 0; cnfcnt < nodes.Count; cnfcnt++)
                        {
                            log.Trace("RPRM Conferences:" + nodes[cnfcnt].InnerXml + "... Time: " + DateTime.Now.ToLocalTime());
                            if (nodes[cnfcnt].InnerXml != "" && nodes[cnfcnt].SelectSingleNode("//Conference/confID") != null)
                            {
                                inXML = nodes[cnfcnt].OuterXml; //ZD 102723
                                outXML = obj.Operations(RTC_ConfigPath, "SetConferenceOnRPRM", inXML);
                                if (outXML.IndexOf("<error>") >= 0)
                                    log.Trace("Error in Generate User Report: " + outXML);
                                else
                                {
                                    inXML = "<Conference><UserID>11</UserID><confID>" + nodes[cnfcnt].SelectSingleNode("//Conference/confID").InnerText.Trim() + "</confID><editFromWeb>1</editFromWeb><IsConfApproved>1</IsConfApproved></Conference>";
                                    outXML = myvrmCom.Operations(MyVRMServer_ConfigPath, "SendSynchronousBridgeemails", inXML);
                                    inXML = "<login><conferenceID>" + nodes[cnfcnt].SelectSingleNode("//Conference/confID").InnerText.Trim() + "</conferenceID></login>";
                                    outXML = obj.Operations(RTC_ConfigPath, "TerminateSyncConference", inXML);
                                }
                            }
                        }
                    }
                    else
                    {
                        log.Trace("Number of ongoing calls: 0");
                        log.Trace("Exit GetConfs Method....");
                    }
                }
                catch (Exception ex)
                {
                    log.Trace(ex.StackTrace + ex.ToString());
                }
               
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + ex.ToString());
              
            }
            finally
            {
                //com = null;
            }
        }
        #endregion
  
        #region CreateEPTDataTable
        private void CreateEPTDataTable()
        {
            try
            {
                if (_dtConfEPTs == null)
                {
                    _dtConfEPTs = new DataTable();
                    _dtConfEPTs.Columns.Add("confid");
                    _dtConfEPTs.Columns.Add("ConfType");
                    _dtConfEPTs.Columns.Add("rowType");
                    _dtConfEPTs.Columns.Add("EndpointID");
                    _dtConfEPTs.Columns.Add("EPTTerminaltype");
                    _dtConfEPTs.Columns.Add("confUniqueID");
                    _dtConfEPTs.Columns.Add("confName");
                    _dtConfEPTs.Columns.Add("endpointname");
                    _dtConfEPTs.Columns.Add("ConfTxtMessage"); //FB 2486
                    _dtConfEPTs.Columns.Add("NetworkState"); //FB 2595
                    _dtConfEPTs.Columns.Add("OrgRequestTime"); //FB 2993
                }
                else
                {
                    if (!_dtConfEPTs.Columns.Contains("confid"))
                        _dtConfEPTs.Columns.Add("confid");
                    if (!_dtConfEPTs.Columns.Contains("rowType"))
                        _dtConfEPTs.Columns.Add("rowType");
                    if (!_dtConfEPTs.Columns.Contains("ConfType"))
                        _dtConfEPTs.Columns.Add("ConfType");
                    if (!_dtConfEPTs.Columns.Contains("EndpointID"))
                        _dtConfEPTs.Columns.Add("EndpointID");
                    if (!_dtConfEPTs.Columns.Contains("EPTTerminaltype"))
                        _dtConfEPTs.Columns.Add("EPTTerminaltype");
                    if (!_dtConfEPTs.Columns.Contains("confUniqueID"))
                        _dtConfEPTs.Columns.Add("confUniqueID");
                    if (!_dtConfEPTs.Columns.Contains("confName"))
                        _dtConfEPTs.Columns.Add("confName");
                    if (!_dtConfEPTs.Columns.Contains("endpointname"))
                        _dtConfEPTs.Columns.Add("endpointname");
                    if (!_dtConfEPTs.Columns.Contains("ConfTxtMessage")) //FB 2486
                        _dtConfEPTs.Columns.Add("ConfTxtMessage");
                    if (!_dtConfEPTs.Columns.Contains("NetworkState")) //FB 2595
                        _dtConfEPTs.Columns.Add("NetworkState");
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + ex.ToString());

            }
        }
        #endregion

        //FB 2595 Start

        #region GetSwitchingConferences
        /// <summary>
        /// GetSwitchingConferences
        /// </summary>
        /// <param name="conftype"></param>
        /// if conftype = 2 video; 4 p2p; 6 audio; 7 room; 1 all
        /// need to code for launching audio calls
        private void GetSwitchingConferences()
        {
            try
            {
                log.Trace("Into GetSwitchingConferences... " + DateTime.Now.ToLocalTime());
                if (myvrmCom == null)
                    myvrmCom = new ASPIL.VRMServer();

                CreateEPTDataTable();
                String searchConfInXML = "<SearchConference><UserID>11</UserID><ConferenceID/><ConferenceUniqueID/><StatusFilter><ConferenceStatus>0</ConferenceStatus></StatusFilter><TypeFilter><ConfType>2</ConfType></TypeFilter></SearchConference>"; 
                String searchConfOutXML = myvrmCom.Operations(MyVRMServer_ConfigPath, "GetCallsForSwitching", searchConfInXML);
                GetSwitchingConfs(searchConfOutXML);
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + ex.ToString());
            }
        }
        #endregion

        //FB 2595 End

        //ALLDEV-807 Starts
        #region ScheduleHDBusyConferences
       /// <summary>
        /// ScheduleHDBusyConferences
       /// </summary>
        private void ScheduleHDBusyConferences()
        {
            try
            {
                log.Trace("Into ScheduleHDBusyConferences... " + DateTime.Now.ToLocalTime());
                if (myvrmCom == null)
                    myvrmCom = new ASPIL.VRMServer();

                String searchConfInXML = "<ScheduleHDBusyConferences><organizationID>11</organizationID></ScheduleHDBusyConferences>";
                String searchConfOutXML = myvrmCom.Operations(MyVRMServer_ConfigPath, "ScheduleHDBusyConferences", searchConfInXML);
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + ex.ToString());
            }
        }
        #endregion
        //ALLDEV-807 Ends

        //FB 2560 - This method get modified for this case

        #region GetConfs
        /// <summary>
        /// GetConfs
        /// </summary>
        /// <param name="pinXML"></param>
        private void GetConfs(String pinXML)
        {
            int StartMode = 0; //FB 2501
            try
            {
                log.Trace("Into GetAllConfs:" + DateTime.Now.ToLocalTime());
                obj = new VRMRTC.VRMRTC();

                //FB 2027 start
                //com = new web_com_v18_Net.ComClass();
                if (myvrmCom == null)
                    myvrmCom = new ASPIL.VRMServer();
                //FB 2027 end

                if (pinXML.IndexOf("<error>") > 0)
                    return;
                if (_dtConfEPTs == null)
                    CreateEPTDataTable();
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(pinXML);
                XmlNodeList nodes = xmldoc.SelectNodes("//SearchConference/Conferences/Conference");
                if (nodes != null)
                {
                    log.Trace("Number of AudioVideo Conferences:" + nodes.Count.ToString() + "... Time: " + DateTime.Now.ToLocalTime());

                    List<DataRow> arrRows = new List<DataRow>();
                    List<DataRow> arrUpdtRows = new List<DataRow>();
                    List<DataRow> arrConfMsgRows = new List<DataRow>();
                    XmlNode nodeConf = null;
                    String ConfInstID = "";
                    string confUID = "";
                    string confname = "";
                    Int32 ConferenceType = 0;
                    XmlNode nodeConftype = null;
                    XmlNode nodeStatus = null;
                    String ConferenceStatus = "";
                    XmlNode nodeLastRun = null;
                    String lstrun = "";
                    string isTextMsg = "";
                    XmlNodeList iCALNodes = null;
                    DataRow drLaunch = null;
                    DataRow drStatus = null;
                    string ConfTxtMessage = "";

                    for (int cnfcnt = 0; cnfcnt < nodes.Count; cnfcnt++)
                    {
                        nodeConf = null;
                        ConfInstID = "";
                        confUID = "";
                        confname = "";
                        nodeConftype = null;
                        ConferenceType = 0;
                        nodeStatus = null;
                        ConferenceStatus = "";
                        nodeLastRun = null;
                        isTextMsg = "";
                        iCALNodes = null;
                        ConfTxtMessage = "";

                        nodeConf = nodes[cnfcnt].SelectSingleNode("ConferenceID");
                        ConfInstID = nodeConf.InnerXml.Trim();
                        confUID = nodes[cnfcnt].SelectSingleNode("ConferenceUniqueID").InnerText.Trim();
                        confname = nodes[cnfcnt].SelectSingleNode("ConferenceName").InnerText.Trim();
                        nodeConftype = nodes[cnfcnt].SelectSingleNode("ConferenceType");
                        Int32.TryParse(nodeConftype.InnerXml.Trim(), out ConferenceType);

                        nodeStatus = nodes[cnfcnt].SelectSingleNode("ConferenceActualStatus");
                        ConferenceStatus = nodeStatus.InnerXml.Trim();
                        nodeLastRun = nodes[cnfcnt].SelectSingleNode("LastRunDate");
                        lstrun = nodeLastRun.InnerXml.Trim();

                        //FB 2501 Starts
                        StartMode = 0;
                        nodeLastRun = nodes[cnfcnt].SelectSingleNode("CallStartMode");
                        if (nodeLastRun != null)
                            int.TryParse(nodeLastRun.InnerXml.Trim(), out StartMode);
                        //FB 2501 Ends
                        
                        //ZD 102985 - Call will be launched now and endpoint status will be updated
                        //iCALNodes = nodes[cnfcnt].SelectNodes("descendant::terminal[isCalendarInvite='1']");
                        //if (iCALNodes != null)
                        //    if (iCALNodes.Count > 0)
                        //        continue;

                        isTextMsg = nodes[cnfcnt].SelectSingleNode("isTextMsg").InnerText.Trim();

                        drLaunch = null;
                        drLaunch = _dtConfEPTs.NewRow();
                        drLaunch["confid"] = ConfInstID;
                        drLaunch["ConfType"] = ConferenceType.ToString();
                        drLaunch["confUniqueID"] = confUID;
                        drLaunch["confName"] = confname;
                        drLaunch["rowType"] = "M";
                        if (ConferenceStatus == "0") //FB 2501  0 - Automatic  1 - Manual
                        {
                            drLaunch["rowType"] = "L"; //Call needs to be launched into MCU
                            if (StartMode == 0)
                                arrRows.Add(drLaunch);
                        }

                        if (arrRows.Count > 0) // create seperate threads for launching calls
                        {
                            log.Trace("New thread started for launching call: " + ConfInstID + " on " + DateTime.Now.ToLocalTime());
                            launchThrd = new Thread(LaunchOngoing);
                            launchThrd.CurrentCulture = globalCultureInfo; //FB 2545
                            launchThrd.SetApartmentState(ApartmentState.MTA);
                            launchThrd.Start(arrRows);
                            arrRows = new List<DataRow>();
                        }

                        //To fetch endpoint status
                        XmlNodeList nodesEP = nodes[cnfcnt].SelectNodes("//SearchConference/Conferences/Conference/terminals/terminal");

                        for (int eptcnt = 0; eptcnt < nodesEP.Count; eptcnt++)
                        {
                            drStatus = null;
                            drStatus = _dtConfEPTs.NewRow();
                            drStatus["confid"] = ConfInstID;
                            drStatus["confUniqueID"] = confUID;
                            drStatus["ConfType"] = ConferenceType.ToString();
                            drStatus["endpointID"] = nodesEP[eptcnt].SelectSingleNode("endpointID").InnerText.Trim();
                            drStatus["EPTTerminaltype"] = nodesEP[eptcnt].SelectSingleNode("type").InnerText.Trim();

                            if (drLaunch["rowType"].ToString() == "M")
                            {
                                arrUpdtRows.Add(drStatus);
                            }
                        }
                        if (arrUpdtRows.Count > 0)
                        {
                            log.Trace(" Endpoint Cnt: " + arrUpdtRows.Count);
                            log.Trace("New thread started for fetching endpoint status: " + ConfInstID + " on " + DateTime.Now.ToLocalTime());
                            updateThrd = new Thread(UpdateConfStatus);
                            updateThrd.SetApartmentState(ApartmentState.MTA);
                            updateThrd.CurrentCulture = globalCultureInfo; //FB 2545 
                            updateThrd.Start(arrUpdtRows);
                            arrUpdtRows = new List<DataRow>();
                        }
                        //To send conference end message - Text overlay
                        if (isTextMsg == "1")
                        {
                            if (drLaunch["rowType"].ToString() == "M")
                            {
                                ConfTxtMessage = nodes[cnfcnt].SelectSingleNode("ConferenceTextMessage").InnerText.Trim();
                                drLaunch["ConfTxtMessage"] = ConfTxtMessage;
                                arrConfMsgRows.Add(drLaunch);
                            }
                        }
                        if (arrConfMsgRows.Count > 0)
                        {
                            log.Trace("New thread started for fetching Conference Msessage: " + ConfInstID + " on " + DateTime.Now.ToLocalTime());
                            updateMessageThrd = new Thread(UpdateConfMesseageStatus);
                            updateMessageThrd.CurrentCulture = globalCultureInfo; //FB 2545
                            updateMessageThrd.SetApartmentState(ApartmentState.MTA);
                            updateMessageThrd.Start(arrConfMsgRows);
                            arrConfMsgRows = new List<DataRow>();
                        }
                        //FB 2560 Code Changes ... End
                    }
                }
                else
                {
                    log.Trace("Number of ongoing calls: 0");
                    log.Trace("Exit GetConfs Method....");
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + ex.ToString());
            }
            finally
            {
                //com = null;
            }
        }

       
        #endregion

        #region GetP2PConfs
        private void GetP2PConfs(String pinXML)
        {
            int StartMode = 0; //FB 2501
            try
            {
                log.Trace("Into Getp2pConfs:" + DateTime.Now.ToLocalTime());
                obj = new VRMRTC.VRMRTC();

                //FB 2027 start
                //com = new web_com_v18_Net.ComClass();
                if (myvrmCom == null)
                    myvrmCom = new ASPIL.VRMServer();
                //FB 2027 end

                if (pinXML.IndexOf("<error>") > 0)
                    return;
                if (_dtConfEPTs == null)
                    CreateEPTDataTable();
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(pinXML);
                XmlNodeList nodes = xmldoc.SelectNodes("//SearchConference/Conferences/Conference");
                if (nodes != null)
                {
                    log.Trace("Number of P2P Conferences:" + nodes.Count.ToString() + "... Time: " + DateTime.Now.ToLocalTime());

                    List<DataRow> arrRows = new List<DataRow>();
                    List<DataRow> arrUpdtRows = new List<DataRow>();
                    List<DataRow> arrConfMsgRows = new List<DataRow>();
                    XmlNode nodeConf = null;
                    String ConfInstID = "";
                    string confUID = "";
                    string confname = "";
                    Int32 ConferenceType = 0;
                    XmlNode nodeConftype = null;
                    XmlNode nodeStatus = null;
                    String ConferenceStatus = "";
                    XmlNode nodeLastRun = null;
                    String lstrun = "";
                    string isTextMsg = "";
                    XmlNodeList iCALNodes = null;
                    DataRow drLaunch = null;
                    DataRow drStatus = null;
                    string ConfTxtMessage = "";
                    int ProcessStatus = 0; // ZD 100676

                    for (int cnfcnt = 0; cnfcnt < nodes.Count; cnfcnt++)
                    {
                        nodeConf = null;
                        ConfInstID = "";
                        confUID = "";
                        confname = "";
                        nodeConftype = null;
                        ConferenceType = 0;
                        nodeStatus = null;
                        ConferenceStatus = "";
                        nodeLastRun = null;
                        isTextMsg = "";
                        iCALNodes = null;
                        ConfTxtMessage = "";
                        ProcessStatus = 0; // ZD 100676


                        nodeConf = nodes[cnfcnt].SelectSingleNode("ConferenceID");
                        ConfInstID = nodeConf.InnerXml.Trim();
                        confUID = nodes[cnfcnt].SelectSingleNode("ConferenceUniqueID").InnerText.Trim();
                        confname = nodes[cnfcnt].SelectSingleNode("ConferenceName").InnerText.Trim();
                        nodeConftype = nodes[cnfcnt].SelectSingleNode("ConferenceType");
                        Int32.TryParse(nodeConftype.InnerXml.Trim(), out ConferenceType);

                        nodeStatus = nodes[cnfcnt].SelectSingleNode("ConferenceActualStatus");
                        ConferenceStatus = nodeStatus.InnerXml.Trim();
                        nodeLastRun = nodes[cnfcnt].SelectSingleNode("LastRunDate");
                        lstrun = nodeLastRun.InnerXml.Trim();

                        //FB 2501 Starts
                        StartMode = 0;
                        nodeLastRun = nodes[cnfcnt].SelectSingleNode("CallStartMode");
                        if (nodeLastRun != null)
                            int.TryParse(nodeLastRun.InnerXml.Trim(), out StartMode);
                        //FB 2501 Ends


                        // ZD 100676
                        if (nodes[cnfcnt].SelectSingleNode("CallProcessState") != null)
                            int.TryParse(nodes[cnfcnt].SelectSingleNode("CallProcessState").InnerXml.Trim(), out ProcessStatus);

                        if (ProcessStatus > 0)
                        {
                            log.Trace("Skipping launching call as it is already in process:" + ConfInstID);
                            continue;
                        }

                        // ZD 100676

                        iCALNodes = nodes[cnfcnt].SelectNodes("descendant::terminal[isCalendarInvite='1']");
                        if (iCALNodes != null)
                            if (iCALNodes.Count > 0)
                                continue;

                        drLaunch = null;
                        drLaunch = _dtConfEPTs.NewRow();
                        drLaunch["confid"] = ConfInstID;
                        drLaunch["ConfType"] = ConferenceType.ToString();
                        drLaunch["confUniqueID"] = confUID;
                        drLaunch["confName"] = confname;
                        drLaunch["rowType"] = "M";
                        if (ConferenceStatus == "0") //FB 2501  0 - Automatic  1 - Manual
                        {
                            drLaunch["rowType"] = "L"; //Call needs to be launched into MCU
                            if (StartMode == 0)
                                arrRows.Add(drLaunch);
                        }

                        if (arrRows.Count > 0) // create seperate threads for launching calls
                        {
                            log.Trace("New thread started for launching call: " + ConfInstID + " on " + DateTime.Now.ToLocalTime());
                            launchThrd = new Thread(LaunchOngoing);
                            launchThrd.CurrentCulture = globalCultureInfo; //FB 2545
                            launchThrd.SetApartmentState(ApartmentState.MTA);
                            launchThrd.Start(arrRows);
                            arrRows = new List<DataRow>();
                        }

                        //To fetch endpoint status
                        XmlNodeList nodesEP = nodes[cnfcnt].SelectNodes("//SearchConference/Conferences/Conference/terminals/terminal");

                        for (int eptcnt = 0; eptcnt < nodesEP.Count; eptcnt++)
                        {
                            drStatus = null;
                            drStatus = _dtConfEPTs.NewRow();
                            drStatus["confid"] = ConfInstID;
                            drStatus["confUniqueID"] = confUID;
                            drStatus["ConfType"] = ConferenceType.ToString();
                            drStatus["endpointID"] = nodesEP[eptcnt].SelectSingleNode("endpointID").InnerText.Trim();
                            drStatus["EPTTerminaltype"] = nodesEP[eptcnt].SelectSingleNode("type").InnerText.Trim();

                            if (drLaunch["rowType"].ToString() == "M")
                            {
                                arrUpdtRows.Add(drStatus);
                            }
                        }
                        if (arrUpdtRows.Count > 0)
                        {
                            log.Trace(" Endpoint Cnt: " + arrUpdtRows.Count);
                            log.Trace("New thread started for fetching endpoint status: " + ConfInstID + " on " + DateTime.Now.ToLocalTime());
                            updateThrd = new Thread(UpdateConfStatus);
                            updateThrd.SetApartmentState(ApartmentState.MTA);
                            updateThrd.CurrentCulture = globalCultureInfo; //FB 2545 
                            updateThrd.Start(arrUpdtRows);
                            arrUpdtRows = new List<DataRow>();
                        }
                        
                    }
                }
                else
                {
                    log.Trace("Number of ongoing calls: 0");
                    log.Trace("Exit GetConfs Method....");
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + ex.ToString());
            }
            finally
            {
                //com = null;
            }
        }
        #endregion

        //FB 2595 Start

        #region GetSwitchingConfs
        /// <summary>
        /// GetSwitchingConfs
        /// </summary>
        /// <param name="pinXML"></param>
        private void GetSwitchingConfs(String pinXML)
        {
            int CallNetworkState = 0;
            try
            {
                log.Trace("Into GetAllConfs:" + DateTime.Now.ToLocalTime());
                obj = new VRMRTC.VRMRTC();

                if (myvrmCom == null)
                    myvrmCom = new ASPIL.VRMServer();

                if (pinXML.IndexOf("<error>") > 0)
                    return;
                if (_dtConfEPTs == null)
                    CreateEPTDataTable();
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(pinXML);
                XmlNodeList nodes = xmldoc.SelectNodes("//SearchConference/Conferences/Conference");
                if (nodes != null)
                {
                    log.Trace("Number of SwitchingConfs Conferences:" + nodes.Count.ToString() + "... Time: " + DateTime.Now.ToLocalTime());

                    List<DataRow> SecureRows = new List<DataRow>();
                    List<DataRow> UnSecureRows = new List<DataRow>();
                    XmlNode nodeConf = null;
                    String ConfInstID = "";
                    string confUID = "";
                    Int32 ConferenceType = 0;
                    int CallRequestTimeout = 0;//FB 2993
                    XmlNode nodeConftype = null;
                    XmlNode nodeConfswitch = null;
                    DataRow drLaunch = null;
                    XmlNode nodeRequestTime = null;

                    for (int cnfcnt = 0; cnfcnt < nodes.Count; cnfcnt++)
                    {
                        nodeConf = null;
                        ConfInstID = "";
                        confUID = "";
                        nodeConftype = null;
                        ConferenceType = 0;
                        nodeConfswitch = null;
                        CallNetworkState = 0;
                        CallRequestTimeout = 0;//FB 2993

                        nodeConf = nodes[cnfcnt].SelectSingleNode("ConferenceID");
                        ConfInstID = nodeConf.InnerXml.Trim();
                        confUID = nodes[cnfcnt].SelectSingleNode("ConferenceUniqueID").InnerText.Trim();
                        nodeConftype = nodes[cnfcnt].SelectSingleNode("ConferenceType");
                        Int32.TryParse(nodeConftype.InnerXml.Trim(), out ConferenceType);

                        CallNetworkState = 0;
                        nodeConfswitch = nodes[cnfcnt].SelectSingleNode("CallNetworkState");
                        if (nodeConfswitch != null)
                            int.TryParse(nodeConfswitch.InnerXml.Trim(), out CallNetworkState);

                        nodeRequestTime = nodes[cnfcnt].SelectSingleNode("CallRequestTimeout");//FB 2993
                        if (nodeConfswitch != null)
                            int.TryParse(nodeRequestTime.InnerXml.Trim(), out CallRequestTimeout);

                        drLaunch = null;
                        drLaunch = _dtConfEPTs.NewRow();
                        drLaunch["confid"] = ConfInstID;
                        drLaunch["ConfType"] = ConferenceType.ToString();
                        drLaunch["confUniqueID"] = confUID;
                        drLaunch["NetworkState"] = CallNetworkState;
                        drLaunch["OrgRequestTime"] = CallRequestTimeout;//FB 2993

                        if (CallNetworkState == 1)
                            SecureRows.Add(drLaunch);
                        else
                            UnSecureRows.Add(drLaunch);

                    }

                    if (SecureRows.Count > 0) // create seperate threads for Checking Secured calls
                    {
                        log.Trace("New thread for Secured call: " + ConfInstID + " on " + DateTime.Now.ToLocalTime());
                        FetchSwitchingCallsThrd = new Thread(SwitchOngoingCall);
                        FetchSwitchingCallsThrd.CurrentCulture = globalCultureInfo;
                        FetchSwitchingCallsThrd.SetApartmentState(ApartmentState.MTA);
                        FetchSwitchingCallsThrd.Start(SecureRows);
                        SecureRows = new List<DataRow>();
                    }
                    Thread.Sleep(8000);
                    if (UnSecureRows.Count > 0) // create seperate threads for Checking Unsecured calls
                    {
                        log.Trace("New thread for Unsecured call: " + ConfInstID + " on " + DateTime.Now.ToLocalTime());
                        FetchSwitchingCallsThrd = new Thread(SwitchOngoingCall);
                        FetchSwitchingCallsThrd.CurrentCulture = globalCultureInfo;
                        FetchSwitchingCallsThrd.SetApartmentState(ApartmentState.MTA);
                        FetchSwitchingCallsThrd.Start(UnSecureRows);
                        UnSecureRows = new List<DataRow>();
                    }
                }
                else
                {
                    log.Trace("Number of ongoing switching calls: 0");
                    log.Trace("Exit GetswitchingConfs Method....");
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + ex.ToString());
            }
            finally
            {
                //com = null;
            }
        }
        #endregion

        //FB 2595 End
        
        #region LaunchOngoing Threads
        private void LaunchOngoing(Object objlaunchrows)
        {
            List<DataRow> launchrows = null;
            try
            {
                if (objlaunchrows != null)
                {
                    obj = new VRMRTC.VRMRTC();
                    launchrows = (List<DataRow>)objlaunchrows;
                    foreach (DataRow dr in launchrows)
                    {
                        if (dr["confUniqueID"].ToString() != "")
                        {
                            if (dr["ConfType"].ToString().Equals(vrmConfType.P2P.ToString().Trim()) || dr["ConfType"].ToString().Equals(vrmConfType.AudioVideo.ToString().Trim()))
                            {
                                log.Trace("Invoking SetConferenceOnMcu" + " Call Type: " + dr["ConfType"].ToString() + " ConfID: " + dr["confid"].ToString() + " Time: " + DateTime.Now.ToLocalTime());

                                string inXML = "";
                                inXML = "<Conference><confID>" + dr["confid"].ToString() + "</confID><FromService>1</FromService></Conference>";//FB 2441
                                string outXML = "";
                                outXML = obj.Operations(RTC_ConfigPath, "SetConferenceOnMcu", inXML);

                                log.Trace("Exits SetConferenceOnMcu" + " Call Type: " + dr["ConfType"].ToString() + " ConfID: " + dr["confid"].ToString() + " Time: " + DateTime.Now.ToLocalTime());
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + ex.ToString());
            }
        }
        #endregion

        #region UpdateConfStatus Thread

        private void UpdateConfStatus(Object objupdtrows)
        {
            String inXML = "";
            String outXML = "";
            List<DataRow> Updaterows = null;
            try
            {
                if (objupdtrows != null)
                {
                    Updaterows = (List<DataRow>)objupdtrows;
                    obj = new VRMRTC.VRMRTC();

                    foreach (DataRow dr in Updaterows)
                    {
                        if (dr["confUniqueID"].ToString() != "")
                        {
                            if (dr["ConfType"].ToString().Equals(vrmConfType.P2P.ToString().Trim()) || dr["ConfType"].ToString().Equals(vrmConfType.AudioVideo.ToString().Trim()))
                            {
                                inXML = "<GetTerminalStatus>";  //FB 1552
                                inXML += "  <login>";
                                inXML += "      <userID>11</userID>";
                                inXML += "      <confID>" + dr["confid"].ToString() + "</confID>";
                                inXML += "      <endpointID>" + dr["EndpointID"].ToString() + "</endpointID>";
                                inXML += "      <terminalType>" + dr["EPTTerminaltype"].ToString() + "</terminalType>";
                                inXML += "  </login>";
                                inXML += "</GetTerminalStatus>";

                                log.Trace("Invoking GetTerminalStatus" + " Call Type: " + dr["ConfType"].ToString() + " ConfId: " + dr["confid"].ToString() + " Time: " + DateTime.Now.ToLocalTime());
                                outXML = obj.Operations(RTC_ConfigPath, "GetTerminalStatus", inXML);
                                log.Trace("Exits GetTerminalStatus" + " Call Type: " + dr["ConfType"].ToString() + " ConfId: " + dr["confid"].ToString() + " Time: " + DateTime.Now.ToLocalTime());
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + ex.ToString());
            }
        }
        #endregion

        #region ValidateServerCertificate
        public bool ValidateServerCertificate(
      object sender,
      X509Certificate certificate,
      X509Chain chain,
      System.Net.Security.SslPolicyErrors sslPolicyErrors)
        {
            //if (sslPolicyErrors == SslPolicyErrors.None) allow  client to communicate with authenticated servers.
            //    return true;
            // To allow client to communicate with unauthenticated servers.
            return true;
        }
        #endregion

        #region OnStop
        protected override void OnStop()
        {
            timerActivation.Enabled = false;
            timerActivation.AutoReset = false;
            timerActivation.Stop();

            timerLaunchandUpdt.Enabled = false;
            timerLaunchandUpdt.AutoReset = false;
            timerLaunchandUpdt.Stop();

            timerP2PLaunchUpdt.Enabled = false; //FB 2434
            timerP2PLaunchUpdt.AutoReset = false;
            timerP2PLaunchUpdt.Stop();

            timerP2PTerminate.Enabled = false;
            timerP2PTerminate.AutoReset = false;
            timerP2PTerminate.Stop();

            myvrmCom = null;
        }
        #endregion

        //FB 2560 Start
        #region UpdateConfMesseageStatus Thread
        /// <summary>
        /// UpdateConfMesseageStatus
        /// </summary>
        /// <param name="objupdtrows"></param>
        private void UpdateConfMesseageStatus(Object objupdtrows)
        {
            String inXML = "";
            String outXML = "";
            List<DataRow> Updaterows = null;
            try
            {
                if (objupdtrows != null)
                {
                    Updaterows = (List<DataRow>)objupdtrows;
                    obj = new VRMRTC.VRMRTC();

                    foreach (DataRow dr in Updaterows)
                    {
                        if (dr["confUniqueID"].ToString() != "")
                        {
                            if (dr["ConfType"].ToString().Equals(vrmConfType.AudioOnly.ToString().Trim()) || dr["ConfType"].ToString().Equals(vrmConfType.AudioVideo.ToString().Trim()))
                            {
                                inXML = "  <login>";
                                inXML += "      <userID>11</userID>";
                                inXML += "      <confID>" + dr["confid"].ToString() + "</confID>";
                                inXML += "      <messageText>" + dr["ConfTxtMessage"].ToString() + "</messageText>";
                                inXML += "  </login>";

                                log.Trace("Invoking SendMessageToConference" + " ConfId: " + dr["confid"].ToString() + " Time: " + DateTime.Now.ToLocalTime() + "ConfMessage:" + dr["ConfTxtMessage"].ToString());
                                outXML = obj.Operations(RTC_ConfigPath, "SendMessageToConference", inXML);
                                log.Trace("Exits SendMessageToConference" + " ConfId: " + dr["confid"].ToString() + " Time: " + DateTime.Now.ToLocalTime() + "ConfMessage:" + dr["ConfTxtMessage"].ToString());
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + ex.ToString());
            }
        }

        #endregion
        //FB 2560 End

        //FB 2595 Start

        #region SwitchOngoingCall Threads
        private void SwitchOngoingCall(Object objlaunchrows)
        {
            List<DataRow> launchrows = null;
            try
            {
                if (objlaunchrows != null)
                {
                    obj = new VRMRTC.VRMRTC();
                    launchrows = (List<DataRow>)objlaunchrows;
                    foreach (DataRow dr in launchrows)
                    {
                        if (dr["confUniqueID"].ToString() != "")
                        {
                            if (dr["ConfType"].ToString().Equals(vrmConfType.P2P.ToString().Trim()) || dr["ConfType"].ToString().Equals(vrmConfType.AudioVideo.ToString().Trim()))
                            {
                                string inXML = "";
                                inXML = "<Conference><confID>" + dr["confid"].ToString() + "</confID><NetworkState>" + dr["NetworkState"].ToString() + "</NetworkState><CallRequestTimeout>" + dr["OrgRequestTime"].ToString() + "</CallRequestTimeout></Conference>";//FB 2993
                                string outXML = "";
                                outXML = obj.Operations(RTC_ConfigPath, "SetSwitchingtoConf", inXML);

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + ex.ToString());
            }
        }
        #endregion

        //FB 2595 End

        //ZD #100093 & ZD #100085 Start

        #region timerCompletedCalls_Elapsed
        void timerCompletedCalls_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            double Switchopt = 40000;
            try
            {
                log.Trace("Entering timerCompletedCalls_Elapsed block...." + DateTime.Now.ToLocalTime());
                UpdateCompletedConferences();
                timerUpdateCompletedCalls.Interval = Switchopt;
                timerUpdateCompletedCalls.Enabled = true;
                timerUpdateCompletedCalls.AutoReset = true;
                timerUpdateCompletedCalls.Start();
                log.Trace("Timer event timerCompletedCalls started...." + DateTime.Now.ToLocalTime());

            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + ex.ToString());

            }
        }
        #endregion

        #region UpdateCompletedConferences
        /// <summary>
        /// UpdateCompletedConferences
        /// </summary>
        /// <param name="conftype"></param>
        /// if conftype = 2 video; 4 p2p; 6 audio; 7 room; 1 all
        /// need to code for launching audio calls
        private void UpdateCompletedConferences()
        {
            try
            {
                log.Trace("Into UpdateCompletedConferences... " + DateTime.Now.ToLocalTime());
                if (myvrmCom == null)
                    myvrmCom = new ASPIL.VRMServer();

                String searchConfInXML = "<SearchConference><UserID>11</UserID></SearchConference>"; //FB 2434
                String searchConfOutXML = myvrmCom.Operations(MyVRMServer_ConfigPath, "UpdateCompletedConference", searchConfInXML);
                log.Trace("UpdateCompletedConferences OUTXML : " + searchConfOutXML);
                if (searchConfOutXML.IndexOf("<success>") >= 0)
                    log.Trace("UpdateCompletedConference Lauched:" + DateTime.Now.ToString("F"));

                log.Trace("After UpdateCompletedConference command:" + DateTime.Now.ToString("F"));
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + ex.ToString());
            }
        }
        #endregion

        //ZD #100093 & ZD #100085 End

        //ZD 104846 start
        #region timerAutoPurgelogs_Elapsed
        /// <summary>
        /// timerAutoPurgelogs_Elapsed
        /// </summary>
        void timerAutoPurgelogs_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                System.Threading.Thread.CurrentThread.CurrentCulture = globalCultureInfo;
                log.Trace("Purge Logs:" + DateTime.Now.ToString("F"));
                timerAutoPurgelogs.AutoReset = false;
                GetSitePurgeLogDuartion();
                PurgeLogs();
                System.Threading.Thread.Sleep(5000);
                timerAutoPurgelogs.AutoReset = true; ;
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);
            }
        }

        #endregion

        #region GetSitePurgeLogDuartion
        /// <summary>
        /// GetSitePurgeLogDuartion()
        /// </summary>
        private void GetSitePurgeLogDuartion()
        {
            XmlDocument xmldoc = new XmlDocument();
            try
            {
                log.Trace("Into GetSitePurgeLogDuartion... " + DateTime.Now.ToLocalTime());
                if (myvrmCom == null)
                    myvrmCom = new ASPIL.VRMServer();
                string confInXML = "<login><userID>11</userID></login>";
                string confOutXML = myvrmCom.Operations(MyVRMServer_ConfigPath, "GetSuperAdmin", confInXML);
                xmldoc.LoadXml(confOutXML);
                XmlNode node = (XmlNode)xmldoc.DocumentElement;
                if (node.SelectSingleNode("//preference/AutoPurgeLogDuration") != null)
                    int.TryParse(node.SelectSingleNode("//preference/AutoPurgeLogDuration").InnerText, out purgeDuration);
            }

            catch (Exception ex)
            {
                log.Trace(ex.Message);
            }
        }
        #endregion

        #region PurgeLogs
        /// <summary>
        /// PurgeLogs
        /// </summary>
        private void PurgeLogs()
        {
            string[] Files = null;
            try
            {
                if (Directory.Exists(MyVRMServer_ConfigPath + "\\MaintenanceLogs"))
                {
                    Files = Directory.GetFiles(MyVRMServer_ConfigPath + "\\MaintenanceLogs");
                    for (int i = 0; i < Files.Length; i++)
                    {
                        FileInfo fi = new FileInfo(Files[i]);
                        if (DateTime.UtcNow - fi.CreationTimeUtc > TimeSpan.FromDays(purgeDuration))
                            fi.Delete();

                    }
                }
                if (Directory.Exists(MyVRMServer_ConfigPath + "\\RTCLogs"))
                {
                    Files = Directory.GetFiles(MyVRMServer_ConfigPath + "\\RTCLogs");
                    for (int i = 0; i < Files.Length; i++)
                    {
                        FileInfo fi = new FileInfo(Files[i]);
                        if (DateTime.UtcNow - fi.CreationTimeUtc > TimeSpan.FromDays(purgeDuration))
                            fi.Delete();
                    }
                }

                log.Trace("Deleted Reminder Service  Logs");
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);
            }
        }
        #endregion

        //ZD 104846 End


    }
}
