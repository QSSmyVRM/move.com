﻿//ZD 100147 Start
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886 End
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
//FB 2556 NEW CLASS ADDED
namespace NS_RADVISION_IView
{

    #region References
    using System;
    using System.Collections;
    using System.Text;
    using System.Net;
    using System.Net.Sockets;
    using System.IO;
    using System.Xml;
    #endregion

    class RadVisionIView
    {
        private NS_LOGGER.Log logger;
        internal string errMsg = null;
        private NS_MESSENGER.ConfigParams configParams;
        internal bool isRecurring = false;
        private NS_DATABASE.Database db;
        List<NS_MESSENGER.Conference> confList = null;
        NS_MESSENGER.Conference globalConf = null;// ZD 101004
        int iHavetoTry = 2, iHaveTried = 0;// ZD 101004
        bool isEdit = false;// ZD 101004

        public class PartyDetails
        {
            public string sAddress { get; set; }
            public string GUID { get; set; }
            public string Response { get; set; }
            public string Onlinestatus { get; set; }
        }

        internal RadVisionIView(NS_MESSENGER.ConfigParams config)
        {
            configParams = new NS_MESSENGER.ConfigParams();
            configParams = config;
            logger = new NS_LOGGER.Log(configParams);
            db = new NS_DATABASE.Database(configParams);
        }

        #region Command

        #region  GetConferenceList
        /// <summary>
        /// GetConferenceList
        /// </summary>
        /// <param name="Conf"></param>
        /// <returns></returns>
        internal bool GetConferenceList(NS_MESSENGER.Conference Conf)
        {
            string inXML = null, responseXML = null;
            bool ret = false;
            try
            {
                logger.Trace("Entering into GetConferenceList Command....");

                ret = CreateXML_GetConferenceList(Conf, ref inXML);


                ret = false;
                ret = SendCommand(Conf.cMcu, inXML, ref responseXML);
                if (!ret) return false;

                ret = false;
                ret = ResponseXML_GetConferenceList(Conf, responseXML);
                if (!ret) return false;

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        
        #endregion

        #region  TestMCUConnection
        /// <summary>
        /// To Test MCU Connection
        /// </summary>
        /// <param name="mcu"></param>
        /// <returns></returns>
        internal bool TestMCUConnection(NS_MESSENGER.MCU mcu)
        {
            string KeepAliveXML = null, responseXML = null;
            bool ret = false;
            try
            {
                logger.Trace("Entering into Test MCU Connection Command....");

                ret = CreateXML_KeepAlive(ref KeepAliveXML);

                ret = false;
                ret = SendCommand(mcu, KeepAliveXML, ref responseXML);
                if (!ret) return false;

                ret = false;
                ret = ResponseXML_KeepAlive(responseXML);
                if (!ret) return false;

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        #endregion
		//ZD 100518 Starts
        internal bool CheckConferenceCalls(DateTime StartDateTime, DateTime EndDateTime, int editConfId, int orgId, NS_MESSENGER.MCU MCU, int TimeZoneID, int MeetingId, string StaticID) //ZD 100890
        {
            string inXML = "", outXML = "";
            bool ret = false;
            try
            {
                ret = Poll_CreateXML_GetConferenceList(StartDateTime, EndDateTime, MCU, editConfId, orgId, ref inXML, TimeZoneID);
                
                SendCommand(MCU, inXML, ref outXML);
                if (!ret)
                {
                    return false; 
                }

                ret = Poll_ResponseXML_GetConferenceList(StartDateTime, EndDateTime, MCU, editConfId, orgId, outXML, TimeZoneID, MeetingId, StaticID);
                if (!ret)
                {
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                logger.Exception(100, ex.Message);
                return false;
            }
        }
		//ZD 100518 Ends
        #region CreateDeleteUpdate
        /// <summary>
        /// CreateDeleteUpdate
        /// </summary>
        /// <param name="templateConf"></param>
        /// <returns></returns>
        internal bool CreateDeleteUpdate(NS_MESSENGER.Conference templateConf)
        {
            bool ret = false;
            int cnfCnt = 0;
            List<NS_MESSENGER.Conference> confList = null;
            List<NS_MESSENGER.Conference> confs = new List<NS_MESSENGER.Conference>();
            try
            {
                confList = new List<NS_MESSENGER.Conference>();
                confList.Add(templateConf);

                if (templateConf.iRecurring == 1 && isRecurring)
                {
                    ret = false;
                    ret=DeleteSyncConference(templateConf);
                    if (!ret)
                        logger.Trace("DeleteSyncconf failed");
                    else
                    {
                        db.FetchConf(ref confList, ref confs, NS_MESSENGER.MCU.eType.RADVISIONIVIEW, 0);//for new and edit conference need to send delStatus as 0 //ZD 100221
                        confList = confs;
                    }
                }

                for (cnfCnt = 0; cnfCnt < confList.Count; cnfCnt++)
                {
                    if (confList[cnfCnt].sGUID == "")
                    {
                        ret = false;
                        ret = SetConference(confList[cnfCnt]);
                        if (!ret)
                            logger.Trace("Setconference Failed");
                       
                    }
                    else
                        if (confList[cnfCnt].sGUID != "")
                        {
                            ret = false;
                            ret=ModifyConference(confList[cnfCnt]);
                            if (!ret)
                                logger.Trace("Modify Conference Failed");
                        }
                        
                }
                return true;
            }
            catch (Exception ex)
            {
                logger.Exception(100, ex.Message);
                return false;
            }
        }


        #endregion

        #region SetConference
        internal bool SetConference(NS_MESSENGER.Conference conf)
        {
            string reservationXML = null;
            bool ret = false;
            string responseXML = null;
            string confGID = null;
            NS_EMAIL.Email sendEmail = null;
            int isPushtoExternal = 0; //ZD 100190
            try
            {
                logger.Trace("Entering into Set Conference Command....");

                globalConf = conf;// ZD 101004

                ret = CreateXML_CreateConference(conf, ref reservationXML);
                if (!ret) return false;

                ret = SendCommand(conf.cMcu, reservationXML, ref responseXML);
                if (!ret)
                {
                    //ZD 100190
                    isPushtoExternal = 2;
                    db.UpdatePushToExternal(conf.iDbID, conf.iInstanceID, isPushtoExternal);
                    logger.Exception(100, "SetConference SendTransaction failed");
                    sendEmail = new NS_EMAIL.Email(configParams);
                    sendEmail.SendEmailtoHostAndMCUAdmin(conf, this.errMsg);
                    return false;
                }

                ret = false; string confDialNumber = null;
                ret = ResponseXML_CreateConference(conf, responseXML, ref confGID, ref confDialNumber);
                if (!ret) 
                    return false;

                return true;
            }
            catch (Exception ex)
            {
                logger.Exception(100, ex.Message);
                return false;
            }
        }

        #endregion

        #region ModifyConference
        internal bool ModifyConference(NS_MESSENGER.Conference Conf)
        {
            string reservationXML = null;
            bool ret = false;
            string responseXML = null;
            string confGID = null;
            try
            {
                    logger.Trace("Entering into Modify Conference Command....");

                    ret = CreateXML_ModifyConference(Conf, ref reservationXML);
                    if (!ret) return false;

                    ret = SendCommand(Conf.cMcu, reservationXML, ref responseXML);
                    if (!ret) return false;

                    ret = false; string confDialNumber = null;
                    ret = ResponseXML_ModifyConference(Conf, responseXML, ref confGID, ref confDialNumber);
                    if (!ret) return false;
                    //ret = db.UpdateSyncStatus(Conf);
                    //if (!ret)
                    //    logger.Exception(100, "Delete failed in confSyncMCUAdj");
                    return true;
            }
            catch (Exception ex)
            {
                logger.Exception(100, ex.Message);
                return false;
            }
        }
        #endregion

        #region SetConference for reccurence
        /// <summary>
        /// SetConference for reccurence
        /// </summary>
        /// <param name="conf"></param>
        /// <returns></returns>
        internal bool SetRecurringConference(NS_MESSENGER.Conference templateConf)
        {
            logger.Trace("Entering SetConference function...");
            int cnfCnt = 0;
            NS_EMAIL.Email sendEmail = null;
            List<NS_MESSENGER.Conference> confList = null;
            bool ret = false;
            List<NS_MESSENGER.Conference> confs = new List<NS_MESSENGER.Conference>();
            try
            {
                sendEmail = new NS_EMAIL.Email(configParams);
                ret = false;

                confList = new List<NS_MESSENGER.Conference>();
                confList.Add(templateConf);

                if (templateConf.iRecurring == 1 && isRecurring)
                {
                    DeleteSyncConference(templateConf);
                    db.FetchConf(ref confList, ref confs, NS_MESSENGER.MCU.eType.RADVISIONIVIEW, 0);//for new and edit conference need to send delStatus as 0 //ZD 100221
                    confList = confs;
                }

                for (cnfCnt = 0; cnfCnt < confList.Count; cnfCnt++)
                {
                    ret = SetConference(confList[cnfCnt]);
                }
                return ret;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion

        #region ExtendConfEndTime
        /// <summary>
        /// ExtendConfEndTime
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="ExtendTime"></param>
        /// <returns></returns>
        internal bool ExtendConfEndTime(NS_MESSENGER.Conference conf, int ExtendTime)
        {
            bool ret = false;
            string ExtendXML = null, responseXML = null;
            try
            {
                logger.Trace("Entering into Extend Conference Endtime Command....");

                ret = CreateXML_ExtendConfEndTime(conf, ExtendTime, ref ExtendXML);
                if (!ret) return false;

                ret = SendCommand(conf.cMcu, ExtendXML, ref responseXML);
                if (!ret) return false;

                ret = ResponseXML_ExtendConfEndTime(responseXML);
                if (!ret) return false;

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }

        #endregion

        # region AddPartyToMcu
        /// <summary>
        /// AddPartyToMcu
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="party"></param>
        /// <returns></returns>
        internal bool AddPartyToMcu(NS_MESSENGER.Conference conf, NS_MESSENGER.Party party)
        {
            bool ret = false;
            StringBuilder participantXML = null;
            string responseXML = null;
            try
            {
                logger.Trace("Entering into Add Participant Command....");

                ret = CreateXML_AddParticipant(conf, party, ref participantXML);
                if (!ret) return false;

                ret = false; responseXML = null;
                ret = SendCommand(conf.cMcu, participantXML.ToString(), ref responseXML);
                if (!ret) return false;

                ret = false;
                ret = ResponseXML_AddParticipant(conf, responseXML);
                if (!ret) return false;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }

            return true;
        }
        #endregion

        #region Connect Disconnect Endpoint
        /// <summary>
        /// ConnectDisconnectEndpoint
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="party"></param>
        /// <param name="connectDisconnect"></param>
        /// <returns></returns>
        internal bool ConnectDisconnectEndpoint(NS_MESSENGER.Conference conf, NS_MESSENGER.Party party, bool connectDisconnect)
        {
            bool ret = false;
            StringBuilder requestXML = null;
            string responseXML = null;
            int onlineStatus = -1;
            try
            {
                logger.Trace("Entering into ConnectDisconnect Endpoint Command....");

                if (party.sGUID == null)
                    party.sGUID = "";

                if (connectDisconnect)
                {
                    if (party.sGUID != "")
                        ret = CreateXML_connectEndpoint(conf, party, ref requestXML);
                    else
                        ret = CreateXML_AddParticipant(conf, party, ref requestXML);

                    if (!ret)
                        return false;
                }
                else
                {
                    ret = CreateXML_DisconnectEndpoint(conf, party, ref requestXML);
                    if (!ret) return false;
                }

                ret = false;
                ret = SendCommand(party.cMcu, requestXML.ToString(), ref responseXML);
                if (!ret) return false;


                if (connectDisconnect)
                {
                    if (party.sGUID != "")
                        ret = ResponseXML_connectEndpoint(responseXML);
                    else
                        ret = ResponseXML_AddParticipant(conf, responseXML);
                }
                else
                {
                    ret = ResponseXML_DisconnectEndpoint(responseXML, ref onlineStatus);
                    if (ret)
                    {
                        db.UpdateConferenceEndpointStatus(conf.iDbID, conf.iInstanceID, party.sAddress, party.sGUID, onlineStatus);
                    }
                    else
                        return false;
                }
                return true;
            }
            catch (Exception e)
            {


                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }


        #endregion

        #region TerminateConference
        /// <summary>
        /// TerminateConference
        /// </summary>
        /// <param name="conf"></param>
        /// <returns></returns>
        internal bool TerminateConference(NS_MESSENGER.Conference conf)
        {
            string terminateXML = null, responseXML = null;
            bool ret = false;
            confList = new List<NS_MESSENGER.Conference>();
            List<NS_MESSENGER.Conference> confs = new List<NS_MESSENGER.Conference>();
           
            try
            {
                logger.Trace("Entering into Terminate Conference Command....");
                confList.Add(conf);
                if (conf.iRecurring == 1 && isRecurring)
                {
                    db.FetchConf(ref confList, ref confs, NS_MESSENGER.MCU.eType.RADVISIONIVIEW, 1); //for delete mode need to send delStatus as 1 //ZD 100221
                    confList = confs;
                }
                for (int cnfCnt = 0; cnfCnt < confList.Count; cnfCnt++)
                {
                    confList[cnfCnt].cMcu = conf.cMcu;
                    ret = CreateXML_TerminateConference(confList[cnfCnt].sGUID, ref terminateXML);
                    
                    if (!ret)
                    {
                        logger.Trace("Error in Generating Terminate XML");
                        continue;
                    }

                    ret = false; responseXML = "";
                    ret = SendCommand(confList[cnfCnt].cMcu, terminateXML, ref responseXML);
                    if (!ret)
                    {
                        logger.Trace("Error in send command");
                        continue;
                    }

                    ret = false;
                    ret = ResponseXML_TerminateConference(responseXML);
                    if (!ret)
                    {
                        logger.Trace("Error in processing Terminate Response");
                        continue;
                    }

                    //ZD 100221 Starts
                    if (confList[cnfCnt].iWebExconf != 1)
                    {
                        ret = false;
                        ret = db.UpdateSyncStatus(confList[cnfCnt]);
                        if (!ret)
                            logger.Exception(100, "Delete failed in confSyncMCUAdj");
                    }
                    //ZD 100221 Ends
                }

                return true;
            }
            catch (Exception ex)
            {
                logger.Exception(100, ex.Message);
                return false;
            }

        }
        #endregion

        //FB 2659 START
        //ZD 100190 Starts
        #region CancelConference
        /// <summary>
        /// CancelConference
        /// </summary>
        /// <param name="conf"></param>
        /// <returns></returns>
        internal bool CancelConference(NS_MESSENGER.Conference conf)
        {
            string CancelXML = null, responseXML = null;
            bool ret = false;
            confList = new List<NS_MESSENGER.Conference>();

            List<NS_MESSENGER.Conference> confs = new List<NS_MESSENGER.Conference>();

            try
            {
                logger.Trace("Entering into Cancel Conference Command....");
                confList.Add(conf);
                if (conf.iRecurring == 1 && isRecurring)
                {
                    db.FetchConf(ref confList, ref confs,NS_MESSENGER.MCU.eType.RADVISIONIVIEW, 1); //for delete mode need to send delStatus as 1 //ZD 100221
                    confList = confs;
                }
                for (int cnfCnt = 0; cnfCnt < confList.Count; cnfCnt++)
                {
                    confList[cnfCnt].cMcu = conf.cMcu;
                    ret = CreateXML_CancelConference(confList[cnfCnt].sGUID, ref CancelXML);
                    if (!ret) return false;

                    ret = false;
                    ret = SendCommand(confList[cnfCnt].cMcu, CancelXML, ref responseXML);
                    if (!ret) return false;

                    ret = false;
                    ret = ResponseXML_CancelConference(responseXML);
                    if (!ret) return false;

                    //ZD 100221 Starts
                    if (confList[cnfCnt].iWebExconf != 1)
                    {
                        ret = false;
                        ret = db.UpdateSyncStatus(confList[cnfCnt]);
                        if (!ret)
                            logger.Exception(100, "Delete failed in confSyncMCUAdj");
                    }
                    //ZD 100221 Ends
                }

                return true;
            }
            catch (Exception ex)
            {
                logger.Exception(100, ex.Message);
                return false;
            }

        }
        #endregion
        //ZD 100190 End
        //FB 2659 END

        #region TerminateEndpoint
        /// <summary>
        /// TerminateEndpoint
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="party"></param>
        /// <returns></returns>
        internal bool TerminateEndpoint(NS_MESSENGER.Conference conf, NS_MESSENGER.Party party)
        {
            string terminateXML = null, responseXML = null;
            bool ret = false;
            try
            {
                logger.Trace("Entering into Terminate Endpoint Command....");

                ret = CreateXML_TerminateEndpoint(conf, party, ref terminateXML);
                if (!ret) return false;

                ret = false;
                ret = SendCommand(party.cMcu, terminateXML, ref responseXML);
                if (!ret) return false;

                ret = false;
                ret = ResponseXML_TerminateEndpoint(responseXML);
                if (!ret) return false;

                return true;
            }
            catch (Exception e)
            {


                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }


        #endregion

        #region Conference Lock/Unlock
        /// <summary>
        /// LockUnlockConference
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="lockorunlock"></param>
        /// <returns></returns>
        internal bool LockUnlockConference(NS_MESSENGER.Conference conf, int lockorunlock)
        {
            bool LockUnLock = false;
            bool ret = false;
            StringBuilder LockUnLockXML = new StringBuilder();
            string responseXML = null;
            try
            {
                logger.Trace("Entering into Lock/Unlock Conference Command....");

                if (lockorunlock == 1)
                    LockUnLock = true;
                else
                    LockUnLock = false;
                ret = CreateXML_LockUnlockConference(conf, LockUnLock, ref LockUnLockXML);
                if (!ret) return false;

                ret = false;
                ret = SendCommand(conf.cMcu, LockUnLockXML.ToString(), ref responseXML);
                if (!ret) return false;

                ret = false;
                ret = ResponseXML_LockUnlockConference(responseXML);
                if (!ret) return false;

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }

        #endregion

        #region Mute Audio Endpoint
        /// <summary>
        /// MuteEndpoint
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="party"></param>
        /// <param name="mute"></param>
        /// <returns></returns>
        internal bool MuteEndpoint(NS_MESSENGER.Conference conf, NS_MESSENGER.Party party, bool mute)
        {
            bool ret = false;
            StringBuilder MuteXML = new StringBuilder();
            string responseXML = null;
            try
            {
                logger.Trace("Entering into Mute Audio Endpoint Command....");

                ret = CreateXML_MuteEndpoint(conf, party, mute, ref MuteXML);
                if (!ret) return false;

                ret = false;
                ret = SendCommand(conf.cMcu, MuteXML.ToString(), ref responseXML);
                if (!ret) return false;

                ret = false;
                ret = ResponseXML_MuteEndpoint(responseXML);
                if (!ret) return false;

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }


        #endregion

        #region Mute All Endpoint
        /// <summary>
        /// MuteAllEndpoint
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="mute"></param>
        /// <returns></returns>
        internal bool MuteAllEndpoint(NS_MESSENGER.Conference conf, bool mute)
        {
            bool ret = false;
            StringBuilder MuteXML = new StringBuilder();
            string responseXML = null;
            try
            {
                logger.Trace("Entering into MuteAll Audio Endpoint Command....");

                ret = CreateXML_MuteAllEndpoint(conf, mute, ref MuteXML);
                if (!ret) return false;

                ret = false;
                ret = SendCommand(conf.cMcu, MuteXML.ToString(), ref responseXML);
                if (!ret) return false;

                ret = false;
                ret = ResponseXML_MuteAllEndpoint(responseXML);
                if (!ret) return false;

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }


        #endregion

        #region Mute Video Endpoint
        /// <summary>
        /// MuteVideoEndpoint
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="party"></param>
        /// <param name="mute"></param>
        /// <returns></returns>
        internal bool MuteVideoEndpoint(NS_MESSENGER.Conference conf, NS_MESSENGER.Party party, bool mute)
        {
            bool ret = false;
            StringBuilder MuteXML = new StringBuilder();
            string responseXML = null;
            try
            {
                logger.Trace("Entering into Mute Video Endpoint Command....");

                ret = CreateXML_MuteVideoEndpoint(conf, party, mute, ref MuteXML);
                if (!ret) return false;

                ret = false;
                ret = SendCommand(conf.cMcu, MuteXML.ToString(), ref responseXML);
                if (!ret) return false;

                ret = false;
                ret = ResponseXML_MuteVideoEndpoint(responseXML);
                if (!ret) return false;

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }


        #endregion

        #region Send Message to Endpoint
        /// <summary>
        /// ParticipantMessage
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="party"></param>
        /// <param name="messageText"></param>
        /// <param name="duration"></param>
        /// <returns></returns>
        internal bool ParticipantMessage(NS_MESSENGER.Conference conf, NS_MESSENGER.Party party, string messageText, int duration)
        {
            bool ret = false;
            StringBuilder MessageXML = new StringBuilder();
            string responseXML = null;
            try
            {
                logger.Trace("Entering into Send message to Endpoint Command....");

                duration = duration * 60;
                ret = CreateXML_EndpointMessage(conf, party, messageText, duration, ref MessageXML);
                if (!ret) return false;

                ret = false;
                ret = SendCommand(conf.cMcu, MessageXML.ToString(), ref responseXML);
                if (!ret) return false;

                ret = false;
                ret = ResponseXML_EndpointMessage(responseXML);
                if (!ret) return false;

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }

        #endregion

        #region Send Message to Conference
        /// <summary>
        /// ConferenceMessage
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="messageText"></param>
        /// <param name="duration"></param>
        /// <returns></returns>
        internal bool ConferenceMessage(NS_MESSENGER.Conference conf, string messageText, int duration)
        {
            bool ret = false;
            StringBuilder MessageXML = new StringBuilder();
            string responseXML = null;
            try
            {
                logger.Trace("Entering into Send message to Conference Command....");

                duration = duration * 60;
                ret = CreateXML_ConferenceMessage(conf, messageText, duration, ref MessageXML);
                if (!ret) return false;

                ret = false;
                ret = SendCommand(conf.cMcu, MessageXML.ToString(), ref responseXML);
                if (!ret) return false;

                ret = false;
                ret = ResponseXML_ConferenceMessage(responseXML);
                if (!ret) return false;

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }

        #endregion

        #region GetEndpointStatus
        /// <summary>
        /// GetEndpointStatus
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="party"></param>
        /// <returns></returns>
        internal bool GetEndpointStatus(NS_MESSENGER.Conference conf, ref NS_MESSENGER.Party party)
        {
            bool ret = false;
            string EndpointStatusXML = null, responseXML = null;
            try
            {
                logger.Trace("Entering into Get Endpoint Status Command....");

                ret = CreateXML_GetEndpointStatus(conf.sGUID, party.sGUID, ref EndpointStatusXML);
                if (!ret) return false;

                ret = SendCommand(party.cMcu, EndpointStatusXML, ref responseXML);
                if (!ret) return false;

                ret = Response_GetEndpointStatus(conf, party, responseXML); //Check
                if (!ret) return false;

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        #endregion

        #region Endpoint FECC
        /// <summary>
        /// ParticipantFECC
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="party"></param>
        /// <param name="direction"></param>
        /// <returns></returns>
        internal bool ParticipantFECC(NS_MESSENGER.Conference conf, NS_MESSENGER.Party party, int direction)
        {
            bool ret = false;
            StringBuilder FECCXML = new StringBuilder();
            string responseXML = null;
            try
            {
                logger.Trace("Entering into Endpoint FECC Command....");

                party.sFECCdirection = direction.ToString();
                string Direction = "";
                bool ret2 = ParticipantFECCDirection(party.sFECCdirection, ref Direction);
                if (!ret2)
                {
                    logger.Trace("Invalid FECC Direction.");
                    return false;
                }

                ret = CreateXML_ParticipantFECC(conf, party, Direction, ref FECCXML);
                if (!ret) return false;

                ret = false;
                ret = SendCommand(conf.cMcu, FECCXML.ToString(), ref responseXML);
                if (!ret) return false;

                ret = false;
                ret = ResponseXML_ParticipantFECC(responseXML);
                if (!ret) return false;

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }

        internal bool ParticipantFECCDirection(string FECCType, ref string Direction)
        {
            try
            {
                switch (FECCType)
                {
                    case "1":
                        {
                            Direction = "Up";
                            break;
                        }
                    case "2":
                        {
                            Direction = "Down";
                            break;
                        }
                    case "3":
                        {
                            Direction = "Left";
                            break;
                        }
                    case "4":
                        {
                            Direction = "Right";
                            break;
                        }
                    case "5":
                        {
                            Direction = "In";
                            break;
                        }
                    case "6":
                        {
                            Direction = "Up";
                            break;
                        }
                    case "7":
                        {
                            Direction = "In";
                            break;
                        }
                    case "8":
                        {
                            Direction = "Up";
                            break;
                        }
                    default:
                        {
                            Direction = "";
                            break;
                        }
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }

        #endregion

        #region Create Virtual Room
        /// <summary>
        /// CreateVirtualRoom
        /// </summary>
        /// <param name="conf"></param>
        /// <returns></returns>
        internal bool CreateVirtualRoom(NS_MESSENGER.Conference conf,NS_MESSENGER.Party Party)
        {
            bool ret = false;
            StringBuilder VirtualXML = new StringBuilder();
            string responseXML = null;
            try
            {
                logger.Trace("Entering into Create Virtual Room Command....");

                ret = CreateXML_CreateVirtualRoom(conf, ref VirtualXML);
                if (!ret) return false;

                ret = false;
                ret = SendCommand(conf.cMcu, VirtualXML.ToString(), ref responseXML);
                if (!ret) return false;

                ret = false;
                ret = ResponseXML_CreateVirtualRoom(ref Party, responseXML);
                if (!ret) return false;

                return true;
            }
            catch (Exception ex)
            {
                logger.Exception(100, ex.Message);
                return false;
            }

        }
        #endregion

        #region Modify Virtual Room
        /// <summary>
        /// ModifyVirtualRoom
        /// </summary>
        /// <param name="conf"></param>
        /// <returns></returns>
        internal bool ModifyVirtualRoom(NS_MESSENGER.Conference conf)
        {
            bool ret = false;
            StringBuilder VirtualXML = new StringBuilder();
            string responseXML = null;
            try
            {
                logger.Trace("Entering into Modify Virtual Room Command....");

                ret = CreateXML_ModifyVirtualRoom(conf, ref VirtualXML);
                if (!ret) return false;

                ret = false;
                ret = SendCommand(conf.cMcu, VirtualXML.ToString(), ref responseXML);
                if (!ret) return false;

                ret = false;
                ret = ResponseXML_ModifyVirtualRoom(responseXML);
                if (!ret) return false;

                return true;
            }
            catch (Exception ex)
            {
                logger.Exception(100, ex.Message);
                return false;
            }

        }
        #endregion

        #region Delete Virtual Room
        /// <summary>
        /// DeleteVirtualRoom
        /// </summary>
        /// <param name="conf"></param>
        /// <returns></returns>
        internal bool DeleteVirtualRoom(NS_MESSENGER.Conference conf)
        {
            bool ret = false;
            StringBuilder VirtualXML = new StringBuilder();
            string responseXML = null;
            try
            {
                logger.Trace("Entering into Delete Virtual Room Command....");

                ret = CreateXML_DeleteVirtualRoom(conf, ref VirtualXML);
                if (!ret) return false;

                ret = false;
                ret = SendCommand(conf.cMcu, VirtualXML.ToString(), ref responseXML);
                if (!ret) return false;

                ret = false;
                ret = ResponseXML_DeleteVirtualRoom(responseXML);
                if (!ret) return false;

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }


        #endregion

        //layout is is not setting need to check

        #region Send Layout to Endpoint
        /// <summary>
        /// ParticipantLayout
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="party"></param>
        /// <returns></returns>
        internal bool ChangePartyDisplayLayout(NS_MESSENGER.Conference conf, NS_MESSENGER.Party party, int Layout)
        {
            bool ret = false;
            StringBuilder LayoutXML = new StringBuilder();
            string responseXML = null;
            try
            {
                logger.Trace("Entering into Set Participant Layout Command....");

                ret = CreateXML_EndpointLayout(conf, party, Layout, ref LayoutXML);
                if (!ret) return false;

                ret = false;
                ret = SendCommand(conf.cMcu, LayoutXML.ToString(), ref responseXML);
                if (!ret) return false;

                ret = false;
                ret = ResponseXML_EndpointLayout(responseXML);
                if (!ret) return false;

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }

        #endregion

        #region GetSiloDetails Request
        /// <summary>
        /// GetSiloDetails
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="party"></param>
        /// <returns></returns>
        internal bool GetSiloDetails(NS_MESSENGER.MCU MCU)
        {
            logger.Trace("Entering into Silo Details"); //ZD 102198
            try
            {
                bool ret = false;
                StringBuilder OrgXML = new StringBuilder();
                string responseXML = null;

                ret = CreateXML_GetOrganization(ref OrgXML);
                if (!ret) return false;

                ret = false;
                ret = SendCommandforOrganization(MCU, OrgXML.ToString(), ref responseXML); //ZD 102198
                if (!ret) return false;

                ret = false;
                ret = ResponseXML_GetOrganization(responseXML, MCU.iDbId, MCU.sMcuType);
                if (!ret) return false;

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }


        #endregion

        #region Get Meeting Service
        /// <summary>
        /// MuteEndpoint
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="party"></param>
        /// <returns></returns>
        internal bool GetMeetingService(NS_MESSENGER.MCU MCU)
        {
            logger.Trace("Entering into MuteEndpoint");
            try
            {
                bool ret = false;
                StringBuilder OrgXML = new StringBuilder();
                string responseXML = null;

                ret = CreateXML_GetMeetingService(ref OrgXML, MCU.IViewOrgID);
                if (!ret) return false;

                ret = false;
                ret = SendCommand(MCU, OrgXML.ToString(), ref responseXML);
                if (!ret) return false;

                ret = false;
                ret = ResponseXML_GetMeetingService(responseXML, MCU.iDbId, MCU.sMcuType, MCU.IViewOrgID);
                if (!ret) return false;

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }


        #endregion

        #region DeleteSyncConference
        internal bool DeleteSyncConference(NS_MESSENGER.Conference conf)
        {
            List<NS_MESSENGER.Conference> confListDelete = null;
            bool ret = false;
            try
            {
                confListDelete = new List<NS_MESSENGER.Conference>();
                confListDelete.Add(conf);

                db.FetchSyncAdjustments(ref confListDelete,NS_MESSENGER.MCU.eType.RADVISIONIVIEW); //ZD 100221

                for (int cnfCnt = 0; cnfCnt < confListDelete.Count; cnfCnt++)
                {
                    confListDelete[cnfCnt].cMcu = conf.cMcu;
                    ret = TerminateConference(confListDelete[cnfCnt]);
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Trace("TerminateConference:" + e.Message);
                return false;
            }
        }
        #endregion

        //ZD 100369_MCU Start
        #region  FetchMCUDetails
        /// <summary>
        /// To Fetch MCU Details
        /// </summary>
        /// <param name="mcu"></param>
        /// <returns></returns>
        internal bool FetchMCUDetails(NS_MESSENGER.MCU mcu, int Timeout)
        {
            string KeepAliveXML = null, responseXML = null;
            bool ret = false;
            try
            {
                logger.Trace("Entering into Test MCU Connection Command....");
                int pollstatus = (int)NS_MESSENGER.MCU.PollState.PASS;
                ret = CreateXML_KeepAlive(ref KeepAliveXML);

                ret = false;
                ret = SendMCUStatusCommand(mcu, KeepAliveXML, ref responseXML, Timeout);
                if (!ret)
                {
                    pollstatus = (int)NS_MESSENGER.MCU.PollState.FAIL;
                }
                else if (responseXML.Length < 1)
                {
                    pollstatus = (int)NS_MESSENGER.MCU.PollState.FAIL;
                }
                else
                {
                    ret = false;
                    ret = ResponseXML_KeepAlive(responseXML);
                    if (!ret)
                        pollstatus = (int)NS_MESSENGER.MCU.PollState.FAIL;
                }
                db.UpdateMcuStatus(mcu, pollstatus);
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                db.UpdateMcuStatus(mcu, (int)NS_MESSENGER.MCU.PollState.FAIL);
                return false;
            }
        }
        #endregion
        //ZD 100369_MCU End

        //ZD 100890 Start

        #region Get Virtual Room
        /// <summary>
        /// GetVirtualRoom
        /// </summary>
        /// <param name="conf"></param>
        /// <returns></returns>
        internal bool GetVirtualRoom(NS_MESSENGER.Conference conf)
        {
            bool ret = false;
            StringBuilder VirtualXML = new StringBuilder();
            string responseXML = null;
            try
            {
                logger.Trace("Entering into Modify Virtual Room Command....");

                ret = CreateXML_GetVirtualRoom(conf, ref VirtualXML);
                if (!ret) return false;

                ret = false;
                ret = SendCommand(conf.cMcu, VirtualXML.ToString(), ref responseXML);
                if (!ret) return false;

                ret = false;
                ret = ResponseXML_GetVirtualRoom(conf, responseXML);
                if (!ret) return false;

                return true;
            }
            catch (Exception ex)
            {
                logger.Exception(100, ex.Message);
                return false;
            }

        }
        #endregion

        #region Create User
        /// <summary>
        /// Create User
        /// </summary>
        /// <param name="conf"></param>
        /// <returns></returns>
        internal bool CreateUser(NS_MESSENGER.MCU MCU, NS_MESSENGER.Party Party)
        {
            bool ret = false;
            StringBuilder UserXML = new StringBuilder();
            string responseXML = null;
            NS_MESSENGER.Conference Conf = new NS_MESSENGER.Conference();
            Conf.radParam = new NS_MESSENGER.RadVisionConfParam();
            try
            {
                logger.Trace("Entering into Create User Command....");

                ret = CreateXML_CreateUser(MCU, Party, ref UserXML);
                if (!ret) return false;

                ret = false;
                ret = SendCommand(MCU, UserXML.ToString(), ref responseXML);
                if (!ret) return false;

                ret = false;
                ret = ResponseXML_CreateUser(ref Party, responseXML);
                if (!ret) return false;

                Conf.cMcu = MCU;
                Conf.radParam.StaticIDNumber = Party.sStaticID;
                Conf.sExternalName = "VrRoom" + Party.siViewUserID;
                Conf.radParam.UserID = Party.siViewUserID;

                if (Party.IsStaticIDEnabled && !string.IsNullOrEmpty(Conf.radParam.StaticIDNumber))
                {
                    if (GetVirtualRoom(Conf))
                    {
                        CreateVirtualRoom(Conf, Party);
                    }
                    else
                    {
                        this.errMsg = "Static ID already used in iView MCU";
                        return false;
                    }
                }
                
                return true;
            }
            catch (Exception ex)
            {
                logger.Exception(100, ex.Message);
                return false;
            }

        }
        #endregion

        #region Update User
        /// <summary>
        /// Update User
        /// </summary>
        /// <param name="conf"></param>
        /// <returns></returns>
        internal bool UpdateUser(NS_MESSENGER.MCU MCU, NS_MESSENGER.Party Party, string OldStaticId)
        {
            bool ret = false;
            StringBuilder UserXML = new StringBuilder();
            string responseXML = null;
            NS_MESSENGER.Conference Conf = new NS_MESSENGER.Conference();
            Conf.radParam = new NS_MESSENGER.RadVisionConfParam();
            try
            {
                logger.Trace("Entering into Create User Command....");

                ret = CreateXML_UpdateUser(MCU, Party, ref UserXML);
                if (!ret) return false;

                ret = false;
                ret = SendCommand(MCU, UserXML.ToString(), ref responseXML);
                if (!ret) return false;

                ret = false;
                ret = ResponseXML_UpdateUser(ref Party, responseXML,MCU);
                if (!ret) return false;


                if (OldStaticId == Party.sStaticID)
                    return true;

                Conf.cMcu = MCU;
                Conf.radParam.StaticIDNumber = Party.sStaticID;
                Conf.sExternalName = "VrRoom" + Party.siViewUserID;
                Conf.radParam.UserID = Party.siViewUserID;
                Conf.radParam.VirtualRoomId = Party.sVirtualRoomId;

                if ((Party.IsStaticIDEnabled) && !string.IsNullOrEmpty(Conf.radParam.StaticIDNumber))
                {
                    if (!string.IsNullOrEmpty(Conf.radParam.VirtualRoomId))
                    {
                        if (GetVirtualRoom(Conf))
                        {
                            DeleteVirtualRoom(Conf);
                            CreateVirtualRoom(Conf, Party);
                            // ModifyVirtualRoom(Conf);
                        }
                        else
                        {
                            this.errMsg = "Static ID already used in iView MCU ";
                            Party.sStaticID = OldStaticId;
                            db.UpdateParticipantExternalID(Party);
                            return false;
                        }
                    }
                    else if (GetVirtualRoom(Conf))
                    {
                        CreateVirtualRoom(Conf, Party);
                    }
                    else
                    {
                        this.errMsg = "Static ID already used in iView MCU ";
                        Party.sStaticID = OldStaticId;
                        db.UpdateParticipantExternalID(Party);
                        return false;
                    }
                }
                else if (!string.IsNullOrEmpty(Conf.radParam.VirtualRoomId))
                {
                    DeleteVirtualRoom(Conf);
                    db.DeleteParticipantExternalID("", Conf.radParam.VirtualRoomId);
                }

                return true;
            }
            catch (Exception ex)
            {
                logger.Exception(100, ex.Message);
                return false;
            }

        }
        #endregion

        #region Delete User
        /// <summary>
        /// DeleteUser
        /// </summary>
        /// <param name="conf"></param>
        /// <returns></returns>
        internal bool DeleteUser(NS_MESSENGER.MCU MCU, NS_MESSENGER.Party Party)
        {
            bool ret = false;
            StringBuilder UserXML = new StringBuilder();
            string responseXML = null;
            NS_MESSENGER.Conference Conf = new NS_MESSENGER.Conference();
            Conf.radParam = new NS_MESSENGER.RadVisionConfParam();
            try
            {
                logger.Trace("Entering into Create User Command....");

                ret = CreateXML_DeleteUser(MCU, Party, ref UserXML);
                if (!ret) return false;

                ret = false;
                ret = SendCommand(MCU, UserXML.ToString(), ref responseXML);
                if (!ret) return false;

                ret = false;
                ret = ResponseXML_DeleteUser(responseXML);
                if (!ret) return false;

                Conf.cMcu = MCU;
                Conf.radParam.VirtualRoomId = Party.sVirtualRoomId;

                if (!string.IsNullOrEmpty(Party.sVirtualRoomId))
                    DeleteVirtualRoom(Conf);

                db.DeleteParticipantExternalID(Party.siViewUserID, Party.sVirtualRoomId);

                return true;
            }
            catch (Exception ex)
            {
                logger.Exception(100, ex.Message);
                return false;
            }

        }
        #endregion

        //ZD 100890 End

        #endregion
		//ZD 101431 Starts
        #region SendCommand
        /// <summary>
        /// SendCommand
        /// </summary>
        /// <param name="mcu"></param>
        /// <param name="sendXML"></param>
        /// <param name="receiveXML"></param>
        /// <returns></returns>
        private bool SendCommand(NS_MESSENGER.MCU mcu, string sendXML, ref string receiveXML)
        {
            NetworkStream stm = null; //ZD 100553 inncrewin 12/13/2013
            ASCIIEncoding asen = new ASCIIEncoding();
            byte[] ba = null;
            //ZD 100553 inncrewin 12/13/2013
            int len = 100000;
            byte[] bb = null;
            int k = 0;
            //ZD 100553 inncrewin 12/13/2013 Ends
            TcpClient client = null;
            try
            {

                string inXML = "<?xml version=\"1.0\"?><MCU_XML_API><Version>" + mcu.sSoftwareVer + "</Version><Account>" + mcu.sLogin + "</Account><Password>" + mcu.sPwd + "</Password><Request>" + sendXML + "</Request></MCU_XML_API>";
                logger.Trace("*********Send inXML*********");
                logger.Trace(inXML);

                logger.Trace("Connecting to Radvision IView MCU - " + mcu.sName + "...");
                client = new TcpClient();
                client.Connect(mcu.sIp, mcu.iHttpPort);
                stm = client.GetStream();

                ba = asen.GetBytes(inXML);
                logger.Trace("Transmitting...");
                stm.Write(ba, 0, ba.Length);

                bb = new byte[len];
                k = stm.Read(bb, 0, len);
                for (int i = 0; i < k; i++)
                    receiveXML += Convert.ToChar(bb[i]).ToString();
                client.Close();

                logger.Trace("*********Receive XML*********");
                logger.Trace(receiveXML);

                //ZD 100969 Start
                if (!string.IsNullOrEmpty(receiveXML))
                {
                    if (receiveXML.Contains("SliderSessionInProgress"))
                    {
                        String[] partyarr = null;
                        String[] delimitedArr = { "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" };
                        partyarr = receiveXML.Split(delimitedArr, StringSplitOptions.RemoveEmptyEntries);
                        for (int i = 0; i < partyarr.Length; i++)
                        {
                            if (!partyarr[i].Contains("SliderSessionInProgress"))
                                receiveXML = partyarr[i];
                        }
                    }
                }
                //ZD 100969 End
                /*
                string inXML = "<?xml version=\"1.0\"?><MCU_XML_API><Version>" + mcu.sSoftwareVer + "</Version><Account>" + mcu.sLogin + "</Account><Password>" + mcu.sPwd + "</Password><Request>" + sendXML + "</Request></MCU_XML_API>";
                logger.Trace("*********Send inXML*********");
                logger.Trace(inXML);

                logger.Trace("Connecting to Radvision IView MCU - " + mcu.sName + "...");
                client = new TcpClient();
                client.ReceiveBufferSize = int.MaxValue;//ZD 100553 inncrewin 12/13/2013
                client.Connect(mcu.sIp, mcu.iHttpPort);
                stm = client.GetStream();
                stm.ReadTimeout = 10000; //ZD 100553 - Changes made by Inncrewin
                //ZD 100553 inncrewin 12/13/2013
                ba = asen.GetBytes(inXML);
                logger.Trace("Transmitting..."); //ZD 100553 - Changes made by Inncrewin
                stm.Write(ba, 0, ba.Length);
                stm.Flush();
                int counter = 0; //ZD 100553 - Changes made by Inncrewin
                if (stm.CanRead)
                {
                    
                    byte[] myReadBuffer = new byte[1024*32];                    
                    int numberOfBytesRead = 0;
                    try
                    {
                        do
                        {
                            numberOfBytesRead = 0;
                            numberOfBytesRead = stm.Read(myReadBuffer, 0, myReadBuffer.Length);
                            receiveXML = String.Concat(receiveXML, Encoding.ASCII.GetString(myReadBuffer, 0, numberOfBytesRead));
                            counter++;
                        }
                        // while (stm.DataAvailable);
                        while (numberOfBytesRead > 0);
                    }
                    catch (IOException er) {
                        logger.Exception(100, er.Message);
                    }
                    
                }
                //ZD 100553 inncrewin 12/13/2013 ends
               client.Close();

                logger.Trace("*********Receive XML*********");
                logger.Trace(receiveXML);*/

                
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
            finally
            {
                try 
	            {	 
                    client = null;
                    stm.Dispose();
                    stm = null;
                    ba = null;
                    bb = null;//ZD 100553 inncrewin 12/13/2013
	            }
	            catch (Exception)
	            {
            		
		            
	            }
            }
        }
		//ZD 102198 Start
        private bool SendCommandforOrganization(NS_MESSENGER.MCU mcu, string sendXML, ref string receiveXML)
        {
            NetworkStream stm = null; //ZD 100553 inncrewin 12/13/2013
            ASCIIEncoding asen = new ASCIIEncoding();
            byte[] dataSent = null;
            //ZD 100553 inncrewin 12/13/2013
            int len = 1024;
            byte[] dataRecvd = null;
            int k = 0;
            //ZD 100553 inncrewin 12/13/2013 Ends
            TcpClient client = null;
            Boolean eof = false;
            NS_MESSENGER.StringHelper stringHelper = null;
            try
            {

                string inXML = "<?xml version=\"1.0\"?><MCU_XML_API><Version>" + mcu.sSoftwareVer + "</Version><Account>" + mcu.sLogin + "</Account><Password>" + mcu.sPwd + "</Password><Request>" + sendXML + "</Request></MCU_XML_API>";
                logger.Trace("*********Send inXML*********");
                logger.Trace(inXML);

                logger.Trace("Connecting to Radvision IView MCU - " + mcu.sName + "...");
                client = new TcpClient();
                client.Connect(mcu.sIp, mcu.iHttpPort);
                stm = client.GetStream();

                dataSent = asen.GetBytes(inXML);
                logger.Trace("Transmitting...");
                stm.Write(dataSent, 0, dataSent.Length);

                dataRecvd = new byte[len];

                 do
                {
                    k = stm.Read(dataRecvd, 0, len);
                    for (int i = 0; i < k; i++)
                        receiveXML += Convert.ToChar(dataRecvd[i]).ToString();
                    if (receiveXML.Contains("</MCU_XML_API>"))
                        eof = true;

                } while (!eof);

                 stringHelper = new NS_MESSENGER.StringHelper();
                 receiveXML = stringHelper.GetStringInBetween("<MCU_XML_API>", "</MCU_XML_API>", receiveXML, true, true);

                 client.Close();

                logger.Trace("*********Receive XML*********");
                logger.Trace(receiveXML);

                //ZD 100969 Start
                if (!string.IsNullOrEmpty(receiveXML))
                {
                    if (receiveXML.Contains("SliderSessionInProgress"))
                    {
                        String[] partyarr = null;
                        String[] delimitedArr = { "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" };
                        partyarr = receiveXML.Split(delimitedArr, StringSplitOptions.RemoveEmptyEntries);
                        for (int i = 0; i < partyarr.Length; i++)
                        {
                            if (!partyarr[i].Contains("SliderSessionInProgress"))
                                receiveXML = partyarr[i];
                        }
                    }
                }
                


                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
            finally
            {
                try
                {
                    client = null;
                    stm.Dispose();
                    stm = null;
                    dataSent = null;
                    dataRecvd = null;//ZD 100553 inncrewin 12/13/2013
                }
                catch (Exception)
                {


                }
            }
        }
		//ZD 101431 End
		//ZD 102198 End
        //ZD 100369_MCU Start
        private bool SendMCUStatusCommand(NS_MESSENGER.MCU mcu, string sendXML, ref string receiveXML, int Timeout)
        {
            NetworkStream stm = null; //ZD 100553 inncrewin 12/13/2013
            ASCIIEncoding asen = new ASCIIEncoding();
            byte[] ba = null;
            //ZD 100553 inncrewin 12/13/2013
            //int len = 100000;
            //byte[] bb = null;
            //int k = 0;
            //ZD 100553 inncrewin 12/13/2013 Ends
            TcpClient client = null;
            try
            {
                string inXML = "<?xml version=\"1.0\"?><MCU_XML_API><Version>" + mcu.sSoftwareVer + "</Version><Account>" + mcu.sLogin + "</Account><Password>" + mcu.sPwd + "</Password><Request>" + sendXML + "</Request></MCU_XML_API>";
                logger.Trace("*********Send inXML*********");
                logger.Trace(inXML);

                logger.Trace("Connecting to Radvision IView MCU - " + mcu.sName + "...");
                client = new TcpClient();
                client.ReceiveBufferSize = int.MaxValue;//ZD 100553 inncrewin 12/13/2013
                client.Connect(mcu.sIp, mcu.iHttpPort);
                client.ReceiveTimeout = Timeout;
                stm = client.GetStream();
                stm.ReadTimeout = 10000; //ZD 100553 - Changes made by Inncrewin
                //ZD 100553 inncrewin 12/13/2013
                ba = asen.GetBytes(inXML);
                logger.Trace("Transmitting..."); //ZD 100553 - Changes made by Inncrewin
                stm.Write(ba, 0, ba.Length);
                stm.Flush();
                int counter = 0; //ZD 100553 - Changes made by Inncrewin
                if (stm.CanRead)
                {

                    byte[] myReadBuffer = new byte[1024 * 32];
                    int numberOfBytesRead = 0;
                    try
                    {
                        do
                        {
                            numberOfBytesRead = 0;
                            numberOfBytesRead = stm.Read(myReadBuffer, 0, myReadBuffer.Length);
                            receiveXML = String.Concat(receiveXML, Encoding.ASCII.GetString(myReadBuffer, 0, numberOfBytesRead));
                            counter++;
                        }
                        // while (stm.DataAvailable);
                        while (numberOfBytesRead > 0);
                    }
                    catch (IOException er)
                    {
                        logger.Exception(100, er.Message);
                    }

                }
                //ZD 100553 inncrewin 12/13/2013 ends
                client.Close();

                logger.Trace("*********Receive XML*********");
                logger.Trace(receiveXML);
                //ZD 100969 Start
                if (!string.IsNullOrEmpty(receiveXML))
                {
                    if (receiveXML.Contains("SliderSessionInProgress"))
                    {
                        String[] partyarr = null;
                        String[] delimitedArr = { "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" };
                        partyarr = receiveXML.Split(delimitedArr, StringSplitOptions.RemoveEmptyEntries);
                        for (int i = 0; i < partyarr.Length; i++)
                        {
                            if (!partyarr[i].Contains("SliderSessionInProgress"))
                                receiveXML = partyarr[i];
                        }
                    }
                }
                //ZD100969 End
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
            finally
            {
                try
                {
                    client = null;
                    stm.Dispose();
                    stm = null;
                    ba = null;
                    //bb = null;//ZD 100553 inncrewin 12/13/2013
                }
                catch (Exception)
                {


                }
            }
        }
        //ZD 100369_MCU End
        #endregion

        # region RequestXML

        #region KeepAlive
        /// <summary>
        ///  KeepAlive   
        /// </summary>
        /// <param name="KeepAliveXML"></param>
        /// <returns></returns>
        private bool CreateXML_KeepAlive(ref string KeepAliveXML)
        {
            try
            {
                KeepAliveXML += "<Keep_Alive_Request>";
                KeepAliveXML += "<RequestID>" + DateTime.Now.ToString("mmddyyyymmhhss") + "</RequestID>";
                KeepAliveXML += "</Keep_Alive_Request>";

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        #endregion

        #region TerminateConference
        /// <summary>
        /// TerminateConference
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="terminateXML"></param>
        /// <returns></returns>
        private bool CreateXML_TerminateConference(string confGUID, ref string terminateXML) //ZD 100190
        {
            try
            {
                terminateXML = "<Terminate_Conference_Request>";
                terminateXML += "<RequestID>" + DateTime.Now.ToString("mmddyyyymmhhss") + "</RequestID>";
                terminateXML += "<ConfGID>" + confGUID + "</ConfGID>";
                terminateXML += "</Terminate_Conference_Request>";

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        #endregion

        //FB 2659 START
        #region CancelConference
        /// <summary>
        /// CancelConference
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="CancelXML"></param>
        /// <returns></returns>
        private bool CreateXML_CancelConference(string confGUID, ref string CancelXML)
        {
            try
            {
                CancelXML = "<Cancel_Conference_Request>";
                CancelXML += "<RequestID>" + DateTime.Now.ToString("mmddyyyymmhhss") + "</RequestID>";
                CancelXML += "<ConferenceId>" + confGUID + "</ConferenceId>";
                //CancelXML += "<StartTime></StartTime>";
                CancelXML += "</Cancel_Conference_Request>";

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        #endregion
        //FB 2659 END

        #region TerminateEndpoint
        /// <summary>
        /// TerminateEndpoint
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="participantXml"></param>
        /// <returns></returns>
        private bool CreateXML_TerminateEndpoint(NS_MESSENGER.Conference conf, NS_MESSENGER.Party party, ref string terminateXML)
        {
            try
            {
                terminateXML = "<Delete_Part_Request>";
                terminateXML += "<RequestID>" + DateTime.Now.ToString("mmddyyyymmhhss") + "</RequestID>";
                terminateXML += "<ConfGID>" + conf.sGUID + "</ConfGID>";
                terminateXML += "<Delete_Part_List>";
                terminateXML += "<PID>" + party.sGUID + "</PID>";
                terminateXML += "</Delete_Part_List>";
                terminateXML += "</Delete_Part_Request>";

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        #endregion

        #region ExtendConfEndTime
        /// <summary>
        /// ExtendConfEndTime
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="ExtendTime"></param>
        /// <param name="ExtendTimeXML"></param>
        /// <returns></returns>
        private bool CreateXML_ExtendConfEndTime(NS_MESSENGER.Conference conf, int ExtendTime, ref string ExtendTimeXML)
        {
            try
            {
                ExtendTimeXML = "<Extend_Conference_Request>";
                ExtendTimeXML += "<RequestID>" + DateTime.Now.ToString("mmddyyyymmhhss") + "</RequestID>";
                ExtendTimeXML += "<ConfGID>" + conf.sGUID + "</ConfGID>";
                ExtendTimeXML += "<ExtendingTime>" + ExtendTime * 60 * 1000 + "</ExtendingTime>";
                ExtendTimeXML += "</Extend_Conference_Request>";
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        #endregion

        #region CreateVirtualRoom
        /// <summary>
        /// CreateVirtualRoom
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="reservationXML"></param>
        /// <returns></returns>
        private bool CreateXML_CreateVirtualRoom(NS_MESSENGER.Conference conf, ref StringBuilder VirtualXML)
        {
            try
            {
                int durationInSecs = 0;
                durationInSecs = conf.iDuration * 60;
                DateTime StartDate = DateTime.Parse(conf.dtStartDateTime.ToString());
                NS_MESSENGER.Party party = null;

                VirtualXML.Append("<Create_Virtual_Room_Request>");
                VirtualXML.Append("<RequestID>" + new Random().Next(int.MaxValue).ToString() + "</RequestID>");//ZD 100755 Inncrewin Starts
                VirtualXML.Append("<VirtualRoom>");
                VirtualXML.Append("<MemberId>" + conf.cMcu.IViewOrgID.ToString() + "</MemberId>");
                if (conf.radParam != null)
                {
                    VirtualXML.Append("<UserId>" + conf.radParam.UserID + "</UserId>"); //Need to check how to get userid
                    VirtualXML.Append("<Number>" + conf.radParam.StaticIDNumber + "</Number>");
                    VirtualXML.Append("<ServicePrefix>" + conf.radParam.ServicePrefix.ToString() + "</ServicePrefix>"); //Need tp get service prefix
                    VirtualXML.Append("<Priority>" + conf.radParam.Priority.ToString() + "</Priority>");
                    VirtualXML.Append("<AllowStreaming>" + conf.radParam.AllowStreaming.ToString() + "</AllowStreaming>");
                    VirtualXML.Append("<StreamingStatus>" + conf.radParam.StreamingStatus.ToString() + "</StreamingStatus>");
                }
                //ZD 100755 Inncrewin Starts
                System.Collections.IEnumerator partyEnumerator;
                partyEnumerator = conf.qParties.GetEnumerator();
                int partyCount = conf.qParties.Count;
                if (partyCount > 0)
                {
                    VirtualXML.Append("<Attendee>");
                    for (int i = 0; i < partyCount; i++)
                    {
                        partyEnumerator.MoveNext();
                        party = new NS_MESSENGER.Party();
                        party = (NS_MESSENGER.Party)partyEnumerator.Current;
                        if (party.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_OUT)
                        {
                            VirtualXML.Append("<Protocol>H323</Protocol>"); //party protocol
                            VirtualXML.Append("<TerminalName>" + party.sName + "</TerminalName>");
                            VirtualXML.Append("<TerminalNumber>" + party.sAddress.Trim() + "</TerminalNumber>");
                            VirtualXML.Append("<MaxBandwidth>384</MaxBandwidth>"); //Pary bandwisth
                        }
                    }
                    VirtualXML.Append("</Attendee>");
                }
                if (conf.radParam != null)
                {
                    VirtualXML.Append("<AutoExtend>" + conf.radParam.AutoExtend.ToString() + "</AutoExtend>");
                    VirtualXML.Append("<WaitingRoom>FALSE</WaitingRoom>");
                    VirtualXML.Append("<AdvancedProperties>");
                    VirtualXML.Append("<DurationAfterLeft>" + conf.radParam.getDurationAfterLeft + "</DurationAfterLeft>");
                    VirtualXML.Append("<TerminationCondition>" + conf.radParam.TerminationCondition.ToString() + "</TerminationCondition>");
                    VirtualXML.Append("</AdvancedProperties>");
                    VirtualXML.Append("<OneTimePINRequired>" + conf.radParam.OneTimePINRequired + "</OneTimePINRequired>");
                }
                //ZD 100755 Inncrewin Ends
                VirtualXML.Append("<Name>" + conf.sExternalName + "</Name>"); // Room Name
                VirtualXML.Append("<Description>" + conf.sExternalName + ":" + conf.sDescription + "</Description>");
                VirtualXML.Append("</VirtualRoom>");
                VirtualXML.Append("</Create_Virtual_Room_Request>");

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }


        }
        #endregion

        #region ModifyVirtualRoom
        /// <summary>
        /// ModifyVirtualRoom
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="reservationXML"></param>
        /// <returns></returns>
        private bool CreateXML_ModifyVirtualRoom(NS_MESSENGER.Conference conf, ref StringBuilder VirtualXML)
        {
            try
            {
                int durationInSecs = 0;
                durationInSecs = conf.iDuration * 60;
                DateTime StartDate = DateTime.Parse(conf.dtStartDateTime.ToString());
                NS_MESSENGER.Party party = null;

                VirtualXML.Append("<Modify_Virtual_Room_Request>");
                VirtualXML.Append("<RequestID>" + new Random().Next(int.MaxValue).ToString() + "</RequestID>");
                VirtualXML.Append("<VirtualRoom>");
                VirtualXML.Append("<MemberId>" + conf.cMcu.IViewOrgID + "</MemberId>");
                if (conf.radParam != null)
                {
                    VirtualXML.Append("<UserId>" + conf.radParam.UserID + "</UserId>"); //Need to check how to get userid
                    VirtualXML.Append("<Number>" + conf.radParam.StaticIDNumber + "</Number>");
                    VirtualXML.Append("<ServicePrefix>" + conf.radParam.ServicePrefix.ToString() + "</ServicePrefix>"); //Need tp get service prefix
                    VirtualXML.Append("<Priority>" + conf.radParam.Priority.ToString() + "</Priority>");
                    VirtualXML.Append("<AllowStreaming>" + conf.radParam.AllowStreaming.ToString() + "</AllowStreaming>");
                    VirtualXML.Append("<StreamingStatus>" + conf.radParam.StreamingStatus.ToString() + "</StreamingStatus>");
                }
                //ZD 100755 Inncrewin Starts
                System.Collections.IEnumerator partyEnumerator;
                partyEnumerator = conf.qParties.GetEnumerator();
                int partyCount = conf.qParties.Count;
                if (partyCount > 0)
                {
                    VirtualXML.Append("<Attendee>");
                    for (int i = 0; i < partyCount; i++)
                    {
                        partyEnumerator.MoveNext();
                        party = new NS_MESSENGER.Party();
                        party = (NS_MESSENGER.Party)partyEnumerator.Current;
                        if (party.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_OUT)
                        {
                            VirtualXML.Append("<Protocol>H323</Protocol>"); //party protocol
                            VirtualXML.Append("<TerminalName>" + party.sName + "</TerminalName>");
                            VirtualXML.Append("<TerminalNumber>" + party.sAddress.Trim() + "</TerminalNumber>");
                            VirtualXML.Append("<MaxBandwidth>384</MaxBandwidth>"); //Pary bandwisth
                        }
                    }
                    VirtualXML.Append("</Attendee>");
                }
                if (conf.radParam != null)
                {
                    VirtualXML.Append("<AutoExtend>" + conf.radParam.AutoExtend.ToString() + "</AutoExtend>");
                    VirtualXML.Append("<WaitingRoom>FALSE</WaitingRoom>");
                    VirtualXML.Append("<AdvancedProperties>");
                    VirtualXML.Append("<DurationAfterLeft>" + conf.radParam.getDurationAfterLeft + "</DurationAfterLeft>");
                    VirtualXML.Append("<TerminationCondition>" + conf.radParam.TerminationCondition.ToString() + "</TerminationCondition>");
                    VirtualXML.Append("</AdvancedProperties>");
                    VirtualXML.Append("<OneTimePINRequired>" + conf.radParam.OneTimePINRequired + "</OneTimePINRequired>");
                }
                //ZD 100755 Inncrewin Ends
                VirtualXML.Append("<VirtualRoomId> " + conf.radParam.VirtualRoomId + " </VirtualRoomId>");//Virtual Room ID
                VirtualXML.Append("<Name>" + conf.sExternalName + "</Name>"); // Room Name
                VirtualXML.Append("<Description>" + conf.sExternalName + ":" + conf.sDescription + "</Description>");
                VirtualXML.Append("</VirtualRoom>");
                VirtualXML.Append("</Modify_Virtual_Room_Request>");

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }


        }
        #endregion

        #region DeleteVirtualRoom
        /// <summary>
        /// DeleteVirtualRoom
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="participantXml"></param>
        /// <returns></returns>
        private bool CreateXML_DeleteVirtualRoom(NS_MESSENGER.Conference conf, ref StringBuilder VirtualXML)
        {
            try
            {
                VirtualXML.Append("<Delete_Virtual_Room_Request>");
                VirtualXML.Append("<RequestID>" + DateTime.Now.ToString("mmddyyyymmhhss") + "</RequestID>");
                VirtualXML.Append("<MemberId>" + conf.cMcu.IViewOrgID + "</MemberId>");
                VirtualXML.Append("<VirtualRoomId>" + conf.radParam.VirtualRoomId + "</VirtualRoomId>"); //Virtual room id
                VirtualXML.Append("</Delete_Virtual_Room_Request>");

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        #endregion

        #region MuteEndpoint
        /// <summary>
        /// MuteEndpoint
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="participantXml"></param>
        /// <returns></returns>
        private bool CreateXML_MuteEndpoint(NS_MESSENGER.Conference conf, NS_MESSENGER.Party party, bool mute, ref StringBuilder MuteXML)
        {
            try
            {
                MuteXML.Append("<Part_Media_Chan_Operation_Request>");
                MuteXML.Append("<RequestID>" + DateTime.Now.ToString("mmddyyyymmhhss") + "</RequestID>");
                MuteXML.Append("<ConfGID>" + conf.sGUID + "</ConfGID>");
                MuteXML.Append("<PID>" + party.sGUID + "</PID>");
                MuteXML.Append("<ChannelMediaType>Audio</ChannelMediaType>");
                MuteXML.Append("<ChannelDirection>microphone</ChannelDirection>");
                MuteXML.Append("<MuteOn>" + mute + "</MuteOn>");
                MuteXML.Append("</Part_Media_Chan_Operation_Request>");

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        #endregion

        #region MuteAllEndpoint
        /// <summary>
        /// MuteAllEndpoint
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="participantXml"></param>
        /// <returns></returns>
        private bool CreateXML_MuteAllEndpoint(NS_MESSENGER.Conference conf, bool mute, ref StringBuilder MuteXML)
        {
            try
            {
                MuteXML.Append("<Mute_Unmute_All_Audio_Request>");
                MuteXML.Append("<RequestID>" + DateTime.Now.ToString("mmddyyyymmhhss") + "</RequestID>");
                MuteXML.Append("<ConfGID>" + conf.sGUID + "</ConfGID>");
                MuteXML.Append("<MuteOn>" + mute + "</MuteOn>");
                MuteXML.Append("</Mute_Unmute_All_Audio_Request>");

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        #endregion

        #region MuteVideoEndpoint
        /// <summary>
        /// MuteVideoEndpoint
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="participantXml"></param>
        /// <returns></returns>
        private bool CreateXML_MuteVideoEndpoint(NS_MESSENGER.Conference conf, NS_MESSENGER.Party party, bool mute, ref StringBuilder MuteXML)
        {
            try
            {
                MuteXML.Append("<Part_Media_Chan_Operation_Request>");
                MuteXML.Append("<RequestID>" + DateTime.Now.ToString("mmddyyyymmhhss") + "</RequestID>");
                MuteXML.Append("<ConfGID>" + conf.sGUID + "</ConfGID>");
                MuteXML.Append("<PID>" + party.sGUID + "</PID>");
                MuteXML.Append("<ChannelMediaType>Video</ChannelMediaType>");
                MuteXML.Append("<ChannelDirection>camera</ChannelDirection>");
                MuteXML.Append("<MuteOn>" + mute + "</MuteOn>");
                MuteXML.Append("</Part_Media_Chan_Operation_Request>");

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        #endregion

        #region connectEndpoint
        /// <summary>
        /// connectEndpoint
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="participantXml"></param>
        /// <returns></returns>
        private bool CreateXML_connectEndpoint(NS_MESSENGER.Conference conf, NS_MESSENGER.Party party, ref StringBuilder requestXML)
        {
            try
            {
                requestXML.Append("<Reinvite_Participant_Request>");
                requestXML.Append("<RequestID>" + DateTime.Now.ToString("mmddyyyymmhhss") + "</RequestID>");
                requestXML.Append("<ConfGID>" + conf.sGUID + "</ConfGID>");
                requestXML.Append("<PID>" + party.sGUID + "</PID>");
                requestXML.Append("</Reinvite_Participant_Request>");

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        #endregion

        #region DisconnectEndpoint
        /// <summary>
        /// DisconnectEndpoint
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="participantXml"></param>
        /// <returns></returns>
        private bool CreateXML_DisconnectEndpoint(NS_MESSENGER.Conference conf, NS_MESSENGER.Party party, ref StringBuilder requestXML)
        {
            try
            {
                requestXML.Append("<Drop_Participant_Request>");
                requestXML.Append("<RequestID>" + DateTime.Now.ToString("mmddyyyymmhhss") + "</RequestID>");
                requestXML.Append("<ConfGID>" + conf.sGUID + "</ConfGID>");
                requestXML.Append("<PID>" + party.sGUID + "</PID>");
                requestXML.Append("</Drop_Participant_Request>");

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        #endregion

        #region ParticipantMessage
        /// <summary>
        /// EndpointMessage
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="participantXml"></param>
        /// <returns></returns>
        private bool CreateXML_EndpointMessage(NS_MESSENGER.Conference conf, NS_MESSENGER.Party party, string messageText, int duration, ref StringBuilder MessageXML)
        {
            try
            {
                MessageXML.Append("<Send_Imci_Request>");
                MessageXML.Append("<RequestID>" + DateTime.Now.ToString("mmddyyyymmhhss") + "</RequestID>");
                MessageXML.Append("<ConfGID>" + conf.sGUID + "</ConfGID>");
                MessageXML.Append("<PID>" + party.sGUID + "</PID>");
                MessageXML.Append("<Message>" + messageText + "</Message>");
                MessageXML.Append("<MinTime>" + duration + "</MinTime>");
                MessageXML.Append("<MaxTime>" + duration + "</MaxTime>");
                MessageXML.Append("<SendToSDS>True</SendToSDS>");
                MessageXML.Append("<IsScrollingText>True</IsScrollingText>");
                MessageXML.Append("<ForceImci>True</ForceImci>");
                MessageXML.Append("</Send_Imci_Request>");

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        #endregion

        #region ConferenceMessage
        /// <summary>
        /// ConferenceMessage
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="participantXml"></param>
        /// <returns></returns>
        private bool CreateXML_ConferenceMessage(NS_MESSENGER.Conference conf, string messageText, int duration, ref StringBuilder MessageXML)
        {
            try
            {
                MessageXML.Append("<Send_Imci_Request>");
                MessageXML.Append("<RequestID>" + DateTime.Now.ToString("mmddyyyymmhhss") + "</RequestID>");
                MessageXML.Append("<ConfGID>" + conf.sGUID + "</ConfGID>");
                MessageXML.Append("<Message>" + messageText + "</Message>");
                MessageXML.Append("<MinTime>" + duration + "</MinTime>");
                MessageXML.Append("<MaxTime>" + duration + "</MaxTime>");
                MessageXML.Append("<SendToSDS>True</SendToSDS>");
                MessageXML.Append("<IsScrollingText>True</IsScrollingText>");
                MessageXML.Append("<ForceImci>True</ForceImci>");
                MessageXML.Append("</Send_Imci_Request>");

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        #endregion


        #region GetEndpointStatus
        /// <summary>
        /// GetEndpointStatus
        /// </summary>
        /// <param name="confGID"></param>
        /// <param name="ParticipantXML"></param>
        /// <returns></returns>
        private bool CreateXML_GetEndpointStatus(string confGID, string PGID, ref string ParticipantXML)
        {
            try
            {
                ParticipantXML = "<Get_Participant_Info_Request>";
                ParticipantXML += "<RequestID>" + DateTime.Now.ToString("mmddyyyymmhhss") + "</RequestID>";
                ParticipantXML += "<ConfGID>" + confGID + "</ConfGID>";
                ParticipantXML += "<PID>" + PGID + "</PID>";
                ParticipantXML += "<GetExtendedInfo>True</GetExtendedInfo>";
                ParticipantXML += "</Get_Participant_Info_Request>";

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        #endregion

        #region Endpoint FECC
        /// <summary>
        /// EndpointFECC
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="participantXml"></param>
        /// <returns></returns>
        private bool CreateXML_ParticipantFECC(NS_MESSENGER.Conference conf, NS_MESSENGER.Party party, string Direction, ref StringBuilder FECCXML)
        {
            try
            {
                FECCXML.Append("<FECC_Request>");
                FECCXML.Append("<RequestID>" + DateTime.Now.ToString("mmddyyyymmhhss") + "</RequestID>");
                FECCXML.Append("<ConfGID>" + conf.sGUID + "</ConfGID>");
                FECCXML.Append("<PID>" + party.sGUID + "</PID>");
                FECCXML.Append("<Action>" + Direction + " </Action>");
                FECCXML.Append("</FECC_Request>");

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        #endregion

        #region LockUnlock Conference
        /// <summary>
        /// LockUnlockConference
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="participantXml"></param>
        /// <returns></returns>
        private bool CreateXML_LockUnlockConference(NS_MESSENGER.Conference conf, bool LockUnLock, ref StringBuilder LockUnlockXML)
        {
            try
            {
                LockUnlockXML.Append("<Freeze_Conference_Request>");
                LockUnlockXML.Append("<RequestID>" + DateTime.Now.ToString("mmddyyyymmhhss") + "</RequestID>");
                LockUnlockXML.Append("<ConfGID>" + conf.sGUID + "</ConfGID>");
                LockUnlockXML.Append("<FreezeOn>" + LockUnLock + "</FreezeOn>");
                LockUnlockXML.Append("</Freeze_Conference_Request>");

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        #endregion

        #region CreateConference
        /// <summary>
        /// CreateConference
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="reservationXML"></param>
        /// <returns></returns>
        private bool CreateXML_CreateConference(NS_MESSENGER.Conference conf, ref string reservationXML)
        {
            NS_MESSENGER.Party party = null;
            int durationInSecs = 0, OrgmaxConcurntCall = 0; //ZD 100518
            try
            {
                db.FetchRadvionMCUDetails(conf.cMcu, ref conf); //ZD 102063 

                durationInSecs = conf.iDuration * 60;
                DateTime StartDate = DateTime.Parse(conf.dtStartDateTime.ToString());

                reservationXML = "<Schedule_Conference_Request>";
                reservationXML += "<RequestID>" + DateTime.Now.ToString("mmddyyyymmhhss") + "</RequestID>";
                reservationXML += "<Conference>";
                reservationXML += "<MemberId>" + conf.cMcu.IViewOrgID + "</MemberId>"; // radvision orgid
                reservationXML += "<Number></Number>";
                reservationXML += "<ServiceTemplateId>" + conf.cMcu.ConfServiceID + "</ServiceTemplateId>";// radvision meeting service id //ZD 100755 Inncrewin Starts //ZD 102063
                reservationXML += "<ServicePrefix>" +  conf.radParam.ServicePrefix.ToString() + "</ServicePrefix>"; //radvision meeting service prefix-Doubt //ZD 100755 Inncrewin Starts //ZD 102063
                reservationXML += "<ReservedPorts>";
                reservationXML += "<Regular>" + conf.iEndpointCount + "</Regular>";
                reservationXML += "</ReservedPorts>";

                if (conf.sPwd != "")
                    reservationXML += "<AccessPIN>" + conf.sPwd + "</AccessPIN>";

                System.Collections.IEnumerator partyEnumerator;
                partyEnumerator = conf.qParties.GetEnumerator();
                int partyCount = conf.qParties.Count;
                for (int i = 0; i < partyCount; i++)
                {
                    partyEnumerator.MoveNext();
                    party = new NS_MESSENGER.Party();
                    party = (NS_MESSENGER.Party)partyEnumerator.Current;
                    if (party.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_OUT)
                    {
                        reservationXML += "<Attendee>";
                        reservationXML += "<Protocol>H323</Protocol>";// Party protocol
                        reservationXML += "<TerminalName>" + party.sName + "</TerminalName>";
                        reservationXML += "<TerminalNumber>" + party.sAddress.Trim() + "</TerminalNumber>";
                        reservationXML += "<MaxBandwidth>384</MaxBandwidth>"; // Party bandwidth or conference
                        reservationXML += "</Attendee>";
                    }
                }
                //ZD 100518 Starts
                db.FetchMaxParticipants(conf.iOrgID.ToString(), ref OrgmaxConcurntCall);
                reservationXML += "<AdvancedProperties>";
                reservationXML += "<MaxParticipants>" + OrgmaxConcurntCall + "</MaxParticipants>";
                reservationXML += "</AdvancedProperties>";
                //ZD 100518 Ends
                reservationXML += "<Subject>" + conf.sExternalName + "</Subject>";
                reservationXML += "<Description>" + conf.sExternalName + ":" + conf.sDescription + "</Description>";
                reservationXML += "<StartTime>" + StartDate.ToString("yyyy-MM-ddTHH:mm:00+00:00") + "</StartTime>"; //FBTDB
                reservationXML += "<Duration>" + "PT" + durationInSecs.ToString() + "S" + "</Duration>";
                reservationXML += "<SendingNotification>false</SendingNotification>";
                reservationXML += "</Conference>";
                reservationXML += "</Schedule_Conference_Request>";

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }


        }
        #endregion

        #region AddParticipant

        private bool CreateXML_AddParticipant(NS_MESSENGER.Conference conf, NS_MESSENGER.Party party, ref StringBuilder participantXml)
        {
            try
            {
                participantXml.Append("<Invite_Participant_Request>");
                participantXml.Append("<RequestID>" + DateTime.Now.ToString("mmddyyyymmhhss") + "</RequestID>");
                participantXml.Append("<ConfGID>" + conf.sGUID + "</ConfGID>");
                participantXml.Append("<Invite_Part_List>");
                participantXml.Append("<Invite_Part>");
                participantXml.Append("<DialStr>" + party.sAddress.Trim() + "</DialStr>");
                participantXml.Append("<PartName>" + party.sName + "</PartName>");
                participantXml.Append("<ProtocolType>H.323</ProtocolType>");// Party protocol
                participantXml.Append("</Invite_Part>");
                participantXml.Append("</Invite_Part_List>");
                participantXml.Append("</Invite_Participant_Request>");

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }

        #endregion

        #region ParticipantLayout
        /// <summary>
        /// EndpointLayout
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="participantXml"></param>
        /// <returns></returns>
        private bool CreateXML_EndpointLayout(NS_MESSENGER.Conference conf, NS_MESSENGER.Party party, int Layout, ref StringBuilder LayoutXML)
        {
            try
            {
                LayoutXML.Append("<Participant_Set_Out_Layout_Request>");
                LayoutXML.Append("<RequestID>" + DateTime.Now.ToString("mmddyyyymmhhss") + "</RequestID>");
                LayoutXML.Append("<ConfGID>" + conf.sGUID + "</ConfGID>");
                LayoutXML.Append("<Out_Layout_Part_List>");
                LayoutXML.Append("<Out_Layout_Part>");
                LayoutXML.Append("<PID>" + party.sGUID + "</PID>");
                LayoutXML.Append("<LID>" + Layout + "</LID>");
                LayoutXML.Append("<LayoutType>Video</LayoutType>");
                LayoutXML.Append("<VideoOutputID></VideoOutputID>");
                LayoutXML.Append("</Out_Layout_Part>");
                LayoutXML.Append("</Out_Layout_Part_List>");
                LayoutXML.Append("</Participant_Set_Out_Layout_Request>");

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        #endregion

        #region ModifyConference
        /// <summary>
        /// ModifyConference
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="reservationXML"></param>
        /// <returns></returns>
        private bool CreateXML_ModifyConference(NS_MESSENGER.Conference conf, ref string reservationXML)
        {
            NS_MESSENGER.Party party = null;
            int durationInSecs = 0, OrgmaxConcurntCall = 0; //ZD 100518
            try
            {
                db.FetchRadvionMCUDetails(conf.cMcu, ref conf); //ZD 102063 

                durationInSecs = conf.iDuration * 60;
                DateTime StartDate = DateTime.Parse(conf.dtStartDateTime.ToString());

                reservationXML = "<Modify_Conference_Request>";
                reservationXML += "<RequestID>" + DateTime.Now.ToString("mmddyyyymmhhss") + "</RequestID>";
                reservationXML += "<Conference>";
                reservationXML += "<MemberId>" + conf.cMcu.IViewOrgID + "</MemberId>"; // radvision orgid
                reservationXML += "<Number></Number>";
                reservationXML += "<ServiceTemplateId>" + conf.cMcu.ConfServiceID + "</ServiceTemplateId>";// radvision meetin service id 
                reservationXML += "<ServicePrefix>" + conf.radParam.ServicePrefix.ToString() + "</ServicePrefix>"; //radvision meeting service prefix //ZD 1012063
                reservationXML += "<ReservedPorts>";
                reservationXML += "<Regular>" + conf.iEndpointCount + "</Regular>";
                reservationXML += "</ReservedPorts>";
                if (conf.sPwd != "")
                    reservationXML += "<AccessPIN>" + conf.sPwd + "</AccessPIN>";

                System.Collections.IEnumerator partyEnumerator;
                partyEnumerator = conf.qParties.GetEnumerator();
                int partyCount = conf.qParties.Count;
                for (int i = 0; i < partyCount; i++)
                {
                    partyEnumerator.MoveNext();
                    party = new NS_MESSENGER.Party();
                    party = (NS_MESSENGER.Party)partyEnumerator.Current;
                    if (party.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_OUT)
                    {
                        reservationXML += "<Attendee>";
                        reservationXML += "<Protocol>H323</Protocol>"; //Party protocol
                        reservationXML += "<TerminalName>" + party.sName + "</TerminalName>";
                        reservationXML += "<TerminalNumber>" + party.sAddress.Trim() + "</TerminalNumber>";
                        reservationXML += "<MaxBandwidth>384</MaxBandwidth>"; // Party bandwidth or conference
                        reservationXML += "</Attendee>";
                    }
                }
                //ZD 100518 Starts
                db.FetchMaxParticipants(conf.iOrgID.ToString(), ref OrgmaxConcurntCall);
                reservationXML += "<AdvancedProperties>";
                reservationXML += "<MaxParticipants>" + OrgmaxConcurntCall + "</MaxParticipants>";
                reservationXML += "</AdvancedProperties>";
                //ZD 100518 Ends
                reservationXML += "<ConferenceId>" + conf.sGUID + "</ConferenceId>";
                reservationXML += "<Subject>" + conf.sExternalName + "</Subject>";
                reservationXML += "<Description>" + conf.sExternalName + ":" + conf.sDescription + "</Description>";
                reservationXML += "<StartTime>" + StartDate.ToString("yyyy-MM-ddTHH:mm:00+00:00") + "</StartTime>"; //FBTDB
                reservationXML += "<Duration>" + "PT" + durationInSecs.ToString() + "S" + "</Duration>";
                reservationXML += "<SendingNotification>false</SendingNotification>";
                reservationXML += "</Conference>";
                reservationXML += "</Modify_Conference_Request>";

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }


        }
        #endregion

        #region CreateXML_GetOrganization
        /// <summary>
        /// CreateXML_GetOrganization
        /// </summary>
        /// <param name="ParticipantXML"></param>
        /// <returns></returns>
        private bool CreateXML_GetOrganization(ref StringBuilder OrgXML)
        {
            try
            {
                OrgXML.Append("<Get_Organization_Request>");
                OrgXML.Append("<RequestID>" + DateTime.Now.ToString("mmddyyyymmhhss") + "</RequestID>");
                OrgXML.Append("</Get_Organization_Request>");

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        #endregion

        #region CreateXML_GetMeetingService
        /// <summary>
        /// CreateXML_GetMeetingService
        /// </summary>
        /// <param name="ParticipantXML"></param>
        /// <returns></returns>
        private bool CreateXML_GetMeetingService(ref StringBuilder ServiceXML, int MemberId)
        {
            try
            {
                ServiceXML.Append("<Get_Meeting_Service_Request>");
                ServiceXML.Append("<RequestID>" + DateTime.Now.ToString("mmddyyyymmhhss") + "</RequestID>");
                ServiceXML.Append("<MemberId>" + MemberId + "</MemberId>");
                ServiceXML.Append("</Get_Meeting_Service_Request>");

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        #endregion

        //ZD 100890 Start

        #region GetVirtualRoom
        /// <summary>
        /// GetVirtualRoom
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="reservationXML"></param>
        /// <returns></returns>
        private bool CreateXML_GetVirtualRoom(NS_MESSENGER.Conference conf, ref StringBuilder VirtualXML)
        {
            try
            {
                int durationInSecs = 0;
                durationInSecs = conf.iDuration * 60;
                DateTime StartDate = DateTime.Parse(conf.dtStartDateTime.ToString());
                NS_MESSENGER.Party party = null;

                VirtualXML.Append("<Get_Virtual_Room_Request>");
                VirtualXML.Append("<RequestID>" + new Random().Next(int.MaxValue).ToString() + "</RequestID>");
                VirtualXML.Append("<MemberId>" + conf.cMcu.IViewOrgID + "</MemberId>");
                VirtualXML.Append("<DialableNumber>" + conf.radParam.StaticIDNumber + "</DialableNumber>");
                VirtualXML.Append("</Get_Virtual_Room_Request>");

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }


        }
        #endregion

        #region Create User
        /// <summary>
        /// CreateXML_CreateUser
        /// </summary>
        /// <param name="MCU"></param>
        /// <param name="Party"></param>
        /// <param name="UserXML"></param>
        /// <returns></returns>
        private bool CreateXML_CreateUser(NS_MESSENGER.MCU MCU, NS_MESSENGER.Party Party, ref StringBuilder UserXML)
        {
            Random rnd = new Random();
            try
            {
                UserXML.Append("<Create_User_Request>");
                UserXML.Append("<RequestID>" + rnd.Next(int.MaxValue).ToString() + "</RequestID>");
                UserXML.Append("<User>");
                UserXML.Append("<MemberId>" + MCU.IViewOrgID + "</MemberId>");
                UserXML.Append("<FirstName>" + Party.sFirstName + "</FirstName>");
                UserXML.Append("<LastName>" + Party.sLastName + "</LastName>");
                UserXML.Append("<LoginID>" + Party.Emailaddress + "</LoginID>");
                UserXML.Append("<Password>" + Party.sPwd + "</Password>");
                UserXML.Append("<TelephoneOffice>" + Party.sWorkPhone + "</TelephoneOffice>");
                UserXML.Append("<TelephoneMobile>" + Party.sCellPhone + "</TelephoneMobile>");
                UserXML.Append("<TimeZoneId>200z</TimeZoneId>"); //Need to check
                //VirtualXML.Append("<DefaultTermialId>200z</DefaultTermialId>"); //Need to check              
                UserXML.Append("<LocationId>NODE-10001</LocationId>"); //Need to check
                UserXML.Append("<UserProfileId>2</UserProfileId>"); //Need to check
                UserXML.Append("<Schedulable>true</Schedulable>");
                UserXML.Append("<Reservable>true</Reservable>");
                UserXML.Append("<AllowStreaming>ON</AllowStreaming >");
                UserXML.Append("<AllowRecording>ON</AllowRecording >");
                UserXML.Append("</User>");
                UserXML.Append("</Create_User_Request>");

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }


        }
        #endregion

        #region Update User
        /// <summary>
        /// CreateXML_UpdateUser
        /// </summary>
        /// <param name="MCU"></param>
        /// <param name="Party"></param>
        /// <param name="UserXML"></param>
        /// <returns></returns>
        private bool CreateXML_UpdateUser(NS_MESSENGER.MCU MCU, NS_MESSENGER.Party Party, ref StringBuilder UserXML)
        {
            Random rnd = new Random();
            try
            {
                UserXML.Append("<Modify_User_Request>");
                UserXML.Append("<RequestID>" + rnd.Next(int.MaxValue).ToString() + "</RequestID>");
                UserXML.Append("<User>");
                UserXML.Append("<MemberId>" + MCU.IViewOrgID + "</MemberId>");
                UserXML.Append("<UserId>" + Party.siViewUserID + "</UserId>");
                UserXML.Append("<FirstName>" + Party.sFirstName + "</FirstName>");
                UserXML.Append("<LastName>" + Party.sLastName + "</LastName>");
                UserXML.Append("<LoginID>" + Party.Emailaddress + "</LoginID>");
                UserXML.Append("<Password>" + Party.sPwd + "</Password>");
                UserXML.Append("<TelephoneOffice>" + Party.sWorkPhone + "</TelephoneOffice>");
                UserXML.Append("<TelephoneMobile>" + Party.sCellPhone + "</TelephoneMobile>");
                UserXML.Append("<TimeZoneId>200z</TimeZoneId>");//Need to check
                UserXML.Append("<LocationId>NODE-10001</LocationId>");//Need to check
                UserXML.Append("<UserProfileId>2</UserProfileId>");//Need to check
                UserXML.Append("<Schedulable>true</Schedulable>");
                UserXML.Append("<Reservable>false</Reservable>");
                UserXML.Append("<AllowStreaming>ON</AllowStreaming >");
                UserXML.Append("<AllowRecording>OFF</AllowRecording >");
                UserXML.Append("</User>");
                UserXML.Append("</Modify_User_Request>");

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }


        }
        #endregion

        #region Delete User
        /// <summary>
        /// CreateXML_DeleteUser
        /// </summary>
        /// <param name="MCU"></param>
        /// <param name="Party"></param>
        /// <param name="UserXML"></param>
        /// <returns></returns>
        private bool CreateXML_DeleteUser(NS_MESSENGER.MCU MCU, NS_MESSENGER.Party Party, ref StringBuilder UserXML)
        {
            Random rnd = new Random();
            try
            {
                UserXML.Append("<Delete_User_Request>");
                UserXML.Append("<RequestID>" + rnd.Next(int.MaxValue).ToString() + "</RequestID>");
                UserXML.Append("<MemberId>" + MCU.IViewOrgID + "</MemberId>");

                if (Party.siViewUserID.Trim() != "")
                    UserXML.Append("<UserId>" + Party.siViewUserID + "</UserId>");
                else
                    UserXML.Append("<UserId>none</UserId>");

                UserXML.Append("</Delete_User_Request>");

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }


        }
        #endregion

        //ZD 100890 End

        #endregion

        #region ResponseXML

        #region KeepAlive
        /// <summary>
        /// Keep Alive Command
        /// </summary>
        /// <param name="responseXML"></param>
        /// <returns></returns>
        private bool ResponseXML_KeepAlive(string responseXML)
        {
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(responseXML);

                string description = xd.SelectSingleNode("//Keep_Alive_Response/ReturnValue").InnerXml.Trim();
                if (description.Length < 0 || description.Trim().ToLower().Contains("ok") != true)
                {
                    this.errMsg = "MCU Test Connection Error: " + description;
                    return false;
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }

        }
        #endregion

        #region TerminateConference
        /// <summary>
        /// TerminateConference
        /// </summary>
        /// <param name="responseXML"></param>
        /// <returns></returns>
        private bool ResponseXML_TerminateConference(string responseXML)
        {
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(responseXML);

                string description = xd.SelectSingleNode("//Terminate_Conference_Response/ReturnValue").InnerXml.Trim();
                if (description.Length < 0 || description.Trim().ToLower().Contains("ok") != true)
                {
                    this.errMsg = "MCU Terminate Conference Error: " + description;
                    return false;
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        #endregion

        //FB 2659 START
        #region CancelConference
        /// <summary>
        /// CancelConference
        /// </summary>
        /// <param name="responseXML"></param>
        /// <returns></returns>
        private bool ResponseXML_CancelConference(string responseXML)
        {
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(responseXML);
                string description = "";

                if (xd.SelectSingleNode("//Cancel_Conference_Response/ReturnValue") != null)
                    description = xd.SelectSingleNode("//Cancel_Conference_Response/ReturnValue").InnerXml.Trim();
                if (description.Length < 0 || description.Contains("OK") != true)
                {
                    this.errMsg = "MCU Cancel Conference Error: " + description;
                    return false;
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        #endregion
        //FB 2659 END

        #region TerminateEndpoint
        /// <summary>
        /// TerminateEndpoint
        /// </summary>
        /// <param name="responseXML"></param>
        /// <returns></returns>
        private bool ResponseXML_TerminateEndpoint(string responseXML)
        {

            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(responseXML);

                string description = xd.SelectSingleNode("//Delete_Part_Response/ReturnValue").InnerXml.Trim();
                if (description.Length < 0 || description.Trim().ToLower().Contains("ok") != true)
                {
                    this.errMsg = "Participant Failed to Delete Error: " + description;
                    return false;
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        #endregion

        #region CreateVirtualRoom
        /// <summary>
        /// CreateVirtualRoom
        /// </summary>
        /// <param name="responseXML"></param>
        /// <returns></returns>
        private bool ResponseXML_CreateVirtualRoom(ref NS_MESSENGER.Party Party, string responseXML)
        {
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(responseXML);

                string description = xd.SelectSingleNode("//Create_Virtual_Room_Response/ReturnValue").InnerXml.Trim();
                if (description.Length < 0 || description.Trim().ToLower().Contains("ok") != true)
                {
                    this.errMsg = "MCU Virtual Room Creation Error: " + description;
                    return false;
                }
                else
                {
                    Party.sVirtualRoomId = xd.SelectSingleNode("//Create_Virtual_Room_Response/VirtualRoomeId").InnerXml.Trim();
                    bool ret = db.UpdateParticipantExternalID(Party);
                    if (!ret)
                    {
                        this.errMsg = "Error in updating iView userID" + Party.siViewUserID;
                        return false;
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        #endregion

        #region ModifyVirtualRoom
        /// <summary>
        /// ModifyVirtualRoom
        /// </summary>
        /// <param name="responseXML"></param>
        /// <returns></returns>
        private bool ResponseXML_ModifyVirtualRoom(string responseXML)
        {
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(responseXML);

                string description = xd.SelectSingleNode("//Modify_Virtual_Room_Response/ReturnValue").InnerXml.Trim();
                if (description.Length < 0 || description.Trim().ToLower().Contains("ok") != true)
                {
                    this.errMsg = "MCU Virtual Room Modify Error: " + description;
                    return false;
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        #endregion

        #region DeleteVirtualRoom
        /// <summary>
        /// DeleteVirtualRoom
        /// </summary>
        /// <param name="responseXML"></param>
        /// <returns></returns>
        private bool ResponseXML_DeleteVirtualRoom(string responseXML)
        {
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(responseXML);

                string description = xd.SelectSingleNode("//Delete_Virtual_Room_Response/ReturnValue").InnerXml.Trim();
                if (description.Length < 0 || description.Trim().ToLower().Contains("ok") != true)
                {
                    this.errMsg = "MCU Virtual Room Delete Error: " + description;
                    return false;
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        #endregion

        #region ExtendConfEndTime
        /// <summary>
        /// ExtendConfEndTime
        /// </summary>
        /// <param name="responseXML"></param>
        /// <returns></returns>
        private bool ResponseXML_ExtendConfEndTime(string responseXML)
        {
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(responseXML);

                string description = xd.SelectSingleNode("//Extend_Conference_Response/ReturnValue").InnerXml.Trim();
                if (description.Length < 0 || description.Trim().ToLower().Contains("ok") != true)
                {
                    this.errMsg = "MCU ExtendConferenceTime Error: " + description;
                    return false;
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        #endregion

        #region MuteEndpoint
        /// <summary>
        /// MuteEndpoint
        /// </summary>
        /// <param name="responseXML"></param>
        /// <returns></returns>
        private bool ResponseXML_MuteEndpoint(string responseXML)
        {
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(responseXML);

                string description = xd.SelectSingleNode("//Part_Media_Chan_Operation_Response/ReturnValue").InnerXml.Trim();
                if (description.Length < 0 || description.Trim().ToLower().Contains("ok") != true)
                {
                    this.errMsg = "Mute Endpoint Error: " + description;
                    return false;
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        #endregion

        #region MuteAllEndpoint
        /// <summary>
        /// MuteAllEndpoint
        /// </summary>
        /// <param name="responseXML"></param>
        /// <returns></returns>
        private bool ResponseXML_MuteAllEndpoint(string responseXML)
        {
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(responseXML);

                string description = xd.SelectSingleNode("//Mute_Unmute_All_Audio_Response/ReturnValue").InnerXml.Trim();
                if (description.Length < 0 || description.Trim().ToLower().Contains("ok") != true)
                {
                    this.errMsg = "Mute All Endpoint Error: " + description;
                    return false;
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        #endregion

        #region MuteVideoEndpoint
        /// <summary>
        /// MuteVideoEndpoint
        /// </summary>
        /// <param name="responseXML"></param>
        /// <returns></returns>
        private bool ResponseXML_MuteVideoEndpoint(string responseXML)
        {
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(responseXML);

                string description = xd.SelectSingleNode("//Part_Media_Chan_Operation_Response/ReturnValue").InnerXml.Trim();
                if (description.Length < 0 || description.Trim().ToLower().Contains("ok") != true)
                {
                    this.errMsg = "Mute Video Endpoint Error: " + description;
                    return false;
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        #endregion

        #region connectEndpoint
        /// <summary>
        /// connectEndpoint
        /// </summary>
        /// <param name="responseXML"></param>
        /// <returns></returns>
        private bool ResponseXML_connectEndpoint(string responseXML)
        {
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(responseXML);

                string description = xd.SelectSingleNode("//Reinvite_Participant_Response/ReturnValue").InnerXml.Trim();
                if (description.Length < 0 || description.Trim().ToLower().Contains("ok") != true)
                {
                    this.errMsg = "connect Endpoint Error: " + description;
                    return false;
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        #endregion

        #region DisconnectEndpoint
        /// <summary>
        /// DisconnectEndpoint
        /// </summary>
        /// <param name="responseXML"></param>
        /// <returns></returns>
        private bool ResponseXML_DisconnectEndpoint(string responseXML, ref int onlineStatus)
        {
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(responseXML);

                string description = xd.SelectSingleNode("//Drop_Participant_Response/ReturnValue").InnerXml.Trim();
                if (description.Length < 0 || description.Trim().ToLower().Contains("ok") != true)
                {
                    this.errMsg = "Disconnect Endpoint Error: " + description;
                    return false;
                }
                onlineStatus = 0;
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        #endregion

        #region AddParticipant
        /// <summary>
        /// AddParticipant   
        /// </summary>
        /// <param name="responseXML"></param>
        /// <returns></returns>
        private bool ResponseXML_AddParticipant(NS_MESSENGER.Conference conf, string responseXML)
        {
            try
            {
                XDocument xmlDoc = null;
                string description = "";
                xmlDoc = XDocument.Parse(responseXML);
                NS_MESSENGER.Party party = new NS_MESSENGER.Party();

                description = xmlDoc.Element("MCU_XML_API").Element("Response").Element("Invite_Participant_Response").Element("ReturnValue").Value;
                if (description.Trim().ToLower().Contains("ok"))
                {
                    var Parties = (from data in xmlDoc.Descendants("Invite_Part_List").Elements("Invite_Part")
                                   select new PartyDetails
                                   {
                                       sAddress = data.Element("DialStr").Value,
                                       GUID = data.Element("PID").Value,
                                       Response = data.Element("ReturnValue").Value,
                                       Onlinestatus = ""
                                   }).ToList();
                    if (Parties.Count > 0)
                    {
                        for (int i = 0; i < Parties.Count; i++)
                        {
                            try
                            {
                                if (Parties[i].Response.ToLower().Trim() == "ok")
                                    db.UpdateConferenceEndpointStatus(conf.iDbID, conf.iInstanceID, Parties[i].sAddress, Parties[i].GUID);

                            }
                            catch (Exception e)
                            {
                                logger.Exception(100, e.Message);
                                this.errMsg = e.Message;
                                return false;
                            }
                        }
                    }
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        #endregion

        #region ParticipantMessage
        /// <summary>
        /// EndpointMessage
        /// </summary>
        /// <param name="responseXML"></param>
        /// <returns></returns>
        private bool ResponseXML_EndpointMessage(string responseXML)
        {
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(responseXML);

                string description = xd.SelectSingleNode("//Send_Imci_Response/ReturnValue").InnerXml.Trim();
                if (description.Length < 0 || description.Trim().ToLower().Contains("ok") != true)
                {
                    this.errMsg = "Endpoint Message Error: " + description;
                    return false;
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        #endregion

        #region ConferenceMessage
        /// <summary>
        /// ConferenceMessage
        /// </summary>
        /// <param name="responseXML"></param>
        /// <returns></returns>
        private bool ResponseXML_ConferenceMessage(string responseXML)
        {
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(responseXML);

                string description = xd.SelectSingleNode("//Send_Imci_Response/ReturnValue").InnerXml.Trim();
                if (description.Length < 0 || description.Trim().ToLower().Contains("ok") != true)
                {
                    this.errMsg = "Conference Message Error: " + description;
                    return false;
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        #endregion

        #region GetEndpointStatus
        private bool Response_GetEndpointStatus(NS_MESSENGER.Conference conf, NS_MESSENGER.Party party, string responseXML)
        {
            try
            {
                XDocument xmlDoc = null;
                string description = "";
                bool ret = false;
                xmlDoc = XDocument.Parse(responseXML);

                description = xmlDoc.Element("MCU_XML_API").Element("Response").Element("Get_Participant_Info _Response").Element("ReturnValue").Value;
                if (description.Trim().ToLower().Contains("ok"))
                {
                    var Parties = (from data in xmlDoc.Descendants("Part")
                                   select new PartyDetails
                                   {
                                       sAddress = data.Element("DialStr").Value,
                                       GUID = data.Element("PID").Value,
                                       Response = "",
                                       Onlinestatus = data.Element("ConSt").Value
                                   }).ToList();
                    if (Parties.Count > 0)
                    {
                        for (int i = 0; i < Parties.Count; i++)
                        {
                            if (Parties[i].sAddress == party.sAddress)
                            {
                                party.sGUID = Parties[i].GUID;
                                party.sAddress = Parties[i].sAddress;
                                if (Parties[0].Onlinestatus == "Connected")
                                    party.etStatus = NS_MESSENGER.Party.eOngoingStatus.FULL_CONNECT;
                                else
                                    party.etStatus = NS_MESSENGER.Party.eOngoingStatus.DIS_CONNECT;
                                ret = db.UpdateConfPartyDetails(party);
                                if (!ret)
                                    return false;
                            }
                        }
                    }

                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }

        #endregion

        #region Endpoint FECC
        /// <summary>
        /// Endpoint FECC
        /// </summary>
        /// <param name="responseXML"></param>
        /// <returns></returns>
        private bool ResponseXML_ParticipantFECC(string responseXML)
        {
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(responseXML);

                string description = xd.SelectSingleNode("//FECC_Response/ReturnValue").InnerXml.Trim();
                if (description.Length < 0 || description.Trim().ToLower().Contains("ok") != true)
                {
                    this.errMsg = "Endpoint FECC Error: " + description;
                    return false;
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        #endregion

        #region LockUnlock Conference
        /// <summary>
        /// LockUnlockConference
        /// </summary>
        /// <param name="responseXML"></param>
        /// <returns></returns>
        private bool ResponseXML_LockUnlockConference(string responseXML)
        {
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(responseXML);

                string description = xd.SelectSingleNode("//Freeze_Conference_Response/ReturnValue").InnerXml.Trim();
                if (description.Length < 0 || description.Trim().ToLower().Contains("ok") != true)
                {
                    this.errMsg = "Conference LockUnlock Error: " + description;
                    return false;
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        #endregion

        #region CreateConference
        /// <summary>
        /// CreateConference
        /// </summary>
        /// <param name="responseXML"></param>
        /// <param name="confGID"></param>
        /// <returns></returns>
        private bool ResponseXML_CreateConference(NS_MESSENGER.Conference conf, string responseXML, ref string confGID, ref String confDialNumber)
        {
            XmlDocument xd = new XmlDocument();
            bool ret = false;
			//FB 2659 Starts
            NS_EMAIL.Email sendEmail = null;
            int isPushtoExternal = 0; //ZD 100190

            try
            {
                xd.LoadXml(responseXML);

                string description  = "", Status  = "", strDetail = "", sliderError = "";
                
                if (xd.SelectSingleNode("//Schedule_Conference_Response/ReturnValue") != null)
                    description = xd.SelectSingleNode("//Schedule_Conference_Response/ReturnValue").InnerXml.Trim();
                
                if (xd.SelectSingleNode("//Schedule_Conference_Response/Report/Success") != null)
                    Status = xd.SelectSingleNode("//Schedule_Conference_Response/Report/Success").InnerXml.Trim();

                if (xd.SelectSingleNode("//Schedule_Conference_Response/Report/Detail") != null)
                    strDetail = xd.SelectSingleNode("//Schedule_Conference_Response/Report/Detail").InnerText.Trim();

                if (xd.SelectSingleNode("//MCU_XML_API/Notification/Message/SDS_Notification/State/StateID") != null)
                    sliderError = xd.SelectSingleNode("//MCU_XML_API/Notification/SDS_Notification/Message/State/StateID").InnerXml.Trim();

                if (description.Length < 0 || description.Trim().ToLower().Contains("ok") != true || Status.ToLower().Contains("false") == true || sliderError != "")
                {
                    this.errMsg = "ReturnValue of MCU: " + strDetail + sliderError;
                    //return false;
                }
				//FB 2659 End
                confDialNumber = "";
                if (xd.SelectSingleNode("//Schedule_Conference_Response/Report/Number") != null)
                    confDialNumber = xd.SelectSingleNode("//Schedule_Conference_Response/Report/Number").InnerXml.Trim();

                if (xd.SelectSingleNode("//Schedule_Conference_Response/Report/ConferenceId") != null)
                    confGID = xd.SelectSingleNode("//Schedule_Conference_Response/Report/ConferenceId").InnerXml.Trim();

                bool isValid = false;
                //ZD 101431
                if (sliderError != "" && (string.IsNullOrEmpty(confGID) || string.IsNullOrEmpty(confDialNumber)))
                {
                    for (int i = 0; i < 5; i++) //ZD 100946 Reduced for 101004
                    {
                        if (GetConferenceList(conf))
                        {
                            isValid = true;
                            break;
                        }

                        if (iHavetoTry > 0)
                            break;
                    }
                    if (!isValid && iHavetoTry == iHaveTried)//ZD 101004
                    {
                        iHavetoTry = 0;//ZD 101004
						//ZD 100190
                        isPushtoExternal = 2;
                        db.UpdatePushToExternal(conf.iDbID, conf.iInstanceID, isPushtoExternal);
                        logger.Trace("Error in Update Conf GUID:" + confGID);
                        sendEmail = new NS_EMAIL.Email(configParams);
                        sendEmail.SendEmailtoHostAndMCUAdmin(conf, this.errMsg);
                        return false;
                    }
                }
                else if (iHaveTried <= 0)//ZD 101004
                {
                    ret = db.UpdateConference(conf.iDbID, conf.iInstanceID, confGID, confDialNumber); //FB 2879
                    if (!ret)
                    {
                        logger.Trace("Error in Update Conf GUID");
                        return false;
                    }
                    else
                        logger.Trace("Successfully Updated Conf GUID:" + confGID);
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        #endregion

        #region ParticipantLayout
        /// <summary>
        /// EndpointLayout
        /// </summary>
        /// <param name="responseXML"></param>
        /// <returns></returns>
        private bool ResponseXML_EndpointLayout(string responseXML)
        {
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(responseXML);

                string description = xd.SelectSingleNode("//Participant_Set_Out_Layout_Response/ReturnValue").InnerXml.Trim();
                if (description.Length < 0 || description.Trim().ToLower().Contains("ok") != true)
                {
                    this.errMsg = "Endpoint Layout Error: " + description;
                    return false;
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        #endregion

        #region ModifyConference
        /// <summary>
        /// ModifyConference
        /// </summary>
        /// <param name="responseXML"></param>
        /// <param name="confGID"></param>
        /// <returns></returns>
        private bool ResponseXML_ModifyConference(NS_MESSENGER.Conference conf, string responseXML, ref string confGID, ref String confDialNumber)
        {
            XmlDocument xd = new XmlDocument();
            bool ret = false;
            NS_EMAIL.Email sendEmail = null;
            int isPushtoExternal = 0; //ZD 100190

            try
            {
                xd.LoadXml(responseXML);

                string description = "", Status = "", strDetail = "", sliderError = "";
                
                if (xd.SelectSingleNode("//Modify_Conference_Response/ReturnValue") != null)
                    description = xd.SelectSingleNode("//Modify_Conference_Response/ReturnValue").InnerXml.Trim();

                if (xd.SelectSingleNode("//Modify_Conference_Response/Report/Success") != null)
                    Status = xd.SelectSingleNode("//Modify_Conference_Response/Report/Success").InnerXml.Trim();

                if (xd.SelectSingleNode("//Modify_Conference_Response/Report/Detail") != null)
                    strDetail = xd.SelectSingleNode("//Modify_Conference_Response/Report/Detail").InnerText.Trim();

                if (xd.SelectSingleNode("//MCU_XML_API/Notification/SDS_Notification/Message/State/StateID") != null)
                    sliderError = xd.SelectSingleNode("//MCU_XML_API/Notification/SDS_Notification/Message/State/StateID").InnerXml.Trim();

                if (description.Length < 0 || description.Trim().ToLower().Contains("ok") != true || Status.ToLower().Contains("false") == true || sliderError != "")
                {
                    this.errMsg = "ReturnValue of MCU: " + strDetail + sliderError;
                    //return false;
                }
               
                confDialNumber = ""; confGID = "";
                if (xd.SelectSingleNode("//Modify_Conference_Response/Report/Number") != null)
                    confDialNumber = xd.SelectSingleNode("//Modify_Conference_Response/Report/Number").InnerXml.Trim();

                if (xd.SelectSingleNode("//Modify_Conference_Response/Report/ConferenceId") != null)
                    confGID = xd.SelectSingleNode("//Modify_Conference_Response/Report/ConferenceId").InnerXml.Trim();

                bool isValid = false;
                if (string.IsNullOrEmpty(confGID) || string.IsNullOrEmpty(confDialNumber))
                {
                    for (int i = 0; i < 5; i++)
                    {
                        if (GetConferenceList(conf))
                        {
                            isValid = true;
                            break;
                        }

                        if (iHavetoTry > 0) //ZD 101004
                            break;
                    }
                    if (!isValid && iHavetoTry == iHaveTried)//ZD 101004
                    {
                        iHavetoTry = 0;//ZD 101004
                        logger.Trace("Error in Update Conf GUID:" + confGID);
                        isPushtoExternal = 2; //ZD 100190
                        db.UpdatePushToExternal(conf.iDbID, conf.iInstanceID, isPushtoExternal);
                        sendEmail = new NS_EMAIL.Email(configParams);
                        sendEmail.SendEmailtoHostAndMCUAdmin(conf, this.errMsg);
                        return false;
                    }
                }
                else
                {
                    ret = db.UpdateConference(conf.iDbID, conf.iInstanceID, confGID, confDialNumber);
                    if (!ret)
                    {
                        logger.Trace("Error in Update Conf GUID");
                        return false;
                    }
                    else
                        logger.Trace("Successfully Updated Conf GUID:" + confGID);
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        #endregion

        #region ResponseXML_GetOrganization
        /// <summary>
        /// ResponseXML_GetOrganization
        /// </summary>
        /// <param name="responseXML"></param>
        /// <returns></returns>
        private bool ResponseXML_GetOrganization(string responseXML, int bridgeId, int bridgeTypeId) //FB 2659
        {
            try
            {
                XmlDocument xd = new XmlDocument();
                XmlNodeList NodeList = null;
                bool ret = false;
                xd.LoadXml(responseXML);

                string description = xd.SelectSingleNode("//Get_Organization_Response/ReturnValue").InnerXml.Trim();
                if (description.Length < 0 || description.Trim().ToLower().Contains("ok") != true)
                {
                    this.errMsg = "GetOrganization Error: " + description;
                    return false;
                }

                NodeList = xd.SelectNodes("//Get_Organization_Response/Organization");
                NS_MESSENGER.ExtMCUSilo MCUOrg = null;
                List<NS_MESSENGER.ExtMCUSilo> MCUOrgList = new List<NS_MESSENGER.ExtMCUSilo>();
                int MemeberID = 0;
                string billingCode = "";
                if (NodeList != null && NodeList.Count > 0)
                {
                    for (int i = 0; i < NodeList.Count; i++)
                    {
                        MCUOrg = new NS_MESSENGER.ExtMCUSilo();

                        MCUOrg.iBridgeId = bridgeId; //FB 2659
                        MCUOrg.iBridgeTypeId = bridgeTypeId;

                        if (NodeList[i].SelectSingleNode("MemberId") != null)
                            int.TryParse(NodeList[i].SelectSingleNode("MemberId").InnerText.Trim(), out MemeberID);
                        MCUOrg.iOrgID = MemeberID;

                        if (NodeList[i].SelectSingleNode("BillingCode") != null)
                            billingCode = NodeList[i].SelectSingleNode("BillingCode").InnerText.Trim();
                        MCUOrg.sBillingCode = billingCode;

                        if (NodeList[i].SelectSingleNode("Name") != null)
                            MCUOrg.sOrgName = NodeList[i].SelectSingleNode("Name").InnerText.Trim();

                        if (NodeList[i].SelectSingleNode("Alias") != null)
                            MCUOrg.sAlias = NodeList[i].SelectSingleNode("Alias").InnerText.Trim();

                        MCUOrgList.Add(MCUOrg);
                    }
                }

                if (MCUOrgList.Count > 0)
                {
                    ret = false;
                    ret = db.InsertMCUOrgDetails(MCUOrgList);
                    if (!ret)
                    {
                        logger.Trace("InsertMCUOrgDetails failed");
                    }
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        #endregion

        #region ResponseXML_GetMeetingService
        /// <summary>
        /// ResponseXML_GetMeetingService
        /// </summary>
        /// <param name="responseXML"></param>
        /// <returns></returns>
        private bool ResponseXML_GetMeetingService(string responseXML, int bridgeId, int bridgeTypeId, int memberId)
        {
            try
            {
                XmlDocument xd = new XmlDocument();
                XmlNodeList NodeList = null;
                bool ret = false;
                xd.LoadXml(responseXML);

                string description = xd.SelectSingleNode("//Get_Meeting_Service_Response/ReturnValue").InnerXml.Trim();
                if (description.Length < 0 || description.ToLower().Contains("ok") != true)
                {
                    this.errMsg = "GetMeetingService Error: " + description;
                    return false;
                }

                NodeList = xd.SelectNodes("//Get_Meeting_Service_Response/MeetingService");
                NS_MESSENGER.ExtMCUService MCUService = null;
                List<NS_MESSENGER.ExtMCUService> MCUServiceList = new List<NS_MESSENGER.ExtMCUService>();
                
                int ServiceId = 0; string Prefix = "";

                if (NodeList != null && NodeList.Count > 0)
                {
                    for (int i = 0; i < NodeList.Count; i++)
                    {
                        ServiceId = 0; Prefix = "";
                        MCUService = new NS_MESSENGER.ExtMCUService();

                        MCUService.iBridgeId = bridgeId;
                        MCUService.iBridgeTypeId = bridgeTypeId;
                        MCUService.iMemberId = memberId;

                        if (NodeList[i].SelectSingleNode("ServiceId") != null)
                            int.TryParse(NodeList[i].SelectSingleNode("ServiceId").InnerText.Trim(), out ServiceId);
                        MCUService.iServiceId = ServiceId;
						//FB 2659 Starts
                        if (NodeList[i].SelectSingleNode("Prefix") != null)
                            Prefix = NodeList[i].SelectSingleNode("Prefix").InnerText.Trim();
                        MCUService.sPrefix = Prefix;
						//FB 2659 End
                        if (NodeList[i].SelectSingleNode("Name") != null)
                            MCUService.sServiceName = NodeList[i].SelectSingleNode("Name").InnerText.Trim();

                        if (NodeList[i].SelectSingleNode("Description") != null)
                            MCUService.sDescription = NodeList[i].SelectSingleNode("Description").InnerText.Trim();

                        MCUServiceList.Add(MCUService);
                    }
                }

                if (MCUServiceList.Count > 0)
                {
                    ret = false;
                    ret = db.InsertMCUServiceDetails(MCUServiceList);
                    if (!ret)
                    {
                        logger.Trace("InsertMCUServiceDetails failed");
                    }
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        #endregion

        #region CreateXML_GetConferenceList
        /// <summary>
        /// CreateXML_GetConferenceList
        /// </summary>
        /// <param name="Conf"></param>
        /// <param name="inXML"></param>
        /// <returns></returns>
        private bool CreateXML_GetConferenceList(NS_MESSENGER.Conference Conf, ref string inXML)
        {
            try
            {
                inXML = "<Get_Conference_Request>";
                inXML += "<RequestID>" + DateTime.Now.ToString("mmddyyyymmhhss") + "</RequestID>";
                inXML += "<MemberId>" + Conf.cMcu.IViewOrgID + "</MemberId>";
                inXML += "<Criteria>";
                inXML += "<Entry>";
                inXML += "<Key>Subject</Key>";
                inXML += "<Value>" + Conf.sExternalName + "</Value>";
                inXML += "</Entry>";
                inXML += "<Entry>";
                inXML += "<Key>StartTime</Key>";
                inXML += "<Value>" + Conf.dtStartDateTime.ToString("yyyy-MM-ddTHH:mm:00+00:00") + "</Value>";
                inXML += "</Entry>";
                inXML += "<Entry>";
                inXML += "<Key>EndTime</Key>";
                inXML += "<Value>" + Conf.dtStartDateTime.AddMinutes(Conf.iDuration).ToString("yyyy-MM-ddTHH:mm:00+00:00") + "</Value>";// ZD 100946
                inXML += "</Entry>";
                inXML += "</Criteria>";
                inXML += "</Get_Conference_Request>";

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        #endregion

        #region ResponseXML_GetConferenceList
        /// <summary>
        /// ResponseXML_GetConferenceList
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="responseXML"></param>
        /// <returns></returns>
        private bool ResponseXML_GetConferenceList(NS_MESSENGER.Conference conf, string responseXML)
        {
            XmlDocument xd = new XmlDocument();
            bool ret = false;
            string confDialNumber = "", confGID = "";
            XmlNodeList NodeList = null;

            try
            {
                xd.LoadXml(responseXML);

                string description = "", sliderError = "";

                if (xd.SelectSingleNode("//Get_Conference_Response/ReturnValue") != null)
                    description = xd.SelectSingleNode("//Get_Conference_Response/ReturnValue").InnerXml.Trim();
               
                if (xd.SelectSingleNode("//Get_Conference_Response/Conference") != null)
                    NodeList = xd.SelectNodes("//Get_Conference_Response/Conference");

                if (xd.SelectSingleNode("//MCU_XML_API/Notification/Message/SDS_Notification/State/StateID") != null)
                    sliderError = xd.SelectSingleNode("//MCU_XML_API/Notification/SDS_Notification/Message/State/StateID").InnerXml.Trim();

                if (sliderError != "")
                    return false;

                if (NodeList != null && NodeList.Count > 0)
                {
                    //for (int i = 0; i < NodeList.Count; i++)
                    //{
                    if (NodeList[0].SelectSingleNode("Number") != null) //Gowniyan-doubt - If Multilpe conf returns na?
                        confDialNumber = NodeList[0].SelectSingleNode("Number").InnerXml.Trim();

                    if (NodeList[0].SelectSingleNode("ConferenceId") != null)
                        confGID = NodeList[0].SelectSingleNode("ConferenceId").InnerXml.Trim();
                    //}
                }
                else // ZD 101004
                {
                    if (iHaveTried < iHavetoTry && globalConf != null)
                    {
                        iHaveTried++;

                        if (isEdit)
                        {
                            if (ModifyConference(globalConf))
                                return true;
                        }
                        else
                        {
                            if (SetConference(globalConf))
                                return true;
                        }
                    }

                    return false;
                }

                if (string.IsNullOrEmpty(confGID) || string.IsNullOrEmpty(confDialNumber))
                {
                    logger.Trace("Error in Update Conf GUID:" + confGID);
                    return false;
                }
                else
                {
                    ret = db.UpdateConference(conf.iDbID, conf.iInstanceID, confGID, confDialNumber);
                    if (!ret)
                    {
                        logger.Trace("Error in Update Conf GUID");
                        return false;
                    }
                    else
                        logger.Trace("Successfully Updated Conf GUID:" + confGID);
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion

        //ZD 100518 Starts
        #region Poll_CreateXML_GetConferenceList
        /// <summary>
        /// Poll_CreateXML_GetConferenceList
        /// </summary>
        /// <param name="Conf"></param>
        /// <param name="inXML"></param>
        /// <returns></returns>
        private bool Poll_CreateXML_GetConferenceList(DateTime StartDateTime, DateTime EndDateTime, NS_MESSENGER.MCU MCU, int editConfId, int orgId, ref string inXML, int TimeZoneID)
        {
            StringBuilder inxml = new StringBuilder();

            try
            {
                StartDateTime = db.ConverttoGMT(StartDateTime, TimeZoneID);
                EndDateTime = db.ConverttoGMT(EndDateTime, TimeZoneID);

                inxml.Append("<Get_Conference_Request>");
                inxml.Append("<RequestID>" + DateTime.Now.ToString("mmddyyyymmhhss") + "</RequestID>");
                inxml.Append("<MemberId>" + MCU.IViewOrgID + "</MemberId>");
                inxml.Append("<Criteria>");
                inxml.Append("<Entry>");
                inxml.Append("<Key>StartTime</Key>");
                inxml.Append("<Value>" + StartDateTime.ToString("yyyy-MM-ddT00:00:00+00:00") + "</Value>");
                inxml.Append("</Entry>");
                inxml.Append("<Entry>");
                inxml.Append("<Key>EndTime</Key>");
                inxml.Append("<Value>" + EndDateTime.ToString("yyyy-MM-ddT23:59:59+00:00") + "</Value>");
                inxml.Append("</Entry>");
                inxml.Append("</Criteria>");
                inxml.Append("</Get_Conference_Request>");

                inXML = inxml.ToString();

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        #endregion

        #region Poll_ResponseXML_GetConferenceList
        /// <summary>
        /// Poll_ResponseXML_GetConferenceList
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="responseXML"></param>
        /// <returns></returns>
        private bool Poll_ResponseXML_GetConferenceList(DateTime StartDateTime, DateTime EndDateTime, NS_MESSENGER.MCU MCU, int editConfId, int orgId, string responseXML, int TimeZoneID, int MeetingId, string StaticID) //ZD 100890
        {
            XmlDocument xd = new XmlDocument();
            bool ret = false;
            int confGUID = 0;
            XmlNodeList NodeList = null;
            string iVewOrgId = "", description = "", iViewDuration = "", Duration = "", Number = "";//ZD 100890
            int ConcurrentCallCnt = 0, totalConfCnt = 0, OrgMaxConCrntCall = 0, editConfGUID = 0, Hour = 0, Min = 0, secs = 0;
            List<int> iViewConfs = null;
            List<string> Confnumberlist = new List<string> ();//ZD 100890
            DateTime ConfStartDateTime = DateTime.Now, ConfEndDateTime = DateTime.Now;
            try
            {
                xd.LoadXml(responseXML);
                iViewConfs = new List<int>();

                StartDateTime = db.ConverttoGMT(StartDateTime, TimeZoneID);
                EndDateTime = db.ConverttoGMT(EndDateTime, TimeZoneID);

                if (xd.SelectSingleNode("//Get_Conference_Response/ReturnValue") != null)
                    description = xd.SelectSingleNode("//Get_Conference_Response/ReturnValue").InnerXml.Trim();

                if (description.Length < 0 || description.ToLower().Contains("ok") != true)
                {
                    this.errMsg = "Poll_ResponseXML_GetConferenceList Error: " + description;
                    return false;
                }
                if (xd.SelectSingleNode("//Get_Conference_Response/Conference") != null)
                    NodeList = xd.SelectNodes("//Get_Conference_Response/Conference");

                if (NodeList != null && NodeList.Count > 0)
                {
                    for (int i = 0; i < NodeList.Count; i++)
                    {
                        if (NodeList[i].SelectSingleNode("MemberId") != null)
                            iVewOrgId = NodeList[i].SelectSingleNode("MemberId").InnerXml.Trim();

                        if (NodeList[i].SelectSingleNode("ConferenceId") != null)
                            int.TryParse(NodeList[i].SelectSingleNode("ConferenceId").InnerXml.Trim(), out confGUID);

                        if (confGUID > 0)
                            db.fetchEditConfGUID(editConfId, ref editConfGUID);

                        if (NodeList[i].SelectSingleNode("StartTime") != null)
                            DateTime.TryParse(NodeList[i].SelectSingleNode("StartTime").InnerXml.Trim(), out ConfStartDateTime);

                        if (NodeList[i].SelectSingleNode("Duration") != null)
                            iViewDuration = NodeList[i].SelectSingleNode("Duration").InnerXml.Trim();

                        //ZD 100890 Start

                        if (NodeList[i].SelectSingleNode("Number") != null)
                        {
                            Number = NodeList[i].SelectSingleNode("Number").InnerXml.Trim();
                            Confnumberlist.Add(Number);
                        }

                        if (MeetingId == 0)
                        {
                            if (Confnumberlist.Count > 0 && StaticID != "")
                            {
                                if (Confnumberlist.Contains(StaticID))
                                {
                                    this.errMsg = "StaticID information is already used";
                                    return false;
                                }
                            }
                        }

                        //ZD 100890 End

                        //PT1H10M0.000S
                        if (iViewDuration.Split('H').Length > 1)
                        {
                            Duration = iViewDuration.Split('T')[1];
                            Duration = Duration.Replace('H', ':').Replace('M', ':').Replace('s', ' ');

                            if (Duration.Split(':').Length > 2)
                            {
                                int.TryParse(Duration.Split(':')[0], out Hour);
                                int.TryParse(Duration.Split(':')[1], out Min);
                                int.TryParse(Duration.Split(':')[2], out secs);
                            }
                            else
                            {
                                int.TryParse(Duration.Split(':')[0], out Min);
                                int.TryParse(Duration.Split(':')[1], out secs);
                            }

                        }
                        ConfEndDateTime = ConfStartDateTime.Add(new TimeSpan(Hour, Min, secs));

                        if (iVewOrgId == MCU.IViewOrgID.ToString())
                        {
                            if (ConfStartDateTime >= StartDateTime && ConfEndDateTime <= EndDateTime)
                            {
                                ConcurrentCallCnt = ConcurrentCallCnt + 1;
                                if (!iViewConfs.Contains(confGUID))
                                    iViewConfs.Add(confGUID);
                            }
                        }
                        if (editConfGUID > 0)
                            if (iViewConfs.Contains(editConfGUID))
                                iViewConfs.Remove(editConfGUID);
                    }
                }
                db.FecthScheduledCalls(StartDateTime.ToString(), EndDateTime.ToString(), editConfId, orgId, ref iViewConfs, ref totalConfCnt);
                db.FetchMaxConcurrentCall(orgId.ToString(), ref OrgMaxConCrntCall);
                if (totalConfCnt >= OrgMaxConCrntCall)
                {
                    this.errMsg = "Concurrent Meeting limit reached.Please contact Admin";
                    return false;

                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion
        //ZD 100518 Ends

        //ZD 100890 Start

        #region Get Virtual Room
        /// <summary>
        /// GetVirtualRoom
        /// </summary>
        /// <param name="responseXML"></param>
        /// <returns></returns>
        private bool ResponseXML_GetVirtualRoom(NS_MESSENGER.Conference conf, string responseXML)
        {
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(responseXML);

                string description = xd.SelectSingleNode("//Get_Virtual_Room_Response/ReturnValue").InnerXml.Trim();
                if (description.Length < 0 || description.Trim().ToLower().Contains("ok") != true)
                {
                    if (description.Length < 0 || description.Trim().ToLower().Contains("not found") == true)
                    {
                        this.errMsg = "Get MCU Virtual Room Error: " + description;
                        return true;
                    }
                }
                else
                {
                    string UserID = xd.SelectSingleNode("//Get_Virtual_Room_Response/VirtualRoom/UserId").InnerXml.Trim();
                    if (conf.radParam.UserID != UserID)
                        return false;
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        #endregion

        #region CreateUser
       /// <summary>
        /// ResponseXML_CreateUser
       /// </summary>
       /// <param name="Party"></param>
       /// <param name="responseXML"></param>
       /// <returns></returns>
        private bool ResponseXML_CreateUser(ref NS_MESSENGER.Party Party, string responseXML)
        {
            try
            {
                XmlDocument xd = new XmlDocument();
                bool ret = false;
                xd.LoadXml(responseXML);

                string description = xd.SelectSingleNode("//Create_User_Response/ReturnValue").InnerXml.Trim();
                if (description.Length < 0 || description.Trim().ToLower().Contains("ok") != true)
                {
                    this.errMsg = "MCU User Creation Error: " + description;
                    return false;
                }
                else
                {
                    Party.siViewUserID = xd.SelectSingleNode("//Create_User_Response/UserId").InnerXml.Trim();
                    ret = db.UpdateParticipantExternalID(Party);
                    if (!ret)
                    {
                        this.errMsg = "Error in updating iView userID" + Party.siViewUserID;
                        return false;
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        #endregion

        #region Update User
        /// <summary>
        /// ResponseXML_UpdateUser
        /// </summary>
        /// <param name="Party"></param>
        /// <param name="responseXML"></param>
        /// <returns></returns>
        private bool ResponseXML_UpdateUser(ref NS_MESSENGER.Party Party, string responseXML, NS_MESSENGER.MCU MCU)
        {
            try
            {
                XmlDocument xd = new XmlDocument();
                bool ret = false;
                xd.LoadXml(responseXML);

                string description = xd.SelectSingleNode("//Modify_User_Response/ReturnValue").InnerXml.Trim();
                if (description.Length < 0 || description.Trim().ToLower().Contains("ok") != true)
                {
                    if (description.Trim().ToLower().Contains("user not existed")) //ZD 102005
                    {
                        CreateUser(MCU, Party);
                    }
                    else
                    {
                        this.errMsg = "MCU User Updation Error: " + description;
                        return false;
                    }
                }
                else
                {
                    Party.siViewUserID = xd.SelectSingleNode("//Modify_User_Response/UserId").InnerXml.Trim();
                    ret = db.UpdateParticipantExternalID(Party);
                    if (!ret)
                    {
                        this.errMsg = "Error in updating iView userID" + Party.siViewUserID;
                        return false;
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        #endregion

        #region Delete User
        /// <summary>
        /// ResponseXML_DeleteUser
        /// </summary>
        /// <param name="responseXML"></param>
        /// <returns></returns>
        private bool ResponseXML_DeleteUser(string responseXML)
        {
            try
            {
                XmlDocument xd = new XmlDocument();
                bool ret = false;
                xd.LoadXml(responseXML);

                string description = xd.SelectSingleNode("//Delete_User_Response/ReturnValue").InnerXml.Trim();
                if (description.Length < 0 || description.Trim().ToLower().Contains("ok") != true)
                {
                    this.errMsg = "MCU User Deleted Error: " + description;
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        #endregion

        //ZD 100890 End

        #endregion


    }
}
