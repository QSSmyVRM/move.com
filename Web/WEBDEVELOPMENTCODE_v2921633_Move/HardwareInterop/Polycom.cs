//ZD 100147 Start
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886 End
/* FILE : PolycomAccord.cs
 * DESCRIPTION : All Polycom MCU api commands are stored in this file. 
 * AUTHOR : Kapil M
 */
namespace NS_POLYCOM
{
	#region References
	using System;
	using System.Xml;
	using System.Net;
	using System.Collections;
    using System.Collections.Generic;
    using System.Text;
    using System.IO;
    using Dart.PowerTCP.Mail;
    using System.Web;
    using System.Diagnostics;
    using System.Xml.Linq;
	#endregion 
	class Polycom
	{
		private NS_LOGGER.Log logger;
		internal string errMsg = null;
		NS_MESSENGER.ConfigParams configParams;
        DateTime mcuCurrentTime = DateTime.MinValue; //FB 2448
        private NS_DATABASE.Database db; //FB 2683

		internal Polycom (NS_MESSENGER.ConfigParams config)
		{
			configParams = new NS_MESSENGER.ConfigParams();
			configParams = config;
			logger = new NS_LOGGER.Log(configParams);
            db = new NS_DATABASE.Database(configParams); //FB 2683
		}
		private bool SendTransaction_overXAP(string send,ref string receive)
		{
			// This is used for legacy pre-7.0 firmware version Accord bridges.
			// It uses the XAP DLL to communicate on port 5001 to the bridge.
			// For bridges with 7.0 and above firmware version use the XMLHTTP(). 

            
            this.errMsg = "Polycom MGC bridges with firmware version 7 or below are not supported.";
            return false;

            /*

			// if string empty return error
			if (send.Length < 1) 
			{
				logger.Exception (100,"Data being sent to bridge is empty string. sendXML = " + send);
				return false;
			}
			
			try
			{
				DOMDocument40 objDOMDocSend = new DOMDocument40();
				DOMDocument40 objDOMDocRecv = new DOMDocument40();
				objDOMDocSend.async = false;
				objDOMDocRecv.async= false;
				
				//load the string in DOM.
				if (objDOMDocSend.loadXML(send))
				{
					logger.Trace("SEND TRANS () : SEND = " + objDOMDocSend.xml );
						
					//Create instance of XAP DLL
					xap_Net.TransactionClass  xapTrans = new xap_Net.TransactionClass();
					xap_Net.ITransaction iXap = (xap_Net.ITransaction) xapTrans;

					iXap.ExecuteDom(objDOMDocSend,objDOMDocRecv);
					Marshal.ReleaseComObject(xapTrans);	
										
					receive = objDOMDocRecv.xml;
		
					logger.Trace("SEND TRANS () : RECEIVE = " + receive);				
					return true;
				}
				else
				{
					string debugString = "Error sending data to bridge. XML parser error = " + objDOMDocSend.parseError.reason + objDOMDocSend.parseError.srcText;
					logger.Exception(100,debugString);
					return false;
				}						
			}
			catch(COMException e)
			{
				this.errMsg = e.Message;
				string debugString = "Error sending data to bridge. Error = " + e.Message;
				debugString = "SendXML = " + send + " , ReceiveXML = " + receive ;
				logger.Exception(100,debugString);
				return false;				
			}
			catch(Exception e)
			{ 
				this.errMsg = e.Message;
				string debugString = "Error sending data to bridge. Error = " + e.Message;
				debugString = "SendXML = " + send + " , ReceiveXML = " + receive ;
				logger.Exception(100,debugString);
				return false;
			}			
            */
		}

		private bool SendTransaction_overXMLHTTP (NS_MESSENGER.MCU mcu,string sendXML,ref string receiveXML)
		{
			// Used for Accord bridges with 7.0 and above firmware versions. 
			// Connects to bridge on HTTP . Default is port 80.

			// if string empty return error
			if (sendXML.Length < 1) 
			{
				logger.Exception (100,"Data being sent to bridge is empty string. sendXML = " + sendXML);
				return false;
			}

			try
			{
                int tryAgainCount =0;
                bool tryAgain = false;
                do
                {
                    tryAgain = false;

                    logger.Trace("*********SendXML***********");
                    logger.Trace(sendXML);

                    ServicePointManager.Expect100Continue = false;
                    string strUrl = "http://" + mcu.sIp.Trim() + ":"+mcu.iHttpPort;//Code added for Api port
                    if(mcu.iHttpPort == 443)
                        strUrl = "https://" + mcu.sIp.Trim() ;//Code added for Api port
                    Uri mcuUri = new Uri(strUrl);
                    NetworkCredential mcuCredential = new NetworkCredential();
                    CredentialCache mcuCredentialCache = new CredentialCache();
                    mcuCredentialCache.Add(mcuUri, "Basic", mcuCredential);

                    System.Net.ServicePointManager.ServerCertificateValidationCallback =
                     ((sender1, certificate, chain, sslPolicyErrors) => true);

                    HttpWebRequest req = (HttpWebRequest)WebRequest.Create(strUrl);
                    req.Credentials = mcuCredentialCache;
                        
                    req.ContentType = "application/xml";  //x-www-form-urlencoded";
                    req.Method = "POST";
                    System.IO.Stream stream = req.GetRequestStream();
                    byte[] arrBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(sendXML);
                    stream.Write(arrBytes, 0, arrBytes.Length);
                    stream.Close();

                    

                    WebResponse resp = req.GetResponse();
                    Stream respStream = resp.GetResponseStream();
                    StreamReader rdr = new StreamReader(respStream, System.Text.Encoding.ASCII);
                    receiveXML = rdr.ReadToEnd();
                    logger.Trace("*********ReceiveXML***********");
                    logger.Trace(receiveXML);

                    // FB#845: check for expired token error "STATUS_FAIL_TO_LOCATE_USER"
                    if (receiveXML.Contains("STATUS_FAIL_TO_LOCATE_USER") || receiveXML.Contains("User not found"))
                    {
                        NS_DATABASE.Database db = new NS_DATABASE.Database(this.configParams);

                        // delete the existing tokens
                        db.DeleteMcuTokens();//FB 2261

                        // save the old tokens
                        string oldMcuToken = mcu.sToken;
                        string oldUserToken = mcu.sUserToken;

                        // generate new tokens
                        bool ret = LoginToBridge(ref mcu);
                        if (!ret)
                        {
                            logger.Exception(100, "Login to MCU failed.");
                            return false;
                        }

                        // try the transaction again. Replace the sendXML tokens with the new ones.
                        sendXML = sendXML.Replace("<MCU_TOKEN>" + oldMcuToken + "</MCU_TOKEN>", "<MCU_TOKEN>" + mcu.sToken + "</MCU_TOKEN>");
                        sendXML = sendXML.Replace("<MCU_USER_TOKEN>" + oldUserToken + "</MCU_USER_TOKEN>", "<MCU_USER_TOKEN>" + mcu.sUserToken + "</MCU_USER_TOKEN>");

                        if (tryAgainCount < 2)
                        {
                            tryAgainCount++;
                            tryAgain = true;
                        }
                    }
                }
                while (tryAgain== true);

                return true;
			
			}
			catch(Exception e)
			{ 
				this.errMsg = e.Message;
				string debugString = "Error sending data to bridge. Error = " + e.Message;
				debugString = "SendXML = " + sendXML + " , ReceiveXML = " + receiveXML ;
				logger.Exception(100,debugString);
				return false;
			}

		}
        //ZD 100369_MCU Start
        private bool SendMCUStatusCommand_overXMLHTTP(NS_MESSENGER.MCU mcu, string sendXML, ref string receiveXML, int Timeout)
        {
            // Used for Accord bridges with 7.0 and above firmware versions. 
            // Connects to bridge on HTTP . Default is port 80.

            // if string empty return error
            if (sendXML.Length < 1)
            {
                logger.Exception(100, "Data being sent to bridge is empty string. sendXML = " + sendXML);
                return false;
            }

            try
            {
                int tryAgainCount = 0;
                bool tryAgain = false;
                do
                {
                    tryAgain = false;

                    logger.Trace("*********SendXML***********");
                    logger.Trace(sendXML);

                    string strUrl = "http://" + mcu.sIp.Trim() + ":" + mcu.iHttpPort;//Code added for Api port
                    Uri mcuUri = new Uri(strUrl);
                    NetworkCredential mcuCredential = new NetworkCredential();
                    CredentialCache mcuCredentialCache = new CredentialCache();
                    mcuCredentialCache.Add(mcuUri, "Basic", mcuCredential);
                    HttpWebRequest req = (HttpWebRequest)WebRequest.Create(strUrl);
                    req.Credentials = mcuCredentialCache;
                    req.Timeout = Timeout;
                    req.ContentType = "application/xml";  //x-www-form-urlencoded";
                    req.Method = "POST";
                    System.IO.Stream stream = req.GetRequestStream();
                    byte[] arrBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(sendXML);
                    stream.Write(arrBytes, 0, arrBytes.Length);
                    stream.Close();



                    WebResponse resp = req.GetResponse();
                    Stream respStream = resp.GetResponseStream();
                    StreamReader rdr = new StreamReader(respStream, System.Text.Encoding.ASCII);
                    receiveXML = rdr.ReadToEnd();
                    logger.Trace("*********ReceiveXML***********");
                    logger.Trace(receiveXML);

                    // FB#845: check for expired token error "STATUS_FAIL_TO_LOCATE_USER"
                    if (receiveXML.Contains("STATUS_FAIL_TO_LOCATE_USER") || receiveXML.Contains("User not found"))
                    {
                        NS_DATABASE.Database db = new NS_DATABASE.Database(this.configParams);

                        // delete the existing tokens
                        db.DeleteMcuTokens();//FB 2261

                        // save the old tokens
                        string oldMcuToken = mcu.sToken;
                        string oldUserToken = mcu.sUserToken;

                        // generate new tokens
                        bool ret = LoginToBridge(ref mcu);
                        if (!ret)
                        {
                            logger.Exception(100, "Login to MCU failed.");
                            return false;
                        }

                        // try the transaction again. Replace the sendXML tokens with the new ones.
                        sendXML = sendXML.Replace("<MCU_TOKEN>" + oldMcuToken + "</MCU_TOKEN>", "<MCU_TOKEN>" + mcu.sToken + "</MCU_TOKEN>");
                        sendXML = sendXML.Replace("<MCU_USER_TOKEN>" + oldUserToken + "</MCU_USER_TOKEN>", "<MCU_USER_TOKEN>" + mcu.sUserToken + "</MCU_USER_TOKEN>");

                        if (tryAgainCount < 2)
                        {
                            tryAgainCount++;
                            tryAgain = true;
                        }
                    }
                }
                while (tryAgain == true);

                return true;

            }
            catch (Exception e)
            {
                this.errMsg = e.Message;
                string debugString = "Error sending data to bridge. Error = " + e.Message;
                debugString = "SendXML = " + sendXML + " , ReceiveXML = " + receiveXML;
                logger.Exception(100, debugString);
                return false;
            }

        }
        //ZD 100369_MCU End

		#region Login to Bridge
		private bool LoginToBridge(ref NS_MESSENGER.MCU bridge)
		{			
			logger.Trace("LoginToBridge()");
			try
			{			
				// check database if the tokens are present or not for this mcu
				NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
				bool ret = db.FetchMcuTokens(bridge.iDbId,ref bridge.sToken,ref bridge.sUserToken,false);//FB 2261
				
				// check if tokens are valid
				if (bridge.sToken == null || bridge.sUserToken == null || bridge.etType == NS_MESSENGER.MCU.eType.ACCORDv6)
				{
					logger.Trace ("Bridge tokens are null.");

					// Create the login record 
					string loginXml = null; ret = false;
					ret = CreateXML_Login (ref bridge,ref loginXml);
					if (!ret) 
					{
						logger.Exception (100,"CreateXML failed for bridge .Bridge Name = " + bridge.sName);
						return false;
					}


					// Login to the bridge.
					string responseXml = null;ret= false;
					if (bridge.etType == NS_MESSENGER.MCU.eType.ACCORDv6)
					{
						ret = SendTransaction_overXAP(loginXml,ref responseXml);
					}
					else
					{
						ret = SendTransaction_overXMLHTTP(bridge,loginXml,ref responseXml);
					}

					if (!ret) 
					{
						logger.Exception (100," SendTransaction failed for bridge .Bridge Name = " + bridge.sName);
						return false; 
					}

					// Parse the response.
					ret = false;
					ret = ProcessXML_Login(ref bridge,responseXml);
					if (!ret) 
					{
						logger.Exception (100," ProcessXML failed for bridge .Bridge Name = " + bridge.sName);
						return false; 
					}
				
					// Save these tokens in the database 
					db.InsertMcuTokens(bridge.iDbId,bridge.sToken,bridge.sUserToken);//FB 2261
				}
				else
				{
					// tokens are still valid and can be used. 
					// hence, update the timesnapshot for the tokens
					db.UpdateMcuTokens(bridge.iDbId);//FB 2261
				}
				
				return true;
			}
			catch (Exception e)
			{
				logger.Exception (100,e.Message);
				return false;
			}
		}// end of BridgeLogin()

		private bool CreateXML_Login(ref NS_MESSENGER.MCU bridge,ref string loginXml)
		{
			// Create the login xml string.
			logger.Trace("LOGIN XML()");

			try
			{
				if (bridge.sIp.Length < 1 || bridge.sLogin.Length < 1 || bridge.sPwd.Length < 1 )
				{
					//Check for empty fields.
					logger.Exception(100,"Empty bridge fields");
					return false;
				}
				loginXml = "<TRANS_MCU><TRANS_COMMON_PARAMS>";
				loginXml += "<MCU_TOKEN>0</MCU_TOKEN>";
				loginXml += "<MCU_USER_TOKEN>0</MCU_USER_TOKEN>";
				loginXml += "<SYNC></SYNC>";
				loginXml += "</TRANS_COMMON_PARAMS>";
				loginXml += "<LOGIN><MCU_IP>";
				
				loginXml += "<IP>" + bridge.sIp + "</IP>";
				loginXml += "</MCU_IP>";
				loginXml += "<USER_NAME>" + bridge.sLogin + "</USER_NAME>";
				loginXml += "<PASSWORD>" + bridge.sPwd + "</PASSWORD>";
				loginXml += "<STATION_NAME>" + bridge.sStation + "</STATION_NAME>";
				loginXml += "</LOGIN></TRANS_MCU>";
				return true;
			}
			catch(Exception e)
			{ 
				logger.Exception (100,e.Message);
				return false;
			}
		}

		private bool ProcessXML_Login(ref NS_MESSENGER.MCU bridge,string responseXml)
		{
			logger.Trace ("ProcessXML_Login");

			try
			{
				if (responseXml.Length < 1) return false; 	// empty login response 
				
				XmlDocument xd = new XmlDocument();
				xd.LoadXml(responseXml);

				string description = xd.GetElementsByTagName("DESCRIPTION").Item(0).InnerText.Trim();
				if (description.Contains("OK") !=true)
				{
					this.errMsg = description;
					logger.Exception(100,"Login to MCU failed. ResponseXML = " + responseXml);
					return false; //error in connection
				}
				// bridge connection is ok
				bridge.sToken = xd.GetElementsByTagName("MCU_TOKEN").Item(0).InnerText.Trim();
                bridge.sUserToken = xd.GetElementsByTagName("MCU_USER_TOKEN").Item(0).InnerText.Trim();

				if (bridge.sToken.Length <1 || bridge.sUserToken.Length <1) 
				{
					// empty values . return error.
					this.errMsg = "Login tokens from MCU are not valid.";
					logger.Exception(100,"Login token from bridge is not valid. ResponseXML = " + responseXml );
					return false ; 
				}
			}
			catch (Exception e)
			{
				this.errMsg = e.Message;
				string debugString = "Login to bridge failed. Error = " + e.Message;
				debugString += ". ResponseXML = " + responseXml;
				logger.Exception(100,debugString);
				return false;
			}
		
			return true; 
		}

		private bool CreateXML_CommonParams(NS_MESSENGER.MCU bridge,ref string commonXML)
		{
			try
			{
				// MCU-specific common transaction parameters.
				commonXML = "<TRANS_COMMON_PARAMS>";
				commonXML += "<MCU_TOKEN>" + bridge.sToken + "</MCU_TOKEN>";
				commonXML += "<MCU_USER_TOKEN>"+ bridge.sUserToken + "</MCU_USER_TOKEN>";
				commonXML += "<SYNC /></TRANS_COMMON_PARAMS>";
				return true;
			}
			catch(Exception e)
			{
				logger.Exception(100,e.Message);
				return false;
			}
		}

		#endregion

        internal bool SetConference(NS_MESSENGER.Conference conf, ref string outxml) //ZD 100522
		{
            logger.Trace("Entering SetConference function...");

			try
			{
				// Setup conferences on the bridge
							
				// Login to the bridge
				bool ret = LoginToBridge(ref conf.cMcu);
				if (!ret) 
				{
					logger.Exception (100,"Login to bridge failed. Bridge Name = " + conf.cMcu.sName);
					return false; 
				}

				// Step 1: Fetch common parameters.
				ret = false;
				string commonXML = null;
				ret = CreateXML_CommonParams(conf.cMcu,ref commonXML);
				if (!ret || commonXML.Length < 1)	
				{
					logger.Exception (100,"Creation of CreateXML_CommonParams failed . commonXML = " + commonXML );
					return false; 
				}

				// Step 2: Create the reservation xml string with data
				string reservationXML = null;ret = false;
				ret = CreateXML_Reservation(ref conf,ref reservationXML);
				if (!ret || reservationXML.Length < 1) 
				{
					logger.Exception (100,"Creation of CreateXML_Reservation failed . commonXML = " + reservationXML);
					return false;
				}

				// Combine the steps : Create the full conference xml
				string confXml= null;
				confXml = "<TRANS_RES_1>";
				confXml += commonXML;
				confXml += "<START>";
				confXml += reservationXML;
				confXml += "</START>";
				confXml += "</TRANS_RES_1>";
			
				//Send the xml to the bridge
				ret=false;
				string responseXml = null;
				if (conf.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv6)
				{
                    logger.Trace("MGC - Accordv6");
					ret = SendTransaction_overXAP(confXml,ref responseXml);
				}
				else
				{
                    logger.Trace("All polycom mcu except MGC - Accordv6");
					ret = SendTransaction_overXMLHTTP(conf.cMcu,confXml,ref responseXml);
				}
				if (!ret) 
				{
					logger.Exception (100,"SendTransaction failed . confXml = " + confXml );
					return false; 
				}

				// Parse the response.
				ret=false;
                ret = ProcessXML_Reservation(responseXml, conf, ref outxml);// FB 2683 //ZD 100522
				if (!ret) 
				{
					logger.Exception (100,"ProcessXML_Reservation failed . responseXml = " + responseXml );
					return false; 
				}

				//FB 2553-RMX-Commented
                //if (conf.bLectureMode)
                //{
                //    NS_MESSENGER.Party party = new NS_MESSENGER.Party();
                //    ret = false;
                //    ret = OngoingConfOps("SetLectureMode", conf, party, ref this.errMsg, false, 0, 0, false, ref party.etStatus, false, false); //FB 2553
                //    if (!ret)
                //    {
                //        logger.Trace("Error in setting up lecture mode.");
                //    }
                //}
				return true;
			}
			catch (Exception e)
			{
				logger.Exception(100,e.Message);
				return false;
			}
		}
		
		#region Reservation
		private bool CreateXML_Reservation(ref NS_MESSENGER.Conference conf,ref string reservationXml)
		{
			try
			{
				#region reservation xml
				reservationXml = "<RESERVATION>";	
				
				//name
				reservationXml += "<NAME>" + conf.sDbName + "</NAME>";
				
				reservationXml += "<ID>"+ conf.iDbNumName.ToString() +"</ID>";
				reservationXml += "<REMARK>myVRM conference</REMARK> ";
				reservationXml += "<NETWORK>h320_h323</NETWORK> ";
 
				// conference type
				if (conf.etMediaType == NS_MESSENGER.Conference.eMediaType.AUDIO) 
				{
					//audio-only
					reservationXml += "<MEDIA>audio</MEDIA>";
				}
				else
				{	//audio & video conf
					reservationXml += "<MEDIA>video_audio</MEDIA>";
				}

                // conference password
                if (conf.iPwd == 0)
                {
                    reservationXml += "<PASSWORD></PASSWORD> ";
                }
                else
                {
                    // MAGIC NUMBER "1" added in the suffix to distinguish it from the entry password tag below.
                    reservationXml += "<PASSWORD>" + conf.iPwd.ToString() + "1" + "</PASSWORD> "; 
                }

				// video session 
				if (conf.etVideoSession == NS_MESSENGER.Conference.eVideoSession.SWITCHING)
					reservationXml += "<VIDEO_SESSION>switching</VIDEO_SESSION>";
				else
				{
					if (conf.etVideoSession == NS_MESSENGER.Conference.eVideoSession.CONTINOUS_PRESENCE)
					{
						if (conf.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv6)
						{
							// Spelling mistake in the xml value which was fixed in XAP version 7.
							// Hence, need this to support legacy 6.0 bridges.
							reservationXml += "<VIDEO_SESSION>contiuous_presence</VIDEO_SESSION>";
						}
						else
						{
							reservationXml += "<VIDEO_SESSION>continuous_presence</VIDEO_SESSION>";
						}						
					}
					else
						reservationXml += "<VIDEO_SESSION>transcoding</VIDEO_SESSION>";		
				}
				
				
				#region video codecs 
				bool ret = false; string polycomVideoCodec = "auto";
				ret = EquateVideoCodecs(conf.etVideoCodec,ref polycomVideoCodec);
				if (!ret)
				{
					logger.Trace("Invalid Video Codec.");
					return false;
				}
				if (this.configParams.client == "VAOHIO")
				{
					polycomVideoCodec = "h263";
				}
				reservationXml += "<VIDEO_PROTOCOL>" + polycomVideoCodec + "</VIDEO_PROTOCOL>";
				#endregion

				#region linerate 
				ret = false; string polycomLineRate = "384"; // default;
				ret = EquateLineRate(conf.stLineRate.etLineRate,true,ref polycomLineRate);
				if (!ret)
				{
					logger.Trace("Invalid Line Rate.");
				}
				reservationXml += "<TRANSFER_RATE>" + polycomLineRate + "</TRANSFER_RATE>";
				#endregion 
								
				#region Audio Codec
				ret = false; string polycomAudioCodec = "auto";
				ret = EquateAudioCodec(conf.etAudioCodec,ref polycomAudioCodec);
				if (!ret)
				{
					logger.Trace("Invalid Audio Codec.");
					return false;
				}

				if (this.configParams.client == "VAOHIO")
				{
					polycomAudioCodec = "g722_56";
				}
				reservationXml += "<AUDIO_RATE>" + polycomAudioCodec + "</AUDIO_RATE>";
				#endregion

				reservationXml += "<VIDEO_FORMAT>auto</VIDEO_FORMAT>"; 
				reservationXml += "<FRAME_RATE>auto</FRAME_RATE> ";

                #region Message Service Type

                logger.Trace("Service Type:" + conf.etMessageServiceType); //PSU RMX Fix
               
                //Message Service Type
                if (conf.etMessageServiceType == NS_MESSENGER.Conference.eMsgServiceType.NONE)
                {
                    reservationXml += "<ATTENDED_MODE>none</ATTENDED_MODE> ";
                }
                else
                {
                    if (conf.etMessageServiceType == NS_MESSENGER.Conference.eMsgServiceType.WELCOME_ONLY)
                    {
                        reservationXml += "<ATTENDED_MODE>welcome_only</ATTENDED_MODE> ";
                    }
                    else
                    {
                        if (conf.etMessageServiceType == NS_MESSENGER.Conference.eMsgServiceType.ATTENDED)
                        {
                            reservationXml += "<ATTENDED_MODE>attended</ATTENDED_MODE> ";
                        }
                        else
                        {
                            reservationXml += "<ATTENDED_MODE>ivr</ATTENDED_MODE> ";
                        }
                    }
                }
                #endregion

                // Message Service Name 
                if (conf.sMessageServiceName != null)
                {
                    reservationXml += "<AV_MSG>"+ conf.sMessageServiceName +"</AV_MSG> ";
                }
                else
                {
                    reservationXml += "<AV_MSG></AV_MSG> ";
                }
                
				reservationXml += "<RESTRICT_MODE>derestricted</RESTRICT_MODE> ";
				reservationXml += "<T120_RATE>none</T120_RATE> ";
                if(conf.bEntryNotification)
                    reservationXml += "<ENTRY_TONE>true</ENTRY_TONE>"; 
                else
				    reservationXml += "<ENTRY_TONE>false</ENTRY_TONE>";
                
                if (conf.bExitNotification)
				    reservationXml += "<EXIT_TONE>true</EXIT_TONE> ";
                else
                    reservationXml += "<EXIT_TONE>false</EXIT_TONE> ";

                if (conf.bEndTimeNotification)
                    reservationXml += "<END_TIME_ALERT_TONE>5</END_TIME_ALERT_TONE> ";
                else
                    reservationXml += "<END_TIME_ALERT_TONE>off</END_TIME_ALERT_TONE> ";

				reservationXml += "<STAND_BY>false</STAND_BY>";
				reservationXml += "<PEPOLE_AND_CONTENT>false</PEPOLE_AND_CONTENT>";
				reservationXml += "<ANNEX_N>false</ANNEX_N> ";
				reservationXml += "<ANNEX_P>false</ANNEX_P> ";
				reservationXml += "<ANNEX_F>false</ANNEX_F> ";
				reservationXml += "<OPERATOR_CONF>false</OPERATOR_CONF>";
				reservationXml += "<SAME_LAYOUT>false</SAME_LAYOUT>";
				reservationXml += "<DUO_VIDEO>false</DUO_VIDEO>";
				

                // FB case#582
                reservationXml += "<AUTO_TERMINATE>";
                    if (configParams.stAutoTerminateCall.On)
                    {
                        reservationXml += "<ON>true</ON>";
                        reservationXml += "<TIME_BEFORE_FIRST_JOIN>"+ configParams.stAutoTerminateCall.Time_Before_First_Join.ToString() +"</TIME_BEFORE_FIRST_JOIN>";
                        reservationXml += "<TIME_AFTER_LAST_QUIT>" +  configParams.stAutoTerminateCall.Time_After_Last_Quit.ToString()+ "</TIME_AFTER_LAST_QUIT>";
                    }
                    else
                    {
                        reservationXml += "<ON>false</ON>";
                        reservationXml += "<TIME_BEFORE_FIRST_JOIN>6</TIME_BEFORE_FIRST_JOIN>";
                        reservationXml += "<TIME_AFTER_LAST_QUIT>1</TIME_AFTER_LAST_QUIT>";
                    }
				reservationXml += "</AUTO_TERMINATE>";

				reservationXml += "<AUDIO_MIX_DEPTH>3</AUDIO_MIX_DEPTH>";
				reservationXml += "<TALK_HOLD_TIME>150</TALK_HOLD_TIME>";
				reservationXml += "<MAX_PARTIES>automatic</MAX_PARTIES>";
				reservationXml += "<MEET_ME_PER_CONF>";
					reservationXml += "<ON>false</ON>";
					reservationXml += "<AUTO_ADD>false</AUTO_ADD>";
					reservationXml += "<MIN_NUM_OF_PARTIES>0</MIN_NUM_OF_PARTIES>";
					reservationXml += "<SERVICE>";
						reservationXml += "<NAME/>";
						reservationXml += "<PHONE1/>";
						reservationXml += "<PHONE2/>";
					reservationXml += "</SERVICE>";
				reservationXml += "</MEET_ME_PER_CONF>";
				reservationXml += "<CHAIR_MODE>none</CHAIR_MODE>";
				reservationXml += "<RESOURCE_FORCE>";
					reservationXml += "<AUDIO_BOARD>automatic</AUDIO_BOARD>";
					reservationXml += "<VIDEO_BOARD>automatic</VIDEO_BOARD>";
					reservationXml += "<DATA_BOARD>automatic</DATA_BOARD>";
					reservationXml += "<DATA_UNIT>automatic</DATA_UNIT>";
				if (conf.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv7)
				{
					reservationXml += "<AUDIO_UNIT>automatic</AUDIO_UNIT>";
					reservationXml += "<VIDEO_UNIT>automatic</VIDEO_UNIT>"; 
				}
				reservationXml += "</RESOURCE_FORCE>";
				reservationXml += "<ADVANCED_AUDIO>auto</ADVANCED_AUDIO>";
				reservationXml += "<H323_BIT_RATE>384</H323_BIT_RATE>";
				reservationXml += "<LEADER_PASSWORD /> ";
				reservationXml += "<TERMINATE_AFTER_LEADER_EXIT>false</TERMINATE_AFTER_LEADER_EXIT> ";
				reservationXml += "<START_CONF_LEADER>false</START_CONF_LEADER> ";
				reservationXml += "<REPEATED_ID>-1</REPEATED_ID>";
				reservationXml += "<MEETING_ROOM>" ;
					reservationXml += "<ON>false</ON>";
					reservationXml += "<LIMITED_SEQ>off</LIMITED_SEQ>";				
				//if (conf.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv7)
				//{
				//	reservationXml += "<MR_STATE>passive</MR_STATE>";
				//}
				reservationXml += "</MEETING_ROOM>" ;
                reservationXml += "<VISUAL_CONCERT>false</VISUAL_CONCERT>";
				// conference start time & duration
				TimeSpan timeDiff = conf.dtStartDateTimeInUTC.Subtract(DateTime.UtcNow);
				if (timeDiff.Days !=0 || timeDiff.Hours !=0 || timeDiff.Minutes >= 5)
				{
					// scheduled reservation
					reservationXml += "<START_TIME>" + conf.dtStartDateTimeInUTC.ToString("s") + "</START_TIME>";
				}
				else
				{
					// immediate conf. so it needs adjustment.
					reservationXml += "<START_TIME>" + DateTime.UtcNow.AddSeconds(15).ToString("s") + "</START_TIME>";
				}
				

				reservationXml += "<DURATION>";
					reservationXml += "<HOUR>" + (conf.iDuration /60) + "</HOUR>";
					reservationXml += "<MINUTE>" + (conf.iDuration %60) + "</MINUTE>";
                    reservationXml += "<SECOND>0</SECOND>";
				reservationXml += "</DURATION>";

                //ZD 100522 Start
                if (conf.iPermanent == 1)
                    reservationXml += "<PERMANENT>true</PERMANENT>";
                //ZD 100522 End
                
				reservationXml += "<LOCK>false</LOCK>";
				reservationXml += "<LSD_RATE>none</LSD_RATE>";

				reservationXml += "<CASCADE>" ;
					if (this.configParams.client == "VAOHIO")
					{
						reservationXml += "<CASCADE_ROLE>negotiated</CASCADE_ROLE>";
					}
					else
					{
						reservationXml += "<CASCADE_ROLE>none</CASCADE_ROLE>";
					}
					reservationXml += "<MASTER_NAME/>";

				reservationXml += "</CASCADE>" ;
				
                #region lecture mode
				reservationXml += "<LECTURE_MODE>" ;
				if (conf.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv7)
				{
                    if (conf.bLectureMode)
                    {
                        reservationXml += "<ON>true</ON>";
                    }
                    else
                    {
                        reservationXml += "<ON>false</ON>";
                    }
				}
					reservationXml += "<TIMER>false</TIMER>";
					reservationXml += "<INTERVAL>15</INTERVAL>";
					reservationXml += "<AUDIO_ACTIVATED>false</AUDIO_ACTIVATED>";
                
                    if (conf.sLecturer == null)
                    {
                        if (conf.cMcu.etType == NS_MESSENGER.MCU.eType.RMX) //PSU RMX Fix
                        {
                            reservationXml += "<LECTURE_NAME/>";
                        }
                        else
                        {
                            if (conf.bLectureMode)
                            {
                                reservationXml += "<LECTURE_NAME>[Auto Select]</LECTURE_NAME>";
                            }
                            else
                            {
                                reservationXml += "<LECTURE_NAME/>";
                            }
                        }
                    }
                    else
                    {
                        reservationXml += "<LECTURE_NAME>" + conf.sLecturer +"</LECTURE_NAME>";
                    }
                    if (conf.bLectureMode)
                    {
                        reservationXml += "<LECTURE_MODE_TYPE>lecture_mode</LECTURE_MODE_TYPE>";
                    }
                    else
                    {
                        reservationXml += "<LECTURE_MODE_TYPE>lecture_none</LECTURE_MODE_TYPE>";
                    }
				    if (conf.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv7)
				    {
					    reservationXml += "<LECTURE_ID>0</LECTURE_ID>";					
				    }
				reservationXml += "</LECTURE_MODE>" ;
#endregion    								

				#region videolayouts
				ret = false;	string videoLayout = "1x1"; // default

                //FB 2335
                if (conf.cMcu.iDefaultLO > 0)
                    if (conf.iVideoLayout < 2)
                        conf.iVideoLayout = conf.cMcu.iDefaultLO;
                //FB 2335

				ret = EquateVideoLayout(conf.iVideoLayout,ref videoLayout);
				if (!ret) logger.Trace ("Invalid video layout.");
				reservationXml += "<LAYOUT>" + videoLayout + "</LAYOUT>";
				#endregion

                //FB 2486 Start
                #region Message Overlay

                if (conf.cMcu.etType == NS_MESSENGER.MCU.eType.RMX)
                {
                    if (conf.bMessageOverlay)
                    {
                        reservationXml += "<MESSAGE_OVERLAY>";
                        reservationXml += "<ON>true</ON>";
                        reservationXml += "<MESSAGE_TEXT> </MESSAGE_TEXT>";
                        reservationXml += "<MESSAGE_FONT_SIZE></MESSAGE_FONT_SIZE>";
                        reservationXml += "<MESSAGE_FONT_SIZE_INT></MESSAGE_FONT_SIZE_INT>";
                        reservationXml += "<MESSAGE_COLOR></MESSAGE_COLOR>";
                        reservationXml += "<NUM_OF_REPETITIONS></NUM_OF_REPETITIONS>";
                        reservationXml += "<MESSAGE_DISPLAY_SPEED></MESSAGE_DISPLAY_SPEED>";
                        reservationXml += "<MESSAGE_DISPLAY_POSITION></MESSAGE_DISPLAY_POSITION>";
                        reservationXml += "<MESSAGE_DISPLAY_POSITION_INT></MESSAGE_DISPLAY_POSITION_INT>";
                        reservationXml += "<MESSAGE_TRANSPARENCE></MESSAGE_TRANSPARENCE>";
                        reservationXml += "</MESSAGE_OVERLAY>";
                    }
                }

                #endregion
                //FB 2486 End

                reservationXml += "<FORCE_LIST>";
				reservationXml += "<FORCE>";
					reservationXml += "<LAYOUT>1x1</LAYOUT> ";
					reservationXml += "<CELL>";
						reservationXml += "<ID>1</ID> ";
						reservationXml += "<FORCE_STATE>auto</FORCE_STATE> ";
				if (conf.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv7)
				{
					//reservationXml += "<FORCE_ID>1</FORCE_ID>";
					//reservationXml += "<FORCE_NAME></FORCE_NAME>";
					reservationXml += "<SOURCE_ID>1</SOURCE_ID>";
				}
					reservationXml += "</CELL>";
/*				reservationXml += "<CELL>";
				reservationXml += "<ID>2</ID> ";
				reservationXml += "<FORCE_STATE>auto</FORCE_STATE> ";
				reservationXml += "</CELL>";
				reservationXml += "<CELL>";
				reservationXml += "<ID>3</ID> ";
				reservationXml += "<FORCE_STATE>auto</FORCE_STATE> ";
				reservationXml += "</CELL>";
				reservationXml += "<CELL>";
				reservationXml += "<ID>4</ID> ";
				reservationXml += "<FORCE_STATE>auto</FORCE_STATE> ";
				reservationXml += "</CELL>";
	*/				reservationXml += "</FORCE>";
				reservationXml += "</FORCE_LIST>";

				//H.239 
				if (conf.bH239)
				{
					if (conf.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv7)
					{
						reservationXml += "<DUAL_VIDEO_MODE>enterprise_people_and_content</DUAL_VIDEO_MODE>";					
						reservationXml += "<HSD_RATE>none</HSD_RATE>";
					}
					else
					{
						// Only for ACCORDv6
						reservationXml += "<DUAL_VIDEO_MODE>none</DUAL_VIDEO_MODE>";
                        reservationXml += "<HSD_RATE>none</HSD_RATE>";
					}
				}
				else
					reservationXml += "<DUAL_VIDEO_MODE>none</DUAL_VIDEO_MODE>";
                
                //reservationXml += "<ENTRY_QUEUE>false</ENTRY_QUEUE>"; //PSU RMX FIX - Commented for PSU RMX Fix
                
                // FB# 739: Entry Queue Access
                if (conf.bEntryQueueAccess)
                {
                    reservationXml += "<ENTRY_QUEUE_ACCESS>true</ENTRY_QUEUE_ACCESS>"; //PSU RMX FIX
                    //reservationXml += "<ENTRY_QUEUE_ROUTING>conference_password_routing</ENTRY_QUEUE_ROUTING>";
                    reservationXml += "<MEET_ME_PER_ENTRY_QUEUE>true</MEET_ME_PER_ENTRY_QUEUE>";
                }
                else
                {                    
                    reservationXml += "<ENTRY_QUEUE_ACCESS>false</ENTRY_QUEUE_ACCESS>";
                    reservationXml += "<MEET_ME_PER_ENTRY_QUEUE>false</MEET_ME_PER_ENTRY_QUEUE>";
                }
				
				reservationXml += "<MUTE_INCOMING_PARTIES>false</MUTE_INCOMING_PARTIES>";
				reservationXml += "<ON_HOLD>false</ON_HOLD> ";
                // conference password
                if (conf.iPwd == 0)
                {
                    reservationXml += "<ENTRY_PASSWORD/>";
                }
                else
                {
                    reservationXml += "<ENTRY_PASSWORD>" + conf.iPwd.ToString() + "</ENTRY_PASSWORD>";
                }
				reservationXml += "<ROLL_CALL>false</ROLL_CALL> ";
				reservationXml += "<INVITE_PARTY>false</INVITE_PARTY> ";
				reservationXml += "<ADVANCED_VIDEO>auto</ADVANCED_VIDEO> ";
				
				//if (conf.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv7)
				//{
				//	reservationXml += "<INTERLACED_MODE>auto</INTERLACED_MODE> ";
				//}
				//else
				//{
					reservationXml += "<INTERLACED_MODE>none</INTERLACED_MODE> ";
				//}
				
				// Video Plus condition check
				if (conf.etVideoCodec == NS_MESSENGER.Conference.eVideoCodec.H264)
					reservationXml += "<VIDEO_PLUS>true</VIDEO_PLUS> ";
				else
					reservationXml += "<VIDEO_PLUS>false</VIDEO_PLUS> ";
				
				reservationXml += "<VISUAL_EFFECTS>";
					reservationXml += "<BACKGROUND_COLOR>";
						reservationXml += "<RED>0</RED> ";
                        reservationXml += "<GREEN>0</GREEN> ";
                        reservationXml += "<BLUE>0</BLUE> ";
					reservationXml += "</BACKGROUND_COLOR>";

                    #region LayoutBorder - specify color
                    if (conf.stLayoutBorderColor.iBlue != 0 || conf.stLayoutBorderColor.iBlue != 0 || conf.stLayoutBorderColor.iBlue != 0)
                    {
                        // Set the layout border color
                        reservationXml += "<LAYOUT_BORDER>true</LAYOUT_BORDER>";
                        reservationXml += "<LAYOUT_BORDER_COLOR>";
                        reservationXml += "<RED>" + conf.stLayoutBorderColor.iRed +"</RED>";
                        reservationXml += "<GREEN>"+ conf.stLayoutBorderColor.iGreen +"</GREEN>";
                        reservationXml += "<BLUE>"+ conf.stLayoutBorderColor.iBlue +"</BLUE>";
                        reservationXml += "</LAYOUT_BORDER_COLOR>";
                    }
                    else
                    {
                        // No color set
                        reservationXml += "<LAYOUT_BORDER>false</LAYOUT_BORDER>";
                        reservationXml += "<LAYOUT_BORDER_COLOR>";
                        reservationXml += "<RED>0</RED>";
                        reservationXml += "<GREEN>0</GREEN>";
                        reservationXml += "<BLUE>0</BLUE>";
                        reservationXml += "</LAYOUT_BORDER_COLOR>";
                    }
                    #endregion

                    #region Speaker Notation - specify color
                    if (conf.stSpeakerNotation.iBlue != 0 || conf.stSpeakerNotation.iBlue != 0 || conf.stSpeakerNotation.iBlue != 0)
                    {
                        // Set the speaker color
                        reservationXml += "<SPEAKER_NOTATION>true</SPEAKER_NOTATION> ";
                        reservationXml += "<SPEAKER_NOTATION_COLOR>";
                        reservationXml += "<RED>" + conf.stSpeakerNotation.iRed + "</RED>";
                        reservationXml += "<GREEN>" + conf.stSpeakerNotation.iGreen + "</GREEN>";
                        reservationXml += "<BLUE>" + conf.stSpeakerNotation.iBlue + "</BLUE>";
                        reservationXml += "</SPEAKER_NOTATION_COLOR>";
                    }
                    else
                    {
                        // No color set
                        reservationXml += "<SPEAKER_NOTATION>false</SPEAKER_NOTATION> ";
                        reservationXml += "<SPEAKER_NOTATION_COLOR>";
                        reservationXml += "<RED>0</RED>";
                        reservationXml += "<GREEN>0</GREEN>";
                        reservationXml += "<BLUE>0</BLUE>";
                        reservationXml += "</SPEAKER_NOTATION_COLOR>";
                    }
                    #endregion

                reservationXml += "</VISUAL_EFFECTS>";

				// new attributes which are added in XAP ver 7 
				// (will not affect prev versions)
				if (conf.bConferenceOnPort)
				{
					reservationXml += "<COP>true</COP>";
					reservationXml += "<COP_NUM_OF_PORTS>single</COP_NUM_OF_PORTS>";
				}
				else
					reservationXml += "<COP>false</COP>";
				 
					reservationXml += "<PROFILE>false</PROFILE>";
					reservationXml += "<DIAL_IN_H323_SRV_PREFIX_LIST/>";
				
				if (conf.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv7)
				{
					reservationXml += "<AD_HOC>false</AD_HOC>";
					reservationXml += "<AD_HOC_PROFILE_ID>-1</AD_HOC_PROFILE_ID>";
				}
				reservationXml += "<AUTO_LAYOUT>false</AUTO_LAYOUT>";
				reservationXml += "<BILLING_DATA/>";

                if (conf.cMcu.etType == NS_MESSENGER.MCU.eType.RMX) //PSU RMX Fix
                {
                    //2839 Start
                    //if (conf.cMcu != null)//FB 2427
                    //if(conf.cMcu.ConfServiceID > 0)
                    //    reservationXml += "<AD_HOC_PROFILE_ID>" + conf.cMcu.ConfServiceID + "</AD_HOC_PROFILE_ID>";

                    if (conf.iConfRMXServiceID > 0)
                        reservationXml += "<AD_HOC_PROFILE_ID>" + conf.iConfRMXServiceID + "</AD_HOC_PROFILE_ID>";

                    //2839 End
                    //ZD 100522 Start 104003
                    if (conf.sDialinNumber != "" && conf.sDialinNumber != "0")
                        reservationXml += "<NUMERIC_ID>" + conf.sDialinNumber.ToString() + "</NUMERIC_ID>";
                    else
                        reservationXml += "<NUMERIC_ID>" + conf.iDbNumName.ToString() + "</NUMERIC_ID>";

                    //ZD 100522 End
                }
                else
                {
                    reservationXml += "<NUMERIC_ID/>";
                }
                
				reservationXml += "<CONTACT_INFO_LIST/>";
					
					// participants can choose their own layouts
					//reservationXml += "<SAME_LAYOUT>false</SAME_LAYOUT>";
				
				if (conf.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv7)
				{
					// encryption
					if (conf.bEncryption)
						reservationXml += "<ENCRYPTION>true</ENCRYPTION>";
					else
						reservationXml += "<ENCRYPTION>false</ENCRYPTION>";

					reservationXml += "<CONF_CONTROL>h243</CONF_CONTROL>";
					reservationXml += "<CONF_PROTOCOL>h243</CONF_PROTOCOL>";
					reservationXml += "<EXTERNAL_MASTER>false</EXTERNAL_MASTER>";
					reservationXml += "<AD_HOC_PROFILE_ID>1</AD_HOC_PROFILE_ID>";
					reservationXml += "<VIDEO>true</VIDEO>";
					reservationXml += "<QCIF_FRAME_RATE>auto</QCIF_FRAME_RATE>";
					reservationXml += "<COUGH_DELAY>0</COUGH_DELAY>";
					reservationXml += "<GUEST_OPER>false</GUEST_OPER>";
					reservationXml += "<WEB_RESERVED_UID>0</WEB_RESERVED_UID>"; 
					reservationXml += "<WEB_OWNER_UID>0</WEB_OWNER_UID>"; 
					reservationXml += "<WEB_DB_ID>0</WEB_DB_ID>";
					reservationXml += "<WEB_RESERVED>false</WEB_RESERVED>";
					reservationXml += "<CONFERENCE_TYPE>standard</CONFERENCE_TYPE>";
					reservationXml += "<CREATOR />";
					reservationXml += "<SILENCE_IT>false</SILENCE_IT>";
                    
                    //FB 1907 - EnableRecording is set to true/false based on MCU profile settings in myvrm Web interface
                    if (conf.cMcu.iEnableRecording == 1)
                    {
                        reservationXml += "<IS_RECORDING_LINK>true</IS_RECORDING_LINK>";
                        reservationXml += "<ENABLE_RECORDING>true</ENABLE_RECORDING>";
                    }
                    else
                    {
                        reservationXml += "<IS_RECORDING_LINK>false</IS_RECORDING_LINK>";
                        reservationXml += "<ENABLE_RECORDING>false</ENABLE_RECORDING>";
                    }
                    //FB 1907 ends
					reservationXml += "<START_REC_POLICY>immediately</START_REC_POLICY>";
					reservationXml += "<REC_LINK_ID>-1</REC_LINK_ID>";
					reservationXml += "<REC_LINK_NAME />";
					reservationXml += "<VIDEO_QUALITY>auto</VIDEO_QUALITY>";
					reservationXml += "<ENTRY_QUEUE_TYPE>normal</ENTRY_QUEUE_TYPE>";                    
					
                    reservationXml += "<END_TIME_ALERT_TONE_EX>";
                    if (conf.bEndTimeNotification)
                    {
                        reservationXml += "<ON>true</ON>";
                    }
                    else
                    {
                        reservationXml += "<ON>false</ON>";                      
                    }
					reservationXml += "<TIME>5</TIME>";
					reservationXml += "</END_TIME_ALERT_TONE_EX>";

					reservationXml += "<DB_EQ_ACCESS_NAME />";
					reservationXml += "<VTX>false</VTX>";
					reservationXml += "<CASCADE_EQ>false</CASCADE_EQ>";
					reservationXml += "<SIP_FACTORY>false</SIP_FACTORY>";
					reservationXml += "<SIP_FACTORY_AUTO_CONNECT>false</SIP_FACTORY_AUTO_CONNECT>";
				}

                if (conf.cMcu.etType == NS_MESSENGER.MCU.eType.RMX) //FB 1742
                {
                    reservationXml += "<DISPLAY_NAME>" + conf.sDbName + "</DISPLAY_NAME>";
                    //FB 1907 - EnableRecording is set to true/false based on MCU profile settings in myvrm Web interface
                    if (conf.cMcu.iEnableRecording == 1)
                    {
                        reservationXml += "<IS_RECORDING_LINK>true</IS_RECORDING_LINK>";
                        reservationXml += "<ENABLE_RECORDING>true</ENABLE_RECORDING>";
                    }
                    else
                    {
                        reservationXml += "<IS_RECORDING_LINK>false</IS_RECORDING_LINK>"; //FB 1910 Recording is set to false for Call Launching - PSU request. set to false in MCU profile
                        reservationXml += "<ENABLE_RECORDING>false</ENABLE_RECORDING>"; //FB 1910 Recording is set to false for Call Launching
                    }

                    if (conf.cMcu.iLPR == 1)
                    {
                        reservationXml += "<LPR>true</LPR>";
                    }
                    else
                    {
                        reservationXml += "<LPR>false</LPR>"; 
                        
                    }

                    //FB 1907 ends
                    reservationXml += "<START_REC_POLICY>upon_request</START_REC_POLICY>"; 
                    reservationXml += "<VIDEO>true</VIDEO>";
                    reservationXml += "<VIDEO_QUALITY>motion</VIDEO_QUALITY>";
                }
				
				// Generate the participants list xml 
				string partyList = null; ret = false;
                if (conf.iPermanent == 0) //ZD 100522
                {
                    ret = CreateXML_PartyList(ref conf, ref partyList);
                    if (!ret)
                    {
                        string debugString = "Conf =" + conf.iDbID.ToString() + "," + conf.iInstanceID.ToString();
                        debugString += ". PartyListXML = " + partyList;
                        logger.Trace("CreateXML_PartyList failed." + debugString);
                        return (false);
                    }
                }
				//add it to reservation xml
				if (partyList != null) 
				{
					reservationXml += "<PARTY_LIST>" + partyList + "</PARTY_LIST>";
				}
				else
				{
					reservationXml += "<PARTY_LIST/>";
				}
				reservationXml += "</RESERVATION>";
				#endregion
			}
			catch(Exception e)
			{ 
				logger.Exception(100,e.Message);
				return false;
			}
			return true;
		}//end of Reservation XML

        private bool ProcessXML_Reservation(string responseXML, NS_MESSENGER.Conference conf, ref string outXml)//FB 2683 //ZD 100522
		{
            string ConfUID =""; bool ret = false; //FB 2683
			try
			{
				// Check if conf has been setup correctly on the bridge
				XmlDocument xd = new XmlDocument();
				xd.LoadXml(responseXML);

				XmlNode node = xd.SelectSingleNode("//RESPONSE_TRANS_RES/RETURN_STATUS/DESCRIPTION");
                XmlNodeList NodeList = null; //FB 2683

                XmlNode node1 = xd.SelectSingleNode("//RESPONSE_TRANS_CONF/RETURN_STATUS/DESCRIPTION"); //104003

				string description=null,updatedescription = null;//104003
				//FB 2683 Starts
                int PartyGUID = 0;
                string Ipisdnaddress = "";
				if (node != null)
				{	
					description = node.InnerText.Trim().ToUpper();

                    if ((description.Contains("OK") == true) || (description.Contains("WARNING") == true))
					{
					    if (xd.SelectSingleNode("//RESPONSE_TRANS_RES/START/RESERVATION/ID") != null)
                        {
                            ConfUID = xd.SelectSingleNode("//RESPONSE_TRANS_RES/START/RESERVATION/ID").InnerText.Trim();
                            ret = false;
                            ret = db.UpdateConfUIDonMCU(ConfUID, conf);
                            if (!ret)
                            {
                                logger.Trace("Error in Update the ConfGUIDonMCU");
                                return false;
                            }
                        }
                        if (xd.SelectSingleNode("//RESPONSE_TRANS_RES/START/RESERVATION/PARTY_LIST") != null)
                        {
                            if (xd.SelectSingleNode("//RESPONSE_TRANS_RES/START/RESERVATION/PARTY_LIST/PARTY") != null)
                            {
                                NodeList = xd.SelectNodes("//RESPONSE_TRANS_RES/START/RESERVATION/PARTY_LIST/PARTY");
                                for (int i = 0; i < NodeList.Count; i++)
                                {
                                    int.TryParse(NodeList[i].SelectSingleNode("ID").InnerText, out PartyGUID);
                                    Ipisdnaddress = NodeList[i].SelectSingleNode("IP").InnerText;

                                    ret = db.UpdateParticipantGUID(conf.iDbID, conf.iInstanceID, Ipisdnaddress, PartyGUID.ToString(), 0);
                                    if (!ret)
                                      logger.Trace("Error in Update Participant GUID for conference id:"+conf.iDbID+" instanceid"+conf.iInstanceID);
                                      
                                }
                            }
                        }
                        // FB 2683 Ends
					}
					else
					{
						this.errMsg = description; // error message from the MCU
					}
				}

                //ZD 104003 start
                if (node1 != null)
                {
                    updatedescription = node1.InnerText.Trim().ToUpper();

                    if ((updatedescription.Contains("OK") == true) || (updatedescription.Contains("WARNING") == true))
                    {
                        if (!string.IsNullOrEmpty(conf.sGUID))
                        {
                            ret = db.UpdateConfUIDonMCU(conf.sGUID, conf);
                            if (!ret)
                            {
                                logger.Trace("Error in Update the ConfGUIDonMCU");
                                return false;
                            }

                            ConfUID = conf.sGUID;
                        }

                    }
                }
                //ZD 104003 End

                //ZD 100522 Start
                if (ConfUID != null)
                {
                    ret = false;

                    if (conf.dtStartDateTimeInUTC > DateTime.UtcNow)
                        conf.etStatus = NS_MESSENGER.Conference.eStatus.OnMCU;
                    else
                        conf.etStatus = NS_MESSENGER.Conference.eStatus.ONGOING;

                    ret = db.UpdateConference(conf.iDbID, conf.iInstanceID, ConfUID, conf.etStatus, string.Empty);//ZD 101217
                    if (!ret)
                    {
                        logger.Trace("Error in Update the ConfGUIDonMCU");
                        return false;
                    }

                    outXml = "<Conference><ConfUID>" + ConfUID + "</ConfUID><PermanentconfName>" + conf.sDbName + "</PermanentconfName></Conference>";//ZD 100522_S1
                }
                //ZD 100522 End
                if (description == null) //FB 2427
                {
                    XElement elem = XElement.Parse(responseXML);
                    description = elem.Element("RETURN_STATUS").Element("DESCRIPTION").Value;
                    if ((description.Contains("OK") == true) || (description.Contains("WARNING") == true))
                        return true;	//conf setup successful
                    else
                        this.errMsg = description; // error message from the MCU
                }
                else
                    return true;
				logger.Exception(100,"Conf setup on Accord failed. ResponseXML = " + description);
				return false;//conf setup was unsuccessfull	
			}
			catch (Exception e)
			{
				logger.Exception(100,e.Message);
				return false;
			}
		}

		private bool CreateXML_PartyList(ref NS_MESSENGER.Conference conf , ref string partyList)
		{
			try 
			{
				//Create the participant xml list 
				string party = null;
				logger.Trace ("Create Party List count =" + conf.qParties.Count.ToString());
		
				//cycle through each participant.
				while (conf.qParties.Count > 0)
				{
					//individual party info
					NS_MESSENGER.Party partyObj = new NS_MESSENGER.Party();
					partyObj = (NS_MESSENGER.Party)conf.qParties.Dequeue();
                    
                    // Requested by Dan for Yorktel - Disney (Jan, 29 2010)
                    // check line rate of party with conference. if conference is lesser than party, then use conference line rate. Else, don't change anything.
                    if (conf.stLineRate.etLineRate < partyObj.stLineRate.etLineRate)
                    {
                        partyObj.stLineRate.etLineRate = conf.stLineRate.etLineRate;
                    }

                    bool ret = CreateXML_Party(ref party, partyObj, ref conf);// ZD 100768
					if (!ret) party = null;
			
					// append party info to partylist
					partyList += party;
				}		
				return true;
			}
			catch (Exception e)
			{
				logger.Exception(100,e.Message);
				return false;
			}
		}

		private bool isValidIPAddress(string ip)
		{
			bool ret = true;
			string[] octets = ip.Split('.');
			if ( octets.Length != 4 )
				ret = false;
			else
			{
				for ( int i=0; i<4; i++ )
				{
					int dig = Convert.ToInt32(octets[i]);
					if ( dig < 0 || dig > 255 )
					{
						ret = false;
						break;
					}
				}
			}
			return ret;
		}


		private bool CreateXML_Party(ref string partyXml,NS_MESSENGER.Party partyObj)
		{
			logger.Trace("Party = " + partyObj.sName);
            if (partyObj.sName == null)
            {
                this.errMsg = "Invalid participant name.";
                return false; 
            }
            if (partyObj.sName.Length < 3)
            {
                this.errMsg = "Invalid participant name.";
                return false; 
            }

			partyXml = null;
            //ZD 103263
            string meetingid = "", passcode = "", partyBJNAddress = "";
            int Bindex = 0;

            try
            {
                //ZD 103263 Start

                if (!String.IsNullOrEmpty(partyObj.sAddress))
                {

                    if (partyObj.sAddress.Trim().Contains("B"))
                    {
                        Bindex = partyObj.sAddress.Trim().IndexOf("B");
                        partyBJNAddress = partyObj.sAddress.Trim().Substring(0, Bindex);

                        if (partyObj.sAddress.Split('+').Length > 0) //ZD 103964
                        {
                            meetingid = partyObj.sAddress.Split('B')[1].Split('+')[0]; //Meeting ID
                            passcode = partyObj.sAddress.Split('B')[1].Split('+')[1]; //Attendee passcode
                        }
                        partyObj.sDTMFSequence = meetingid + "##" + passcode;
                        logger.Trace(meetingid + " and " + passcode); //ZD 103964
                        partyObj.sAddress = partyBJNAddress;

                    }
                }
                //ZD 103263 End
                if (partyObj.etProtocol == NS_MESSENGER.Party.eProtocol.IP)
                {
                    // ip participant 
                    partyXml = "<PARTY>";
                    partyXml += "<NAME>" + partyObj.sName + "</NAME>";
                    partyXml += "<ID>" + partyObj.iDbId.ToString() + "</ID>";
                    partyXml += "<PHONE_LIST></PHONE_LIST>";
                    partyXml += "<INTERFACE>h323</INTERFACE>";
                    if (partyObj.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_IN)
                    {
                        partyXml += "<CONNECTION>dial_in</CONNECTION>";
                    }
                    else
                    {
                        if (partyObj.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_OUT)
                        {
                            partyXml += "<CONNECTION>dial_out</CONNECTION>";
                        }
                        else
                        {
                            partyXml += "<CONNECTION>direct</CONNECTION>";
                        }
                    }
                    //Meet me method
                    partyXml += "<MEET_ME_METHOD>party</MEET_ME_METHOD>";
                    partyXml += "<NUM_TYPE>taken_from_service</NUM_TYPE>";
                    partyXml += "<BONDING>auto</BONDING>";
                    partyXml += "<MULTI_RATE>auto</MULTI_RATE>";
                    partyXml += "<NET_CHANNEL_NUMBER>auto</NET_CHANNEL_NUMBER>";

                    //if (partyObj.etVideoProtocol == NS_MESSENGER.Party.eVideoProtocol.AUTO)
                    if (configParams.client == "VAOHIO")
                    {
                        partyXml += "<VIDEO_PROTOCOL>h263</VIDEO_PROTOCOL>";
                    }
                    else
                    {
                        partyXml += "<VIDEO_PROTOCOL>auto</VIDEO_PROTOCOL>";
                    }
                    //else
                    //{
                    //	if (partyObj.etVideoProtocol == NS_MESSENGER.Party.eVideoProtocol.H261)
                    //		partyXml += "<VIDEO_PROTOCOL>h261</VIDEO_PROTOCOL>"; 
                    //	else 
                    //		partyXml += "<VIDEO_PROTOCOL>h263</VIDEO_PROTOCOL>"; 
                    //}

                    //video conf = framed , audio conf = voice

                    if (partyObj.etCallType == NS_MESSENGER.Party.eCallType.AUDIO)
                    {
                        partyXml += "<CALL_CONTENT>voice</CALL_CONTENT>";
                    }
                    else
                    {
                        partyXml += "<CALL_CONTENT>framed</CALL_CONTENT>";
                    }

                    // Check if the address is an alias or an IP //ZD 100132 GateKeeperSupport START

                    if (partyObj.etAddressType == NS_MESSENGER.Party.eAddressType.H323_ID || partyObj.etAddressType == NS_MESSENGER.Party.eAddressType.IP_ADDRESS)
                    {
                        // IP address or H323.ID 
                        partyXml += "<ALIAS>"; //ZD 103964 Starts
                        logger.Trace(meetingid + " and " + passcode);
                        if (meetingid != "")
                        {
                            partyXml += "<NAME>" + meetingid + "." + passcode + "@"+  partyObj.sAddress.Trim()+ "</NAME>";
                            partyXml += "<ALIAS_TYPE>323_id</ALIAS_TYPE></ALIAS>";
                            partyXml += "<IP>0.0.0.0</IP>";
                        }
                        else
                        {
                            partyXml += "<NAME />";
                            partyXml += "<ALIAS_TYPE>323_id</ALIAS_TYPE></ALIAS>";
                            partyXml += "<IP>" + partyObj.sAddress.Trim() + "</IP>";
                        } //ZD 103964 Ends


                       
                    }
                    else
                    {
                        if (partyObj.etAddressType == NS_MESSENGER.Party.eAddressType.E_164)
                        {
                            // It is an E164 alias.
                            partyXml += "<ALIAS><NAME>" + partyObj.sAddress + "</NAME>";
                            partyXml += "<ALIAS_TYPE>e164</ALIAS_TYPE></ALIAS>";
                            if (partyObj.bIsOutsideNetwork == true)
                            {
                                partyXml += "<IP>" + partyObj.sGateKeeeperAddress.Trim() + "</IP>";
                            }
                            else
                            {
                                partyXml += "<IP>0.0.0.0</IP>";
                            }
                        }
                        //ZD 100132 GateKeeperSupport END
                        else
                        {
                            logger.Exception(100, "Invalid participant address type");
                        }
                    }
                    partyXml += "<SIGNALING_PORT>1720</SIGNALING_PORT>";
                    partyXml += "<VOLUME>5</VOLUME>";
                    partyXml += "<MCU_PHONE_LIST/>";
                    partyXml += "<BONDING_PHONE/>";

                    // mcu service name
                    partyXml += "<SERVICE_NAME>" + partyObj.sMcuServiceName + "</SERVICE_NAME>";

                    partyXml += "<SUB_SERVICE_NAME></SUB_SERVICE_NAME>";
                    partyXml += "<AUTO_DETECT>false</AUTO_DETECT>";
                    partyXml += "<RESTRICT>false</RESTRICT>";
                    partyXml += "<ENHANCED_VIDEO>false</ENHANCED_VIDEO>";

                    //FB 1742 start.
                    string videoBitRate = "automatic";
                    if (partyObj.cMcu.etType == NS_MESSENGER.MCU.eType.RMX)
                    {
                        bool ret = EquateLineRate(partyObj.stLineRate.etLineRate, true, ref videoBitRate);
                        if (!ret)
                        {
                            logger.Trace("Invalid Video Bit Rate.");
                        }
                    }
                    //FB 1742 end

                    if (configParams.client == "VAOHIO")
                    {
                        partyXml += "<VIDEO_BIT_RATE>384</VIDEO_BIT_RATE>";
                    }
                    else
                    {
                        partyXml += "<VIDEO_BIT_RATE>" + videoBitRate + "</VIDEO_BIT_RATE>";
                    }
                    partyXml += "<AGC>true</AGC>";

                    if (partyObj.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv7)
                    {
                        partyXml += "<IP_QOS>";
                        partyXml += "<QOS_ACTION>from_service</QOS_ACTION>";
                        partyXml += "<QOS_DIFF_SERV>precedence</QOS_DIFF_SERV>";
                        partyXml += "<QOS_IP_AUDIO>4</QOS_IP_AUDIO>";
                        partyXml += "<QOS_IP_VIDEO>4</QOS_IP_VIDEO>";
                        partyXml += "<QOS_TOS>delay</QOS_TOS>";
                        partyXml += "</IP_QOS>";
                        partyXml += "<RECORDING_PORT>no</RECORDING_PORT>";
                        partyXml += "<LAYOUT_TYPE>conference</LAYOUT_TYPE>";
                        partyXml += "<PERSONAL_LAYOUT>1x1</PERSONAL_LAYOUT>";
                        partyXml += "<PERSONAL_FORCE_LIST />";
                        partyXml += "<VIP>false</VIP> ";
                        partyXml += "<CONTACT_INFO_LIST />";
                        partyXml += "<LISTEN_VOLUME>5</LISTEN_VOLUME>";
                        partyXml += "<SIP_ADDRESS />";
                        partyXml += "<SIP_ADDRESS_TYPE>uri_type</SIP_ADDRESS_TYPE>";
                        partyXml += "<WEB_USER_ID>0</WEB_USER_ID>";
                        partyXml += "<UNDEFINED>false</UNDEFINED>";
                        partyXml += "<BACKUP_SERVICE_NAME />";
                        partyXml += "<BACKUP_SUB_SERVICE_NAME />";
                        partyXml += "<DEFAULT_TEMPLATE>false</DEFAULT_TEMPLATE>";
                        partyXml += "<NODE_TYPE>terminal</NODE_TYPE>";
                        partyXml += "<ENCRYPTION_EX>auto</ENCRYPTION_EX>";
                        partyXml += "<H323_PSTN>false</H323_PSTN>";
                        partyXml += "<EMAIL />";
                        partyXml += "<IS_RECORDING_LINK_PARTY>false</IS_RECORDING_LINK_PARTY>";
                        partyXml += "<USER_IDENTIFIER_STRING />";
                        partyXml += "<IDENTIFICATION_METHOD>called_phone_number</IDENTIFICATION_METHOD>";
                    }

                    //ZD 103263
                    if (partyObj.sDTMFSequence != null)
                    {
                        if (partyObj.sDTMFSequence.Trim() != "")
                        {


                            if (partyObj.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv7)
                            {
                                if (partyObj.sDTMFSequence.Trim() != "")
                                {
                                    partyXml += "<USER_IDENTIFIER_STRING>" + partyObj.sDTMFSequence.Trim() + "</USER_IDENTIFIER_STRING>";
                                }
                                else
                                {
                                    partyXml += "<USER_IDENTIFIER_STRING/>";
                                }

                            }
                        }
                    }

                    partyXml += "</PARTY>";
                }

                //ZD 103825 START [myVRM to Support SIP Dialing in RMX]
                else if (partyObj.etProtocol == NS_MESSENGER.Party.eProtocol.SIP)
                {
                    // SIP participant 
                    partyXml = "<PARTY>";
                    partyXml += "<NAME>" + partyObj.sName + "</NAME>";
                    partyXml += "<ID>" + partyObj.iDbId.ToString() + "</ID>";
                    partyXml += "<PHONE_LIST></PHONE_LIST>";
                    partyXml += "<INTERFACE>sip</INTERFACE>";
                    if (partyObj.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_IN)
                    {
                        partyXml += "<CONNECTION>dial_in</CONNECTION>";
                    }
                    else
                    {
                        if (partyObj.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_OUT)
                        {
                            partyXml += "<CONNECTION>dial_out</CONNECTION>";
                        }
                        else
                        {
                            partyXml += "<CONNECTION>direct</CONNECTION>";
                        }
                    }
                    //Meet me method
                    partyXml += "<MEET_ME_METHOD>party</MEET_ME_METHOD>";
                    partyXml += "<NUM_TYPE>taken_from_service</NUM_TYPE>";
                    partyXml += "<BONDING>auto</BONDING>";
                    partyXml += "<MULTI_RATE>auto</MULTI_RATE>";
                    partyXml += "<NET_CHANNEL_NUMBER>auto</NET_CHANNEL_NUMBER>";

                    //if (partyObj.etVideoProtocol == NS_MESSENGER.Party.eVideoProtocol.AUTO)
                    if (configParams.client == "VAOHIO")
                    {
                        partyXml += "<VIDEO_PROTOCOL>h263</VIDEO_PROTOCOL>";
                    }
                    else
                    {
                        partyXml += "<VIDEO_PROTOCOL>auto</VIDEO_PROTOCOL>";
                    }

                    if (partyObj.etCallType == NS_MESSENGER.Party.eCallType.AUDIO)
                    {
                        partyXml += "<CALL_CONTENT>voice</CALL_CONTENT>";
                    }
                    else
                    {
                        partyXml += "<CALL_CONTENT>framed</CALL_CONTENT>";
                    }

                    // Check if the address is an alias or an IP //ZD 100132 GateKeeperSupport START

                    //if (partyObj.etAddressType == NS_MESSENGER.Party.eAddressType.SIP)
                    //{
                    //    // IP address or H323.ID 
                    //    partyXml += "<ALIAS><NAME /><ALIAS_TYPE>party_number</ALIAS_TYPE></ALIAS>";
                    //    partyXml += "<SIP_ADDRESS>" + partyObj.sAddress.Trim() + "</SIP_ADDRESS>";
                    //}
                    //else
                    //{
                    //    if (partyObj.etAddressType == NS_MESSENGER.Party.eAddressType.E_164)
                    //    {
                    //        // It is an E164 alias.
                    //        partyXml += "<ALIAS><NAME>" + partyObj.sAddress + "</NAME>";
                    //        partyXml += "<ALIAS_TYPE>e164</ALIAS_TYPE></ALIAS>";
                    //        if (partyObj.bIsOutsideNetwork == true)
                    //        {
                    //            partyXml += "<IP>" + partyObj.sGateKeeeperAddress.Trim() + "</IP>";
                    //        }
                    //        else
                    //        {
                    //            partyXml += "<IP>0.0.0.0</IP>";
                    //        }
                    //    }
                    //    //ZD 100132 GateKeeperSupport END
                    //    else
                    //    {
                    //        logger.Exception(100, "Invalid participant address type");
                    //    }
                    //}


                    partyXml += "<SIP_ADDRESS>" + partyObj.sAddress.Trim() + "</SIP_ADDRESS>";
                    partyXml += "<SIP_ADDRESS_TYPE>uri_type</SIP_ADDRESS_TYPE>";
                    partyXml += "<SIGNALING_PORT>1720</SIGNALING_PORT>";
                    partyXml += "<VOLUME>5</VOLUME>";
                    partyXml += "<MCU_PHONE_LIST/>";
                    partyXml += "<BONDING_PHONE/>";

                    // mcu service name
                    partyXml += "<SERVICE_NAME>" + partyObj.sMcuServiceName + "</SERVICE_NAME>";

                    partyXml += "<SUB_SERVICE_NAME></SUB_SERVICE_NAME>";
                    partyXml += "<AUTO_DETECT>false</AUTO_DETECT>";
                    partyXml += "<RESTRICT>false</RESTRICT>";
                    partyXml += "<ENHANCED_VIDEO>false</ENHANCED_VIDEO>";

                    //FB 1742 start.
                    string videoBitRate = "automatic";
                    if (partyObj.cMcu.etType == NS_MESSENGER.MCU.eType.RMX)
                    {
                        bool ret = EquateLineRate(partyObj.stLineRate.etLineRate, true, ref videoBitRate);
                        if (!ret)
                        {
                            logger.Trace("Invalid Video Bit Rate.");
                        }
                    }
                    //FB 1742 end

                    if (configParams.client == "VAOHIO")
                    {
                        partyXml += "<VIDEO_BIT_RATE>384</VIDEO_BIT_RATE>";
                    }
                    else
                    {
                        partyXml += "<VIDEO_BIT_RATE>" + videoBitRate + "</VIDEO_BIT_RATE>";
                    }
                    partyXml += "<AGC>true</AGC>";

                    if (partyObj.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv7)
                    {
                        partyXml += "<IP_QOS>";
                        partyXml += "<QOS_ACTION>from_service</QOS_ACTION>";
                        partyXml += "<QOS_DIFF_SERV>precedence</QOS_DIFF_SERV>";
                        partyXml += "<QOS_IP_AUDIO>4</QOS_IP_AUDIO>";
                        partyXml += "<QOS_IP_VIDEO>4</QOS_IP_VIDEO>";
                        partyXml += "<QOS_TOS>delay</QOS_TOS>";
                        partyXml += "</IP_QOS>";
                        partyXml += "<RECORDING_PORT>no</RECORDING_PORT>";
                        partyXml += "<LAYOUT_TYPE>conference</LAYOUT_TYPE>";
                        partyXml += "<PERSONAL_LAYOUT>1x1</PERSONAL_LAYOUT>";
                        partyXml += "<PERSONAL_FORCE_LIST />";
                        partyXml += "<VIP>false</VIP> ";
                        partyXml += "<CONTACT_INFO_LIST />";
                        partyXml += "<LISTEN_VOLUME>5</LISTEN_VOLUME>";
                        partyXml += "<SIP_ADDRESS />";
                        partyXml += "<SIP_ADDRESS_TYPE>uri_type</SIP_ADDRESS_TYPE>";
                        partyXml += "<WEB_USER_ID>0</WEB_USER_ID>";
                        partyXml += "<UNDEFINED>false</UNDEFINED>";
                        partyXml += "<BACKUP_SERVICE_NAME />";
                        partyXml += "<BACKUP_SUB_SERVICE_NAME />";
                        partyXml += "<DEFAULT_TEMPLATE>false</DEFAULT_TEMPLATE>";
                        partyXml += "<NODE_TYPE>terminal</NODE_TYPE>";
                        partyXml += "<ENCRYPTION_EX>auto</ENCRYPTION_EX>";
                        partyXml += "<H323_PSTN>false</H323_PSTN>";
                        partyXml += "<EMAIL />";
                        partyXml += "<IS_RECORDING_LINK_PARTY>false</IS_RECORDING_LINK_PARTY>";
                        partyXml += "<USER_IDENTIFIER_STRING />";
                        partyXml += "<IDENTIFICATION_METHOD>called_phone_number</IDENTIFICATION_METHOD>";
                    }

                    partyXml += "</PARTY>";
                }
                //ZD 103825 END

                else if (partyObj.etProtocol == NS_MESSENGER.Party.eProtocol.ISDN)
                {
                    // ISDN participant
                    string partyAddress = "";  //FB 1740 - Audio Add-on implementation for Polycom sites
                    string phonePrefix = "";
                    partyAddress = partyObj.sAddress.Trim();

                    partyXml = "<PARTY>";
                    partyXml += "<NAME>" + partyObj.sName + "</NAME>";
                    partyXml += "<ID>" + partyObj.iDbId.ToString() + "</ID>";

                    // participant's isdn address : to be added only in dial-out scenario
                    if (partyObj.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_OUT)
                    {
                        //FB 1740 - Audio Add-on implementation for Polycom sites - code start

                        //partyXml += "<PHONE_LIST><PHONE1>" + partyObj.sAddress.Trim() + "</PHONE1></PHONE_LIST>";
                        if (partyAddress.Trim().Contains("G"))
                        {
                            int Gindex = partyAddress.IndexOf("G");
                            partyAddress = partyAddress.Substring(0, Gindex);
                        }
                        if (partyAddress.Trim().Contains("D") && partyObj.bAudioBridgeParty)//ZD 101655
                        {
                            int Dindex = partyAddress.IndexOf("D");
                            partyAddress = partyAddress.Substring(0, Dindex);

                            //FB 2655 DTMF Start
                            //Audio Dial-in prefix is added before the address
                            //phonePrefix = configParams.audioDialNoPrefix;
                            phonePrefix = partyObj.AudioDialInPrefix;
                            //FB 2655 DTMF End
                        }

                        //if (partyObj.etVideoEquipment == NS_MESSENGER.Party.eVideoEquipment.RECORDER)
                        //{ 
                        //    //send gatewayAddress
                        //}
                        // check for gateway  
                        if (partyObj.sAddress.Trim().Contains("G"))
                        {
                            // gateway exists
                            if (partyObj.sAddress.Trim().Contains("D") && partyObj.bAudioBridgeParty)//ZD 101655
                            {
                                // dtmf sequence also exists. Hence, gateway value needs to be extracted between G & D.
                                int Gindex = partyObj.sAddress.IndexOf("G");
                                int Dindex = partyObj.sAddress.IndexOf("D");
                                partyObj.sGatewayAddress = partyObj.sAddress.Substring(Gindex + 1, Dindex - Gindex - 1);
                            }
                            else
                            {
                                int Gindex = partyObj.sAddress.IndexOf("G");
                                partyObj.sGatewayAddress = partyObj.sAddress.Substring(Gindex + 1);
                            }
                        }
                        // check for dtmf sequence
                        if (partyObj.sAddress.Trim().Contains("D") && partyObj.bAudioBridgeParty)//FB 2655
                        {
                            // dtmf sequence exists                            
                            int Dindex = partyObj.sAddress.IndexOf("D");
                            partyObj.sDTMFSequence = partyObj.sAddress.Substring(Dindex + 1);

                            // FB case#1632
                            // GENERIC STRING: ,,,,,,,,CONFCODE,#,,,,,*,,,,,LEADERPIN,# ,,,,,1 (MGC uses "p" for pauses)

                            // add the pre-ConfCode DTMF codes
                            partyObj.sDTMFSequence = partyObj.preConfCode + partyObj.sDTMFSequence; //FB 2655
                            //partyObj.sDTMFSequence = partyObj.cMcu.preConfCode + ":" + partyObj.sDTMFSequence;

                            // add the pre-LeaderPIN DTMF codes
                            partyObj.sDTMFSequence = partyObj.sDTMFSequence.Replace("+", partyObj.preLPin); //FB 2655
                            //partyObj.sDTMFSequence = partyObj.sDTMFSequence.Replace("+", (":"+ partyObj.cMcu.preLPin));

                            // add the post-LeaderPIN DTMF codes
                            partyObj.sDTMFSequence = partyObj.sDTMFSequence.Trim() + partyObj.postLPin; //FB 2655
                            //partyObj.sDTMFSequence = partyObj.sDTMFSequence.Trim() +":"+ partyObj.cMcu.postLPin;
                        }

                        if (partyObj.sDTMFSequence != null)
                        {

                            if (partyObj.sDTMFSequence.Trim() != "")
                            {
                                partyXml += "<PHONE_LIST><PHONE1>" + phonePrefix.Trim() + partyAddress.Trim() + "</PHONE1></PHONE_LIST>";
                            }
                            else
                            {
                                partyXml += "<PHONE_LIST><PHONE1>" + partyAddress.Trim() + "</PHONE1></PHONE_LIST>";
                            }


                        }
                        else
                        {
                            partyXml += "<PHONE_LIST><PHONE1>" + partyAddress.Trim() + "</PHONE1></PHONE_LIST>";
                        }
                        //FB 1740 - Audio Add-on implementation for Polycom sites - code end
                    }
                    else
                    {
                        partyXml += "<PHONE_LIST></PHONE_LIST>";
                    }

                    // isdn 
                    partyXml += "<INTERFACE>isdn</INTERFACE>";

                    // connection type
                    if (partyObj.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_IN)
                    {
                        partyXml += "<CONNECTION>dial_in</CONNECTION>";
                    }
                    else
                    {
                        if (partyObj.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_OUT)
                        {
                            partyXml += "<CONNECTION>dial_out</CONNECTION>";
                        }
                        else
                        {
                            partyXml += "<CONNECTION>direct</CONNECTION>";
                        }
                    }
                    partyXml += "<MEET_ME_METHOD>party</MEET_ME_METHOD>";
                    logger.Trace("After meet me method...");
                    partyXml += "<NUM_TYPE>taken_from_service</NUM_TYPE>";
                    partyXml += "<BONDING>auto</BONDING>";
                    partyXml += "<MULTI_RATE>disabled</MULTI_RATE>";

                    // ISDN channels.
                    string polycomLineRate = "channel_6";
                    bool ret = EquateLineRate(partyObj.stLineRate.etLineRate, false, ref polycomLineRate);
                    if (!ret)
                    {
                        logger.Trace("Line rate conversion failed.");
                    }
                    partyXml += "<NET_CHANNEL_NUMBER>" + polycomLineRate + "</NET_CHANNEL_NUMBER>";

                    //if (partyObj.m_videoProtocol == NS_MESSENGER.Party.eVideoProtocol.AUTO)
                    //	partyXml += "<VIDEO_PROTOCOL>auto</VIDEO_PROTOCOL>"; 
                    //else
                    //{
                    //	if (partyObj.m_videoProtocol == NS_MESSENGER.Party.eVideoProtocol.H261)
                    partyXml += "<VIDEO_PROTOCOL>h261</VIDEO_PROTOCOL>";
                    //	else 
                    //		partyXml += "<VIDEO_PROTOCOL>h263</VIDEO_PROTOCOL>"; 
                    //}

                    //video conf = framed , audio conf = voice
                    if (partyObj.etCallType == NS_MESSENGER.Party.eCallType.AUDIO)
                    {
                        partyXml += "<CALL_CONTENT>voice</CALL_CONTENT>";
                    }
                    else
                    {
                        partyXml += "<CALL_CONTENT>framed</CALL_CONTENT>";
                    }

                    partyXml += "<ALIAS><NAME /><ALIAS_TYPE>323_id</ALIAS_TYPE></ALIAS>";
                    partyXml += "<IP>255.255.255.255</IP>";
                    partyXml += "<SIGNALING_PORT>1720</SIGNALING_PORT>";
                    partyXml += "<VOLUME>5</VOLUME>";

                    // mcu's isdn address : to be added only in dial-in scenario
                    if (partyObj.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_IN)
                    {
                        partyXml += "<MCU_PHONE_LIST><PHONE1>" + partyObj.sMcuAddress.Trim() + "</PHONE1></MCU_PHONE_LIST>";
                    }
                    else
                    {
                        partyXml += "<MCU_PHONE_LIST><PHONE1></PHONE1></MCU_PHONE_LIST>";
                    }

                    if (partyObj.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_OUT)
                    {
                        if (partyObj.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv7)
                        {
                            partyXml += "<BONDING_PHONE/>"; //FB 1740 Audio Add-on implementation for Polycom
                        }
                        else
                        {
                            //partyXml += "<BONDING_PHONE>" + partyObj.sAddress.Trim() + "</BONDING_PHONE>"; //Commented during FB 1740
                            partyXml += "<BONDING_PHONE>" + partyAddress.Trim() + "</BONDING_PHONE>"; //FB 1740 - Audio Add-on implementation for Polycom
                        }
                    }
                    else
                    {
                        partyXml += "<BONDING_PHONE></BONDING_PHONE>";
                    }

                    // mcu service name
                    partyXml += "<SERVICE_NAME>" + partyObj.sMcuServiceName.Trim() + "</SERVICE_NAME>";

                    partyXml += "<SUB_SERVICE_NAME>ZERO</SUB_SERVICE_NAME>";
                    partyXml += "<AUTO_DETECT>false</AUTO_DETECT>";
                    partyXml += "<RESTRICT>false</RESTRICT>";
                    partyXml += "<ENHANCED_VIDEO>false</ENHANCED_VIDEO>";
                    partyXml += "<VIDEO_BIT_RATE>automatic</VIDEO_BIT_RATE>";
                    partyXml += "<AGC>true</AGC>";
                    if (partyObj.sDTMFSequence != null)
                    {
                        if (partyObj.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv7) //FB 1740
                        {
                            if (partyObj.sDTMFSequence.Trim() != "")
                            {
                                partyXml += "<USER_IDENTIFIER_STRING>" + partyObj.sDTMFSequence.Trim() + "</USER_IDENTIFIER_STRING>";
                            }
                            else
                            {
                                partyXml += "<USER_IDENTIFIER_STRING/>";
                            }
                            //partyXml += "<IDENTIFICATION_METHOD>called_phone_number</IDENTIFICATION_METHOD>";
                        }
                    }
                    else
                    {
                        partyXml += "<USER_IDENTIFIER_STRING/>";
                    }
                    partyXml += "</PARTY>";
                }
                else if (partyObj.etProtocol == NS_MESSENGER.Party.eProtocol.MPI)
                {
                    // MPI participant
                    partyXml = "<PARTY>";
                    partyXml += "<NAME>" + partyObj.sName + "</NAME>";
                    partyXml += "<ID>" + partyObj.iDbId.ToString() + "</ID>";

                    // participant's isdn address : to be added only in dial-out scenario
                    //if (partyObj.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_OUT)
                    //    partyXml += "<PHONE_LIST><PHONE1>" + partyObj.sAddress.Trim() + "</PHONE1></PHONE_LIST>";
                    //else
                    partyXml += "<PHONE_LIST></PHONE_LIST>";

                    // V35 
                    partyXml += "<INTERFACE>mpi</INTERFACE>";

                    // connection type
                    if (partyObj.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_IN)
                    {
                        partyXml += "<CONNECTION>dial_in</CONNECTION>";
                    }
                    else if (partyObj.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_OUT)
                    {
                        partyXml += "<CONNECTION>dial_out</CONNECTION>";
                    }
                    else if (partyObj.etConnType == NS_MESSENGER.Party.eConnectionType.DIRECT)
                    {
                        partyXml += "<CONNECTION>direct</CONNECTION>";

                    }
                    partyXml += "<MEET_ME_METHOD>party</MEET_ME_METHOD>";
                    logger.Trace("After meet me method...");
                    partyXml += "<NUM_TYPE>taken_from_service</NUM_TYPE>";
                    partyXml += "<BONDING>disabled</BONDING>";
                    partyXml += "<MULTI_RATE>enabled</MULTI_RATE>";

                    // V35 channels.
                    string polycomLineRate = "channel_6";
                    bool ret = EquateLineRate(partyObj.stLineRate.etLineRate, false, ref polycomLineRate);
                    if (!ret)
                    {
                        logger.Trace("Line rate conversion failed.");
                    }
                    partyXml += "<NET_CHANNEL_NUMBER>" + polycomLineRate + "</NET_CHANNEL_NUMBER>";


                    // Video Protocol
                    //if (partyObj.m_videoProtocol == NS_MESSENGER.Party.eVideoProtocol.AUTO)
                    partyXml += "<VIDEO_PROTOCOL>auto</VIDEO_PROTOCOL>";
                    //else
                    //{
                    //	if (partyObj.m_videoProtocol == NS_MESSENGER.Party.eVideoProtocol.H261)
                    //        partyXml += "<VIDEO_PROTOCOL></VIDEO_PROTOCOL>";
                    //	else 
                    //		partyXml += "<VIDEO_PROTOCOL>h263</VIDEO_PROTOCOL>"; 
                    //}

                    // Video conf = framed , Audio conf = voice
                    if (partyObj.etCallType == NS_MESSENGER.Party.eCallType.AUDIO)
                    {
                        partyXml += "<CALL_CONTENT>voice</CALL_CONTENT>";
                    }
                    else
                    {
                        partyXml += "<CALL_CONTENT>framed</CALL_CONTENT>";
                    }

                    partyXml += "<ALIAS><NAME /><ALIAS_TYPE>323_id</ALIAS_TYPE></ALIAS>";
                    partyXml += "<IP>255.255.255.255</IP>";
                    partyXml += "<SIGNALING_PORT>1720</SIGNALING_PORT>";
                    partyXml += "<VOLUME>5</VOLUME>";

                    // mcu's isdn address : to be added only in dial-in scenario
                    if (partyObj.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_IN)
                    {
                        partyXml += "<MCU_PHONE_LIST><PHONE1>" + partyObj.sMcuAddress.Trim() + "</PHONE1></MCU_PHONE_LIST>";
                    }
                    else
                    {
                        partyXml += "<MCU_PHONE_LIST><PHONE1></PHONE1></MCU_PHONE_LIST>";
                    }

                    if (partyObj.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_OUT)
                    {
                        partyXml += "<BONDING_PHONE>" + partyObj.sAddress.Trim() + "</BONDING_PHONE>";
                    }
                    else
                    {
                        partyXml += "<BONDING_PHONE></BONDING_PHONE>";
                    }

                    if (partyObj.etConnType == NS_MESSENGER.Party.eConnectionType.DIRECT)
                    {
                        partyXml += "<BONDING_PHONE></BONDING_PHONE>";
                    }
                    // mcu service name
                    partyXml += "<SERVICE_NAME>CODEC</SERVICE_NAME>";
                    
                    partyXml += "<SUB_SERVICE_NAME>ZERO</SUB_SERVICE_NAME>";
                    partyXml += "<AUTO_DETECT>false</AUTO_DETECT>";
                    partyXml += "<RESTRICT>false</RESTRICT>";
                    partyXml += "<ENHANCED_VIDEO>false</ENHANCED_VIDEO>";
                    partyXml += "<VIDEO_BIT_RATE>automatic</VIDEO_BIT_RATE>";
                    partyXml += "<AGC>true</AGC>";
                    partyXml += "</PARTY>";
                }
              	return true;
			}
			catch (Exception e)
			{
				// log the error and trash this participant info.
				string debugString = "Participant Not Added . Error = "+ e.Message ;
				debugString += ". PartyXML = " + partyXml;
				logger.Exception (100,debugString);
				return false;
			}
		}
        /** ZD 100768 **/
        private bool CreateXML_Party(ref string partyXml, NS_MESSENGER.Party partyObj, ref NS_MESSENGER.Conference conf)
        {
            logger.Trace("Party = " + partyObj.sName);
            if (partyObj.sName == null)
            {
                this.errMsg = "Invalid participant name.";
                return false;
            }
            if (partyObj.sName.Length < 3)
            {
                this.errMsg = "Invalid participant name.";
                return false;
            }

            partyXml = null;

            //ZD 103263
            string meetingid = "", passcode = "" , partyBJNAddress = "";
            int Bindex = 0;

            try
            {
                //ZD 103263 Start

                if (!String.IsNullOrEmpty(partyObj.sAddress))
                {

                    if (partyObj.sAddress.Trim().Contains("B"))
                    {
                        Bindex = partyObj.sAddress.Trim().IndexOf("B");
                        partyBJNAddress = partyObj.sAddress.Trim().Substring(0, Bindex); 

                        if (partyObj.sAddress.Split('+').Length > 0) //ZD 103964
                        {
                            meetingid = partyObj.sAddress.Split('B')[1].Split('+')[0]; //Meeting ID
                            passcode = partyObj.sAddress.Split('B')[1].Split('+')[1]; //Attendee passcode
                        }
                        partyObj.sDTMFSequence = meetingid + "##" + passcode;
                        logger.Trace(meetingid + " and " + passcode); //ZD 103964
                        partyObj.sAddress = partyBJNAddress;

                    }
                }
                //ZD 103263 End


                if (partyObj.etProtocol == NS_MESSENGER.Party.eProtocol.IP)
                {
                    // ip participant 
                    partyXml = "<PARTY>";
                    partyXml += "<NAME>" + partyObj.sName + "</NAME>";
                    partyXml += "<ID>" + partyObj.iDbId.ToString() + "</ID>";
                    partyXml += "<PHONE_LIST></PHONE_LIST>";
                    partyXml += "<INTERFACE>h323</INTERFACE>";
                    if (partyObj.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_IN)
                    {
                        partyXml += "<CONNECTION>dial_in</CONNECTION>";
                    }
                    else
                    {
                        if (partyObj.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_OUT)
                        {
                            partyXml += "<CONNECTION>dial_out</CONNECTION>";
                        }
                        else
                        {
                            partyXml += "<CONNECTION>direct</CONNECTION>";
                        }
                    }
                    //Meet me method
                    partyXml += "<MEET_ME_METHOD>party</MEET_ME_METHOD>";
                    partyXml += "<NUM_TYPE>taken_from_service</NUM_TYPE>";
                    partyXml += "<BONDING>auto</BONDING>";
                    partyXml += "<MULTI_RATE>auto</MULTI_RATE>";
                    partyXml += "<NET_CHANNEL_NUMBER>auto</NET_CHANNEL_NUMBER>";

                    //if (partyObj.etVideoProtocol == NS_MESSENGER.Party.eVideoProtocol.AUTO)
                    if (configParams.client == "VAOHIO")
                    {
                        partyXml += "<VIDEO_PROTOCOL>h263</VIDEO_PROTOCOL>";
                    }
                    else
                    {
                        partyXml += "<VIDEO_PROTOCOL>auto</VIDEO_PROTOCOL>";
                    }
                    //else
                    //{
                    //	if (partyObj.etVideoProtocol == NS_MESSENGER.Party.eVideoProtocol.H261)
                    //		partyXml += "<VIDEO_PROTOCOL>h261</VIDEO_PROTOCOL>"; 
                    //	else 
                    //		partyXml += "<VIDEO_PROTOCOL>h263</VIDEO_PROTOCOL>"; 
                    //}

                    //video conf = framed , audio conf = voice

                    if (partyObj.etCallType == NS_MESSENGER.Party.eCallType.AUDIO)
                    {
                        partyXml += "<CALL_CONTENT>voice</CALL_CONTENT>";
                    }
                    else
                    {
                        partyXml += "<CALL_CONTENT>framed</CALL_CONTENT>";
                    }

                    // Check if the address is an alias or an IP //ZD 100132 GateKeeperSupport START

                    if (((partyObj.etAddressType == NS_MESSENGER.Party.eAddressType.H323_ID || partyObj.etAddressType == NS_MESSENGER.Party.eAddressType.IP_ADDRESS) && partyObj.etType != NS_MESSENGER.Party.eType.CASCADE_LINK) || (partyObj.etType == NS_MESSENGER.Party.eType.CASCADE_LINK && partyObj.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_OUT && conf.bIsCasecadeofsametype)) // ZD 100768
                    {
                        // IP address or H323.ID 
                        if (partyObj.etType == NS_MESSENGER.Party.eType.CASCADE_LINK) // ZD 100768
                        {
                            partyXml += (conf.bE164 || conf.bH323) ? "<ALIAS><NAME>" + conf.sDialinNumber + "</NAME>" : "<ALIAS><NAME>" + partyObj.sCascadeBridgeExtNo + conf.iDbNumName.ToString() + "</NAME>";
                            partyXml += "<ALIAS_TYPE>323_id</ALIAS_TYPE></ALIAS>";
                        }
                        else
                        { 
                            logger.Trace(meetingid + " and " + passcode); //ZD 103964 Starts
                            partyXml += "<ALIAS>";
                            if (meetingid != "")
                            {
                                partyXml += "<NAME>" + meetingid + "." + passcode + "@" + partyObj.sAddress.Trim() + "</NAME>";
                                partyXml += "<ALIAS_TYPE>323_id</ALIAS_TYPE></ALIAS>";
                                partyXml += "<IP>0.0.0.0</IP>";
                            }
                            else
                            {
                                partyXml += "<NAME />";
                                partyXml += "<ALIAS_TYPE>323_id</ALIAS_TYPE></ALIAS>";
                                partyXml += "<IP>" + partyObj.sAddress.Trim() + "</IP>";
                            }
                            
                        } //ZD 103964 Ends
                       
                    }
                    else
                    {
                        if (partyObj.etType == NS_MESSENGER.Party.eType.CASCADE_LINK) // ZD 100768
                        {
                            partyXml += (conf.bE164 || conf.bH323) ? "<ALIAS><NAME>" + conf.sDialinNumber + "</NAME>" : "<ALIAS><NAME>" + conf.iDbNumName.ToString() + "</NAME>";
                            partyXml += "<ALIAS_TYPE>323_id</ALIAS_TYPE></ALIAS>";
                            partyXml += "<IP>" + partyObj.sAddress.Trim() + "</IP>";

                        }
                        else if (partyObj.etAddressType == NS_MESSENGER.Party.eAddressType.E_164)
                        {
                            // It is an E164 alias.
                            partyXml += "<ALIAS><NAME>" + partyObj.sAddress + "</NAME>";
                            partyXml += "<ALIAS_TYPE>e164</ALIAS_TYPE></ALIAS>";
                            if (partyObj.bIsOutsideNetwork == true)
                            {
                                partyXml += "<IP>" + partyObj.sGateKeeeperAddress.Trim() + "</IP>";
                            }
                            else
                            {
                                partyXml += "<IP>0.0.0.0</IP>";
                            }
                        }
                        //ZD 100132 GateKeeperSupport END
                        else
                        {
                            logger.Exception(100, "Invalid participant address type");
                        }
                    }
                    partyXml += "<SIGNALING_PORT>1720</SIGNALING_PORT>";
                    partyXml += "<VOLUME>5</VOLUME>";
                    partyXml += "<MCU_PHONE_LIST/>";
                    partyXml += "<BONDING_PHONE/>";

                    // mcu service name
                    partyXml += "<SERVICE_NAME>" + partyObj.sMcuServiceName + "</SERVICE_NAME>";

                    partyXml += "<SUB_SERVICE_NAME></SUB_SERVICE_NAME>";
                    partyXml += "<AUTO_DETECT>false</AUTO_DETECT>";
                    partyXml += "<RESTRICT>false</RESTRICT>";
                    partyXml += "<ENHANCED_VIDEO>false</ENHANCED_VIDEO>";

                    //FB 1742 start.
                    string videoBitRate = "automatic";
                    if (partyObj.cMcu.etType == NS_MESSENGER.MCU.eType.RMX)
                    {
                        bool ret = EquateLineRate(partyObj.stLineRate.etLineRate, true, ref videoBitRate);
                        if (!ret)
                        {
                            logger.Trace("Invalid Video Bit Rate.");
                        }
                    }
                    //FB 1742 end

                    if (configParams.client == "VAOHIO")
                    {
                        partyXml += "<VIDEO_BIT_RATE>384</VIDEO_BIT_RATE>";
                    }
                    else
                    {
                        partyXml += "<VIDEO_BIT_RATE>" + videoBitRate + "</VIDEO_BIT_RATE>";
                    }
                    partyXml += "<AGC>true</AGC>";

                    if (partyObj.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv7)
                    {
                        partyXml += "<IP_QOS>";
                        partyXml += "<QOS_ACTION>from_service</QOS_ACTION>";
                        partyXml += "<QOS_DIFF_SERV>precedence</QOS_DIFF_SERV>";
                        partyXml += "<QOS_IP_AUDIO>4</QOS_IP_AUDIO>";
                        partyXml += "<QOS_IP_VIDEO>4</QOS_IP_VIDEO>";
                        partyXml += "<QOS_TOS>delay</QOS_TOS>";
                        partyXml += "</IP_QOS>";
                        partyXml += "<RECORDING_PORT>no</RECORDING_PORT>";
                        partyXml += "<LAYOUT_TYPE>conference</LAYOUT_TYPE>";
                        partyXml += "<PERSONAL_LAYOUT>1x1</PERSONAL_LAYOUT>";
                        partyXml += "<PERSONAL_FORCE_LIST />";
                        partyXml += "<VIP>false</VIP> ";
                        partyXml += "<CONTACT_INFO_LIST />";
                        partyXml += "<LISTEN_VOLUME>5</LISTEN_VOLUME>";
                        partyXml += "<SIP_ADDRESS />";
                        partyXml += "<SIP_ADDRESS_TYPE>uri_type</SIP_ADDRESS_TYPE>";
                        partyXml += "<WEB_USER_ID>0</WEB_USER_ID>";
                        partyXml += "<UNDEFINED>false</UNDEFINED>";
                        partyXml += "<BACKUP_SERVICE_NAME />";
                        partyXml += "<BACKUP_SUB_SERVICE_NAME />";
                        partyXml += "<DEFAULT_TEMPLATE>false</DEFAULT_TEMPLATE>";
                        partyXml += "<NODE_TYPE>terminal</NODE_TYPE>";
                        partyXml += "<ENCRYPTION_EX>auto</ENCRYPTION_EX>";
                        partyXml += "<H323_PSTN>false</H323_PSTN>";
                        partyXml += "<EMAIL />";
                        partyXml += "<IS_RECORDING_LINK_PARTY>false</IS_RECORDING_LINK_PARTY>";
                        partyXml += "<USER_IDENTIFIER_STRING />";
                        partyXml += "<IDENTIFICATION_METHOD>called_phone_number</IDENTIFICATION_METHOD>";
                    }

                    //ZD 103263
                    if (partyObj.sDTMFSequence != null)
                    {
                        if (partyObj.sDTMFSequence.Trim() != "")
                        {


                            if (partyObj.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv7)
                            {
                                if (partyObj.sDTMFSequence.Trim() != "")
                                {
                                    partyXml += "<USER_IDENTIFIER_STRING>" + partyObj.sDTMFSequence.Trim() + "</USER_IDENTIFIER_STRING>";
                                }
                                else
                                {
                                    partyXml += "<USER_IDENTIFIER_STRING/>";
                                }

                            }
                        }
                    }


                    partyXml += "</PARTY>";
                }
                else if (partyObj.etProtocol == NS_MESSENGER.Party.eProtocol.ISDN)
                {
                    // ISDN participant
                    string partyAddress = "";  //FB 1740 - Audio Add-on implementation for Polycom sites
                    string phonePrefix = "";
                    partyAddress = partyObj.sAddress.Trim();

                    partyXml = "<PARTY>";
                    partyXml += "<NAME>" + partyObj.sName + "</NAME>";
                    partyXml += "<ID>" + partyObj.iDbId.ToString() + "</ID>";

                    // participant's isdn address : to be added only in dial-out scenario
                    if (partyObj.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_OUT)
                    {
                        //FB 1740 - Audio Add-on implementation for Polycom sites - code start

                        //partyXml += "<PHONE_LIST><PHONE1>" + partyObj.sAddress.Trim() + "</PHONE1></PHONE_LIST>";
                        
                        if (partyAddress.Trim().Contains("G"))
                        {
                            int Gindex = partyAddress.IndexOf("G");
                            partyAddress = partyAddress.Substring(0, Gindex);
                        }
                        if (partyAddress.Trim().Contains("D") && partyObj.bAudioBridgeParty)//ZD 101655
                        {
                            int Dindex = partyAddress.IndexOf("D");
                            partyAddress = partyAddress.Substring(0, Dindex);

                            //FB 2655 DTMF Start
                            //Audio Dial-in prefix is added before the address
                            //phonePrefix = configParams.audioDialNoPrefix;
                            phonePrefix = partyObj.AudioDialInPrefix;
                            //FB 2655 DTMF End
                        }

                        //if (partyObj.etVideoEquipment == NS_MESSENGER.Party.eVideoEquipment.RECORDER)
                        //{ 
                        //    //send gatewayAddress
                        //}
                        // check for gateway  
                        if (partyObj.sAddress.Trim().Contains("G"))
                        {
                            // gateway exists
                            if (partyObj.sAddress.Trim().Contains("D") && partyObj.bAudioBridgeParty)//ZD 101655
                            {
                                // dtmf sequence also exists. Hence, gateway value needs to be extracted between G & D.
                                int Gindex = partyObj.sAddress.IndexOf("G");
                                int Dindex = partyObj.sAddress.IndexOf("D");
                                partyObj.sGatewayAddress = partyObj.sAddress.Substring(Gindex + 1, Dindex - Gindex - 1);
                            }
                            else
                            {
                                int Gindex = partyObj.sAddress.IndexOf("G");
                                partyObj.sGatewayAddress = partyObj.sAddress.Substring(Gindex + 1);
                            }
                        }
                        // check for dtmf sequence
                        if (partyObj.sAddress.Trim().Contains("D") && partyObj.bAudioBridgeParty)//FB 2655
                        {
                            // dtmf sequence exists                            
                            int Dindex = partyObj.sAddress.IndexOf("D");
                            partyObj.sDTMFSequence = partyObj.sAddress.Substring(Dindex + 1);

                            // FB case#1632
                            // GENERIC STRING: ,,,,,,,,CONFCODE,#,,,,,*,,,,,LEADERPIN,# ,,,,,1 (MGC uses "p" for pauses)

                            // add the pre-ConfCode DTMF codes
                            partyObj.sDTMFSequence = partyObj.preConfCode + partyObj.sDTMFSequence; //FB 2655
                            //partyObj.sDTMFSequence = partyObj.cMcu.preConfCode + ":" + partyObj.sDTMFSequence;

                            // add the pre-LeaderPIN DTMF codes
                            partyObj.sDTMFSequence = partyObj.sDTMFSequence.Replace("+", partyObj.preLPin); //FB 2655
                            //partyObj.sDTMFSequence = partyObj.sDTMFSequence.Replace("+", (":"+ partyObj.cMcu.preLPin));

                            // add the post-LeaderPIN DTMF codes
                            partyObj.sDTMFSequence = partyObj.sDTMFSequence.Trim() + partyObj.postLPin; //FB 2655
                            //partyObj.sDTMFSequence = partyObj.sDTMFSequence.Trim() +":"+ partyObj.cMcu.postLPin;
                        }
                       

                        if (partyObj.sDTMFSequence != null)
                        {

                            if (partyObj.sDTMFSequence.Trim() != "")
                            {
                                partyXml += "<PHONE_LIST><PHONE1>" + phonePrefix.Trim() + partyAddress.Trim() + "</PHONE1></PHONE_LIST>";
                            }
                            else
                            {
                                partyXml += "<PHONE_LIST><PHONE1>" + partyAddress.Trim() + "</PHONE1></PHONE_LIST>";
                            }


                        }
                        else
                        {
                            partyXml += "<PHONE_LIST><PHONE1>" + partyAddress.Trim() + "</PHONE1></PHONE_LIST>";
                        }
                        //FB 1740 - Audio Add-on implementation for Polycom sites - code end
                    }
                    else
                    {
                        partyXml += "<PHONE_LIST></PHONE_LIST>";
                    }

                    // isdn 
                    partyXml += "<INTERFACE>isdn</INTERFACE>";

                    // connection type
                    if (partyObj.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_IN)
                    {
                        partyXml += "<CONNECTION>dial_in</CONNECTION>";
                    }
                    else
                    {
                        if (partyObj.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_OUT)
                        {
                            partyXml += "<CONNECTION>dial_out</CONNECTION>";
                        }
                        else
                        {
                            partyXml += "<CONNECTION>direct</CONNECTION>";
                        }
                    }
                    partyXml += "<MEET_ME_METHOD>party</MEET_ME_METHOD>";
                    logger.Trace("After meet me method...");
                    partyXml += "<NUM_TYPE>taken_from_service</NUM_TYPE>";
                    partyXml += "<BONDING>auto</BONDING>";
                    partyXml += "<MULTI_RATE>disabled</MULTI_RATE>";

                    // ISDN channels.
                    string polycomLineRate = "channel_6";
                    bool ret = EquateLineRate(partyObj.stLineRate.etLineRate, false, ref polycomLineRate);
                    if (!ret)
                    {
                        logger.Trace("Line rate conversion failed.");
                    }
                    partyXml += "<NET_CHANNEL_NUMBER>" + polycomLineRate + "</NET_CHANNEL_NUMBER>";

                    //if (partyObj.m_videoProtocol == NS_MESSENGER.Party.eVideoProtocol.AUTO)
                    //	partyXml += "<VIDEO_PROTOCOL>auto</VIDEO_PROTOCOL>"; 
                    //else
                    //{
                    //	if (partyObj.m_videoProtocol == NS_MESSENGER.Party.eVideoProtocol.H261)
                    partyXml += "<VIDEO_PROTOCOL>h261</VIDEO_PROTOCOL>";
                    //	else 
                    //		partyXml += "<VIDEO_PROTOCOL>h263</VIDEO_PROTOCOL>"; 
                    //}

                    //video conf = framed , audio conf = voice
                    if (partyObj.etCallType == NS_MESSENGER.Party.eCallType.AUDIO)
                    {
                        partyXml += "<CALL_CONTENT>voice</CALL_CONTENT>";
                    }
                    else
                    {
                        partyXml += "<CALL_CONTENT>framed</CALL_CONTENT>";
                    }

                    partyXml += "<ALIAS><NAME /><ALIAS_TYPE>323_id</ALIAS_TYPE></ALIAS>";
                    partyXml += "<IP>255.255.255.255</IP>";
                    partyXml += "<SIGNALING_PORT>1720</SIGNALING_PORT>";
                    partyXml += "<VOLUME>5</VOLUME>";

                    // mcu's isdn address : to be added only in dial-in scenario
                    if (partyObj.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_IN)
                    {
                        partyXml += "<MCU_PHONE_LIST><PHONE1>" + partyObj.sMcuAddress.Trim() + "</PHONE1></MCU_PHONE_LIST>";
                    }
                    else
                    {
                        partyXml += "<MCU_PHONE_LIST><PHONE1></PHONE1></MCU_PHONE_LIST>";
                    }

                    if (partyObj.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_OUT)
                    {
                        if (partyObj.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv7)
                        {
                            partyXml += "<BONDING_PHONE/>"; //FB 1740 Audio Add-on implementation for Polycom
                        }
                        else
                        {
                            //partyXml += "<BONDING_PHONE>" + partyObj.sAddress.Trim() + "</BONDING_PHONE>"; //Commented during FB 1740
                            partyXml += "<BONDING_PHONE>" + partyAddress.Trim() + "</BONDING_PHONE>"; //FB 1740 - Audio Add-on implementation for Polycom
                        }
                    }
                    else
                    {
                        partyXml += "<BONDING_PHONE></BONDING_PHONE>";
                    }

                    // mcu service name
                    partyXml += "<SERVICE_NAME>" + partyObj.sMcuServiceName.Trim() + "</SERVICE_NAME>";

                    partyXml += "<SUB_SERVICE_NAME>ZERO</SUB_SERVICE_NAME>";
                    partyXml += "<AUTO_DETECT>false</AUTO_DETECT>";
                    partyXml += "<RESTRICT>false</RESTRICT>";
                    partyXml += "<ENHANCED_VIDEO>false</ENHANCED_VIDEO>";
                    partyXml += "<VIDEO_BIT_RATE>automatic</VIDEO_BIT_RATE>";
                    partyXml += "<AGC>true</AGC>";
                    if (partyObj.sDTMFSequence != null)
                    {
                        if (partyObj.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv7) //FB 1740
                        {
                            if (partyObj.sDTMFSequence.Trim() != "")
                            {
                                partyXml += "<USER_IDENTIFIER_STRING>" + partyObj.sDTMFSequence.Trim() + "</USER_IDENTIFIER_STRING>";
                            }
                            else
                            {
                                partyXml += "<USER_IDENTIFIER_STRING/>";
                            }
                            //partyXml += "<IDENTIFICATION_METHOD>called_phone_number</IDENTIFICATION_METHOD>";
                        }
                    }
                    else
                    {
                        partyXml += "<USER_IDENTIFIER_STRING/>";
                    }
                    partyXml += "</PARTY>";
                }
                else if (partyObj.etProtocol == NS_MESSENGER.Party.eProtocol.MPI)
                {
                    // MPI participant
                    partyXml = "<PARTY>";
                    partyXml += "<NAME>" + partyObj.sName + "</NAME>";
                    partyXml += "<ID>" + partyObj.iDbId.ToString() + "</ID>";

                    // participant's isdn address : to be added only in dial-out scenario
                    //if (partyObj.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_OUT)
                    //    partyXml += "<PHONE_LIST><PHONE1>" + partyObj.sAddress.Trim() + "</PHONE1></PHONE_LIST>";
                    //else
                    partyXml += "<PHONE_LIST></PHONE_LIST>";

                    // V35 
                    partyXml += "<INTERFACE>mpi</INTERFACE>";

                    // connection type
                    if (partyObj.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_IN)
                    {
                        partyXml += "<CONNECTION>dial_in</CONNECTION>";
                    }
                    else if (partyObj.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_OUT)
                    {
                        partyXml += "<CONNECTION>dial_out</CONNECTION>";
                    }
                    else if (partyObj.etConnType == NS_MESSENGER.Party.eConnectionType.DIRECT)
                    {
                        partyXml += "<CONNECTION>direct</CONNECTION>";

                    }
                    partyXml += "<MEET_ME_METHOD>party</MEET_ME_METHOD>";
                    logger.Trace("After meet me method...");
                    partyXml += "<NUM_TYPE>taken_from_service</NUM_TYPE>";
                    partyXml += "<BONDING>disabled</BONDING>";
                    partyXml += "<MULTI_RATE>enabled</MULTI_RATE>";

                    // V35 channels.
                    string polycomLineRate = "channel_6";
                    bool ret = EquateLineRate(partyObj.stLineRate.etLineRate, false, ref polycomLineRate);
                    if (!ret)
                    {
                        logger.Trace("Line rate conversion failed.");
                    }
                    partyXml += "<NET_CHANNEL_NUMBER>" + polycomLineRate + "</NET_CHANNEL_NUMBER>";


                    // Video Protocol
                    //if (partyObj.m_videoProtocol == NS_MESSENGER.Party.eVideoProtocol.AUTO)
                    partyXml += "<VIDEO_PROTOCOL>auto</VIDEO_PROTOCOL>";
                    //else
                    //{
                    //	if (partyObj.m_videoProtocol == NS_MESSENGER.Party.eVideoProtocol.H261)
                    //        partyXml += "<VIDEO_PROTOCOL></VIDEO_PROTOCOL>";
                    //	else 
                    //		partyXml += "<VIDEO_PROTOCOL>h263</VIDEO_PROTOCOL>"; 
                    //}

                    // Video conf = framed , Audio conf = voice
                    if (partyObj.etCallType == NS_MESSENGER.Party.eCallType.AUDIO)
                    {
                        partyXml += "<CALL_CONTENT>voice</CALL_CONTENT>";
                    }
                    else
                    {
                        partyXml += "<CALL_CONTENT>framed</CALL_CONTENT>";
                    }

                    partyXml += "<ALIAS><NAME /><ALIAS_TYPE>323_id</ALIAS_TYPE></ALIAS>";
                    partyXml += "<IP>255.255.255.255</IP>";
                    partyXml += "<SIGNALING_PORT>1720</SIGNALING_PORT>";
                    partyXml += "<VOLUME>5</VOLUME>";

                    // mcu's isdn address : to be added only in dial-in scenario
                    if (partyObj.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_IN)
                    {
                        partyXml += "<MCU_PHONE_LIST><PHONE1>" + partyObj.sMcuAddress.Trim() + "</PHONE1></MCU_PHONE_LIST>";
                    }
                    else
                    {
                        partyXml += "<MCU_PHONE_LIST><PHONE1></PHONE1></MCU_PHONE_LIST>";
                    }

                    if (partyObj.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_OUT)
                    {
                        partyXml += "<BONDING_PHONE>" + partyObj.sAddress.Trim() + "</BONDING_PHONE>";
                    }
                    else
                    {
                        partyXml += "<BONDING_PHONE></BONDING_PHONE>";
                    }

                    if (partyObj.etConnType == NS_MESSENGER.Party.eConnectionType.DIRECT)
                    {
                        partyXml += "<BONDING_PHONE></BONDING_PHONE>";
                    }
                    // mcu service name
                    partyXml += "<SERVICE_NAME>CODEC</SERVICE_NAME>";

                    partyXml += "<SUB_SERVICE_NAME>ZERO</SUB_SERVICE_NAME>";
                    partyXml += "<AUTO_DETECT>false</AUTO_DETECT>";
                    partyXml += "<RESTRICT>false</RESTRICT>";
                    partyXml += "<ENHANCED_VIDEO>false</ENHANCED_VIDEO>";
                    partyXml += "<VIDEO_BIT_RATE>automatic</VIDEO_BIT_RATE>";
                    partyXml += "<AGC>true</AGC>";
                    partyXml += "</PARTY>";
                }
                return true;
            }
            catch (Exception e)
            {
                // log the error and trash this participant info.
                string debugString = "Participant Not Added . Error = " + e.Message;
                debugString += ". PartyXML = " + partyXml;
                logger.Exception(100, debugString);
                return false;
            }
        }
        #endregion
		
		#region Ongoing Conference List

		private bool ProcessXML_OngoingConfList(string responseXml,ref Queue conflist)
		{			
			try
			{
				//Load the response xml recd from the bridge and parse it
				XmlDocument xd = new XmlDocument();
				xd.LoadXml(responseXml);
				
				XmlNodeList nodelist;
				XmlNode node;

				// if description = OK , then continue processing
                string description = "";//FB 2553-RMX
				node = xd.SelectSingleNode("//RESPONSE_TRANS_CONF_LIST/RETURN_STATUS/DESCRIPTION");
                if (node != null)
                {
                    description = node.InnerXml.Trim();
                }
				if (description.Length < 0 || description.Contains("OK")!= true) 
				{
					this.errMsg = description;
					return false; //error
				}
				//### IGNORE THIS CHECK FOR NOW ###
				//check for any status change
				//node = xd.SelectSingleNode("//RESPONSE_TRANS_CONF_LIST/GET_LS/CONF_SUMMARY_LS/CHANGED");
				//string changed = node.InnerXml;
				//changed = changed.Trim();
				//if (changed.CompareTo("false")==0) 
				//	return(0); //no change in conf list since prev request so just return
				
							
				// Retreive all conferences	
				nodelist = xd.SelectNodes("//RESPONSE_TRANS_CONF_LIST/GET_LS/CONF_SUMMARY_LS/CONF_SUMMARY");
				
				//cycle through each conference one-by-one
				foreach(XmlNode node1 in nodelist)
				{
					// new conf object to store the each conf ongoing details
					NS_MESSENGER.Conference conf = new NS_MESSENGER.Conference();

					//name of conference
					XmlNode name = node1.SelectSingleNode("NAME");
					conf.sMcuName = name.InnerText;

					XmlNode id = node1.SelectSingleNode("ID");
					conf.stStatus.iMcuID = Int32.Parse(id.InnerText);

					//### NOT REQUIRED ###
					//XmlNode conf = node1.SelectSingleNode("CONF_CHANGE");
					//VRM_ERROR_HANDLER.ErrorHandler.(conf.InnerText);
					//XmlNode status = node1.SelectSingleNode("CONF_STATUS");
					//if (status.ChildNodes.Item(1).InnerText == "false") //CONF_EMPTY = true/false
					//	conf.m_status.m_isEmpty = false;
					//else 
					//	conf.m_status.m_isEmpty = true;
					//if (status.ChildNodes.Item(4).InnerText == "true") //RESOURCES_DEFICIENCY = true/false
					//	conf.m_status.m_isResourceDeficient = true;
					//else
					//	conf.m_status.m_isResourceDeficient = false;
					//### ADDITIONAL STATUS PARAMS . NOT REQUIRED. ###
					//status.ChildNodes.Item(0).InnerText; //CONF_OK = true/false				
					//status.ChildNodes.Item(2).InnerText; //SINGLE_PARTY = true/false
					//status.ChildNodes.Item(3).InnerText; //NOT_FULL = true/false
					//status.ChildNodes.Item(5).InnerText; //BAD_RESOURCES = true/false
					//status.ChildNodes.Item(6).InnerText; //PROBLEM_PARTY = true/false	
					// total number of connected parties
					//XmlNode numConnectedParties = node1.SelectSingleNode("NUM_CONNECTED_PARTIES");
					//conf.m_status.m_totalConnectedParties = Int32.Parse(numConnectedParties.InnerText);

					conflist.Enqueue(conf);
				}
			}
			catch(Exception e)
			{
				logger.Exception(100,e.Message);
				return false;
			}
			return true;
		}

		private bool ProcessXML_ReservationConfList(string responseXml,ref Queue conflist)
		{			
			try
			{
				//Load the response xml recd from the bridge and parse it
				XmlDocument xd = new XmlDocument();
				xd.LoadXml(responseXml);
				
				XmlNodeList nodelist;
				XmlNode node;

				// if description = OK , then continue processing
                string description = "";//FB 2553-RMX
				node = xd.SelectSingleNode("//RESPONSE_TRANS_CONF_LIST/RETURN_STATUS/DESCRIPTION");
                if (node != null)
                    description = node.InnerXml.Trim();
				if (description.Length < 0 || description.Contains("OK")!=true) 
				{
					this.errMsg = description;
					return false; //error
				}
				//### IGNORE THIS CHECK FOR NOW ###
				//check for any status change
				//node = xd.SelectSingleNode("//RESPONSE_TRANS_CONF_LIST/GET_LS/CONF_SUMMARY_LS/CHANGED");
				//string changed = node.InnerXml;
				//changed = changed.Trim();
				//if (changed.CompareTo("false")==0) 
				//	return(0); //no change in conf list since prev request so just return
				
							
				// Retreive all conferences	
				nodelist = xd.SelectNodes("//RESPONSE_TRANS_CONF_LIST/GET_LS/CONF_SUMMARY_LS/CONF_SUMMARY");
				
				//cycle through each conference one-by-one
				foreach(XmlNode node1 in nodelist)
				{
					// new conf object to store the each conf ongoing details
					NS_MESSENGER.Conference conf = new NS_MESSENGER.Conference();

					//name of conference
					XmlNode name = node1.SelectSingleNode("NAME");
					conf.sMcuName = name.InnerText;

					XmlNode id = node1.SelectSingleNode("ID");
					conf.stStatus.iMcuID = Int32.Parse(id.InnerText);

					//### NOT REQUIRED ###
					//XmlNode conf = node1.SelectSingleNode("CONF_CHANGE");
					//VRM_ERROR_HANDLER.ErrorHandler.(conf.InnerText);
					//XmlNode status = node1.SelectSingleNode("CONF_STATUS");
					//if (status.ChildNodes.Item(1).InnerText == "false") //CONF_EMPTY = true/false
					//	conf.m_status.m_isEmpty = false;
					//else 
					//	conf.m_status.m_isEmpty = true;
					//if (status.ChildNodes.Item(4).InnerText == "true") //RESOURCES_DEFICIENCY = true/false
					//	conf.m_status.m_isResourceDeficient = true;
					//else
					//	conf.m_status.m_isResourceDeficient = false;
					//### ADDITIONAL STATUS PARAMS . NOT REQUIRED. ###
					//status.ChildNodes.Item(0).InnerText; //CONF_OK = true/false				
					//status.ChildNodes.Item(2).InnerText; //SINGLE_PARTY = true/false
					//status.ChildNodes.Item(3).InnerText; //NOT_FULL = true/false
					//status.ChildNodes.Item(5).InnerText; //BAD_RESOURCES = true/false
					//status.ChildNodes.Item(6).InnerText; //PROBLEM_PARTY = true/false	
					// total number of connected parties
					//XmlNode numConnectedParties = node1.SelectSingleNode("NUM_CONNECTED_PARTIES");
					//conf.m_status.m_totalConnectedParties = Int32.Parse(numConnectedParties.InnerText);

					conflist.Enqueue(conf);
				}
			}
			catch(Exception e)
			{
				logger.Exception(100,e.Message);
				return false;
			}
			return true;
		}

		private bool ProcessXML_FindOngoingConf(string responseXml,ref NS_MESSENGER.Conference conf,ref bool searchResult)
		{	
			logger.Trace("ProcessXML_FindOngoingConf()");
			searchResult = false;
			try
			{
				//Load the response xml recd from the bridge and parse it
				XmlDocument xd = new XmlDocument();
				xd.LoadXml(responseXml);
				
				XmlNodeList nodelist;
				XmlNode node;

				// if description = OK , then continue processing
                string description = "";//FB 2553-RMX
				node = xd.SelectSingleNode("//RESPONSE_TRANS_CONF_LIST/RETURN_STATUS/DESCRIPTION");
                if (node != null)
                    description = node.InnerXml.Trim();
				if (description.Length < 0 || description.Contains("OK")!= true) 
				{
					this.errMsg = description;
					return false; //error
				}

				//### IGNORE THIS CHECK FOR NOW ###
				//check for any status change
				//node = xd.SelectSingleNode("//RESPONSE_TRANS_CONF_LIST/GET_LS/CONF_SUMMARY_LS/CHANGED");
				//string changed = node.InnerXml;
				//changed = changed.Trim();
				//if (changed.CompareTo("false")==0) 
				//	return(0); //no change in conf list since prev request so just return
							
				// Retreive all conferences	
				nodelist = xd.SelectNodes("//RESPONSE_TRANS_CONF_LIST/GET_LS/CONF_SUMMARY_LS/CONF_SUMMARY");
				
				//cycle through each conference one-by-one
				foreach(XmlNode node1 in nodelist)
				{	
					logger.Trace("in node...");

					//name of conference
                    //PSU RMX Fix Starts...
                    string dispName = "";
                    if (conf.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv6 || conf.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv7)
                        dispName = "NAME";
                    else
                        dispName = "DISPLAY_NAME";

                    XmlNode name = node1.SelectSingleNode(dispName);
                    //PSU RMX FIX Ends...
					string confBridgeName = name.InnerText;
					confBridgeName =confBridgeName.Trim();
					logger.Trace("bridge name = " + confBridgeName);
					conf.sDbName = conf.sDbName.Trim();
					logger.Trace("bridge name = " + conf.sDbName);
					if (confBridgeName.CompareTo(conf.sDbName) ==0)
					{
						//conference has been found 
						//ZD 100085
                        conf.etStatus = NS_MESSENGER.Conference.eStatus.ONGOING;
						XmlNode id = node1.SelectSingleNode("ID");
						conf.stStatus.iMcuID= Int32.Parse(id.InnerText);
						logger.Trace("conf.m_status.m_mcuID = " + conf.stStatus.iMcuID);
					
						//### NOT REQUIRED ###
						//XmlNode conf = node1.SelectSingleNode("CONF_CHANGE");
						//VRM_ERROR_HANDLER.ErrorHandler.DebugToEventLog(conf.InnerText);
						//XmlNode status = node1.SelectSingleNode("CONF_STATUS");
						//if (status.ChildNodes.Item(1).InnerText == "false") //CONF_EMPTY = true/false
						//	conf.m_status.m_isEmpty = false;
						//else 
						//	conf.m_status.m_isEmpty = true;
						//### ADDITIONAL STATUS PARAMS . NOT REQUIRED. ###
						//if (status.ChildNodes.Item(4).InnerText == "true") //RESOURCES_DEFICIENCY = true/false
						//	conf.m_status.m_isResourceDeficient = true;
						//else
						//	conf.m_status.m_isResourceDeficient = false;
						//status.ChildNodes.Item(0).InnerText; //CONF_OK = true/false				
						//status.ChildNodes.Item(2).InnerText; //SINGLE_PARTY = true/false
						//status.ChildNodes.Item(3).InnerText; //NOT_FULL = true/false
						//status.ChildNodes.Item(5).InnerText; //BAD_RESOURCES = true/false
						//status.ChildNodes.Item(6).InnerText; //PROBLEM_PARTY = true/false	
						// total number of connected parties
						//XmlNode numConnectedParties = node1.SelectSingleNode("NUM_CONNECTED_PARTIES");
						//conf.m_status.m_totalConnectedParties = Int32.Parse(numConnectedParties.InnerText);
						
						// conference has been found & all info retreived.
						//ZD 100085
                        if (conf.stStatus.iMcuID > 0)
                            db.UpdateConferenceStatus(conf.iDbID, conf.iInstanceID, conf.etStatus);
						searchResult = true;
						return true;						
					}
                   
				
				}
                

				this.errMsg = "Conference not found on the MCU";
				return false;
			}
			catch(Exception e)
			{
				logger.Exception(100,e.Message);
				return false;
			}			
		}

		private bool ProcessXML_FindReservationConf(string responseXml,ref NS_MESSENGER.Conference conf,ref bool searchResult)
		{	
			logger.Trace("ProcessXML_FindReservationConf()");
			searchResult = false;
            int cmpNames = 0;
            //ZD 100085 Starts
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(responseXml);

                XmlNodeList nodelist;

                string description = "";//FB 2553-RMX
                if (xd.SelectSingleNode("//RESPONSE_TRANS_CONF_LIST/RETURN_STATUS/DESCRIPTION") != null)
                    description = xd.SelectSingleNode("//RESPONSE_TRANS_CONF_LIST/RETURN_STATUS/DESCRIPTION").InnerText.Trim();
                if (description.Length < 0 || description.Contains("OK") != true)
                {
                    this.errMsg = description;
                    //return false; //error
                }

                //### IGNORE THIS CHECK FOR NOW ###
                //check for any status change
                //node = xd.SelectSingleNode("//RESPONSE_TRANS_CONF_LIST/GET_LS/CONF_SUMMARY_LS/CHANGED");
                //string changed = node.InnerXml;
                //changed = changed.Trim();
                //if (changed.CompareTo("false")==0) 
                //	return(0); //no change in conf list since prev request so just return

                // Retreive all conferences	
                nodelist = xd.SelectNodes("//RESPONSE_TRANS_CONF_LIST/GET_RES_LIST/RES_SUMMARY_LS/RES_SUMMARY");

                //cycle through each conference one-by-one
                foreach (XmlNode node1 in nodelist)
                {
                    logger.Trace("in node...");
                    cmpNames = 0;
                    //name of conference
                    string confBridgeName = node1.SelectSingleNode("NAME").InnerText.Trim();
                    
                    conf.sDbName = conf.sDbName.Trim();
                    cmpNames = confBridgeName.CompareTo(conf.sDbName);
                    
                    if (cmpNames != 0)
                    {
                        logger.Trace("Comparing conf names again...");
                        logger.Trace("Conf db name = " + conf.iDbID.ToString().Trim());
                        cmpNames = confBridgeName.CompareTo(conf.iDbID.ToString().Trim());
                    }
                    if (cmpNames == 0)
                    {
                        logger.Trace(" Conference match successful...");

                        XmlNode id = node1.SelectSingleNode("ID");
                        conf.stStatus.iMcuID = Int32.Parse(id.InnerText);
                        logger.Trace("conf.m_status.m_mcuID = " + conf.stStatus.iMcuID);

                        //### NOT REQUIRED ###
                        //XmlNode conf = node1.SelectSingleNode("CONF_CHANGE");
                        //VRM_ERROR_HANDLER.ErrorHandler.DebugToEventLog(conf.InnerText);
                        //XmlNode status = node1.SelectSingleNode("CONF_STATUS");
                        //if (status.ChildNodes.Item(1).InnerText == "false") //CONF_EMPTY = true/false
                        //	conf.m_status.m_isEmpty = false;
                        //else 
                        //	conf.m_status.m_isEmpty = true;
                        //### ADDITIONAL STATUS PARAMS . NOT REQUIRED. ###
                        //if (status.ChildNodes.Item(4).InnerText == "true") //RESOURCES_DEFICIENCY = true/false
                        //	conf.m_status.m_isResourceDeficient = true;
                        //else
                        //	conf.m_status.m_isResourceDeficient = false;
                        //status.ChildNodes.Item(0).InnerText; //CONF_OK = true/false				
                        //status.ChildNodes.Item(2).InnerText; //SINGLE_PARTY = true/false
                        //status.ChildNodes.Item(3).InnerText; //NOT_FULL = true/false
                        //status.ChildNodes.Item(5).InnerText; //BAD_RESOURCES = true/false
                        //status.ChildNodes.Item(6).InnerText; //PROBLEM_PARTY = true/false	
                        // total number of connected parties
                        //XmlNode numConnectedParties = node1.SelectSingleNode("NUM_CONNECTED_PARTIES");
                        //conf.m_status.m_totalConnectedParties = Int32.Parse(numConnectedParties.InnerText);

                        // conference has been found & all info retreived.
                        conf.etStatus = NS_MESSENGER.Conference.eStatus.ONGOING;
                        searchResult = true;
                        //return true;
                    }
                }

                // Don't return false yet. Maybe the conf is not an ongoing conf but a reservation.
                // So let's check that out as well.
                if (description.Length < 0 || description.Contains("OK") != true)
                {
                    logger.Trace("Conference is in reservation");

                    description = "";
                    if (xd.SelectSingleNode("//RESPONSE_TRANS_RES_LIST/RETURN_STATUS/DESCRIPTION") != null)
                        description = xd.SelectSingleNode("//RESPONSE_TRANS_RES_LIST/RETURN_STATUS/DESCRIPTION").InnerXml.Trim();
                    
                    if (description.Length < 0 || description.Contains("OK") != true)
                    {
                        this.errMsg = "Conference not found on the MCU";
                        return false;
                    }

                    // Retreive all conferences	
                    nodelist = xd.SelectNodes("//RESPONSE_TRANS_RES_LIST/GET_RES_LIST/RES_SUMMARY_LS/RES_SUMMARY");
                    int MCUConfId = 0;
                    string ConfName = "", displayName = "";

                    for (int i = 0; i < nodelist.Count; i++)
                    {
                        if (nodelist[i].SelectSingleNode("DISPLAY_NAME") != null)
                            displayName = nodelist[i].SelectSingleNode("DISPLAY_NAME").InnerText.Trim();

                        if (displayName.CompareTo(conf.sDbName) == 0)
                        {
                            if (nodelist[i].SelectSingleNode("ID") != null)
                                int.TryParse(nodelist[i].SelectSingleNode("ID").InnerText.Trim(), out MCUConfId);

                            if (nodelist[i].SelectSingleNode("NAME") != null)
                                ConfName = nodelist[i].SelectSingleNode("NAME").InnerText.Trim();

                            //searchResult = true;
                            conf.stStatus.iMcuID = MCUConfId;
                            conf.etStatus = NS_MESSENGER.Conference.eStatus.OnMCU;
                        }
                    }
                }

                if (conf.stStatus.iMcuID > 0)
                    db.UpdateConferenceStatus(conf.iDbID, conf.iInstanceID, conf.etStatus);

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }			
		}
        //ZD 100085 End
		#endregion
		
		#region Ongoing Conference 
		private bool CreateXML_OngoingConf(ref string confXml,NS_MESSENGER.Conference conf)
		{
			// xml for fetching individual conf details. 

			string commonXml = null;
			bool ret = CreateXML_CommonParams(conf.cMcu,ref commonXml);
			if (!ret) return false; // error in common params 

			confXml = "<TRANS_CONF_2>";
			confXml += commonXml;
			confXml += "<GET>";
			confXml += "<ID>" + conf.stStatus.iMcuID + "</ID>";
			confXml += "<OBJ_TOKEN>-1</OBJ_TOKEN>";
			confXml += "</GET></TRANS_CONF_2>";

			return true;
		}

        private bool ProcessXML_FindOngoingParty(string responseXml, ref NS_MESSENGER.Party party)//string partyNameFromDb,ref int partyIdOnMcu)
        {
            Boolean bRet = false;//FB 1552
            try
            {
                //Load the response xml
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(responseXml);

                // if description = OK , then continue processing
                string description = "";//FB 2553-RMX
                if (xd.SelectSingleNode("//RESPONSE_TRANS_CONF/RETURN_STATUS/DESCRIPTION") != null)
                    description = xd.SelectSingleNode("//RESPONSE_TRANS_CONF/RETURN_STATUS/DESCRIPTION").InnerXml.Trim();
                if (description.Length < 0 || description.Contains("OK") != true)
                {
                    this.errMsg = description;
                    return false; //error
                }

                // Retreive all participants
                XmlNodeList nodelist = xd.SelectNodes("//RESPONSE_TRANS_CONF/GET/CONFERENCE/ONGOING_PARTY_LIST/ONGOING_PARTY");

                //cycle through each party one-by-one
                foreach (XmlNode node in nodelist)
                {
                    string partyNameFromMcu = node.SelectSingleNode("PARTY").ChildNodes.Item(0).InnerText.Trim(); //NAME;
                    logger.Trace("Party name from mcu = " + partyNameFromMcu);
                    logger.Trace("Party name from db = " + party.sName);
                    //partyNameFromDb = partyNameFromDb.Trim();
                    if (partyNameFromMcu == party.sName)
                    {
                        //partyIdOnMcu = Int32.Parse(partyNode.ChildNodes.Item(1).InnerText.ToString()); //ID
                        party.iMcuId = Int32.Parse(node.SelectSingleNode("PARTY").ChildNodes.Item(1).InnerText.Trim()); //ID
                        logger.Trace("Party mcuid = " + party.iMcuId.ToString());

                        // party ongoing status
                        string ongoingPartyStatus = node.SelectSingleNode("ONGOING_PARTY_STATUS").ChildNodes.Item(1).InnerText.Trim();
                        logger.Trace("Party status = " + ongoingPartyStatus);
                        if (ongoingPartyStatus.Trim().ToLower().CompareTo("connected") == 0)
                        {
                            party.etStatus = NS_MESSENGER.Party.eOngoingStatus.FULL_CONNECT;
                        }
                        else
                        {
                            if (ongoingPartyStatus.Trim().ToLower().CompareTo("disconnected") == 0) //FB 2989
                            {
                                party.etStatus = NS_MESSENGER.Party.eOngoingStatus.DIS_CONNECT;
                            }
                            else
                            {
                                party.etStatus = NS_MESSENGER.Party.eOngoingStatus.PARTIAL_CONNECT;
                            }
                        }
                        logger.Trace("Ongoing party status : " + party.etStatus.ToString());

                        bRet = true;
                    }
                }
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
            //FB 1552
            //return true; 
            return bRet;
        }

		#endregion

		#region Add/Delete Party
		private bool CreateXML_AddParty(ref string addPartyXml,NS_MESSENGER.Party party,string confIdOnMcu)
		{
			try
			{
				string commonXml = null;
				bool ret = CreateXML_CommonParams(party.cMcu,ref commonXml);
				if (!ret) return false; // error in common params 

				string partyXml = null;ret = false;
				ret = CreateXML_Party(ref partyXml,party);
				if (!ret) return false; // error in creating party xml

				addPartyXml = null;
				addPartyXml = "<TRANS_CONF_1>";
				addPartyXml += commonXml;
				addPartyXml += "<ADD_PARTY>";
				addPartyXml += "<ID>" + confIdOnMcu + "</ID>";
				addPartyXml += partyXml;
				addPartyXml += "</ADD_PARTY>";
				addPartyXml += "</TRANS_CONF_1>";

				return true;
			}
			catch (Exception e)
			{
				logger.Exception (100,e.Message);
				return false;
			}
		}

		private bool ProcessXML_AddParty (ref string responseXml)
		{
			try
			{	
				//Load the response xml recd from the bridge and parse it
				XmlDocument xd = new XmlDocument();
				xd.LoadXml(responseXml);
				
				// if description = OK , then party was added successfully
                string description = "";//FB 2553-RMX
				XmlNode node = xd.SelectSingleNode("//RESPONSE_TRANS_CONF/RETURN_STATUS/DESCRIPTION");
                if (node != null)
                    description = node.InnerXml.Trim();
				if (description.Length < 0 || description.Contains("OK")!=true) 
				{
					this.errMsg = description;
					return false; //error
				}
			}
			catch(Exception e)
			{
				logger.Exception(100,e.Message);
				return false;
			}
			return true;
		}
		
		#endregion

		#region Terminal Services Operations
		private bool CreateXML_TerminateParty( ref string deletePartyXml,NS_MESSENGER.MCU mcu , int confIdOnMcu,int partyIdOnMcu)
		{
			try
			{
				string commonXml = null;
				bool ret = CreateXML_CommonParams(mcu,ref commonXml);
				if (!ret) return false; // error in common params 

				deletePartyXml = null;
				deletePartyXml = "<TRANS_CONF_2>";
				deletePartyXml += commonXml;
				deletePartyXml += "<DELETE_PARTY>";
				deletePartyXml += "<ID>"+ confIdOnMcu.ToString() + "</ID>";
				deletePartyXml += "<PARTY_ID>"+ partyIdOnMcu.ToString() +"</PARTY_ID>";
				deletePartyXml += "</DELETE_PARTY>";
				deletePartyXml += "</TRANS_CONF_2>";
			
				return true;
			}
			catch (Exception e)
			{
				logger.Exception(100,e.Message);
				return false;
			}
		}

		private bool CreateXML_ChangeDisplayLayoutOfParty( ref string newLayoutPartyXml,NS_MESSENGER.MCU mcu , int confIdOnMcu,int partyIdOnMcu,int layout)
		{
			try
			{
				string commonXml = null;
				bool ret = CreateXML_CommonParams(mcu,ref commonXml);
				if (!ret) return false; // error in common params 

				ret = false; string strLayout = null;
				ret = EquateVideoLayout(layout,ref strLayout);
				if (!ret) return false;

				newLayoutPartyXml = null;
				newLayoutPartyXml = "<TRANS_CONF_1>";
				newLayoutPartyXml += commonXml;
				newLayoutPartyXml += "<SET_PARTY_VIDEO_LAYOUT_EX>";
				newLayoutPartyXml += "<ID>"+ confIdOnMcu.ToString() + "</ID>";
				newLayoutPartyXml += "<PARTY_ID>"+ partyIdOnMcu.ToString() +"</PARTY_ID>";
				//logger.Trace ("In the party display layout...MCU ver : " + mcu.etType.ToString());
				if (mcu.etType == NS_MESSENGER.MCU.eType.ACCORDv7 || mcu.etType == NS_MESSENGER.MCU.eType.RMX) //FB 2553-RMX
				{
					newLayoutPartyXml += "<LAYOUT_TYPE>personal</LAYOUT_TYPE>";
				}
				newLayoutPartyXml += "<FORCE>";
				newLayoutPartyXml += "<LAYOUT>" + strLayout + "</LAYOUT>";
				newLayoutPartyXml += "<CELL><ID>1</ID><FORCE_STATE>auto</FORCE_STATE></CELL><CELL><ID>2</ID><FORCE_STATE>auto</FORCE_STATE></CELL></FORCE>";
				newLayoutPartyXml += "</SET_PARTY_VIDEO_LAYOUT_EX>";
				newLayoutPartyXml += "</TRANS_CONF_1>";
			
				return true;
			}
			catch (Exception e)
			{
				logger.Exception(100,e.Message);
				return false;
			}
		}
        private bool CreateXML_SetDisplayLayoutTypeToPersonal(ref string newLayoutTypeXml, NS_MESSENGER.MCU mcu, int confIdOnMcu, int partyIdOnMcu)
        {
            try
            {
                string commonXml = null;
                bool ret = CreateXML_CommonParams(mcu, ref commonXml);
                if (!ret) return false; // error in common params 

                ret = false; 

                newLayoutTypeXml = null;
                newLayoutTypeXml = "<TRANS_CONF_1>";
                newLayoutTypeXml += commonXml;
                newLayoutTypeXml += "<SET_PARTY_LAYOUT_TYPE>";
                    newLayoutTypeXml += "<ID>" + confIdOnMcu.ToString() + "</ID>";
                    newLayoutTypeXml += "<PARTY_ID>" + partyIdOnMcu.ToString() + "</PARTY_ID>";
                    newLayoutTypeXml += "<LAYOUT_TYPE>personal</LAYOUT_TYPE>";
                newLayoutTypeXml += "</SET_PARTY_LAYOUT_TYPE>";
                newLayoutTypeXml += "</TRANS_CONF_1>";

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
		private bool EquateVideoCodecs (NS_MESSENGER.Conference.eVideoCodec videoCodec,ref string polycomVideoCodec)
		{
			try 
			{
				// equating to VRM to Polycom values
				if (videoCodec == NS_MESSENGER.Conference.eVideoCodec.AUTO)
				{
					polycomVideoCodec = "auto";
				}
				else
				{
					if (videoCodec == NS_MESSENGER.Conference.eVideoCodec.H261)
					{
						polycomVideoCodec = "h261";
					}
					else
					{
						if (videoCodec == NS_MESSENGER.Conference.eVideoCodec.H263)
						{
							polycomVideoCodec = "h263";
						}
						else
						{
							polycomVideoCodec = "h264";
						}
					}
				}
				
				return true;
			}
			catch (Exception e)
			{
				logger.Exception(100,e.Message);
				return false;
			}
		}
		private bool EquateAudioCodec (NS_MESSENGER.Conference.eAudioCodec audioCodec, ref string polycomAudioCodec)
		{
			try 
			{
			
				if (audioCodec == NS_MESSENGER.Conference.eAudioCodec.G722_56)
					polycomAudioCodec = "g722_56";
				else
				{
					if (audioCodec == NS_MESSENGER.Conference.eAudioCodec.G728)
						polycomAudioCodec = "g728";
					else
					{
						if (audioCodec == NS_MESSENGER.Conference.eAudioCodec.G722_32)
							polycomAudioCodec = "g722_32";
						else
						{
							if (audioCodec == NS_MESSENGER.Conference.eAudioCodec.G722_24)
								polycomAudioCodec = "g722_24";
							else
							{
								if (audioCodec == NS_MESSENGER.Conference.eAudioCodec.G711_56)
									polycomAudioCodec = "g711_56";
								else
								{
									if (audioCodec == NS_MESSENGER.Conference.eAudioCodec.AUTO)
										polycomAudioCodec = "auto";								
								}
							}
						}
					}	
				}
				return true;
			}
			catch (Exception e)
			{
				logger.Exception(100,e.Message);
				return false;
			}
		}

		private bool EquateLineRate (NS_MESSENGER.LineRate.eLineRate lineRate,bool isIP, ref string polycomLineRate)
		{
			try 
			{
                // equating VRM to Polycom values

                
                if (isIP)
                {
                    #region IP linerate mappings
                    //Modified during FB 1742
                    switch (lineRate)
                    {
                        case NS_MESSENGER.LineRate.eLineRate.K64:
                            {
                                polycomLineRate = "64";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.K128:
                            {
                                polycomLineRate = "128";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.K192:
                            {
                                polycomLineRate = "128";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.K256:
                            {
                                polycomLineRate = "256";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.K320:
                            {
                                polycomLineRate = "256";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.K384:
                            {
                                polycomLineRate = "384";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.K512:
                            {
                                polycomLineRate = "512";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.K768:
                            {
                                polycomLineRate = "768";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.M1024:
                            {
                                polycomLineRate = "1024";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.M1152:
                            {
                                polycomLineRate = "1152";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.M1250:
                            {
                                polycomLineRate = "1152";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.M1472:
                            {
                                polycomLineRate = "1472";
                                break;
                            }

                        case NS_MESSENGER.LineRate.eLineRate.M1536:
                            {
                                polycomLineRate = "1472";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.M1792:
                            {
                                polycomLineRate = "1472";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.M1920:
                            {
                                polycomLineRate = "1920";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.M2048:
                            {
                                polycomLineRate = "1920";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.M2560:
                            {
                                polycomLineRate = "1920";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.M3072:
                            {
                                polycomLineRate = "1920";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.M3584:
                            {
                                polycomLineRate = "1920";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.M4096:
                            {
                                polycomLineRate = "4096";
                                break;
                            }
                        default:
                            {
                                // default is 384 kbps
                                polycomLineRate = "384";
                                break;
                            }
                    }
#endregion

                    #region IP linerate mappings - Commented during FB 1742

                    //if (lineRate == NS_MESSENGER.LineRate.eLineRate.K64)
                    //    polycomLineRate = "64";
                    //else
                    //{
                    //    if (lineRate == NS_MESSENGER.LineRate.eLineRate.K128)
                    //        polycomLineRate = "128";
                    //    else
                    //    {
                    //        if (lineRate == NS_MESSENGER.LineRate.eLineRate.K256)
                    //            polycomLineRate = "256";
                    //        else
                    //        {
                    //            if (lineRate == NS_MESSENGER.LineRate.eLineRate.K512)
                    //                polycomLineRate = "512";
                    //            else
                    //            {
                    //                if (lineRate == NS_MESSENGER.LineRate.eLineRate.K768)
                    //                    polycomLineRate = "768";
                    //                else
                    //                {
                    //                    if (lineRate == NS_MESSENGER.LineRate.eLineRate.M1152)
                    //                        polycomLineRate = "1152";
                    //                    else
                    //                    {
                    //                        if (lineRate == NS_MESSENGER.LineRate.eLineRate.M1472)
                    //                            polycomLineRate = "1472";
                    //                        else
                    //                        {
                    //                            if (lineRate == NS_MESSENGER.LineRate.eLineRate.M1536)
                    //                                polycomLineRate = "1536";
                    //                            else
                    //                            {
                    //                                if (lineRate >= NS_MESSENGER.LineRate.eLineRate.M1920)
                    //                                    polycomLineRate = "1920";
                    //                            }
                    //                        }

                    //                    }
                    //                }
                    //            }
                    //        }
                    //    }
                    //}
                    #endregion
                }
                else
                {
                    #region ISDN linerate mappings
                    if (lineRate == NS_MESSENGER.LineRate.eLineRate.K64)
                        polycomLineRate = "channel_1";
                    else
                    {
                        if (lineRate == NS_MESSENGER.LineRate.eLineRate.K128)
                            polycomLineRate = "channel_2";
                        else
                        {
                            if (lineRate == NS_MESSENGER.LineRate.eLineRate.K256)
                                polycomLineRate = "channel_4";
                            else
                            {
                                if (lineRate == NS_MESSENGER.LineRate.eLineRate.K512)
                                    polycomLineRate = "channel_8";
                                else
                                {
                                    if (lineRate == NS_MESSENGER.LineRate.eLineRate.K768)
                                        polycomLineRate = "channel_12";
                                    else
                                    {
                                        if (lineRate == NS_MESSENGER.LineRate.eLineRate.M1152)
                                            polycomLineRate = "channel_18";
                                        else
                                        {
                                            if (lineRate == NS_MESSENGER.LineRate.eLineRate.M1472)
                                                polycomLineRate = "channel_23";
                                            else
                                            {
                                                if (lineRate == NS_MESSENGER.LineRate.eLineRate.M1536)
                                                    polycomLineRate = "channel_24";
                                                else
                                                {
                                                    if (lineRate == NS_MESSENGER.LineRate.eLineRate.M1920)
                                                        polycomLineRate = "channel_30";
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    #endregion
                }

				return true;
			}
			catch (Exception e)
			{
				logger.Exception(100,e.Message);
				return false;
			}
		}


		private bool EquateVideoLayout (int videoLayoutID, ref string polycomVideoLayoutValue)
		{

#if COMMENT_DO_NOT_DELETE
Dan's email 10/22/06 - MGC equivalent mappings of Codian layout code :
enumeration 	1x1 Codian # 1 � Classic and Quad Mode 
enumeration 	1x2 No Codian layout avail � Classic and Quad Mode Image 16
enumeration 	2x1 No Codian layout avail � Classic and Quad Mode 
enumeration 	2x2 Codian # 2 � Classic and Quad Mode
enumeration 	1and5 Codian # 5 � Classic Mode
enumeration 	3x3 Codian # 3 � Classic and Quad Mode
enumeration 	1x2Ver Codian # 16 � Classic Mode
enumeration 	1x2Hor Codian # 12 � Classic Mode
enumeration 	1and2Hor No Codian layout avail � Classic Mode Image 61
enumeration 	1and2Ver Codian # 17 � Classic Mode
enumeration 	1and3Hor No Codian layout avail � Classic Mode Image 62
enumeration 	1and3Ver Codian # 18 � Classic Mode
enumeration 	1and4Ver Codian # 19 � Classic Mode
enumeration 	1and4Hor No Codian layout avail � Classic Mode Image 63
enumeration 	1and8Central Codian # 24 � Classic Mode
enumeration 	1and8Upper No Codian layout avail � Classic Mode Image 60
enumeration 	1and2HorUpper Codian # 13 � Classic Mode
enumeration 	1and3HorUpper Codian # 14 � Classic Mode
enumeration 	1and4HorUpper Codian # 15 � Classic Mode
enumeration 	1and8Lower Codian # 20 � Classic Mode
enumeration 	1and7 Codian # 6 � Classic Mode
enumeration 	4x4 Codian # 4 � Quad Mode
enumeration 	2and8 Codian # 25 � Quad Mode
enumeration 	1and12 Codian # 33 � Quad Mode
enumeration 	1x1Qcif No Codian layout avail � Quad Mode
#endif
            polycomVideoLayoutValue = "1x1";
			try
			{
				switch (videoLayoutID)
				{
					case 1:
					{
						polycomVideoLayoutValue = "1x1";
						break;
					}
					case 2:
					{
						polycomVideoLayoutValue = "2x2";
						break;
					}
					case 3:
					{
						polycomVideoLayoutValue = "3x3";
						break;
					}
                    case 4: //FB 2335
                    {
                        polycomVideoLayoutValue = "4x4";
                        break;
                    }
					case 5:
					{
						polycomVideoLayoutValue = "1and5";
						break;
					}
					case 6:
					{
						polycomVideoLayoutValue = "1and7";
						break;
					}
					// case 7-11 : layout is not supported 
					case 12:
					{
						polycomVideoLayoutValue = "1x2Hor";
						break;
					}
					case 13:
					{
						polycomVideoLayoutValue = "1and2HorUpper";
						break;
					}
					case 14:
					{
						polycomVideoLayoutValue = "1and3HorUpper";
						break;
					}
					case 15:
					{
						polycomVideoLayoutValue = "1and4HorUpper";
						break;
					}
					case 16:
					{
						// Changed from 1x2Ver to 1x2 on BTBoces request (case 1606)
						polycomVideoLayoutValue = "1x2";
						break;
					}
					case 17:
					{
						polycomVideoLayoutValue = "1and2Ver";
						break;
					}
					case 18:
					{
						polycomVideoLayoutValue = "1and3Ver";
						break;
					}
					case 19:
					{
						polycomVideoLayoutValue = "1and4Ver";
						break;
					}
					case 20:
					{
						polycomVideoLayoutValue = "1and8Lower";
						break;
					}
					
					//case 21-23 : layout is not supported
					case 24:
					{
						polycomVideoLayoutValue = "1and8Central"; 
						break;
					}
					case 25:
					{
						polycomVideoLayoutValue = "2and8"; 
						break;
					}
					//case 26-32: layout is not supported					
					case 33:
					{
						polycomVideoLayoutValue = "1and12"; 
						break;
					}
                    case 60://FB 2335
                    {
                        polycomVideoLayoutValue = "1and8Upper";
                        break;
                    }
                    case 61://FB 2335
                    {
                        polycomVideoLayoutValue = "1and2Hor";
                        break;
                    }
                    case 62://FB 2335
                    {
                        polycomVideoLayoutValue = "1and3Hor";
                        break;
                    }
                    case 63://FB 2335
                    {
                        polycomVideoLayoutValue = "1and4Hor";
                        break;
                    }
                    
                        

                    
                       
					//case 33-42: layout is not supported					

					//case 101-109 are all custom to btboces for MGC accord bridges
					case 101:
					{
						polycomVideoLayoutValue = "1x1"; 
						break;
					}
					case 102:
					{
						polycomVideoLayoutValue = "1x2";//"1x2Ver"; 
						break;
					}
					case 103:
					{
						polycomVideoLayoutValue = "1and2HorUpper"; //"1x2"; 
						break;
					}
					case 104:
					{
						polycomVideoLayoutValue = "2x2"; //"1and2HorUpper"; 
						break;
					}
					case 105:
					{
						polycomVideoLayoutValue = "1and5"; //"1and2Ver"; 
						break;
					}
					case 106:
					{
						polycomVideoLayoutValue = "1x2Ver"; //"2x2"; 
						break;
					}
					case 107:
					{
						polycomVideoLayoutValue = "1and2Ver";//"1and3Ver"; 
						break;
					}
					case 108:
					{
						polycomVideoLayoutValue = "1and3Ver"; //"1and5"; 
						break;
					}
					case 109:
					{
						polycomVideoLayoutValue = "1and7"; 
						break;
					}				

					default:
					{
						// layout not supported
						this.errMsg = "This video layout is not supported on Polycom MCU.";
						return false;
					}
				}
				return true;
			}
			catch (Exception e)
			{
				logger.Exception(100,e.Message);
				return (false);
			}		
		}

		private bool CreateXML_ChangeDisplayLayoutOfConference( ref string newLayoutPartyXml,NS_MESSENGER.MCU mcu , int confIdOnMcu,int layout)
		{
			try
			{
				string commonXml = null;
				bool ret = CreateXML_CommonParams(mcu,ref commonXml);
				if (!ret) return false; // error in common params 

				ret = false; string strLayout = "1x1";
				ret = EquateVideoLayout(layout,ref strLayout);
				if (!ret) return false; // error  

				newLayoutPartyXml = null;
				newLayoutPartyXml = "<TRANS_CONF_1>";
				newLayoutPartyXml += commonXml;
				newLayoutPartyXml += "<SET_VIDEO_LAYOUT>";
				newLayoutPartyXml += "<ID>"+ confIdOnMcu.ToString() + "</ID>";
				if (mcu.etType == NS_MESSENGER.MCU.eType.ACCORDv7)
				{
						newLayoutPartyXml += "<LAYOUT_TYPE>conference</LAYOUT_TYPE>";
				}
				newLayoutPartyXml += "<FORCE>";
				newLayoutPartyXml += "<LAYOUT>" + strLayout + "</LAYOUT>";
				newLayoutPartyXml += "<CELL><ID>1</ID><FORCE_STATE>auto</FORCE_STATE></CELL><CELL><ID>2</ID><FORCE_STATE>auto</FORCE_STATE></CELL></FORCE>";
				newLayoutPartyXml += "</SET_VIDEO_LAYOUT>";
				newLayoutPartyXml += "</TRANS_CONF_1>";
			
				return true;
			}
			catch (Exception e)
			{
				logger.Exception(100,e.Message);
				return false;
			}
		}

        private bool CreateXML_SetLectureMode(ref string lectureModeXml, NS_MESSENGER.MCU mcu, int confIdOnMcu, string lecturerName, bool isLectureMode)//FB 2553-RMX
        {
            try
            {
                string commonXml = null;
                bool ret = CreateXML_CommonParams(mcu, ref commonXml);
                if (!ret) return false; // error in common params                 

                lectureModeXml = null;
                lectureModeXml = "<TRANS_CONF_1>";
                lectureModeXml += commonXml;
                lectureModeXml += "<SET_LECTURE_MODE>";
                lectureModeXml += "<ID>" + confIdOnMcu.ToString() + "</ID>";
                lectureModeXml += "<LECTURE_MODE>";
                lectureModeXml += "<ON>true</ON>";
                lectureModeXml += "<TIMER>false</TIMER>";
                lectureModeXml += "<INTERVAL>15</INTERVAL>";
                lectureModeXml += "<AUDIO_ACTIVATED>false</AUDIO_ACTIVATED>";
                lectureModeXml += "<LECTURE_NAME>" + lecturerName + "</LECTURE_NAME>"; //FB 2553-RMX
                if (isLectureMode)
                    lectureModeXml += "<LECTURE_MODE_TYPE>lecture_mode</LECTURE_MODE_TYPE>";
                else
                    lectureModeXml += "<LECTURE_MODE_TYPE>lecture_none</LECTURE_MODE_TYPE>";
                lectureModeXml += "<LECTURE_ID>0</LECTURE_ID>";
                lectureModeXml += "</LECTURE_MODE>";
                lectureModeXml += "</SET_LECTURE_MODE>";
                lectureModeXml += "</TRANS_CONF_1>";

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
		private bool CreateXML_MuteParty(ref string mutePartyXml,NS_MESSENGER.MCU mcu, int confIdOnMcu,int partyIdOnMcu,bool audioMute,bool videoMute) //FB 2553-RMX
		{
			try
			{
				string commonXml = null;
				bool ret = CreateXML_CommonParams(mcu,ref commonXml);
				if (!ret) return false; // error in common params 

				mutePartyXml = null;
				mutePartyXml = "<TRANS_CONF_2>";
				mutePartyXml += commonXml;
				mutePartyXml += "<SET_AUDIO_VIDEO_MUTE>";
				mutePartyXml += "<ID>"+ confIdOnMcu.ToString() + "</ID>";
				if (audioMute)
					mutePartyXml += "<AUDIO_MUTE>true</AUDIO_MUTE>";
				else
					mutePartyXml += "<AUDIO_MUTE>false</AUDIO_MUTE>";
            
                mutePartyXml += "<PARTY_ID>"+ partyIdOnMcu.ToString() +"</PARTY_ID>";
				mutePartyXml += "</SET_AUDIO_VIDEO_MUTE>";
				mutePartyXml += "</TRANS_CONF_2>";
			
				return true;
			}
			catch (Exception e)
			{
				logger.Exception(100,e.Message);
				return false;
			}
		}


        private bool CreateXML_MuteVideoParty(ref string mutePartyXml, NS_MESSENGER.MCU mcu, int confIdOnMcu, int partyIdOnMcu, bool audioMute, bool videoMute) //FB 2553-RMX
        {
            try
            {
                string commonXml = null;
                bool ret = CreateXML_CommonParams(mcu, ref commonXml);
                if (!ret) return false; // error in common params 

                mutePartyXml = null;
                mutePartyXml = "<TRANS_CONF_2>";
                mutePartyXml += commonXml;
                mutePartyXml += "<SET_AUDIO_VIDEO_MUTE>";
                mutePartyXml += "<ID>" + confIdOnMcu.ToString() + "</ID>";
               
                if (videoMute) //FB 2553-RMX
                    mutePartyXml += "<VIDEO_MUTE>true</VIDEO_MUTE>";
                else
                    mutePartyXml += "<VIDEO_MUTE>false</VIDEO_MUTE>";

                mutePartyXml += "<PARTY_ID>" + partyIdOnMcu.ToString() + "</PARTY_ID>";
                mutePartyXml += "</SET_AUDIO_VIDEO_MUTE>";
                mutePartyXml += "</TRANS_CONF_2>";

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
		
		private bool CreateXML_TerminateConference(ref string terminateXml,NS_MESSENGER.MCU mcu, int confIdOnMcu)
		{
			try 
			{
				string commonXml = null;
				bool ret = CreateXML_CommonParams(mcu,ref commonXml);
				if (!ret) return false; 
	
				terminateXml = "<TRANS_CONF_2>";
				terminateXml += commonXml;
				terminateXml += "<TERMINATE_CONF>";
				terminateXml += "<ID>" +  confIdOnMcu.ToString() + "</ID>";
				terminateXml += "</TERMINATE_CONF></TRANS_CONF_2>";

				return true;
			}			
			catch (Exception e)
			{
				logger.Exception(100,e.Message);
				return false;
			}
		}
		private bool ProcessXML_CommonConfOps (ref string responseXml)
		{
			try
			{	
				//Load the response xml recd from the bridge and parse it
				XmlDocument xd = new XmlDocument();
				xd.LoadXml(responseXml);
				
				// if description = OK , then party was deleted successfully
                string description = "";//FB 2553-RMX
				XmlNode node = xd.SelectSingleNode("//RESPONSE_TRANS_CONF/RETURN_STATUS/DESCRIPTION");
                if (node != null)
                    description = node.InnerXml.Trim();
                if (!description.Contains("progress")) // Status sent by MGC //FB 2568
				{
                    if (!description.Contains("OK")) // Status sent by RMX
                    {
                        this.errMsg = description;
                        return false; //error
                    }
				}
			}
			catch(Exception e)
			{
				logger.Exception(100,e.Message);
				return false;
			}
			return true;
		}
						
		private bool CreateXML_ExtendEndTime(ref string extendXml,NS_MESSENGER.MCU mcu,int iConfMcuID,DateTime newConfEndTime)
		{
			try 
			{
				string commonXml = null;
				bool ret = CreateXML_CommonParams(mcu,ref commonXml);
				if (!ret) return false; 
	
				extendXml = "<TRANS_CONF_2>";
				extendXml += commonXml;
				extendXml += "<SET_END_TIME>";
				extendXml += "<ID>" + iConfMcuID + "</ID>";
				extendXml += "<END_TIME>" + newConfEndTime.ToString("s") + "</END_TIME>";
				extendXml += "</SET_END_TIME></TRANS_CONF_2>";

				return true;
			}			
			catch (Exception e)
			{
				logger.Exception (100,e.Message);
				return false;
			}
		}
				
		#endregion

		internal bool AddPartyToMcu (NS_MESSENGER.Conference conf,NS_MESSENGER.Party party) 
		{
			try
			{
				int tryAgainCount = 0;
                bool tryAgain = false;
                bool searchResult = false;
                bool ret = false;
                // create a tmp conf which is for this party only.
                NS_MESSENGER.Conference partyConf = new NS_MESSENGER.Conference();

                do
                {
                    tryAgain = false;
                    ret = false;
                    searchResult = false;
                    
                    // Login to the bridge the party is on.
                    ret = LoginToBridge(ref party.cMcu);
                    if (!ret)
                    {
                        logger.Exception(100, "Login to bridge failed. Bridge Name = " + party.cMcu.sName);
                        return false;
                    }

                    // Fetch common parameters.
                    ret = false;
                    string commonXML = null;
                    ret = CreateXML_CommonParams(party.cMcu, ref commonXML);
                    if (!ret || commonXML.Length < 1) return false;// error

                    // check if conf is running on the bridge
                    // if true, get the conf.bridge_id (bridge-specific id . this is diff from the db-specific conf.id)		
                    // build the xml for fetching all ongoing confs
                    ret = false;
                    string confXml = null;
                    partyConf.cMcu = party.cMcu;
                    ret = CreateXML_OngoingConfList(partyConf.cMcu, ref confXml);
                    if (!ret) return false; // error

                    // send the transaction
                    ret = false;
                    string resXml = null;
                    if (partyConf.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv6)
                    {
                        ret = SendTransaction_overXAP(confXml, ref resXml);
                    }
                    else
                    {
                        ret = SendTransaction_overXMLHTTP(partyConf.cMcu, confXml, ref resXml);
                    }
                    if (!ret)
                        return false; //error

                    // get the conf.status.bridge_id 
                    //process the response				
                    ret = false; 
                    partyConf.sDbName = conf.sDbName;
                    ret = ProcessXML_FindOngoingConf(resXml, ref partyConf, ref searchResult);
                    if (!ret)
                    {
                        if (tryAgainCount < 5) // repeat max 5 times 
                        {
                            tryAgainCount++;
                            tryAgain = true;
                        }
                        else
                        {
                            return false; //error
                        }
                    }
                } while (tryAgain == true);

				if (searchResult)
				{
                    logger.Trace("Into Add Party Finally");
					// if conf found, add the participant
					string addPartyXml = null;ret = false;
					ret = CreateXML_AddParty(ref addPartyXml,party,partyConf.stStatus.iMcuID.ToString());
					if (!ret) return false; //error

					// send the transaction
					ret= false;
					string resAddPartyXml = null;
					if (partyConf.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv6)
					{
						ret = SendTransaction_overXAP(addPartyXml,ref resAddPartyXml);
					}
					else
					{
						ret = SendTransaction_overXMLHTTP(partyConf.cMcu,addPartyXml,ref resAddPartyXml);
					}
					if (!ret) return false; //error


					// Process the response
					ret = false;
					ret = ProcessXML_AddParty(ref resAddPartyXml);
					if (!ret) return false; //error

					return true;
				}
			
				return false;
			}
			catch (Exception e)
			{
				logger.Exception(100,e.Message);
				return false;
			}
		}


        //FB 2486 Start

        #region Add Message to Party

        internal bool AddMessageToParty(NS_MESSENGER.Conference conf, NS_MESSENGER.Party party, string MessageText, int sendmsg)//FB 3059
        {
            try
            {
                int tryAgainCount = 0;
                bool tryAgain = false;
                bool searchResult = false;
                bool ret = false;
                // create a tmp conf which is for this party only.
                NS_MESSENGER.Conference partyConf = new NS_MESSENGER.Conference();

                do
                {
                    tryAgain = false;
                    ret = false;
                    searchResult = false;

                    // Login to the bridge the party is on.
                    ret = LoginToBridge(ref party.cMcu);
                    if (!ret)
                    {
                        logger.Exception(100, "Login to bridge failed. Bridge Name = " + party.cMcu.sName);
                        return false;
                    }

                    // Fetch common parameters.
                    ret = false;
                    string commonXML = null;
                    ret = CreateXML_CommonParams(party.cMcu, ref commonXML);
                    if (!ret || commonXML.Length < 1) return false;// error

                    // check if conf is running on the bridge
                    // if true, get the conf.bridge_id (bridge-specific id . this is diff from the db-specific conf.id)		
                    // build the xml for fetching all ongoing confs
                    ret = false;
                    string confXml = null;
                    partyConf.cMcu = party.cMcu;
                    ret = CreateXML_OngoingConfList(partyConf.cMcu, ref confXml);
                    if (!ret) return false; // error

                    // send the transaction
                    ret = false;
                    string resXml = null;
                    if (partyConf.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv6)
                    {
                        ret = SendTransaction_overXAP(confXml, ref resXml);
                    }
                    else
                    {
                        ret = SendTransaction_overXMLHTTP(partyConf.cMcu, confXml, ref resXml);
                    }
                    if (!ret)
                        return false; //error

                    // get the conf.status.bridge_id 
                    //process the response				
                    ret = false;
                    partyConf.sDbName = conf.sDbName;
                    ret = ProcessXML_FindOngoingConf(resXml, ref partyConf, ref searchResult);
                    if (!ret)
                    {
                        if (tryAgainCount < 5) // repeat max 5 times 
                        {
                            tryAgainCount++;
                            tryAgain = true;
                        }
                        else
                        {
                            return false; //error
                        }
                    }
                } while (tryAgain == true);

                if (searchResult)
                {
                    // find the participant now in the conference 
                    logger.Trace("Find Party to Send Message Overlay...");
                    //create the xml for ongoing conf
                    string ongoingConfXml = null; ret = false;
                    ret = CreateXML_OngoingConf(ref ongoingConfXml, partyConf);
                    if (!ret) return false; //error

                    // send the transaction
                    ret = false;
                    string resOngoingConfXml = null;
                    if (partyConf.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv6)
                    {
                        ret = SendTransaction_overXAP(ongoingConfXml, ref resOngoingConfXml);
                    }
                    else
                    {
                        ret = SendTransaction_overXMLHTTP(partyConf.cMcu, ongoingConfXml, ref resOngoingConfXml);
                    }
                    if (!ret) return false; //error

                    // find the participant in the conference 
                    ret = false;
                    ret = ProcessXML_FindOngoingParty(resOngoingConfXml, ref party);//party.sName,ref partyIdOnMcu);
                    if (!ret) return false; //error

                    logger.Trace("Into Party Message Add Overlay...");
                    // if conf found, add the participant
                    string addPartyXml = null; ret = false;
                    ret = CreateXML_AddMessagetoParty(ref addPartyXml, party, partyConf.stStatus.iMcuID.ToString(), MessageText, sendmsg); //FB 3059
                    if (!ret) return false; //error

                    // send the transaction
                    ret = false;
                    string resAddPartyXml = null;
                    if (partyConf.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv6)
                    {
                        ret = SendTransaction_overXAP(addPartyXml, ref resAddPartyXml);
                    }
                    else
                    {
                        ret = SendTransaction_overXMLHTTP(partyConf.cMcu, addPartyXml, ref resAddPartyXml);
                    }
                    if (!ret) return false; //error


                    // Process the response
                    ret = false;
                    ret = ProcessXML_AddParty(ref resAddPartyXml);
                    if (!ret) return false; //error

                    return true;
                }

                return false;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }

        private bool CreateXML_AddMessagetoParty(ref string addPartyXml, NS_MESSENGER.Party party, string confIdOnMcu, string MessageText, int sendmsg) //FB 3059
        {
            try
            {
                string commonXml = null;
                bool ret = CreateXML_CommonParams(party.cMcu, ref commonXml);
                if (!ret) return false; // error in common params 

                ret = false;
                
                addPartyXml = null;
                addPartyXml = "<TRANS_CONF_2>";
                addPartyXml += commonXml;

                addPartyXml += "<SET_PARTY_MESSAGE_OVERLAY>";
                addPartyXml += "<ID>" + confIdOnMcu + "</ID>";
                addPartyXml += "<PARTY_ID>" + party.iMcuId + "</PARTY_ID>";
                
                addPartyXml += "<MESSAGE_OVERLAY>";
                if (party.bIsMessageOverlay || sendmsg == 1) //FB 3059
                {
                    addPartyXml += "<ON>true</ON>";
                }
                else
                {
                    addPartyXml += "<ON>false</ON>";
                }
                addPartyXml += "<MESSAGE_TEXT>" + MessageText + "</MESSAGE_TEXT>";
                addPartyXml += "<MESSAGE_FONT_SIZE>small</MESSAGE_FONT_SIZE>";
                addPartyXml += "<MESSAGE_FONT_SIZE_INT>12</MESSAGE_FONT_SIZE_INT>";
                addPartyXml += "<MESSAGE_COLOR>white_font_on_light_blue_background</MESSAGE_COLOR>";
                addPartyXml += "<NUM_OF_REPETITIONS>3</NUM_OF_REPETITIONS>";
                addPartyXml += "<MESSAGE_DISPLAY_SPEED>slow</MESSAGE_DISPLAY_SPEED>";
                addPartyXml += "<MESSAGE_DISPLAY_POSITION>bottom</MESSAGE_DISPLAY_POSITION>";
                addPartyXml += "<MESSAGE_DISPLAY_POSITION_INT>90</MESSAGE_DISPLAY_POSITION_INT>";
                addPartyXml += "<MESSAGE_TRANSPARENCE>50</MESSAGE_TRANSPARENCE>";
                addPartyXml += "</MESSAGE_OVERLAY>";
                addPartyXml += "</SET_PARTY_MESSAGE_OVERLAY>";
                addPartyXml += "</TRANS_CONF_2>";

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }

        #endregion
        
        //FB 2486 End
        
		internal bool DeletePartyFromMcu (NS_MESSENGER.Party party,string dbConfName) 
		{
			try
			{
				// create a tmp conf which is for this party only.
				NS_MESSENGER.Conference partyConf = new NS_MESSENGER.Conference();

				// Login to the bridge the party is on.
				bool ret = LoginToBridge(ref party.cMcu);
				if (!ret) 
				{
					logger.Exception (100,"Login to bridge failed. Bridge Name = " + party.cMcu.sName );
					return false; 
				}
				
				// Fetch common parameters.
				ret = false;
				string commonXML = null;
				ret = CreateXML_CommonParams(party.cMcu ,ref commonXML);
				if (!ret || commonXML.Length < 1)	return false;// error
			
				// check if conf is running on the bridge
				// if true, get the conf.bridge_id (bridge-specific id . this is diff from the db-specific conf.id)		
				// build the xml for fetching all ongoing confs
				ret = false;		
				string confXml=null;
				partyConf.cMcu = party.cMcu;
				ret = CreateXML_OngoingConfList(partyConf.cMcu,ref confXml);
				if (!ret) return false; // error
			
				// send the transaction
				ret=false;
				string resXml = null;
				if (partyConf.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv6)
				{
					ret = SendTransaction_overXAP(confXml,ref resXml);
				}
				else
				{
					ret = SendTransaction_overXMLHTTP(partyConf.cMcu,confXml,ref resXml);
				}
				if (!ret) return false; //error
					
				// get the conf.status.bridge_id 
				//process the response				
				ret=false;bool searchResult = false;
				partyConf.sDbName = dbConfName;
				ret= ProcessXML_FindOngoingConf(resXml,ref partyConf,ref searchResult);
				if (!ret) return false; //error
				if (searchResult)
				{
					// find the participant now in the conference 

					//create the xml for ongoing conf
					string ongoingConfXml = null;ret = false;
					ret = CreateXML_OngoingConf(ref ongoingConfXml,partyConf);
					if (!ret) return false; //error

					// send the transaction
					ret= false;
					string resOngoingConfXml = null;
					if (partyConf.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv6)
					{
						ret = SendTransaction_overXAP(ongoingConfXml,ref resOngoingConfXml);
					}
					else
					{
						ret = SendTransaction_overXMLHTTP(partyConf.cMcu,ongoingConfXml,ref resOngoingConfXml);
					}
					if (!ret) return false; //error

					// find the participant in the conference 
					int partyIdOnMcu = 0; ret = false;
                    ret = ProcessXML_FindOngoingParty(resOngoingConfXml, ref party);//party.sName,ref partyIdOnMcu);
					if (!ret) return false; //error

					// participant found successfully.
					string deletePartyXml = null ; ret = false;
					ret = CreateXML_TerminateParty(ref deletePartyXml,party.cMcu,partyConf.stStatus.iMcuID,partyIdOnMcu);
					if (!ret) return false; //error
				
					// send the transaction
					ret= false;
					string resDeletePartyXml = null;
					if (partyConf.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv6)
					{
						ret = SendTransaction_overXAP(deletePartyXml,ref resDeletePartyXml);
					}
					else
					{
						ret = SendTransaction_overXMLHTTP(partyConf.cMcu,deletePartyXml,ref resDeletePartyXml);
					}
					if (!ret) return false; //error

					// Process the response
					ret = false;
					ret = ProcessXML_CommonConfOps(ref resDeletePartyXml);
					if (!ret) return false; //error

					return true;
				}
			
				return false;
			}
			catch (Exception e)
			{
				logger.Exception(100,e.Message);
				return false;
			}
		}

        internal bool OngoingConfOps(string operation, NS_MESSENGER.Conference conf, NS_MESSENGER.Party party, ref string msg, bool newMuteValue, int newLayoutValue, int extendTime, bool connectOrDisconnect, ref NS_MESSENGER.Party.eOngoingStatus terminalStatus, bool videoMute, bool isChairParty) //FB 2553-RMX
        {
            try
            {
                int tryAgainCount = 0;
                bool tryAgain = false;
                bool searchResult = false;
                bool ret = false;
                // create a tmp conf which is for this party only.
                NS_MESSENGER.Conference partyConf = new NS_MESSENGER.Conference();

                do
                {
                    searchResult = false;
                    ret = false;
                    tryAgain = false;

                    // Login to the bridge the party is on.
                    ret = LoginToBridge(ref party.cMcu);
                    if (!ret)
                    {
                        logger.Exception(100, "Login to bridge failed. Bridge Name = " + party.cMcu.sName);
                        return false;
                    }

                    // Fetch common parameters.
                    ret = false;
                    string commonXML = null;
                    ret = CreateXML_CommonParams(party.cMcu, ref commonXML);
                    if (!ret || commonXML.Length < 1) return false;// error

                    // check if conf is running on the bridge
                    // if true, get the conf.bridge_id (bridge-specific id . this is diff from the db-specific conf.id)		
                    // build the xml for fetching all ongoing confs
                    ret = false;
                    string confXml = null;
                    partyConf.cMcu = party.cMcu;
                    if (conf.dtStartDateTimeInUTC < DateTime.UtcNow)
                    {
                        ret = CreateXML_OngoingConfList(partyConf.cMcu, ref confXml);
                        if (!ret) return false; // error
                    }
                    else
                    {
                        ret = CreateXML_ReservationConfList(partyConf.cMcu, ref confXml);
                        if (!ret) return false; // error
                    }

                    // send the transaction
                    ret = false;
                    string resXml = null;
                    if (partyConf.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv6)
                    {
                        ret = SendTransaction_overXAP(confXml, ref resXml);
                    }
                    else
                    {
                        ret = SendTransaction_overXMLHTTP(partyConf.cMcu, confXml, ref resXml);
                    }
                    if (!ret) return false; //error

                    // get the conf.status.bridge_id 
                    //process the response				
                    ret = false;
                    partyConf.sDbName = conf.sDbName;
                    partyConf.iDbID = conf.iDbID; //ZD 100085
                    partyConf.iInstanceID = conf.iInstanceID;

                    if (conf.dtStartDateTimeInUTC < DateTime.UtcNow)
                    {
                        ret = ProcessXML_FindOngoingConf(resXml, ref partyConf, ref searchResult);
                    }
                    else
                    {
                        ret = ProcessXML_FindReservationConf(resXml, ref partyConf, ref searchResult);
                    }

                    if (!ret)
                    {
                        if (tryAgainCount < 1)
                        {
                            // Try again. This is to address the weird issue that Polycom MCU have.
                            // MCU doesnot return the complete conf listing in first attempt.					
                            tryAgainCount++;
                            tryAgain = true;
                        }
                        else
                        {
                            logger.Trace("Conference not found on the MCU.");
                            msg = "Conference not found on the Polycom MGC MCU.";
                            return false;
                        }
                    }
                }
                while (tryAgain == true);

                if (searchResult)
                {
                    // check if the operation is conference-related 
                    string confOpsPartyXml = null;
                    if (operation == "TerminateConference" || operation == "ExtendEndTime" || operation == "ChangeConferenceDisplayLayout")
                    {
                        // Conference-related Operations 

                        switch (operation)
                        {
                            case "TerminateConference":
                                {
                                    ret = false;
                                    ret = CreateXML_TerminateConference(ref confOpsPartyXml, party.cMcu, partyConf.stStatus.iMcuID);
                                    if (!ret) return false; //error	
                                    break;
                                }
                            case "ExtendEndTime":
                                {
                                    ret = false;
                                    ret = CreateXML_ExtendEndTime(ref confOpsPartyXml, party.cMcu, partyConf.stStatus.iMcuID, conf.dtStartDateTime.AddMinutes(conf.iDuration + extendTime));
                                    if (!ret) return false; //error	
                                    break;
                                }
                            case "ChangeConferenceDisplayLayout":
                                {
                                    ret = false;
                                    ret = CreateXML_ChangeDisplayLayoutOfConference(ref confOpsPartyXml, party.cMcu, partyConf.stStatus.iMcuID, newLayoutValue);
                                    if (!ret) return false; //error	
                                    break;
                                }

                            case "SetLectureMode":
                                {
                                    ret = false;
                                    ret = CreateXML_SetLectureMode(ref confOpsPartyXml, party.cMcu, partyConf.stStatus.iMcuID, party.sName, conf.bLectureMode);//FB 2553
                                    if (!ret) return false; //error	
                                    break;
                                }
                        }
                    }
                    else
                    {
                        // Participant-related Operations 

                        // find the participant now in the conference 
                        //create the xml for ongoing conf
                        string ongoingConfXml = null; ret = false;
                        ret = CreateXML_OngoingConf(ref ongoingConfXml, partyConf);
                        if (!ret) return false; //error

                        // send the transaction
                        ret = false;
                        string resOngoingConfXml = null;
                        if (partyConf.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv6)
                        {
                            ret = SendTransaction_overXAP(ongoingConfXml, ref resOngoingConfXml);
                        }
                        else
                        {
                            ret = SendTransaction_overXMLHTTP(partyConf.cMcu, ongoingConfXml, ref resOngoingConfXml);
                        }
                        if (!ret) return false; //error

                        // find the participant in the conference 
                        ret = false; //int partyIdOnMcu = 0; 
                        ret = ProcessXML_FindOngoingParty(resOngoingConfXml, ref party);//party.sName,ref partyIdOnMcu);
                        if (!ret)
                        {
                            //FB 1552
                            logger.Trace("Add endpoint for ongoing.");
                            ret = AddPartyToMcu(conf, party);
                            if (!ret)
                            {
                                msg = "Endpoint cannot be added to the conference on the MCU.";
                                return false; //error
                            }
                            else
                            {
                                ret = ProcessXML_FindOngoingParty(resOngoingConfXml, ref party);//party.sName,ref partyIdOnMcu);
                                if (!ret)
                                {
                                    msg = "Endpoint not found in the conference on the MCU.";
                                    return false; //error
                                }

                            }
                            //FB 1552
                        }
                        

                        // Participant has been found. Now,depending on the operation create the SEND transaction xml.
                        operation = operation.Trim();
                        confOpsPartyXml = null; ret = false;
                        switch (operation)
                        {
                            case "MuteEndpoint":
                                {
                                    // Mute/unmute the endpoint.
                                    ret = CreateXML_MuteParty(ref confOpsPartyXml, party.cMcu, partyConf.stStatus.iMcuID, party.iMcuId, newMuteValue, videoMute);//FB 2553-RMX
                                    if (!ret) return false; //error
                                    break;
                                }
                            case "TerminateEndpoint":
                                {
                                    // Terminate endpoint.
                                    ret = CreateXML_TerminateParty(ref confOpsPartyXml, party.cMcu, partyConf.stStatus.iMcuID, party.iMcuId);
                                    if (!ret) return false; //error
                                    break;
                                }
                            case "ChangePartyDisplayLayout":
                                {
                                    // Change party display layout.

                                    #region First, change the setting on endpoint display from "Conference" mode to "Personal" mode.
                                    string layoutTypePersonalXml = null;
                                    ret = CreateXML_SetDisplayLayoutTypeToPersonal(ref layoutTypePersonalXml, party.cMcu, partyConf.stStatus.iMcuID, party.iMcuId);
                                    if (!ret)
                                    {
                                        logger.Trace("Changing the display layout to Personal mode - Failure in creating xml string.");
                                        return false; //error
                                    }
                                    // send the transaction
                                    ret = false;
                                    string resLayoutTypePersonalXml = null;
                                    if (partyConf.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv6)
                                    {
                                        ret = SendTransaction_overXAP(layoutTypePersonalXml, ref resLayoutTypePersonalXml);
                                    }
                                    else
                                    {
                                        ret = SendTransaction_overXMLHTTP(partyConf.cMcu, layoutTypePersonalXml, ref resLayoutTypePersonalXml);
                                    }
                                    if (!ret)
                                    {
                                        logger.Trace("Changing the display layout to Personal mode - Transaction failed.");
                                        return false; //error
                                    }

                                    // Process the response
                                    ret = false;
                                    ret = ProcessXML_CommonConfOps(ref resLayoutTypePersonalXml);
                                    if (!ret)
                                    {
                                        logger.Trace("Changing the display layout to Personal mode - Invalid response");
                                        return false; //error
                                    }
                                    #endregion

                                    // Now, change the actual display of the endpoint.
                                    ret = CreateXML_ChangeDisplayLayoutOfParty(ref confOpsPartyXml, party.cMcu, partyConf.stStatus.iMcuID, party.iMcuId, newLayoutValue);
                                    if (!ret) return false; //error
                                    break;
                                }
                            case "ConnectDisconnectEndpoint":
                                {
                                    // Connect/Disconnect the endpoint.
                                    ret = CreateXML_ConnectDisconnectParty(ref confOpsPartyXml, party.cMcu, partyConf.stStatus.iMcuID, party.iMcuId, connectOrDisconnect);
                                    if (!ret) return false; //error
                                    break;
                                }
                            case "GetTerminalStatus":
                                {
                                    // participant status has already been fetched.
                                    return true;
                                }
                            case "SetLectureMode"://FB 2553-RMX
                                {
                                    ret = false;
                                    ret = CreateXML_SetLectureMode(ref confOpsPartyXml, party.cMcu, partyConf.stStatus.iMcuID, party.sName, conf.bLectureMode);//FB 2553
                                    if (!ret) return false;
                                    break;
                                }
                            case "SetLeaderParty"://FB 2553-RMX
                                {
                                    ret = false;
                                    ret = CreateXML_SetLeader(ref confOpsPartyXml, party.cMcu, partyConf.stStatus.iMcuID, party.iMcuId, isChairParty);
                                    if (!ret) return false;
                                    break;
                                }
                            case "MuteTransmitTerminal"://FB 2553-RMX
                                {
                                    ret = false;
                                    ret = CreateXML_SetPartyAudio(ref confOpsPartyXml, party.cMcu, partyConf.stStatus.iMcuID, party.iMcuId, newMuteValue);
                                    if (!ret) return false;
                                    break;
                                }
                            case "MuteVideoEndpoint": //FB 2553-RMX
                                {
                                    // Mute/unmute the endpoint.
                                    ret = CreateXML_MuteVideoParty(ref confOpsPartyXml, party.cMcu, partyConf.stStatus.iMcuID, party.iMcuId, newMuteValue, videoMute);//FB 2553-RMX
                                    if (!ret) return false; //error
                                    break;
                                }
                        }
                    }

                    // send the transaction
                    ret = false;
                    string resConfOpsPartyXml = null;
                    if (partyConf.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv6)
                    {
                        ret = SendTransaction_overXAP(confOpsPartyXml, ref resConfOpsPartyXml);
                    }
                    else
                    {
                        ret = SendTransaction_overXMLHTTP(partyConf.cMcu, confOpsPartyXml, ref resConfOpsPartyXml);
                    }
                    if (!ret) return false; //error

                    // Process the response
                    ret = false;
                    ret = ProcessXML_CommonConfOps(ref resConfOpsPartyXml);
                    if (!ret) return false; //error

                    return true;
                }

                return false;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }

		internal bool TestMCUConnection (NS_MESSENGER.MCU mcu)
		{
            //FB 2883 Starts
			try 
			{
                string loginXml = null; bool ret = false;
                ret = CreateXML_Login(ref mcu, ref loginXml);
                if (!ret)
                {
                    logger.Exception(100, "CreateXML failed for bridge .Bridge Name = " + mcu.sName);
                    return false;
                }


                // Login to the bridge.
                string responseXml = null; ret = false;
                if (mcu.etType == NS_MESSENGER.MCU.eType.ACCORDv6)
                {
                    ret = SendTransaction_overXAP(loginXml, ref responseXml);
                }
                else
                {
                    ret = SendTransaction_overXMLHTTP(mcu, loginXml, ref responseXml);
                }

                if (!ret)
                {
                    logger.Exception(100, " SendTransaction failed for bridge .Bridge Name = " + mcu.sName);
                    return false;
                }

                // Parse the response.
                ret = false;
                ret = ProcessXML_Login(ref mcu, responseXml);
                if (!ret)
                {
                    this.errMsg = "Connection to MCU failed.";
                    return false;
                }				
				return true;
			}
            //FB 2883 Ends
			catch (Exception e)
			{
				logger.Exception(100,e.Message);
				return false;
			}			
		}
		

		#region ConfMonitor Methods
		#region Ongoing conf list 
		private bool CreateXML_OngoingConfList(NS_MESSENGER.MCU mcu , ref string ongoingXml)
		{
			try
			{
				// Build the xml for fetching list of ongoing confs
				// To retreive the full list of confs , OBJ_TOKEN = -1
				string commonXml = null;
				bool ret = CreateXML_CommonParams(mcu,ref commonXml);
				if (!ret) return false;//error

				ongoingXml = "<TRANS_CONF_LIST>";
				ongoingXml += commonXml;
				ongoingXml += "<GET_LS><OBJ_TOKEN>-1</OBJ_TOKEN></GET_LS></TRANS_CONF_LIST>";
				return true;
			}
			catch (Exception e)
			{
				logger.Exception (100,e.Message);
				return false;
			}
		}

		private bool CreateXML_ReservationConfList(NS_MESSENGER.MCU mcu , ref string resXml)
		{
			try
			{
				// Build the xml for fetching list of ongoing confs
				// To retreive the full list of confs , OBJ_TOKEN = -1
				string commonXml = null;
				bool ret = CreateXML_CommonParams(mcu,ref commonXml);
				if (!ret) return false;//error

				resXml = "<TRANS_RES_LIST>";
				resXml += commonXml;
				resXml += "<GET_RES_LIST><OBJ_TOKEN>-1</OBJ_TOKEN></GET_RES_LIST></TRANS_RES_LIST>";
				return true;
			}
			catch (Exception e)
			{
				logger.Exception (100,e.Message);
				return false;
			}
		}


		#endregion

		#region Ongoing conf 
		private bool CreateXML_OngoingConf(ref string confXml,NS_MESSENGER.MCU mcu ,NS_MESSENGER.Conference conf)
		{
	
			try 
			{
				// xml for fetching individual conf details. 

				string commonXml = null;
				bool ret = CreateXML_CommonParams(mcu,ref commonXml);
				if (!ret) return false; // error in common params 
	
				confXml = "<TRANS_CONF_2>";
				confXml += commonXml;
				confXml += "<GET>";
				confXml += "<ID>" + conf.stStatus.iMcuID + "</ID>";
				confXml += "<OBJ_TOKEN>-1</OBJ_TOKEN>";
				confXml += "</GET></TRANS_CONF_2>";

				return true;
			}
			catch (Exception e)
			{
				logger.Exception (100,e.Message);
				return false;
			}
		}

		private bool ProcessXML_OngoingConf(string responseXml,ref NS_MESSENGER.Conference conf)
		{
			// process each conf to get the latest status for each participant
			try
			{
				//Load the response xml recd from the bridge and parse it
				XmlDocument xd = new XmlDocument();
				xd.LoadXml(responseXml);
				
				XmlNodeList nodelist;
				XmlNode node;

				// if description = OK , then continue processing
                string description = "";//FB 2553-RMX
				node = xd.SelectSingleNode("//RESPONSE_TRANS_CONF/RETURN_STATUS/DESCRIPTION");
                if (node != null)
                    description = node.InnerXml.Trim();
				if (description.Length < 0 || description.Contains("OK")!=true)
				{
					this.errMsg = description;
					return false; //error
				}
				
				// Retreive all conferences	
				nodelist = xd.SelectNodes("//RESPONSE_TRANS_CONF/GET/CONFERENCE/ONGOING_PARTY_LIST/ONGOING_PARTY");
				
				//cycle through each party one-by-one
				foreach(XmlNode node1 in nodelist)
				{
					// party object 
					NS_MESSENGER.Party party = new NS_MESSENGER.Party();
					XmlNode partyNode = node1.SelectSingleNode("PARTY");
					
					// name , partyID on mcu
					party.sMcuName= partyNode.ChildNodes.Item(0).InnerText; //NAME
					party.iMcuId = Int32.Parse(partyNode.ChildNodes.Item(1).InnerText); //ID
					
					//interface type  - ip or isdn
					//XmlNode interfaceType = node1.SelectSingleNode("INTERFACE");
					//string interfaceTypeVal = interfaceType.InnerText ; 					
					string interfaceTypeVal = partyNode.ChildNodes.Item(3).InnerText;
					interfaceTypeVal = interfaceTypeVal.Trim();
					if (interfaceTypeVal == "h323")
						party.etProtocol = NS_MESSENGER.Party.eProtocol.IP;
					else
						party.etProtocol = NS_MESSENGER.Party.eProtocol.ISDN;
				
					// connection type - dial-in or dial-out 
					//XmlNode connectionType = node1.SelectSingleNode("CONNECTION");
					//string connectionTypeVal = connectionType.InnerText;
					string connectionTypeVal = partyNode.ChildNodes.Item(4).InnerText;
					connectionTypeVal = connectionTypeVal.Trim();
					if (connectionTypeVal == "dial_in")
						party.etConnType = NS_MESSENGER.Party.eConnectionType.DIAL_IN;
					else
						party.etConnType = NS_MESSENGER.Party.eConnectionType.DIAL_OUT;

					// ip or isdn address 
					if (party.etProtocol == NS_MESSENGER.Party.eProtocol.IP)
					{
						//XmlNode ipaddress = node1.SelectSingleNode("IP");						
						//party.m_mcuAddress =ipaddress.InnerText ; 
						party.sMcuAddress = partyNode.SelectSingleNode("IP").InnerText;
					}
					else
					{
						// party is connected on ISDN
						party.etProtocol = NS_MESSENGER.Party.eProtocol.ISDN;
						if (party.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_IN)
						{
							// Dial-In : Fetch the address from mcu phone list 
							//XmlNode isdnaddress = node1.SelectSingleNode("MCU_PHONE_LIST");						
							party.sMcuAddress = partyNode.SelectSingleNode("MCU_PHONE_LIST").SelectSingleNode("PHONE1").InnerText;				
						}
						else
						{
							// Dial-Out : Fetch the address from the participant phone list
							//XmlNode isdnaddress = node1.SelectSingleNode("");						
							party.sMcuAddress = partyNode.SelectSingleNode("BONDING_PHONE").InnerText;						
						}
					}

					// party ongoing status
					XmlNode partyStatus = node1.SelectSingleNode("ONGOING_PARTY_STATUS");
					string ongoingPartyStatus = partyStatus.ChildNodes.Item(1).InnerText ; //description
					ongoingPartyStatus = ongoingPartyStatus.Trim();
                    if (ongoingPartyStatus.Trim().ToLower().CompareTo("connected") == 0)
					{
						party.etStatus = NS_MESSENGER.Party.eOngoingStatus.FULL_CONNECT;
					}
					else
					{
                        if (ongoingPartyStatus.Trim().ToLower().CompareTo("disconnected") == 0) //FB 2989
                        {
                            party.etStatus = NS_MESSENGER.Party.eOngoingStatus.DIS_CONNECT;
                        }
                        else
                        {
                            party.etStatus = NS_MESSENGER.Party.eOngoingStatus.PARTIAL_CONNECT;
                        }
					}
					logger.Trace("Ongoing party status : " + party.etStatus.ToString());

					//Audio Mute status 
					XmlNode partyAudioMute = node1.SelectSingleNode("AUDIO_MUTE");
					string audioMute = partyAudioMute.InnerXml.Trim();
					if ( audioMute == "true")
					{
						party.bMute = true;
					}
					else
					{
						party.bMute = false;
					}

					// individual party details added to the resp conf
					conf.qParties.Enqueue(party);
				}
			}
			catch(Exception e)
			{
				logger.Exception(100,e.Message);
				return false;
			}
			return true;
		}
		#endregion

        private bool CreateXML_ConnectDisconnectParty(ref string connectionXML, NS_MESSENGER.MCU mcu, int confIdOnMcu, int partyIdOnMcu, bool isConnect)
        {
            try
            {
                string commonXml = null;
                bool ret = CreateXML_CommonParams(mcu, ref commonXml);
                if (!ret) return false;

                connectionXML = "<TRANS_CONF_2>";
                connectionXML += commonXml;
                connectionXML += "<SET_CONNECT>";
                connectionXML += "<ID>" + confIdOnMcu.ToString() + "</ID>";
                if (isConnect)
                {
                    connectionXML += "<CONNECT>true</CONNECT>";
                }
                else
                {
                    connectionXML += "<CONNECT>false</CONNECT>";
                }
                connectionXML += "<PARTY_ID>" + partyIdOnMcu.ToString() + "</PARTY_ID>";
                connectionXML += "</SET_CONNECT>";
                connectionXML += "</TRANS_CONF_2>";

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
      
		#region Terminate conf	
		private bool CreateXML_TerminateConf(ref string terminateXml,NS_MESSENGER.Conference conf)
		{
			try 
			{
				string commonXml = null;
				bool ret = CreateXML_CommonParams(conf.cMcu,ref commonXml);
				if (!ret) return false; 
	
				terminateXml = "<TRANS_CONF_2>";
				terminateXml += commonXml;
				terminateXml += "<TERMINATE_CONF>";
				terminateXml += "<ID>" + conf.stStatus.iMcuID + "</ID>";
				terminateXml += "</TERMINATE_CONF></TRANS_CONF_2>";

				return true;
			}			
			catch (Exception e)
			{
				logger.Exception (100,e.Message);
				return false;
			}
		}
		
		private bool ProcessXML_TerminateConf (string responseXml)
		{
			try
			{
				XmlDocument res = new XmlDocument();
				res.LoadXml(responseXml);

				XmlNode node;

				node = res.SelectSingleNode("//RESPONSE_TRANS_CONF/RETURN_STATUS/DESCRIPTION");
				if (node != null)
				{
					string msg = node.InnerText;
					msg = msg.Trim();
                    if (msg.CompareTo("IN_PROGRESS") == 0 || msg.CompareTo("In progress") == 0)
					{
						// conf got terminated. correct confirmation msg recd.
						return true; 
					}
					else 
					{
						// error encountered
						this.errMsg = msg;
						return false;
					}
				}
			}
			catch(Exception e)
			{
				logger.Exception(100,e.Message);
			}
			
			// either correct confirmation not recd or conf doesnot exist on the bridge anymore.		
			return false; 
		}
		#endregion

		internal bool FetchAllOngoingConfsStatus(NS_MESSENGER.MCU mcu , ref Queue confq)
		{
		
			try
			{
				// Login to the bridge
				bool ret = LoginToBridge(ref mcu);
				if (!ret) return false; // quit if error returned.

				// Fetch common parameters.
				ret = false;
				string commonXML = null;
				ret = CreateXML_CommonParams(mcu ,ref commonXML);
				if (!ret || commonXML.Length < 1)	return false;// error
			
				// build the xml for fetching all ongoing confs
				ret = false;		
				string confXml=null;
				ret = CreateXML_OngoingConfList(mcu,ref confXml);
				if (!ret) return false; // error

				// send the transaction
				ret=false;
				string resXml = null;
				if (mcu.etType == NS_MESSENGER.MCU.eType.ACCORDv6)
				{
					ret = SendTransaction_overXAP(confXml,ref resXml);
				}
				else
				{
					ret = SendTransaction_overXMLHTTP(mcu,confXml,ref resXml);
				}
				if (!ret) return false; //error
		
				// process the response				
				Queue partialq = new Queue();
				ret=false;
				ret= ProcessXML_OngoingConfList(resXml,ref partialq);
				if (!ret) return false; //error
				while (partialq.Count > 0)
				{
					NS_MESSENGER.Conference conf = new NS_MESSENGER.Conference();
					conf = (NS_MESSENGER.Conference)partialq.Dequeue();

					// build the xml for fetching single conf details
					ret = false;		
					string confOngoingXml=null;
					ret = CreateXML_OngoingConf(ref confOngoingXml,mcu,conf);
					if (!ret) continue;

					// send the transaction
					ret= false;
					string resOngoingXml = null;
					if (mcu.etType == NS_MESSENGER.MCU.eType.ACCORDv6)
					{
						ret = SendTransaction_overXAP(confOngoingXml,ref resOngoingXml);
					}
					else
					{
						ret = SendTransaction_overXMLHTTP(mcu,confOngoingXml,ref resOngoingXml);
					}
					if (!ret) continue;

					// process the xml and retrieve each party
					ret= false;
					ret = ProcessXML_OngoingConf(resOngoingXml,ref conf);
					if (!ret) continue;

					// add the conf to the final confq 
					confq.Enqueue(conf);
				}
				return true;
			}
			catch (Exception e)
			{
				logger.Exception (100,e.Message);
				return false;
			}
		}
		#endregion
		
		internal bool CreateXML_GetMcuTime(NS_MESSENGER.MCU mcu,ref string getTimeXml)
		{
			try 
			{
				string commonXml = null;
				bool ret = CreateXML_CommonParams(mcu,ref commonXml);
				if (!ret) return false; 
	
				getTimeXml = "<TRANS_MCU>";
				getTimeXml += commonXml;
				getTimeXml += "<ACTION><GET_TIME><MCU_TIME>";
				getTimeXml += "<MCU_BASE_TIME>" + DateTime.UtcNow.ToString("s") + "</MCU_BASE_TIME>";				
				if (mcu.iTimezoneID < 0)
				{
					getTimeXml += "<GMT_OFFSET_SIGN>true</GMT_OFFSET_SIGN>";
				}
				else
				{
					getTimeXml += "<GMT_OFFSET_SIGN>false</GMT_OFFSET_SIGN>";
				}
		
				getTimeXml += "<GMT_OFFSET>" +  mcu.iTimezoneID.ToString() + "</GMT_OFFSET>";
				getTimeXml += "</MCU_TIME></GET_TIME></ACTION></TRANS_MCU>";

				return true;
			}			
			catch (Exception e)
			{
				logger.Exception(100,e.Message);
				return false;
			}
		}
		
        internal bool CreateXML_SetMcuTime(NS_MESSENGER.MCU mcu,ref string setTimeXml)
		{
			try 
			{
				string commonXml = null;
				bool ret = CreateXML_CommonParams(mcu,ref commonXml);
				if (!ret) return false; 
	
				setTimeXml = "<TRANS_MCU>";
				setTimeXml += commonXml;
				setTimeXml += "<ACTION><SET_TIME><MCU_TIME>";
				setTimeXml += "<MCU_BASE_TIME>" + DateTime.UtcNow.ToString("s") + "</MCU_BASE_TIME>";				
				if (mcu.iTimezoneID < 0)
				{
					setTimeXml += "<GMT_OFFSET_SIGN>true</GMT_OFFSET_SIGN>";
				}
				else
				{
					setTimeXml += "<GMT_OFFSET_SIGN>false</GMT_OFFSET_SIGN>";
				}
		
				setTimeXml += "<GMT_OFFSET>" +  mcu.iTimezoneID.ToString() + "</GMT_OFFSET>";
				setTimeXml += "</MCU_TIME></SET_TIME></ACTION></TRANS_MCU>";

				return true;
			}			
			catch (Exception e)
			{
				logger.Exception(100,e.Message);
				return false;
			}
		}
		
        internal bool CheckMcuTime(NS_MESSENGER.MCU mcu)
		{
			try 
			{				
				// Login to the bridge the party is on.
				bool ret = LoginToBridge(ref mcu);
				if (!ret) 
				{
					this.errMsg = "Connection to MCU failed. Please recheck the MCU information.";
					return false; 
				}				

				// Fetch common parameters.
				ret = false;
				string commonXML = null;
				ret = CreateXML_CommonParams(mcu ,ref commonXML);
				if (!ret || commonXML.Length < 1)	return false;// error
			
				// create xml to fetch mcu time
				ret = false;		
				string getTimeXml=null;
				ret = CreateXML_GetMcuTime(mcu,ref getTimeXml);
				if (!ret) return false; // error
			
				// send the transaction
				ret=false;
				string resXml = null;
				if (mcu.etType == NS_MESSENGER.MCU.eType.ACCORDv6)
				{
					ret = SendTransaction_overXAP(getTimeXml,ref resXml);
				}
				else
				{
					ret = SendTransaction_overXMLHTTP(mcu,getTimeXml,ref resXml);
				}
				if (!ret) return false; //error
									
				//process the response				
				ret=false;
				//partyConf.sDbName = conf.sDbName;
				//ret= ProcessXML_FindOngoingConf(resXml,ref partyConf,ref searchResult);

				// fetch mcu time


				// compare mcu time with vrm time

				// if off by 30 secs send new time to hardware clock

				return true;
			}
			catch (Exception e)
			{
				logger.Exception(100,e.Message);
				return false;
			}			

			
			//return true;
		}
		
		private bool isMcuOutputErroneous (string mcuOutput)
		{
			try
			{
				if (mcuOutput.IndexOf("STATUS_FAIL_TO_LOCATE_USER") > 0)
				{
					logger.Trace ("Mcu error - Tokens are invalid.");
					return true;
				}
			}
			catch (Exception e)
			{
				logger.Exception (100,e.Message);
			}			
			return false;			
		}
		
        private bool RefreshMcuTokens(NS_MESSENGER.MCU mcu , ref string transXML)
		{
			try
			{
				logger.Trace ("Refresh Mcu tokens...");
					
				// Clean out all MCU tokens
				NS_DATABASE.Database db = new NS_DATABASE.Database(this.configParams);
				bool ret = db.DeleteMcuTokens();//FB 2261
				if (!ret)
				{
					logger.Trace ("Error deleting tokens.");
					return false;
				}
				
				// Login to MCU and get new tokens
				ret = false;
				ret = LoginToBridge(ref mcu);
				if (!ret)
				{
					logger.Trace ("Error logging on to bridge.");
					return false;
				}

				// Insert the new tokens in the transaction xml
				// Replace the mcu token
				int startMcuToken = transXML.IndexOf("<MCU_TOKEN>");
				startMcuToken = startMcuToken + 11;				
				int endMcuToken = transXML.IndexOf("</MCU_TOKEN>");
				string oldMcuToken = transXML.Substring(startMcuToken,(endMcuToken-startMcuToken));
				logger.Trace ("Old Mcu Token = " + oldMcuToken);
				transXML = transXML.Replace(oldMcuToken,mcu.sToken);
				
				// Replace the mcu user token
				int startMcuUserToken = transXML.IndexOf("<MCU_USER_TOKEN>");
				startMcuUserToken = startMcuUserToken + 16;				
				int endMcuUserToken = transXML.IndexOf("</MCU_USER_TOKEN>");
				string oldMcuUserToken = transXML.Substring(startMcuUserToken,(endMcuUserToken-startMcuUserToken));
				logger.Trace ("Old Mcu User Token = " + oldMcuUserToken);
				transXML = transXML.Replace(oldMcuUserToken,mcu.sUserToken);
				
				logger.Trace ("Trans XML = " + transXML);

				return true;
			}
			catch (Exception e)
			{
				logger.Exception(100,e.Message);
				return false;
			}
        }

        //FB 2448 start
        #region FetchMCUDetails
        /// <summary>
        /// FetchMCUDetails
        /// </summary>
        /// <param name="mcu"></param>
        /// <returns></returns>
        internal bool FetchMCUDetails(NS_MESSENGER.MCU mcu)
        {
            logger.Trace("Entering FetchMCUDetails function...");
            try
            {
                string commonXML = null, SendXML = null, responseXml = null;
                NS_DATABASE.Database db = null;
                bool ret = false;
                    
                ret = LoginToBridge(ref mcu);
                if (!ret)
                {
                    logger.Exception(100, "Login to bridge failed. Bridge Name = " + mcu.sName);
                    return false;
                }

                // Step 1: Fetch common parameters.
                ret = false;
                ret = CreateXML_CommonParams(mcu, ref commonXML);
                if (!ret || commonXML.Length < 1)
                {
                    logger.Exception(100, "Creation of CreateXML_CommonParams failed . commonXML = " + commonXML);
                    return false;
                }

                // Step 2: Create the Send xml string with data
                ret = false;
                ret = CreateXML_GetMCUTime(ref mcu, ref SendXML);
                if (!ret || SendXML.Length < 1)
                {
                    logger.Exception(100, "Creation of CreateXML_GetMCUTime failed . commonXML = " + SendXML);
                    return false;
                }

                //Send the xml to the bridge
                ret = false;
                if (mcu.etType == NS_MESSENGER.MCU.eType.ACCORDv6)
                    ret = SendTransaction_overXAP(SendXML, ref responseXml);
                else
                    ret = SendTransaction_overXMLHTTP(mcu, SendXML, ref responseXml);

                if (!ret)
                {
                    logger.Exception(100, "SendTransaction failed for bridge .Bridge Name = " + mcu.sName);
                    return false;
                }

                // Parse the response.
                ret = false;
                ret = ProcessXML_ResponseMCU(responseXml);
                if (!ret)
                {
                    logger.Exception(100, "ProcessXML_ResponseMCU failed . responseXml = " + responseXml);
                    return false;
                }

                if (mcuCurrentTime != DateTime.MinValue)
                {
                    db = new NS_DATABASE.Database(configParams);
                    db.UpdateMcuCurrentTime(mcu.iDbId, mcuCurrentTime);
                }
                else
                    logger.Trace("MCU Current Time not successfully updated");

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion

        #region CreateXML_GetMCUTime
        /// <summary>
        /// CreateXML_GetMCUTime
        /// </summary>
        /// <param name="bridge"></param>
        /// <param name="loginXml"></param>
        /// <returns></returns>
        private bool CreateXML_GetMCUTime(ref NS_MESSENGER.MCU bridge, ref string SendXml)
        {
            try
            {
                string commonXML = null;
                bool ret = false;

                if (bridge.sIp.Length < 1 || bridge.sLogin.Length < 1 || bridge.sPwd.Length < 1)
                {
                    logger.Exception(100, "Empty bridge fields");
                    return false;
                }

                ret = CreateXML_CommonParams(bridge, ref commonXML);
                if (!ret || commonXML.Length < 1)
                    return false;// error

                SendXml = "<TRANS_MCU>"
                            + commonXML
                            + " <ACTION>"
                            + "  <GET_TIME>"
                            + "   <MCU_IP>"
                            + "    <IP>" + bridge.sIp + "</IP>"
                            + "   </MCU_IP>"
                            + "   <USER_NAME>" + bridge.sLogin + "</USER_NAME>"
                            + "   <PASSWORD>" + bridge.sPwd + "</PASSWORD>"
                            + "   <STATION_NAME>" + bridge.sStation + "</STATION_NAME>"
                            + "  </GET_TIME>"
                            + " </ACTION>"
                            + "</TRANS_MCU>";

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion

        #region ProcessXML_ResponseMCU
        /// <summary>
        /// ProcessXML_ResponseMCU
        /// </summary>
        /// <param name="responseXML"></param>
        /// <returns></returns>
        private bool ProcessXML_ResponseMCU(string responseXML)
        {
            try
            {
                string description = null;
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(responseXML);

                if (xd.SelectSingleNode("//RESPONSE_TRANS_MCU/RETURN_STATUS/DESCRIPTION") != null)
                {
                    description = xd.SelectSingleNode("//RESPONSE_TRANS_MCU/RETURN_STATUS/DESCRIPTION").InnerText.Trim().ToUpper();

                    if ((description.Contains("OK") == true) || (description.Contains("WARNING") == true))
                    {
                        if (xd.SelectSingleNode("//RESPONSE_TRANS_MCU/ACTION/GET_TIME/MCU_TIME/MCU_BASE_TIME") != null)
                            if (xd.SelectSingleNode("//RESPONSE_TRANS_MCU/ACTION/GET_TIME/MCU_TIME/MCU_BASE_TIME").InnerText.Trim() != "")
                                DateTime.TryParse(xd.SelectSingleNode("//RESPONSE_TRANS_MCU/ACTION/GET_TIME/MCU_TIME/MCU_BASE_TIME").InnerText.Trim(), out mcuCurrentTime);

                        return true;	
                    }
                    else
                        this.errMsg = description; // error message from the MCU
                }

                if (description == null)
                {
                    XElement elem = XElement.Parse(responseXML);
                    description = elem.Element("RETURN_STATUS").Element("DESCRIPTION").Value;
                    String McuBaseTime = "";
                    if ((description.Contains("OK") == true) || (description.Contains("WARNING") == true))
                    {
                        McuBaseTime = elem.Element("ACTION").Element("GET_TIME").Element("MCU_TIME").Element("MCU_BASE_TIME").Value;
                        if (McuBaseTime != "")
                            DateTime.TryParse(McuBaseTime, out mcuCurrentTime);
                        
                        return true;
                    }
                    else
                        this.errMsg = description; // error message from the MCU
                }
                
                logger.Exception(100, "ResponseMCU failed. ResponseXML = " + description);
                return false;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion
        //FB 2448 end

        //FB 2591
        #region FetchMCUProfileDetails
        /// <summary>
        /// FetchMCUProfileDetails
        /// </summary>
        /// <param name="mcu"></param>
        /// <returns></returns>
        internal bool FetchMCUProfileDetails(NS_MESSENGER.MCU mcu)
        {
            logger.Trace("Entering FetchMCUProfileDetails function...");
            try
            {
                string SendXML = null, responseXml = null;
                bool ret = false;
                    
                ret = LoginToBridge(ref mcu);
                if (!ret)
                {
                    logger.Exception(100, "Login to bridge failed. Bridge Name = " + mcu.sName);
                    return false;
                }

                // Step 2: Create the Send xml string with data
                ret = false;
                ret = CreateXML_GetMCUProfile(ref mcu, ref SendXML);
                if (!ret || SendXML.Length < 1)
                {
                    logger.Exception(100, "Creation of CreateXML_GetMCUTime failed . commonXML = " + SendXML);
                    return false;
                }

                //Send the xml to the bridge
                ret = false;
                if (mcu.etType == NS_MESSENGER.MCU.eType.ACCORDv6)
                    ret = SendTransaction_overXAP(SendXML, ref responseXml);
                else
                    ret = SendTransaction_overXMLHTTP(mcu, SendXML, ref responseXml);

                if (!ret)
                {
                    logger.Exception(100, "SendTransaction failed for bridge .Bridge Name = " + mcu.sName);
                    return false;
                }

                // Parse the response.
                ret = false;
                ret = ProcessXML_ResponseMCUProfilelist(mcu,responseXml);
                if (!ret)
                {
                    logger.Exception(100, "ProcessXML_ResponseMCU failed . responseXml = " + responseXml);
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion

        #region CreateXML_GetMCUProfile
        /// <summary>
        /// CreateXML_GetMCUProfile
        /// </summary>
        /// <param name="bridge"></param>
        /// <param name="loginXml"></param>
        /// <returns></returns>
        private bool CreateXML_GetMCUProfile(ref NS_MESSENGER.MCU bridge, ref string SendXml)
        {
            try
            {
                // Build the xml for fetching list of ongoing confs
                // To retreive the full list of confs , OBJ_TOKEN = -1
                string commonXml = null;
                bool ret = CreateXML_CommonParams(bridge, ref commonXml);
                if (!ret) return false;//error

                SendXml = "<TRANS_RES_LIST>";
                SendXml += commonXml;
                SendXml += "<ACTION><GET_PROFILE_LIST><OBJ_TOKEN>-1</OBJ_TOKEN></GET_PROFILE_LIST>";
                SendXml += "</ACTION></TRANS_RES_LIST>";
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion

        #region ProcessXML_ResponseMCUProfilelist
        /// <summary>
        /// ProcessXML_ResponseMCU
        /// </summary>
        /// <param name="responseXML"></param>
        /// <returns></returns>
        private bool ProcessXML_ResponseMCUProfilelist(NS_MESSENGER.MCU mcu, string responseXML)
        {
            try
            {
                string description = null;
                XmlDocument xd = new XmlDocument();
                XmlNodeList nodelist = null;
                NS_DATABASE.Database db = null;
                xd.LoadXml(responseXML);
                List<NS_MESSENGER.MCUProfile> MCUProfilelist = new List<NS_MESSENGER.MCUProfile> ();
                NS_MESSENGER.MCUProfile MCUProfile = null;
                if (xd.SelectSingleNode("//RESPONSE_TRANS_RES_LIST/RETURN_STATUS/DESCRIPTION") != null)
                {
                    description = xd.SelectSingleNode("//RESPONSE_TRANS_RES_LIST/RETURN_STATUS/DESCRIPTION").InnerText.Trim().ToUpper();

                    if ((description.Contains("OK") == true) || (description.Contains("WARNING") == true))
                    {
                        nodelist = xd.SelectNodes("//RESPONSE_TRANS_RES_LIST/ACTION/GET_PROFILE_LIST/PROFILE_SUMMARY_LS/PROFILE_SUMMARY");
                        for (int i = 0; i < nodelist.Count; i++)
                        {
                            MCUProfile = new NS_MESSENGER.MCUProfile();
                            if (nodelist[i].SelectSingleNode("ID") != null)
                                Int32.TryParse(nodelist[i].SelectSingleNode("ID").InnerText.Trim(), out MCUProfile.iId);

                            if (nodelist[i].SelectSingleNode("NAME") != null)
                                MCUProfile.sName = nodelist[i].SelectSingleNode("NAME").InnerText.Trim();

                            if (nodelist[i].SelectSingleNode("DISPLAY_NAME") != null)
                                MCUProfile.sDisplayname = nodelist[i].SelectSingleNode("DISPLAY_NAME").InnerText.Trim();

                            MCUProfilelist.Add(MCUProfile);
                        }
                        db = new NS_DATABASE.Database(configParams);
                        db.UpdateMcuProfile(mcu.iDbId, MCUProfilelist);
                        return true;
                    }
                    else
                        this.errMsg = description; // error message from the MCU
                }
                logger.Exception(100, "ResponseMCU failed. ResponseXML = " + description);
                return false;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion

		//FB 2553-RMX Starts
        #region CreateXML_SetLeader
        /// <summary>
        /// CreateXML_SetLecture
        /// </summary>
        /// <param name="lectureXml"></param>
        /// <param name="mcu"></param>
        /// <param name="confIdOnMcu"></param>
        /// <param name="partyIdOnMcu"></param>
        /// <param name="isLeader"></param>
        /// <returns></returns>
        private bool CreateXML_SetLeader(ref string lectureXml, NS_MESSENGER.MCU mcu, int confIdOnMcu, int partyIdOnMcu, bool isLeader)
        {
            try
            {
                string commonXml = null;
                bool ret = CreateXML_CommonParams(mcu, ref commonXml);
                if (!ret) return false; // error in common params                 

                lectureXml = null;
                lectureXml = "<TRANS_CONF_2>";
                lectureXml += commonXml;
                lectureXml += "<SET_LEADER>";
                lectureXml += "<ID>" + confIdOnMcu.ToString() + "</ID>";
                lectureXml += "<PARTY_ID>" + partyIdOnMcu.ToString() + "</PARTY_ID>";

                if (isLeader)
                    lectureXml += "<LEADER>true</LEADER>";
                else
                    lectureXml += "<LEADER>false</LEADER>";

                lectureXml += "</SET_LEADER>";
                lectureXml += "</TRANS_CONF_2>";

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion

        # region CreateXML_SetPartyAudio
        /// <summary>
        /// CreateXML_SetPartyAudio
        /// </summary>
        /// <param name="partyAudio"></param>
        /// <param name="mcu"></param>
        /// <param name="confIdOnMcu"></param>
        /// <param name="partyIdOnMcu"></param>
        /// <param name="isPartyAudio"></param>
        /// <returns></returns>
        private bool CreateXML_SetPartyAudio(ref string partyAudio, NS_MESSENGER.MCU mcu, int confIdOnMcu, int partyIdOnMcu, bool isPartyAudio)
        {
            try
            {
                string commonXml = null;
                bool ret = CreateXML_CommonParams(mcu, ref commonXml);
                if (!ret) return false; // error in common params                 

                partyAudio = null;
                partyAudio = "<TRANS_CONF_2>";
                partyAudio += commonXml;
                partyAudio += "<SET_AUDIO_BLOCK>";
                partyAudio += "<ID>" + confIdOnMcu.ToString() + "</ID>";
                partyAudio += "<PARTY_ID>" + partyIdOnMcu.ToString() + "</PARTY_ID>";

                if (isPartyAudio)
                    partyAudio += "<AUDIO_BLOCK>true</AUDIO_BLOCK>";
                else
                    partyAudio += "<AUDIO_BLOCK>false</AUDIO_BLOCK>";

                partyAudio += "</SET_AUDIO_BLOCK>";
                partyAudio += "</TRANS_CONF_2>";

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        # endregion
        //FB 2553-RMX Ends

		 //FB 2683 Starts

        #region PolycomRMXCDR
        /// <summary>
        /// Process to Get the PolycomRMX CDR
        /// </summary>
        /// <param name="MCU"></param>
        /// <param name="responseXML"></param>
        /// <returns></returns>
        internal bool PolycomRMXCDR(NS_MESSENGER.MCU MCU,string confuidonmcu, ref string responseXML)
        {
            logger.Trace("Entering PolycomRMXCDR function...");
            try
            {
                string SendXML = null;
                bool ret = false;

                ret = LoginToBridge(ref MCU);
                if (!ret)
                {
                    logger.Exception(100, "Login to bridge failed. Bridge Name = " + MCU.sName);
                    return false;
                }
                ret = false;
                ret = CreateXML_PolycomRMXCDR(MCU,confuidonmcu, ref SendXML);
                if (!ret || SendXML.Length < 1)
                {
                    logger.Exception(100, "Creation of CreateXML_GetMCUTime failed . commonXML = " + SendXML);
                    return false;
                }
                ret = false;
                ret = SendTransaction_overXMLHTTP(MCU, SendXML, ref responseXML);

                return true;
            }
            catch (Exception ex)
            {

                logger.Exception(100, ex.Message);
                return false;
            }
        }
        #endregion

        #region CreateXML_PolycomCDR
        /// <summary>
        /// Generate XML to Get Polycom RMX CDR
        /// </summary>
        /// <param name="MCU"></param>
        /// <param name="SendXML"></param>
        /// <returns></returns>
        private bool CreateXML_PolycomRMXCDR(NS_MESSENGER.MCU MCU,string confuidonmcu, ref string SendXML)
        {
            try
            {
                SendXML = "<TRANS_CDR_FULL>";
                SendXML = SendXML + "<TRANS_COMMON_PARAMS>";
                SendXML = SendXML + "<MCU_TOKEN>" + MCU.sToken + "</MCU_TOKEN>";
                SendXML = SendXML + "<MCU_USER_TOKEN>" + MCU.sUserToken + "</MCU_USER_TOKEN>";
                SendXML = SendXML + "</TRANS_COMMON_PARAMS>";
                SendXML = SendXML + "<ACTION>";
                SendXML = SendXML + "<GET>";
                SendXML = SendXML + "<ID>" + confuidonmcu + "</ID>"; // Code entered to fetch RMX CDR based on RMX CONFGUID
                SendXML = SendXML + "</GET>";
                SendXML = SendXML + "</ACTION>";
                SendXML = SendXML + "</TRANS_CDR_FULL>";


                return true;

            }
            catch (Exception ex)
            {
                logger.Exception(100, ex.Message);
                return false;
            }
        }
        #endregion

        //FB 2683 Ends

        //ZD #100093 & ZD #100085 starts
        #region ProcessXML_ResponseReservationConferences
        /// <summary>
        /// ProcessXML_ResponseReservationConferences
        /// </summary>
        /// <param name="responseXML"></param>
        /// <returns></returns>
        private bool ProcessXML_ResponseReservationConferences(NS_MESSENGER.Conference conf, string responseXML)
        {
            try
            {
                string description = null;
                XmlDocument xd = new XmlDocument();
                XmlNodeList nodelist = null;
                NS_DATABASE.Database db = null;
                xd.LoadXml(responseXML);

                int MCUConfId = 0;
                string ConfName = "", ConfMCUName = "";

                if (xd.SelectSingleNode("//RESPONSE_TRANS_RES_LIST/RETURN_STATUS/DESCRIPTION") != null)
                {
                    description = xd.SelectSingleNode("//RESPONSE_TRANS_RES_LIST/RETURN_STATUS/DESCRIPTION").InnerText.Trim().ToUpper();

                    if ((description.Contains("OK") == true) || (description.Contains("WARNING") == true))
                    {
                        db = new NS_DATABASE.Database(configParams);

                        nodelist = xd.SelectNodes("//RESPONSE_TRANS_RES_LIST/GET_RES_LIST/RES_SUMMARY_LS/RES_SUMMARY");

                        for (int i = 0; i < nodelist.Count; i++)
                        {
                            if (nodelist[i].SelectSingleNode("ID") != null)
                                int.TryParse(nodelist[i].SelectSingleNode("ID").InnerText.Trim(), out MCUConfId);

                            if (nodelist[i].SelectSingleNode("NAME") != null)
                                ConfName = nodelist[i].SelectSingleNode("NAME").InnerText.Trim();

                            if (nodelist[i].SelectSingleNode("DISPLAY_NAME") != null)
                                ConfMCUName = nodelist[i].SelectSingleNode("DISPLAY_NAME").InnerText.Trim();

                            conf.etStatus = NS_MESSENGER.Conference.eStatus.OnMCU;
                            if(conf.iDbNumName.ToString() == ConfName)
                                db.UpdateConferenceStatus(conf.iDbID, conf.iInstanceID, conf.etStatus);
                        }
                        return true;
                    }
                    else
                        this.errMsg = description; // error message from the MCU
                }
                logger.Exception(100, "ResponseMCU failed. ResponseXML = " + description);
                return false;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion
        //ZD #100093 & ZD #100085 Ends

        //ZD 100369_MCU Start

        internal bool FetchMCUStatus(NS_MESSENGER.MCU mcu, int Timeout)
        {
            try
            {
                string loginXml = null, responseXml = null; 
                bool ret = false;
                int pollstatus = (int)NS_MESSENGER.MCU.PollState.PASS;

                ret = CreateXML_Login(ref mcu, ref loginXml);
                if (!ret)
                {
                    logger.Exception(100, "CreateXML failed for bridge .Bridge Name = " + mcu.sName);
                    //return false; //ZD 100369-MCU Fail Over
                }

                if (ret)
                {
                    ret = false;
                    if (mcu.etType == NS_MESSENGER.MCU.eType.ACCORDv6)
                    {
                        ret = SendTransaction_overXAP(loginXml, ref responseXml);
                    }
                    else
                    {
                        ret = SendMCUStatusCommand_overXMLHTTP(mcu, loginXml, ref responseXml, Timeout);
                    }
                }
                if (!ret)
                {
                    logger.Exception(100, " SendTransaction failed for bridge .Bridge Name = " + mcu.sName);
                    pollstatus = (int)NS_MESSENGER.MCU.PollState.FAIL;
                }
                else if (string.IsNullOrEmpty(responseXml))
                {
                    pollstatus = (int)NS_MESSENGER.MCU.PollState.FAIL;
                }
                else
                {
                    XmlDocument xd = new XmlDocument();
                    xd.LoadXml(responseXml);
                    string description = xd.GetElementsByTagName("DESCRIPTION").Item(0).InnerText.Trim();
                    if (description.Contains("OK") != true)
                    {
                        this.errMsg = description;
                        logger.Exception(100, "Login to MCU failed. ResponseXML = " + responseXml);
                        pollstatus = (int)NS_MESSENGER.MCU.PollState.FAIL;
                    }
                }
                db.UpdateMcuStatus(mcu, pollstatus);
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        //ZD 100369_MCU End

        // 104003 start
        #region UpdateConfPassword
        /// <summary>
        /// UpdateConfPassword
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="outxml"></param>
        /// <returns></returns>
        internal bool UpdateConfPassword(NS_MESSENGER.Conference conf, ref string outxml) /////ZD 
        {
            logger.Trace("Entering SetConference function...");

            try
            {
                // Setup conferences on the bridge

                // Login to the bridge
                bool ret = LoginToBridge(ref conf.cMcu);
                if (!ret)
                {
                    logger.Exception(100, "Login to bridge failed. Bridge Name = " + conf.cMcu.sName);
                    return false;
                }

                // Step 1: Fetch common parameters.
                ret = false;
                string commonXML = null;
                ret = CreateXML_CommonParams(conf.cMcu, ref commonXML);
                if (!ret || commonXML.Length < 1)
                {
                    logger.Exception(100, "Creation of CreateXML_CommonParams failed . commonXML = " + commonXML);
                    return false;
                }

                // Step 2: Create the reservation xml string with data
                string updateXml = null; ret = false;

                ret = UpdateConferencePassword(conf, ref updateXml);
                //Send the xml to the bridge
                ret = false;
                string responseXml = null;
                if (conf.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv6)
                {
                    logger.Trace("MGC - Accordv6");
                    ret = SendTransaction_overXAP(updateXml, ref responseXml);
                }
                else
                {
                    logger.Trace("All polycom mcu except MGC - Accordv6");
                    ret = SendTransaction_overXMLHTTP(conf.cMcu, updateXml, ref responseXml);
                }
                if (!ret)
                {
                    logger.Exception(100, "SendTransaction failed . confXml = " + updateXml);
                    return false;
                }

                //ret = ProcessXML_CommonConfOps(ref responseXml);

                ret = ProcessXML_Reservation(responseXml, conf, ref outxml);// FB 2683 //ZD 100522
                if (!ret)
                {
                    logger.Exception(100, "ProcessXML_Reservation failed . responseXml = " + responseXml);
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
       
        #endregion

        #region UpdateConferencePassword
        /// <summary>
        /// UpdateConferencePassword
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="updateXml"></param>
        /// <returns></returns>
        internal bool UpdateConferencePassword(NS_MESSENGER.Conference conf, ref string updateXml)
        {

            string commonXml = null;
            bool ret = CreateXML_CommonParams(conf.cMcu, ref commonXml);
            if (!ret) return false;

            updateXml = "<TRANS_CONF_2>";
            updateXml += commonXml;
            updateXml += "<SET_ENTRY_PASSWORD>";
            updateXml += "<ID>" + conf.sGUID.ToString() + "</ID>";
            if (conf.iPwd == 0)
            {
                updateXml += "<ENTRY_PASSWORD/>";
            }
            else
            {
                updateXml += "<ENTRY_PASSWORD>" + conf.iPwd.ToString() + "</ENTRY_PASSWORD>";
            }

            updateXml += "</SET_ENTRY_PASSWORD>";
            updateXml += "</TRANS_CONF_2>";
            return false;
        }
        #endregion

        //ZD 104003 End

    }
}

#if COMMENT_1
internal bool TerminateConference(NS_MESSENGER.Conference conf)
		{
			try 
			{

				// Login to the bridge
				bool ret = LoginToBridge(ref conf.cMcu);
				if (!ret) return false; // quit if error returned.

				// Fetch common parameters.
				ret = false;
				string commonXML = null;
				ret = CreateXML_CommonParams(conf.cMcu ,ref commonXML);
				if (!ret || commonXML.Length < 1)	return false;// error
			
				// check if conf is ongoing or reservation
				string confXml=null;
				if (conf.dtStartDateTimeInUTC < DateTime.UtcNow)
				{
					// build the xml for fetching all ongoing confs
					ret = false;		
					ret = CreateXML_OngoingConfList(conf.cMcu,ref confXml);
					if (!ret) return false; // error
				}
				else
				{
					// build the xml for fetching all future reservation confs
					ret = false;							
					ret = CreateXML_ReservationConfList(conf.cMcu,ref confXml);
					if (!ret) return false; // error
				}

				// send the transaction
				ret=false;
				string resXml = null;
				if (conf.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv6)
				{
					ret = SendTransaction_overXAP(confXml,ref resXml);
				}
				else
				{
					ret = SendTransaction_overXMLHTTP(conf.cMcu,confXml,ref resXml);
				}
				if (!ret) return false; //error

				bool searchResult = false;
				if (conf.dtStartDateTimeInUTC < DateTime.UtcNow)
				{
					// process the xml and get all ongoing confs running on the bridge
					ret=false;
					ret= ProcessXML_FindOngoingConf(resXml,ref conf,ref searchResult);
					if (!ret) return false; 
				}
				else
				{
					// process the xml and get all reservation confs on the bridge
					ret=false;
					ret= ProcessXML_FindReservationConf(resXml,ref conf,ref searchResult);
					if (!ret) return false; 
				}

				if(searchResult)
				{
					// conf found on bridge . 
					// Build the xml to terminate a conference.
					string terminateXml = null; ret = false;
					ret = CreateXML_TerminateConf(ref terminateXml,conf);
					if (!ret) return false; //error

					// Send transaction to the bridge.
					ret= false;
					string responseXml = null;
					if (conf.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv6)
					{
						ret = SendTransaction_overXAP(terminateXml,ref responseXml);
					}
					else
					{
						ret = SendTransaction_overXMLHTTP(conf.cMcu,terminateXml,ref responseXml);
					}
					if (!ret) return false; //error

					// Parse the response to check if conference was terminated on the bridge successfully.
					ret= false;
					ret = ProcessXML_TerminateConf(responseXml);
					if (!ret) return false;

					return true;			
				}
				else
				{
					this.errMsg = "Conference not found on the MCU";
					return false;
				}

			}
			catch (Exception e)
			{
				logger.Exception (100,e.Message);
				return false;
			}
		}
		
		private bool CreateXML_OngoingConfList(ref string ongoingXml,NS_MESSENGER.Conference conf)
		{
			try
			{
				// Build the xml for fetching list of ongoing confs
				// To retreive the full list of confs , OBJ_TOKEN = -1
				string commonXml = null;
				bool ret = CreateXML_CommonParams(conf.cMcu,ref commonXml);
				if (!ret) return false;//error

				ongoingXml = "<TRANS_CONF_LIST>";
				ongoingXml += commonXml;
				ongoingXml += "<GET_LS><OBJ_TOKEN>-1</OBJ_TOKEN></GET_LS></TRANS_CONF_LIST>";
				return true;
			}
			catch (Exception e)
			{
				logger.Exception(100,e.Message);
				return false;
			}
		}
	
#endif
#if COMMENT			
		/*
		private bool ProcessXML_OngoingConfList(string responseXml,ref Queue conflist)
		{			
			try
			{
				//Load the response xml recd from the bridge and parse it
				XmlDocument xd = new XmlDocument();
				xd.LoadXml(responseXml);
				
				XmlNodeList nodelist;
				XmlNode node;

				// if description = OK , then continue processing
				node = xd.SelectSingleNode("//RESPONSE_TRANS_CONF_LIST/RETURN_STATUS/DESCRIPTION");
				string description = node.InnerXml;
				description=description.Trim();
				if (description.Length < 0 || description.CompareTo("OK")!=0) 
					return false; //error

				//### IGNORE THIS CHECK FOR NOW ###
				//check for any status change
				//node = xd.SelectSingleNode("//RESPONSE_TRANS_CONF_LIST/GET_LS/CONF_SUMMARY_LS/CHANGED");
				//string changed = node.InnerXml;
				//changed = changed.Trim();
				//if (changed.CompareTo("false")==0) 
				//	return(0); //no change in conf list since prev request so just return
				
							
				// Retreive all conferences	
				nodelist = xd.SelectNodes("//RESPONSE_TRANS_CONF_LIST/GET_LS/CONF_SUMMARY_LS/CONF_SUMMARY");
				
				//cycle through each conference one-by-one
				foreach(XmlNode node1 in nodelist)
				{
					// new conf object to store the each conf ongoing details
					NS_MESSENGER.Conference conf = new NS_MESSENGER.Conference();

					//name of conference
					XmlNode name = node1.SelectSingleNode("NAME");
					conf.m_mcuName = name.InnerText;

					XmlNode id = node1.SelectSingleNode("ID");
					conf.m_status.m_mcuID = id.InnerText;

					//### NOT REQUIRED ###
					//XmlNode conf = node1.SelectSingleNode("CONF_CHANGE");
					//VRM_ERROR_HANDLER.ErrorHandler.(conf.InnerText);

					XmlNode status = node1.SelectSingleNode("CONF_STATUS");
					if (status.ChildNodes.Item(1).InnerText == "false") //CONF_EMPTY = true/false
						conf.m_status.m_isEmpty = false;
					else 
						conf.m_status.m_isEmpty = true;

					if (status.ChildNodes.Item(4).InnerText == "true") //RESOURCES_DEFICIENCY = true/false
						conf.m_status.m_isResourceDeficient = true;
					else
						conf.m_status.m_isResourceDeficient = false;

					//### ADDITIONAL STATUS PARAMS . NOT REQUIRED. ###
					//status.ChildNodes.Item(0).InnerText; //CONF_OK = true/false				
					//status.ChildNodes.Item(2).InnerText; //SINGLE_PARTY = true/false
					//status.ChildNodes.Item(3).InnerText; //NOT_FULL = true/false
					//status.ChildNodes.Item(5).InnerText; //BAD_RESOURCES = true/false
					//status.ChildNodes.Item(6).InnerText; //PROBLEM_PARTY = true/false	

					// total number of connected parties
					XmlNode numConnectedParties = node1.SelectSingleNode("NUM_CONNECTED_PARTIES");
					conf.m_status.m_totalConnectedParties = Int32.Parse(numConnectedParties.InnerText);

					conflist.Enqueue(conf);
				}
			}
			catch(Exception e)
			{
				logger.Exception(100,0,"PolycomAccord.cs","Accord::ProcessXML_OngoingConfList()",362,e.Message);
				return false;
			}
			return true;
		}

		*/
	
		private bool ProcessXML_FindOngoingConf(string responseXml,ref Queue confq)
		{	
			try
			{
				//Load the response xml recd from the bridge and parse it
				XmlDocument xd = new XmlDocument();
				xd.LoadXml(responseXml);
				
				XmlNodeList nodelist;
				XmlNode node;

				// if description = OK , then continue processing
				node = xd.SelectSingleNode("//RESPONSE_TRANS_CONF_LIST/RETURN_STATUS/DESCRIPTION");
				string description = node.InnerXml;
				description=description.Trim();
				if (description.Length < 0 || description.CompareTo("OK")!=0) 
					return false; //error

				//### IGNORE THIS CHECK. NOT NEEDED. ###
				//check for any status change
				//node = xd.SelectSingleNode("//RESPONSE_TRANS_CONF_LIST/GET_LS/CONF_SUMMARY_LS/CHANGED");
				//string changed = node.InnerXml;
				//changed = changed.Trim();
				//if (changed.CompareTo("false")==0) 
				//	return(0); //no change in conf list since prev request so just return
				
							
				// Retreive all conferences	
				nodelist = xd.SelectNodes("//RESPONSE_TRANS_CONF_LIST/GET_LS/CONF_SUMMARY_LS/CONF_SUMMARY");
				
				//cycle through each conference one-by-one
				foreach(XmlNode node1 in nodelist)
				{				
					NS_MESSENGER.Conference conf = new NS_MESSENGER.Conference();

					//name of conference
					XmlNode name = node1.SelectSingleNode("NAME");
					string confBridgeName = name.InnerText;
					confBridgeName =confBridgeName.Trim();

					// name of conference on the bridge
					conf.m_mcuName = confBridgeName;
					
					// conf id on the bridge. this is seprate from the db id.
					XmlNode id = node1.SelectSingleNode("ID");
					conf.m_status.m_mcuID = id.InnerText;

					//### NOT REQUIRED. NOT SUPPORTED IN DB. ###
					//XmlNode conf = node1.SelectSingleNode("CONF_CHANGE");
					//VRM_ERROR_HANDLER.ErrorHandler.DebugToEventLog(conf.InnerText);

					//CONF_EMPTY = true/false
					XmlNode status = node1.SelectSingleNode("CONF_STATUS");
					if (status.ChildNodes.Item(1).InnerText == "false") 
						conf.m_status.m_isEmpty = false;
					else 
						conf.m_status.m_isEmpty = true;

					//RESOURCES_DEFICIENCY = true/false
					if (status.ChildNodes.Item(4).InnerText == "true") 
						conf.m_status.m_isResourceDeficient = true;
					else
						conf.m_status.m_isResourceDeficient = false;

					//### ADDITIONAL STATUS PARAMS . NOT REQUIRED AT THIS POINT. NOT SUPPORTED IN DB. ###
					//status.ChildNodes.Item(0).InnerText; //CONF_OK = true/false				
					//status.ChildNodes.Item(2).InnerText; //SINGLE_PARTY = true/false
					//status.ChildNodes.Item(3).InnerText; //NOT_FULL = true/false
					//status.ChildNodes.Item(5).InnerText; //BAD_RESOURCES = true/false
					//status.ChildNodes.Item(6).InnerText; //PROBLEM_PARTY = true/false	

					// total number of connected parties
					XmlNode numConnectedParties = node1.SelectSingleNode("NUM_CONNECTED_PARTIES");
					conf.m_status.m_totalConnectedParties = Int32.Parse(numConnectedParties.InnerText);
					
					// add the conf to the queue.
					confq.Enqueue(conf);
				}				
			}
			catch(Exception e)
			{
				loggerger.Exception(e.Message,"PolycomAccord.cs","ProcessXML_FindOngoingConf");
				return false;
			}
	
			return true;
		}


		/*
		private bool ProcessXML_FindOngoingConf(string responseXml,ref NS_MESSENGER.Conference conf,ref bool searchResult)
		{	
			searchResult = false;
			try
			{
				//Load the response xml recd from the bridge and parse it
				XmlDocument xd = new XmlDocument();
				xd.LoadXml(responseXml);
				
				XmlNodeList nodelist;
				XmlNode node;

				// if description = OK , then continue processing
				node = xd.SelectSingleNode("//RESPONSE_TRANS_CONF_LIST/RETURN_STATUS/DESCRIPTION");
				string description = node.InnerXml;
				description=description.Trim();
				if (description.Length < 0 || description.CompareTo("OK")!=0) 
					return false; //error

				//### IGNORE THIS CHECK. NOT NEEDED. ###
				//check for any status change
				//node = xd.SelectSingleNode("//RESPONSE_TRANS_CONF_LIST/GET_LS/CONF_SUMMARY_LS/CHANGED");
				//string changed = node.InnerXml;
				//changed = changed.Trim();
				//if (changed.CompareTo("false")==0) 
				//	return(0); //no change in conf list since prev request so just return
				
							
				// Retreive all conferences	
				nodelist = xd.SelectNodes("//RESPONSE_TRANS_CONF_LIST/GET_LS/CONF_SUMMARY_LS/CONF_SUMMARY");
				
				//cycle through each conference one-by-one
				foreach(XmlNode node1 in nodelist)
				{				
					//name of conference
					XmlNode name = node1.SelectSingleNode("NAME");
					string confBridgeName = name.InnerText;
					confBridgeName =confBridgeName.Trim();
					conf.m_mcuName = conf.m_mcuName.Trim();

					// compare if same conf
					if (conf.m_mcuName == confBridgeName)
					{
						// conf id on the bridge. this is seprate from the db id.
						XmlNode id = node1.SelectSingleNode("ID");
						conf.m_status.m_mcuID = id.InnerText;

#region ADDTL PARAMS. NOT SUPPORTED IN DB YET.
						//XmlNode conf = node1.SelectSingleNode("CONF_CHANGE");
						//VRM_ERROR_HANDLER.ErrorHandler.DebugToEventLog(conf.InnerText);
						//CONF_EMPTY = true/false
						//XmlNode status = node1.SelectSingleNode("CONF_STATUS");
						//if (status.ChildNodes.Item(1).InnerText == "false") 
						//	conf.m_status.m_isEmpty = false;
						//else 
						//	conf.m_status.m_isEmpty = true;
						//RESOURCES_DEFICIENCY = true/false
						//if (status.ChildNodes.Item(4).InnerText == "true") 
						//	conf.m_status.m_isResourceDeficient = true;
						//else
						//	conf.m_status.m_isResourceDeficient = false;
						//status.ChildNodes.Item(0).InnerText; //CONF_OK = true/false				
						//status.ChildNodes.Item(2).InnerText; //SINGLE_PARTY = true/false
						//status.ChildNodes.Item(3).InnerText; //NOT_FULL = true/false
						//status.ChildNodes.Item(5).InnerText; //BAD_RESOURCES = true/false
						//status.ChildNodes.Item(6).InnerText; //PROBLEM_PARTY = true/false	
						// total number of connected parties
						//XmlNode numConnectedParties = node1.SelectSingleNode("NUM_CONNECTED_PARTIES");
						//conf.m_status.m_totalConnectedParties = Int32.Parse(numConnectedParties.InnerText);
#endregion
						searchResult = true;
						break;
					}
				}				
				
				return true;
			}
			catch(Exception e)
			{
				logger.Exception(100,0,"PolycomAccord.cs","ProcessXML_FindOngoingConf",537,e.Message);
				return false;
			}
		}

		*/
#endif
