//ZD 100147 Start
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886 End
/* FILE : Database.cs
 * DESCRIPTION : All database-related sql queries are stored in this file.
 * AUTHOR : Kapil M
 */
namespace NS_DATABASE
{
	#region References
	using System;
	using System.Data;
	using System.Data.SqlClient;
	using System.Xml;
	using System.Collections;
	using cryptography;
	using System.Threading;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.RegularExpressions;
    //using RTC_v181.WhyGoServices;
    using System.Reflection;
    using System.Text;
    using System.Xml.Linq;//FB 2593

	#endregion 
	
	class Database
	{
		private NS_LOGGER.Log logger;
		
		private NS_MESSENGER.ConfigParams configParams;
	
		internal string errMsg = null;
		private string conStr = null;

        // NOTE: conference status (0= Scheduled, 1=Pending,3 = Terminated,5 = Ongoing, 6 = OnMcu, 7 = Completed , 9 = Deleted)
        // NOTE: conference type (4= p2p , 7= room conference ) 

        // fetch only conferences which match this filter - status = scheduled (0) & not deleted (0), 
        private string immediateSql_Status0 = "SELECT confid,instanceid FROM CONF_CONFERENCE_D C WHERE C.STATUS = 0 AND C.deleted = 0  AND C.status = 0 AND (C.confdate > getutcdate() AND C.confdate < dateadd(s,1, getutcdate())) ORDER BY C.confdate";
        private string ongoingSql_Status0 = "SELECT confid,instanceid FROM CONF_CONFERENCE_D C WHERE (C.STATUS = 0 ) AND C.deleted = 0  AND (C.confdate < getutcdate() AND dateadd(minute, C.duration, C.confdate) > getutcdate()) ORDER BY C.confdate";
        private string retrySql_Status0 = "SELECT confid,instanceid FROM CONF_CONFERENCE_D C WHERE (C.STATUS = 0 ) AND C.deleted = 0   AND (C.confdate < getutcdate() AND dateadd(minute, C.duration, C.confdate) > getutcdate()) ORDER BY C.confdate";
        private string completedP2PSql = "SELECT confid,instanceid FROM CONF_CONFERENCE_D C WHERE (C.STATUS = 0 OR C.STATUS = 5) AND C.Conftype = 4 AND C.deleted = 0 AND (getutcdate() > C.confdate) AND (datediff(minute, getutcdate(),(dateadd (minute,C.duration,C.confdate))) between -2 and 0)ORDER BY C.confdate"; //ZD 101567
        private string completedVMRSql = "SELECT confid,instanceid FROM CONF_CONFERENCE_D C WHERE (C.STATUS = 0 OR C.STATUS = 5) AND C.Conftype = 2 AND C.IsVMR = 2 AND C.deleted = 0 AND C.isBJNConf = 0 AND (getutcdate() > C.confdate) AND (datediff(minute, getutcdate(),(dateadd (minute,C.duration,C.confdate))) between -2 and 0)ORDER BY C.confdate";//ZD 100522_S1 //ZD 101567 //ZD 103263
        private string immediateSql_Status5 = "SELECT confid,instanceid FROM CONF_CONFERENCE_D C WHERE (C.STATUS = 5) AND C.deleted = 0 AND C.status = 0 AND (C.confdate > getutcdate() AND C.confdate < dateadd(s,1, getutcdate())) ORDER BY C.confdate";
        private string ongoingSql_Status5 = "SELECT confid,instanceid FROM CONF_CONFERENCE_D C WHERE (C.STATUS = 5 ) AND C.deleted = 0 AND (C.confdate < getutcdate() AND dateadd(minute, C.duration, C.confdate) > getutcdate()) ORDER BY C.confdate";
        private string retrySql_Status5 = "SELECT confid,instanceid FROM CONF_CONFERENCE_D C WHERE (C.STATUS = 0 OR C.STATUS = 5 ) AND C.deleted = 0 AND (C.confdate < getutcdate() AND dateadd(minute, C.duration, C.confdate) > getutcdate()) ORDER BY C.confdate";
        //private string completedVideoSql = "SELECT confid,instanceid FROM CONF_CONFERENCE_D C WHERE (C.STATUS = 0 OR C.STATUS = 5 OR C.STATUS = 6) AND C.Conftype = 2 AND C.deleted = 0 AND C.isBJNConf = 0 AND (getutcdate() > C.confdate) AND (datediff(minute, getutcdate(),(dateadd (minute,C.duration,C.confdate))) between -2 and 1)ORDER BY C.confdate";//ZD 101522 //ZD 103263
        private string completedVideoSql = "SELECT confid,instanceid FROM CONF_CONFERENCE_D C WHERE (C.STATUS = 0 OR C.STATUS = 5 OR C.STATUS = 6) AND C.Conftype = 2 AND C.deleted = 0 AND  c.confnumname in (select confuid from Conf_Bridge_D b where b.BridgeTypeid = 16) AND (getutcdate() > C.confdate) AND (datediff(minute, getutcdate(),(dateadd (minute,C.duration,C.confdate))) between -2 and 1)ORDER BY C.confdate";//ZD 104579
       
        
        //private string p2pSql = "SELECT confid,instanceid FROM CONF_CONFERENCE_D C WHERE (C.STATUS = 0 ) AND C.deleted = 0 AND C.conftype  = 4 AND (C.confdate >= getutcdate() AND c.confdate < dateadd(s, 300, getutcdate())) ORDER BY C.confdate";
        
        //Vidyo FB 2599 Start
        private string completedCloudCall = " SELECT confid,instanceid,C.orgID FROM CONF_CONFERENCE_D C, Org_settings_d O WHERE (C.STATUS = 0 OR C.STATUS = 5) AND C.Conftype = 2 AND C.deleted = 0 AND (getutcdate() > C.confdate) AND (datediff(minute, getutcdate(),(dateadd (minute,C.duration,C.confdate))) between -2 and 1) and O.OrgID = c.OrgID and O.EnableCloud = 1 ORDER BY C.confdate";
        internal int defaultOrgId = 11;
        internal Dictionary<string, int> deptList = null;
        internal int eptProfileID = -1; //ALLDEV-814

        //FB 2599 End

        private static List<NS_MESSENGER.timeZoneDetails> timeZoneArray; //FB 2594

		#region Constructor				
		internal Database(NS_MESSENGER.ConfigParams config)
		{
			// assign the config params
			configParams = new NS_MESSENGER.ConfigParams();
			configParams = config;
            if (configParams.trustedConnection.Trim().ToLower() == "true") //FB 2700
                conStr = "Trusted_Connection = true ";
            else
            {
                conStr = "User ID = " + configParams.databaseLogin;
                conStr += ";Password = " + configParams.databasePwd;
            }
                
			// connection string
			conStr += ";database=" + configParams.databaseName ;
			conStr += ";Data Source=" + configParams.databaseServer ;
			conStr += ";Connect Timeout = "+ configParams.databaseConTimeout.ToString();

			logger = new NS_LOGGER.Log(configParams);

		}
       
		#endregion

		#region Query executor
		private bool SelectCommand (string query,ref DataSet ds , string dsTable)
		{
			logger.Trace(query);
            SqlConnection con;
            try
            {
                con = new SqlConnection(conStr);
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return (false);
            }
			try
			{

				SqlCommand cmd = new SqlCommand(query,con);		
				SqlDataAdapter da = new SqlDataAdapter();
				con.Open();	
				da.SelectCommand = cmd;
				da.Fill(ds,dsTable);
                da.Dispose();
                con.Close();

				return (true);
			}
			catch (Exception e)
			{
				logger.Exception(100,"Query = " + query + ", Error = " + e.Message);
				con.Close();
				return(false);
			}
			
		}

		private bool NonSelectCommand (string query)
		{
			logger.Trace(query);
			SqlConnection con;
			try
			{
				con = new SqlConnection(conStr);
			}
			catch (Exception e) 
			{
			    logger.Exception(100,e.Message);				
			    return(false);
			}

			try
			{
				con.Open();	
				SqlCommand cmd = new SqlCommand(query,con);		
				SqlDataReader reader = cmd.ExecuteReader();				
				reader.Close();
				con.Close();				
				return (true);
			}
			catch (Exception e)
			{
				logger.Exception(100,"Query = " + query + ", Error = " + e.Message);
				con.Close();
				return(false);
			}	
		}
		//FB 2594
        private object SelectScalarCommand(string query)
        {
            logger.Trace(query);
            SqlConnection con;
            object oRet =  null;
            try
            {
                con = new SqlConnection(conStr);
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return (null);
            }

            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(query, con);
                oRet = cmd.ExecuteScalar();
                con.Close();
                return (null);
            }
            catch (Exception e)
            {
                logger.Exception(100, "Query = " + query + ", Error = " + e.Message);
                con.Close();
                return (null);
            }

            return oRet;

        }

		#endregion

		#region Terminal Control Methods
		/*
				internal bool FetchConf(int confId,int instanceId,ref NS_MESSENGER.Conference conf)
				{			
					try 
					{
						// Retreive conf details
						DataSet ds = new DataSet();
						string dsTable = "Conference";
						string query = "Select confnumname from CONF_CONFERENCE_D where confid = " + confId.ToString() + " and instanceid = " + instanceId.ToString();
						bool ret = SelectCommand(query,ref ds,dsTable);
						if (!ret)
						{
							logger.Trace("SQL error");
							return (false);
						}		
				
						// retrieve the confnumname
						conf.sDbName = ds.Tables[dsTable].Rows[0][0].ToString().Trim();  
				
						conf.iDbID = confId;
						conf.iInstanceID = instanceId;

						// fetch the rest of conf details
						ret = false;
						ret = FetchConf(ref conf);
						if (!ret)
						{
							logger.Trace ("Problem fetching conf details...");
							return false;
						}
		
						// retrieve all rooms
						bool ret1= FetchAllRooms(confId,instanceId,ref conf.qParties);
						if (!ret1)
						{
							logger.Trace("Error retreiving rooms for conference =" + conf.sDbName);
						}
				
						// retrieve all users & guests
						bool ret2= FetchAllUsers(confId,instanceId,ref conf.qParties);
						if (!ret2)
						{
							logger.Trace("Error retreiving users for conference =" + conf.sDbName);
						}
				
						return (true);
					}
					catch(Exception e)
					{	
						logger.Exception(100,e.Message);
						return false;
					}
				}

		*/
		internal bool FetchAllRooms(int confId,int instanceId,ref Queue roomq)
		{
			try 
			{
				DataSet ds = new DataSet();
				string dsTable = "AllRooms";

				// Fetch which bridge the room endpoint is on.
				string query = " select  e.[name],cr.bridgeid,cr.roomid";
				query += " from Conf_Room_d cr,Loc_Room_D r, Ept_List_D e";
				query += " where cr.confid = " + confId.ToString() ;
				query += " and cr.instanceid = " + instanceId.ToString(); 
				query += " and cr.roomid = r.roomid";
                query += " and r.endpointid = e.endpointid";
                query += " and cr.profileid = e.profileid";

				bool ret = SelectCommand(query,ref ds,dsTable);
				if (!ret)
				{
					logger.Exception(100,"SQL error");
					return (false);
				}

				for (int i=0;i<ds.Tables[dsTable].Rows.Count ; i++)
				{	
					// New room
					NS_MESSENGER.Party room = new NS_MESSENGER.Party();
	
					// Endpoint name 
					room.sName = ds.Tables[dsTable].Rows[i]["name"].ToString().Trim();					
					
					// BridgeID which is assigned to that endpoint
					int bridgeId = Int32.Parse(ds.Tables[dsTable].Rows[i]["bridgeid"].ToString());
					logger.Trace ("Party MCU ID = "+ bridgeId.ToString());	

					// Fetch the Bridge info  
					bool ret2 = FetchMcu(bridgeId ,ref room.cMcu);
					if (!ret2)
					{
						logger.Trace("Error in fetching MCU info");
						return (false);
					}									
	
					// room id
					room.iDbId = Int32.Parse(ds.Tables[dsTable].Rows[i]["roomid"].ToString().Trim());					

					// type = ROOM
					room.etType = NS_MESSENGER.Party.eType.ROOM;

					// Add to the room queue.
					roomq.Enqueue(room);
				}			
			
				return(true);			
			}
			catch(Exception e)
			{	
				logger.Exception(100,e.Message);				
				return (false);
			}					

		}
		internal bool FetchAllUsers(int confId,int instanceId,ref Queue userq)
		{
			try 			
			{
				DataSet ds = new DataSet();
				string dsTable = "AllUsers";
			
				// Fetch the bridge which the endpoint is on.
                string query = "select C.bridgeID,U.PreConfCode,U.PreLeaderPin,U.PostLeaderPin,U.Audioaddon,U.AudioDialInPrefix"; //FB 2655
                query += " from Conf_User_D C,Usr_List_D U ";//FB 2655
                query += " where  C.UserID = U.UserID "; //FB 2655
                query += " and C.ConfID = " + confId.ToString(); //FB 2655
				query += " and C.InstanceId = " + instanceId.ToString();
				query += " and C.Status = 1 and C.Invitee = 1";

				bool ret = SelectCommand(query,ref ds,dsTable);
				if (!ret)
				{
					logger.Trace("SQL error");
					return (false);
				}

				for (int i=0;i<ds.Tables[dsTable].Rows.Count ; i++)
				{
					NS_MESSENGER.Party user = new NS_MESSENGER.Party();

					// bridgeid 
					int bridgeId = Int32.Parse (ds.Tables[dsTable].Rows[i]["bridgeID"].ToString());

                    //FB 2655 Start
                    user.preConfCode = ds.Tables[dsTable].Rows[i]["PreConfCode"].ToString();
                    user.preLPin = ds.Tables[dsTable].Rows[i]["PreLeaderPin"].ToString();
                    user.postLPin = ds.Tables[dsTable].Rows[i]["PostLeaderPin"].ToString();
                    user.AudioDialInPrefix = ds.Tables[dsTable].Rows[i]["AudioDialInPrefix"].ToString();
                    int Audioparty = Int32.Parse(ds.Tables[dsTable].Rows[i]["Audioaddon"].ToString());
                    if (Audioparty == 1)
                        user.bAudioBridgeParty = true;
                    else
                        user.bAudioBridgeParty = false;
                    // 2655 End

					// Fetch the Bridge info  
					bool ret2 = FetchMcu(bridgeId ,ref user.cMcu);
					if (!ret2)
					{
						logger.Exception(100,"Error in fetching MCU info");
						return (false);
					}						

					// Add user/guest to the queue
					userq.Enqueue(user);
				}								
				return true;
			}
			catch (Exception e)
			{
				logger.Exception(100,e.Message);
				return (false);
			}		
		}		
		
		internal bool FetchRoomAdminsEmails(int confId,int instanceId, ref Queue roomAdminList)
		{
			try
			{
				DataSet ds = new DataSet();
				string dsTable = "RoomAdmins";
			    

                //ALLDEV-807 Starts
                //// Fetch the bridge which the endpoint is on.
                //string query = "select r.GuestContactEmail,r.NotifyEmails"; //ZD 100619
                //query += " from Conf_Room_d cr,Loc_Room_D r"; //,Usr_List_D u //ZD 100619
                //query += " where cr.ConfID = " + confId.ToString();
                //query += " and cr.InstanceId = " + instanceId.ToString();
                //query += " and cr.roomid = r.roomID";
                ////query += " and r.Assistant = u.UserID"; //ZD 100619
                string query = "select distinct cr.RoomId , (select top 1 case when l.AssistantId <10 then l.EmailId else u.Email end from Loc_Assistant_D l, Usr_List_D u ";
                query += "where  l.AssistantId = u.UserID and r.RoomID=l.Roomid order by l.ID) as GuestContactEmail ,r.NotifyEmails from Conf_Room_d cr,Loc_Room_D r  where cr.ConfID =" + confId.ToString();
                query += " and cr.InstanceId = " + instanceId.ToString() + " and cr.roomid = r.roomID -- and la.RoomId = cr.RoomID";
                //ALLDEV-807 Ends

				bool ret = SelectCommand(query,ref ds,dsTable);
				if (!ret)
				{
					logger.Trace("SQL error");
					return (false);
				}

				for (int i=0;i<ds.Tables[dsTable].Rows.Count ; i++)
				{

                    roomAdminList.Enqueue(ds.Tables[dsTable].Rows[i]["GuestContactEmail"].ToString().Trim()); //ZD 100619
					
					string notifyEmails = ds.Tables[dsTable].Rows[i]["notifyEmails"].ToString().Trim();					
					string delimitter = ";";
					string[] split = notifyEmails.Split(delimitter.ToCharArray());
					foreach (string s in split) 
					{
						string email = s.Trim();
						if (email.Length > 3)
						{
							roomAdminList.Enqueue(s.Trim());		
						}
					}
				}

				return true;
			}
			catch (Exception e)
			{
				logger.Exception(100,e.Message);
				return (false);
			}
		}

		
		internal bool FetchParticipantsEmails(int confId,int instanceId, ref Queue partyList)
		{
			try
			{
				DataSet ds = new DataSet();
				string dsTable = "Participants";
			
				// Fetch the participant emails - users & guests
				string query ="declare @confid as int";
				query+=" declare @instanceid as int";
				query+=" set @confid = " + confId.ToString();
				query+=" set @instanceid = " + instanceId.ToString();
				query+=" select u.email from conf_user_d cu, usr_list_d u";
				query+=" where cu.confid = @confid and cu.instanceid = @instanceid and cu.userid = u.userid";
				query+=" union";
				query+=" select g.email from conf_user_d cu, usr_guestlist_d g";
				query+=" where cu.confid = @confid and cu.instanceid = @instanceid and cu.userid = g.userid";

				bool ret = SelectCommand(query,ref ds,dsTable);
				if (!ret)
				{
					logger.Trace("SQL error");
					return (false);
				}

				for (int i=0;i<ds.Tables[dsTable].Rows.Count ; i++)
				{
					// adding the email to the party list queue
					partyList.Enqueue(ds.Tables[dsTable].Rows[i]["Email"].ToString().Trim());
				}

				return true;
			}
			catch (Exception e)
			{
				logger.Exception(100,e.Message);
				return (false);
			}
		}

		internal bool FetchRoom(int confId,int instanceId, int endpointId, ref NS_MESSENGER.Party room)
		{
			try 
			{
				DataSet ds = new DataSet();
				string dsTable = "Room";
                int temp = 0;
				// Fetch which bridge the room endpoint is on.
                string query = " select  cr.roomid, e.[name],cr.bridgeid, connect2, cr.ipisdnaddress, cr.GUID, cr.AddressType, cr.OnlineStatus,"; //FB 2249 FB 2501 Call Monitoring FB 2501 Dec5
                query += " e.isTelePresence, e.videoequipmentid, cr.MultiCodecAddress,cr.DefLineRate, cr.isTextMsg,cr.connectiontype,cr.PartyNameonMCU,cr.SysLocationId"; //FB 2578 //FB 2553-RMX //ZD 100697 //ZD 101056 //ZD104821
				query += " from Conf_Room_D cr , Loc_Room_D c, Ept_List_D e";
				query += " where cr.confid = " + confId.ToString() ;
				query += " and cr.instanceid = " + instanceId.ToString(); 
				query += " and c.roomid = " + endpointId.ToString();
				query += " and cr.roomid = c.roomid";
                query += " and c.endpointid = e.endpointid";
                query += " and cr.profileid = e.profileid";

				bool ret = SelectCommand(query,ref ds,dsTable);
				if (!ret)
				{
					logger.Exception(100,"SQL error");
					return (false);
				}

				if(ds.Tables[dsTable].Rows.Count > 0)
				{
                    room = new NS_MESSENGER.Party(); //ZD 101496
                    // room id . 
                    // Magic Number = 10000 . Needed so that it doesnot clash with user-id's.
                    int.TryParse(ds.Tables[dsTable].Rows[0]["roomid"].ToString(), out temp);
                    room.iDbId = temp + 10000;
                    //room.iEndpointId = int.Parse(ds.Tables[dsTable].Rows[0]["roomid"].ToString());//FB 2959	
                    room.iEndpointId = temp;
					// full name 					
					room.sName = ds.Tables[dsTable].Rows[0]["name"].ToString().Trim();
                    room.sMcuName = ds.Tables[dsTable].Rows[0]["PartyNameonMCU"].ToString().Trim(); //ZD 101056
                    //ZD 104821 Starts
                    temp = 0;
                    int.TryParse(ds.Tables[dsTable].Rows[0]["SysLocationId"].ToString(), out temp);
                    room.SysLocationID = temp;
                    //Zd 104821 Ends
					
                    // BridgeID which is assigned to that endpoint
					int bridgeId = Int32.Parse(ds.Tables[dsTable].Rows[0]["bridgeid"].ToString());
					logger.Trace ("Party MCU ID = "+ bridgeId.ToString());	

                    // update room type
                    room.etType = NS_MESSENGER.Party.eType.ROOM;

                    if (ds.Tables[dsTable].Rows[0]["ipisdnaddress"] != null)//FB 2249
                        room.sAddress = ds.Tables[dsTable].Rows[0]["ipisdnaddress"].ToString().Trim();

                    if (ds.Tables[dsTable].Rows[0]["GUID"] != null)
                        room.sGUID = ds.Tables[dsTable].Rows[0]["GUID"].ToString().Trim();//FB 2501 Call Monitorin

                    //FB 2501 Dec5 start
                    //address type
                    int iAddressType = Int32.Parse(ds.Tables[dsTable].Rows[0]["AddressType"].ToString());
                    ret = ConvertToMcuAddressType(iAddressType, ref room);
                    if (!ret)
                    {
                        logger.Trace("Participant address type is incorrect.");
                        return false;
                    }
                    //FB 2501 Dec5 End

                    //FB 2501 Dec6 Start
                    int status = Int32.Parse(ds.Tables[dsTable].Rows[0]["OnlineStatus"].ToString());
                    ret = FetchPartyStatus(status, ref room);
                    if (!ret)
                    {
                        logger.Trace("Participant status is incorrect.");
                        return false;
                    }
                    //FB 2501 Dec6 End

                    //FB 2578 start
                   
                    int isTelepres = 0;

                    if (ds.Tables[dsTable].Rows[0]["isTelePresence"].ToString() != null)
                        if (ds.Tables[dsTable].Rows[0]["isTelePresence"].ToString().Trim() != "")
                            Int32.TryParse(ds.Tables[dsTable].Rows[0]["isTelePresence"].ToString(), out isTelepres);

                    if (isTelepres == 1)
                        room.isTelePresence = true;

                    if (ds.Tables[dsTable].Rows[0]["MultiCodecAddress"].ToString() != null)
                        if (ds.Tables[dsTable].Rows[0]["MultiCodecAddress"].ToString().Trim() != "")
                            room.MulticodecAddress = ds.Tables[dsTable].Rows[0]["MultiCodecAddress"].ToString();
              
                    //FB 2578 end


                    //FB 2553-RMX Starts
                    int isMessageOverlay = Int32.Parse(ds.Tables[dsTable].Rows[0]["isTextMsg"].ToString());
                    if (isMessageOverlay == 1)
                    {
                        room.bIsMessageOverlay = true;
                    }
                    else
                    {
                        room.bIsMessageOverlay = false;
                    }
                    //FB 2553-RMX Ends


                    // ZD 100697 Start - line rate
                    if (ds.Tables[dsTable].Rows[0]["DefLineRate"].ToString() != null)
                    {
                        int iLineRate = Int32.Parse(ds.Tables[dsTable].Rows[0]["DefLineRate"].ToString());
                        ret = ConvertToMcuLineRate(iLineRate, ref room.stLineRate.etLineRate);
                        if (!ret)
                        {
                            logger.Trace("Participant line rate is incorrect.");
                            return false;
                        }
                    }
                    else
                    {
                        // default to 384 Kbps
                        room.stLineRate.etLineRate = NS_MESSENGER.LineRate.eLineRate.K384;
                    }
                    // ZD 100697 End - line rate

					// Fetch the Bridge info  
					bool ret2 = FetchMcu(bridgeId ,ref room.cMcu);
					if (!ret2)
					{
						logger.Trace("Error in fetching MCU info");
						return (false);
					}

                    //ZD 101056 Starts
                    int iConnectionType = 0;
                    iConnectionType = Int32.Parse(ds.Tables[dsTable].Rows[0]["connectiontype"].ToString());
                    ret = ConvertToConnectionType(iConnectionType, ref room);
                    if (!ret)
                    {
                        logger.Trace("Participant connection type is incorrect.");
                        return false;
                    }
                    //ZD 101056 Ends
					return(true);
				}
				else
				{
					//no record in the database
					return (false);
				}							
			}
			catch(Exception e)
			{	
				logger.Exception(100,e.Message);				
				return (false);
			}					
		}

        //FB 1552
        internal bool FetchGuestDetail(int confId, int instanceId, int endpointId, ref NS_MESSENGER.Party guest)
        {
            DataSet ds = new DataSet();
            string dsTable = "Guests";

            string query = "select g.email,c.IPISDNAddress,c.connectionType,c.userid,c.GUID,c.bridgeid";//FB 2501 - Call Monitoring
            query += ",c.bridgeipisdnaddress,C.AudioOrVideo,C.OutsideNetwork,C.McuServiceName";
            query += ",C.AddressType,C.DefLineRate, C.isLecturer, C.Connect2";
            query += ",c.APIPortNo,c.mute,c.MuteTxvideo ";//Code changed for API Port //ZD 100628
            query += " from Usr_GuestList_D g , Conf_User_D c ";
            query += " where c.confid = " + confId.ToString();
            query += " and c.instanceid = " + instanceId.ToString();
            query += " and c.userid = " + endpointId.ToString();
            query += " and c.userid = g.userid ";

            bool ret = SelectCommand(query, ref ds, dsTable);
            if (!ret)
            {
                logger.Exception(10, "SQL error");
                return (false);
            }

            for (int i = 0; i < ds.Tables[dsTable].Rows.Count; i++)
            {
                try
                {
                    NS_MESSENGER.Party party = new NS_MESSENGER.Party();

                    // name
                    party.sName = ds.Tables[dsTable].Rows[i]["Email"].ToString();
                    logger.Trace(" User = " + party.sName);

                    //ip/isdn address
                    party.sAddress = ds.Tables[dsTable].Rows[i]["IPISDNAddress"].ToString();

                    // connection type
                    int iConnectionType = 0;
                    iConnectionType = Int32.Parse(ds.Tables[dsTable].Rows[i]["ConnectionType"].ToString());
                    ret = ConvertToConnectionType(iConnectionType, ref party);
                    if (!ret)
                    {
                        logger.Trace("Participant connection type is incorrect.");
                        return false;
                    }

                    //guestid 
                    party.iDbId = Int32.Parse(ds.Tables[dsTable].Rows[i]["UserID"].ToString());

                    //FB 2501 - Call Monitoring 
                    if (ds.Tables[dsTable].Rows[i]["GUID"] != null)
                    {
                        party.sGUID = ds.Tables[dsTable].Rows[i]["GUID"].ToString();
                    }

                    //bridgeid 
                    party.cMcu.iDbId = Int32.Parse(ds.Tables[dsTable].Rows[i]["BridgeID"].ToString());

                    // bridgeipisdnaddress
                    party.sMcuAddress = ds.Tables[dsTable].Rows[i]["BridgeIpIsdnAddress"].ToString();
                    logger.Trace("MCU Address: " + party.sMcuAddress);

                    // Fetch the Bridge info
                    bool ret4 = FetchMcu(party.cMcu.iDbId, ref party.cMcu);
                    if (!ret4)
                    {
                        logger.Exception(100, "Error in fetching MCU info");
                        return (false);
                    }

                    // audio or video 
                    int audioOrVideo = Int32.Parse(ds.Tables[dsTable].Rows[i]["AudioOrVideo"].ToString());
                    if (audioOrVideo == 1)  //FB 1740 //FB 1744
                        party.etCallType = NS_MESSENGER.Party.eCallType.AUDIO;
                    else
                        party.etCallType = NS_MESSENGER.Party.eCallType.VIDEO;

                    //outside network
                    int outsideNetwork = Int32.Parse(ds.Tables[dsTable].Rows[i]["OutsideNetwork"].ToString());
                    if (outsideNetwork == 1)
                        party.bIsOutsideNetwork = true;

                    //mcu service name
                    party.sMcuServiceName = ds.Tables[dsTable].Rows[i]["McuServiceName"].ToString();

                    // Port address fo ept API port
                    int iAPIPort = 23;
                    if (ds.Tables[dsTable].Rows[i]["APIPortNo"].ToString() != "")
                        Int32.TryParse(ds.Tables[dsTable].Rows[i]["APIPortNo"].ToString().Trim(), out iAPIPort);
                    if (iAPIPort <= 0)
                        iAPIPort = 23;

                    party.iAPIPortNo = iAPIPort;

                    //address type
                    int iAddressType = Int32.Parse(ds.Tables[dsTable].Rows[i]["AddressType"].ToString());
                    ret = ConvertToMcuAddressType(iAddressType, ref party);
                    if (!ret)
                    {
                        logger.Trace("Participant address type is incorrect.");
                        return false;
                    }
                    //ZD 100628 Start
                    int mute = 0;
                    if (ds.Tables[dsTable].Rows[i]["mute"].ToString() != "")
                        Int32.TryParse(ds.Tables[dsTable].Rows[i]["mute"].ToString().Trim(), out mute);
                    if (mute == 1)
                        party.bMute = true;
                    else
                        party.bMute = false;

                    if (ds.Tables[dsTable].Rows[i]["MuteTxvideo"].ToString() != "")
                        Int32.TryParse(ds.Tables[dsTable].Rows[i]["MuteTxvideo"].ToString().Trim(), out mute);
                    if (mute == 1)
                        party.bMuteTxvideo = true;
                    else
                        party.bMuteTxvideo = false;                  

                    //ZD 100628 End
                    // line rate
                    if (ds.Tables[dsTable].Rows[i]["DefLineRate"].ToString() != null)
                    {
                        int iLineRate = Int32.Parse(ds.Tables[dsTable].Rows[i]["DefLineRate"].ToString());
                        ret = ConvertToMcuLineRate(iLineRate, ref party.stLineRate.etLineRate);
                        if (!ret)
                        {
                            logger.Trace("Participant address type is incorrect.");
                            return false;
                        }
                    }
                    else
                    {
                        // default to 384 Kbps
                        party.stLineRate.etLineRate = NS_MESSENGER.LineRate.eLineRate.K384;
                    }


                    // lecturer
                    int isLecturer = Int32.Parse(ds.Tables[dsTable].Rows[i]["isLecturer"].ToString());
                    if (isLecturer == 1)
                    {
                        party.bIsLecturer = true;
                    }

                    try
                    {
                        // connect2
                        party.etCallerCallee = NS_MESSENGER.Party.eCallerCalleeType.CALLEE;
                        int connect2 = Int32.Parse(ds.Tables[dsTable].Rows[i]["Connect2"].ToString());
                        if (connect2 == 1)
                        {
                            party.etCallerCallee = NS_MESSENGER.Party.eCallerCalleeType.CALLER;
                        }
                    }
                    catch (Exception e1)
                    {
                        // just log the error
                        logger.Trace("Error in Connect2 attribute." + e1.Message);
                    }
                    // endpoint type
                    party.etType = NS_MESSENGER.Party.eType.GUEST;
                    guest = party;
                    //add the participant to the conf.
                    logger.Trace("Party added In Detail. Name: " + party.sName + " , McuAddress = " + party.sMcuAddress);
                }
                catch (Exception e)
                {
                    logger.Exception(200, e.Message);
                }
            }

            return true;
        }

        internal bool FetchRoomDetail(int confId, int instanceId, int endpointId, ref NS_MESSENGER.Party room)
        {
            cryptography.Crypto crypto = null;//ZD 100601
            try
            {
                DataSet ds = new DataSet();
                string dsTable = "Rooms";

                string query = "Select e.[name] as endpointName , c.ipisdnaddress,c.GUID"; //FB 2501 - Call Monitoring 
                query += ",c.ConnectionType,c.roomid,c.bridgeid,c.bridgeipisdnaddress,C.MultiCodecAddress,e.isTelePresence"; //FB 2400
                query += ",C.audioorvideo,C.outsidenetwork,C.GateKeeeperAddress,C.mcuservicename, e.videoequipmentid";
                query += ",C.AddressType,C.DefLineRate,C.isLecturer, C.Connect2";
                query += ",e.password,c.APIPortNo,c.SysLocationId";//Code changed for API Port//ZD 100132 //ZD 104821
                query += " from Loc_Room_D r,Conf_Room_D c,Ept_List_D e";
                query += " where c.confid = " + confId.ToString();
                query += " and c.instanceid = " + instanceId.ToString();
                query += " and c.roomid = " + endpointId.ToString();
                query += " and c.roomid = r.roomid";
                query += " and r.endpointid = e.endpointid";
                query += " and c.profileid = e.profileid";
                bool ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Exception(100, "SQL error");
                    return (false);
                }

                for (int i = 0; i < ds.Tables[dsTable].Rows.Count; i++)
                {
                    try
                    {
                        NS_MESSENGER.Party party = new NS_MESSENGER.Party();

                        // name
                        party.sName = ds.Tables[dsTable].Rows[i]["endpointName"].ToString().Trim();

                        logger.Trace("Room endpoint = " + party.sName);

                        //ip/isdn address
                        party.sAddress = ds.Tables[dsTable].Rows[i]["ipisdnaddress"].ToString();

                        //ZD 100601 - Start
                        crypto = new Crypto();
                        if (ds.Tables[dsTable].Rows[i]["password"].ToString() != null && ds.Tables[dsTable].Rows[i]["password"].ToString() != "")
                            party.sPwd = crypto.decrypt(ds.Tables[dsTable].Rows[i]["password"].ToString());
                        //ZD 100601 - End

                        //FB 2501 - Call Monitoring 
                        if (ds.Tables[dsTable].Rows[i]["GUID"] != null)
                        {
                            party.sGUID = ds.Tables[dsTable].Rows[i]["GUID"].ToString();
                        }

                        //ZD 104821 Starts
                        int temp = 0;
                        int.TryParse(ds.Tables[dsTable].Rows[i]["SysLocationId"].ToString(), out temp);
                        room.SysLocationID = temp;
                        //Zd 104821 Ends
                        // Port address fo ept API port 
                        int iAPIPort = 23;
                        if (ds.Tables[dsTable].Rows[i]["APIPortNo"].ToString() != "")
                            Int32.TryParse(ds.Tables[dsTable].Rows[i]["APIPortNo"].ToString().Trim(), out iAPIPort);

                        if (iAPIPort <= 0)
                            iAPIPort = 23;

                        party.iAPIPortNo = iAPIPort;

                        // connection type
                        int iConnectionType = 0;
                        iConnectionType = Int32.Parse(ds.Tables[dsTable].Rows[i]["ConnectionType"].ToString());
                        ret = ConvertToConnectionType(iConnectionType, ref party);
                        if (!ret)
                        {
                            logger.Trace("Participant connection type is incorrect.");
                            return false;
                        }


                        // room id . 
                        // Magic Number = 10000 . Needed so that it doesnot clash with user-id's.
                        party.iDbId = Int32.Parse(ds.Tables[dsTable].Rows[i]["roomid"].ToString()) + 10000;


                        //bridgeid
                        party.iMcuId = Int32.Parse(ds.Tables[dsTable].Rows[i]["bridgeid"].ToString());

                        //bridgeaddress
                        party.sMcuAddress = ds.Tables[dsTable].Rows[i]["bridgeipisdnaddress"].ToString();
                        logger.Trace("MCU Address: " + party.sMcuAddress);

                        // Fetch the Bridge info
                        logger.Trace("Getting the mcu for the room...");
                        bool ret4 = FetchMcu(party.iMcuId, ref party.cMcu);
                        if (!ret4)
                        {
                            logger.Exception(100, "Error in fetching MCU info");
                            continue;
                        }

                        // audio or video 
                        int audioOrVideo = Int32.Parse(ds.Tables[dsTable].Rows[i]["audioOrvideo"].ToString());
                        if (audioOrVideo == 1)  //FB 1740 //FB 1744
                        {
                            logger.Trace("Call Type: Audio");
                            party.etCallType = NS_MESSENGER.Party.eCallType.AUDIO;
                        }
                        else
                        {
                            logger.Trace("Call Type: Video");
                            party.etCallType = NS_MESSENGER.Party.eCallType.VIDEO;
                        }

                        //outside network
                        int outsideNetwork = Int32.Parse(ds.Tables[dsTable].Rows[i]["outsidenetwork"].ToString());
                        if (outsideNetwork == 1)
                            party.bIsOutsideNetwork = true;
                        
                        //GateKeeperAddress //ZD 100132
                        party.sGateKeeeperAddress = "";
                        if (ds.Tables[dsTable].Rows[i]["GateKeeeperAddress"].ToString() != null)
                            if (ds.Tables[dsTable].Rows[i]["GateKeeeperAddress"].ToString().Trim() != "")
                                party.sGateKeeeperAddress = ds.Tables[dsTable].Rows[i]["GateKeeeperAddress"].ToString().Trim();

                        //mcu service name
                        party.sMcuServiceName = ds.Tables[dsTable].Rows[i]["mcuservicename"].ToString();

                        //address type
                        int iAddressType = Int32.Parse(ds.Tables[dsTable].Rows[i]["AddressType"].ToString());
                        ret = ConvertToMcuAddressType(iAddressType, ref party);
                        if (!ret)
                        {
                            logger.Trace("Participant address type is incorrect.");
                            return false;
                        }

                        // endpoint type
                        party.etType = NS_MESSENGER.Party.eType.ROOM;

                        // video equipment id 
                        int videoequipmentid = Int32.Parse(ds.Tables[dsTable].Rows[i]["videoequipmentid"].ToString().Trim());
                        if (videoequipmentid >= 21 && videoequipmentid <= 23)
                        {
                            // its a codian vcr 
                            // get the gateway address from the address field. 
                            party.sGatewayAddress = party.sAddress.Substring(party.sAddress.LastIndexOf(":") + 1);
                            party.sAddress = party.sAddress.Substring(0, party.sAddress.LastIndexOf(":"));
                            party.etVideoEquipment = NS_MESSENGER.Party.eVideoEquipment.RECORDER;
                            party.etModelType = NS_MESSENGER.Party.eModelType.CODIAN_VCR;
                        }
                        else
                        {
                            EndpointModelType(videoequipmentid, ref party.etModelType);
                            logger.Trace("Party model type: " + party.etModelType.ToString());
                        }

                        // line rate
                        if (ds.Tables[dsTable].Rows[i]["DefLineRate"].ToString() != null)
                        {
                            int iLineRate = Int32.Parse(ds.Tables[dsTable].Rows[i]["DefLineRate"].ToString());
                            ret = ConvertToMcuLineRate(iLineRate, ref party.stLineRate.etLineRate);
                            if (!ret)
                            {
                                logger.Trace("Participant line rate is incorrect.");
                                return false;
                            }
                        }
                        else
                        {
                            // default to 384 Kbps
                            party.stLineRate.etLineRate = NS_MESSENGER.LineRate.eLineRate.K384;
                        }

                        // lecturer
                        int isLecturer = Int32.Parse(ds.Tables[dsTable].Rows[i]["isLecturer"].ToString());
                        if (isLecturer == 1)
                        {
                            party.bIsLecturer = true;
                        }

                        try
                        {
                            // connect2
                            party.etCallerCallee = NS_MESSENGER.Party.eCallerCalleeType.CALLEE;
                            int connect2 = Int32.Parse(ds.Tables[dsTable].Rows[i]["Connect2"].ToString());
                            if (connect2 == 1)
                            {
                                party.etCallerCallee = NS_MESSENGER.Party.eCallerCalleeType.CALLER;
                            }
                        }
                        catch (Exception e1)
                        {
                            // just log the error
                            logger.Trace("Error in Connect2 attribute." + e1.Message);
                        }

                        //FB 2400 start
                        party.isTelePresence = false;
                        if (ds.Tables[dsTable].Rows[i]["isTelePresence"].ToString() != null)
                          if (ds.Tables[dsTable].Rows[i]["isTelePresence"].ToString().Trim() == "1")
                                party.isTelePresence = true;

                        party.MulticodecAddress = "";
                        if (ds.Tables[dsTable].Rows[i]["MultiCodecAddress"].ToString() != null)
                            if (ds.Tables[dsTable].Rows[i]["MultiCodecAddress"].ToString().Trim() != "")
                                party.MulticodecAddress = ds.Tables[dsTable].Rows[i]["MultiCodecAddress"].ToString().Trim();

                        //FB 2400 end

                        
                        room = party;
                        logger.Trace("Party . Name: " + party.sName + " , McuAddress = " + party.sMcuAddress);
                    }
                    catch (Exception e)
                    {
                        logger.Exception(100, e.Message);
                        continue;
                    }
                }
                return (true);
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }

        }	

        internal bool FetchUserDetail(int confId, int instanceId, int endpointId, ref NS_MESSENGER.Party party)//ZD 102712 
        {
            DataSet ds = new DataSet();
            string dsTable = "Users";

            string query = "select U.email, C.connectionType,C.IPISDNAddress,C.UserID,C.GUID,C.bridgeID"; //FB 2501 - Call Monitoring 
            query += ",C.bridgeIPISDNAddress,C.AudioOrVideo,C.OutsideNetwork,C.McuServiceName";
            query += ",C.AddressType,C.DefLineRate, C.isLecturer,C.Connect2";
            query += ",c.APIPortNo,c.confuId,u.FirstName,U.PreConfCode,U.PreLeaderPin,U.PostLeaderPin,U.Audioaddon,U.AudioDialInPrefix,C.SysLocationId ";//Code changed for API Port //FB 2387 //FB 2655 //ZD104821
            query += " from Conf_User_D C,Usr_List_D U ";
            query += " where  C.UserID = U.UserID ";
            query += " and C.status = 1 and C.Invitee = 1"; //FB 2490 status-accepted and external attendee
            query += " and C.ConfID = " + confId.ToString();
            query += " and C.InstanceId = " + instanceId.ToString();
            query += " and C.UserID = " + endpointId.ToString(); //ZD 102712

            bool ret = SelectCommand(query, ref ds, dsTable);
            if (!ret)
            {
                logger.Exception(10, "SQL error");
                return (false);
            }

            for (int i = 0; i < ds.Tables[dsTable].Rows.Count; i++)
            {
                try
                {
					//ZD 102712 - commented
                    //NS_MESSENGER.Party party = new NS_MESSENGER.Party();
                    if(party == null)
                        party = new NS_MESSENGER.Party();
                    //ZD 102712 - End

                    // name
                    party.sMcuName = ds.Tables[dsTable].Rows[i]["Email"].ToString();
                    party.sName = party.sMcuName;
                    logger.Trace(" User = " + party.sName);

                    // connection type
                    int iConnectionType = 0;
                    iConnectionType = Int32.Parse(ds.Tables[dsTable].Rows[i]["ConnectionType"].ToString());
                    ret = ConvertToConnectionType(iConnectionType, ref party);
                    if (!ret)
                    {
                        logger.Trace("Participant connection type is incorrect.");
                        return false;
                    }

                    //party ip/isdn address
                    party.sAddress = ds.Tables[dsTable].Rows[i]["IPISDNAddress"].ToString();
					//ZD 102712-coded at end of the method	
                    //FB 2387 Start
                    //if (party.sAddress.Trim().Contains("D") && party.bAudioBridgeParty)//ZD 101655
                    //{
                    //    party.sName = ds.Tables[dsTable].Rows[i]["FirstName"].ToString() + "(" + ds.Tables[dsTable].Rows[i]["confuId"].ToString() + ")";
                    //}
                    //FB 2387 End
                    //userid 
                    party.iDbId = Int32.Parse(ds.Tables[dsTable].Rows[i]["UserID"].ToString());

                    //FB 2501 - Call Monitoring 
                    if (ds.Tables[dsTable].Rows[i]["GUID"] != null)
                    {
                        party.sGUID = ds.Tables[dsTable].Rows[i]["GUID"].ToString();
                    }

                    // bridgeid 
                    party.iMcuId = Int32.Parse(ds.Tables[dsTable].Rows[i]["bridgeID"].ToString());

                    //bridge ip/isdn address
                    party.sMcuAddress = ds.Tables[dsTable].Rows[i]["bridgeIPISDNAddress"].ToString();
                    logger.Trace("MCU Address: " + party.sMcuAddress);

                    //FB 2655 Start
                    //ZD 104821 Starts
                    int  temp = 0;
                    int.TryParse(ds.Tables[dsTable].Rows[i]["SysLocationId"].ToString(), out temp);
                    party.SysLocationID = temp;
                    //ZD 104821 Emds
                    party.preConfCode = ds.Tables[dsTable].Rows[i]["PreConfCode"].ToString();
                    logger.Trace("preConfCode: " + party.preConfCode);

                    party.preLPin = ds.Tables[dsTable].Rows[i]["PreLeaderPin"].ToString();
                    logger.Trace("preLPin: " + party.preLPin);

                    party.postLPin = ds.Tables[dsTable].Rows[i]["PostLeaderPin"].ToString();
                    logger.Trace("postLPin: " + party.postLPin);

                    party.AudioDialInPrefix = ds.Tables[dsTable].Rows[i]["AudioDialInPrefix"].ToString();
                    logger.Trace("AudioDialInPrefix: " + party.AudioDialInPrefix);
					//ZD 102712 Starts
                    int Audioparty = Int32.Parse(ds.Tables[dsTable].Rows[i]["Audioaddon"].ToString());
                    if (Audioparty == 1)
                        party.bAudioBridgeParty = true;
                    else
                        party.bAudioBridgeParty = false;
                    //FB 2655 End

                    //FB 2387 Start
                    if (party.sAddress.Trim().Contains("D") && party.bAudioBridgeParty)//ZD 101655
                    {
                        party.sName = ds.Tables[dsTable].Rows[i]["FirstName"].ToString() + "(" + ds.Tables[dsTable].Rows[i]["confuId"].ToString() + ")";
                    }
                    //FB 2387 End
					//ZD 102712 End
                    // Fetch the Bridge info  
                    bool ret4 = FetchMcu(party.iMcuId, ref party.cMcu);
                    if (!ret4)
                    {
                        logger.Exception(100, "Error in fetching MCU info");
                        continue; //FB 2490 this to avoid ignoring other parties//return (false);
                    }

                    // audio or video 
                    int audioOrVideo = Int32.Parse(ds.Tables[dsTable].Rows[i]["AudioOrVideo"].ToString());
                    if (audioOrVideo == 1)  //FB 1740 //FB 1744
                        party.etCallType = NS_MESSENGER.Party.eCallType.AUDIO;
                    else
                        party.etCallType = NS_MESSENGER.Party.eCallType.VIDEO;

                    //outside network
                    int outsideNetwork = Int32.Parse(ds.Tables[dsTable].Rows[i]["OutsideNetwork"].ToString());
                    if (outsideNetwork == 1)
                        party.bIsOutsideNetwork = true;

                    //mcu service name
                    party.sMcuServiceName = ds.Tables[dsTable].Rows[i]["McuServiceName"].ToString();

                    // Port address fo ept API port
                    int iAPIPort = 23; //Default telnet port no
                    if (ds.Tables[dsTable].Rows[i]["APIPortNo"].ToString() != "")
                        Int32.TryParse(ds.Tables[dsTable].Rows[i]["APIPortNo"].ToString().Trim(), out iAPIPort);

                    if (iAPIPort <= 0)
                        iAPIPort = 23;

                    party.iAPIPortNo = iAPIPort;

                    //address type
                    int iAddressType = Int32.Parse(ds.Tables[dsTable].Rows[i]["AddressType"].ToString());
                    ret = ConvertToMcuAddressType(iAddressType, ref party);
                    if (!ret)
                    {
                        logger.Trace("Participant address type is incorrect.");
                        continue;
                    }

                    // line rate
                    if (ds.Tables[dsTable].Rows[i]["DefLineRate"].ToString() != null)
                    {
                        int iLineRate = Int32.Parse(ds.Tables[dsTable].Rows[i]["DefLineRate"].ToString());
                        ret = ConvertToMcuLineRate(iLineRate, ref party.stLineRate.etLineRate);
                        if (!ret)
                        {
                            logger.Trace("Participant address type is incorrect.");
                            return false;
                        }
                        logger.Trace("Party line rate: " + party.stLineRate.etLineRate.ToString());
                    }
                    else
                    {
                        // default to 384 Kbps
                        party.stLineRate.etLineRate = NS_MESSENGER.LineRate.eLineRate.K384;
                    }

                    // lecturer
                    int isLecturer = Int32.Parse(ds.Tables[dsTable].Rows[i]["isLecturer"].ToString());
                    if (isLecturer == 1)
                    {
                        party.bIsLecturer = true;
                    }

                    try
                    {
                        // connect2
                        party.etCallerCallee = NS_MESSENGER.Party.eCallerCalleeType.CALLEE;
                        int connect2 = Int32.Parse(ds.Tables[dsTable].Rows[i]["Connect2"].ToString());
                        if (connect2 == 1)
                        {
                            party.etCallerCallee = NS_MESSENGER.Party.eCallerCalleeType.CALLER;
                        }
                    }
                    catch (Exception e1)
                    {
                        // just log the error
                        logger.Trace("Error in Connect2 attribute." + e1.Message);
                    }
                    // endpoint type
                    party.etType = NS_MESSENGER.Party.eType.USER;

                    //user = party;//ZD 102712
                   
                    logger.Trace("Party details. Name: " + party.sName + " , McuAddress = " + party.sMcuAddress);
                }
                catch (Exception e)
                {
                    logger.Exception(200, e.Message);
                }
            }

            return true;
        }
        //FB 1552

        internal bool FetchUser(int confId, int instanceId, int endpointId, ref NS_MESSENGER.Party user)
		{
			try
			{
				DataSet ds = new DataSet();
				string dsTable = "Users";
			
				// Fetch the bridge which the endpoint is on.
                string query = "select C.bridgeID,u.email,c.ipisdnaddress,c.GUID,C.AddressType,C.OnlineStatus,u.PreConfCode,"; //FB 2249 FB 2501 //FB Dec5 //FB 2655 //FB 2553 RMX 
                query += " u.PreLeaderPin,u.PostLeaderPin,u.Audioaddon,u.AudioDialInPrefix, C.isTextMsg,C.DefLineRate, C.UserID,C.connectiontype,C.PartyNameonMCU,u.FirstName,C.SysLocationId "; //FB 2989 //ZD 100697 //ZD 101056 //ZD 103633  //ZD 104821
                query += " ,c.confuId from Conf_User_D C,Usr_List_D u"; //ZD 103633
				query += " where C.ConfID = " + confId.ToString();
				query += " and C.InstanceId = " + instanceId.ToString();
				query += " and c.userid = " + endpointId.ToString();
                //ALLDEV-814
                if(eptProfileID > 0)
                    query += " and c.ProfileID = " + eptProfileID.ToString();                
				query += " and c.userid = u.userid" ;

				bool ret = SelectCommand(query,ref ds,dsTable);
				if (!ret)
				{
					logger.Trace("SQL error");
					return (false);
				}

				if (ds.Tables[dsTable].Rows.Count > 0)
				{
					// bridgeid 
					int bridgeId = Int32.Parse (ds.Tables[dsTable].Rows[0][0].ToString());

                    int.TryParse(ds.Tables[dsTable].Rows[0]["UserID"].ToString(), out user.iEndpointId); //FB 2989

                    //FB 2655 Start

                    user.preConfCode = ds.Tables[dsTable].Rows[0]["PreConfCode"].ToString();
                    logger.Trace("preConfCode: " + user.preConfCode);

                    user.preLPin = ds.Tables[dsTable].Rows[0]["PreLeaderPin"].ToString();
                    logger.Trace("preLPin: " + user.preLPin);

                    user.postLPin = ds.Tables[dsTable].Rows[0]["PostLeaderPin"].ToString();
                    logger.Trace("postLPin: " + user.postLPin);

                    user.AudioDialInPrefix = ds.Tables[dsTable].Rows[0]["AudioDialInPrefix"].ToString();
                    logger.Trace("AudioDialInPrefix: " + user.AudioDialInPrefix);

                    int Audioparty = Int32.Parse(ds.Tables[dsTable].Rows[0]["Audioaddon"].ToString());
                    if (Audioparty == 1)
                        user.bAudioBridgeParty = true;
                    else
                        user.bAudioBridgeParty = false;

                    //FB 2655 End


                    // Fetch the Bridge info  
                    bool ret2 = FetchMcu(bridgeId, ref user.cMcu);
                    if (!ret2)
                    {
                        logger.Exception(100, "Error in fetching MCU info");
                        return (false);
                    }						

                    // user name
					user.sName = ds.Tables[dsTable].Rows[0][1].ToString();
                    user.sMcuName = ds.Tables[dsTable].Rows[0]["PartyNameonMCU"].ToString(); //ZD 101056

                    // update type
                    user.etType = NS_MESSENGER.Party.eType.USER;

                    if (ds.Tables[dsTable].Rows[0]["ipisdnaddress"] != null)//FB 2249
                        user.sAddress = ds.Tables[dsTable].Rows[0]["ipisdnaddress"].ToString().Trim();

                    user.sGUID = ds.Tables[dsTable].Rows[0]["GUID"].ToString().Trim(); //FB 2501 Call Monitoring

                    //FB 2501 Dec5 start
                    //address type
                    int iAddressType = Int32.Parse(ds.Tables[dsTable].Rows[0]["AddressType"].ToString());
                    ret = ConvertToMcuAddressType(iAddressType, ref user);
                    if (!ret)
                    {
                        logger.Trace("Participant address type is incorrect.");
                        return false;
                    }
                    //FB 2501 Dec5 End
                    
                    //ZD 104821 Starts
                    int temp = 0;
                    int.TryParse(ds.Tables[dsTable].Rows[0]["SysLocationId"].ToString(), out temp);
                    user.SysLocationID = temp;
                    //ZD 104821 Ends

                    if (user.sAddress.Trim().Contains("D") && user.bAudioBridgeParty)//ZD 101655
                    {
                        user.sName = ds.Tables[dsTable].Rows[0]["FirstName"].ToString() + "(" + ds.Tables[dsTable].Rows[0]["confuId"].ToString() + ")";
                    }
                    //FB 2501 Dec6 Start
                    int status = Int32.Parse(ds.Tables[dsTable].Rows[0]["OnlineStatus"].ToString());
                    ret = FetchPartyStatus(status, ref user);
                    if (!ret)
                    {
                        logger.Trace("Participant status is incorrect.");
                        return false;
                    }
                    //FB 2501 Dec6 End                  

                    //ZD 100697 Start -  line rate
                    if (ds.Tables[dsTable].Rows[0]["DefLineRate"].ToString() != null)
                    {
                        int iLineRate = Int32.Parse(ds.Tables[dsTable].Rows[0]["DefLineRate"].ToString());
                        ret = ConvertToMcuLineRate(iLineRate, ref user.stLineRate.etLineRate);
                        if (!ret)
                        {
                            logger.Trace("Participant line rate is incorrect.");
                            return false;
                        }
                    }
                    else
                    {
                        // default to 384 Kbps
                        user.stLineRate.etLineRate = NS_MESSENGER.LineRate.eLineRate.K384;
                    }
                    //ZD 100697 End

                    //FB 2553-RMX Starts
                    int isMessageOverlay = Int32.Parse(ds.Tables[dsTable].Rows[0]["isTextMsg"].ToString());
                    if (isMessageOverlay == 1)
                    {
                        user.bIsMessageOverlay = true;
                    }
                    else
                    {
                        user.bIsMessageOverlay = false;
                    }
                    //FB 2553-RMX Ends

                    //ZD 101056 Starts
                    int iConnectionType = 0;
                    iConnectionType = Int32.Parse(ds.Tables[dsTable].Rows[0]["connectiontype"].ToString());
                    ret = ConvertToConnectionType(iConnectionType, ref user);
                    if (!ret)
                    {
                        logger.Trace("Participant connection type is incorrect.");
                        return false;
                    }
                    //ZD 101056 Ends

					return true;
				}			
				else
				{
					// no record in the database;
					return false;
				}
			}
			catch (Exception e)
			{
				logger.Exception(100,e.Message);
				return (false);
			}
		}

		internal bool FetchGuest(int confId, int instanceId, int endpointId, ref NS_MESSENGER.Party guest)
		{
			try 
			{
				DataSet ds = new DataSet();
				string dsTable = "Guests";

                string query = "select c.bridgeid,g.email,c.ipisdnaddress,c.GUID,c.AddressType,c.OnlineStatus, c.isTextMsg, c.DefLineRate, c.UserID,c.connectiontype, c.PartyNameonMCU "; //FB 2249 FB 2501 Call Monitoring FB 2501 Dec5 //FB 2553-RMX //FB 2989 //ZD 100697 //ZD 101056
				query += " from  Conf_User_D c , Usr_GuestList_D G ";
				query += " where c.confid = " + confId.ToString() ;
				query += " and c.instanceid = " + instanceId.ToString() ;
				query += " and c.userid = " + endpointId.ToString();
				query += " and c.userid = g.userid";

				bool ret = SelectCommand(query,ref ds,dsTable);
				if (!ret)
				{
					logger.Trace("SQL error");
					return (false);
				}
				
				if (ds.Tables[dsTable].Rows.Count > 0)
				{	
		
                    int.TryParse(ds.Tables[dsTable].Rows[0]["UserID"].ToString(), out guest.iEndpointId); //FB 2989
					//bridgeid 
					int bridgeId = Int32.Parse(ds.Tables[dsTable].Rows[0][0].ToString());

                    // fetch the Bridge info
                    bool ret2 = FetchMcu(bridgeId, ref guest.cMcu);
                    if (!ret2)
                    {
                        logger.Exception(100, "Error in fetching MCU info");
                        return (false);
                    }

                    //guest name
                    guest.sName = ds.Tables[dsTable].Rows[0][1].ToString();

					guest.sMcuName=ds.Tables[dsTable].Rows[0]["PartyNameonMCU"].ToString();//ZD 101056
                    // update type
                    guest.etType = NS_MESSENGER.Party.eType.GUEST;

                    if (ds.Tables[dsTable].Rows[0]["ipisdnaddress"] != null)//FB 2249
                        guest.sAddress = ds.Tables[dsTable].Rows[0]["ipisdnaddress"].ToString().Trim();

                    guest.sGUID = ds.Tables[dsTable].Rows[0]["GUID"].ToString().Trim();//FB 2501 Call Monitoring

                    //FB 2501 Dec5 start
                    //address type
                    int iAddressType = Int32.Parse(ds.Tables[dsTable].Rows[0]["AddressType"].ToString());
                    ret = ConvertToMcuAddressType(iAddressType, ref guest);
                    if (!ret)
                    {
                        logger.Trace("Participant address type is incorrect.");
                        return false;
                    }
                    //FB 2501 Dec5 End

                    //FB 2501 Dec6 Start
                    int status = Int32.Parse(ds.Tables[dsTable].Rows[0]["OnlineStatus"].ToString());
                    ret = FetchPartyStatus(status, ref guest);
                    if (!ret)
                    {
                        logger.Trace("Participant status is incorrect.");
                        return false;
                    }
                    //FB 2501 Dec6 End

                    //ZD 100697 Start line rate
                    if (ds.Tables[dsTable].Rows[0]["DefLineRate"].ToString() != null)
                    {
                        int iLineRate = Int32.Parse(ds.Tables[dsTable].Rows[0]["DefLineRate"].ToString());
                        ret = ConvertToMcuLineRate(iLineRate, ref guest.stLineRate.etLineRate);
                        if (!ret)
                        {
                            logger.Trace("Participant line rate is incorrect.");
                            return false;
                        }
                    }
                    else
                    {
                        // default to 384 Kbps
                        guest.stLineRate.etLineRate = NS_MESSENGER.LineRate.eLineRate.K384;
                    }
                    //ZD 100697 End

                    //FB 2553-RMX Starts
                    int isMessageOverlay = Int32.Parse(ds.Tables[dsTable].Rows[0]["isTextMsg"].ToString());
                    if (isMessageOverlay == 1)
                    {
                        guest.bIsMessageOverlay = true;
                    }
                    else
                    {
                        guest.bIsMessageOverlay = false;
                    }
                    //FB 2553-RMX Ends

                    //ZD 101056 Starts
                    int iConnectionType = 0;
                    iConnectionType = Int32.Parse(ds.Tables[dsTable].Rows[0]["connectiontype"].ToString());
                    ret = ConvertToConnectionType(iConnectionType, ref guest);
                    if (!ret)
                    {
                        logger.Trace("Participant connection type is incorrect.");
                        return false;
                    }
                    //ZD 101056 Ends


					return true;
				}
				else
				{
					// no record in the database
					return false;
				}
			}
			catch (Exception e)
			{
				logger.Exception(100,e.Message);				
				return (false);
			}
		}

        internal bool FetchCascadeLink(int confId, int instanceId, int endpointId, ref NS_MESSENGER.Party cascade)
        {
            try
            {
                DataSet ds = new DataSet();
                string dsTable = "CascadeLink";

                string query = "select cas.bridgeid, cas.cascadelinkname, cas.GUID, cas.AddressType, cas.OnlineStatus, cas.uid"; //FB 2501 Call Monitoring //FB 2501 Dec //FB 2989
                query += " from  Conf_Cascade_D cas";
                query += " where cas.uid = " + endpointId.ToString();

                bool ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Trace("SQL error");
                    return (false);
                }

                if (ds.Tables[dsTable].Rows.Count > 0)
                {
                    int.TryParse(ds.Tables[dsTable].Rows[0]["uid"].ToString(), out cascade.iEndpointId); //FB 2989

                    //bridgeid 
                    int bridgeId = Int32.Parse(ds.Tables[dsTable].Rows[0][0].ToString());

                    // Fetch the Bridge info
                    bool ret2 = FetchMcu(bridgeId, ref cascade.cMcu);
                    if (!ret2)
                    {
                        logger.Exception(100, "Error in fetching MCU info");
                        return (false);
                    }
                    
                    // cascade link name
                    cascade.sName = ds.Tables[dsTable].Rows[0][1].ToString();

                    // update type
                    cascade.etType = NS_MESSENGER.Party.eType.CASCADE_LINK;

                    if (ds.Tables[dsTable].Rows[0]["GUID"] != null)
                        cascade.sGUID = ds.Tables[dsTable].Rows[0]["GUID"].ToString().Trim();//FB 2501 Call Monitoring

                    //FB 2501 Dec5 start
                    //address type
                    int iAddressType = Int32.Parse(ds.Tables[dsTable].Rows[0]["AddressType"].ToString());
                    ret = ConvertToMcuAddressType(iAddressType, ref cascade);
                    if (!ret)
                    {
                        logger.Trace("Participant address type is incorrect.");
                        return false;
                    }
                    //FB 2501 Dec5 End

                    //FB 2501 Dec6 Start
                    int status = Int32.Parse(ds.Tables[dsTable].Rows[0]["OnlineStatus"].ToString());
                    ret = FetchPartyStatus(status, ref cascade);
                    if (!ret)
                    {
                        logger.Trace("Participant status is incorrect.");
                        return false;
                    }
                    //FB 2501 Dec6 End


                    return true;
                }
                else
                {
                    // no record in the database
                    return false;
                }
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return (false);
            }
        }

		internal bool FetchCascadeLinks(int iConfId, int iInstanceId, ref Queue qParty)
		{

			DataSet ds = new DataSet();
			string dsTable = "CascadeLinks";
			
			string sQuery = "select bridgeid from Conf_Cascade_D";
			sQuery += " where  confid = " + iConfId.ToString() ;
			sQuery += " and instanceid = " + iInstanceId.ToString() ;
			
			bool ret = SelectCommand(sQuery,ref ds,dsTable);
			if (!ret)
			{
				logger.Trace("SQL error");
				return (false);
			}
			
			for (int i=0;i<ds.Tables[dsTable].Rows.Count ; i++)
			{
				NS_MESSENGER.Party party = new NS_MESSENGER.Party();

				// bridgeid
				int iBridgeId = Int32.Parse(ds.Tables[dsTable].Rows[i][8].ToString());

				// Fetch the Bridge info  
				bool ret4 = FetchMcu(iBridgeId,ref party.cMcu);
				if (!ret4)
				{
					logger.Exception(100,"Error in fetching MCU info");
					return (false);
				}

				//add the participant to the queue.
				qParty.Enqueue(party);
			}

			return true;
		}
		
		internal bool FetchMcu(int mcuId,ref NS_MESSENGER.MCU cMcu)
		{
			try
			{
				// Retreive  the  bridge record
				DataSet ds = new DataSet();
				string dsTable = "MCU";
                string sQuery = "Select * from Mcu_List_D where bridgeid = '" + mcuId.ToString() + "'";
				bool ret = SelectCommand(sQuery,ref ds,dsTable);
				if (!ret)
				{
					logger.Exception(100,"SQL error");
					return (false);
				}	
				
				if (ds.Tables[dsTable].Rows.Count < 1)
				{
					logger.Trace ("No bridge found...");
					return false;
				}

                //FB 1642 - DTMF Commented for FB 2655
                //cMcu.preConfCode = ds.Tables[dsTable].Rows[0]["PreConfCode"].ToString().Trim();
                //cMcu.preLPin = ds.Tables[dsTable].Rows[0]["PreLeaderPin"].ToString().Trim();
                //cMcu.postLPin = ds.Tables[dsTable].Rows[0]["PostLeaderPin"].ToString().Trim();

                // id
				cMcu.iDbId = mcuId;
                
                // login
				cMcu.sLogin = ds.Tables[dsTable].Rows[0]["BridgeLogin"].ToString().Trim();
                
                // pwd
				cMcu.sPwd = ds.Tables[dsTable].Rows[0]["BridgePassword"].ToString().Trim();				
				
                // decrypt mcu pwd 
                if (cMcu.sPwd != null && cMcu.sPwd.Length > 0)
                {
                    cryptography.Crypto crypto = new Crypto();
                    cMcu.sPwd = crypto.decrypt(cMcu.sPwd);
                }
                // ip address
				cMcu.sIp = ds.Tables[dsTable].Rows[0]["BridgeAddress"].ToString().Trim();
                
                // name
				cMcu.sName = ds.Tables[dsTable].Rows[0]["BridgeName"].ToString();				
				logger.Trace("MCU Name = " + cMcu.sName);
                
                // timezone id 
                cMcu.iTimezoneID = Int32.Parse(ds.Tables[dsTable].Rows[0]["Timezone"].ToString().Trim());
                logger.Trace("MCU timezone ID : " + cMcu.iTimezoneID.ToString());

                // server name
                cMcu.sStation = System.Environment.MachineName;

                // s/w ver
                cMcu.sSoftwareVer = ds.Tables[dsTable].Rows[0]["SoftwareVer"].ToString().Trim();

                // concurrent audio calls
                cMcu.iMaxConcurrentAudioCalls = Int32.Parse(ds.Tables[dsTable].Rows[0]["maxConcurrentAudioCalls"].ToString().Trim());

                // concurrent video calls
                cMcu.iMaxConcurrentVideoCalls = Int32.Parse(ds.Tables[dsTable].Rows[0]["maxConcurrentVideoCalls"].ToString().Trim());

                // Port address fo MCu API port FOR mCU
                int iHttpPort = 80;
                if(ds.Tables[dsTable].Rows[0]["APIPortNo"].ToString() != "")
                    Int32.TryParse(ds.Tables[dsTable].Rows[0]["APIPortNo"].ToString().Trim(), out iHttpPort);

                if (iHttpPort <= 0)
                    iHttpPort = 80;

                cMcu.iHttpPort = iHttpPort;

                // ZD 100113 Start
                int iURLAccess = 0;
                if (ds.Tables[dsTable].Rows[0]["URLAccess"].ToString() != "")
                    Int32.TryParse(ds.Tables[dsTable].Rows[0]["URLAccess"].ToString().Trim(), out iURLAccess);

                cMcu.iURLAccess = iURLAccess;
                // ZD 100113 End

                //FB 1766 - IVR Settings - start
                int enableIVR = 0;
                if (ds.Tables[dsTable].Rows[0]["EnableIVR"].ToString() != "")
                    Int32.TryParse(ds.Tables[dsTable].Rows[0]["EnableIVR"].ToString().Trim(), out enableIVR);

                if (enableIVR < 0)
                    enableIVR = 0;

                cMcu.iEnableIVR = enableIVR;

                cMcu.sIVRServiceName = ds.Tables[dsTable].Rows[0]["IVRServiceName"].ToString();

                //FB 1766 - IVR Settings - end

                //FB 1907 - Recording Settings - start
                int enableRecording = 0;
                if (ds.Tables[dsTable].Rows[0]["EnableRecording"].ToString() != "")
                    Int32.TryParse(ds.Tables[dsTable].Rows[0]["EnableRecording"].ToString().Trim(), out enableRecording);

                if (enableRecording < 0)
                    enableRecording = 0;

                cMcu.iEnableRecording = enableRecording;

                int eLPR= 0;
                if (ds.Tables[dsTable].Rows[0]["LPR"].ToString() != "")
                    Int32.TryParse(ds.Tables[dsTable].Rows[0]["LPR"].ToString().Trim(), out eLPR);

                if (eLPR < 0)
                    eLPR = 0;

                cMcu.iLPR = eLPR;

                //FB 1907 - Recording Settings - end

                //FB 2003 - ISDN Prefix - start
                string audioPrefix = "";
                audioPrefix = ds.Tables[dsTable].Rows[0]["ISDNAudioPrefix"].ToString().Trim();
                cMcu.iISDNAudioPrefix = audioPrefix;

                string videoPrefix = "";
                videoPrefix = ds.Tables[dsTable].Rows[0]["ISDNVideoPrefix"].ToString().Trim();
                cMcu.iISDNVideoPrefix = videoPrefix;

                if (ds.Tables[dsTable].Rows[0]["ISDNGateway"] != null)//MSE 8000
                    cMcu.sisdnGateway = ds.Tables[dsTable].Rows[0]["ISDNGateway"].ToString().Trim();
                //FB 2003 - ISDN Prefix - end
                //FB 2016 start
                int confServiceID = 71;
                if (ds.Tables[dsTable].Rows[0]["ConfServiceID"] != null)//MSE 8000
                    Int32.TryParse(ds.Tables[dsTable].Rows[0]["ConfServiceID"].ToString().Trim(), out confServiceID);

                cMcu.ConfServiceID = confServiceID;
                logger.Trace("ConfServiceID = " + cMcu.ConfServiceID.ToString());
                //FB 2016 end

                //ZD 100768
                cMcu.sBridgeExtNo = "";
                if (ds.Tables[dsTable].Columns.Contains("BridgeExtNo") && ds.Tables[dsTable].Rows[0]["BridgeExtNo"] != null)
                    cMcu.sBridgeExtNo = ds.Tables[dsTable].Rows[0]["BridgeExtNo"].ToString();

                //ZD 100522
                cMcu.sBridgeDomain = "";
                if (ds.Tables[dsTable].Columns.Contains("BridgeDomain") && ds.Tables[dsTable].Rows[0]["BridgeDomain"] != null)
                    cMcu.sBridgeDomain = ds.Tables[dsTable].Rows[0]["BridgeDomain"].ToString();

                //ZD 104256 Starts
                int PoolOrderID = 0;
                if (ds.Tables[dsTable].Rows[0]["PoolOrderID"] != null)
                    Int32.TryParse(ds.Tables[dsTable].Rows[0]["PoolOrderID"].ToString().Trim(), out PoolOrderID);

                cMcu.iPoolOrder = PoolOrderID;
                logger.Trace("PoolOrderID = " + cMcu.iPoolOrder.ToString());
                //ZD 104256 Ends

                //FB 2441 II Starts
                if (cMcu.ConfServiceID > 0)
                {
                    DataSet ds1 = new DataSet();
                    string dsTable1 = "MCUProfile";
                    string Query = " Select MCUId, profileName from MCU_Profiles_D where MCUId = " + mcuId + " and profileId = " + cMcu.ConfServiceID + "";
                    ret = SelectCommand(Query, ref ds1, dsTable1);
                    if (!ret)
                    {
                        logger.Exception(100, "SQL error");
                        return (false);
                    }

                    if (ds1.Tables[dsTable1].Rows.Count > 0)
                    {
                        string profileName = "";
                        profileName = ds1.Tables[dsTable1].Rows[0]["ProfileName"].ToString().Trim();
                        cMcu.sTemplateName = profileName;
                        logger.Trace("DMA MCU Template Name = " + profileName);
                    }
                }
                //FB 2441 II End\

                //ZD 104256 Starts
                if (cMcu.iPoolOrder > 0)
                {
                    DataSet ds1 = new DataSet();
                    string dsTable1 = "MCUProfile";
                    string Query = " Select MCUId, PoolOrderName from MCU_PoolOrders_D where MCUId = " + mcuId + " and PoolOrderId = " + cMcu.iPoolOrder + "";
                    ret = SelectCommand(Query, ref ds1, dsTable1);
                    if (!ret)
                    {
                        logger.Exception(100, "SQL error");
                        return (false);
                    }

                    if (ds1.Tables[dsTable1].Rows.Count > 0)
                    {
                        string poolOrder = "";
                        poolOrder = ds1.Tables[dsTable1].Rows[0]["PoolOrderName"].ToString().Trim();
                        cMcu.sPoolOrderName = poolOrder;
                        logger.Trace("DMA MCU PoolOrder Name = " + poolOrder);
                    }
                }
                //ZD 104256 Ends

                // mcu type
				int iMcuType = 0;
				iMcuType = Int32.Parse(ds.Tables[dsTable].Rows[0]["BridgeTypeId"].ToString().Trim());
                cMcu.sMcuType = iMcuType; //FB 2448
				logger.Trace("MCU Type = " + iMcuType.ToString());

                int orgId = 0; //FB 2556
                if (ds.Tables[dsTable].Rows[0]["orgId"] != null)
                    int.TryParse(ds.Tables[dsTable].Rows[0]["orgId"].ToString(), out orgId);
                cMcu.iOrgId = orgId; //ZD 101527

                //FB 2441 Starts

                int iSendMail = 0;
                int.TryParse(ds.Tables[dsTable].Rows[0]["DMASendMail"].ToString().Trim(),out iSendMail);
                cMcu.iDMASendmail = iSendMail;
                

                cMcu.sDMADomain = ds.Tables[dsTable].Rows[0]["DMADomain"].ToString().Trim();
                
                // FB 2441 II Starts
                cMcu.sRPRMDomain = ds.Tables[dsTable].Rows[0]["RPRMDomain"].ToString().Trim();
                // FB 2441 II Ends
                cMcu.sDMAip = ds.Tables[dsTable].Rows[0]["DMAURL"].ToString().Trim();
                

                cMcu.sDMALogin = ds.Tables[dsTable].Rows[0]["DMALogin"].ToString().Trim();
                logger.Trace("DMA MCU Login=" + cMcu.sDMALogin);

                cMcu.sDMAPassword = ds.Tables[dsTable].Rows[0]["DMAPassword"].ToString().Trim();
                if (cMcu.sDMAPassword != null && cMcu.sDMAPassword.Length > 0)
                {
                    cryptography.Crypto crypto = new Crypto();
                    cMcu.sDMAPassword = crypto.decrypt(cMcu.sDMAPassword);
                }

                int HttpPort = 0;
                int.TryParse(ds.Tables[dsTable].Rows[0]["DMAPort"].ToString().Trim(), out HttpPort);
                cMcu.iDMAHttpport = HttpPort;

                int AdminId = 0;
                int.TryParse(ds.Tables[dsTable].Rows[0]["Admin"].ToString().Trim(), out AdminId);
                cMcu.iAdminID = AdminId;

                DataSet ds2 = new DataSet();
                string dsTable2 = "MCUAdmin";
                string query2 = " Select Email, FirstName + ' ' + LastName as HostName from Usr_List_D where userid = '" + AdminId + "'";
                bool ret3 = SelectCommand(query2, ref ds2, dsTable2);
                if (!ret3)
                {
                    logger.Exception(100, "SQL error");
                }

                if (ds2.Tables[dsTable2].Rows.Count < 1)
                {
                    // no record 
                    logger.Trace("No Admin email found.");
                }

                cMcu.sAdminEmail = ds2.Tables[dsTable2].Rows[0]["Email"].ToString();
                cMcu.sAdminName = ds2.Tables[dsTable2].Rows[0]["HostName"].ToString();

                int.TryParse(ds.Tables[dsTable].Rows[0]["Synchronous"].ToString().Trim(), out HttpPort); //use same variable httpport to hold temporary value
                cMcu.iSynchronous = HttpPort;
                //FB 2689 start
                string sDMADialinprefix = "";
                sDMADialinprefix = ds.Tables[dsTable].Rows[0]["Dialinprefix"].ToString().Trim();
                cMcu.sDMADialinprefix = sDMADialinprefix;
                //FB 2689 end
                logger.Trace("DMA iSynchronous =" + cMcu.iSynchronous);

                //FB 2441 Ends

                //FB 2709
                cMcu.sRPRMLogin = ds.Tables[dsTable].Rows[0]["LoginName"].ToString().Trim();

                int logincount = 0;
                int.TryParse(ds.Tables[dsTable].Rows[0]["loginCount"].ToString().Trim(), out logincount);
                cMcu.ilogincount = logincount;

                cMcu.sRPRMEmailaddress = ds.Tables[dsTable].Rows[0]["RPRMEmailaddress"].ToString().Trim();
                //ALLDEV-854 Start
                int EnableAlias = 0;
                int.TryParse(ds.Tables[dsTable].Rows[0]["EnableAlias"].ToString().Trim(), out EnableAlias);
                cMcu.iEnableAlias = EnableAlias;
                cMcu.sAlias2 = ds.Tables[dsTable].Rows[0]["Alias2"].ToString();
                cMcu.sAlias3 = ds.Tables[dsTable].Rows[0]["Alias3"].ToString();
                //ALLDEV-854 End
                //ZD 101869 Starts
                //int VideoLayoutId = 1;
                //if (ds.Tables[dsTable].Rows[0]["VideoLayoutId"] != null && !string.IsNullOrWhiteSpace(ds.Tables[dsTable].Rows[0]["VideoLayoutId"].ToString()))
                //    int.TryParse(ds.Tables[dsTable].Rows[0]["VideoLayoutId"].ToString().Trim(), out VideoLayoutId);
                //cMcu.iVideoLayoutId = VideoLayoutId;

                //int FamilyLayout = 0;
                //if (ds.Tables[dsTable].Rows[0]["FamilyLayout"] != null && !string.IsNullOrWhiteSpace(ds.Tables[dsTable].Rows[0]["FamilyLayout"].ToString()))
                //    int.TryParse(ds.Tables[dsTable].Rows[0]["FamilyLayout"].ToString().Trim(), out FamilyLayout);
                //cMcu.iFamilyLayout = FamilyLayout;
                //ZD 101869 End

                //FB 2709
              
                // assign type & ver info
                switch (iMcuType)
                {
                    case 1:
                    case 2:
                    case 3:
                        {
                            // Its a Polycom ACCORD bridge. 
                            cMcu.dblSoftwareVer = Double.Parse(cMcu.sSoftwareVer);                            
					        if (cMcu.dblSoftwareVer < 7)
					        {
						        // bridge is containing firmware version 6
						        cMcu.etType = NS_MESSENGER.MCU.eType.ACCORDv6;
					        }
					        else
					        {
						        // bridge is containing firmware version 7 & above
						        cMcu.etType = NS_MESSENGER.MCU.eType.ACCORDv7;
					        }
                            break;
				        }
                    //----------------------------------------------------------------//
                    case 4:
                    case 5:
                    case 6:                    
        				{
                            // Its a Codian bridge.
                            //cMcu.dblSoftwareVer = Double.Parse(cMcu.sSoftwareVer);
                            cMcu.etType = NS_MESSENGER.MCU.eType.CODIAN;
                            break;
                        }
                    //----------------------------------------------------------------//
                    case 7:
                        {
                            // Its a Tandberg bridge.
                            cMcu.dblSoftwareVer = 0.0;
                            cMcu.etType = NS_MESSENGER.MCU.eType.TANDBERG;
                            break;
                        }
                    //ZD 103787- It is for Polycom RMX MCU
                    case 17:
                    case 18:
                    case 19:
                    case 8:
                        {
                            // Polycom RMX
                            cMcu.dblSoftwareVer = 0.0;
                            cMcu.etType = NS_MESSENGER.MCU.eType.RMX;
                            break;
                        }
                    //----------------------------------------------------------------//
                    case 9:
                        {
                            // Radvision Scopia
                            cMcu.dblSoftwareVer = 0.0;
                            cMcu.etType = NS_MESSENGER.MCU.eType.RADVISION;
                            break;
                        }
                    //----------------------------------------------------------------//
                    //----------------------------------------------------------------//
                    case 10://polyCOM CMA
                        {
                            // polycom CMA
                            cMcu.dblSoftwareVer = 0.0;
                            cMcu.etType = NS_MESSENGER.MCU.eType.CMA;
                            break;
                        }
                    //----------------------------------------------------------------//
                    case 11://FB 2261
                        {
                            // life size
                            cMcu.dblSoftwareVer = 0.0;
                            cMcu.etType = NS_MESSENGER.MCU.eType.Lifesize;
                            break;
                        }
                    //FB 2501 Call Monitoring Start
                    case 12:
                        {
                            // CISCO MSE 8710
                            cMcu.dblSoftwareVer = 0.0;
                            cMcu.etType = NS_MESSENGER.MCU.eType.CISCOTP;
                            break;
                        }
                    //FB 2501 Call Monitoring End
                    //FB 2441 Starts
                    case 13:
                        {
                            // Polycom RPRM
                            cMcu.dblSoftwareVer = 0.0;
                            cMcu.etType = NS_MESSENGER.MCU.eType.RPRM;
                            break;
                        }
                    //FB 2441 Ends
                    //FB 2556
                    case 14:
                        {
                            cMcu.dblSoftwareVer = 0.0;
                            cMcu.etType = NS_MESSENGER.MCU.eType.RADVISIONIVIEW;

                            //FB 2556 Starts  
                            if (iMcuType == 14)
                            {
                                DataSet ds3 = new DataSet();
                                string dsTable3 = "MCUOrg";
                                string query3 = " Select * from MCU_Orgspecific_D where bridgeid = '" + mcuId + "' and OrgId = '" + orgId + "'";//DoubtT
                                bool ret4 = SelectCommand(query3, ref ds3, dsTable3);
                                if (!ret4)
                                {
                                    logger.Exception(100, "SQL error");
                                }

                                int ViewOrgID = 0, serviceID = 0;

                                for (int i = 0; i < ds3.Tables[dsTable3].Rows.Count; i++)
                                {
                                    if (ds3.Tables[dsTable3].Rows[i]["propertyName"].ToString().Trim().Equals("ScopiaOrgID"))
                                    {
                                        int.TryParse(ds3.Tables[dsTable3].Rows[i]["propertyValue"].ToString(), out ViewOrgID);
                                        cMcu.IViewOrgID = ViewOrgID;
                                    }
                                    if (ds3.Tables[dsTable3].Rows[i]["propertyName"].ToString().Trim().Equals("ScopiaServiceID"))
                                    {
                                        int.TryParse(ds3.Tables[dsTable3].Rows[i]["propertyValue"].ToString(), out serviceID);
                                        cMcu.ConfServiceID = serviceID;
                                    }
                                }
                            }
                            //FB 2556 Ends 

                            break;
                        }
                    //FB 2718 Starts
                    case 15:
                        {
                            // Polycom RPRM
                            cMcu.dblSoftwareVer = 0.0;
                            cMcu.etType = NS_MESSENGER.MCU.eType.TMSScheduling;
                            break;
                        }
                    //FB 2718 Ends
                    //ZD 101522 Start
                    case 16:
                        {
                            // Its a Pexip MCU
                            logger.Trace("Pexip MCU.");
                            cMcu.dblSoftwareVer = 0.0;
                            cMcu.etType = NS_MESSENGER.MCU.eType.Pexip;
                            break;
                        }
                    //ZD 101522 end
                    //ZD 104021 Start
                    case 20:
                        {
                            // Its a BJN MCU
                            cMcu.dblSoftwareVer = 0.0;
                            cMcu.etType = NS_MESSENGER.MCU.eType.BLUEJEANS;
                            break;
                        }
                    //ZD 104021 end
                    default:
                        {
                            logger.Trace("Unknown MCU type.");
                            return false;
                        }
				    }
												
				return (true);
			}
			catch(Exception e)
			{
				logger.Exception(100,e.Message);
				return false;
			}
		}

        //FB 2659 Start
        internal bool FetchCloudMcu(int mcuId, ref NS_MESSENGER.MCU cMcu, int orgID)//TBD Cloud
        {
            try
            {
                // Retreive  the  bridge record
                DataSet ds = new DataSet();
                string dsTable = "MCU";
                string sQuery = "Select TOP(1) * from Mcu_List_D where  virtualbridge = 0 and status =1 and deleted = 0";
                bool ret = SelectCommand(sQuery, ref ds, dsTable);
                if (!ret)
                {
                    logger.Exception(100, "SQL error");
                    return (false);
                }

                if (ds.Tables[dsTable].Rows.Count < 1)
                {
                    logger.Trace("No bridge found...");
                    return false;
                }

                //FB 1642 - DTMF Commented for FB 2655
                //cMcu.preConfCode = ds.Tables[dsTable].Rows[0]["PreConfCode"].ToString().Trim();
                //cMcu.preLPin = ds.Tables[dsTable].Rows[0]["PreLeaderPin"].ToString().Trim();
                //cMcu.postLPin = ds.Tables[dsTable].Rows[0]["PostLeaderPin"].ToString().Trim();

                // id
                cMcu.iDbId = (int)ds.Tables[dsTable].Rows[0]["BridgeID"];
                mcuId = cMcu.iDbId;
                // login
                cMcu.sLogin = ds.Tables[dsTable].Rows[0]["BridgeLogin"].ToString().Trim();

                // pwd
                cMcu.sPwd = ds.Tables[dsTable].Rows[0]["BridgePassword"].ToString().Trim();

                // decrypt mcu pwd 
                if (cMcu.sPwd != null && cMcu.sPwd.Length > 0)
                {
                    cryptography.Crypto crypto = new Crypto();
                    cMcu.sPwd = crypto.decrypt(cMcu.sPwd);
                }
                // ip address
                cMcu.sIp = ds.Tables[dsTable].Rows[0]["BridgeAddress"].ToString().Trim();

                // name
                cMcu.sName = ds.Tables[dsTable].Rows[0]["BridgeName"].ToString();
                logger.Trace("MCU Name = " + cMcu.sName);

                // timezone id 
                cMcu.iTimezoneID = Int32.Parse(ds.Tables[dsTable].Rows[0]["Timezone"].ToString().Trim());
                logger.Trace("MCU timezone ID : " + cMcu.iTimezoneID.ToString());

                // server name
                cMcu.sStation = System.Environment.MachineName;

                // s/w ver
                cMcu.sSoftwareVer = ds.Tables[dsTable].Rows[0]["SoftwareVer"].ToString().Trim();
                logger.Trace("MCU S/w Ver = " + cMcu.sSoftwareVer);

                // concurrent audio calls
                cMcu.iMaxConcurrentAudioCalls = Int32.Parse(ds.Tables[dsTable].Rows[0]["maxConcurrentAudioCalls"].ToString().Trim());

                // concurrent video calls
                cMcu.iMaxConcurrentVideoCalls = Int32.Parse(ds.Tables[dsTable].Rows[0]["maxConcurrentVideoCalls"].ToString().Trim());

                // Port address fo MCu API port FOR mCU
                int iHttpPort = 80;
                if (ds.Tables[dsTable].Rows[0]["APIPortNo"].ToString() != "")
                    Int32.TryParse(ds.Tables[dsTable].Rows[0]["APIPortNo"].ToString().Trim(), out iHttpPort);

                if (iHttpPort <= 0)
                    iHttpPort = 80;

                cMcu.iHttpPort = iHttpPort;

                //FB 1766 - IVR Settings - start
                int enableIVR = 0;
                if (ds.Tables[dsTable].Rows[0]["EnableIVR"].ToString() != "")
                    Int32.TryParse(ds.Tables[dsTable].Rows[0]["EnableIVR"].ToString().Trim(), out enableIVR);

                if (enableIVR < 0)
                    enableIVR = 0;

                cMcu.iEnableIVR = enableIVR;
                logger.Trace("Enable IVR = " + cMcu.iEnableIVR);

                cMcu.sIVRServiceName = ds.Tables[dsTable].Rows[0]["IVRServiceName"].ToString();
                logger.Trace("IVR Service Name = " + cMcu.sIVRServiceName);

                //FB 1766 - IVR Settings - end

                //FB 1907 - Recording Settings - start
                int enableRecording = 0;
                if (ds.Tables[dsTable].Rows[0]["EnableRecording"].ToString() != "")
                    Int32.TryParse(ds.Tables[dsTable].Rows[0]["EnableRecording"].ToString().Trim(), out enableRecording);

                if (enableRecording < 0)
                    enableRecording = 0;

                cMcu.iEnableRecording = enableRecording;
                logger.Trace("Enable Recording = " + enableRecording);

                int eLPR = 0;
                if (ds.Tables[dsTable].Rows[0]["LPR"].ToString() != "")
                    Int32.TryParse(ds.Tables[dsTable].Rows[0]["LPR"].ToString().Trim(), out eLPR);

                if (eLPR < 0)
                    eLPR = 0;

                cMcu.iLPR = eLPR;

                //FB 1907 - Recording Settings - end

                //FB 2003 - ISDN Prefix - start
                string audioPrefix = "";
                audioPrefix = ds.Tables[dsTable].Rows[0]["ISDNAudioPrefix"].ToString().Trim();
                cMcu.iISDNAudioPrefix = audioPrefix;

                string videoPrefix = "";
                videoPrefix = ds.Tables[dsTable].Rows[0]["ISDNVideoPrefix"].ToString().Trim();
                cMcu.iISDNVideoPrefix = videoPrefix;

                if (ds.Tables[dsTable].Rows[0]["ISDNGateway"] != null)//MSE 8000
                    cMcu.sisdnGateway = ds.Tables[dsTable].Rows[0]["ISDNGateway"].ToString().Trim();
                //FB 2003 - ISDN Prefix - end
                //FB 2016 start
                int confServiceID = 71;
                if (ds.Tables[dsTable].Rows[0]["ConfServiceID"] != null)//MSE 8000
                    Int32.TryParse(ds.Tables[dsTable].Rows[0]["ConfServiceID"].ToString().Trim(), out confServiceID);

                cMcu.ConfServiceID = confServiceID;
                logger.Trace("ConfServiceID = " + cMcu.ConfServiceID.ToString());
                //FB 2016 end

                // mcu type
                int iMcuType = 0;
                iMcuType = Int32.Parse(ds.Tables[dsTable].Rows[0]["BridgeTypeId"].ToString().Trim());
                cMcu.sMcuType = iMcuType; //FB 2448
                logger.Trace("MCU Type = " + iMcuType.ToString());

                int orgId = 0;
                if (ds.Tables[dsTable].Rows[0]["orgId"] != null)
                    int.TryParse(ds.Tables[dsTable].Rows[0]["orgId"].ToString(), out orgId);


                //ZD 100768
                cMcu.sBridgeExtNo = "";
                if (ds.Tables[dsTable].Columns.Contains("BridgeExtNo") && ds.Tables[dsTable].Rows[0]["BridgeExtNo"] != null)
                    cMcu.sBridgeExtNo = ds.Tables[dsTable].Rows[0]["BridgeExtNo"].ToString();

                //FB 2556 Starts  
                if (iMcuType == 14)
                {
                    DataSet ds3 = new DataSet();
                    string dsTable3 = "MCUOrg";
                    string query3 = " Select * from MCU_Orgspecific_D where bridgeid = '" + mcuId + "' and orgId = '" + orgID + "'"; //FB 2879
                    bool ret4 = SelectCommand(query3, ref ds3, dsTable3);
                    if (!ret4)
                    {
                        logger.Exception(100, "SQL error");
                    }

                    int ViewOrgID = 0, serviceID = 0;

                    for (int i = 0; i < ds3.Tables[dsTable3].Rows.Count; i++)
                    {
                        if (ds3.Tables[dsTable3].Rows[i]["propertyName"].ToString().Trim().Equals("ScopiaOrgID"))
                        {
                            int.TryParse(ds3.Tables[dsTable3].Rows[i]["propertyValue"].ToString(), out ViewOrgID);
                            cMcu.IViewOrgID = ViewOrgID;
                        }
                        if (ds3.Tables[dsTable3].Rows[i]["propertyName"].ToString().Trim().Equals("ScopiaServiceID"))
                        {
                            int.TryParse(ds3.Tables[dsTable3].Rows[i]["propertyValue"].ToString(), out serviceID);
                            cMcu.ConfServiceID = serviceID;
                        }
                    }
                }
                //FB 2556 Ends 

                //FB 2441 Starts

                int iSendMail = 0;
                int.TryParse(ds.Tables[dsTable].Rows[0]["DMASendMail"].ToString().Trim(), out iSendMail);
                cMcu.iDMASendmail = iSendMail;
                logger.Trace("DMA MCU SendMail = " + iSendMail.ToString());

                cMcu.sDMADomain = ds.Tables[dsTable].Rows[0]["DMADomain"].ToString().Trim();
                logger.Trace("DMA MCU Domain=" + cMcu.sDMADomain);


                cMcu.sDMAip = ds.Tables[dsTable].Rows[0]["DMAURL"].ToString().Trim();
                logger.Trace("DMA MCU URL=" + cMcu.sDMAip);

                cMcu.sDMALogin = ds.Tables[dsTable].Rows[0]["DMALogin"].ToString().Trim();
                logger.Trace("DMA MCU Login=" + cMcu.sDMALogin);

                cMcu.sDMAPassword = ds.Tables[dsTable].Rows[0]["DMAPassword"].ToString().Trim();
                if (cMcu.sDMAPassword != null && cMcu.sDMAPassword.Length > 0)
                {
                    cryptography.Crypto crypto = new Crypto();
                    cMcu.sDMAPassword = crypto.decrypt(cMcu.sDMAPassword);
                }

                logger.Trace("DMA MCU Password=" + cMcu.sDMAPassword);

                int HttpPort = 0;
                int.TryParse(ds.Tables[dsTable].Rows[0]["DMAPort"].ToString().Trim(), out HttpPort);
                cMcu.iDMAHttpport = HttpPort;
                logger.Trace("DMA MCU Port=" + cMcu.iDMAHttpport);

                int AdminId = 0;
                int.TryParse(ds.Tables[dsTable].Rows[0]["Admin"].ToString().Trim(), out AdminId);
                cMcu.iAdminID = AdminId;

                DataSet ds2 = new DataSet();
                string dsTable2 = "MCUAdmin";
                string query2 = " Select Email, FirstName + ' ' + LastName as HostName from Usr_List_D where userid = '" + AdminId + "'";
                bool ret3 = SelectCommand(query2, ref ds2, dsTable2);
                if (!ret3)
                {
                    logger.Exception(100, "SQL error");
                }

                if (ds2.Tables[dsTable2].Rows.Count < 1)
                {
                    // no record 
                    logger.Trace("No Admin email found.");
                }

                cMcu.sAdminEmail = ds2.Tables[dsTable2].Rows[0]["Email"].ToString();
                cMcu.sAdminName = ds2.Tables[dsTable2].Rows[0]["HostName"].ToString();

                int.TryParse(ds.Tables[dsTable].Rows[0]["Synchronous"].ToString().Trim(), out HttpPort); //use same variable httpport to hold temporary value
                cMcu.iSynchronous = HttpPort;
                logger.Trace("DMA iSynchronous =" + cMcu.iSynchronous);

                //FB 2441 Ends



                // assign type & ver info
                switch (iMcuType)
                {
                    case 1:
                    case 2:
                    case 3:
                        {
                            // Its a Polycom ACCORD bridge. 
                            cMcu.dblSoftwareVer = Double.Parse(cMcu.sSoftwareVer);
                            if (cMcu.dblSoftwareVer < 7)
                            {
                                // bridge is containing firmware version 6
                                cMcu.etType = NS_MESSENGER.MCU.eType.ACCORDv6;
                            }
                            else
                            {
                                // bridge is containing firmware version 7 & above
                                cMcu.etType = NS_MESSENGER.MCU.eType.ACCORDv7;
                            }
                            break;
                        }
                    //----------------------------------------------------------------//
                    case 4:
                    case 5:
                    case 6:
                        {
                            // Its a Codian bridge.
                            //cMcu.dblSoftwareVer = Double.Parse(cMcu.sSoftwareVer);
                            cMcu.etType = NS_MESSENGER.MCU.eType.CODIAN;
                            break;
                        }
                    //----------------------------------------------------------------//
                    case 7:
                        {
                            // Its a Tandberg bridge.
                            cMcu.dblSoftwareVer = 0.0;
                            cMcu.etType = NS_MESSENGER.MCU.eType.TANDBERG;
                            break;
                        }
                    //ZD 103787- It is for polycom RMX MCU
                    case 17:
                    case 18:
                    case 19:
                    case 8:
                        {
                            // Polycom RMX
                            cMcu.dblSoftwareVer = 0.0;
                            cMcu.etType = NS_MESSENGER.MCU.eType.RMX;
                            break;
                        }
                    //----------------------------------------------------------------//
                    case 9:
                        {
                            // Radvision Scopia
                            cMcu.dblSoftwareVer = 0.0;
                            cMcu.etType = NS_MESSENGER.MCU.eType.RADVISION;
                            break;
                        }
                    //----------------------------------------------------------------//
                    //----------------------------------------------------------------//
                    case 10://polyCOM CMA
                        {
                            // polycom CMA
                            cMcu.dblSoftwareVer = 0.0;
                            cMcu.etType = NS_MESSENGER.MCU.eType.CMA;
                            break;
                        }
                    //----------------------------------------------------------------//
                    case 11://FB 2261
                        {
                            // life size
                            cMcu.dblSoftwareVer = 0.0;
                            cMcu.etType = NS_MESSENGER.MCU.eType.Lifesize;
                            break;
                        }
                    //FB 2501 Call Monitoring Start
                    case 12:
                        {
                            // CISCO MSE 8710
                            cMcu.dblSoftwareVer = 0.0;
                            cMcu.etType = NS_MESSENGER.MCU.eType.CISCOTP;
                            break;
                        }
                    //FB 2501 Call Monitoring End
                    //FB 2441 Starts
                    case 13:
                        {
                            // Polycom RPRM
                            cMcu.dblSoftwareVer = 0.0;
                            cMcu.etType = NS_MESSENGER.MCU.eType.RPRM;
                            break;
                        }
                    //FB 2441 Ends
                    //FB 2556
                    case 14:
                        {
                            cMcu.dblSoftwareVer = 0.0;
                            cMcu.etType = NS_MESSENGER.MCU.eType.RADVISIONIVIEW;


                            //FB 2556 Starts  
                            if (iMcuType == 14)
                            {
                                DataSet ds3 = new DataSet();
                                string dsTable3 = "MCUOrg";
                                string query3 = " Select * from MCU_Orgspecific_D where bridgeid = '" + mcuId + "' and OrgId = '" + orgID + "'";//FB 2879
                                bool ret4 = SelectCommand(query3, ref ds3, dsTable3);
                                if (!ret4)
                                {
                                    logger.Exception(100, "SQL error");
                                }

                                int ViewOrgID = 0, serviceID = 0;

                                for (int i = 0; i < ds3.Tables[dsTable3].Rows.Count; i++)
                                {
                                    if (ds3.Tables[dsTable3].Rows[i]["propertyName"].ToString().Trim().Equals("ScopiaOrgID"))
                                    {
                                        int.TryParse(ds3.Tables[dsTable3].Rows[i]["propertyValue"].ToString(), out ViewOrgID);
                                        cMcu.IViewOrgID = ViewOrgID;
                                    }
                                    if (ds3.Tables[dsTable3].Rows[i]["propertyName"].ToString().Trim().Equals("ScopiaServiceID"))
                                    {
                                        int.TryParse(ds3.Tables[dsTable3].Rows[i]["propertyValue"].ToString(), out serviceID);
                                        cMcu.ConfServiceID = serviceID;
                                    }
                                }
                            }
                            //FB 2556 Ends 
                            break;
                        }
                    //ZD 104021 Starts
                    case 20:
                        {
                            cMcu.dblSoftwareVer = 0.0;
                            cMcu.etType = NS_MESSENGER.MCU.eType.BLUEJEANS;
                            break;
                        }
                    //ZD 104021 Ends
                    default:
                        {
                            logger.Trace("Unknown MCU type.");
                            return false;
                        }
                }

                return (true);
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        //FB 2659 End

        internal bool FetchAllMcu(ref List<NS_MESSENGER.MCU> cMcuList)// FB 2501
        {
            NS_MESSENGER.MCU cMcu = null;
            try
            {
                // Retreive  the  bridge record
                DataSet ds = new DataSet();
                string dsTable = "MCU";
                string sQuery = "Select * from Mcu_List_D where  virtualbridge = 0 and EnablePollFailure = 1 and deleted = 0"; //ZD 100369_MCU
                bool ret = SelectCommand(sQuery, ref ds, dsTable);
                if (!ret)
                {
                    logger.Exception(100, "SQL error");
                    return (false);
                }

                if (ds.Tables[dsTable].Rows.Count < 1)
                {
                    logger.Trace("No bridge found...");
                    return false;
                }

               for (int i = 0; i < ds.Tables[dsTable].Rows.Count; i++)
                {
                    cMcu = new NS_MESSENGER.MCU();

                    //FB 1642 - DTMF Commented for FB 2655
                    //cMcu.preConfCode = ds.Tables[dsTable].Rows[i]["PreConfCode"].ToString().Trim();
                    //cMcu.preLPin = ds.Tables[dsTable].Rows[i]["PreLeaderPin"].ToString().Trim();
                    //cMcu.postLPin = ds.Tables[dsTable].Rows[i]["PostLeaderPin"].ToString().Trim();
                    // id
                    cMcu.iDbId = Int32.Parse(ds.Tables[dsTable].Rows[i]["bridgeid"].ToString().Trim());

                    // login
                    cMcu.sLogin = ds.Tables[dsTable].Rows[i]["BridgeLogin"].ToString().Trim();

                    // pwd
                    cMcu.sPwd = ds.Tables[dsTable].Rows[i]["BridgePassword"].ToString().Trim();

                    // decrypt mcu pwd 
                    if (cMcu.sPwd != null && cMcu.sPwd.Length > 0)
                    {
                        cryptography.Crypto crypto = new Crypto();
                        cMcu.sPwd = crypto.decrypt(cMcu.sPwd);
                    }
                    // ip address
                    cMcu.sIp = ds.Tables[dsTable].Rows[i]["BridgeAddress"].ToString().Trim();

                    // name
                    cMcu.sName = ds.Tables[dsTable].Rows[i]["BridgeName"].ToString();
                    logger.Trace("MCU Name = " + cMcu.sName);

                    // timezone id 
                    cMcu.iTimezoneID = Int32.Parse(ds.Tables[dsTable].Rows[i]["Timezone"].ToString().Trim());
                    logger.Trace("MCU timezone ID : " + cMcu.iTimezoneID.ToString());

                    // server name
                    cMcu.sStation = System.Environment.MachineName;

                    // s/w ver
                    cMcu.sSoftwareVer = ds.Tables[dsTable].Rows[i]["SoftwareVer"].ToString().Trim();
                    logger.Trace("MCU S/w Ver = " + cMcu.sSoftwareVer);

                    // concurrent audio calls
                    cMcu.iMaxConcurrentAudioCalls = Int32.Parse(ds.Tables[dsTable].Rows[i]["maxConcurrentAudioCalls"].ToString().Trim());

                    // concurrent video calls
                    cMcu.iMaxConcurrentVideoCalls = Int32.Parse(ds.Tables[dsTable].Rows[i]["maxConcurrentVideoCalls"].ToString().Trim());

                    // Port address fo MCu API port FOR mCU
                    int iHttpPort = 80;
                    if (ds.Tables[dsTable].Rows[i]["APIPortNo"].ToString() != "")
                        Int32.TryParse(ds.Tables[dsTable].Rows[i]["APIPortNo"].ToString().Trim(), out iHttpPort);

                    if (iHttpPort <= 0)
                        iHttpPort = 80;

                    cMcu.iHttpPort = iHttpPort;

                    //ZD 100113 Start
                    int iURLAccess = 0;
                    if (ds.Tables[dsTable].Rows[i]["URLAccess"].ToString() != "")
                        Int32.TryParse(ds.Tables[dsTable].Rows[i]["URLAccess"].ToString().Trim(), out iURLAccess);

                    cMcu.iURLAccess = iURLAccess;
                    //ZD 100113 End

                    //FB 1766 - IVR Settings - start
                    int enableIVR = 0;
                    if (ds.Tables[dsTable].Rows[i]["EnableIVR"].ToString() != "")
                        Int32.TryParse(ds.Tables[dsTable].Rows[i]["EnableIVR"].ToString().Trim(), out enableIVR);

                    if (enableIVR < 0)
                        enableIVR = 0;

                    cMcu.iEnableIVR = enableIVR;
                    logger.Trace("Enable IVR = " + cMcu.iEnableIVR);

                    cMcu.sIVRServiceName = ds.Tables[dsTable].Rows[i]["IVRServiceName"].ToString();
                    logger.Trace("IVR Service Name = " + cMcu.sIVRServiceName);

                    //FB 1766 - IVR Settings - end

                    //FB 1907 - Recording Settings - start
                    int enableRecording = 0;
                    if (ds.Tables[dsTable].Rows[i]["EnableRecording"].ToString() != "")
                        Int32.TryParse(ds.Tables[dsTable].Rows[i]["EnableRecording"].ToString().Trim(), out enableRecording);

                    if (enableRecording < 0)
                        enableRecording = 0;

                    cMcu.iEnableRecording = enableRecording;
                    logger.Trace("Enable Recording = " + enableRecording);

                    int eLPR = 0;
                    if (ds.Tables[dsTable].Rows[i]["LPR"].ToString() != "")
                        Int32.TryParse(ds.Tables[dsTable].Rows[i]["LPR"].ToString().Trim(), out eLPR);

                    if (eLPR < 0)
                        eLPR = 0;

                    cMcu.iLPR = eLPR;

                    //FB 1907 - Recording Settings - end

                    //FB 2003 - ISDN Prefix - start
                    string audioPrefix = "";
                    audioPrefix = ds.Tables[dsTable].Rows[i]["ISDNAudioPrefix"].ToString().Trim();
                    cMcu.iISDNAudioPrefix = audioPrefix;

                    string videoPrefix = "";
                    videoPrefix = ds.Tables[dsTable].Rows[i]["ISDNVideoPrefix"].ToString().Trim();
                    cMcu.iISDNVideoPrefix = videoPrefix;

                    if (ds.Tables[dsTable].Rows[i]["ISDNGateway"] != null)//MSE 8000
                        cMcu.sisdnGateway = ds.Tables[dsTable].Rows[i]["ISDNGateway"].ToString().Trim();
                    //FB 2003 - ISDN Prefix - end
                    //FB 2016 start
                    int confServiceID = 71;
                    if (ds.Tables[dsTable].Rows[i]["ConfServiceID"] != null)//MSE 8000
                        Int32.TryParse(ds.Tables[dsTable].Rows[i]["ConfServiceID"].ToString().Trim(), out confServiceID);

                    cMcu.ConfServiceID = confServiceID;
                    logger.Trace("ConfServiceID = " + cMcu.ConfServiceID.ToString());
                    //FB 2016 end

                    //ZD 100768
                    cMcu.sBridgeExtNo = "";
                    if (ds.Tables[dsTable].Columns.Contains("BridgeExtNo") && ds.Tables[dsTable].Rows[i]["BridgeExtNo"] != null)
                        cMcu.sBridgeExtNo = ds.Tables[dsTable].Rows[i]["BridgeExtNo"].ToString();

                    //ZD 100369_MCU Start

                    int Orgid = 11;
                    if (ds.Tables[dsTable].Rows[i]["orgId"] != null)
                        Int32.TryParse(ds.Tables[dsTable].Rows[i]["orgId"].ToString().Trim(), out Orgid);
                    cMcu.iOrgId = Orgid;

                    int PollCount = -2;
                    if (ds.Tables[dsTable].Rows[i]["PollCount"] != null)
                        Int32.TryParse(ds.Tables[dsTable].Rows[i]["PollCount"].ToString().Trim(), out PollCount);
                    cMcu.iPollCount = PollCount;

                    if (ds.Tables[dsTable].Rows[i]["MultipleAddress"] != null)
                        cMcu.sMultipleAssistant = ds.Tables[dsTable].Rows[i]["MultipleAddress"].ToString().Trim();

                    int AdminId = 0;
                    if (ds.Tables[dsTable].Rows[i]["Admin"] != null)
                        Int32.TryParse(ds.Tables[dsTable].Rows[i]["Admin"].ToString().Trim(), out AdminId);

                    DataSet ds2 = new DataSet();
                    string dsTable2 = "MCUAdmin";
                    string query2 = " Select Email from Usr_List_D where userid = '" + AdminId + "'";
                    bool ret3 = SelectCommand(query2, ref ds2, dsTable2);
                    if (!ret3)
                    {
                        logger.Exception(100, "SQL error");
                    }

                    if (ds2.Tables[dsTable2].Rows.Count < 1)
                    {
                        // no record 
                        logger.Trace("No Admin email found.");
                    }

                    cMcu.sAdminEmail = ds2.Tables[dsTable2].Rows[0]["Email"].ToString();

                    //ZD 100369_MCU End
                    // mcu type
                    int iMcuType = 0;
                    iMcuType = Int32.Parse(ds.Tables[dsTable].Rows[i]["BridgeTypeId"].ToString().Trim());
                    cMcu.sMcuType = iMcuType; //FB 2448
                    logger.Trace("MCU Type = " + iMcuType.ToString());



                    // assign type & ver info
                    switch (iMcuType)
                    {
                        case 1:
                        case 2:
                        case 3:
                            {
                                // Its a Polycom ACCORD bridge. 
                                cMcu.dblSoftwareVer = Double.Parse(cMcu.sSoftwareVer);
                                if (cMcu.dblSoftwareVer < 7)
                                {
                                    // bridge is containing firmware version 6
                                    cMcu.etType = NS_MESSENGER.MCU.eType.ACCORDv6;
                                }
                                else
                                {
                                    // bridge is containing firmware version 7 & above
                                    cMcu.etType = NS_MESSENGER.MCU.eType.ACCORDv7;
                                }
                                break;
                            }
                        //----------------------------------------------------------------//
                        case 4:
                        case 5:
                        case 6:
                            {
                                // Its a Codian bridge.
                                //cMcu.dblSoftwareVer = Double.Parse(cMcu.sSoftwareVer);
                                cMcu.etType = NS_MESSENGER.MCU.eType.CODIAN;
                                break;
                            }
                        //----------------------------------------------------------------//
                        case 7:
                            {
                                // Its a Tandberg bridge.
                                cMcu.dblSoftwareVer = 0.0;
                                cMcu.etType = NS_MESSENGER.MCU.eType.TANDBERG;
                                break;
                            }
                        //ZD 103787- It is for polycom RMX MCU
                        case 17:
                        case 18:
                        case 19:
                        case 8:
                            {
                                // Polycom RMX
                                cMcu.dblSoftwareVer = 0.0;
                                cMcu.etType = NS_MESSENGER.MCU.eType.RMX;
                                break;
                            }
                        //----------------------------------------------------------------//
                        case 9:
                            {
                                // Radvision Scopia
                                cMcu.dblSoftwareVer = 0.0;
                                cMcu.etType = NS_MESSENGER.MCU.eType.RADVISION;
                                break;
                            }
                        //----------------------------------------------------------------//
                        //----------------------------------------------------------------//
                        case 10://polyCOM CMA
                            {
                                // polycom CMA
                                cMcu.dblSoftwareVer = 0.0;
                                cMcu.etType = NS_MESSENGER.MCU.eType.CMA;
                                break;
                            }
                        //----------------------------------------------------------------//
                        case 11://FB 2261
                            {
                                // life size
                                cMcu.dblSoftwareVer = 0.0;
                                cMcu.etType = NS_MESSENGER.MCU.eType.Lifesize;
                                break;
                            }
                        case 12://FB 2261
                            {
                                // Cisco 8710
                                cMcu.dblSoftwareVer = 0.0;
                                cMcu.etType = NS_MESSENGER.MCU.eType.CISCOTP;
                                break;
                            }
                        case 13://ZD 100369_MCU
                            {
                                cMcu.dblSoftwareVer = 0.0;
                                cMcu.etType = NS_MESSENGER.MCU.eType.RPRM;
                                break;
                            }
                        case 14://FB 2556
                            {
                                cMcu.dblSoftwareVer = 0.0;
                                cMcu.etType = NS_MESSENGER.MCU.eType.RADVISIONIVIEW;
                                break;
                            }
                        case 15://ZD 100369_MCU
                            {
                                cMcu.dblSoftwareVer = 0.0;
                                cMcu.etType = NS_MESSENGER.MCU.eType.TMSScheduling;
                                break;
                            }
                        case 20://ZD 104021
                            {
                                cMcu.dblSoftwareVer = 0.0;
                                cMcu.etType = NS_MESSENGER.MCU.eType.BLUEJEANS;
                                break;
                            }
                        default:
                            {
                                logger.Trace("Unknown MCU type.");
                                return false;
                            }
                    }

                    if (cMcuList == null)
                        cMcuList = new List<NS_MESSENGER.MCU>();
                    if (cMcuList != null)
                        cMcuList.Add(cMcu);
                }

                return (true);
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }

        internal bool FetchMcuTokens(int mcuDbID, ref string mcuToken, ref string mcuUserToken, Boolean nonPolycom) //2261
        {
            int timeOut = 120; // 2 mins is the magic number - max time for polycom session
            try
            {
                // mcu tokens
                string query = "Select mcutoken,mcuusertoken,lastupdate from Mcu_Tokens_D where mcuid = " + mcuDbID.ToString();
                DataSet ds = new DataSet();
                string dsTable = "McuToken";
                bool ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                {
                    #region ONLY FOR V18. REMOVE IN V19... AUTOMATED TABLE CREATION...
                    string query1 = "CREATE TABLE [dbo].[Mcu_Tokens_D] ([McuID] int NOT NULL,[McuToken] nvarchar(50) NULL,[McuUserToken] nvarchar(50) NULL,	[LastUpdate] datetime NULL) ALTER TABLE [dbo].[Mcu_Tokens_D] ADD CONSTRAINT [PK_Mcu_Tokens_D] PRIMARY KEY CLUSTERED ([McuID] ASC)";
                    bool ret1 = NonSelectCommand(query1);
                    if (!ret1)
                    {
                        logger.Trace("SQL Mcu_Tokens_D table creation error");
                        return false;
                    }
                    #endregion

                    // now try again
                    query = "Select mcutoken,mcuusertoken,lastupdate from Mcu_Tokens_D where mcuid = " + mcuDbID.ToString();
                    ds = null;
                    dsTable = "McuToken";
                    ret = SelectCommand(query, ref ds, dsTable);
                    if (!ret)
                    {
                        logger.Trace("Error fetching mcu token data.");
                        return false;
                    }
                }

                // reset the tokens
                mcuToken = null;
                mcuUserToken = null;

                if (ds.Tables[dsTable].Rows.Count > 0)
                {
                    //check if last update is within last 3 mins
                    DateTime lastUpdate = DateTime.Parse(ds.Tables[dsTable].Rows[0]["lastupdate"].ToString());
                    TimeSpan timeDiff = DateTime.UtcNow.Subtract(lastUpdate);

                    if (nonPolycom) //2261
                        timeOut = 30;

                    if (timeDiff.TotalSeconds <= timeOut) //2261
                    {
                        mcuToken = ds.Tables[dsTable].Rows[0]["mcutoken"].ToString();
                        mcuUserToken = ds.Tables[dsTable].Rows[0]["mcuusertoken"].ToString();
                    }
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }

        internal bool InsertMcuTokens(int mcuDbId, string mcuToken, string mcuUserToken)
        {
            try
            {
                // delete existing tokens & add the new token
                string query = " delete from mcu_tokens_d where mcuid = " + mcuDbId.ToString();
                query += " insert into mcu_tokens_d values (" + mcuDbId.ToString() + ",'" + mcuToken + "','" + mcuUserToken + "',getutcdate())";
                bool ret = NonSelectCommand(query);
                if (!ret)
                {
                    logger.Trace("SQL error");
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }

        internal bool UpdateMcuTokens(int mcuDbId)
        {
            try
            {
                // update the timesnapshot
                string query = " update mcu_tokens_d set lastupdate = getutcdate() where mcuid = " + mcuDbId.ToString();
                bool ret = NonSelectCommand(query);
                if (!ret)
                {
                    logger.Trace("SQL error");
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        internal bool DeleteMcuTokens()
        {
            try
            {
                string query = "delete from mcu_tokens_d";
                bool ret = NonSelectCommand(query);
                if (!ret)
                {
                    logger.Trace("SQL error");
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
		#endregion

		#region Email Methods
		internal bool FetchSmtpServerInfo (ref NS_MESSENGER.Smtp smtp)
		{
			try
			{
				// Retreive smtp server settings
				DataSet ds = new DataSet();
				string dsTable = "SMTP";
				string query = "Select * from Sys_MailServer_D";
				bool ret = SelectCommand(query,ref ds,dsTable);
				if (!ret)
				{
					logger.Trace("SQL error");
					return (false);
				}		
				
				if (ds.Tables[dsTable].Rows.Count < 1) 
				{
					logger.Trace("No SMTP Server defined in system.");
					return false;
				}

				smtp.ServerAddress = ds.Tables[dsTable].Rows[0]["ServerAddress"].ToString().Trim(); 
				logger.Trace ("SMTP Server : " + smtp.ServerAddress);

                try
                {
                    smtp.Login = ds.Tables[dsTable].Rows[0]["Login"].ToString().Trim(); //login
                    logger.Trace("SMTP Login : " + smtp.Login);

                    //ZD 100653 Start

                    Crypto crypto = new Crypto();
                    if (ds.Tables[dsTable].Rows[0]["password"].ToString() != null && ds.Tables[dsTable].Rows[0]["password"].ToString() != "")
                        smtp.Password = crypto.decrypt(ds.Tables[dsTable].Rows[0]["password"].ToString());

                    //ZD 100653 End

                }
                catch (Exception e)
                {
                    logger.Trace("SMTP Login or Password missing.");
                }
				smtp.PortNumber = Int32.Parse(ds.Tables[dsTable].Rows[0]["PortNo"].ToString().Trim()); 
				smtp.ConnectionTimeOut = Int32.Parse(ds.Tables[dsTable].Rows[0]["ConTimeOut"].ToString().Trim()); 
				smtp.CompanyMailAddress = ds.Tables[dsTable].Rows[0]["CompanyMail"].ToString().Trim();
				smtp.DisplayName = ds.Tables[dsTable].Rows[0]["DisplayName"].ToString().Trim();
                smtp.WebsiteURL = ds.Tables[dsTable].Rows[0]["WebsiteUrl"].ToString().Trim();//Vidyo FB 2599
                smtp.RetryCount = Int32.Parse(ds.Tables[dsTable].Rows[0]["RetryCount"].ToString().Trim());//FB 2552	
				//smtp.FooterMessage = ds.Tables[dsTable].Rows[0]["MessageTemplate"].ToString().Trim(); //commented for FB 1710

				return true;
			}
			catch (Exception e)
			{
				logger.Exception(100,e.Message);	
				return false;
			}		
		}
         
		internal bool FetchNewEmails(ref Queue newEmailQueue)
		{	
			try
			{
				// Retrive all emails.
				DataSet ds = new DataSet();
				string dsTable = "Emails";
                string query = "Select [From],[To],CC,BCC,Subject,Message,UUID,RetryCount,LastRetryDateTime,Attachment,Iscalendar,orgID from Email_Queue_D "; //ICAL Fix // orgid Added for FB 1710
				bool ret = SelectCommand(query,ref ds,dsTable);
				if (!ret)
				{
					logger.Trace("SQL error");
					return (false);
				}

                for (int i = 0; i < ds.Tables[dsTable].Rows.Count; i++)
                {
                    try
                    {
                        // Retreive email components and add to email object 
                        NS_MESSENGER.Email email = new NS_MESSENGER.Email();                        
                        try
                        {
                            email.From = ds.Tables[dsTable].Rows[i]["From"].ToString().Trim();
                        }
                        catch (Exception)
                        {
                            logger.Trace("No FROM address...");
                        }                        
                        email.To = ds.Tables[dsTable].Rows[i]["To"].ToString().Trim();

                        email.CC = ds.Tables[dsTable].Rows[i]["CC"].ToString().Trim();

                        email.BCC = ds.Tables[dsTable].Rows[i]["BCC"].ToString().Trim();

                        email.Subject = ds.Tables[dsTable].Rows[i]["Subject"].ToString().Trim();

                        email.Body = ds.Tables[dsTable].Rows[i]["Message"].ToString().Trim();

                        email.UUID = Int32.Parse(ds.Tables[dsTable].Rows[i]["UUID"].ToString().Trim());

                        email.orgID = Int32.Parse(ds.Tables[dsTable].Rows[i]["orgID"].ToString().Trim()); //Added for FB 1710

                        email.isCalender = Int32.Parse(ds.Tables[dsTable].Rows[i]["Iscalendar"].ToString().Trim()); //ICAL Fix
                        
                        if (ds.Tables[dsTable].Rows[i]["Attachment"].ToString() != null)
                        {
                            if (ds.Tables[dsTable].Rows[i]["Attachment"].ToString().Length > 3)
                            {
                                email.Attachment = ds.Tables[dsTable].Rows[i]["Attachment"].ToString().Trim();
                                email.Attachment = email.Attachment.Remove(0, (email.Attachment.IndexOf("BEGIN:VCALENDAR")));
                                //email.Attachment = email.Attachment.Remove(email.Attachment.IndexOf("END:VCALENDAR")+12);
                                email.Attachment = email.Attachment.Replace("` + CHAR(13) + CHAR(10) + `", "\r\n");
                            }
                        }
                        
                        if (ds.Tables[dsTable].Rows[i]["RetryCount"].ToString().Trim().Length < 1)
                        {
                            email.RetryCount = 0;
                        }
                        else
                        {
                            email.RetryCount = Int32.Parse(ds.Tables[dsTable].Rows[i]["RetryCount"].ToString().Trim());
                        }
                        
                        if (ds.Tables[dsTable].Rows[i]["LastRetryDateTime"].ToString().Trim().Length < 1)
                        {
                            email.LastRetryDateTime = DateTime.Now;
                        }
                        else
                        {
                            email.LastRetryDateTime = DateTime.Parse(ds.Tables[dsTable].Rows[i]["LastRetryDateTime"].ToString().Trim());
                        }
                        
                        //Check for all mandatory fields.
                        // If empty skip and go to next email record.	
                        if ((email.To.Length > 3 || email.CC.Length > 3 || email.BCC.Length > 3) && email.Body.Length > 1)
                        { //email.From.Length > 3 &&
                            if (email.To.Length > 3)
                            {
                                logger.Trace("Valid email. To = " + email.To);
                            }
                            else
                            {
                                if (email.CC.Length > 3)
                                    logger.Trace("Valid email. CC = " + email.CC);
                                else
                                    logger.Trace("Valid email. BCC = " + email.BCC);
                            }
                        
                            newEmailQueue.Enqueue(email);
                        }
                        
                    }
                    catch (Exception e)
                    {
                        logger.Trace("Invalid email. Error = " + e.Message);
                    }
                }
				
				logger.Trace(" Total valid email count = " + newEmailQueue.Count.ToString());
				return (true);
			}
			catch (Exception e) 
			{	
				logger.Exception(100,e.Message);
				return false;
			}						
		}
			
		internal void DeleteEmail(NS_MESSENGER.Email emailObj)
		{
			// Delete the sent emails.
			try 
			{
				string query = "Delete from Email_Queue_D where UUID = " + emailObj.UUID.ToString();
				bool ret = NonSelectCommand(query);
				if (!ret)
				{
					logger.Trace("SQL error");
				}

			}
			catch (Exception e) 
			{	
				logger.Exception(100,e.Message);
			}

		}
		
		internal void UpdateUnsentEmail(NS_MESSENGER.Email email)
		{
			// Update the unsent emails
			try 
			{
				string query = " Declare @UUID int";
				query += " Set @UUID = " + email.UUID.ToString();
				query += " if ((select  max(retrycount) ";
				query += " from email_queue_d where UUID = @UUID) IS NULL )";
				query += " Update Email_Queue_D set RetryCount = 1, LastRetryDateTime = getdate()";
				query += " where UUID = @UUID";
				query += " else ";
				query += " Update Email_Queue_D set RetryCount = RetryCount +1, LastRetryDateTime = getdate()";
				query += " where UUID = @UUID";

				bool ret = NonSelectCommand(query);
				if (!ret)
				{
					logger.Trace("SQL error");
				}

			}
			catch (Exception e) 
			{	
				logger.Exception(100,e.Message);
			}

		}
		#endregion

		#region Logger Methods
		internal void InsertLogRecord (int errorCode,int severity,string file, string function,int line, string message)
		{
			// Insert log record.	
			try
			{
				// query
				string query = "Insert into Err_Log_D (modulename,moduleErrCode,severity,[file],line,timesubmitted,timelogged,message) values (";
				query += "'RTC'," + errorCode.ToString() + "," + severity.ToString() + ",'" + file + "'," + line.ToString() + ",getutcdate(),getutcdate(),'"+ message + "')";
			
				// open a connection and insert the record.
				SqlConnection con;
				con = new SqlConnection(conStr);
				con.Open();	
				SqlCommand cmd = new SqlCommand(query,con);		
				SqlDataReader reader = cmd.ExecuteReader();				
				reader.Close();
				con.Close();				
			}
			catch (Exception)
			{
				// do nothing
			}	
		}	
		
		internal bool FetchLogSettings (ref NS_MESSENGER.ConfigParams dbConfigParams)
		{						
			try
			{
				SqlConnection con ;
				DataSet ds = new DataSet();
				string dsTable = "LogSettings";
				con = new SqlConnection(conStr);
				string query = "Select loglevel from Err_LogPrefs_S where LogModule Like 'RTC'";
				SqlCommand cmd = new SqlCommand(query,con);		
				SqlDataAdapter da = new SqlDataAdapter();
				con.Open();	
				da.SelectCommand = cmd;
				da.Fill(ds,dsTable);
				con.Close();
				
				//loglevel
				int logLevel = 0;
				if (ds.Tables[dsTable].Rows.Count > 0)

				{
					logLevel = Int32.Parse(ds.Tables[dsTable].Rows[0]["loglevel"].ToString());
				}

				switch (logLevel) 
				{
					case 0: 
					{
						dbConfigParams.logLevel = NS_MESSENGER.ConfigParams.eLogLevel.SYSTEM_ERROR;
						break;
					}
					case 1: 
					{
						dbConfigParams.logLevel = NS_MESSENGER.ConfigParams.eLogLevel.USER_ERROR;
						break;
					}
					case 2: 
					{
						dbConfigParams.logLevel = NS_MESSENGER.ConfigParams.eLogLevel.WARNING;
						break;
					}
					case 3: 
					{
						dbConfigParams.logLevel = NS_MESSENGER.ConfigParams.eLogLevel.INFO;
						break;
					}
					case 9: 
					{
						dbConfigParams.logLevel = NS_MESSENGER.ConfigParams.eLogLevel.DEBUG;
						break;
					}
					default :
					{
						dbConfigParams.logLevel = NS_MESSENGER.ConfigParams.eLogLevel.SYSTEM_ERROR;
						break;
					}
                        
				}				
				
				return true;
			}
			catch (Exception ex)
			{				
				return false;
			}				
		}

		
		internal bool FetchLdapSettings (ref NS_MESSENGER.LdapSettings ldap)
		{						
			try
			{
				string query = "Select * from Ldap_ServerConfig_D";
				DataSet ds = new DataSet();
				string dsTable = "LDAPSettings";
				bool ret = SelectCommand (query,ref ds,dsTable);
				if (!ret)
				{
					logger.Exception(10,"SQL error");
					return (false);
				}

				if (ds.Tables[dsTable].Rows.Count < 1)
				{

					logger.Trace ("No LDAP server defined.");
					return false;
				}
				
				// ldap server params
				ldap.serverAddress = ds.Tables[dsTable].Rows[0]["ServerAddress"].ToString().Trim();
				ldap.serverLogin = ds.Tables[dsTable].Rows[0]["Login"].ToString().Trim();
				ldap.serverPassword = ds.Tables[dsTable].Rows[0]["Password"].ToString().Trim();
				
				//decrypt pwd
				cryptography.Crypto crypto = new Crypto();
				ldap.serverPassword = crypto.decrypt(ldap.serverPassword);
				
				ldap.connPort = Int32.Parse(ds.Tables[dsTable].Rows[0]["Port"].ToString().Trim());
				ldap.sessionTimeout = Int32.Parse(ds.Tables[dsTable].Rows[0]["Timeout"].ToString().Trim());
				ldap.loginKey = ds.Tables[dsTable].Rows[0]["LoginKey"].ToString().Trim();
				ldap.searchFilter = ds.Tables[dsTable].Rows[0]["SearchFilter"].ToString().Trim();
				
				// Replace "&amp;" with "&" as ldap search filters need that. 
                ldap.searchFilter = logger.RevertBackTheInvalidChars(ldap.searchFilter);
 
                //domain prefix
                ldap.domainPrefix = ds.Tables[dsTable].Rows[0]["DomainPrefix"].ToString().Trim();

                //schedule time
                ldap.scheduleTime = DateTime.Parse(ds.Tables[dsTable].Rows[0]["ScheduleTime"].ToString().Trim());
                
				// maneesh pujari 07/29/2008 FB 594.
                // SyncTime
                try
                {
                    // sync time
                    if (ds.Tables[dsTable].Rows[0]["SyncTime"].ToString() != null)
                    {
                        ldap.syncTime = DateTime.Parse(ds.Tables[dsTable].Rows[0]["SyncTime"].ToString().Trim());
                    }
                    else
                    {
                        ldap.syncTime = DateTime.Parse("2000-01-01 01:00:00.000");
                    }
                }
                catch (Exception)
                {
                    ldap.syncTime = DateTime.Parse("2000-01-01 01:00:00.000");
                }

                // schedule days
                if (ds.Tables[dsTable].Rows[0]["ScheduleDays"].ToString() != null)
                {
                    ldap.scheduleDays = ds.Tables[dsTable].Rows[0]["ScheduleDays"].ToString().Trim();
                }

				return true;			
			}
			catch (Exception e)
			{
				logger.Exception(100,e.Message);		
				return false;
			}									
		}

		#endregion

        /*
        private string ReplaceInvalidChars(string input)
        {
            input = input.Replace("<", "&lt;");
            input = input.Replace(">", "&gt;");
            input = input.Replace("&", "&amp;");
            input = input.Replace("\"", "&quot;");
            input = input.Replace("\"", "&quot;");
            input = input.Replace("\'", "&apos;;");
            return (input);
        }
        
        private string RevertBackTheInvalidChars(string input)
        {
            input = input.Replace("&lt;","<");
            input = input.Replace("&gt;",">");
            input = input.Replace("&amp;","&");
            input = input.Replace("&quot;","\"");
            input = input.Replace("&quot;","\"");
            input = input.Replace("&apos;","\'");
            return (input);
        }
       */
          
		#region ConfSetup Methods
		internal bool FetchAdminSettings (ref NS_MESSENGER.AdminSettings admin)
		{
			try
			{
				string query = "Select DeltaTime,Connect2,DialOut,AutoTermination,NumRetry from Sys_Settings_D";
				DataSet ds = new DataSet();
				string dsTable = "AdminSettings";
				bool ret = SelectCommand (query,ref ds,dsTable);
				if (!ret)
				{
					logger.Exception(10,"SQL error");
					return (false);
				}
				
				//deltatime
				admin.delta = Int32.Parse(ds.Tables[dsTable].Rows[0]["DeltaTime"].ToString());
				

				//connect2
				int connect2 =0;
				connect2 = Int32.Parse(ds.Tables[dsTable].Rows[0]["Connect2"].ToString());
				if (connect2 == 0 )
					admin.allowP2P=false;
				else
					admin.allowP2P=true;

				//dial-out
				int dialout =0;
				dialout = Int32.Parse(ds.Tables[dsTable].Rows[0]["DialOut"].ToString());
				if (dialout == 0 )
					admin.allowDialOut=false;
				else
					admin.allowDialOut=true;
					
				//auto-terminate calls
				int autoterminate =0;
				autoterminate =Int32.Parse(ds.Tables[dsTable].Rows[0]["AutoTermination"].ToString());
				
				if (autoterminate == 0 )
					admin.autoTerminate=false;
				else
					admin.autoTerminate=true;
										
				//num of retries
				admin.retries = Int32.Parse(ds.Tables[dsTable].Rows[0]["NumRetry"].ToString()) ;
				if (admin.retries < 1)
					admin.retries = 0;

			}
			catch (SqlException e)
			{
				logger.Exception(100,e.Message);		
				return false;
			}	
			
			return true;
		}

        internal bool FetchImmediateConferences(ref Queue confq, int confStatus)
        {
            try
            {
                DataSet ds = new DataSet();
                string dsTable = "Immediate";
                bool ret = false;
                if (confStatus == 0)
                {
                    ret = SelectCommand(immediateSql_Status0, ref ds, dsTable);
                }
                else
                {
                    ret = SelectCommand(immediateSql_Status5, ref ds, dsTable);
                }
				if (!ret)
				{
					
					logger.Exception(10,"SQL error");
					return (false);
				}
				
				for (int i=0;i<ds.Tables[dsTable].Rows.Count ; i++)
				{
					NS_MESSENGER.Conference conf = new NS_MESSENGER.Conference();

					// Retreive confid,instanceid
					conf.iDbID = Int32.Parse(ds.Tables[dsTable].Rows[i][0].ToString());
					logger.Trace ("Confid = " + conf.iDbID);
					conf.iInstanceID =  Int32.Parse(ds.Tables[dsTable].Rows[i][1].ToString());
					logger.Trace ("InstanceID= " + conf.iInstanceID);

					confq.Enqueue(conf);
				}
			}
			catch(Exception e)
			{	
				logger.Exception(100,e.Message );
				return false;
			}
			return true;
		}
        internal bool FetchCompletedP2PConfs(ref Queue confq)
        {
            try
            {
                DataSet ds = new DataSet();
                string dsTable = "p2p";
                bool ret = false;
                ret = SelectCommand(completedP2PSql, ref ds, dsTable);
                if (!ret)
                {

                    logger.Exception(10, "SQL error");
                    return (false);
                }

                for (int i = 0; i < ds.Tables[dsTable].Rows.Count; i++)
                {
                    NS_MESSENGER.Conference conf = new NS_MESSENGER.Conference();

                    // Retreive confid,instanceid
                    conf.iDbID = Int32.Parse(ds.Tables[dsTable].Rows[i][0].ToString());
                    logger.Trace("Confid = " + conf.iDbID);
                    conf.iInstanceID = Int32.Parse(ds.Tables[dsTable].Rows[i][1].ToString());
                    logger.Trace("InstanceID= " + conf.iInstanceID);

                    confq.Enqueue(conf);
                }
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
            return true;
        }
        //Vidyo FB 2599 Start
        internal bool FetchCompletedCloudConfs(ref Queue confq)
        {
            try
            {
                DataSet ds = new DataSet();
                string dsTable = "p2p";
                bool ret = false;
                ret = SelectCommand(completedCloudCall, ref ds, dsTable);
                if (!ret)
                {

                    logger.Exception(10, "SQL error");
                    return (false);
                }

                for (int i = 0; i < ds.Tables[dsTable].Rows.Count; i++)
                {
                    NS_MESSENGER.Conference conf = new NS_MESSENGER.Conference();

                    // Retreive confid,instanceid
                    conf.iDbID = Int32.Parse(ds.Tables[dsTable].Rows[i]["confid"].ToString());
                    logger.Trace("Confid = " + conf.iDbID);
                    conf.iInstanceID = Int32.Parse(ds.Tables[dsTable].Rows[i]["instanceid"].ToString());
                    logger.Trace("InstanceID= " + conf.iInstanceID);
                    conf.iOrgID = Int32.Parse(ds.Tables[dsTable].Rows[i]["OrgID"].ToString());
                    logger.Trace("orgID= " + conf.iOrgID);

                    confq.Enqueue(conf);
                }
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
            return true;
        }		
        //FB 2599 End
        //ZD 100522_S1 Start
        internal bool FetchCompletedVMRConfs(ref Queue confq)
        {
            try
            {
                DataSet ds = new DataSet();
                DataSet ds1 = new DataSet();
                string dsTable = "VMR", dsTable1 = "conf";
                bool ret = false;
                ret = SelectCommand(completedVMRSql, ref ds, dsTable);
                if (!ret)
                {

                    logger.Exception(10, "SQL error");
                    return (false);
                }

                for (int i = 0; i < ds.Tables[dsTable].Rows.Count; i++)
                {
                    NS_MESSENGER.Conference conf = new NS_MESSENGER.Conference();

                    // Retreive confid,instanceid
                    conf.iDbID = Int32.Parse(ds.Tables[dsTable].Rows[i][0].ToString());
                    logger.Trace("Confid = " + conf.iDbID);
                    conf.iInstanceID = Int32.Parse(ds.Tables[dsTable].Rows[i][1].ToString());
                    logger.Trace("InstanceID= " + conf.iInstanceID);

                    confq.Enqueue(conf);
                }

                //ZD 101522 Start
                ret = SelectCommand(completedVideoSql, ref ds1, dsTable1);
                if (!ret)
                {

                    logger.Exception(10, "SQL error");
                    return (false);
                }

                for (int i = 0; i < ds1.Tables[dsTable1].Rows.Count; i++)
                {
                    NS_MESSENGER.Conference conf = new NS_MESSENGER.Conference();

                    // Retreive confid,instanceid
                    conf.iDbID = Int32.Parse(ds1.Tables[dsTable1].Rows[i][0].ToString());
                    logger.Trace("Confid = " + conf.iDbID);
                    conf.iInstanceID = Int32.Parse(ds1.Tables[dsTable1].Rows[i][1].ToString());
                    logger.Trace("InstanceID= " + conf.iInstanceID);

                    confq.Enqueue(conf);
                }
                //ZD 101522 End
                
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
            return true;
        }
        //ZD 100522_S1 End

		internal bool FetchRetryConferences(ref Queue confq)
		{
			try 
			{
                // retry confs
                DataSet ds1 = new DataSet();
                string dsTable1 = "Retry";
                bool ret = SelectCommand(retrySql_Status0, ref ds1, dsTable1);
                if (!ret)
                {
                    logger.Exception(100, "SQL error");
                    return (false);
                }

                for (int i = 0; i < ds1.Tables[dsTable1].Rows.Count; i++)
                {
                    NS_MESSENGER.Conference conf = new NS_MESSENGER.Conference();

                    // Retreive confid,instanceid
                    conf.iDbID = Int32.Parse(ds1.Tables[dsTable1].Rows[i][0].ToString());
                    logger.Trace("Confid = " + conf.iDbID);

                    conf.iInstanceID = Int32.Parse(ds1.Tables[dsTable1].Rows[i][1].ToString());
                    logger.Trace("InstanceID= " + conf.iInstanceID);

                    confq.Enqueue(conf);
                }
			}
			catch(Exception e)
			{	
				logger.Exception(100,e.Message);
				return false;
			}
			return true;
		}

        internal bool FetchOngoingConfs(ref Queue confq, int confStatus)
        {
            // Fetch the ongoing bridge confs i.e. multipoint confs.
            try
            {
                DataSet ds = new DataSet();
                string dsTable = "Ongoing";
                bool ret = false;
                if (confStatus == 0)
                {
                    ret = SelectCommand(ongoingSql_Status0, ref ds, dsTable);
                }
                else
                {
                    ret = SelectCommand(ongoingSql_Status5, ref ds, dsTable);
                }
				if (!ret)
				{
					logger.Exception(10,"SQL error");
					return (false);
				}
				
				for (int i=0;i<ds.Tables[dsTable].Rows.Count ; i++)
				{
					NS_MESSENGER.Conference conf = new NS_MESSENGER.Conference();

					// Retreive confid,instanceid
					conf.iDbID = Int32.Parse(ds.Tables[dsTable].Rows[i]["confid"].ToString());
					conf.iInstanceID =  Int32.Parse(ds.Tables[dsTable].Rows[i]["instanceid"].ToString());

					confq.Enqueue(conf);
				}
			}
			catch(Exception e)
			{	
				logger.Exception(100,e.Message);
				return false;
			}
			return true;
		}

		internal bool FetchReminderConfs (int minsRemainingToConfStart,ref Queue confq)
		{
			// Fetch the ongoing bridge confs i.e. multipoint confs.
			try 
			{
				DataSet ds = new DataSet();
				string dsTable = "Future";
				int secsRemainingToStart = minsRemainingToConfStart * 60;
				int endLimit = secsRemainingToStart + 60; // 1 min = MAGIC NUMBER
				string query = "SELECT confid,instanceid FROM CONF_CONFERENCE_D C";
				query += " WHERE C.deleted = 0 AND C.status = 0 AND (C.confdate > dateadd(s,"+ secsRemainingToStart.ToString() +", getutcdate()) AND C.confdate < dateadd(s,"+ endLimit.ToString() +", getutcdate())) ORDER BY C.confdate";
				bool ret = SelectCommand(query,ref ds,dsTable);
				if (!ret)
				{
					logger.Exception(10,"SQL error");
					return (false);
				}
				
				for (int i=0;i<ds.Tables[dsTable].Rows.Count ; i++)
				{
					NS_MESSENGER.Conference conf = new NS_MESSENGER.Conference();

					// Retreive confid,instanceid
					conf.iDbID = Int32.Parse(ds.Tables[dsTable].Rows[i]["confid"].ToString());
					conf.iInstanceID =  Int32.Parse(ds.Tables[dsTable].Rows[i]["instanceid"].ToString());

					confq.Enqueue(conf);
				}

				return true;
			}
			catch(Exception e)
			{	
				logger.Exception(100,e.Message);
				return false;
			}						
		}
		#region Fetch conf info and participants
		
		internal bool FetchConf(ref NS_MESSENGER.Conference conf)
		{
            int iStatus = 0;//FB 2710
			try 
			{
				// Retreive conf details
				DataSet ds = new DataSet();
				string dsTable = "Conference";
                string VMRTable = "VMRConf";
                int ConfID, ConfInstanceID = 0; //ZD 100522
                string query = null; bool ret = false; int RMXserviceID = 0, PoolOrderID = 0; //ZD 104256 //ZD 101459
                if (conf.iDbID > 0 && conf.iInstanceID > 0)
                {
                    // conf id & instance id are present
                    //query = "Select * from Conf_Conference_D C, where confid = '" + conf.iDbID.ToString() + "' and instanceid = '" + conf.iInstanceID.ToString() + "'"; ;
                    //query = "Select c.*,b.E164Dialnumber,b.RPRMUserid,b.ConfRMXServiceID from Conf_Conference_D C,Conf_Bridge_D B where C.confid = '" + conf.iDbID.ToString() + "'  and C.instanceid = '" + conf.iInstanceID.ToString() + "' and c.confnumname=b.confuId"; //FB 2636 //FB 2709 //FB 2839
                    //Point to Point Call issue
                    //query = " Select C.*,B.E164Dialnumber, B.RPRMUserid, B.ConfRMXServiceID, B.SystemLocation,B.PoolOrderID from Conf_Conference_D C ";
                    //query += " left outer join Conf_Bridge_D B on  C.confnumname = B.confuId where C.confid = '" + conf.iDbID.ToString() + "'  and C.instanceid = '" + conf.iInstanceID.ToString() + "'"; //FB 2636 //FB 2709 //FB 2839 //ZD 104256
                    query = " Select C.*,B.E164Dialnumber, B.RPRMUserid, B.ConfRMXServiceID, B.SystemLocation,B.PoolOrderID,O.EnableSyncLaunchBuffer,O.SyncLaunchBuffer,O.EnableHostGuestPwd,O.EnableConfPassword from Org_Settings_D O,Conf_Conference_D C "; //ALLDEV-837 //ALLDEV-826
                    query += " left outer join Conf_Bridge_D B on  C.confnumname = B.confuId where C.confid = '" + conf.iDbID.ToString() + "'  and C.instanceid = '" + conf.iInstanceID.ToString() + "' and C.orgId = O.OrgId"; //FB 2636 //FB 2709 //FB 2839 //ZD 104256 //ALLDEV-837
                }
                else
                {
                    if ((!string.IsNullOrEmpty(conf.sGUID)) && conf.iIsVMR == 2) //ZD 100522
                    {
                        VMRTable = "VMRConf";
                        //query = "Select ConfId,InstanceId from Conf_Conference_D where permanent = 1 and GUID = '" + conf.sGUID + "'";
                        query = "Select C.ConfId,C.InstanceId,O.EnableSyncLaunchBuffer,O.SyncLaunchBuffer,O.EnableHostGuestPwd,O.EnableConfPassword from Conf_Conference_D C, Org_Settings_D O where C.permanent = 1 and C.GUID = '" + conf.sGUID + "' and C.orgId = O.OrgId";  //ALLDEV-837 //ALLDEV-826

                        ret = false;
                        ret = SelectCommand(query, ref ds, VMRTable);
                        if (!ret)
                        {
                            logger.Exception(100, "SQL error");
                            return (false);
                        }

                        if (ds.Tables[VMRTable].Rows.Count < 1)
                        {
                            // no confs 
                            logger.Trace("No conf found.");
                            return false;
                        }

                        int.TryParse(ds.Tables[VMRTable].Rows[0]["ConfId"].ToString(), out ConfID);
                        int.TryParse(ds.Tables[VMRTable].Rows[0]["InstanceId"].ToString(), out ConfInstanceID);

                        if (ConfID > 0 && ConfInstanceID > 0)
                        {
                            //query = " Select C.*,B.E164Dialnumber, B.RPRMUserid, B.ConfRMXServiceID, B.SystemLocation,B.PoolOrderID  from Conf_Conference_D C ";
                            //query += " left outer join Conf_Bridge_D B on  C.confnumname = B.confuId where C.confid = '" + ConfID + "'  and C.instanceid = '" + ConfInstanceID + "'";  //ZD 104256
                            query = " Select C.*,B.E164Dialnumber, B.RPRMUserid, B.ConfRMXServiceID, B.SystemLocation,B.PoolOrderID,O.EnableSyncLaunchBuffer,O.SyncLaunchBuffer, O.EnableHostGuestPwd,O.EnableConfPassword from Org_Settings_D O,Conf_Conference_D C ";    //ALLDEV-837 //ALLDEV-826
                            query += " left outer join Conf_Bridge_D B on  C.confnumname = B.confuId where C.confid = '" + ConfID + "'  and C.instanceid = '" + ConfInstanceID + "' and C.orgId = O.OrgId";  //ZD 104256 //ALLDEV-837
                        }
                    }
                    else if ((!string.IsNullOrEmpty(conf.sMcuName)) && conf.sMcuName.ToString().Length > 1) //ZD 100522
                    {
                        // mcu name is present 
                        ret = ParseConfName(conf.sMcuName, ref conf.iDbNumName);
                        if (!ret)
                        {
                            this.errMsg = "Error retrieving the conf uniqueid.";
                            logger.Trace("Error retrieving the conf uniqueid.");
                            return false;
                        }

                        //query = "Select * from Conf_Conference_D where confnumname = '" + conf.iDbNumName.ToString() + "'";
                        query = "Select * from Conf_Conference_D C, Org_Settings_D O where C.confnumname = '" + conf.iDbNumName.ToString() + "' and C.orgId = O.OrgId"; //ALLDEV-837
                    }
                    else
                    {
                        // no data present using which conf info can be retreived
                        return false;
                    }
                }
				ret = false;
				ret = SelectCommand(query,ref ds,dsTable);
				if (!ret)
				{
					logger.Exception(100,"SQL error");
					return (false);
				}		

				if (ds.Tables[dsTable].Rows.Count < 1 )
				{
					// no confs 
					logger.Trace ("No conf found.");
					return false;
				}

                //ZD 100522 Ends

                conf.iOrgID = Int32.Parse(ds.Tables[dsTable].Rows[0]["orgId"].ToString()); //FB 2335 
				conf.iDbID = Int32.Parse(ds.Tables[dsTable].Rows[0]["ConfId"].ToString());  
				conf.iInstanceID = Int32.Parse(ds.Tables[dsTable].Rows[0]["InstanceId"].ToString());  
				conf.iDbNumName = Int32.Parse(ds.Tables[dsTable].Rows[0]["ConfNumName"].ToString());  
				conf.sExternalName = ds.Tables[dsTable].Rows[0]["ExternalName"].ToString();
				conf.iHostUserID = Int32.Parse(ds.Tables[dsTable].Rows[0]["owner"].ToString());  
				conf.ESId = ds.Tables[dsTable].Rows[0]["ESId"].ToString(); //Polycom CMA
                conf.iImmediate = Int32.Parse(ds.Tables[dsTable].Rows[0]["immediate"].ToString());//FB 2440
                conf.sDescription = ds.Tables[dsTable].Rows[0]["description"].ToString(); //FB 2595
                conf.iPushtoExternal = int.Parse(ds.Tables[dsTable].Rows[0]["PushedToExternal"].ToString()); //FB 2441
                conf.sEtag = ds.Tables[dsTable].Rows[0]["Etag"].ToString(); //FB 2441
                conf.iMcuSetupTime = int.Parse(ds.Tables[dsTable].Rows[0]["McuSetupTime"].ToString()); //FB 2998
                conf.iMCUTeardonwnTime = int.Parse(ds.Tables[dsTable].Rows[0]["MCUTeardonwnTime"].ToString()); //FB 2998
                conf.iMeetingId = int.Parse(ds.Tables[dsTable].Rows[0]["MeetingId"].ToString());//ZD 100167 
                //ZD 103263 Start
                if(ds.Tables[dsTable].Rows[0]["isBJNConf"] != null)
                    conf.iBJNConf = int.Parse(ds.Tables[dsTable].Rows[0]["isBJNConf"].ToString());
                if (ds.Tables[dsTable].Rows[0]["BJNUniqueid"] != null)
                    conf.sBJNUniqueid = ds.Tables[dsTable].Rows[0]["BJNUniqueid"].ToString();
                if (ds.Tables[dsTable].Rows[0]["BJNUserId"] != null)
                    conf.sBJNUserId = ds.Tables[dsTable].Rows[0]["BJNUserId"].ToString();
                //ZD 103263 End
                //ZD 104021 Start
                if (ds.Tables[dsTable].Rows[0]["BJNMeetingType"] != null)
                    conf.iBJNMeetingType = int.Parse(ds.Tables[dsTable].Rows[0]["BJNMeetingType"].ToString());
                if (ds.Tables[dsTable].Rows[0]["CascadeBJN"] != null)
                    conf.iCascadeBJN = int.Parse(ds.Tables[dsTable].Rows[0]["CascadeBJN"].ToString());
                if (ds.Tables[dsTable].Rows[0]["BJNPasscode"] != null)
                    conf.sBJNPasscode = ds.Tables[dsTable].Rows[0]["BJNPasscode"].ToString();
                if (ds.Tables[dsTable].Rows[0]["BJNHostPasscode"] != null)
                    conf.sBJNHostPasscode = ds.Tables[dsTable].Rows[0]["BJNHostPasscode"].ToString();
                if (ds.Tables[dsTable].Rows[0]["BJNMeetingid"] != null)
                    conf.sBJNMeetingid = ds.Tables[dsTable].Rows[0]["BJNMeetingid"].ToString();
                //ZD 104021 End
                //ZD 101348 Starts
                int isPermanent = 0;
                if (ds.Tables[dsTable].Rows[0]["Permanent"] != null)
                    int.TryParse(ds.Tables[dsTable].Rows[0]["Permanent"].ToString(), out isPermanent);
                conf.iPermanent = isPermanent;
                conf.sPermanentconfName = ds.Tables[dsTable].Rows[0]["PermanentconfName"].ToString();
                //ZD 101348 End
                if (ds.Tables[dsTable].Rows[0]["ConfURI"] != null)//ZD 101496
                    if (ds.Tables[dsTable].Rows[0]["ConfURI"].ToString() != null)//ZD 101496
                        conf.sURI = ds.Tables[dsTable].Rows[0]["ConfURI"].ToString(); //ZD 101217

                //FB 2710
                Int32.TryParse(ds.Tables[dsTable].Rows[0]["status"].ToString(), out iStatus);
                conf.iStatus = iStatus;


                if (ds.Tables[dsTable].Rows[0]["isvmr"].ToString().Trim() != "")//FB 2447
                    conf.iIsVMR = Int32.Parse(ds.Tables[dsTable].Rows[0]["isvmr"].ToString());//FB 2447
                
                RMXserviceID = 0;
                if (ds.Tables[dsTable].Rows[0]["ConfRMXServiceID"] != null)//FB 2839 //ZD 100522
                    if (ds.Tables[dsTable].Rows[0]["ConfRMXServiceID"].ToString() != null)//FB 2839 //ZD 100522
                        int.TryParse(ds.Tables[dsTable].Rows[0]["ConfRMXServiceID"].ToString(), out RMXserviceID);
                conf.iConfRMXServiceID = RMXserviceID;

                //ZD 104256 Starts
                PoolOrderID = 0;
                if (ds.Tables[dsTable].Rows[0]["PoolOrderID"] != null)
                    if (ds.Tables[dsTable].Rows[0]["PoolOrderID"].ToString() != null)
                        int.TryParse(ds.Tables[dsTable].Rows[0]["PoolOrderID"].ToString(), out PoolOrderID);
                conf.iConfPoolOrderID = PoolOrderID;
                //ZD 104256 Ends

                //FB 2441
                if (ds.Tables[dsTable].Rows[0]["recuring"].ToString() != null)
                    Int32.TryParse(ds.Tables[dsTable].Rows[0]["recuring"].ToString(), out conf.iRecurring);

                //FB 2659
                int eptCount = 0;
                if (ds.Tables[dsTable].Rows[0]["Seats"] != null)
                    int.TryParse(ds.Tables[dsTable].Rows[0]["Seats"].ToString(), out eptCount);
                conf.iEndpointCount = eptCount;

                try
                {
                    conf.sPwd = ds.Tables[dsTable].Rows[0]["password"].ToString().Trim();
                    conf.iPwd = Int32.Parse(conf.sPwd);
                }
                catch (Exception)
                {
                    conf.sPwd = "";
                    conf.iPwd = 0;
                }

                //ALLDEV-826 Starts  

                conf.iEnableHostGuestPwd = int.Parse(ds.Tables[dsTable].Rows[0]["EnableHostGuestPwd"].ToString());
                conf.iEnableConfpassword = int.Parse(ds.Tables[dsTable].Rows[0]["EnableConfPassword"].ToString());
                try
                {
                    conf.sHostPwd = ds.Tables[dsTable].Rows[0]["hostPassword"].ToString().Trim();
                    conf.iHostPwd = Int32.Parse(conf.sHostPwd);
                }
                catch (Exception)
                {
                    conf.sHostPwd = "";
                    conf.iHostPwd = 0;
                }
                //ALLDEV-826 Ends

				#region Fetch the host email
				DataSet ds2 = new DataSet();
                string dsTable2 = "ConfHost";//FB 2659
                string query2 = "Select Email, FirstName + ' ' + LastName as HostName from Usr_List_D where userid = " + conf.iHostUserID.ToString(); 
				bool ret3 = SelectCommand(query2,ref ds2,dsTable2);
				if (!ret3)
				{
					logger.Exception(100,"SQL error");
					return (false);
				}		

				if (ds2.Tables[dsTable2].Rows.Count < 1 )
				{
					// no record 
					logger.Trace ("No host email found.");
					return false;
				}
                conf.sHostEmail = ds2.Tables[dsTable2].Rows[0]["Email"].ToString();
                conf.sHostName = ds2.Tables[dsTable2].Rows[0]["HostName"].ToString();//FB 2659
				#endregion

				ret = false;
				ret = GenerateConfName(conf,ref conf.sDbName);
				if (!ret) 
				{
					logger.Trace ("Failure in generating conf name for bridges.");
					return false;
				}

                if (conf.iIsVMR == 2 && conf.iBJNConf == 0) //ZD 100522 //ZD 103263
                    conf.sDbName = conf.sPermanentconfName;

                // duration
                conf.iDuration = Int32.Parse(ds.Tables[dsTable].Rows[0]["Duration"].ToString()); //ZD 100085
                //conf.iDuration = conf.iDuration + timeDiff.Minutes;
                logger.Trace("Conf Duration : " + conf.iDuration.ToString());

                // start date & time
				//ZD 100085 Starts
                //ZD 104332 starts
                if (conf.iImmediate == 1)
                    conf.dtStartDateTime = DateTime.Parse(ds.Tables[dsTable].Rows[0]["ConfDate"].ToString());
                else
                    conf.dtStartDateTime = DateTime.Parse(ds.Tables[dsTable].Rows[0]["ConfDate"].ToString()).AddSeconds(-40); //FB 2440  // 5 secs buffer
                
                //ZD 104332 ends

                conf.dtSetUpDateTime = DateTime.Parse(ds.Tables[dsTable].Rows[0]["SetupTime"].ToString()); //FB 2595

                
                logger.Trace("Start Time : " + conf.dtStartDateTime.ToString("T"));
                conf.dtEndDateTime = conf.dtStartDateTime.AddMinutes(conf.iDuration);//FB 2440 //ZD 100085
                //conf.dtEndDateTime = DateTime.Parse(ds.Tables[dsTable].Rows[0]["TearDownTime"].ToString());//FB 2440
                //ZD 100085 End
                // conf timezone
                conf.iTimezoneID = Int32.Parse(ds.Tables[dsTable].Rows[0]["Timezone"].ToString());
                logger.Trace("TimezoneID : " + conf.iTimezoneID.ToString());

                // timezone 
                DataSet dsTZ = new DataSet();
                string dsTableTZ = "TZName";
                string queryTZ = "select timezone,timezonediff from gen_timezone_s where timezoneID = " + conf.iTimezoneID.ToString();
                bool retTZ = SelectCommand(queryTZ, ref dsTZ, dsTableTZ);
                if (!retTZ)
                {
                    logger.Exception(100, "SQL error");
                    return (false);
                }
                conf.sName_StartDateTimeInLocalTZ = dsTZ.Tables[dsTableTZ].Rows[0]["timezonediff"].ToString() + dsTZ.Tables[dsTableTZ].Rows[0]["timezone"].ToString(); 

				// start date & time in UTC
				conf.dtStartDateTimeInUTC = conf.dtStartDateTime;

				// Time difference 
				/*TimeSpan timeDiff = conf.dtStartDateTime.Subtract(DateTime.UtcNow);
				logger.Trace ("DateTime.UtcNow: " + DateTime.UtcNow.ToString("T"));
				logger.Trace ("Time Diff : " + timeDiff.Minutes.ToString());
                */

                // Convert to local TZ
                DataSet dsTZ1 = new DataSet();
                string dsTableTZ1 = "LocalTZ";
                string queryTZ1 = "select dbo.changeTime (" + conf.iTimezoneID.ToString() + ",'" + conf.dtStartDateTimeInUTC + "')";                
                bool retTZ1 = SelectCommand(queryTZ1, ref dsTZ1, dsTableTZ1);
                if (!retTZ1)
                {
                    logger.Exception(100, "SQL error");
                    return (false);
                }
                conf.dtStartDateTimeInLocalTZ = DateTime.Parse(dsTZ1.Tables[dsTableTZ1].Rows[0][0].ToString()); 

				

                //check conf type
				int confType = 0;
				confType = Int32.Parse(ds.Tables[dsTable].Rows[0]["ConfType"].ToString()); //dbo.Conference.ConfType				
                conf.iConfType = confType;//ZD 101816
				logger.Trace ("Conf Type : " + confType.ToString());
				switch (confType)
				{
                    case 2:
                        {
                            // audio-video conference
                            conf.etMediaType = NS_MESSENGER.Conference.eMediaType.AUDIO_VIDEO;
                            conf.etType = NS_MESSENGER.Conference.eType.MULTI_POINT;
                            break;
                        }
					case 6:
					{
                        // audio conference
						conf.etMediaType = NS_MESSENGER.Conference.eMediaType.AUDIO;
						conf.etType = NS_MESSENGER.Conference.eType.MULTI_POINT;
						break;
					}
                case 4:
                    {
                        // p2p conference
                        conf.etMediaType = NS_MESSENGER.Conference.eMediaType.AUDIO_VIDEO;
                        conf.etType = NS_MESSENGER.Conference.eType.POINT_TO_POINT;
                        break;
                    }       
                default:
                    {   // 7 = room conference
                        this.errMsg = "This is not an audio/video conference.";
                        return false;                        
                    }
				}
               
				//conf.bAutoTerminate = NS_CONFIG.Config.CONF_AUTO_TERMINATE; 
				conf.bAutoTerminate = true;

                // lecture mode
                int lecMode = Int32.Parse(ds.Tables[dsTable].Rows[0]["LectureMode"].ToString().Trim());
                if (lecMode == 0)
                {
                    conf.bLectureMode = false;
                    logger.Trace("Lecture Mode : Disabled ");
                }
                else
                {
                    conf.bLectureMode = true;
                    logger.Trace("Lecture Mode : Enabled ");
                }                
                //FB 2486 Start
                int MessageOverlay = Int32.Parse(ds.Tables[dsTable].Rows[0]["isTextMsg"].ToString().Trim()); 
                if (MessageOverlay == 0)
                {
                    conf.bMessageOverlay = false;
                    logger.Trace("Message Overlay : Disabled ");
                }
                else
                {
                    conf.bMessageOverlay = true;
                    logger.Trace("Message Overlay : Enabled ");
                }
                //FB 2486 End

                //FB 2636 Start
                int EnableE164 = Int32.Parse(ds.Tables[dsTable].Rows[0]["E164Dialing"].ToString().Trim());
                if (EnableE164 == 0)
                {
                    conf.bE164 = false;
                    logger.Trace("Enable E.164 : Disabled ");
                }
                else
                {
                    conf.bE164 = true;
                    logger.Trace("Enable E.164 : Enabled ");
                }
                int EnableH323 = Int32.Parse(ds.Tables[dsTable].Rows[0]["H323Dialing"].ToString().Trim());
                if (EnableH323 == 0)
                {
                    conf.bH323 = false;
                    logger.Trace("Enable H.323 : Disabled ");
                }
                else
                {
                    conf.bH323 = true;
                    logger.Trace("Enable H.323 : Enabled ");
                }
                if (ds.Tables[dsTable].Rows[0]["E164Dialnumber"]!= null)
                {
                    if (ds.Tables[dsTable].Rows[0]["E164Dialnumber"].ToString() != null)
                    {
                        conf.sDialinNumber = ds.Tables[dsTable].Rows[0]["E164Dialnumber"].ToString().Trim();//FB 2659
                    }
                }
                //FB 2636 End
                
                //FB 2709
                int RPRMUserID = 0;
                if (ds.Tables[dsTable].Rows[0]["RPRMUserid"] != null)
                {
                    if (ds.Tables[dsTable].Rows[0]["RPRMUserid"].ToString() != null)
                    {
                        int.TryParse(ds.Tables[dsTable].Rows[0]["RPRMUserid"].ToString(), out RPRMUserID);
                        conf.iRPRMUserID = RPRMUserID;
                    }
                }
                FetchRPRMConfUser(conf.iRPRMUserID, ref conf);
                //FB 2709

                //FB 2501 Call Monitoring Start
                conf.sGUID = ds.Tables[dsTable].Rows[0]["GUID"].ToString();
                //FB 2501 Call Monitoring End

                conf.sDialString = ds.Tables[dsTable].Rows[0]["DialString"].ToString();//FB 2441

                //ZD 100221 Starts
                int temp = 0;
                if (ds.Tables[dsTable].Columns["WebExconf"] != null)
                    int.TryParse(ds.Tables[dsTable].Rows[0]["WebExconf"].ToString(), out temp);
                conf.iWebExconf = temp;
                //ZD 100221 Ends

                //ZD 101522 Start
                conf.iSystemLocation = 0;
                if (ds.Tables[dsTable].Rows[0]["SystemLocation"] != null)
                    int.TryParse(ds.Tables[dsTable].Rows[0]["SystemLocation"].ToString(), out conf.iSystemLocation);
                //ZD 101522 End


				#region Fetch the conf advanced audio/video params 
				DataSet ds1 = new DataSet();
				string dsTable1 = "Conference";
                string query1 = "Select * from Conf_AdvAVParams_D where confid = '" + conf.iDbID.ToString() + "' and instanceid = '" + conf.iInstanceID.ToString() + "'";
				bool ret1 = SelectCommand(query1,ref ds1,dsTable1);
				if (!ret1)
				{
					logger.Exception(100,"SQL error");
					return (false);
				}		

				if (ds1.Tables[dsTable1].Rows.Count > 0 )
				{
					//line rate
					bool ret2 = ConvertToMcuLineRate((Int32.Parse(ds1.Tables[dsTable1].Rows[0]["LineRateID"].ToString())),ref conf.stLineRate.etLineRate);
					if (!ret2)
					{
						logger.Exception(100,"Error in LineRateID");
						return (false);
					}		


					// audio codec
					ret2=false;
					ret2 = ConvertToMcuAudioCodec((Int32.Parse(ds1.Tables[dsTable1].Rows[0]["AudioAlgorithmID"].ToString())),ref conf.etAudioCodec);
					if (!ret2)
					{
						logger.Exception(100,"Error in AudioAlgorithmID");
						return (false);
					}		
		

					// network
					ret2=false;
					ret2 = ConvertToMcuNetwork((Int32.Parse(ds1.Tables[dsTable1].Rows[0]["VideoProtocolID"].ToString())),ref conf.etNetwork);							
					if (!ret2)
					{
						logger.Exception(100,"Error in VideoProtocolID");
						return (false);
					}


                    try 
					{

						// video layouts 
						if (ds1.Tables[dsTable1].Rows[0]["videoLayoutID"].ToString()!= null)
						{
							conf.iVideoLayout = Int32.Parse(ds1.Tables[dsTable1].Rows[0]["videoLayoutID"].ToString());							
						}
                        //ZD 101869
                        if (ds1.Tables[dsTable1].Rows[0]["Familylayout"].ToString() != null)
                        {
                            conf.iFamilylayout = Int32.Parse(ds1.Tables[dsTable1].Rows[0]["Familylayout"].ToString());
                        }

						// dual stream mode - H.239
						if (ds1.Tables[dsTable1].Rows[0]["dualStreamModeID"].ToString()!= null)
						{
							if (Int32.Parse(ds1.Tables[dsTable1].Rows[0]["dualStreamModeID"].ToString())== 1)
							{
								conf.bH239 = true;
							}
						}
					
						// conference on port (polycom-specific feature)
						if (ds1.Tables[dsTable1].Rows[0]["conferenceOnPort"].ToString()!= null)
						{
							if (Int32.Parse(ds1.Tables[dsTable1].Rows[0]["conferenceOnPort"].ToString())== 1)
							{
								conf.bConferenceOnPort = true;
							}
						}
									
						// encryption (polycom-specific feature)
						if (ds1.Tables[dsTable1].Rows[0]["encryption"].ToString()!= null)
						{
							if (Int32.Parse(ds1.Tables[dsTable1].Rows[0]["encryption"].ToString())== 1)
							{
								conf.bEncryption = true;
							}
						}
						
						// max audio ports
						if (ds1.Tables[dsTable1].Rows[0]["maxAudioParticipants"].ToString()!= null)
						{
							conf.iMaxAudioPorts = Int32.Parse(ds1.Tables[dsTable1].Rows[0]["maxAudioParticipants"].ToString());							
						}
						
						// max video ports
						if (ds1.Tables[dsTable1].Rows[0]["maxVideoParticipants"].ToString()!= null)
						{
							conf.iMaxVideoPorts = Int32.Parse(ds1.Tables[dsTable1].Rows[0]["maxVideoParticipants"].ToString());							
						}

                        
                        //FB 2441 Start
                        if (ds1.Tables[dsTable1].Rows[0]["PolycomSendEmail"].ToString() != null)
                        {
                            conf.iPolycomSendMail = Int32.Parse(ds1.Tables[dsTable1].Rows[0]["PolycomSendEmail"].ToString());
                        }
                        //if (ds1.Tables[dsTable1].Rows[0]["PolycomTemplate"].ToString() != null) //ZD 104465
                        //{
                        //    conf.sPolycomTemplate = ds1.Tables[dsTable1].Rows[0]["PolycomTemplate"].ToString();
                        //}
                        //Fb 2441 End

						// video codecs
						if (ds1.Tables[dsTable1].Rows[0]["videoSession"].ToString()!= null)
						{
							// Note : this is a confusing name matchup 
							// In VRM DB this is equated to VideoSession (which is actually incorrect but needed to keep it that way to support legacy deployments)
							// Its actually a VideoCodec (hence, in RTC the name is now corrected internally).
							switch (Int32.Parse(ds1.Tables[dsTable1].Rows[0]["videoSession"].ToString()))
							{
								case 0:
								{
									conf.etVideoCodec = NS_MESSENGER.Conference.eVideoCodec.AUTO;
									break;
								}
								case 1:
								{
									conf.etVideoCodec = NS_MESSENGER.Conference.eVideoCodec.H261;
									break;
								}
								case 2:
								{
									conf.etVideoCodec = NS_MESSENGER.Conference.eVideoCodec.H263;
									break;
								}
								case 3:
								{
									conf.etVideoCodec = NS_MESSENGER.Conference.eVideoCodec.H264;
									break;
								}
							}
						}
                        
                        try
                        {
                            // video modes
                            switch (Int32.Parse(ds1.Tables[dsTable1].Rows[0]["videoMode"].ToString()))
                            {
                                case 1:
                                    {
                                        conf.etVideoSession = NS_MESSENGER.Conference.eVideoSession.SWITCHING;
                                        break;
                                    }
                                case 2:
                                    {
                                        conf.etVideoSession = NS_MESSENGER.Conference.eVideoSession.TRANSCODING;
                                        break;
                                    }
                                default:
                                    {
                                        conf.etVideoSession = NS_MESSENGER.Conference.eVideoSession.CONTINOUS_PRESENCE;
                                        break;
                                    }
                            }
                        }
                        catch (Exception)
                        {
                            logger.Trace("Error in video mode.");
                        }

                        try
                        {
                            // single dial-in
                            if (ds1.Tables[dsTable1].Rows[0]["singleDialIn"].ToString()!=null)
                            {
                                if (Int32.Parse(ds1.Tables[dsTable1].Rows[0]["singleDialIn"].ToString()) == 1)
                                {
                                    conf.isSingleDialIn = true;
                                }
                            }
                        }
                        catch (Exception)
                        {
                            logger.Trace("Error in video mode.");
                        }

                        /** FB 2447 **/
                        if (conf.iIsVMR > 0)
                        {
                            if (ds1.Tables[dsTable1].Rows[0]["internalbridge"] != null)
                                conf.sInternalBridgeNumber = ds1.Tables[dsTable1].Rows[0]["internalbridge"].ToString().Trim();

                            if (ds1.Tables[dsTable1].Rows[0]["externalbridge"] != null)
                                conf.sExternalBridgeNumber = ds1.Tables[dsTable1].Rows[0]["externalbridge"].ToString().Trim();
                        }
                        /** FB 2447 **/

                        conf.IViewNumber = ds1.Tables[dsTable1].Rows[0]["internalbridge"].ToString().Trim();//FB 2556
					}
					catch (Exception e)
					{
						logger.Trace ("Advanced Audio/Video Params: " + e.Message);
					}
				}
				#endregion												
                //ALLDEV-837 Starts 
                conf.iEnableSyncLaunchBuffer = int.Parse(ds.Tables[dsTable].Rows[0]["EnableSyncLaunchBuffer"].ToString());
                conf.iSyncLaunchBuffer = int.Parse(ds.Tables[dsTable].Rows[0]["SyncLaunchBuffer"].ToString());
                //ALLDEV-837 Ends
			}
			catch(Exception e)
			{	
				logger.Exception(100,e.Message);
				return false;
			}

			return true;
		}
		//FB 2441 Starts
        internal bool FetchConf(ref List<NS_MESSENGER.Conference> confsList,ref List<NS_MESSENGER.Conference> confs,NS_MESSENGER.MCU.eType MCU,  int delStatus) // FB 2441 II  //ZD 100221
        {

            DataSet dsTZ = null;
            string dsTableTZ = "TZName";
            string queryTZ = "";
            bool retTZ = false;
            DataSet ds = new DataSet();
            string dsTable = "Conference";
			//FB 2441 II Starts
            string dtsync = "syncConference";
            string query = null,syncQuery=""; bool ret = false;
            DataSet dsTZ1 = new DataSet();
            DataSet dssync = new DataSet();
			//FB 2441 II Ends
            string dsTableTZ1 = "LocalTZ";
            string queryTZ1 = "";
            bool retTZ1 = false;
            NS_MESSENGER.Conference conf = null;
            NS_MESSENGER.Conference templateConf = null;
            int tbleCount = 0;
            try
            {
                if (confsList != null && confsList.Count > 0)
                {
                    templateConf = confsList[0];

                    //query = "Select ConfNumName,ESId,PushedToExternal,Etag,ConfDate,SetupTime,TearDownTime,Timezone,GUID,DialString from Conf_Conference_D where  deleted = 0 and status = 0 and confid = '" + templateConf.iDbID.ToString() + "'";

                    if (NS_MESSENGER.MCU.eType.Webex == MCU)
                    {
                        if (delStatus == 1)
                            query = "Select * from Conf_Conference_D where  deleted = 1 and status = 9 and confid = '" + templateConf.iDbID.ToString() + "'";   //ZD 100221
                        else
                        if (delStatus == 2)
                            query = "Select * from Conf_Conference_D where  deleted = 2 and status = 3 and confid = '" + templateConf.iDbID.ToString() + "'";   //ZD 100221
                        else
                            if (delStatus == 3)
                                query = "Select * from Conf_Conference_D where  deleted = 0 and status = 1 and confid = '" + templateConf.iDbID.ToString() + "'";   //ZD 100221
                        else
                            query = "Select * from Conf_Conference_D where  deleted = 0 and status = 0 and confid = '" + templateConf.iDbID.ToString() + "'"; //ZD 100221

                    }
                    else
                        if (delStatus == 1) //Delete mode //ZD 100085
                            query = "Select * from Conf_Conference_D where  deleted = 1 and status = 9 and confid = '" + templateConf.iDbID.ToString() + "'";
                        else
                            query = "Select * from Conf_Conference_D where  deleted = 0 and status = 0 and confid = '" + templateConf.iDbID.ToString() + "'";
                        
                    

                    ret = false;
                    ret = SelectCommand(query, ref ds, dsTable);
                    if (!ret)
                    {
                        logger.Exception(100, "SQL error");
                        return (false);
                    }

                    if (ds.Tables[dsTable].Rows.Count < 1)
                    {
                        // no confs 
                        logger.Trace("No conf found.");
                        return false;
                    }
                    if (ds.Tables[dsTable] != null)
                    {
                        for (tbleCount = 0; tbleCount < ds.Tables[dsTable].Rows.Count; tbleCount++)
                        {
 							// FB 2441 II Starts
                            dssync.Reset();
                            //ZD 100221 starts
                            switch (MCU.ToString())
                            {
                                case "RPRM":
                                    syncQuery = "Select Etag,ESId,instanceID,DialString from Conf_SyncMCUAdj_D where  deletedecision = 0  and confid = '" + templateConf.iDbID.ToString() + "' and instanceid=" + Int32.Parse(ds.Tables[dsTable].Rows[tbleCount]["InstanceId"].ToString()); //ZD 104496
                                    break;
                                case "TMSScheduling":
                                    syncQuery = "Select Etag,ESId,instanceID from Conf_SyncMCUAdj_D where  deletedecision = 0  and confid = '" + templateConf.iDbID.ToString() + "' and instanceid=" + Int32.Parse(ds.Tables[dsTable].Rows[tbleCount]["InstanceId"].ToString());
                                    break;
                                case "RADVISIONIVIEW":
                                    syncQuery = "Select Etag,ESId,instanceID from Conf_SyncMCUAdj_D where  deletedecision = 0  and confid = '" + templateConf.iDbID.ToString() + "' and instanceid=" + Int32.Parse(ds.Tables[dsTable].Rows[tbleCount]["InstanceId"].ToString());
                                    break;
                                case "Webex":
                                    syncQuery = "Select WebEXMeetingKey from Conf_SyncMCUAdj_D where  webexinstancehandle = 0  and confid = '" + templateConf.iDbID.ToString() + "' and instanceid=" + Int32.Parse(ds.Tables[dsTable].Rows[tbleCount]["InstanceId"].ToString());
                                    break;
                                case "BLUEJEANS": //ZD 103263
                                    syncQuery = "Select BJNUniqueid,BJNUserId,instanceID from Conf_SyncMCUAdj_D where  deletedecision = 0  and confid = '" + templateConf.iDbID.ToString() + "' and instanceid=" + Int32.Parse(ds.Tables[dsTable].Rows[tbleCount]["InstanceId"].ToString());
                                    break;
                                default:
                                    syncQuery = "";
                                    break;
                            }
                            //ZD 100221 Ends
                            ret = false;
                            ret = SelectCommand(syncQuery, ref dssync, dtsync);
                            if (!ret)
                            {
                                logger.Exception(100, "SQL error");
                                continue;
                            }
							// FB 2441 II Ends

                            conf = NS_MESSENGER.ObjectHelper.DeepClone<NS_MESSENGER.Conference>(templateConf);

                            conf.iDbID = Int32.Parse(ds.Tables[dsTable].Rows[tbleCount]["ConfId"].ToString());
                            conf.iInstanceID = Int32.Parse(ds.Tables[dsTable].Rows[tbleCount]["InstanceId"].ToString());

                            conf.iDbNumName = Int32.Parse(ds.Tables[dsTable].Rows[tbleCount]["ConfNumName"].ToString());
                            //ZD 100513 Starts
                            conf.iConfType = Int32.Parse(ds.Tables[dsTable].Rows[tbleCount]["conftype"].ToString());
                            conf.iOBTP = Int32.Parse(ds.Tables[dsTable].Rows[tbleCount]["isOBTP"].ToString());
                            conf.iWebExconf = Int32.Parse(ds.Tables[dsTable].Rows[tbleCount]["WebExConf"].ToString());
                            //ZD 100513 Ends
                            //ZD 100221 starts
                            int temp = 0;
                            if (ds.Tables[dsTable].Columns["owner"] != null)
                                int.TryParse(ds.Tables[dsTable].Rows[tbleCount]["owner"].ToString(), out temp);
                            conf.iHostUserID = temp;

                            temp = 0;
                            if (ds.Tables[dsTable].Columns["Duration"] != null)
                                int.TryParse(ds.Tables[dsTable].Rows[0]["Duration"].ToString(), out temp);
                            conf.iDuration = temp;

                            temp = 0;
                            if (ds.Tables[dsTable].Columns["McuSetupTime"] != null)
                                int.TryParse(ds.Tables[dsTable].Rows[0]["McuSetupTime"].ToString(), out temp);
                            conf.iMcuSetupTime = temp;

                            temp = 0;
                            if (ds.Tables[dsTable].Columns["MCUTeardonwnTime"] != null)
                                int.TryParse(ds.Tables[dsTable].Rows[0]["MCUTeardonwnTime"].ToString(), out temp);
                            conf.iMCUTeardonwnTime = temp;

                            if (ds.Tables[dsTable].Columns["Description"] != null)
                                conf.sDescription = ds.Tables[dsTable].Rows[0]["Description"].ToString();

                            if (ds.Tables[dsTable].Columns["ExternalName"] != null)
                                conf.sExternalName = ds.Tables[dsTable].Rows[0]["ExternalName"].ToString();

                            if (conf.iHostUserID > 0)
                            {
                                #region Fetch the host email
                                DataSet ds2 = new DataSet();
                                string dsTable2 = "ConfHost";
                                string query2 = "Select Email, FirstName + ' ' + LastName as HostName from Usr_List_D where userid = " + conf.iHostUserID.ToString();
                                bool ret3 = SelectCommand(query2, ref ds2, dsTable2);
                                if (!ret3)
                                {
                                    logger.Exception(100, "SQL error");
                                    return (false);
                                }

                                if (ds2.Tables[dsTable2].Rows.Count < 1)
                                {
                                    // no record 
                                    logger.Trace("No host email found.");
                                    return false;
                                }

                                conf.sHostEmail = ds2.Tables[dsTable2].Rows[0]["Email"].ToString();
                                conf.sHostName = ds2.Tables[dsTable2].Rows[0]["HostName"].ToString();
                                #endregion
                            }
                            temp = 0;
                            if (ds.Tables[dsTable].Columns["orgId"] != null)
                                int.TryParse(ds.Tables[dsTable].Rows[0]["orgId"].ToString(), out temp);
                            conf.iOrgID = temp;

                            temp = 0;
                            if (ds.Tables[dsTable].Columns["Recuring"] != null)
                                int.TryParse(ds.Tables[dsTable].Rows[0]["Recuring"].ToString(), out temp);
                            conf.iRecurring = temp;

                            temp = 0;
                            if (ds.Tables[dsTable].Columns["webexconf"] != null)
                            {
                                int.TryParse(ds.Tables[dsTable].Rows[0]["webexconf"].ToString(), out temp);
                                conf.iWebExconf = temp;
                            }
                           

                            //ZD 100221 Ends

							// FB 2441 II Starts 
                            if (dssync.Tables[0].Rows.Count >0)
                            {
                                if (dssync.Tables[0].Columns["ESId"]!=null)//ZD 100221 
                                    conf.ESId = dssync.Tables[0].Rows[0]["ESId"].ToString();
                                if (dssync.Tables[0].Columns["Etag"] != null)//ZD 100221 
                                    conf.sEtag = dssync.Tables[0].Rows[0]["Etag"].ToString();
                                if (dssync.Tables[0].Columns["WebEXMeetingKey"] != null)//ZD 100221 
                                    conf.sWebEXMeetingKey = dssync.Tables[0].Rows[0]["WebEXMeetingKey"].ToString();//ZD 100221 
                                //ZD 103263 Start
                                if (dssync.Tables[0].Columns["BJNUniqueid"] != null)
                                    conf.sBJNUniqueid = dssync.Tables[0].Rows[0]["BJNUniqueid"].ToString();
                                if (dssync.Tables[0].Columns["BJNUserId"] != null)
                                    conf.sBJNUserId = dssync.Tables[0].Rows[0]["BJNUserId"].ToString();
                                //ZD 103263 end 
                                if (dssync.Tables[0].Columns["DialString"] != null) //ZD 104496
                                    conf.sDialString = dssync.Tables[0].Rows[0]["DialString"].ToString();
                            }
                            else
                            {
                                conf.ESId = ds.Tables[dsTable].Rows[tbleCount]["ESId"].ToString();
                                conf.sEtag = ds.Tables[dsTable].Rows[tbleCount]["Etag"].ToString();
                                if (ds.Tables[dsTable].Columns["WebEXMeetingKey"] != null)//ZD 100221 
                                    conf.sWebEXMeetingKey = ds.Tables[dsTable].Rows[tbleCount]["WebEXMeetingKey"].ToString();//ZD 100221 
                                //ZD 103263 Start
                                if (ds.Tables[dsTable].Rows[tbleCount]["BJNUniqueid"] != null)
                                    conf.sBJNUniqueid = ds.Tables[dsTable].Rows[tbleCount]["BJNUniqueid"].ToString();
                                if (ds.Tables[dsTable].Rows[tbleCount]["BJNUserId"] != null)
                                    conf.sBJNUserId = ds.Tables[dsTable].Rows[tbleCount]["BJNUserId"].ToString();
                                //ZD 103263 End
                                if (ds.Tables[dsTable].Rows[tbleCount]["DialString"] != null) //ZD 104496
                                    conf.sDialString = ds.Tables[dsTable].Rows[tbleCount]["DialString"].ToString();
                            }
							// FB 2441 II Ends
                            //ZD 103263 Start
                            if (ds.Tables[dsTable].Rows[tbleCount]["isBJNConf"] != null)
                                conf.iBJNConf = int.Parse(ds.Tables[dsTable].Rows[tbleCount]["isBJNConf"].ToString());
                            //ZD 103263 End
                            conf.iPushtoExternal = int.Parse(ds.Tables[dsTable].Rows[tbleCount]["PushedToExternal"].ToString());
                            ret = false;
                            ret = GenerateConfName(conf, ref conf.sDbName);
                            if (!ret)
                            {
                                logger.Trace("Failure in generating conf name for bridges.");
                                return false;
                            }//ZD 100085 Starts
                            //ZD 104332 starts
                            if (conf.iImmediate == 1)
                                conf.dtStartDateTime = DateTime.Parse(ds.Tables[dsTable].Rows[tbleCount]["ConfDate"].ToString());
                            else
                                conf.dtStartDateTime = DateTime.Parse(ds.Tables[dsTable].Rows[tbleCount]["ConfDate"].ToString()).AddSeconds(-40); //FB 2440 // 5 seconds buffer
                            //ZD 104332 ends

                            conf.dtSetUpDateTime = DateTime.Parse(ds.Tables[dsTable].Rows[tbleCount]["SetupTime"].ToString());
                            logger.Trace("Start Time : " + conf.dtStartDateTime.ToString("T"));
							//ZD 100085 Starts
                            int.TryParse(ds.Tables[dsTable].Rows[tbleCount]["duration"].ToString(), out conf.iDuration);
                            conf.dtEndDateTime = conf.dtStartDateTime.AddMinutes(conf.iDuration); //ZD 100085
                            //conf.dtEndDateTime = DateTime.Parse(ds.Tables[dsTable].Rows[tbleCount]["TearDownTime"].ToString());//FB 2440
							//ZD 100085 End
                            // conf timezone
                            conf.iTimezoneID = Int32.Parse(ds.Tables[dsTable].Rows[tbleCount]["Timezone"].ToString());
                            logger.Trace("TimezoneID : " + conf.iTimezoneID.ToString());

                            // timezone 
                            if (dsTZ == null)
                            {
                                dsTZ = new DataSet();
                                dsTableTZ = "TZName";
                                queryTZ = "select timezone,timezonediff from gen_timezone_s where timezoneID = " + conf.iTimezoneID.ToString();
                                retTZ = SelectCommand(queryTZ, ref dsTZ, dsTableTZ);
                                if (!retTZ)
                                {
                                    logger.Exception(100, "SQL error");
                                    return (false);
                                }
                            }
                            conf.sName_StartDateTimeInLocalTZ = dsTZ.Tables[dsTableTZ].Rows[0]["timezonediff"].ToString() + dsTZ.Tables[dsTableTZ].Rows[0]["timezone"].ToString();
                            // start date & time in UTC
                            conf.dtStartDateTimeInUTC = conf.dtStartDateTime;
                            // Convert to local TZ
                            dsTZ1 = new DataSet();
                            dsTableTZ1 = "LocalTZ";
                            queryTZ1 = "select dbo.changeTime (" + conf.iTimezoneID.ToString() + ",'" + conf.dtStartDateTimeInUTC + "')";
                            retTZ1 = SelectCommand(queryTZ1, ref dsTZ1, dsTableTZ1);
                            if (!retTZ1)
                            {
                                logger.Exception(100, "SQL error");
                                return (false);
                            }
                            conf.dtStartDateTimeInLocalTZ = DateTime.Parse(dsTZ1.Tables[dsTableTZ1].Rows[0][0].ToString());
                            conf.sGUID = ds.Tables[dsTable].Rows[tbleCount]["GUID"].ToString();
                            //conf.sDialString = ds.Tables[dsTable].Rows[tbleCount]["DialString"].ToString();

                            confs.Add(conf);// FB 2441 II
                        }
                    }
                }

            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }

            return true;
        }

        //ZD 100221 Starts
        internal bool FetchSyncAdjustments(ref List<NS_MESSENGER.Conference> confsList,NS_MESSENGER.MCU.eType Fetch) //ZD 100221
        {

           
            DataSet ds = new DataSet();
            string dsTable = "Conference";
            string query = null; bool ret = false;
            DataSet dsTZ1 = new DataSet();
            NS_MESSENGER.Conference conf = null;
            NS_MESSENGER.Conference templateConf = null;
            int tbleCount = 0;
            try
            {
                if (confsList != null && confsList.Count > 0)
                {
                    templateConf = confsList[0];
                    confsList = new List<NS_MESSENGER.Conference>();
                    //ZD 100221 Starts
                    switch (Fetch.ToString())
                    {
                        case "RPRM":
                            query = "Select Confid,instanceID,Confnumname,esID,Etag,DialString,BridgeID from Conf_SyncMCUAdj_D" // FB 2441 II
                                     + " where deleteDecision = 1 and  confid = '" + templateConf.iDbID.ToString() + "'";
                            break;
                        case "TMSScheduling":
                            query = "Select Confid,instanceID,Confnumname,esID,Etag,DialString,BridgeID from Conf_SyncMCUAdj_D" // FB 2441 II
                                    + " where deleteDecision = 1 and  confid = '" + templateConf.iDbID.ToString() + "'";
                            break;
                        case "RADVISIONIVIEW":
                            query = "Select Confid,instanceID,Confnumname,esID,Etag,DialString,BridgeID from Conf_SyncMCUAdj_D" // FB 2441 II
                                     + " where deleteDecision = 1 and  confid = '" + templateConf.iDbID.ToString() + "'";
                            break;
                        case "Webex":
                            if (templateConf.iWebexExceptionHandler == 1)
                            {
                                query = "delete Conf_SyncMCUAdj_D where confid='" + templateConf.iDbID.ToString() + "' and Webexinstancehandle = 0  ;Select Confid,instanceID,Confnumname,webexmeetingkey from Conf_SyncMCUAdj_D" // FB 2441 II
                                        + " where Webexinstancehandle = 1 and  confid = '" + templateConf.iDbID.ToString() + "'";
                            }
                            else
                            if (templateConf.iWebexExceptionHandler == 0)
                            {
                                query = "Select Confid,instanceID,Confnumname,webexmeetingkey from Conf_SyncMCUAdj_D"
                                        + " where confid = '" + templateConf.iDbID.ToString() + "';delete Conf_SyncMCUAdj_D where confid='" + templateConf.iDbID.ToString() + "'";
                            }
                            break;
                        case "BLUEJEANS": //ZD 103263
                            query = "Select Confid,instanceID,Confnumname,BJNUniqueid,BJNUserId from Conf_SyncMCUAdj_D" 
                                     + " where deleteDecision = 1 and  confid = '" + templateConf.iDbID.ToString() + "'";
                            break;
                        default:
                            query = "";
                            break;
                    };
                    

                    //ZD 100221 Ends
                    ret = false;
                    ret = SelectCommand(query, ref ds, dsTable);
                    if (!ret)
                    {
                        logger.Exception(100, "SQL error");
                        return (false);
                    }

                    if (ds.Tables[dsTable].Rows.Count < 1)
                    {
                        // no confs 
                        logger.Trace("No conf found.");
                        return true; //FB 2441 II
                    }
                    if (ds.Tables[dsTable] != null)
                    {
                        for (tbleCount = 0; tbleCount < ds.Tables[dsTable].Rows.Count; tbleCount++)
                        {
                            conf = new NS_MESSENGER.Conference();
							//ZD 100221 Starts
                            if(!string.IsNullOrEmpty(ds.Tables[dsTable].Rows[tbleCount]["Confid"].ToString()))
                                 conf.iDbID = Int32.Parse(ds.Tables[dsTable].Rows[tbleCount]["Confid"].ToString());
                            if (!string.IsNullOrEmpty(ds.Tables[dsTable].Rows[tbleCount]["instanceID"].ToString()))
                                conf.iInstanceID = Int32.Parse(ds.Tables[dsTable].Rows[tbleCount]["instanceID"].ToString());
                            if (!string.IsNullOrEmpty(ds.Tables[dsTable].Rows[tbleCount]["Confnumname"].ToString()))
                                conf.iDbNumName = Int32.Parse(ds.Tables[dsTable].Rows[tbleCount]["Confnumname"].ToString());
                            if (ds.Tables[dsTable].Columns["esID"]!=null)
                                conf.ESId = ds.Tables[dsTable].Rows[tbleCount]["esID"].ToString();
                            if (ds.Tables[dsTable].Columns["Etag"] != null)
                                conf.sEtag = ds.Tables[dsTable].Rows[tbleCount]["Etag"].ToString();
                            if (ds.Tables[dsTable].Columns["DialString"] != null)
                                conf.sDialString = ds.Tables[dsTable].Rows[tbleCount]["DialString"].ToString();
                            if (ds.Tables[dsTable].Columns["BridgeID"] != null)
                                conf.cMcu.iDbId = Int32.Parse(ds.Tables[dsTable].Rows[tbleCount]["BridgeID"].ToString()); // FB 2441 II
                            if (ds.Tables[dsTable].Columns["webexmeetingkey"] != null)
                                conf.sWebEXMeetingKey = ds.Tables[dsTable].Rows[tbleCount]["webexmeetingkey"].ToString();
                            //ZD 100221 Ends
                            //ZD 103263 Start
                            if (ds.Tables[dsTable].Columns["BJNUniqueid"] != null)
                                conf.sBJNUniqueid = ds.Tables[dsTable].Rows[tbleCount]["BJNUniqueid"].ToString(); 
                            if (ds.Tables[dsTable].Columns["BJNUserId"] != null)
                                conf.sBJNUserId = ds.Tables[dsTable].Rows[tbleCount]["BJNUserId"].ToString();
                            //ZD 103263 End
                            confsList.Add(conf);
                        }
                    }
                }

            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }

            return true;
        }

		//FB 2441 Ends

		internal bool ChangeTimeToAnotherTimeZone(int timezoneID,ref DateTime dt)
		{
			try
			{
				DataSet ds1 = new DataSet();
				string dsTable1 = "ConfTime";
				string query1 = "select dbo.changeTime(" + timezoneID.ToString() + ",'" + dt.ToString()+"')";
				bool ret1 = SelectCommand(query1,ref ds1,dsTable1);
				if (!ret1)
				{
					logger.Exception(100,"SQL error");
					return (false);
				}
				else
				{					
					dt = DateTime.Parse(ds1.Tables[dsTable1].Rows[0][0].ToString().Trim()); 
				}
		
				return true;
			}
			catch (Exception e)
			{
				logger.Exception (100,e.Message);
				return false;
			}
		}
		
		internal bool ChangeGMTTimeToUserPreferredTimeZone(int userID,ref DateTime dt)
		{
			try
			{
				DataSet ds1 = new DataSet();
				string dsTable1 = "ConfTime";
				string query1 = "select dbo.userPreferedTime(" + userID.ToString() + ",'" + dt.ToString()+"')";
				bool ret1 = SelectCommand(query1,ref ds1,dsTable1);
				if (!ret1)
				{
					logger.Exception(100,"SQL error");
					return (false);
				}
				else
				{					
					dt = DateTime.Parse(ds1.Tables[dsTable1].Rows[0][0].ToString().Trim()); 
				}
		
				return true;
			}
			catch (Exception e)
			{
				logger.Exception (100,e.Message);
				return false;
			}
		}


		internal bool FetchUserTimeZone (int userid,ref string userTimeZone)
		{
			try
			{
				DataSet ds1 = new DataSet();
				string dsTable1 = "UserTimeZone";
				string query1 = "select t.timezone from usr_list_d u,gen_timezone_s t where u.timezone =t.timezoneid and u.userid = " + userid.ToString();
				bool ret1 = SelectCommand(query1,ref ds1,dsTable1);
				if (!ret1)
				{
					logger.Exception(100,"SQL error");
					return (false);
				}
				else
				{					
					userTimeZone = ds1.Tables[dsTable1].Rows[0][0].ToString().Trim(); 
				}
					
				return true;
			}
			catch (Exception e)
			{
				logger.Exception (100,e.Message);
				return false;
			}
		}

		internal bool FetchRoomName(int roomid,ref string roomName)
		{
			
			try
			{
				DataSet ds1 = new DataSet();
				string dsTable1 = "RoomName";
				string query1 = "select name from loc_room_d where roomid = " + roomid.ToString();
				bool ret1 = SelectCommand(query1,ref ds1,dsTable1);
				if (!ret1)
				{
					logger.Exception(100,"SQL error");
					return (false);
				}
				else
				{			
					if (ds1.Tables[dsTable1].Rows.Count > 0 )
					{
						roomName = ds1.Tables[dsTable1].Rows[0][0].ToString().Trim(); 
					}
					else
					{
						logger.Trace (" Room not found.");
						return false;
					}
				}
					
				return true;
			}
			catch (Exception e)
			{
				logger.Exception (100,e.Message);
				return false;
			}
		}



		public bool FetchConfRooms (int confid,int instanceid, ref Queue roomList)
		{
			try
			{
				DataSet ds = new DataSet();
				string dsTable = "ConfTime";
				string query = "select l.roomid, l.[name] from loc_room_d l,conf_room_d c where l.roomid = c.roomid and c.confid = " + confid.ToString() + " and c.instanceid = " + instanceid.ToString();
				bool ret = SelectCommand(query,ref ds,dsTable);
				if (!ret)
				{
					logger.Exception(100,"SQL error");
					return (false);
				}
				for (int i=0;i<ds.Tables[dsTable].Rows.Count ; i++)				
				{			
					NS_PERSISTENCE.Room room = new NS_PERSISTENCE.Room();
					room.roomID = Int32.Parse(ds.Tables[dsTable].Rows[i]["roomid"].ToString().Trim()); 
					room.name = ds.Tables[dsTable].Rows[i]["name"].ToString().Trim(); 
					roomList.Enqueue(room);
				}
				return true;
			}
			catch (Exception e)
			{
				logger.Exception(100,e.Message);
				return false;
			}
		}
		public bool FetchConfInfo(ref NS_MESSENGER.Conference conf)
		{
			try 
			{
				// Retreive conf details
				bool ret = FetchConf(ref conf);
				int isVMRConf =0; //ZD 100522
				//End Time needs to be adjusted
				DataSet ds = new DataSet();
				string dsTable = "Conference";
				string query = "select confdate,duration,isVMR from conf_conference_d where confid = " + conf.iDbID.ToString() + " and instanceid = " + conf.iInstanceID.ToString(); 
				ret = false;
				ret = SelectCommand(query,ref ds,dsTable);
				if (!ret)
				{
					logger.Exception(100,"SQL error");
					return (false);
				}		

				if (ds.Tables[dsTable].Rows.Count < 1 )
				{
					// no confs 
					return false;
				}

				// start date & time
                //ZD 104332 starts
                if (conf.iImmediate == 1)
                    conf.dtStartDateTime = DateTime.Parse(ds.Tables[dsTable].Rows[0]["ConfDate"].ToString()); //FB 2440 
                else
                    conf.dtStartDateTime = DateTime.Parse(ds.Tables[dsTable].Rows[0]["ConfDate"].ToString()).AddSeconds(-40); //FB 2440 //5 secs buffer

                //ZD 104332 ends

                logger.Trace ("Start Time : " + conf.dtStartDateTime.ToString("T"));

                conf.dtStartDateTimeInUTC = conf.dtStartDateTime; //FB 2501 Call Monitoring
                
				// duration
				conf.iDuration = Int32.Parse(ds.Tables[dsTable].Rows[0]["Duration"].ToString());				
				logger.Trace ("Conf Duration : " + conf.iDuration.ToString());
				
                //ZD 100522 Starts
                if (!string.IsNullOrEmpty(ds.Tables[dsTable].Rows[0]["isVMR"].ToString()))
                    int.TryParse(ds.Tables[dsTable].Rows[0]["isVMR"].ToString(), out isVMRConf);
                conf.iIsVMR = isVMRConf;
                //ZD 100522 Ends
               
				return true;
			}
			catch(Exception e)
			{	
				logger.Exception(100,e.Message);
				return false;
			}
		}
        
        internal bool EndpointModelType(int videoEquipmentID, ref NS_MESSENGER.Party.eModelType etModelType) //ZD 101363
        {
                DataSet ds = new DataSet();
                string dsTable = "VEquipment";
            	string query = "Select VEname from Gen_VideoEquipment_S where  VEid = " + videoEquipmentID.ToString();                
				bool ret = SelectCommand(query,ref ds,dsTable);
				if (!ret)
				{
					logger.Exception(100,"SQL error");
					return (false);
				}

				for (int i=0;i<ds.Tables[dsTable].Rows.Count ; i++)
				{
					try
					{
                        //party ip/isdn address
                        string videoEquipmentName = ds.Tables[dsTable].Rows[i]["VEName"].ToString().Replace("*", "");//ZD 100736
                        //FB 2390 Start
                        if (videoEquipmentName.Contains("HDX") || videoEquipmentName.Contains("RealPresence") || videoEquipmentName.Contains("QDX")) //ZD 100520 //ZD 100616
                        {
                            etModelType = NS_MESSENGER.Party.eModelType.POLYCOM_HDX;                            
                        }
                        else if (videoEquipmentName.Contains("VSX"))
                        {
                            etModelType = NS_MESSENGER.Party.eModelType.POLYCOM_VSX;
                        }
                        else if (videoEquipmentName.Contains("Tandberg"))
                        {
                            etModelType = NS_MESSENGER.Party.eModelType.TANDBERG;
                        }
                        else if (videoEquipmentName.Contains("OTX"))//FB 2335
                        {
                            etModelType = NS_MESSENGER.Party.eModelType.POLYCOM_OTX;
                        }
                        else if (videoEquipmentName.Contains("RPX"))//FB 2335
                        {
                            etModelType = NS_MESSENGER.Party.eModelType.POLYCOM_RPX;
                        }
                        else if (videoEquipmentName.Contains("MXP"))
                        {
                            etModelType = NS_MESSENGER.Party.eModelType.MXP;
                        }
                        else if (videoEquipmentName.Contains("Cisco"))
                        {
                            //ZD 100519 Start
                            if (videoEquipmentName.Contains("TX") || videoEquipmentName.Contains("Telepresence"))
                                etModelType = NS_MESSENGER.Party.eModelType.CISCO_TS_TX;
                            else
                                etModelType = NS_MESSENGER.Party.eModelType.CISCO;
                            //ZD 100519 End
                        }
                        else if (videoEquipmentName.Contains("CTS")) //ZD 101363
                        {
                            etModelType = NS_MESSENGER.Party.eModelType.CISCO_TS_TX;
                        }
                        else if (videoEquipmentName.Contains("ViewStation"))
                        {
                            etModelType = NS_MESSENGER.Party.eModelType.ViewStation;
                        }
                        else
                        {
                            etModelType = NS_MESSENGER.Party.eModelType.OTHER;
                        }
                        //FB 2390 End

                        logger.Trace("Endpoint model type: " + etModelType.ToString());
   
                    }
                    catch (Exception e)
                    {
                        logger.Exception(200, e.Message);                      
                    }
                 
                }
                return true;
        }

		public bool FetchRooms(int confid,int instanceid,ref Queue partyq,int mode)
		{
            NS_MESSENGER.Party partyMulti = null; //Fb 2400
            cryptography.Crypto crypto = null; //ZD 100601
			try 
			{
                bool isPartyAdded = false; //FB 2400
                int isTelepres = 0;//FB 2400
                string partyName = "";
				DataSet ds = new DataSet();
				string dsTable = "Rooms";

                string query = "Select distinct r.RoomId, e.[name] as endpointName,c.ipisdnaddress, c.GUID"; //FB 2501 -  //ALLDEV-807
				query += ",c.ConnectionType,c.roomid,c.bridgeid,c.bridgeipisdnaddress";//ZD 100132
                query += ",C.audioorvideo,C.outsidenetwork,C.GateKeeeperAddress,C.mcuservicename, e.videoequipmentid";
                query += ",C.AddressType,C.DefLineRate,C.isLecturer, C.Connect2,C.MultiCodecAddress,e.isTelePresence"; //FB 2400
                query += ",e.password,c.APIPortNo,c.confuId,c.isTextMsg,c.terminaltype,c.OnlineStatus,e.NetworkURL,e.Secureport ,";
                //case When la.AssistantId = 0 Then  la.EmailId else u.Email  end as GuestContactEmail "; //ALLDEV-807
                query += "(select top 1 case when l.AssistantId =0 then l.EmailId else u.Email end from Loc_Assistant_D l, Usr_List_D u "; //ALLDEV-807
                query += "where  l.AssistantId = u.UserID and r.RoomID=l.Roomid order by l.ID) as GuestContactEmail";
                query += ", e.UserName,c.Partynameonmcu,e.SSHSupport,c.SysLocationId"; //ZD 100814 //ZD 101056 //ZD 101363 //ZD104821
                query += " from Loc_Room_D r,Conf_Room_D c,Ept_List_D e"; //ALLDEV-807
				query += " where c.confid = " + confid.ToString();
				query += " and c.instanceid = " + instanceid.ToString();
				query += " and c.roomid = r.roomid";
				query += " and r.endpointid = e.endpointid";
                query += " and c.profileid = e.profileid ";
                //and la.RoomId = r.RoomID"; //ALLDEV-807
                //query += " and r.Assistant = u.userid";//FB 2441 ZD 100619

				if (mode!= 0) 
				{
					query += " and C.connectstatus = " + mode.ToString();
				}

				bool ret = SelectCommand(query,ref ds,dsTable);
				if (!ret)
				{
					logger.Exception(100,"SQL error");
					return (false);
				}

				for (int i=0;i<ds.Tables[dsTable].Rows.Count ; i++)
				{
					try
					{
						NS_MESSENGER.Party party = new  NS_MESSENGER.Party();

						// name
                        if(!string.IsNullOrEmpty(ds.Tables[dsTable].Rows[i]["endpointName"].ToString())) //ZD 100588
						    party.sName = ds.Tables[dsTable].Rows[i]["endpointName"].ToString().Trim();
                        if (!string.IsNullOrEmpty(ds.Tables[dsTable].Rows[i]["Partynameonmcu"].ToString())) //ZD 100588
                            party.sMcuName = ds.Tables[dsTable].Rows[i]["Partynameonmcu"].ToString().Trim();// ZD 101056

						logger.Trace ("Room endpoint = "+ party.sName);
					
						//ip/isdn address
						party.sAddress = ds.Tables[dsTable].Rows[i]["ipisdnaddress"].ToString();

                        //ZD 100814
                        if (ds.Tables[dsTable].Rows[i]["UserName"].ToString() != "")
                            party.sLogin = ds.Tables[dsTable].Rows[i]["UserName"].ToString();
                        
                        //ZD 100601 - Start
                        crypto = new Crypto();
                        if (ds.Tables[dsTable].Rows[i]["password"].ToString() != null && ds.Tables[dsTable].Rows[i]["password"].ToString() != "")
                            party.sPwd = crypto.decrypt(ds.Tables[dsTable].Rows[i]["password"].ToString());
                        //ZD 100601 - End

                        // Port address fo ept API port 
                        int iAPIPort = 23, iSecurePort = 0;//FB 2595
                        if (ds.Tables[dsTable].Rows[i]["APIPortNo"].ToString() != "")
                            Int32.TryParse(ds.Tables[dsTable].Rows[i]["APIPortNo"].ToString().Trim(), out iAPIPort);

                        if (iAPIPort <= 0)
                            iAPIPort = 23;

                        party.iAPIPortNo = iAPIPort;

                        //ZD 104821 Starts
                        iAPIPort = 0;
                        int.TryParse(ds.Tables[dsTable].Rows[i]["SysLocationId"].ToString(), out iAPIPort);
                        party.SysLocationID = iAPIPort;
                        //Zd 104821 Ends

                        //FB 2595 Start

                        if (ds.Tables[dsTable].Rows[i]["Secureport"].ToString() != "")
                            Int32.TryParse(ds.Tables[dsTable].Rows[i]["Secureport"].ToString().Trim(), out iSecurePort);
                        if (iSecurePort > 0)
                            party.iSecureport = iSecurePort;
                        else
                            iSecurePort = 5000;

                        if (ds.Tables[dsTable].Rows[i]["NetworkURL"].ToString() != "")
                            party.sNetworkURL = ds.Tables[dsTable].Rows[i]["NetworkURL"].ToString();

                        //FB 2595 End

                        // connection type
                        party.isRoom = true;//Polycom CMA
                        party.iconfnumname = Int32.Parse(ds.Tables[dsTable].Rows[i]["confuId"].ToString());//FB 2390

                        int iConnectionType = 0;
                        iConnectionType = Int32.Parse(ds.Tables[dsTable].Rows[i]["ConnectionType"].ToString());
                        ret = ConvertToConnectionType(iConnectionType, ref party);
                        if (!ret)
                        {
                            logger.Trace("Participant connection type is incorrect.");
                            return false;
                        }
				
					
						// room id . 
						// Magic Number = 10000 . Needed so that it doesnot clash with user-id's.
                        party.iDbId = int.Parse(ds.Tables[dsTable].Rows[i]["roomid"].ToString()) + 10000;
                        party.iEndpointId = int.Parse(ds.Tables[dsTable].Rows[i]["roomid"].ToString()); //FB 2989					

					
						//bridgeid
						party.iMcuId = Int32.Parse(ds.Tables[dsTable].Rows[i]["bridgeid"].ToString());
					
						//bridgeaddress
						party.sMcuAddress = ds.Tables[dsTable].Rows[i]["bridgeipisdnaddress"].ToString();
                        logger.Trace("MCU Address: " + party.sMcuAddress);

						// Fetch the Bridge info
						logger.Trace ("Getting the mcu for the room...");
						bool ret4 = FetchMcu(party.iMcuId ,ref party.cMcu);
						if (!ret4)
						{
							logger.Exception(100,"Error in fetching MCU info");
							continue;
						}
					
						// audio or video 
						int audioOrVideo = Int32.Parse(ds.Tables[dsTable].Rows[i]["audioOrvideo"].ToString());
                        if (audioOrVideo == 1)  //FB 1740 //FB 1744
                        {
                            logger.Trace("Call Type: Audio");
                            party.etCallType = NS_MESSENGER.Party.eCallType.AUDIO;
                        }
                        else
                        {
                            logger.Trace("Call Type: Video");
                            party.etCallType = NS_MESSENGER.Party.eCallType.VIDEO;
                        }

						//outside network
						int outsideNetwork = Int32.Parse(ds.Tables[dsTable].Rows[i]["outsidenetwork"].ToString());
						if (outsideNetwork == 1)
							party.bIsOutsideNetwork = true;

                        //GateKeeperAddress ZD 100132
                        if (ds.Tables[dsTable].Rows[i]["GateKeeeperAddress"].ToString() != null)
                            if (ds.Tables[dsTable].Rows[i]["GateKeeeperAddress"].ToString().Trim() != "")
                                party.sGateKeeeperAddress = ds.Tables[dsTable].Rows[i]["GateKeeeperAddress"].ToString();

						//mcu service name
						party.sMcuServiceName = ds.Tables[dsTable].Rows[i]["mcuservicename"].ToString();

						//address type
						int iAddressType = Int32.Parse(ds.Tables[dsTable].Rows[i]["AddressType"].ToString());
						ret = ConvertToMcuAddressType(iAddressType,ref party);
						if (!ret)
						{
							logger.Trace ("Participant address type is incorrect.");
							return false;
						}

						// endpoint type
						party.etType= NS_MESSENGER.Party.eType.ROOM;

						// video equipment id 
						int videoequipmentid = Int32.Parse(ds.Tables[dsTable].Rows[i]["videoequipmentid"].ToString().Trim());
                        if (videoequipmentid >= 21 && videoequipmentid <= 23)
                        {
                            // its a codian vcr 
                            // get the gateway address from the address field. 
                            party.sGatewayAddress = party.sAddress.Substring(party.sAddress.LastIndexOf(":") + 1);
                            party.sAddress = party.sAddress.Substring(0, party.sAddress.LastIndexOf(":"));
                            party.etVideoEquipment = NS_MESSENGER.Party.eVideoEquipment.RECORDER;
                            party.etModelType = NS_MESSENGER.Party.eModelType.CODIAN_VCR;
                        }
                        else
                        {
                            EndpointModelType(videoequipmentid, ref party.etModelType);
                            logger.Trace("Party model type: " + party.etModelType.ToString());
                        }                        

                        // line rate
                        if (ds.Tables[dsTable].Rows[i]["DefLineRate"].ToString() != null)
                        {
                            int iLineRate = Int32.Parse(ds.Tables[dsTable].Rows[i]["DefLineRate"].ToString());
                            ret = ConvertToMcuLineRate(iLineRate, ref party.stLineRate.etLineRate);
                            if (!ret)
                            {
                                logger.Trace("Participant line rate is incorrect.");
                                return false;
                            }
                        }
                        else
                        {
                            // default to 384 Kbps
                            party.stLineRate.etLineRate = NS_MESSENGER.LineRate.eLineRate.K384;
                        }
                        //FB 2486 Starts
                        int isMessageOverlay = Int32.Parse(ds.Tables[dsTable].Rows[i]["isTextMsg"].ToString());
                        if (isMessageOverlay == 1)
                        {
                            party.bIsMessageOverlay = true;
                        }
                        else
                        {
                            party.bIsMessageOverlay = false;
                        }
                        //FB 2486 Ends

                        //FB 2501 P2P Call Monitoring Start

                        party.iTerminalType = Int32.Parse(ds.Tables[dsTable].Rows[i]["terminaltype"].ToString().Trim());

                        //FB 2501 P2P Call Monitoring End
                        
                        // lecturer
                        int isLecturer = Int32.Parse(ds.Tables[dsTable].Rows[i]["isLecturer"].ToString());
                        if (isLecturer == 1)
                        {
                            party.bIsLecturer = true;
                        }

                        try
                        {
                            // connect2
                            party.etCallerCallee = NS_MESSENGER.Party.eCallerCalleeType.CALLEE;
                            int connect2 = Int32.Parse(ds.Tables[dsTable].Rows[i]["Connect2"].ToString());
                            if (connect2 == 1)
                            {
                                party.etCallerCallee = NS_MESSENGER.Party.eCallerCalleeType.CALLER;
                            }
                        }
                        catch (Exception e1)
                        {
                            // just log the error
                            logger.Trace("Error in Connect2 attribute." + e1.Message);                            
                        }

						//add the participant to the conf.

                        if (ds.Tables[dsTable].Rows[i]["GUID"] != null)
                            party.sGUID = ds.Tables[dsTable].Rows[i]["GUID"].ToString().Trim();//FB 2501 Call Monitorin - 


                        FetchCallId(confid, instanceid, ref party);//FB 2390
                        //FB 2400 start
                        isPartyAdded = false;
                        isTelepres = 0;
                       
                        if (ds.Tables[dsTable].Rows[i]["isTelePresence"].ToString() != null)
                            if (ds.Tables[dsTable].Rows[i]["isTelePresence"].ToString().Trim() != "")
                                Int32.TryParse(ds.Tables[dsTable].Rows[i]["isTelePresence"].ToString(), out isTelepres);


                        //FB 2501 Call Monitoring Starts

                        if (isTelepres == 1)
                            party.isTelePresence = true;

                        if (ds.Tables[dsTable].Rows[i]["MultiCodecAddress"].ToString() != null)
                            if (ds.Tables[dsTable].Rows[i]["MultiCodecAddress"].ToString().Trim() != "")
                                party.MulticodecAddress = ds.Tables[dsTable].Rows[i]["MultiCodecAddress"].ToString();

                        //FB 2501 Call Monitoring Ends

                        //ZD 101363 Start
                        int SSHSupport = 0;
                        if (ds.Tables[dsTable].Rows[i]["SSHSupport"].ToString() != null)
                            if (ds.Tables[dsTable].Rows[i]["SSHSupport"].ToString().Trim() != "")
                                Int32.TryParse(ds.Tables[dsTable].Rows[i]["SSHSupport"].ToString(), out SSHSupport);
                        party.iSSHSupport = SSHSupport;
                        //ZD 101363 End

                        //FB 2501 Dec6 Start
                        int status = Int32.Parse(ds.Tables[dsTable].Rows[i]["OnlineStatus"].ToString());//FB 2501 Dec10
                        ret = FetchPartyStatus(status, ref party);
                        if (!ret)
                        {
                            logger.Trace("Participant status is incorrect.");
                            return false;
                        }
                        //FB 2501 Dec6 End  

                        party.Emailaddress = ds.Tables[dsTable].Rows[i]["GuestContactEmail"].ToString(); //FB 2441 //ZD 100619

                        if (isTelepres == 1 && (ds.Tables[dsTable].Rows[i]["MultiCodecAddress"].ToString() != null) && party.cMcu.etType != NS_MESSENGER.MCU.eType.CISCOTP)//FB 2501 Call Monitoring Tamil
                        {
                            isPartyAdded = true;
                            String[] codecAdds = ds.Tables[dsTable].Rows[i]["MultiCodecAddress"].ToString().Split('�');
                            for (int a = 0; a < codecAdds.Length; a++)
                            {
                                partyMulti = new NS_MESSENGER.Party();
                                SetPartyProperties(ref partyMulti, ref party);
                                partyMulti.iDbId = party.iDbId +a;

                                if (a > 0)
                                    partyMulti.iDbId += 5000;

                                partyName = party.sName;
                                if (codecAdds[a].Trim() != "")
                                {
                                    partyMulti.sAddress = codecAdds[a].Trim();
                                    partyName += a + 1; //FB 2493

                                    if (videoequipmentid >= 21 && videoequipmentid <= 23)
                                    {
                                        partyMulti.sGatewayAddress = partyMulti.sAddress.Substring(partyMulti.sAddress.LastIndexOf(":") + 1);
                                        partyMulti.sAddress = partyMulti.sAddress.Substring(0, partyMulti.sAddress.LastIndexOf(":"));
                                    }
                                    
                                    partyMulti.sName = partyName;
                                    logger.Trace("partyName:" + partyMulti.sName +" Address:"+ partyMulti.sAddress);
                                    partyq.Enqueue(partyMulti);
                                }
                            }

                        }
                        
                        if(!isPartyAdded)
                            partyq.Enqueue(party);
                        //FB 2400 end
                        logger.Trace("Party added to queue. Name: " + party.sName + " , McuAddress = " + party.sMcuAddress);
					} 
					catch (Exception e)
					{
						logger.Exception (100,e.Message);
						continue;
					}
				}				
				return(true);
			}
			catch(Exception e)
			{	
				logger.Exception(100,e.Message);
				return false;
			}

		}	

		public bool FetchUsers(int confid,int instanceid,ref Queue partyq,int mode)
		{
			DataSet ds = new DataSet();
			string dsTable = "Users";
			
			string query =	"select U.email, C.connectionType,C.IPISDNAddress,C.UserID,C.bridgeID, C.GUID"; //FB 2501 - Call Monitoring 
            query += ",C.bridgeIPISDNAddress,C.AudioOrVideo,C.OutsideNetwork,C.McuServiceName";
            query += ",C.AddressType,C.DefLineRate, C.isLecturer,C.Connect2";
            query += ",c.APIPortNo,c.confuId,u.FirstName,C.isTextMsg,C.terminaltype,C.OnlineStatus,U.PreConfCode";
            query += ",U.PreLeaderPin,U.PostLeaderPin,U.Audioaddon,U.AudioDialInPrefix,c.Partynameonmcu ,C.SysLocationId";//Code changed for API Port FB 2387 //FB 2486 //FB 2501 P2P Call Monitoring Dec6 //FB 2655 //ZD 101056 //ZD 104821
			query += " from Conf_User_D C,Usr_List_D U ";
			query += " where  C.UserID = U.UserID ";
			query += " and C.ConfID = " + confid.ToString();
			query += " and C.InstanceId = " + instanceid.ToString();
            //ZD 100221 starts
            if (mode != -1) // for webex as per valli instruction mode set as -1
            {
                if (mode == 0)
                    query += " and c.status = 1 and c.invitee = 1 ";
                else
                    query += " and c.connectstatus = " + mode.ToString();
            }
            //ZD 100221 ends





			bool ret = SelectCommand(query,ref ds,dsTable);
			if (!ret)
			{
				logger.Exception(10,"SQL error");
				return (false);
			}

			for (int i=0;i<ds.Tables[dsTable].Rows.Count ; i++)
			{
				try 
				{
					NS_MESSENGER.Party party = new  NS_MESSENGER.Party();

					// name
                    //ZD 101056 Starts
                    party.sMcuName = ds.Tables[dsTable].Rows[i]["Partynameonmcu"].ToString();
                    party.sName = ds.Tables[dsTable].Rows[i]["Email"].ToString();
					logger.Trace (" User = "+ party.sName);
                    //ZD 101056 Ends
					
					// connection type
                    int iConnectionType = 0;
                    iConnectionType = Int32.Parse(ds.Tables[dsTable].Rows[i]["ConnectionType"].ToString());
                    ret = ConvertToConnectionType(iConnectionType, ref party);
                    if (!ret)
                    {
                        logger.Trace("Participant connection type is incorrect.");
                        return false;
                    }
                    //Polycom CMA Start
                    party.isRoom = false;
                    party.Emailaddress = ds.Tables[dsTable].Rows[i]["email"].ToString();
                    //Polycom CMA End
						
					//party ip/isdn address
					party.sAddress = ds.Tables[dsTable].Rows[i]["IPISDNAddress"].ToString();
					
					//ZD 102712-coded at end of the method	
					//FB 2387 Start
                    //if (party.sAddress.Trim().Contains("D") && party.bAudioBridgeParty)//ZD 101655
                    //{
                    //  party.sName = ds.Tables[dsTable].Rows[i]["FirstName"].ToString() + "(" + ds.Tables[dsTable].Rows[i]["confuId"].ToString() + ")";
                    //
                    //FB 2387 End
	
					//userid 
                    party.iDbId = int.Parse(ds.Tables[dsTable].Rows[i]["UserID"].ToString());
                    party.iEndpointId = party.iDbId; //FB 2989

					// bridgeid 
					party.iMcuId = Int32.Parse (ds.Tables[dsTable].Rows[i]["bridgeID"].ToString());

					//bridge ip/isdn address
					party.sMcuAddress =  ds.Tables[dsTable].Rows[i]["bridgeIPISDNAddress"].ToString();
                    logger.Trace("MCU Address: " + party.sMcuAddress);


                    if (ds.Tables[dsTable].Rows[i]["GUID"] != null)
                        party.sGUID = ds.Tables[dsTable].Rows[i]["GUID"].ToString().Trim();//FB 2501 Call Monitorin - 
                    logger.Trace("Party GUID in Fetch Users: " + party.sGUID);

                    //ZD 104821 
                    if (ds.Tables[dsTable].Rows[i]["SysLocationId"] != null)
                        int.TryParse(ds.Tables[dsTable].Rows[i]["SysLocationId"].ToString(), out party.SysLocationID);
                    logger.Trace("SysLocationId: " + party.SysLocationID);

                    //FB 2655 Start

                    party.preConfCode = ds.Tables[dsTable].Rows[i]["PreConfCode"].ToString();
                    logger.Trace("preConfCode: " + party.preConfCode);

                    party.preLPin = ds.Tables[dsTable].Rows[i]["PreLeaderPin"].ToString();
                    logger.Trace("preLPin: " + party.preLPin);

                    party.postLPin = ds.Tables[dsTable].Rows[i]["PostLeaderPin"].ToString();
                    logger.Trace("postLPin: " + party.postLPin);

                    party.AudioDialInPrefix = ds.Tables[dsTable].Rows[i]["AudioDialInPrefix"].ToString();
                    logger.Trace("AudioDialInPrefix: " + party.AudioDialInPrefix);

                    int Audioparty = Int32.Parse(ds.Tables[dsTable].Rows[i]["Audioaddon"].ToString());
                    if (Audioparty == 1)
                        party.bAudioBridgeParty = true;
                    else
                        party.bAudioBridgeParty = false;

                    //FB 2655 End
					//ZD 102712 Starts		
                    //FB 2387 Start
                    if (party.sAddress.Trim().Contains("D") && party.bAudioBridgeParty)//ZD 101655
                    {
                        party.sName = ds.Tables[dsTable].Rows[i]["FirstName"].ToString() + "(" + ds.Tables[dsTable].Rows[i]["confuId"].ToString() + ")";
                    }
                    //FB 2387 End
					//ZD 102712 End
                    if (mode != -1) //ZD 100221 
                    {
                        // Fetch the Bridge info  
                        bool ret4 = FetchMcu(party.iMcuId, ref party.cMcu);
                        if (!ret4)
                        {
                            logger.Exception(100, "Error in fetching MCU info");
                            return (false);
                        }
                    }

					// audio or video 
					int audioOrVideo = Int32.Parse(ds.Tables[dsTable].Rows[i]["AudioOrVideo"].ToString());
					if (audioOrVideo == 1)  //FB 1740 //FB 1744
						party.etCallType= NS_MESSENGER.Party.eCallType.AUDIO;
					else
						party.etCallType = NS_MESSENGER.Party.eCallType.VIDEO;

					//outside network
					int outsideNetwork = Int32.Parse(ds.Tables[dsTable].Rows[i]["OutsideNetwork"].ToString());
					if (outsideNetwork == 1)
						party.bIsOutsideNetwork = true;

					//mcu service name
					party.sMcuServiceName = ds.Tables[dsTable].Rows[i]["McuServiceName"].ToString();

                    // Port address fo ept API port
                    int iAPIPort = 23; //Default telnet port no
                    if (ds.Tables[dsTable].Rows[i]["APIPortNo"].ToString() != "")
                        Int32.TryParse(ds.Tables[dsTable].Rows[i]["APIPortNo"].ToString().Trim(), out iAPIPort);

                    if (iAPIPort <= 0)
                        iAPIPort = 23;

                    party.iAPIPortNo = iAPIPort;

                    party.iconfnumname = Int32.Parse(ds.Tables[dsTable].Rows[i]["confuId"].ToString()); //FB 2390

					//address type
					int iAddressType = Int32.Parse(ds.Tables[dsTable].Rows[i]["AddressType"].ToString());
					ret = ConvertToMcuAddressType(iAddressType,ref party);
					if (!ret)
					{
						logger.Trace ("Participant address type is incorrect.");
                        continue;
					}

                    // line rate
                    if (ds.Tables[dsTable].Rows[i]["DefLineRate"].ToString() != null)
                    {
                        int iLineRate = Int32.Parse(ds.Tables[dsTable].Rows[i]["DefLineRate"].ToString());
                        ret = ConvertToMcuLineRate(iLineRate, ref party.stLineRate.etLineRate);
                        if (!ret)
                        {
                            logger.Trace("Participant address type is incorrect.");
                            return false;
                        }
                        logger.Trace("Party line rate: " + party.stLineRate.etLineRate.ToString());
                    }
                    else
                    {
                        // default to 384 Kbps
                        party.stLineRate.etLineRate = NS_MESSENGER.LineRate.eLineRate.K384;
                    }

                    //FB 2486 Starts
                    int isMessageOverlay = Int32.Parse(ds.Tables[dsTable].Rows[i]["isTextMsg"].ToString());
                    if (isMessageOverlay == 1)
                    {
                        party.bIsMessageOverlay = true;
                    }
                    else
                    {
                        party.bIsMessageOverlay = false;
                    }
                    //FB 2486 Ends

                    //FB 2501 P2P Call Monitoring Start

                    party.iTerminalType = Int32.Parse(ds.Tables[dsTable].Rows[i]["terminaltype"].ToString().Trim());

                    //FB 2501 P2P Call Monitoring End

                    //FB 2501 Dec6 Start
                    int status = Int32.Parse(ds.Tables[dsTable].Rows[i]["OnlineStatus"].ToString());//FB 2501 Dec10
                    ret = FetchPartyStatus(status, ref party);
                    if (!ret)
                    {
                        logger.Trace("Participant status is incorrect.");
                        return false;
                    }
                    //FB 2501 Dec6 End   

                    // lecturer
                    int isLecturer = Int32.Parse(ds.Tables[dsTable].Rows[i]["isLecturer"].ToString());
                    if (isLecturer == 1)
                    {
                        party.bIsLecturer = true;
                    }

                    try
                    {
                        // connect2
                        party.etCallerCallee = NS_MESSENGER.Party.eCallerCalleeType.CALLEE;
                        int connect2 = Int32.Parse(ds.Tables[dsTable].Rows[i]["Connect2"].ToString());
                        if (connect2 == 1)
                        {
                            party.etCallerCallee = NS_MESSENGER.Party.eCallerCalleeType.CALLER;
                        }
                    }
                    catch (Exception e1)
                    {
                        // just log the error
                        logger.Trace("Error in Connect2 attribute." + e1.Message);
                    }
					// endpoint type
					party.etType = NS_MESSENGER.Party.eType.USER;


					//add the participant to the partyq.

                    FetchCallId(confid, instanceid, ref party);//FB 2390
                    

					partyq.Enqueue(party);
                    logger.Trace("Party added to queue. Name: " + party.sName + " , McuAddress = " + party.sMcuAddress);
				}
				catch (Exception e)
				{
					logger.Exception(200,e.Message);
				}
			}
			
			return true;
		}

		public bool FetchGuests(int confid, int instanceid, ref Queue partyq,int mode)
		{
			DataSet ds = new DataSet();
			string dsTable = "Guests";
			
			string query = "select g.email,c.IPISDNAddress,c.connectionType,c.userid,c.bridgeid,c.GUID"; //FB 2501 - Call Monitoring 
            query += ",c.bridgeipisdnaddress,c.AudioOrVideo,c.OutsideNetwork,c.McuServiceName";
            query += ",c.AddressType,c.DefLineRate, c.isLecturer, c.Connect2, c.terminaltype, c.OnlineStatus"; //FB 2501 Dec6
            query += ",c.APIPortNo,c.confuId,c.isTextMsg,c.Partynameonmcu ";//Code changed for API Port //FB 2390 //FB 2486 //ZD 101056
			query += " from Usr_GuestList_D g , Conf_User_D c ";
			query += " where c.confid = " + confid.ToString() ;
			query += " and c.instanceid = " + instanceid.ToString() ;
			query += " and c.userid = g.userid ";
            //ZD 100221 starts
            if (mode != -1) // for webex as per valli instruction mode set as -1
            {
                if (mode != 0)
                   query += " and c.connectstatus = " + mode.ToString();
                if (mode == 0)
                   query += " and c.status = 1 and c.invitee = 1 ";
               
            }
            //ZD 100221 ends

			bool ret = SelectCommand(query,ref ds,dsTable);
			if (!ret)
			{
				logger.Exception(10,"SQL error");
				return (false);
			}
			
			for (int i=0;i<ds.Tables[dsTable].Rows.Count ; i++)
			{
				try
				{
					NS_MESSENGER.Party party = new NS_MESSENGER.Party();
								
					// name
					party.sName = ds.Tables[dsTable].Rows[i]["Email"].ToString();
					logger.Trace (" User = "+ party.sName);
                    party.sMcuName = ds.Tables[dsTable].Rows[i]["Partynameonmcu"].ToString(); //ZD 101056
															
					//ip/isdn address
					party.sAddress = ds.Tables[dsTable].Rows[i]["IPISDNAddress"].ToString();
					
					// connection type
					int iConnectionType = 0;
                    iConnectionType = Int32.Parse(ds.Tables[dsTable].Rows[i]["ConnectionType"].ToString());
                    ret = ConvertToConnectionType(iConnectionType, ref party);
                    if (!ret)
                    {
                        logger.Trace("Participant connection type is incorrect.");
                        return false;
                    }

                    //Polycom CMA Start
                    party.isRoom = false;
                    party.Emailaddress = ds.Tables[dsTable].Rows[i]["email"].ToString();
                    //Polycom CMA End

					//guestid 
					party.iDbId = int.Parse(ds.Tables[dsTable].Rows[i]["UserID"].ToString());
                    party.iEndpointId = party.iDbId; //FB 2989
			
					//bridgeid 
					party.cMcu.iDbId = Int32.Parse(ds.Tables[dsTable].Rows[i]["BridgeID"].ToString());

					// bridgeipisdnaddress
					party.sMcuAddress = ds.Tables[dsTable].Rows[i]["BridgeIpIsdnAddress"].ToString();
                    logger.Trace("MCU Address: " + party.sMcuAddress);

                    if (mode != -1) //ZD 100221
                    {
                        // Fetch the Bridge info
                        bool ret4 = FetchMcu(party.cMcu.iDbId, ref party.cMcu);
                        if (!ret4)
                        {
                            logger.Exception(100, "Error in fetching MCU info");
                            return (false);
                        }
                    }

                    if (ds.Tables[dsTable].Rows[i]["GUID"] != null)
                        party.sGUID = ds.Tables[dsTable].Rows[i]["GUID"].ToString().Trim();//FB 2501 Call Monitorin - 
                    logger.Trace("Party GUID in Fetch Users: " + party.sGUID);

					// audio or video 
					int audioOrVideo = Int32.Parse(ds.Tables[dsTable].Rows[i]["AudioOrVideo"].ToString());
                    if (audioOrVideo == 1)  //FB 1740 //FB 1744
						party.etCallType = NS_MESSENGER.Party.eCallType.AUDIO;
					else
						party.etCallType = NS_MESSENGER.Party.eCallType.VIDEO;

					//outside network
					int outsideNetwork = Int32.Parse(ds.Tables[dsTable].Rows[i]["OutsideNetwork"].ToString());
					if (outsideNetwork == 1)
						party.bIsOutsideNetwork = true;

					//mcu service name
					party.sMcuServiceName = ds.Tables[dsTable].Rows[i]["McuServiceName"].ToString();

                    // Port address fo ept API port
                    int iAPIPort = 23;
                    if (ds.Tables[dsTable].Rows[i]["APIPortNo"].ToString() != "")
                        Int32.TryParse(ds.Tables[dsTable].Rows[i]["APIPortNo"].ToString().Trim(), out iAPIPort);
                    if (iAPIPort <= 0)
                        iAPIPort = 23;

                    party.iAPIPortNo = iAPIPort;

                    party.iconfnumname = Int32.Parse(ds.Tables[dsTable].Rows[i]["confuId"].ToString()); //FB 2390

					//address type
					int iAddressType = Int32.Parse(ds.Tables[dsTable].Rows[i]["AddressType"].ToString());
					ret = ConvertToMcuAddressType(iAddressType,ref party);
					if (!ret)
					{
						logger.Trace ("Participant address type is incorrect.");
						return false;
					}

                    // line rate
                    if (ds.Tables[dsTable].Rows[i]["DefLineRate"].ToString() != null)
                    {
                        int iLineRate = Int32.Parse(ds.Tables[dsTable].Rows[i]["DefLineRate"].ToString());
                        ret = ConvertToMcuLineRate(iLineRate, ref party.stLineRate.etLineRate);
                        if (!ret)
                        {
                            logger.Trace("Participant address type is incorrect.");
                            return false;
                        }
                    }
                    else
                    {
                        // default to 384 Kbps
                        party.stLineRate.etLineRate = NS_MESSENGER.LineRate.eLineRate.K384;
                    }

                    //FB 2486 Starts
                    int isMessageOverlay = Int32.Parse(ds.Tables[dsTable].Rows[i]["isTextMsg"].ToString());
                    if (isMessageOverlay == 1)
                    {
                        party.bIsMessageOverlay = true;
                    }
                    else
                    {
                        party.bIsMessageOverlay = false;
                    }
                    //FB 2486 Ends
                    //FB 2501 Dec6 Start
                    int status = Int32.Parse(ds.Tables[dsTable].Rows[i]["OnlineStatus"].ToString());//FB 2501 Dec10
                    ret = FetchPartyStatus(status, ref party);
                    if (!ret)
                    {
                        logger.Trace("Participant status is incorrect.");
                        return false;
                    }
                    //FB 2501 Dec6 End   
                    // lecturer
                    int isLecturer = Int32.Parse(ds.Tables[dsTable].Rows[i]["isLecturer"].ToString());
                    if (isLecturer == 1)
                    {
                        party.bIsLecturer = true;
                    }
                    
                    try
                    {
                        // connect2
                        party.etCallerCallee = NS_MESSENGER.Party.eCallerCalleeType.CALLEE;
                        int connect2 = Int32.Parse(ds.Tables[dsTable].Rows[i]["Connect2"].ToString());
                        if (connect2 == 1)
                        {
                            party.etCallerCallee = NS_MESSENGER.Party.eCallerCalleeType.CALLER;
                        }
                    }
                    catch (Exception e1)
                    {
                        // just log the error
                        logger.Trace("Error in Connect2 attribute." + e1.Message);
                    }
					// endpoint type
					party.etType = NS_MESSENGER.Party.eType.GUEST;

					//add the participant to the conf.

                    FetchCallId(confid, instanceid, ref party); //FB 2390
                   
                   	partyq.Enqueue(party);
                    logger.Trace("Party added to queue. Name: " + party.sName + " , McuAddress = " + party.sMcuAddress);
				}
				catch(Exception e)
				{
					logger.Exception(200,e.Message);
				}
			}

			return true;
		}


		public bool FetchCascadeLinks(int confid, int instanceid, ref Queue partyq,int mode)
		{
            String sBridgeExtensionNo = "";// ZD 100768
			DataSet ds = new DataSet();
			string dsTable = "CascadeLinks";
            try
            {
                //FB 2390 Start
                string query = "select d.*,c.confnumname,c.isTextMsg "; //FB 2486
                query += " from Conf_Cascade_D d,Conf_Conference_D c";
                query += " where d.confid=c.confid and d.instanceid=c.instanceid and d.confid = " + confid.ToString();
                query += " and d.instanceid = " + instanceid.ToString();
                //FB 2390 End
                if (mode != 0)
                {
                    query += " and connectstatus = " + mode.ToString();
                }

                query += " order by connectiontype ";

                /*			//if (mode !=0)
                            //{
                                if (mode ==2)
                                {	
                                    // find any cascade links which haven't been sent yet
                                    query += " and (connectstatus = 0 or connectstatus =2)";
                                }
                                else
                                {
                                    query += " and connectstatus = " + mode.ToString();
                                }
                            //}
                            //else
                            //{
                            //	// fetch only master cascade links
                            //	query += " and masterorslave = 1 ";
                            //}
                */
                bool ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Exception(10, "SQL error");
                    return (false);
                }

                for (int i = 0; i < ds.Tables[dsTable].Rows.Count; i++)
                {

                    NS_MESSENGER.Party party = new NS_MESSENGER.Party();

                    // db id
                    // Magic Number = 20000 . Needed so that it doesnot clash with user-id's or room-ids.
                    party.iDbId = 20000 + int.Parse(ds.Tables[dsTable].Rows[i]["CascadeLinkId"].ToString());
                    party.iEndpointId = int.Parse(ds.Tables[dsTable].Rows[i]["CascadeLinkId"].ToString()); //FB 2989

                    // name
                    party.sName = ds.Tables[dsTable].Rows[i]["CascadeLinkName"].ToString();
                    logger.Trace(" Cascade Link Party = " + party.sName);

                    // master or slave
                    int masterOrSlave = Int32.Parse(ds.Tables[dsTable].Rows[i]["masterorslave"].ToString());
                    if (masterOrSlave == 1)
                    {
                        party.etCascadeRole = NS_MESSENGER.Party.eCascadeRole.MASTER;
                    }
                    else
                    {
                        party.etCascadeRole = NS_MESSENGER.Party.eCascadeRole.SLAVE;
                    }


                    party.iconfnumname = Int32.Parse(ds.Tables[dsTable].Rows[i]["confnumname"].ToString()); //FB 2390
                    // audio or video 
                    int audioOrVideo = Int32.Parse(ds.Tables[dsTable].Rows[i]["audioOrvideo"].ToString());
                    if (audioOrVideo == 1)  //FB 1740 //FB 1744
                        party.etCallType = NS_MESSENGER.Party.eCallType.AUDIO;
                    else
                        party.etCallType = NS_MESSENGER.Party.eCallType.VIDEO;

                    // connection type
                    int iConnectionType = 0;
                    iConnectionType = Int32.Parse(ds.Tables[dsTable].Rows[i]["connectiontype"].ToString());
                    ret = ConvertToConnectionType(iConnectionType, ref party);
                    if (!ret)
                    {
                        logger.Trace("Participant connection type is incorrect.");
                        return false;
                    }

                    party.isRoom = false;//Polycom CMA

                    if (ds.Tables[dsTable].Rows[i]["GUID"] != null)
                        party.sGUID = ds.Tables[dsTable].Rows[i]["GUID"].ToString().Trim();//FB 2501 Call Monitorin - 
                    logger.Trace("Party GUID in Fetch Users: " + party.sGUID);

                    //ip/isdn address
                    party.sAddress = ds.Tables[dsTable].Rows[i]["ipisdnaddress"].ToString();

                    //bridgeipisdnaddress
                    party.sMcuAddress = ds.Tables[dsTable].Rows[i]["bridgeipisdnaddress"].ToString();

                    // bridgeid
                    party.cMcu.iDbId = Int32.Parse(ds.Tables[dsTable].Rows[i]["bridgeid"].ToString());

                    // Fetch the Bridge info  
                    bool ret4 = FetchMcu(party.cMcu.iDbId, ref party.cMcu);
                    if (!ret4)
                    {
                        logger.Exception(100, "Error in fetching MCU info");
                        return (false);
                    }

                    //ZD 100768
                    if (party.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_IN)
                        sBridgeExtensionNo = party.cMcu.sBridgeExtNo;
                    else if (party.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_OUT)
                        party.sCascadeBridgeExtNo = sBridgeExtensionNo;

                    //mcu service name
                    party.sMcuServiceName = ds.Tables[dsTable].Rows[i]["McuServiceName"].ToString();

                    //address type
                    int iAddressType = Int32.Parse(ds.Tables[dsTable].Rows[i]["AddressType"].ToString());
                    ret = ConvertToMcuAddressType(iAddressType, ref party);
                    if (!ret)
                    {
                        logger.Trace("Participant address type is incorrect.");
                        continue;
                    }

                    // line rate
                    if (ds.Tables[dsTable].Rows[i]["DefLineRate"].ToString() != null)
                    {
                        int iLineRate = Int32.Parse(ds.Tables[dsTable].Rows[i]["DefLineRate"].ToString());
                        ret = ConvertToMcuLineRate(iLineRate, ref party.stLineRate.etLineRate);
                        if (!ret)
                        {
                            logger.Trace("Participant address type is incorrect.");
                            return false;
                        }
                    }
                    else
                    {
                        // default to 384 Kbps
                        party.stLineRate.etLineRate = NS_MESSENGER.LineRate.eLineRate.K384;
                    }

                    //FB 2486 Starts
                    int isMessageOverlay = Int32.Parse(ds.Tables[dsTable].Rows[i]["isTextMsg"].ToString());
                    if (isMessageOverlay == 1)
                    {
                        party.bIsMessageOverlay = true;
                    }
                    else
                    {
                        party.bIsMessageOverlay = false;
                    }
                    //FB 2486 Ends
                    // lecturer
                    int isLecturer = Int32.Parse(ds.Tables[dsTable].Rows[i]["isLecturer"].ToString());
                    if (isLecturer == 1)
                    {
                        party.bIsLecturer = true;
                    }

                    // endpoint type
                    party.etType = NS_MESSENGER.Party.eType.CASCADE_LINK;

                    //add the participant to the queue.

                    //FB 2501 Dec6 Start
                    int status = Int32.Parse(ds.Tables[dsTable].Rows[i]["OnlineStatus"].ToString()); //FB 2501 Dec10
                    ret = FetchPartyStatus(status, ref party);
                    if (!ret)
                    {
                        logger.Trace("Participant status is incorrect.");
                        return false;
                    }
                    //FB 2501 Dec6 End   

                    FetchCallId(confid, instanceid, ref party);//FB 2390

                    partyq.Enqueue(party);

                }

            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
            }

			return true;
		}

        /**  FB 2447 **/
        public bool FetchVMRParty(ref NS_MESSENGER.Conference conf, ref Queue partyq,NS_MESSENGER.Party refParty)
        {
         

             try
                {
                   NS_MESSENGER.Party party = new NS_MESSENGER.Party();
                   
                   party.sName = conf.sDbName + "_VMR Party";
                   logger.Trace(" User = " + party.sName);
                   //ip/isdn address
                   party.sAddress = "";
                   if (IsContainsIP(conf.sInternalBridgeNumber))
                   {
                       //outside network
                       party.bIsOutsideNetwork = false;
                       party.sAddress = conf.sInternalBridgeNumber.Trim();
 
                   }

                   if (party.sAddress.Trim() == "")
                   {
                       if (IsContainsIP(conf.sExternalBridgeNumber))
                       {
                           //outside network
                           party.bIsOutsideNetwork = true;
                           party.sAddress = conf.sExternalBridgeNumber.Trim();

                       }
 
                   }

                   if (party.sAddress.Trim() == "")
                   { 
                       //outside network
                       party.bIsOutsideNetwork = false;
                       party.sAddress = conf.sInternalBridgeNumber.Trim();
                   }
                      

                   if (party.sAddress.Trim() != "")
                   {

                       // connection type
                       party.etConnType = NS_MESSENGER.Party.eConnectionType.DIAL_OUT;
                       //Polycom CMA 
                       party.isRoom = true;
                       //guestid 
                       party.iDbId = conf.iDbID;
                       //bridgeid 
                       party.cMcu.iDbId = conf.cMcu.iDbId;
                       // bridgeipisdnaddress
                       party.sMcuAddress = conf.cMcu.sIp;
                       logger.Trace("MCU Address: " + party.sMcuAddress);
                       if (refParty != null)
                       {
                           party.cMcu.iDbId = refParty.cMcu.iDbId;
                           party.sMcuAddress = refParty.sMcuAddress;
                       }
                       // audio or video                     
                       party.etCallType = NS_MESSENGER.Party.eCallType.VIDEO;
                       //mcu service name
                       party.sMcuServiceName = "";
                       // Port address fo ept API port
                       party.iAPIPortNo = 23;
                       party.iconfnumname = conf.iDbNumName;
                       party.etAddressType = NS_MESSENGER.Party.eAddressType.H323_ID;
                       party.etProtocol = NS_MESSENGER.Party.eProtocol.IP;
                       party.etVideoProtocol = NS_MESSENGER.Party.eVideoProtocol.H263;
                       party.stLineRate = conf.stLineRate;
                       party.bIsLecturer = false;
                       // endpoint type
                       party.etType = NS_MESSENGER.Party.eType.VMR;
                       partyq.Enqueue(party);
                       logger.Trace("Party added to queue. Name: " + party.sName + " , McuAddress = " + party.sMcuAddress);
                   }
                }
                catch (Exception e)
                {
                    logger.Exception(200, e.Message);
                }
            

            return true;
        } 
        /**  FB 2447 **/

		
		#endregion

		#region Fetch bridge info
		/*
		public bool FetchMcu(int mcuID,ref NS_MESSENGER.MCU mcu)
		{
			try
			{
				// Retreive  the  bridge record
				DataSet ds = new DataSet();
				string dsTable = "MCU";
				string query = "Select BridgeLogin,BridgePassword,BridgeAddress,BridgeName,BridgeTypeId,SoftwareVer ,Timezone";
				query +=	" from bridge where bridgeid = " + mcuID;				
				bool ret = SelectCommand(query,ref ds,dsTable);
				if (!ret)
				{
					logger.Exception(100,0,"Database.cs","FetchMcu()",823,"SQL error");
					return (false);
				}	
				
				mcu.iDbId = mcuID;
				mcu.m_login = ds.Tables[dsTable].Rows[0][0].ToString();//login
				mcu.m_pwd = ds.Tables[dsTable].Rows[0][1].ToString();//pwd
				
				try
				{
					CRYPTOCOMLib.CryptoClass crypto = new CRYPTOCOMLib.CryptoClass();
					mcu.m_pwd = crypto.Decrypt (mcu.m_pwd);
				}
				catch(Exception ex)
				{
					logger.Exception(100,0,"Database.cs","FetchMcu()",836,ex.Message);
					return false;
				}

				mcu.m_ip = ds.Tables[dsTable].Rows[0][2].ToString();//ip address
				mcu.m_name = ds.Tables[dsTable].Rows[0][3].ToString();
					
				int type = 0;
				type = Int32.Parse(ds.Tables[dsTable].Rows[0][4].ToString());//bridge type
				double softwareVer = Double.Parse(ds.Tables[dsTable].Rows[0][5].ToString()); //software ver
				mcu.m_timezoneId = Int32.Parse(ds.Tables[dsTable].Rows[0][6].ToString());

				logger.Trace("Database.cs","FetchMcu",852,"Bridge Type ID = " + type.ToString());
				logger.Trace("Database.cs","FetchMcu",853,"Software Ver = " + softwareVer.ToString());


				if (type < 4)
				{
					if (softwareVer < 7 )
						mcu.etType = NS_MESSENGER.MCU.eType.ACCORDv6;
					else
						mcu.etType = NS_MESSENGER.MCU.eType.ACCORDv7;									
				}
				else
				{
					if ((type >= 4) && (type <= 7) )
					{
						mcu.etType = NS_MESSENGER.MCU.eType.CODIAN;

						// Fetch the Codian Port-A ip address since thats the default.
						DataSet ds2 = new DataSet();
						string dsTable2 = "MCU";												
						string query2 = "Select ipaddress from bridgeipservices where ServiceID = 1 and bridgeid = " + mcuID;				
						bool ret2 = SelectCommand(query2,ref ds2,dsTable2);
						if (!ret2)
						{
							logger.Exception(100,0,"Database.cs","FetchMcu()",863,"SQL error");
							return (false);
						}	
					
						mcu.m_ip = ds2.Tables[dsTable2].Rows[0][0].ToString(); //overwrite the existing ip address
					}					
				}
				
				logger.Trace("Database.cs","FetchMcu",880,mcu.etType.ToString());
		
				mcu.m_station = "VRMCCC";

				return (true);
			}
			catch(Exception e)
			{
				logger.Exception(100,0,"Database.cs","FetchMcu()",853,e.Message);
				return false;
			}
		}
		
		*/
		public bool FetchAllMcu (ref Queue mcuq) 
		{
			// Retreive  the  bridges
			DataSet ds = new DataSet();
			string dsTable = "MCU";
			string query = "Select bridgeid from MCU_List_D where virtualbridge = 0 and status =1 and deleted = 0";				
			bool ret = SelectCommand(query,ref ds,dsTable);
			if (!ret)
			{
				logger.Exception(10,"SQL error");
				return (false);
			}		
			for (int i=0;i<ds.Tables[dsTable].Rows.Count ; i++)
			{
				try
				{
					NS_MESSENGER.MCU mcu = new NS_MESSENGER.MCU();				
					bool ret1 = FetchMcu(Int32.Parse(ds.Tables[dsTable].Rows[i]["bridgeid"].ToString()),ref mcu);
					if (!ret1)
					{
						// mcu data is buggy. so skip this mcu and go to the next one.
						logger.Exception(100,"Error fetching MCU info.");
						continue; 
					}
					mcuq.Enqueue (mcu);
				}
				catch (Exception e)
				{
					logger.Exception (100,e.Message);
				}				
			}
			return true;
		}
		
		#endregion

		#region Conference parameter conversions
		public bool ConvertToMcuLineRate(int linerate,ref NS_MESSENGER.LineRate.eLineRate line_rate)//FB 2501 - Call Monitoring
		{
			try
			{
				switch(linerate)
				{
					case 56: 
					{
						line_rate = NS_MESSENGER.LineRate.eLineRate.K56;
						return true;
					}
					case 64: 
					{
                        line_rate = NS_MESSENGER.LineRate.eLineRate.K64;
						return true;
					}
					case 112: 
					{
                        line_rate = NS_MESSENGER.LineRate.eLineRate.K2x56;
						return true;
					}
					case 127: 
					{
                        line_rate = NS_MESSENGER.LineRate.eLineRate.K2x64;
						return true;
					}
					case 128 :
					{
                        line_rate = NS_MESSENGER.LineRate.eLineRate.K128;
						return true;
					}
                    case 192:
                    {
                        line_rate = NS_MESSENGER.LineRate.eLineRate.K192;
                        return true;
                    }
					case 256 :
					{
                        line_rate = NS_MESSENGER.LineRate.eLineRate.K256;
						return true;
					}
                    case 320:
                    {
                        line_rate = NS_MESSENGER.LineRate.eLineRate.K320;
                        return true;
                    }			
					case 512:
					{
                        line_rate = NS_MESSENGER.LineRate.eLineRate.K512;
						return true;
					}				
					case 768:
					{
                        line_rate = NS_MESSENGER.LineRate.eLineRate.K768;
						return true;
					}
                    case 1024:
                    {
                        line_rate = NS_MESSENGER.LineRate.eLineRate.M1024;
                        return true;
                    }
					case 1152:
					{
                        line_rate = NS_MESSENGER.LineRate.eLineRate.M1152;
						return true;
					}
                    case 1250:
                    {
                        line_rate = NS_MESSENGER.LineRate.eLineRate.M1250;
                        return true;
                    }
					case 1472:
					{
                        line_rate = NS_MESSENGER.LineRate.eLineRate.M1472;
						return true;
					}
                    case 1536:
                    {
                        line_rate = NS_MESSENGER.LineRate.eLineRate.M1536;
                        return true;
                    }
                    case 1792:
                    {
                        line_rate = NS_MESSENGER.LineRate.eLineRate.M1792;
                        return true;
                    }
                    case 1920:
                    {
                        line_rate = NS_MESSENGER.LineRate.eLineRate.M1920;
                        return true;
                    }
                    case 2048:
                    {
                        line_rate = NS_MESSENGER.LineRate.eLineRate.M2048;
                        return true;
                    }
                    case 2560:
                    {
                        line_rate = NS_MESSENGER.LineRate.eLineRate.M2560;
                        return true;
                    }
                    case 3072:
                    {
                        line_rate = NS_MESSENGER.LineRate.eLineRate.M3072;
                        return true;
                    }
                    case 3584:
                    {
                        line_rate = NS_MESSENGER.LineRate.eLineRate.M3584;
                        return true;
                    }
                    case 4096:
                    {
                        line_rate = NS_MESSENGER.LineRate.eLineRate.M4096;
                        return true;
                    }
					case 384:
					default :
					{
                        line_rate = NS_MESSENGER.LineRate.eLineRate.K384;
						return true;
					}
				}
			}
			catch (Exception e)
			{
				logger.Exception(100,e.Message);
				return false;
			}
		}

		bool ConvertToMcuAudioCodec(int audio,ref NS_MESSENGER.Conference.eAudioCodec audio_rate)
		{
			try
			{
				switch(audio)
				{
					case 1: 
					{	
						audio_rate = NS_MESSENGER.Conference.eAudioCodec.G728;
						return true;
					}
					case 2: 
					{	
						audio_rate = NS_MESSENGER.Conference.eAudioCodec.G722_24;
						return true;
					}
					case 3: 
					{	
						audio_rate = NS_MESSENGER.Conference.eAudioCodec.G722_32;						
						return true;
					}
					case 4: 
					{	
						audio_rate = NS_MESSENGER.Conference.eAudioCodec.G722_56;						
						return true;
					}
					case 5: 
					{	
						audio_rate = NS_MESSENGER.Conference.eAudioCodec.G711_56;						
						return true;
					}
			
					case 0: 
					default:
					{	
						audio_rate = NS_MESSENGER.Conference.eAudioCodec.AUTO;
						return true;
					}
				}
			}
			catch (Exception e)
			{
				logger.Exception(100,e.Message);
				return false;
			}
		}

		bool ConvertToMcuNetwork(int video,ref NS_MESSENGER.Conference.eNetwork network)
		{
			try 
			{
				switch(video)
				{
					case 2:
					{
						network = NS_MESSENGER.Conference.eNetwork.ISDN;
						return true;
					}
					case 1:
					default:
					{
						network = NS_MESSENGER.Conference.eNetwork.IP;
						return true;
					}
				}
			}
			catch (Exception e)
			{
				logger.Exception(100,e.Message);
				return false;
			}
		}

        bool ConvertToConnectionType(int connectionType, ref NS_MESSENGER.Party party)
        {
            try
            {
                switch (connectionType)
                {
                    case 1:
                        {
                            party.etConnType = NS_MESSENGER.Party.eConnectionType.DIAL_IN;
                            break;
                        }
                    case 2:
                        {
                            party.etConnType = NS_MESSENGER.Party.eConnectionType.DIAL_OUT;
                            break;
                        }
                    case 3:
                        {
                            party.etConnType = NS_MESSENGER.Party.eConnectionType.DIRECT;
                            break;
                        }
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
		bool ConvertToMcuAddressType (int addressType,ref NS_MESSENGER.Party party)
		{
			try
			{
				switch (addressType)
				{
					case 1: 
					{
						party.etAddressType = NS_MESSENGER.Party.eAddressType.IP_ADDRESS;
                        party.etProtocol = NS_MESSENGER.Party.eProtocol.IP;
                        party.etVideoProtocol = NS_MESSENGER.Party.eVideoProtocol.H263;
						break;
					}
					case 2: 
					{
						party.etAddressType = NS_MESSENGER.Party.eAddressType.H323_ID;
                        party.etProtocol = NS_MESSENGER.Party.eProtocol.IP;
                        party.etVideoProtocol = NS_MESSENGER.Party.eVideoProtocol.H263;
						break;
					}
					case 3: 
					{
						party.etAddressType = NS_MESSENGER.Party.eAddressType.E_164;
                        party.etProtocol = NS_MESSENGER.Party.eProtocol.IP;
                        party.etVideoProtocol = NS_MESSENGER.Party.eVideoProtocol.H263;
						break;
					}
					case 4: 
					{
						party.etAddressType = NS_MESSENGER.Party.eAddressType.ISDN_PHONE_NUMBER;
                        party.etProtocol = NS_MESSENGER.Party.eProtocol.ISDN;
                        party.etVideoProtocol = NS_MESSENGER.Party.eVideoProtocol.AUTO;
						break;
					}
                    case 5:
                    {
                        party.etAddressType = NS_MESSENGER.Party.eAddressType.MPI;
                        party.etProtocol = NS_MESSENGER.Party.eProtocol.MPI;
                        party.etVideoProtocol = NS_MESSENGER.Party.eVideoProtocol.AUTO;
                        break;
                    }
                    //FB 2390
                    case 6:
                    {
                        party.etAddressType = NS_MESSENGER.Party.eAddressType.SIP;
                        party.etProtocol = NS_MESSENGER.Party.eProtocol.SIP;
                        party.etVideoProtocol = NS_MESSENGER.Party.eVideoProtocol.AUTO;
                        break;
                    }
				}
				return true;
			}
			catch (Exception e)
			{
				logger.Exception(100,e.Message);
				return false;
			}
		}
		#endregion

		public bool FetchOngoingParticipants (ref NS_MESSENGER.Conference conf,int mode )
		{
			// Mode 1 = only Unchanged party (default)
			// Mode 1 = only Edit party
			// Mode 2 = only New party
			// Mode 3 = only Deleted party 				
			FetchUsers (conf.iDbID,conf.iInstanceID,ref conf.qParties,mode);
			FetchGuests (conf.iDbID,conf.iInstanceID,ref conf.qParties,mode);
			FetchRooms (conf.iDbID,conf.iInstanceID,ref conf.qParties,mode);
						
			return true;
		}

		public bool FetchOngoingCascadeLinks (ref NS_MESSENGER.Conference conf,int mode )
		{
			// Mode 1 = only Unchanged party (default)
			// Mode 1 = only Edit party
			// Mode 2 = only New party
			// Mode 3 = only Deleted party 				
			FetchCascadeLinks(conf.iDbID,conf.iInstanceID,ref conf.qParties,mode);
			return true;
		}

		public bool UpdateOngoingPartyStatus (NS_MESSENGER.Party.eType type,int partyid,int confid,int instanceid,int mode)
		{
			string query = null;
			if (type == NS_MESSENGER.Party.eType.GUEST ||type == NS_MESSENGER.Party.eType.USER)
			{
				query = "update Conf_User_D set connectstatus = " + mode.ToString();
				query += " where userid = " + partyid.ToString();
				query += " and confid = " + confid.ToString();
				query += "  and instanceid = " + instanceid.ToString();
			}
			if (type == NS_MESSENGER.Party.eType.ROOM)
			{
				// magic number = 10000 . this is only for rooms , since roomids overlap with userid.
				partyid = partyid - 10000;

				query = "update Conf_Room_D set connectstatus = " + mode.ToString();
				query += " where roomid = " + partyid.ToString();
				query += " and confid = " + confid.ToString();
				query += "  and instanceid = " + instanceid.ToString();
			}
	
			if (type == NS_MESSENGER.Party.eType.CASCADE_LINK)
			{
				// magic number = 20000 . this is only for cascadelinks , since cascadeid's overlap with userid , roomid.
				partyid = partyid - 20000;

				query = "update Conf_Cascade_D set connectstatus = " + mode.ToString();
				query += " where cascadelinkid = " + partyid.ToString();
				query += " and confid = " + confid.ToString();
				query += "  and instanceid = " + instanceid.ToString();
			}
	
			bool ret = NonSelectCommand(query);
			if (!ret)
			{
				logger.Exception(100,"SQL error");
				return (false);
			}					
			return true;	
		}

		/// <summary>
		/// Update the conference status (0= Scheduled, 1=Pending,3 = Terminated,5 = Ongoing, 6 = OnMcu, 7 = Completed , 9 = Deleted)
		/// </summary>
		/// <param name="confid"></param>
		/// <param name="instanceid"></param>
		/// <param name="confStatus"></param>
		/// <returns></returns>
		public bool	UpdateConferenceStatus(int confid,int instanceid,NS_MESSENGER.Conference.eStatus confStatus)
		{
            int iConfStatus =0;
            if (confStatus == NS_MESSENGER.Conference.eStatus.SCHEDULED)
                iConfStatus = 0;
            if (confStatus == NS_MESSENGER.Conference.eStatus.PENDING)
                iConfStatus = 1;
            if (confStatus == NS_MESSENGER.Conference.eStatus.TERMINATED)
                iConfStatus = 3;
            if (confStatus == NS_MESSENGER.Conference.eStatus.ONGOING)
                iConfStatus = 5;
            if (confStatus == NS_MESSENGER.Conference.eStatus.OnMCU)
                iConfStatus = 6;
            if (confStatus == NS_MESSENGER.Conference.eStatus.COMPLETED)
                iConfStatus = 7;
            if (confStatus == NS_MESSENGER.Conference.eStatus.DELETED)
                iConfStatus = 9;
            if (confStatus == NS_MESSENGER.Conference.eStatus.WAITLIST) //ZD 102532
                iConfStatus = 2;

			string query = "update Conf_Conference_D set status = " + iConfStatus.ToString();
			query += " where confid = " + confid.ToString();
			query += " and instanceid = " + instanceid.ToString();

			bool ret = NonSelectCommand (query);
			if (!ret)
			{
				logger.Exception(100,"Error executing query.");
				return false;
			}
			return true;
		}

        public bool UpdateConferenceProcessStatus(int confid, int instanceid, int iConfStatus)// FB 2710
        {


            string query = "update Conf_Conference_D set ProcessStatus = " + iConfStatus.ToString();
            query += " where confid = " + confid.ToString();
            query += " and instanceid = " + instanceid.ToString();

            bool ret = NonSelectCommand(query);
            if (!ret)
            {
                logger.Exception(100, "Error executing query.");
                return false;
            }
            return true;
        }

        public bool UpdateConferenceStatus(int confid, int instanceid, NS_MESSENGER.Conference.eStatus confStatus, int deleted)
        {
            int iConfStatus = 0;
            if (confStatus == NS_MESSENGER.Conference.eStatus.SCHEDULED)
                iConfStatus = 0;
            if (confStatus == NS_MESSENGER.Conference.eStatus.PENDING)
                iConfStatus = 1;
            if (confStatus == NS_MESSENGER.Conference.eStatus.TERMINATED)
                iConfStatus = 3;
            if (confStatus == NS_MESSENGER.Conference.eStatus.ONGOING)
                iConfStatus = 5;
            if (confStatus == NS_MESSENGER.Conference.eStatus.OnMCU)
                iConfStatus = 6;
            if (confStatus == NS_MESSENGER.Conference.eStatus.COMPLETED)
                iConfStatus = 7;
            if (confStatus == NS_MESSENGER.Conference.eStatus.DELETED)
                iConfStatus = 9;
            if (confStatus == NS_MESSENGER.Conference.eStatus.WAITLIST) //ZD 102532
                iConfStatus = 2;

            string query = "update Conf_Conference_D set deleted = " + deleted + ", status = " + iConfStatus.ToString(); //ZD 100522
            query += " where confid = " + confid.ToString();
            query += " and instanceid = " + instanceid.ToString();

            bool ret = NonSelectCommand(query);
            if (!ret)
            {
                logger.Exception(100, "Error executing query.");
                return false;
            }
            return true;
        }

        //ZD 1002600
        public bool UpdateRPRMConferenceStatus(int confid, int instanceid, NS_MESSENGER.Conference.eStatus confStatus, int isRPRMConf, bool iRecurring)  //ZD 104496
        {
            int iConfStatus = 0;
            if (confStatus == NS_MESSENGER.Conference.eStatus.SCHEDULED)
                iConfStatus = 0;
            if (confStatus == NS_MESSENGER.Conference.eStatus.PENDING)
                iConfStatus = 1;
            if (confStatus == NS_MESSENGER.Conference.eStatus.TERMINATED)
                iConfStatus = 3;
            if (confStatus == NS_MESSENGER.Conference.eStatus.ONGOING)
                iConfStatus = 5;
            if (confStatus == NS_MESSENGER.Conference.eStatus.OnMCU)
                iConfStatus = 6;
            if (confStatus == NS_MESSENGER.Conference.eStatus.COMPLETED)
                iConfStatus = 7;
            if (confStatus == NS_MESSENGER.Conference.eStatus.DELETED)
                iConfStatus = 9;

            string query = "update Conf_Conference_D set status = " + iConfStatus.ToString() + " , isRPRMConf = " + isRPRMConf.ToString();
            query += " where confid = " + confid.ToString();
            if (!iRecurring)
                query += " and instanceid = " + instanceid.ToString();

            bool ret = NonSelectCommand(query);
            if (!ret)
            {
                logger.Exception(100, "Error executing query.");
                return false;
            }
            return true;
        }

        //FB 2390 Start

        #region UpdateCallId
        /// <summary>
        /// UpdateCallId
        /// </summary>
        /// <param name="confid"></param>
        /// <param name="instanceid"></param>
        /// <param name="callId"></param>
        /// <returns></returns>
        public bool UpdateCallId(int confid, int instanceid, int callId)
        {
            string query = "update Conf_Conference_D set Connect2 = " + callId.ToString();
            query += " where confid = " + confid.ToString();
            query += " and instanceid = " + instanceid.ToString();

            bool ret = NonSelectCommand(query);
            if (!ret)
            {
                logger.Exception(100, "Error executing query.");
                return false;
            }
            return true;
        }
        # endregion

        # region FetchCallId
        /// <summary>
        /// FetchCallId
        /// </summary>
        /// <param name="confid"></param>
        /// <param name="instanceid"></param>
        /// <param name="party"></param>
        /// <returns></returns>
        public bool FetchCallId(int confid, int instanceid, ref NS_MESSENGER.Party party)
        {
            DataSet ds = new DataSet();
            string dsTable = "Conference";

            string query = "Select connect2 from Conf_Conference_D ";
            query += " where confid = " + confid.ToString();
            query += " and instanceid = " + instanceid.ToString();

            bool ret = SelectCommand(query,ref ds,dsTable);
			if (!ret)
			{
			        logger.Exception(100,"SQL error");
					return (false);
			}

			for (int i=0;i<ds.Tables[dsTable].Rows.Count ; i++)
			{
			    try
				{
				    party.ip2pCallid = Int32.Parse(ds.Tables[dsTable].Rows[0]["connect2"].ToString());
                    logger.Trace("P2P connection Id = " + party.ip2pCallid);
				} 
				catch (Exception e)
				{
					logger.Exception (100,e.Message);
					continue;
				}
			}				
			return(true);
        }
        # endregion

        //FB 2390 End
        public bool UpdateLdapSyncTime()
        {
            try
            {
                string query = "update ldap_serverconfig_d set synctime = getutcdate()";
                bool ret = NonSelectCommand(query);
                if (!ret)
                {
                    logger.Exception(100, "Error executing query.");
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }

        public bool UpdateMailServerLastRunDateTime()
        {
            try
            {
                string query = "update sys_mailserver_d set lastrundatetime = getutcdate()";
                bool ret = NonSelectCommand(query);
                if (!ret)
                {
                    logger.Exception(100, "Error executing query.");
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        public bool UpdateConferenceLastRunDateTime(int confid, int instanceid)
        {
            try
            {
                string query = "update conf_conference_d set lastrundatetime = getutcdate() where confid = " + confid.ToString() + " and instanceid = " + instanceid.ToString();
                bool ret = NonSelectCommand(query);
                if (!ret)
                {
                    logger.Exception(100, "Error executing query.");
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
       


        public bool UpdateP2PConferenceLastRunDateTime(int confid, int instanceid)
        {
            try
            {
                string query = "update conf_conference_d set lastrundatetime = getutcdate() where confid = " + confid.ToString() + " and instanceid = " + instanceid.ToString();
                query += " update conf_user_d set lastrundatetime = getutcdate() where confid = " + confid.ToString() + " and instanceid = " + instanceid.ToString();
                query += " update conf_room_d set lastrundatetime = getutcdate() where confid = " + confid.ToString() + " and instanceid = " + instanceid.ToString();

                bool ret = NonSelectCommand(query);
                if (!ret)
                {
                    logger.Exception(100, "Error executing query.");
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        public bool UpdateConferenceUserLastRunDateTime(int confid,int instanceid, int userid )
        {
            try
            {
                string query = "update conf_user_d set lastrundatetime = getutcdate() where confid = " + confid.ToString() + " and instanceid = " + instanceid.ToString() + " and userid = " + userid.ToString();
                bool ret = NonSelectCommand(query);
                if (!ret)
                {
                    logger.Exception(100, "Error executing query.");
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        public bool UpdateConferenceUserStatus(int confid, int instanceid, int userid,NS_MESSENGER.Party.eOngoingStatus ongoingStatus )
        {
            try
            {
                int onlineStatus = -1;

                if (ongoingStatus == NS_MESSENGER.Party.eOngoingStatus.FULL_CONNECT)
                {
                    onlineStatus = 2; //Connected - Blue Status
                }
                else
                {
                    if (ongoingStatus == NS_MESSENGER.Party.eOngoingStatus.DIS_CONNECT)
                    {
                        onlineStatus = 0; //Disconnected
                    }
                    else if (ongoingStatus == NS_MESSENGER.Party.eOngoingStatus.PARTIAL_CONNECT)
                    {
                        onlineStatus = 1; //Connecting
                    }
                    else if (ongoingStatus == NS_MESSENGER.Party.eOngoingStatus.ONLINE)
                    {
                        onlineStatus = 3; //Online
                    }
                    else
                    {
                        onlineStatus = -1; //Unreachable 
                    }
                }

                string query = "update conf_user_d set  lastrundatetime = getutcdate(), onlineStatus = " + onlineStatus.ToString() + " where confid = " + confid.ToString() + " and instanceid = " + instanceid.ToString();
                if (userid > -1)
                {
                    query += " and userid = " + userid.ToString() + " and profileid =" + eptProfileID;
                }
                bool ret = NonSelectCommand(query);
                if (!ret)
                {
                    logger.Exception(100, "Error executing query.");
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        
        public bool UpdateConferenceRoomStatus(int confid, int instanceid, int roomid, NS_MESSENGER.Party.eOngoingStatus ongoingStatus)
        {
            try
            {
                int onlineStatus = -1;

                if (ongoingStatus == NS_MESSENGER.Party.eOngoingStatus.FULL_CONNECT)
                {
                    onlineStatus = 2; //Connected - Blue Status
                }
                else
                {
                    if (ongoingStatus == NS_MESSENGER.Party.eOngoingStatus.DIS_CONNECT)
                    {
                        onlineStatus = 0; //Disconnected
                    }
                    else if (ongoingStatus == NS_MESSENGER.Party.eOngoingStatus.PARTIAL_CONNECT)
                    {
                        onlineStatus = 1; //Connecting
                    }
                    else if (ongoingStatus == NS_MESSENGER.Party.eOngoingStatus.ONLINE)
                    {
                        onlineStatus = 3; //Online
                    }
                    else
                    {
                        onlineStatus = -1; //Unreachable 
                    }
                }

                string query = "update conf_room_d set lastrundatetime = getutcdate() , onlineStatus = " + onlineStatus.ToString() + " where confid = " + confid.ToString() + " and instanceid = " + instanceid.ToString();
                if (roomid > -1)
                {
                    query += " and roomid = " + roomid.ToString();
                } 

                bool ret = NonSelectCommand(query);
                if (!ret)
                {
                    logger.Exception(100, "Error executing query.");
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }        
        public bool UpdateConferenceCascadeStatus(int confid, int instanceid, int cascadelinkid, NS_MESSENGER.Party.eOngoingStatus ongoingStatus)
        {
            try
            {
                int onlineStatus = -1;

                if (ongoingStatus == NS_MESSENGER.Party.eOngoingStatus.FULL_CONNECT)
                {
                    onlineStatus = 2; //Connected - Blue Status
                }
                else
                {
                    if (ongoingStatus == NS_MESSENGER.Party.eOngoingStatus.DIS_CONNECT)
                    {
                        onlineStatus = 0; //Disconnected
                    }
                    else if (ongoingStatus == NS_MESSENGER.Party.eOngoingStatus.PARTIAL_CONNECT)
                    {
                        onlineStatus = 1; //Connecting
                    }
                    else if (ongoingStatus == NS_MESSENGER.Party.eOngoingStatus.ONLINE)
                    {
                        onlineStatus = 3; //Online
                    }
                    else
                    {
                        onlineStatus = -1; //Unreachable 
                    }
                }

                string query = "update conf_cascade_d set  lastrundatetime = getutcdate(), onlineStatus = " + onlineStatus.ToString() + " where confid = " + confid.ToString() + " and instanceid = " + instanceid.ToString();
                if (cascadelinkid > -1)
                {
                    query += " and cascadelinkid = " + cascadelinkid.ToString();
                }
                bool ret = NonSelectCommand(query);
                if (!ret)
                {
                    logger.Exception(100, "Error executing query.");
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }


        public bool UpdateP2PConferenceEndpointStatus(int confid, int instanceid, NS_MESSENGER.Party.eOngoingStatus ongoingStatus)// FB 2710
        {
            bool ret = false;
            string query = "";
            try
            {
                int onlineStatus = -1;

                if (ongoingStatus == NS_MESSENGER.Party.eOngoingStatus.FULL_CONNECT)
                {
                    onlineStatus = 2; //Connected - Blue Status
                }
                else
                {
                    if (ongoingStatus == NS_MESSENGER.Party.eOngoingStatus.DIS_CONNECT)
                    {
                        onlineStatus = 0; //Disconnected
                    }
                    else if (ongoingStatus == NS_MESSENGER.Party.eOngoingStatus.PARTIAL_CONNECT)
                    {
                        onlineStatus = 1; //Connecting
                    }
                    else if (ongoingStatus == NS_MESSENGER.Party.eOngoingStatus.ONLINE)
                    {
                        onlineStatus = 3; //Online
                    }
                    else
                    {
                        onlineStatus = -1; //Unreachable 
                    }
                }

                query = "update conf_user_d set  lastrundatetime = getutcdate(), onlineStatus = " + onlineStatus.ToString() + " where confid = " + confid.ToString() + " and instanceid = " + instanceid.ToString();
               
                ret = NonSelectCommand(query);
                if (!ret)
                {
                    logger.Exception(100, "Error executing query.");
                    return false;
                }

                query = "update conf_room_d set lastrundatetime = getutcdate() , onlineStatus = " + onlineStatus.ToString() + " where confid = " + confid.ToString() + " and instanceid = " + instanceid.ToString();                

                ret = NonSelectCommand(query);
                if (!ret)
                {
                    logger.Exception(100, "Error executing query.");
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }

        public bool LogInMcuIssue(int mcuid, string operation, string message)
		{
			string query = "insert into Mcu_Log_D values (" + mcuid.ToString();
			query += ",'" + operation;
			query += "',getdate(),'" + message;
			query += "')";
			bool ret = NonSelectCommand (query);
			if (!ret)
			{
				logger.Exception(10,"Error executing query.");
				return false;
			}
			return true;
		}
		
		internal bool ConvertTimeAcrossTimeZone(DateTime inputDateTime,int outputTimeZoneID,ref DateTime outputDateTime)
		{
			// Convert date/time across timezones
			DataSet ds = new DataSet();
			string dsTable = "LocalTime";
			string query = "Select  dbo.ChangeTime(" +  outputTimeZoneID.ToString() + ",'" + inputDateTime.ToString() + "')";
			bool ret = SelectCommand(query,ref ds,dsTable);
			if (!ret)
			{
				logger.Exception(10,"SQL error");
				return (false);
			}		
			if (ds.Tables[dsTable].Rows.Count >0)
			{
                outputDateTime = DateTime.Parse(ds.Tables[dsTable].Rows[0][0].ToString());
				return true;
			}
			else
				return false;			
		}

		private bool GenerateConfName(NS_MESSENGER.Conference conf,ref string confName)
		{
			// concatenate the conf uniqueid with conf external name 
			// format : tech meeting...(1234)
			// limit of total 30 chars.

			try 
			{
				if (conf.sExternalName.Trim().Length > 20)
				{
					// truncate to 20 chars 
					conf.sExternalName = conf.sExternalName.Substring(0,19).Trim();
					conf.sExternalName += "..."; 
				}

				confName = conf.sExternalName +"("+ conf.iDbNumName.ToString().Trim()+ ")";
				
				return true;
			}
			catch (Exception e)
			{
				logger.Exception (100,e.Message);
				return false;
			}
		}

		private bool ParseConfName(string confNameOnMcu,ref int confUniqueID)
		{
			// format : tech meeting...(1234)
			// remove the unique-id from last 6-7 chars

			try 
			{
				logger.Trace ("ConfNameOnMcu = " + confNameOnMcu);

				int start= confNameOnMcu.LastIndexOf("(");
				//logger.Trace ("Start = " + start.ToString());
				int len = confNameOnMcu.Length;
				//logger.Trace ("Len = " + len.ToString());

				confUniqueID = Int32.Parse(confNameOnMcu.Substring(start+1,len-start-2));								

				return true;
			}
			catch (Exception e)
			{
				logger.Exception (100,e.Message);
				return false;
			}
		}

		#endregion

		#region ConfMonitor Methods

		public bool FetchUser(ref NS_MESSENGER.Party party)
		{
			DataSet ds = new DataSet();
			string dsTable = "User";

			// check party name validity
			if(party.sMcuName ==  null || party.sMcuName.Length < 1)
			{
				logger.Trace ("Invalid party name.");
				return false;
			}

			// select the user 
			string query =	"select UserID from Usr_List_D U ";
			query += " where Email = '" + party.sMcuName;
			query += "'";

			bool ret=false;
			ret = SelectCommand(query,ref ds,dsTable);
			if (!ret)
			{
				logger.Exception(100,"SQL error");
				return (false);
			}

			if (ds.Tables[dsTable].Rows.Count > 0)
			{
			
				// userID found
				party.iDbId = Int32.Parse(ds.Tables[dsTable].Rows[0][0].ToString());	
				return true;
			}
			else
				// userID not found
				return false;
		}

		public bool FetchGuest(ref NS_MESSENGER.Party party)
		{
			DataSet ds = new DataSet();
			string dsTable = "Guest";

			// check party name validity
			if(party.sMcuName ==  null || party.sMcuName.Length < 1)
			{
				logger.Trace ("Invalid party name.");
				return false;
			}

			// select the guest 
			string query =	"select UserID from Usr_GuestList_D G ";
			query += " where Email = '" + party.sMcuName;
			query += "'";

			bool ret = false;
			ret = SelectCommand(query,ref ds,dsTable);
			if (!ret)
			{
				logger.Exception(100,"SQL error");
				return (false);
			}

			if (ds.Tables[dsTable].Rows.Count > 0)
			{
				// userID found
				party.iDbId = Int32.Parse(ds.Tables[dsTable].Rows[0][0].ToString());	
				
				return true;
			}
			else
				// userID not found
				return false;
		}

		public bool FetchRoom(ref NS_MESSENGER.Party party)
		{
			try 
			{
				// Format for room name on bridges is : Endpoint name associated with the room.
				logger.Trace (party.sMcuName);
				party.sMcuName = party.sMcuName.Trim();
				
				// check party name validity
				if(party.sMcuName ==  null || party.sMcuName.Length < 1)
				{
					logger.Trace ("Invalid party name.");
					return false;
				}

				DataSet ds = new DataSet();
				string dsTable = "Room";

				// find the endpoint and then the first room associated with it.
				string query =	"select r.RoomID from Loc_Room_D r, Ept_List_D e ";
				query += " where e.[name] = '" + party.sMcuName + "'";
                query += " and r.endpointid = e.endpointid";                

				logger.Trace (query);

				bool ret = false;
				ret = SelectCommand(query,ref ds,dsTable);
				if (!ret)
				{
					logger.Exception(100,"SQL error");
					return (false);
				}

				if (ds.Tables[dsTable].Rows.Count > 0)
				{
					// roomID found
					party.iDbId = Int32.Parse(ds.Tables[dsTable].Rows[0][0].ToString());					
					return true;
				}
				else
					// roomID not found
					return false;
			}
			catch (Exception e)
			{
				logger.Exception (100,e.Message);
				return false;
			}
		}

		public bool FetchCascadeLink(ref NS_MESSENGER.Party party,int confid,int instanceid)
		{
			
			DataSet ds = new DataSet();
			string dsTable = "CascadeLink";

			// select the room 
			string query =	"select CascadeLinkID from Conf_Cascade_D ";
			query += " where confid = " + confid.ToString();
			query += " and instanceid = " + instanceid.ToString();
			query += " and CascadeLinkName = '" + party.sMcuName;
			query += "'";

			bool ret = false;
			ret = SelectCommand(query,ref ds,dsTable);
			if (!ret)
			{
				logger.Exception(100,"SQL error");
				return (false);
			}

			if (ds.Tables[dsTable].Rows.Count > 0)
			{
				// cascadeID found
				party.iDbId = Int32.Parse(ds.Tables[dsTable].Rows[0][0].ToString());					
				return true;
			}
			else
				// cascadeID not found
				return false;
		}

#if COMMENT
		bool ConvertToDbLineRate(NS_MESSENGER.Party.e_LineRate mcuLineRate,ref int dbLineRate)
		{
			if (mcuLineRate == NS_MESSENGER.Party.e_LineRate.K56)
			{
				dbLineRate = 56;
				return true;
			}
			
			if (mcuLineRate == NS_MESSENGER.Party.e_LineRate.K64)
			{
				dbLineRate = 64;
				return true;
			}
			if (mcuLineRate == NS_MESSENGER.Party.e_LineRate.K128)
			{
				dbLineRate = 128;
				return true;
			}
			if (mcuLineRate == NS_MESSENGER.Party.e_LineRate.K256)
			{
				dbLineRate = 256;
				return true;
			}
			if (mcuLineRate == NS_MESSENGER.Party.e_LineRate.K512)
			{
				dbLineRate = 512;
				return true;
			}
			if (mcuLineRate == NS_MESSENGER.Party.e_LineRate.K768)
			{
				dbLineRate = 768;
				return true;
			}
			if (mcuLineRate == NS_MESSENGER.Party.e_LineRate.M1024)
			{
				dbLineRate = 1024;
				return true;
			}
			if (mcuLineRate == NS_MESSENGER.Party.e_LineRate.M1472)
			{
				dbLineRate = 1472;
				return true;
			}
			
			// default value = 384 kbps	
			dbLineRate = 384;
			return true;
		}
#endif

		public bool UpdatePartyWithMCUMonitorData(NS_MESSENGER.Party party,int confid,int instanceid)
		{
			try 
			{
				#region party type : conversion from mcu to db values
				int partyType = 0;
				if (party.etType == NS_MESSENGER.Party.eType.USER)
				{
					partyType = 1;	
				}
				else 
				{
					if (party.etType == NS_MESSENGER.Party.eType.ROOM)
					{

						partyType = 2;
					}
					else
					{
						if (party.etType == NS_MESSENGER.Party.eType.GUEST)
						{

							partyType = 3;
						}
						else
						{
							// default = cascade link
							partyType = 4;
						}
					}
				}
				#endregion

				#region party ongoing status : conversion from mcu to db values
				int partyStatus = 0;
				if (party.etStatus == NS_MESSENGER.Party.eOngoingStatus.FULL_CONNECT)
				{
					partyStatus = 1;
				}
				else
				{
					if (party.etStatus == NS_MESSENGER.Party.eOngoingStatus.PARTIAL_CONNECT)
					{
						partyStatus = 2;
					}
					else
					{
						// default = disconnected.
						partyStatus = 3;
					}
				}
				#endregion

				//Conf line rate is being fetched from scheduled data instead of live mcu
				//TODO: Change this later. Plus support addtl conf/party attributes in later versions.
				int confLineRate = 384;
				DataSet ds = new DataSet();
				string dsTable = "Params";
				string sqlQuery= "select linerateid from Conf_AdvAVParams_D where confid = " + confid.ToString() + " and instanceid = " + instanceid.ToString();
				bool ret = false;
				ret = SelectCommand(sqlQuery,ref ds,dsTable);
				if (!ret)
				{
					logger.Exception(100,"SQL error");				
					// do nothing and just proceed
				}
				else
				{
					if (ds.Tables[dsTable].Rows.Count > 0)
					{
						confLineRate = Int32.Parse(ds.Tables[dsTable].Rows[0][0].ToString());
					}
				}	
				
				// insert the party status in the db
				string query = "Insert into Conf_Monitor_D values (" + confid.ToString();
				query += " , " + instanceid.ToString();
				query += " , " + party.iDbId.ToString();
				query += " , " + partyType.ToString();
				query += " ,'" + DateTime.Now.ToString() ;
				query += "','" + party.sMcuAddress;
				query += "'," + confLineRate.ToString();
				query += "," + partyStatus.ToString();
				query += ")";

				// execute the query
				ret = false;
				ret = NonSelectCommand(query);
				if (!ret) 
				{
					// log the error
					logger.Exception (100,"query failed. query =" + query);						
				}
				return true;
			}
			catch (Exception e)
			{
				logger.Exception (100,"Error updating participant.Exception = " + e.Message);
				return false;
			}
		}

		/*
		public bool FetchTerminatedConfs(ref Queue terminateConfList)
		{
			DataSet ds = new DataSet();
			string dsTable = "TerminatedList";

			// fetch the conf list 
			// TODO : change the query to retreive only confs which fall in range.
			string query =	" select t.confid,t.instanceid,c.confnumname,t.bridgeid";
			query += " from Conf_Conference_D c , confterminate t ";
			query += " where t.confid = c.confid and t.instanceid = c.instanceid and t.terminated = 0";
			query += " and (datediff(d,getutcdate(),t.datetimeterminated) < 1)";
			bool ret = SelectCommand(query,ref ds,dsTable);
			if (!ret)
			{
				logger.Exception(100,"SQL error");
				return (false);
			}
			
			for (int i = 0 ;i < ds.Tables[dsTable].Rows.Count ; i++)
			{
				NS_MESSENGER.Conference conf = new NS_MESSENGER.Conference();
				conf.iDbID = Int32.Parse(ds.Tables[dsTable].Rows[i][0].ToString()); 
				conf.iInstanceID = Int32.Parse(ds.Tables[dsTable].Rows[i][1].ToString()); 
				conf.sMcuName = ds.Tables[dsTable].Rows[i][2].ToString(); 
				conf.cMcu.iDbId = Int32.Parse(ds.Tables[dsTable].Rows[i][3].ToString());

				// fetch the bridge info
				bool ret1 = FetchMcu(conf.cMcu.iDbId,ref conf.cMcu);
				if (!ret1)
				{
					logger.Exception(100,"Error fetching mcu info. McuDbID = " + conf.cMcu.iDbId.ToString());
					continue;
				}
					
				// add the conf to the list
				terminateConfList.Enqueue(conf);
			}

			return true;
		}

		

		public bool UpdateTerminatedConfStatus(int confid,int instanceid,int bridgeid)
		{			
			string query = "update confterminate ";
			query += " set terminated = 1 "; 	
			query += " where confid = " + confid.ToString();
			query += " and instanceid = " + instanceid.ToString();
			query += " and bridgeid = " + bridgeid.ToString();
			bool ret = NonSelectCommand (query);
			if (!ret)
			{
				logger.Exception(100,"Error executing query.");
				return false;
			}
			return true;
		}


		// Zombie confs are one which are still lying in the conf terminate table inspite of getting deleted from the bridge.
		public bool ZombieConfsCleanup ()
		{
			DataSet ds = new DataSet();
			string dsTable = "ZombieList";

			string query = "Select * from ConfTerminate where terminated = 0";
			bool ret = SelectCommand(query,ref ds,dsTable);
			if (!ret)
			{
				logger.Exception(100,"SQL error");
				return (false);
			}

			for (int i=0;i<ds.Tables[dsTable].Rows.Count ; i++)
			{
				string confid = ds.Tables[dsTable].Rows[i][0].ToString();
				string instanceid = ds.Tables[dsTable].Rows[i][1].ToString();
				
				DataSet ds1 = new DataSet();
				string dsTable1 = "Conf";

				string query1 = "Select confdate,duration from Conference ";
				query1 += " where confid = " + confid + " and instanceid = " + instanceid;
				bool ret1 = SelectCommand(query1,ref ds1,dsTable1);
				if (!ret1)
				{
					logger.Exception(100,"SQL error");
					continue;
				}
				
				if (ds1.Tables[dsTable1].Rows.Count > 0)
				{
					DateTime  dtStartConf = DateTime.Parse(ds1.Tables[dsTable1].Rows[0][0].ToString());
					int iDuration = Int32.Parse(ds1.Tables[dsTable1].Rows[0][1].ToString());

					DateTime dtEndConf = dtStartConf.AddMinutes(iDuration);
				
					// calculate = current server time - conf end time 
					TimeSpan dtDiff =  DateTime.UtcNow.Subtract(dtEndConf) ;
					logger.Trace ("Diff Hrs = " + dtDiff.ToString());
					
					// check if the diff is greater than 5 hrs (hardcoded)
					if (dtDiff.TotalHours > 5)
					{
						// update conf record
						string query2 = "update confterminate set terminated = 1 where confid = " + confid + " and instanceid = " + instanceid ;
						bool ret2 = NonSelectCommand (query2);
						if (!ret2)
						{
							logger.Exception(100,"Error executing query.");
							continue;
						}
					}
				}
			}
					
			return true;
		}
*/
		public bool FetchPartyRealTimeStatus(ref NS_MESSENGER.Party party)
		{
			// Fetch party's mute status .
			DataSet ds = new DataSet();
			string dsTable = "Mute";
			string query = null;
			
			if (party.etType == NS_MESSENGER.Party.eType.GUEST || party.etType == NS_MESSENGER.Party.eType.USER)
			{
				query = "Select Mute,Layout from Conf_User_D where confid = " + "123" + " and userid = " + party.iDbId.ToString();
			}
			else 
			{
				if (party.etType == NS_MESSENGER.Party.eType.ROOM)
				{
					query = "Select Mute,Layout from Conf_Room_D where confid = " + "123" + " and userid = " + party.iDbId.ToString();
				}
				else
				{
					query = "Select Mute,Layout from Conf_Cascade_D where confid = " + "123" + " and userid = " + party.iDbId.ToString();
				}
			}
			bool ret = SelectCommand(query,ref ds,dsTable);
			if (!ret)
			{
				logger.Exception(100,"SQL error");
				return (false);
			}
			if (ds.Tables[dsTable].Rows.Count > 0)
			{
				

			}

			return true;
		}
		#endregion

		#region Misc Tasks
		public bool DeleteZombieConfs()
		{
			try 
			{
				DataSet ds = new DataSet();
				string dsTable = "MCU";
				string query = "Select U.Email,C.Confid,C.InstanceId,C.ExternalName,C.ConfDate,C.ConfNumName,T.Timezone from Conf_Conference_D C, Usr_List_D U, Gen_Timezone_S T ";
				query += " WHERE C.deleted = 0 AND C.status = 1 AND dateadd(minute, -5, C.confdate) <= getutcdate() ";
				query += " AND dateadd(minute, C.duration, C.confdate) > getutcdate() ";
				query += " AND c.timezone = t.timezoneid AND C.Owner = U.UserID";
				bool ret = SelectCommand(query,ref ds,dsTable);
				if (!ret)
				{
					logger.Exception(100,"SQL error");
					return (false);
				}	
				
				for (int i = 0; i < ds.Tables[dsTable].Rows.Count ; i++)
				{
					string hostEmail = ds.Tables[dsTable].Rows[i]["Email"].ToString().Trim();
					int confid = Int32.Parse(ds.Tables[dsTable].Rows[i]["Confid"].ToString().Trim());
					int instanceid = Int32.Parse(ds.Tables[dsTable].Rows[i]["InstanceId"].ToString().Trim());
					string externalName = ds.Tables[dsTable].Rows[i]["ExternalName"].ToString().Trim();
					DateTime confDate = DateTime.Parse(ds.Tables[dsTable].Rows[i]["ConfDate"].ToString().Trim());
					int uniqueId = Int32.Parse(ds.Tables[dsTable].Rows[i]["ConfNumName"].ToString().Trim());
					string timezoneName = ds.Tables[dsTable].Rows[i]["Timezone"].ToString().Trim();
					
					// delete the conference 
					query = "UPDATE  Conf_Conference_D SET deleted = 1, deletereason = 'Pending conference: This conference has been automatically deleted to release resources.'";
					query += " WHERE confid = " +  confid.ToString();
					query += " AND instanceid = " + instanceid.ToString();				
					// execute the query
					ret = false;
					ret = NonSelectCommand(query);
					if (!ret) 
					{
						// log the error 
						logger.Exception (100,"query failed. query =" + query);						
						return false;
					}		
		
					// send notification email to host 
					NS_MESSENGER.Email email = new NS_MESSENGER.Email();
					email.To = hostEmail;
					email.isHTML = true;
					email.Subject = "[VRM Alert] Pending conference deleted";
					email.Body = "<html><body><BR>This is a system generated notification. A pending conference setup by you was deleted automatically by the system to release resources. <br> The action was taken since no approver had approved the conference before the start time. <br> Conference details are as follows:";
					email.Body += "<br>Conference Name : " + externalName;
					email.Body += "<br>Conference Start Date & Time : " + confDate.ToString() + " " + timezoneName;
					email.Body += "<br>Conference UniqueID : " + uniqueId.ToString();
					
					ret = InsertEmailInQueue(email);
					if (!ret) 
					{
						logger.Trace ("Pending conference deletion notification email not sent out."); 
					}
				}
				
				return true;
			}
			catch (Exception e)
			{
				logger.Exception (100,"Error deleting zombie conference.Exception = " + e.Message);
				return false;
			}
		}
		public bool DeleteUnsentOldEmails()
		{
			try 
			{
				// Delete emails with more than 250 retries 				
				string query = " DELETE FROM Email_Queue_D WHERE retrycount > 250";

				// execute the query
				bool ret = false;
				ret = NonSelectCommand(query);
				if (!ret) 
				{
					// log the error 
					logger.Exception (100,"query failed. query =" + query);						
					return false;
				}
				return true;
			}
			catch (Exception e)
			{
				logger.Exception (100,"Error deleting unsent emails.Exception = " + e.Message);
				return false;
			}
		}
		
		public bool InsertEmailInQueue(NS_MESSENGER.Email email)
		{
			// Tech Support Info
			/*			DataSet ds = new DataSet();
						string dsTable = "TechSupport";
						string query = "Select name,email,phone,info from Sys_TechSupport_D ";
						bool ret = SelectCommand(query,ref ds,dsTable);
						if (!ret)
						{
							logger.Exception(100,"SQL error");
							return (false);
						}	

						string tsName = ds.Tables[dsTable].Rows[0]["Name"].ToString().Trim();
						string tsEmail = ds.Tables[dsTable].Rows[0]["Email"].ToString().Trim();
						string tsPhone = ds.Tables[dsTable].Rows[0]["Phone"].ToString().Trim();
						string tsInfo = ds.Tables[dsTable].Rows[0]["Info"].ToString().Trim();

						email.Body += "<BR><BR><BR>Technical Support:";
						email.Body += "<BR>---------------------";
						email.Body += "<BR>Name : "+ tsName;
						email.Body += "<BR>Email : "+ tsEmail;
						email.Body += "<BR>Phone : "+ tsPhone;
						if (tsInfo.Length > 1)
						{
							email.Body += "<BR>Additional Info : "+ tsInfo;
						}

						// Disclaimer Text
						DataSet ds1 = new DataSet();
						string dsTable1 = "Email";
						string query1 = "Select MessageTemplate from Sys_MailServer_D ";
						bool ret1 = SelectCommand(query1,ref ds1,dsTable1);
						if (!ret1)
						{
							logger.Exception(100,"SQL error");
							return (false);
						}	

						string disclaimerText = ds1.Tables[dsTable1].Rows[0]["MessageTemplate"].ToString().Trim();
						if (disclaimerText.Length > 3)
						{
							email.Body += "<BR><BR>";
							email.Body += "<BR>---------------";
							email.Body += "<BR>" + disclaimerText;
						}

						// closing the tags
						email.Body += "</body></html>";
			*/
			// insert email in queue
            if (!string.IsNullOrEmpty(email.From) && !string.IsNullOrEmpty(email.To)) //ZD 101885
            {
                string query = " Declare @uuid as int";
                query += " set @uuid = (select max(uuid) from email_queue_d)";
                query += "if (@uuid IS NULL) begin set @uuid = 0 end ";
                query += "Insert  into Email_Queue_D ([from],[to],subject,datetimecreated,message,uuid,LastRetryDateTime,Attachment,cc,BCC) values (";
                query += "'" + email.From + "','" + email.To + "','" + email.Subject + "',getutcdate(),'" + email.Body + "',(@uuid+1),'" + DateTime.UtcNow + "','','','')";

                // execute the query
                bool ret = NonSelectCommand(query);
                if (!ret)
                {
                    // log the error 
                    logger.Exception(100, "query failed. query =" + query);
                    return false;
                }
            }
			return true;
		}
		
		#endregion

		public bool FetchConfAlerts(NS_MESSENGER.Conference conf, ref Queue alertQ)
		{
			// Retreive  the  alerts
			DataSet ds = new DataSet();
			string dsTable = "ConfAlerts";
			string query = "Select C.AlertTypeID,C.[Message],C.[Timestamp],G.Description from Conf_Alerts_D C, Gen_AlertType_S G where C.ConfID = " + conf.iDbID.ToString() + " and C.InstanceID = " + conf.iInstanceID.ToString();
			query += " and C.AlertTypeID = G.AlertTypeID order by C.[Timestamp] desc";
			bool ret = SelectCommand(query,ref ds,dsTable);
			if (!ret)
			{
				logger.Exception(10,"SQL error");
				return (false);
			}		
			
			for (int i=0;i<ds.Tables[dsTable].Rows.Count ; i++)
			{
				NS_MESSENGER.Alert alert = new NS_MESSENGER.Alert();
				try
				{
					alert.confID = conf.iDbID;
					alert.instanceID = conf.iInstanceID;
					alert.typeID = Int32.Parse(ds.Tables[dsTable].Rows[i]["AlertTypeID"].ToString().Trim());
					alert.message = ds.Tables[dsTable].Rows[i]["Description"].ToString().Trim() + "Message = " +ds.Tables[dsTable].Rows[i]["Message"].ToString().Trim();
					alert.timestamp = DateTime.Parse(ds.Tables[dsTable].Rows[i]["Timestamp"].ToString().Trim());
					
					alertQ.Enqueue(alert);			
				}
				catch (Exception e)
				{
					logger.Trace("Alert not in correct format : " + e.Message);
					continue;
				}
			}
			return true;						
		}
		
		public bool InsertConfAlert(NS_MESSENGER.Alert alert)
		{
			try
			{
				if (alert.typeID == 2) 
				{
					// do not insert "add party" alert
					return false;
				}
				if (alert.message == null) 
				{
					logger.Trace ("Invalid alert message.Message text is null.");
					return false;
				}

				if (alert.message.Trim().Length < 2 || alert.confID == 0 || alert.instanceID ==0 || alert.typeID == 0)
				{
					logger.Trace ("Invalid alert message.");
					return false;
				}

				string query = "DECLARE @typeid int";
				query += " DECLARE @confid int";
				query += " DECLARE @instanceid int";
				query += " SET @confid =" + alert.confID.ToString();
				query += " SET @instanceid =" + alert.instanceID.ToString();
				query += " SET @typeid =" + alert.typeID.ToString();
				query += " delete from Conf_Alerts_D where ConfID = @confid and InstanceID = @instanceid and AlertTypeID = @typeid ";
				query += " insert into Conf_Alerts_D values (@confid,@instanceid,@typeid,getutcdate(),'" + alert.message.Trim()+"')";

				bool ret = NonSelectCommand (query);
				if (!ret)
				{
					logger.Exception(10,"Error executing query.");
					return false;
				}
				return true;
			}
			catch (Exception e)
			{
				logger.Exception(100,e.Message);
				return false;
			}
		}

        //ZD 102515 START
        public bool DeleteRPRMConfAlert(NS_MESSENGER.Alert alert)
        {
            try
            {
                string query = " DECLARE @confid int";
                query += " DECLARE @instanceid int";
                query += " SET @confid =" + alert.confID.ToString();
                query += " SET @instanceid =" + alert.instanceID.ToString();
                query += "IF  EXISTS (SELECT ConfId FROM Conf_Alerts_D WHERE Confid =@confid and instanceId = @instanceid) delete from Conf_Alerts_D where Confid =@confid and instanceId = @instanceid";
                bool ret = NonSelectCommand(query);
                if (!ret)
                {
                    logger.Exception(10, "Error executing query.");
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        //ZD 102515 END
		
		public bool FetchConfHistory (int confID,int instanceID,ref Queue diffList)
		{
			try
			{
				DataSet ds = new DataSet();
				string dsTable = "ConfHistory";
			
				string query =	"select * from Conf_History_D";
                query += " where ConfID = '" + confID.ToString() + "'";
                query += " and instanceId = '" + instanceID.ToString() + "'";
				query += " and [commit] = 1 order by timestamp asc";
 
				bool ret = SelectCommand(query,ref ds,dsTable);
				if (!ret)
				{
					logger.Exception(10,"SQL error");
					return (false);
				}

				for (int i=0;i<ds.Tables[dsTable].Rows.Count ; i++)
				{
					NS_PERSISTENCE.DiffChanges diff = new NS_PERSISTENCE.DiffChanges();

					diff.confID = confID;
					diff.instanceID = instanceID;
					diff.eventType = Int32.Parse(ds.Tables[dsTable].Rows[i]["eventtype"].ToString());
					diff.message = ds.Tables[dsTable].Rows[i]["message"].ToString();
					diff.timestamp = DateTime.Parse(ds.Tables[dsTable].Rows[i]["timestamp"].ToString());
					diff.userID = Int32.Parse(ds.Tables[dsTable].Rows[i]["userid"].ToString());
					
					DataSet ds1 = new DataSet();
					string dsTable1 = "ConfHistory";
					string query1 =	"select * from [usr_list_d]";
                    query1 += " where userID = '" + diff.userID.ToString() + "'";
					bool ret1 = SelectCommand(query1,ref ds1,dsTable1);
					if (!ret1)
					{
						logger.Exception(10,"SQL error");
						return (false);
					}

					string firstName = null,lastName = null;
					if (ds1.Tables[dsTable1].Rows.Count >0)
					{
						firstName = ds1.Tables[dsTable1].Rows[0]["firstname"].ToString();
						lastName = ds1.Tables[dsTable1].Rows[0]["lastname"].ToString();
						diff.userName = firstName + " " + lastName;
						diff.userEmail = ds1.Tables[dsTable1].Rows[0]["email"].ToString();
						//diff.userTimezoneID = Int32.Parse(ds1.Tables[dsTable1].Rows[0]["timezone"].ToString());

					}					
					diffList.Enqueue(diff);
				}

				return true;
			}
			catch (Exception e)
			{
				logger.Exception(100,e.Message);
				return false;
			}
		}
		public bool FetchNextTransactionID(int confid,int instanceid,ref int transID)
		{
			
			DataSet ds = new DataSet();
			string dsTable = "ConfHistory";
			
			string query = " select max(transactionid) from Conf_History_D where confid = "+ confid.ToString() + " and instanceid = " + instanceid.ToString() ;
			bool ret = SelectCommand(query,ref ds,dsTable);
			if (!ret)
			{
				#region ONLY FOR V18. REMOVE IN V19... AUTOMATED TABLE CREATION... 
				string query1 = "CREATE TABLE [dbo].[Conf_History_D]([ConfID] [int] NULL,[InstanceID] [int] NULL,[TransactionID] [int] NULL,[EventType] [int] NULL,[Message] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,[UserID] [int] NULL,[Timestamp] [datetime] NULL,[Commit] [int] NOT NULL CONSTRAINT [DF_Conf_History_D_Commit]  DEFAULT ((0))) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]";
				bool ret1 = NonSelectCommand(query1);
				if (!ret1)
				{
					logger.Trace("SQL Conf_History_D table creation error");
					return false;
				}	
				#endregion					

				// now try again
				query = " select max(transactionid) from Conf_History_D where confid = "+ confid.ToString() + " and instanceid = " + instanceid.ToString() ;
				ret = SelectCommand(query,ref ds,dsTable);
				if (!ret)
				{
					logger.Trace ("Error fetching tranaction ID.");
					return false;
				}

				logger.Exception(10,"Error executing query.");
				return false;
			}
			
			// initialize
			transID = 0;
		
			try
			{
				if (ds.Tables[dsTable].Rows[0][0].ToString() != null)
				{
					// transaction ID
					transID = Int32.Parse(ds.Tables[dsTable].Rows[0][0].ToString().Trim());
					transID++;
				}
			}
			catch (Exception e)
			{
				logger.Trace(e.Message);			
			}

			return true;
		}

		public bool InsertConfDiffRecord(NS_PERSISTENCE.DiffChanges diff,int transID)
		{
			try
			{				
				string query = "insert into Conf_History_D values ("+ diff.confID.ToString() + "," + diff.instanceID.ToString() + "," + transID.ToString() + ","+ diff.eventType.ToString() +",'" + diff.message + "'," + diff.userID.ToString() + ",getutcdate(),0)";
				bool ret = NonSelectCommand (query);	
				if (!ret)
				{
					logger.Exception(10,"Error executing query.");
					return false;
				}
				
				return true;
			}
			catch (Exception e)
			{
				logger.Exception(100,e.Message);
				return false;
			}
		}

		public bool CommitDiffs(int confID,int instanceID, int transID)
		{
			try
			{
				logger.Trace ("Inside Commit Diffs.");
				string query = "update Conf_History_D set [commit] = 1 where confid = " + confID.ToString() + " and instanceid = " + instanceID.ToString() + " and transactionid = " + transID.ToString() ;
				bool ret = NonSelectCommand (query);	
				if (!ret)
				{
					logger.Exception(10,"Error executing query.");
					return false;
				}				
				return true;
			}
			catch (Exception e)
			{
				logger.Exception(100,e.Message);
				return false;
			}
		}

		public bool ShrinkDbTransLogFile()
		{
			try 
			{
				// Fetch the db trans log
				DataSet ds = new DataSet();
				string dsTable = "ShrinkLog";
				string query = "Select Name,Filename from sysfiles Where FILEID=2";
				bool ret = SelectCommand(query,ref ds,dsTable);
				if (!ret)
				{
					logger.Exception(10,"SQL error");
					return (false);
				}		

				// log file details
				string logicalName = ds.Tables[dsTable].Rows[0]["Name"].ToString().Trim();
				string fileName = ds.Tables[dsTable].Rows[0]["FileName"].ToString().Trim();

				// Truncate the transaction log with no backup taken
				// Hopefully, the SQL Admin is taking regular backups becos on execution of this there is no going back.
				query = "BACKUP LOG " + configParams.databaseName.Trim() + " WITH TRUNCATE_ONLY";
				// Shrink the transaction log to 1 GB = 1024 MB(magic number)
				query += " DBCC SHRINKFILE (" + logicalName.Trim() + ",1024)";
				ret = false;
				ret = NonSelectCommand (query);
				if (!ret)
				{
					logger.Exception(10,"Error executing query.");
					return false;
				}
		
				return true;	
			}
			catch (Exception e)
			{
				logger.Exception(100,e.Message);
				return false;
			}
		}

		public bool DeleteLogRecords()
		{
			try 
			{
				// Fetch the db trans log
				DataSet ds = new DataSet();
				string dsTable = "DeleteLog";
				string query = "select logmodule, loglife from Err_logPrefs_S";
				bool ret = SelectCommand(query,ref ds,dsTable);
				if (!ret)
				{
					logger.Exception(10,"SQL error");
					return (false);
				}		
				
				// cycle thru each of the modules
				for (int i=0;i<ds.Tables[dsTable].Rows.Count ; i++)
				{				
					try
					{
						// retreive module's log prefs
						string moduleName = ds.Tables[dsTable].Rows[i]["logmodule"].ToString().Trim();
						logger.Trace ("Module name : "+ moduleName);
						logger.Trace ("Log Life (in days) before : "+ ds.Tables[dsTable].Rows[i]["loglife"].ToString().Trim());
						int logLife = 7;
						try
						{
							// TODO: Weird exception being thrown.
							logLife = Int32.Parse(ds.Tables[dsTable].Rows[i]["loglife"].ToString().Trim());	
							logger.Trace ("Log Life (in days) : "+ logLife.ToString("s"));
						}
						catch (Exception e)
						{
							logger.Trace (e.Message+ "---" + e.StackTrace  );
						}
						// 0 = do not delete log records ever
						if (logLife !=0)
						{
							logger.Trace ("Log Life 2 (in days) : "+ logLife.ToString("s"));
							DateTime td = DateTime.Today; 
							TimeSpan ts = new TimeSpan(logLife,0,0,0);						
							DateTime diff = td.Subtract(ts);

							// delete all the logs for the module
							string query1 = "delete from Err_Log_D where timelogged <'" + diff.ToString("G").Trim() + "' and modulename LIKE '" + moduleName + "'";
							ret = false;
							ret = NonSelectCommand (query1);
							if (!ret)
							{
								logger.Exception(10,"Error executing query.");
								return false;
							}
						}
					}
					catch (Exception e)
					{
						logger.Trace("Invalid log setting for module. Error : " + e.Message);
						continue;
					}
				}
		
				return true;	
			}
			catch (Exception e)
			{
				logger.Exception(100,e.Message);
				return false;
			}
		}
		
		public bool DeleteAllLogRecords()
		{
			try 
			{
				// just truncate the table so that all records are dropped		
				string query = "delete from err_log_d";
				bool ret = NonSelectCommand(query);
				if (!ret)
				{
					logger.Exception(10,"SQL error");
					return (false);
				}						
				return true;	
			}
			catch (Exception e)
			{
				logger.Exception(100,e.Message);
				return false;
			}
		}

		#region ReportGen
		public bool FetchAllRooms(ref Queue roomList)
		{
			DataSet ds = new DataSet();
			string dsTable = "Rooms";
			string query = "select r.roomid,r.[name],r.l2locationid,r.l3locationid,rep.timezoneid,rep.filepath, t.[name] ";
			query += " from loc_room_d r, custom_hklaw_roomreport_d rep , loc_tier3_d t";
			query += " where r.disabled =0 and r.roomid = rep.roomid and t.id = r.l3locationid";
			bool ret = SelectCommand(query,ref ds,dsTable);
			if (!ret)
			{
				logger.Trace("SQL error");
				return (false);
			}		
			
			for (int i = 0;i < ds.Tables[dsTable].Rows.Count; i++)
			{
				// room object
				NS_MESSENGER_CUSTOMIZATIONS.Room room = new NS_MESSENGER_CUSTOMIZATIONS.Room();

				// room data
				room.roomID = Int32.Parse(ds.Tables[dsTable].Rows[i][0].ToString());
				room.name = ds.Tables[dsTable].Rows[i][1].ToString();
				room.tier2ID = Int32.Parse(ds.Tables[dsTable].Rows[i][2].ToString());
				room.tier1ID = Int32.Parse(ds.Tables[dsTable].Rows[i][3].ToString());
				room.timezoneID = Int32.Parse(ds.Tables[dsTable].Rows[i][4].ToString());
				room.filePath = ds.Tables[dsTable].Rows[i][5].ToString();
				room.topTierName = ds.Tables[dsTable].Rows[i][6].ToString();

				//timezone name
				DataSet ds1 = new DataSet();
				string dsTable1 = "TZ";
				string query1 = "SELECT timezone from gen_timezone_s where timezoneid = " + room.timezoneID.ToString();
				bool ret1 = SelectCommand(query1,ref ds1,dsTable1);
				if (!ret1)
				{
					room.timezoneID_Name = "Eastern Time";
				}		
				else
				{
					room.timezoneID_Name = ds1.Tables[dsTable1].Rows[0]["timezone"].ToString();
				}
			
				// save room in list
				roomList.Enqueue(room);
			}
			return true;
		}
	
		public bool FetchDailyConfSchedulePerRoom(ref NS_MESSENGER_CUSTOMIZATIONS.Room room)
		{
			DataSet ds = new DataSet();
			string dsTable = "Room";
			string query = " declare @fromDate datetime, @toDate datetime, @roomid int , @tz int";
			query += " set @toDate = '" +  DateTime.Now.ToString("d") + "'";
			query += " set @roomid =  " + room.roomID.ToString();
			query += " set @tz = " + room.timezoneID.ToString();
			query += " SELECT DISTINCT conf.confid, conf.instanceid, dbo.changeTime(@tz,conf.confdate) AS Start, dateadd(minute,conf.duration, dbo.changeTime(@tz,conf.confdate)) AS [End],	conf.confnumname,conf.externalname, conf.[description] FROM conf_conference_d conf, loc_room_d cr INNER JOIN conf_room_d c ON c.roomid = cr.roomid WHERE (conf.confid = c.confid AND conf.instanceid = c.instanceid)AND (CAST(FLOOR(CAST(dbo.changeTime(@tz,conf.confdate) AS float)) AS datetime) =  @toDate) AND cr.roomid = @roomid AND conf.deleted = 0 ORDER BY start,externalname";

			bool ret = SelectCommand(query,ref ds,dsTable);
			if (!ret)
			{
				logger.Trace("SQL error");
				return (false);
			}		
			
			for (int i = 0;i < ds.Tables[dsTable].Rows.Count; i++)
			{
				// conf object
				NS_MESSENGER_CUSTOMIZATIONS.Conference conf = new NS_MESSENGER_CUSTOMIZATIONS.Conference();

				// conf data
				conf.confID = Int32.Parse(ds.Tables[dsTable].Rows[i][0].ToString());
				conf.instanceID = Int32.Parse(ds.Tables[dsTable].Rows[i][1].ToString());
				conf.startTime = DateTime.Parse(ds.Tables[dsTable].Rows[i][2].ToString());
				conf.endTime = DateTime.Parse(ds.Tables[dsTable].Rows[i][3].ToString());
				conf.uniqueID = Int32.Parse(ds.Tables[dsTable].Rows[i][4].ToString());
				conf.name = ds.Tables[dsTable].Rows[i][5].ToString();
				conf.description = ds.Tables[dsTable].Rows[i][6].ToString();
				
				// fetch the participants 
				Participants(ref conf);

				// fetch the locations
				Locations(ref conf);

				// fetch the catering orders
				//CateringOrders(ref conf);

				// fetch the resource orders
				//ResourceOrders(ref conf);

				
				logger.Trace ("Conf: " + conf.name);

				// add conf to list
				room.confList.Enqueue(conf);

				// add delays
				Thread.Sleep (100);
			}
			return true;
		}

		public bool WeeklyAllConfs(ref Queue confList)
		{
			DataSet ds = new DataSet();
			string dsTable = "AllTiers";
			string query = " declare @fromDate datetime, @toDate datetime, @roomid int , @tz int";
			query += " set @fromDate = '" + DateTime.Now.Date.ToString() + "'";
			query += " set @toDate = '" + DateTime.Now.AddDays(7).Date.ToString() + "'";
			query += " set @tz = 26";
			query += " SELECT DISTINCT conf.confid, conf.instanceid, dbo.changeTime(@tz,conf.confdate) AS Start, dateadd(minute,conf.duration, dbo.changeTime(@tz,conf.confdate)) AS [End],	conf.confnumname,conf.externalname , conf.[description]  FROM  conf_conference_d conf WHERE (CAST(FLOOR(CAST(dbo.changeTime(@tz,conf.confdate) AS float)) AS datetime) BETWEEN @fromDate AND @toDate) AND conf.deleted = 0 ORDER BY Start,ExternalName";
			
			bool ret = SelectCommand(query,ref ds,dsTable);
			if (!ret)
			{
				logger.Trace("SQL error");
				return (false);
			}		
			
			for (int i = 0;i < ds.Tables[dsTable].Rows.Count; i++)
			{
				// conf object
				NS_MESSENGER_CUSTOMIZATIONS.Conference conf = new NS_MESSENGER_CUSTOMIZATIONS.Conference();

				// conf data
				conf.confID = Int32.Parse(ds.Tables[dsTable].Rows[i][0].ToString());
				conf.instanceID = Int32.Parse(ds.Tables[dsTable].Rows[i][1].ToString());
				conf.startTime = DateTime.Parse(ds.Tables[dsTable].Rows[i][2].ToString());
				conf.endTime = DateTime.Parse(ds.Tables[dsTable].Rows[i][3].ToString());
				conf.uniqueID = Int32.Parse(ds.Tables[dsTable].Rows[i][4].ToString());
				conf.name = ds.Tables[dsTable].Rows[i][5].ToString();
				conf.description = ds.Tables[dsTable].Rows[i][6].ToString();
				
				// fetch the participants 
				Participants(ref conf);

				// fetch the locations
				Locations(ref conf);

				// fetch the catering orders
				//CateringOrders(ref conf);

				// fetch the resource orders
				//ResourceOrders(ref conf);

				// add conf to list
				confList.Enqueue(conf);
				
				// add delays
				Thread.Sleep (100);
			}
			return true;
		}
		
		
		public bool DailyAllConfs(ref Queue confList)
		{
			DataSet ds = new DataSet();
			string dsTable = "AllTiers";
			string query = " declare @fromDate datetime, @toDate datetime, @roomid int , @tz int";
			query += " set @fromDate = '" + DateTime.Now.Date.ToString() + " 00:00:00'";
			query += " set @toDate = '" + DateTime.Now.Date.ToString() + " 23:59:00'";
			query += " set @tz = 26";
			query += " SELECT DISTINCT conf.confid, conf.instanceid, dbo.changeTime(@tz,conf.confdate) AS Start, dateadd(minute,conf.duration, dbo.changeTime(@tz,conf.confdate)) AS [End],	conf.confnumname,conf.externalname , conf.[description]  FROM  conf_conference_d conf WHERE (CAST(FLOOR(CAST(dbo.changeTime(@tz,conf.confdate) AS float)) AS datetime) BETWEEN @fromDate AND @toDate) AND conf.deleted = 0 ORDER BY Start,ExternalName";
			
			bool ret = SelectCommand(query,ref ds,dsTable);
			if (!ret)
			{
				logger.Trace("SQL error");
				return (false);
			}		
			
			for (int i = 0;i < ds.Tables[dsTable].Rows.Count; i++)
			{
				// conf object
				NS_MESSENGER_CUSTOMIZATIONS.Conference conf = new NS_MESSENGER_CUSTOMIZATIONS.Conference();

				// conf data
				conf.confID = Int32.Parse(ds.Tables[dsTable].Rows[i][0].ToString());
				conf.instanceID = Int32.Parse(ds.Tables[dsTable].Rows[i][1].ToString());
				conf.startTime = DateTime.Parse(ds.Tables[dsTable].Rows[i][2].ToString());
				conf.endTime = DateTime.Parse(ds.Tables[dsTable].Rows[i][3].ToString());
				conf.uniqueID = Int32.Parse(ds.Tables[dsTable].Rows[i][4].ToString());
				conf.name = ds.Tables[dsTable].Rows[i][5].ToString();
				conf.description = ds.Tables[dsTable].Rows[i][6].ToString();
				
				// fetch the participants 
				Participants(ref conf);

				// fetch the locations
				Locations(ref conf);

				// fetch the catering orders
				//CateringOrders(ref conf);

				// fetch the resource orders
				//ResourceOrders(ref conf);

				// add conf to list
				confList.Enqueue(conf);
				
				// add delays
				Thread.Sleep (100);
			}
			return true;
		}
		
		public bool WeeklyConfsByTier(int tier2,int tzId, ref Queue confList)
		{
			DataSet ds = new DataSet();
			string dsTable = "Tier2";
			string query = " declare @fromDate datetime, @toDate datetime, @roomid int ,@tz int, @tier2 int";
			query += " set @fromDate = '" + DateTime.Now.ToString("d") +"'";   
			query += " set @toDate = '" +  DateTime.Now.AddDays(7).ToString("d") + "'";     
			query += " set @tz = " + tzId.ToString();
			query += " set @tier2 = " + tier2.ToString();
			query += " SELECT DISTINCT c.confid, c.instanceid, dbo.changeTime(@tz,conf.confdate) AS Start, dateadd(minute,conf.duration, dbo.changeTime(@tz,conf.confdate)) AS [End], conf.confnumname,conf.externalname, conf.[description]  FROM  loc_tier3_d l2, Conf_conference_d conf, Loc_room_d cr INNER JOIN conf_room_d c ON c.roomid = cr.roomid  WHERE l3locationid = l2.[id] AND conf.deleted =0 AND (conf.confid = c.confid AND conf.instanceid = c.instanceid) AND (CAST(FLOOR(CAST(dbo.changeTime(@tz,conf.confdate) AS float)) AS datetime) BETWEEN @fromDate AND @toDate) AND l3locationid = @tier2 ORDER BY start,externalname";
			
			bool ret = SelectCommand(query,ref ds,dsTable);
			if (!ret)
			{
				logger.Trace("SQL error");
				return (false);
			}		
			
			for (int i = 0;i < ds.Tables[dsTable].Rows.Count; i++)
			{
				// conf object
				NS_MESSENGER_CUSTOMIZATIONS.Conference conf = new NS_MESSENGER_CUSTOMIZATIONS.Conference();

				// conf data
				conf.confID = Int32.Parse(ds.Tables[dsTable].Rows[i][0].ToString());
				conf.instanceID = Int32.Parse(ds.Tables[dsTable].Rows[i][1].ToString());
				conf.startTime = DateTime.Parse(ds.Tables[dsTable].Rows[i][2].ToString());
				conf.startDate = conf.startTime;
				conf.endTime = DateTime.Parse(ds.Tables[dsTable].Rows[i][3].ToString());
				conf.uniqueID = Int32.Parse(ds.Tables[dsTable].Rows[i][4].ToString());
				conf.name = ds.Tables[dsTable].Rows[i][5].ToString();
				conf.description = ds.Tables[dsTable].Rows[i][6].ToString();
				
				logger.Trace ("Conf Name: " + conf.name);

				// fetch the participants 
				Participants(ref conf);				

				// fetch the locations
				Locations(ref conf);


				// fetch the catering orders
				//CateringOrders(ref conf);

				// fetch the resource orders
				//ResourceOrders(ref conf);

				// add conf to list
				confList.Enqueue(conf);

				// add delays
				Thread.Sleep (100);
			}
			return true;
		}
		
		
		public bool DailyConfsByTier(int tier2,int tzId, ref Queue confList)
		{
			DataSet ds = new DataSet();
			string dsTable = "Tier2";
			string query = " declare @fromDate datetime, @toDate datetime, @roomid int ,@tz int, @tier2 int";
			//DateTime startOfToday = new DateTime(DateTime.Now.Year,DateTime.Now.Month,DateTime.Now.Day,1,1,0);
			query += " set @fromDate = '" + DateTime.Now.ToString("d") +" 00:00:00'";   
			//DateTime endOfToday = new DateTime(DateTime.Now.Year,DateTime.Now.Month,DateTime.Now.Day,23,59,00);
			query += " set @toDate = '" +  DateTime.Now.ToString("d") + " 23:59:00'";     
			query += " set @tz = " + tzId.ToString();
			query += " set @tier2 = " + tier2.ToString();
			query += " SELECT DISTINCT c.confid, c.instanceid, dbo.changeTime(@tz,conf.confdate) AS Start, dateadd(minute,conf.duration, dbo.changeTime(@tz,conf.confdate)) AS [End], conf.confnumname,conf.externalname, conf.[description]  FROM  loc_tier3_d l2, Conf_conference_d conf, Loc_room_d cr INNER JOIN conf_room_d c ON c.roomid = cr.roomid  WHERE l3locationid = l2.[id] AND conf.deleted =0 AND (conf.confid = c.confid AND conf.instanceid = c.instanceid) AND (CAST(FLOOR(CAST(dbo.changeTime(@tz,conf.confdate) AS float)) AS datetime) BETWEEN @fromDate AND @toDate) AND l3locationid = @tier2 ORDER BY start,externalname";
			
			bool ret = SelectCommand(query,ref ds,dsTable);
			if (!ret)
			{
				logger.Trace("SQL error");
				return (false);
			}		
			
			for (int i = 0;i < ds.Tables[dsTable].Rows.Count; i++)
			{
				// conf object
				NS_MESSENGER_CUSTOMIZATIONS.Conference conf = new NS_MESSENGER_CUSTOMIZATIONS.Conference();

				// conf data
				conf.confID = Int32.Parse(ds.Tables[dsTable].Rows[i][0].ToString());
				conf.instanceID = Int32.Parse(ds.Tables[dsTable].Rows[i][1].ToString());
				conf.startTime = DateTime.Parse(ds.Tables[dsTable].Rows[i][2].ToString());
				conf.startDate = conf.startTime;
				conf.endTime = DateTime.Parse(ds.Tables[dsTable].Rows[i][3].ToString());
				conf.uniqueID = Int32.Parse(ds.Tables[dsTable].Rows[i][4].ToString());
				conf.name = ds.Tables[dsTable].Rows[i][5].ToString();
				conf.description = ds.Tables[dsTable].Rows[i][6].ToString();
				
				logger.Trace ("Conf Name: " + conf.name);

				// fetch the participants 
				Participants(ref conf);

				// fetch the locations
				Locations(ref conf);

				// fetch the catering orders
				//CateringOrders(ref conf);

				// fetch the resource orders
				//ResourceOrders(ref conf);

				// add conf to list
				confList.Enqueue(conf);

				// add delays
				Thread.Sleep (100);
			}
			return true;
		}
		
		public bool TwoWeekConfsByTier(int tier2,int tzId, ref Queue confList)
		{
			DataSet ds = new DataSet();
			string dsTable = "Tier2";
			string query = " declare @fromDate datetime, @toDate datetime, @roomid int ,@tz int, @tier2 int";
			query += " set @fromDate = '" + DateTime.Now.ToString("d") +"'";   
			query += " set @toDate = '" +  DateTime.Now.AddDays(14).ToString("d") + "'";     
			query += " set @tz = " + tzId.ToString();
			query += " set @tier2 = " + tier2.ToString();
			query += " SELECT DISTINCT c.confid, c.instanceid, dbo.changeTime(@tz,conf.confdate) AS Start, dateadd(minute,conf.duration, dbo.changeTime(@tz,conf.confdate)) AS [End], conf.confnumname,conf.externalname, conf.[description]  FROM  loc_tier3_d l2, Conf_conference_d conf, Loc_room_d cr INNER JOIN conf_room_d c ON c.roomid = cr.roomid  WHERE l3locationid = l2.[id] AND conf.deleted =0 AND (conf.confid = c.confid AND conf.instanceid = c.instanceid) AND (CAST(FLOOR(CAST(dbo.changeTime(@tz,conf.confdate) AS float)) AS datetime) BETWEEN @fromDate AND @toDate) AND l3locationid = @tier2 ORDER BY start,externalname";
			
			bool ret = SelectCommand(query,ref ds,dsTable);
			if (!ret)
			{
				logger.Trace("SQL error");
				return (false);
			}		
			
			for (int i = 0;i < ds.Tables[dsTable].Rows.Count; i++)
			{
				// conf object
				NS_MESSENGER_CUSTOMIZATIONS.Conference conf = new NS_MESSENGER_CUSTOMIZATIONS.Conference();

				// conf data
				conf.confID = Int32.Parse(ds.Tables[dsTable].Rows[i][0].ToString());
				conf.instanceID = Int32.Parse(ds.Tables[dsTable].Rows[i][1].ToString());
				conf.startTime = DateTime.Parse(ds.Tables[dsTable].Rows[i][2].ToString());
				conf.startDate = conf.startTime;
				conf.endTime = DateTime.Parse(ds.Tables[dsTable].Rows[i][3].ToString());
				conf.uniqueID = Int32.Parse(ds.Tables[dsTable].Rows[i][4].ToString());
				conf.name = ds.Tables[dsTable].Rows[i][5].ToString();
				conf.description = ds.Tables[dsTable].Rows[i][6].ToString();
				
				logger.Trace ("Conf Name: " + conf.name);

				// fetch the participants 
				Participants(ref conf);

				// fetch the locations
				Locations(ref conf);

				// fetch the catering orders
				//CateringOrders(ref conf);

				// fetch the resource orders
				//ResourceOrders(ref conf);

				// add conf to list
				confList.Enqueue(conf);

				// add delays
				Thread.Sleep (100);
			}
			return true;
		}
		public bool Participants(ref NS_MESSENGER_CUSTOMIZATIONS.Conference conf)
		{
			DataSet ds = new DataSet();
			string dsTable = "Participants";
			string query = "DECLARE @confid int, @instanceid int";
			query += " SET @confid = " + conf.confID.ToString();
			query += " SET @instanceid = " + conf.instanceID.ToString();
			//query += " SELECT DISTINCT firstname, lastname FROM usr_list_d U INNER JOIN Conf_User_D C ON C.userid = u.userid WHERE confid = @confid and c.instanceid = @instanceid	UNION (SELECT DISTINCT firstname, lastname FROM Usr_GuestList_D G)";
			query += " SELECT DISTINCT firstname, lastname FROM usr_list_d U INNER JOIN Conf_User_D C ON C.userid = u.userid WHERE confid = @confid and c.instanceid = @instanceid	UNION (SELECT DISTINCT firstname, lastname FROM Usr_GuestList_D G INNER JOIN Conf_User_D C ON C.userid = G.userid WHERE confid = @confid and c.instanceid = @instanceid)";
			bool ret = SelectCommand(query,ref ds,dsTable);
			if (!ret)
			{
				logger.Trace("SQL error");
				return (false);
			}		
			
			for (int i = 0;i < ds.Tables[dsTable].Rows.Count; i++)
			{
				string firstName = ds.Tables[dsTable].Rows[i][0].ToString();
				string lastName = ds.Tables[dsTable].Rows[i][1].ToString();
				conf.participantList.Enqueue(firstName + " " + lastName);		
			}

			return true;
		}

		public bool Locations(ref NS_MESSENGER_CUSTOMIZATIONS.Conference conf)
		{
			DataSet ds = new DataSet();
			string dsTable = "Locations";
			string query = "DECLARE @confid int, @instanceid int";
			query += " SET @confid = " + conf.confID.ToString();
			query += " SET @instanceid = " + conf.instanceID.ToString();
			query += " SELECT DISTINCT CR.[NAME] AS Location FROM loc_Room_D CR 	INNER JOIN Conf_Room_D C ON C.RoomID = CR.RoomID 	WHERE C.confid = @confid and c.instanceid = @instanceid order by location asc";
			bool ret = SelectCommand(query,ref ds,dsTable);
			if (!ret)
			{
				logger.Trace("SQL error");
				return (false);
			}		
			
			for (int i = 0;i < ds.Tables[dsTable].Rows.Count; i++)
			{
				try 
				{
					if (ds.Tables[dsTable].Rows[i][0].ToString().Trim() != null)
					{
						string location = ds.Tables[dsTable].Rows[i][0].ToString().Trim();
						if (location.Length > 1)
							conf.locationList.Enqueue(location);		
					}
				}
				catch (Exception e)
				{
					logger.Trace(e.Message);
				}
			}

			return true;
		}

		public bool CateringOrders(ref NS_MESSENGER_CUSTOMIZATIONS.Conference conf)
		{

			//remove when query is fixed
			//return true;

			DataSet ds = new DataSet();
			string dsTable = "Catering";
			string query = "DECLARE @confid int, @instanceid int";
			query += " SET @confid = " + conf.confID.ToString();
			query += " SET @instanceid = " + conf.instanceID.ToString();
			//query += " SELECT F.[NAME] AS CATERING FROM Cat_FoodItem_D F LEFT OUTER JOIN Conf_FoodOrder_D O ON O.orderID = F.[id] AND(O.confid = @confid and O.instanceid = @instanceid)";
			query += " SELECT F.[NAME] AS CATERING FROM Conf_FoodOrder_D F 	WHERE (F.confid = @confid and F.instanceid = @instanceid)";
			bool ret = SelectCommand(query,ref ds,dsTable);
			if (!ret)
			{
				logger.Trace("SQL error");
				return (false);
			}		
			
			for (int i = 0;i < ds.Tables[dsTable].Rows.Count; i++)
			{
				try
				{
					if ( ds.Tables[dsTable].Rows[i][0].ToString().Trim() != null)
					{
						string cateringOrder = ds.Tables[dsTable].Rows[i][0].ToString().Trim();
						logger.Trace ("Catering order:" + cateringOrder);
						if (cateringOrder.Length > 1)
							conf.cateringOrders.Enqueue(cateringOrder);		
					}
				}
				catch (Exception e)
				{
					logger.Trace (e.Message);
				}
			}

			return true;
		}

		public bool ResourceOrders(ref NS_MESSENGER_CUSTOMIZATIONS.Conference conf)
		{
			DataSet ds = new DataSet();
			string dsTable = "Resource";
			string query = "DECLARE @confid int, @instanceid int";
			query += " SET @confid = " + conf.confID.ToString();
			query += " SET @instanceid = " + conf.instanceID.ToString();
			query += " SELECT [NAME] AS RESOURCE FROM Conf_ResourceOrder_D	WHERE confid = @confid and instanceid = @instanceid";
			bool ret = SelectCommand(query,ref ds,dsTable);
			if (!ret)
			{
				logger.Trace("SQL error");
				return (false);
			}		
			
			for (int i = 0;i < ds.Tables[dsTable].Rows.Count; i++)
			{
				try 
				{
					if (ds.Tables[dsTable].Rows[i][0].ToString().Trim() != null)
					{
						string resourceOrder = ds.Tables[dsTable].Rows[i][0].ToString().Trim();
						if (resourceOrder.Length > 1)
							conf.resourceOrders.Enqueue(resourceOrder);		
					}
				}
				catch (Exception e)
				{
					logger.Trace(e.Message);
				}
			}
			return true;
		}

		public bool FetchTier2List(ref Queue tier2List)
		{
			DataSet ds = new DataSet();
			string dsTable = "Tier-2";
			string query = "SELECT l.[id],l.[name],t.timezoneid,t.filepath from loc_tier3_d l,custom_hklaw_tier2report_d t where l.disabled = 0 and l.[id] = t.[tier2id]";
			bool ret = SelectCommand(query,ref ds,dsTable);
			if (!ret)
			{
				logger.Trace("SQL error");
				return (false);
			}		
			
			for (int i = 0;i < ds.Tables[dsTable].Rows.Count; i++)
			{
				NS_MESSENGER_CUSTOMIZATIONS.Tier2 tier2 = new NS_MESSENGER_CUSTOMIZATIONS.Tier2();
				tier2.id = Int32.Parse(ds.Tables[dsTable].Rows[i][0].ToString());
				tier2.name = ds.Tables[dsTable].Rows[i][1].ToString();
				tier2.timezoneId = Int32.Parse(ds.Tables[dsTable].Rows[i][2].ToString());
				tier2.filePath = ds.Tables[dsTable].Rows[i][3].ToString();

				DataSet ds1 = new DataSet();
				string dsTable1 = "TZ";
				string query1 = "SELECT timezone from gen_timezone_s where timezoneid = " + tier2.timezoneId.ToString();
				bool ret1 = SelectCommand(query1,ref ds1,dsTable1);
				if (!ret1)
				{
					tier2.timezoneId_Name = "Eastern Time";
				}		
				else
				{
					tier2.timezoneId_Name = ds1.Tables[dsTable1].Rows[0]["timezone"].ToString();
				}
			
				tier2List.Enqueue(tier2);
			}

			return true;
		}

		#endregion
		public bool FetchAllUserEmails(ref Queue emailq)
		{
			DataSet ds = new DataSet();
			string dsTable = "Emails";
			string query = "select email from usr_list_d ";
			bool ret = SelectCommand(query,ref ds,dsTable);
			if (!ret)
			{
				logger.Trace("SQL error");
				return (false);
			}		
			
			for (int i = 0;i < ds.Tables[dsTable].Rows.Count; i++)
			{
				// user object
				NS_MESSENGER.LdapUser user = new NS_MESSENGER.LdapUser();

				// user email
				user.email = ds.Tables[dsTable].Rows[i][0].ToString().Trim();

				// save user in list
				emailq.Enqueue(user);
			}
			
			ds.Clear();
			query = "select email from usr_guestlist_d where deleted = 0";
			ret = SelectCommand(query,ref ds,dsTable);
			if (!ret)
			{
				logger.Trace("SQL error");
				return (false);
			}		
			
			for (int i = 0;i < ds.Tables[dsTable].Rows.Count; i++)
			{
				// room object
				NS_MESSENGER.LdapUser user = new NS_MESSENGER.LdapUser();

				// user email
				user.email = ds.Tables[dsTable].Rows[i][0].ToString().Trim();

				// save user in list
				emailq.Enqueue(user);
			}
			
			ds.Clear();
			query = "select email from usr_inactive_d";
			ret = SelectCommand(query,ref ds,dsTable);
			if (!ret)
			{
				logger.Trace("SQL error");
				return (false);
			}		
			
			for (int i = 0;i < ds.Tables[dsTable].Rows.Count; i++)
			{
				// room object
				NS_MESSENGER.LdapUser user = new NS_MESSENGER.LdapUser();

				// user email
				user.email = ds.Tables[dsTable].Rows[i][0].ToString().Trim();

				// save user in list
				emailq.Enqueue(user);
			}

			ds.Clear();
            query = "select email from ldap_blankuser_d";
			ret = SelectCommand(query,ref ds,dsTable);
			if (!ret)
			{
				logger.Trace("SQL error");
				return (false);
			}					
			for (int i = 0;i < ds.Tables[dsTable].Rows.Count; i++)
			{
				// room object
				NS_MESSENGER.LdapUser user = new NS_MESSENGER.LdapUser();

				// user email
				user.email = ds.Tables[dsTable].Rows[i][0].ToString().Trim();

				// save user in list
				emailq.Enqueue(user);
			}

			return true;
		}
		
		public bool FetchAllUserLogins(ref Queue qUserLoginList)
		{
			DataSet ds = new DataSet();
			string dsTable = "Emails";
			string query = "select login from usr_list_d ";
			bool ret = SelectCommand(query,ref ds,dsTable);
			if (!ret)
			{
				logger.Trace("SQL error");
				return (false);
			}		
			
			for (int i = 0;i < ds.Tables[dsTable].Rows.Count; i++)
			{
				// user object
				NS_MESSENGER.LdapUser user = new NS_MESSENGER.LdapUser();

				// user login
				user.login = ds.Tables[dsTable].Rows[i][0].ToString().Trim();

				// save user in list
				qUserLoginList.Enqueue(user);
			}
			
			ds.Clear();
			query = "select login from usr_guestlist_d where deleted =0";
			ret = SelectCommand(query,ref ds,dsTable);
			if (!ret)
			{
				logger.Trace("SQL error");
				return (false);
			}		
			
			for (int i = 0;i < ds.Tables[dsTable].Rows.Count; i++)
			{
				// user object
				NS_MESSENGER.LdapUser user = new NS_MESSENGER.LdapUser();

				// user login
				user.login = ds.Tables[dsTable].Rows[i][0].ToString().Trim();

				// save user in list
				qUserLoginList.Enqueue(user);
			}
			
			ds.Clear();
			query = "select login from usr_inactive_d";
			ret = SelectCommand(query,ref ds,dsTable);
			if (!ret)
			{
				logger.Trace("SQL error");
				return (false);
			}		
			
			for (int i = 0;i < ds.Tables[dsTable].Rows.Count; i++)
			{
				// user object
				NS_MESSENGER.LdapUser user = new NS_MESSENGER.LdapUser();

				// user email
				user.login = ds.Tables[dsTable].Rows[i][0].ToString().Trim();

				// save user in list
				qUserLoginList.Enqueue(user);
			}

			ds.Clear();
			query = "select login from ldap_blankuser_d";
			ret = SelectCommand(query,ref ds,dsTable);
			if (!ret)
			{
				logger.Trace("SQL error");
				return (false);
			}					
			for (int i = 0;i < ds.Tables[dsTable].Rows.Count; i++)
			{
				// user object
				NS_MESSENGER.LdapUser user = new NS_MESSENGER.LdapUser();

				// user email
				user.login = ds.Tables[dsTable].Rows[i][0].ToString().Trim();

				// save user in list
				qUserLoginList.Enqueue(user);
			}

			return true;
		}
		// maneesh pujari 07/29/2008 FB 594.
        public bool DoesUserExists(string strLogin, string strEmailId)
        {
            DataSet ds = new DataSet();
            string dsTable = "Emails";

            try
            {
                // check the active users list
                string query = "select * from usr_list_d where Email='" + strEmailId + "' or Login='" + strLogin + "'";
                bool ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Trace("SQL error");
                    return (false);
                }
                // If any user found with given email id or login then return true.
                if (ds.Tables[dsTable].Rows.Count > 0)
                {
                    return true;
                }
                else
                {
                    // Now check if user exists in the ldap_blankuser_d table
                    DataSet ds1 = new DataSet();
                    string dsTable1 = "Emails";
                    string query1 = "select * from ldap_blankuser_d where Email='" + strEmailId + "' or Login='" + strLogin + "'";
                    bool ret1 = SelectCommand(query1, ref ds1, dsTable1);
                    if (!ret1)
                    {
                        logger.Trace("SQL error");
                        return (false);
                    }

                    if (ds1.Tables[dsTable1].Rows.Count > 0)
                    {
                        return true;
                    }
                    else
                    {

                        // check the inactive user list 
                        DataSet ds2 = new DataSet();
                        string dsTable2 = "Emails";
                        string query2 = "select * from usr_inactive_d where Email='" + strEmailId + "' or Login='" + strLogin + "'";
                        bool ret2 = SelectCommand(query2, ref ds2, dsTable2);
                        if (!ret2)
                        {
                            logger.Trace("SQL error");
                            return (false);
                        }
                        if (ds2.Tables[dsTable2].Rows.Count > 0)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool InsertLdapUser(NS_MESSENGER.LdapUser ldapUser)
		{
			string query = "declare @userid int "; 
			query += " if (select max (userid) from ldap_blankuser_d) is NULL";
			query += " BEGIN";
			query += "	set @userid = 0	";
			query += " 	END";
			query += " else";
			query += " BEGIN";
			query += " set @userid = (select max (userid) from ldap_blankuser_d)";
			query += " END";
			query += " insert into ldap_blankuser_d  values (@userid+1,'" + ldapUser.firstName + "','"+ ldapUser.lastName + "','" + ldapUser.email + "','" + ldapUser.telephone + "',getutcdate(),1,'" + ldapUser.login +"')";
			bool ret = NonSelectCommand (query);
			if (!ret)
			{
				logger.Exception(10,"Error executing query.");
				return false;
			}
			return true;
		}

		public bool TerminateConference(int confID,int instanceID)
		{
			string query = "update conf_conference_d set deleted = 2 , status = 3 where confid = " + confID.ToString() + " and instanceid = " + instanceID.ToString(); 
			bool ret = NonSelectCommand (query);
			if (!ret)
			{
				logger.Exception(10,"Error executing query.");
				return false;
			}
			return true;
		}

		internal bool FetchPendingWorkOrders(int iPendingTimeInMinutes, ref Queue qWorkOrderList)
		{
			DataSet ds = new DataSet();
			string dsTable = "PendingWorkOrderList";
			string query = "declare @hours int";
			query += " set @hours = " + iPendingTimeInMinutes.ToString();
			query += " SELECT Type,CatID,AdminID,ConfID,InstanceID,Name,RoomID,CompletedBy,Comment";
			query += " FROM inv_workorder_d";
			query += " where DateDiff(n, DateAdd(hour,@hours,GetDate()), dbo.userPreferedTime((SELECT timezone FROM sys_Settings_D),CompletedBy)) <= 0 AND status = 0";
			
			bool ret = SelectCommand(query,ref ds,dsTable);
			if (!ret)
			{
				logger.Trace("SQL error");
				return (false);
			}		
			
			for (int i = 0;i < ds.Tables[dsTable].Rows.Count; i++)
			{
				try 
				{
					// work order object
					NS_MESSENGER.WorkOrder workOrder = new NS_MESSENGER.WorkOrder();

					// type
					int type = Int32.Parse(ds.Tables[dsTable].Rows[i]["Type"].ToString().Trim());
			
					// cat id
					workOrder.iCategoryType = Int32.Parse(ds.Tables[dsTable].Rows[i]["CatID"].ToString().Trim());
					// admin user ID
					workOrder.iAdminUserID = Int32.Parse(ds.Tables[dsTable].Rows[i]["AdminID"].ToString().Trim());
					// confid
					workOrder.iConfID = Int32.Parse(ds.Tables[dsTable].Rows[i]["ConfID"].ToString().Trim());
					// instanceid
					workOrder.iInstanceID = Int32.Parse(ds.Tables[dsTable].Rows[i]["InstanceID"].ToString().Trim());
					// name
					workOrder.sName =ds.Tables[dsTable].Rows[i]["Name"].ToString().Trim();
					// roomid
					workOrder.iRoomID = Int32.Parse(ds.Tables[dsTable].Rows[i]["RoomID"].ToString().Trim());
					// completed by
					workOrder.dtCompletedBy = DateTime.Parse(ds.Tables[dsTable].Rows[i]["CompletedBy"].ToString().Trim());
					// comments
					workOrder.sComments = ds.Tables[dsTable].Rows[i]["Comments"].ToString().Trim();
				
				}
				catch (Exception e)
				{
					logger.Trace (e.Message);
					continue;
				}
			}
			return true;
		}

		private bool ConvertToWorkOrderType(int iWorkOrderType,ref NS_MESSENGER.WorkOrder workOrder)
		{
			switch (iWorkOrderType)
			{
				case 0: 
				{
					workOrder.etType  = NS_MESSENGER.WorkOrder.eType.AUDIO_VISUAL;
					break;
				}
				case 1: 
				{
					workOrder.etType  = NS_MESSENGER.WorkOrder.eType.CATERING;
					break;
				}
				case 2: 
				{
					workOrder.etType  = NS_MESSENGER.WorkOrder.eType.HOUSEKEEPING;
					break;
				}
				default: 
				{
					workOrder.etType  = NS_MESSENGER.WorkOrder.eType.GENERIC;
					break;
				}
			}

			return true;
		}

        internal bool FetchConferenceList_WithNoWorkOrdersAndNoReminderAlert(ref Queue confList)
        {
            try
            {
                // fetch conferences which meet the following conditions - 
                // ---- start date is exactly in 2 weeks and within the exact hour.
                // ---- active conference
                // ---- no work orders are associated with it.
                DataSet ds = new DataSet();
                string dsTable = "ConfList";
                string query = " declare @hours int ";
                query += " set @hours = 2*7*24"; //2 * 7 days * 24 hrs = 2-weeks in hours
                query += " select C.confid,C.instanceid,C.confnumname,C.ExternalName, U.email  from conf_conference_d c , usr_list_d u where"; 
                query += " (C.status != 7 AND C.status != 9)";
                query += " AND datediff(hh,getutcdate(),confdate) = @hours ";                
                query += " AND U.userid = C.userid ";
                query += " AND C.confid NOT IN (SELECT confid from Inv_WorkOrder_D WHERE confid = C.confid and instanceid = C.instanceid AND deleted = 0)";

                bool ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Trace("SQL error");
                    return (false);
                }

                for (int i = 0; i < ds.Tables[dsTable].Rows.Count; i++)
                {
                    try
                    {
                        // conference
                        NS_MESSENGER.Conference conf = new NS_MESSENGER.Conference();

                        // id
                        conf.iDbID = Int32.Parse(ds.Tables[dsTable].Rows[i]["ConfID"].ToString().Trim());

                        // instance id
                        conf.iInstanceID = Int32.Parse(ds.Tables[dsTable].Rows[i]["InstanceID"].ToString().Trim());

                        // name
                        conf.sExternalName= ds.Tables[dsTable].Rows[i]["ExternalName"].ToString().Trim();
                        
                        // unique id
                        conf.iDbNumName = Int32.Parse(ds.Tables[dsTable].Rows[i]["ConfNumName"].ToString().Trim());
                        
                        // owner email
                        conf.sHostEmail = ds.Tables[dsTable].Rows[i]["Email"].ToString().Trim();

                        // check if the reminder alert has already been sent or not for this conference
                        DataSet ds1 = new DataSet();
                        string dsTable1 = "ConfAlert";
                        string query1 = " select * from conf_alerts_d ";
                        query1 += " where confid = '" + conf.iDbID.ToString() + "'";
                        query1 += " and instanceid= '" + conf.iInstanceID.ToString() + "'";
                        query1 += " and alerttypeid = 10";
                        bool ret1 = SelectCommand(query1, ref ds1, dsTable1);
                        if (!ret1)
                        {
                            logger.Trace("SQL error");
                            return (false);
                        }

                        if (ds1.Tables[dsTable1].Rows.Count == 0)
                        {
                            // if alert not found, then add the conference to the list
                            confList.Enqueue(conf);
                        }
                    }
                    catch (Exception e)
                    {
                        logger.Trace(e.Message);
                        continue;
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        
        internal bool FetchConferenceList(int excludeConfNumName, DateTime fromDate, DateTime toDate,ref Queue confList, bool isWithinTimeRange)
        {
            try
            {
                // Search all confs between the date range. Exclude the "deleted" confs.
                DataSet ds = new DataSet();
                string dsTable = "ConfList";
                string query = null;

                if (isWithinTimeRange)
                {
                    // search between a time range excluding a particular conference.
                    query += "select * from conf_conference_d where  conftype <>7 and [status]<>9 and confNumName <> " + excludeConfNumName.ToString();
                    query += " and dateadd(mi,duration,confdate) >= '" + fromDate.ToString("G") + "'";
                    query += " and confdate <= '" + toDate.ToString("G") + "'";
                    query += " order by confdate asc";
                }
                else
                {
                    // general search between dates with no time range defined.
                    query = "select * from conf_conference_d";
                    query += " where  conftype <>7 and [status] <> 9 ";
                    query += " and confdate between '" + fromDate.ToShortDateString() + " 00:00:01 AM'";
                    query += " and '" + toDate.ToShortDateString() + " 11:59:59 PM'";
                    query += " order by confdate asc";
                }

                bool ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Trace("SQL error");
                    return (false);
                }

                for (int i = 0; i < ds.Tables[dsTable].Rows.Count; i++)
                {
                        // conference object
                        NS_MESSENGER.Conference conf = new NS_MESSENGER.Conference();
                        // confid
                        conf.iDbID = Int32.Parse(ds.Tables[dsTable].Rows[i]["ConfID"].ToString().Trim());
                        // instanceid
                        conf.iInstanceID = Int32.Parse(ds.Tables[dsTable].Rows[i]["InstanceID"].ToString().Trim());

                        // fetch rest of conf details
                        bool ret1 =  FetchConf(ref conf);
                        if (ret1)
                        {
                            logger.Trace("Some problems in fetching all the conf details...");                            
                        }
                        
                        // add to queue 
                        confList.Enqueue(conf);
                }
                
                return true;

            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }

        internal bool FetchUniqueMcuListForConference(NS_MESSENGER.Conference conf,ref Queue mcuList)
        {
            try
            {
                DataSet ds = new DataSet();
                string dsTable = "McuList";
                string query = " ";
               
                bool ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Trace("SQL error");
                    return (false);
                }

                for (int i = 0; i < ds.Tables[dsTable].Rows.Count; i++)
                {
                    // mcu id 
                    int mcuId = Int32.Parse(ds.Tables[dsTable].Rows[i][0].ToString().Trim());
                    
                    // add to queue
                    mcuList.Enqueue(mcuId);
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        
        internal bool FetchPartyListCount(bool eitherAudioOrVideo, bool eitherUserOrRoom, int bridgeID, Queue uidList, ref int partyCount)
        {
            try
            {
                // uidlist stringed out in sql format                
                string strUidList = null;
                while (uidList.Count > 0)
                {
                    strUidList += uidList.Dequeue() + ",";
                }
                logger.Trace("strUidList : " + strUidList);
                
                // validate string 
                if (strUidList == null)
                    return true;
                if (strUidList.Trim().Length < 1)
                    return true;

                // stripping the last comma out                
                strUidList = strUidList.Remove(strUidList.Length - 1);
                logger.Trace("After stripping last comma : " + strUidList);

                // Search all confs between the date range. Exclude the "deleted" confs.
                DataSet ds = new DataSet();
                string dsTable = "PartyList";
                string query = null;
                if (eitherUserOrRoom)
                {
                    // ROOM
                    query = " select count(*) from conf_room_d ";
                    query += " where UID in (" + strUidList + ")";
                    query += " and bridgeID = " + bridgeID.ToString();
                    if (eitherAudioOrVideo)
                    {
                        query += " and audioorvideo = 2"; //VIDEO //FB 1744
                    }
                    else
                    {
                        query += " and audioorvideo <> 2"; //AUDIO //FB 1744
                    }

                    logger.Trace(query);
                }
                else
                {
                    // USER
                    query = " select count(*) from conf_user_d ";
                    query += " where UID in (" + strUidList + ")";
                    query += " and invitee = 1";
                    query += " and bridgeID = " + bridgeID.ToString();
                    if (eitherAudioOrVideo)
                    {
                        query += " and audioorvideo = 2"; //VIDEO //FB 1744
                    }
                    else
                    {
                        query += " and audioorvideo <> 2"; //AUDIO //FB 1744
                    }                    
                }
                
               
                bool ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Trace("SQL error");
                    return (false);
                }

                if( ds.Tables[dsTable].Rows.Count > 0)
                {
                   partyCount = Int32.Parse(ds.Tables[dsTable].Rows[0][0].ToString().Trim());
                }                
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }

        internal bool FetchAudioVideoUserListInConference(NS_MESSENGER.Conference conf, ref int audioCallsInConf, ref int videoCallsInConf)
        {
            try
            {
                DataSet ds = new DataSet();
                string dsTable = "VideoList";
                string query = null;
                query = " select count (*) from conf_room_d where audioOrVideo = 2 and confid = " + conf.iDbID.ToString() + " and instanceid = " + conf.iInstanceID.ToString(); //FB 1744               
                bool ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Trace("SQL error");
                    return (false);
                }

                if (ds.Tables[dsTable].Rows.Count > 0)
                {
                    videoCallsInConf = Int32.Parse(ds.Tables[dsTable].Rows[0][0].ToString().Trim());
                }


                DataSet ds1 = new DataSet();
                string dsTable1 = "AudioList";
                string query1 = null;
                query1 = " select count (*) from conf_room_d where audioOrVideo <>2 and confid = " + conf.iDbID.ToString() + " and instanceid = " + conf.iInstanceID.ToString(); //FB 1744
                bool ret1 = SelectCommand(query1, ref ds1, dsTable1);
                if (!ret1)
                {
                    logger.Trace("SQL error");
                    return (false);
                }
                if (ds1.Tables[dsTable1].Rows.Count > 0)
                {
                    audioCallsInConf += Int32.Parse(ds1.Tables[dsTable1].Rows[0][0].ToString().Trim());
                }


                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }

        internal bool FetchConfUserUIDs(NS_MESSENGER.Conference conf, ref Queue confUserUidList)
        {
            try
            {
                // Search all confs between the date range. Exclude the "deleted" confs.
                DataSet ds = new DataSet();
                string dsTable = "ConfUserUidList";
                string query = null;

                query = "select uid from conf_user_d";
                query += " where confid = " + conf.iDbID.ToString();
                query += " and instanceid = " + conf.iInstanceID.ToString();

                bool ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Trace("SQL error");
                    return (false);
                }

                for (int i = 0; i < ds.Tables[dsTable].Rows.Count; i++)
                {

                    int uid = Int32.Parse(ds.Tables[dsTable].Rows[i][0].ToString().Trim());
                    logger.Trace("UserUID : " + uid.ToString());

                    // add to queue 
                    confUserUidList.Enqueue(uid);
                }

                return true;

            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }

        internal bool FetchConfRoomUIDs(NS_MESSENGER.Conference conf,ref Queue confRoomUidList)
        {
            try
            {
                // Search all confs between the date range. Exclude the "deleted" confs.
                DataSet ds = new DataSet();
                string dsTable = "ConfRoomUidList";
                string query = null;

                query = "select uid from conf_room_d";
                query += " where confid = " + conf.iDbID.ToString();
                query += " and instanceid = " + conf.iInstanceID.ToString();                

                bool ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Trace("SQL error");
                    return (false);
                }

                for (int i = 0; i < ds.Tables[dsTable].Rows.Count; i++)
                {
                    
                    int uid = Int32.Parse(ds.Tables[dsTable].Rows[i][0].ToString().Trim());
                    logger.Trace("RoomUID : " + uid.ToString());

                    // add to queue 
                    confRoomUidList.Enqueue(uid);
                }

                return true;

            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }

        public bool GetTechSupportEmail(ref string strTechSupportEmail)
        {
            // In this case the Email would be send from TechSupport User Account
            // So first Retrieve the Tech Support Email Information
            // FB 377- Maneesh code Start
            DataSet dsTechSupport = new DataSet();
            string strTableName = "TechSupport";
            string queryFetchSupportInfo = "Select email from Sys_TechSupport_D ";
            bool bRetVal = SelectCommand(queryFetchSupportInfo, ref dsTechSupport, strTableName);
            if (!bRetVal)
            {
                logger.Trace("myVRM Tech support information not found in the database.");
                return false;
            }
            // fetch the Support Email ID from the returned data set.
            strTechSupportEmail = dsTechSupport.Tables[strTableName].Rows[0]["email"].ToString().Trim();
            if (strTechSupportEmail.Length <= 0)
            {
                logger.Trace("myVRM Tech support email not found in the database.");
                return false;
            }
            return true;
            // FB 377- Maneesh code End
        }

        internal bool FetchUserLoginHistory(NS_MESSENGER.Party party,DateTime fromDate, DateTime toDate, ref Queue loginDateList)
        {
            try
            {
                DataSet ds = new DataSet();
                string dsTable = "GetUserHistory";
                string query = null;

                query = "select logindate from sys_loginaudit_d where userid = " + party.iDbId.ToString();
                query += " and logindate between ('" + fromDate.ToString() + "')";
                query += " AND ('"+ toDate.ToString() +"')";
                query += " order by logindate desc";                 
                bool ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Trace("SQL error");
                    return (false);
                }

                for (int i = 0; i < ds.Tables[dsTable].Rows.Count; i++)
                {

                    DateTime loginDate = DateTime.Parse(ds.Tables[dsTable].Rows[i][0].ToString().Trim());
                    logger.Trace("Login Date : " + loginDate.ToString());

                    // add to queue 
                    loginDateList.Enqueue(loginDate);
                }

                return true;
            }
            catch (Exception e)
            {
                this.errMsg = e.Message;
                return false;
            }
        }
        
        #region Fetch Company Mail Logo
        internal int FetchMailLogo(int orgid, ref byte[] mailLogoImage)
        {
            int mailLogo = 0;
            try
            {
                mailLogoImage = null;
                DataSet ds = new DataSet();
                string dsTable = "OrgSettings";
                string query = "";

                query = "select  a.mailLogo, (select Attributeimage from img_list_d where imageid=a.mailLogo)";
                query += " as MailLogoImage from Org_Settings_d a where orgid=" + orgid + " and a.mailLogo > 0 "; //ZD 102157
                                
                bool ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Trace("SQL error");
                    return 0;
                }
                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            if (ds.Tables[0].Rows[0]["mailLogo"] != null)
                            {
                                Int32.TryParse(ds.Tables[0].Rows[0]["mailLogo"].ToString().Trim(), out mailLogo);
                            }

                            if (ds.Tables[0].Rows[0]["MailLogoImage"] != null)
                            {
                                if (ds.Tables[0].Rows[0]["MailLogoImage"].ToString() != "")
                                    mailLogoImage = (byte[])ds.Tables[0].Rows[0]["MailLogoImage"];
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return 0;
            }
            return mailLogo;
        }
        #endregion
        //Embedded Image Ends...

        //Method added for FB 1710
        #region Fetch Footer Message of Organisation
        internal string FetchFooterMessage(int orgid, ref byte[] footerImage)
        {
            string footermessage = "";
            try
            {

                DataSet ds = new DataSet();
                string dsTable = "OrgSettings";
                string query = "";

                query = "select  a.FooterMessage, (select Attributeimage from img_list_d where imageid=a.footerimage)";
                query += " as FooterImg from Org_Settings_d a where orgid=" + orgid;

                bool ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Trace("SQL error");
                    return null;
                }
                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            if (ds.Tables[0].Rows[0]["FooterMessage"] != null)
                                footermessage = ds.Tables[0].Rows[0]["FooterMessage"].ToString().Trim();

                            if (ds.Tables[0].Rows[0]["FooterImg"] != null)
                            {
                                if (ds.Tables[0].Rows[0]["FooterImg"].ToString() != "")
                                    footerImage = (byte[])ds.Tables[0].Rows[0]["FooterImg"];
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return null;
            }
            return footermessage;
        }

        #endregion

        //Method added for FB 1758 - This method is modified during FB 1830
        #region Fetch SytTech Info
        /// <summary>
        /// FetchTechInfo
        /// </summary>
        /// <param name="orgid"></param>
        /// <param name="emailBody"></param>
        internal void FetchTechInfo(int orgid, ref string emailBody)
        {
            try
            {
                DataSet ds = new DataSet();
                string dsTable = "SysTechInfo";
                string query = "";
                string contact = "";
                string emailid = "";
                string phone = "";

                query = "select [Name], email, phone from Sys_TechSupport_D where orgid=" + orgid;
                
                bool ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Trace("SQL error");
                    //return null;
                }
                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            if (ds.Tables[0].Rows[0]["Name"] != null)
                            {
                                contact = ds.Tables[0].Rows[0]["Name"].ToString().Trim();
                                contact = contact.Replace("||", "\"").Replace("!!", "'"); // FB 1888   
                            }
                            if (ds.Tables[0].Rows[0]["email"] != null)
                            {
                                emailid = ds.Tables[0].Rows[0]["email"].ToString().Trim();
                            }
                            if (ds.Tables[0].Rows[0]["phone"] != null)
                            {
                                phone = ds.Tables[0].Rows[0]["phone"].ToString().Trim();
                            }
                        }
                    }
                }
                emailBody = emailBody.Replace("{37}", contact);
                emailBody = emailBody.Replace("{38}", emailid);
                emailBody = emailBody.Replace("{39}", phone);
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                //return "";
            }
        }
        #endregion

        //Method added for FB 1830 - start
        #region FetchEmailString
        /// <summary>
        /// FetchEmailString
        /// </summary>
        /// <param name="orgid"></param>
        /// <param name="emailbody"></param>
        /// <param name="emailsubject"></param>
        internal void FetchEmailString(int orgid, ref string emailBody, ref string emailSubject, int emailType)
        {
            emailBody = "";
            emailSubject = "";
            try
            {
                DataSet ds = new DataSet();
                string dsTable = "EmailContent";
                string query = "";

                if (orgid < 11)
                    orgid = 11; //default company

                query = "select emailbody, emailsubject from email_content_d where emailmode=0 and emailtypeid='" + emailType + "'";
                query += " and emaillangid=(select emaillangid from Org_Settings_d where orgid='" + orgid + "')";
                

                bool ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Trace("SQL error");
                    //return null;
                }
                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            if (ds.Tables[0].Rows[0]["emailbody"] != null)
                                emailBody = ds.Tables[0].Rows[0]["emailbody"].ToString().Trim();

                            if (ds.Tables[0].Rows[0]["emailsubject"] != null)
                                emailSubject = ds.Tables[0].Rows[0]["emailsubject"].ToString().Trim();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                //return null;
            }
        }
        #endregion
        //Method added for FB 1830 - end

        //FB 2335
        #region SetDefaultLO
        /// <summary>
        /// To set default Layout if the conditions are met
        /// </summary>
        /// <param name="orgID"></param>
        /// <param name="cMcu"></param>
        /// <param name="partyq"></param>
        /// <returns></returns>
        public bool SetDefaultLO(int orgID, ref NS_MESSENGER.MCU cMcu, Queue partyq)
        {
            DataSet ds = new DataSet();
            string query = "";
            string dsTable = "layOut";
            bool ret = false;
            int defPolycomMGCLO = 0, defPolycomRMXLO = 0;
            System.Collections.IEnumerator partyEnumerator;
            int partyCount = 0;
            NS_MESSENGER.Party party = null;
            int cntOTX = 0, cntVSX = 0, cntRPX = 0,cntHDX = 0;
            try
            {
                if (cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv6 || cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv7 || cMcu.etType == NS_MESSENGER.MCU.eType.RMX)
                {

                    query = "Select DefPolycomMGCLO,DefPolycomRMXLO,DefCiscoTPLO,DefCTMSLO  from org_settings_d where orgID = " + orgID;

                    ret = SelectCommand(query, ref ds, dsTable);

                    if (!ret)
                    {
                        logger.Trace("SQL error");
                        return (false);
                    }

                    for (int i = 0; i < ds.Tables[dsTable].Rows.Count; i++)
                    {

                        Int32.TryParse(ds.Tables[dsTable].Rows[i]["DefPolycomMGCLO"].ToString().Trim(), out defPolycomMGCLO);
                        Int32.TryParse(ds.Tables[dsTable].Rows[i]["DefPolycomRMXLO"].ToString().Trim(), out defPolycomRMXLO);
                    }



                    partyEnumerator = partyq.GetEnumerator();
                    partyCount = partyq.Count;
                    for (int i = 0; i < partyCount; i++)
                    {
                        // go to next party in the queue
                        partyEnumerator.MoveNext();

                        // check if party is lecturer. if yes, save the lecturer name in conf object.
                        party = new NS_MESSENGER.Party();
                        party = (NS_MESSENGER.Party)partyEnumerator.Current;

                        switch (party.etModelType)
                        {
                            case NS_MESSENGER.Party.eModelType.POLYCOM_OTX:
                                cntOTX = cntOTX + 1;
                                break;
                            case NS_MESSENGER.Party.eModelType.POLYCOM_RPX:
                                cntRPX = cntRPX + 1;
                                break;
                            case NS_MESSENGER.Party.eModelType.POLYCOM_VSX:
                                cntVSX = cntVSX + 1;
                                break;
                            case NS_MESSENGER.Party.eModelType.POLYCOM_HDX:
                                cntHDX = cntHDX + 1;
                                break;
                            default:
                                return true;
                                break;
                        }

                    }

                    switch (cMcu.etType)
                    {
                        case NS_MESSENGER.MCU.eType.ACCORDv6:
                        case NS_MESSENGER.MCU.eType.ACCORDv7:

                            if (cntRPX > 0 && (cntVSX > 0 || cntHDX > 0))
                                cMcu.iDefaultLO = defPolycomMGCLO;

                            break;

                        case NS_MESSENGER.MCU.eType.RMX:
                            if (cntOTX > 0 && (cntVSX > 0 || cntHDX > 0))
                                cMcu.iDefaultLO = defPolycomMGCLO;

                            if (cntRPX > 0 && (cntVSX > 0 || cntHDX > 0))
                                cMcu.iDefaultLO = defPolycomMGCLO;
                            break;
                    }
                }


            }
            catch (Exception ex)
            {

                logger.Exception(100, ex.Message);
            }

            return ret;

        }
        #endregion

        //FB 2363 start
        #region FetchytsConf
        /// <summary>
        /// FetchytsConf
        /// </summary>
        /// <param name="confS"></param>
        /// <param name="ConfID"></param>
        /// <param name="InstanceID"></param>
        /// <returns></returns>
        internal bool FetchytsConf(ref List<NS_YorktelScheduling.YTSConference> confS, string ConfID, string InstanceID)
        {
            NS_YorktelScheduling.YTSConference conf = null;
            try
            {
                //CONF.IP
                // Retreive conf details
                DataSet ds = new DataSet();
                string dsTable = "Conference";

                string query = null; bool ret = false; // FB 2720  �(Alt 0134) -  Delimiter for users   �(Alt 0167) -  Delimiter for user info 
                query = "SELECT  isnull(Stuff ((SELECT distinct  N'� ' + d.name +'�'+ CAST(d.RoomID AS VARCHAR(15)) + '�' + cast(isTelepresence as varchar(5)) from conf_room_d r, loc_room_d d "
                                     + " where r.Roomid = d.Roomid and r.Confid = a.Confid FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N''),'') as [Rooms In Conf] "
                                     + " ,isnull(Stuff (( SELECT distinct N'� ' + ui.FirstName +' '+ ui.Lastname +'�'+CAST(ui.UserID AS VARCHAR(15))+'�'+ui.Email+'�'+audioaddon"
                                     + " from dbo.Conf_User_D I, usr_list_D ui "
                                     + " where I.userid = ui.userid and I.Confid = a.confid and I.instanceid = a.instanceid"
                                     + " FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N''),'') as [Users in Conf]"
                                     + " ,isnull(Stuff (( SELECT distinct N', ' + ui.FirstName +' '+ ui.Lastname +'�'+CAST(ui.UserID AS VARCHAR(15))+'�'+ui.Email+'�0'"
                                     + " from dbo.Conf_User_D I, dbo.Usr_GuestList_D ui "
                                     + " where I.userid = ui.userid and I.Confid = a.confid and I.instanceid = a.instanceid"
                                     + " FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N''),'') as [Guests in Conf], (select top 1 BridgeName from Conf_Room_D x, mcu_list_d y where x.Confid = a.confid and x.bridgeid = y.bridgeid) as Bridgename "
                                     + " ,a.MeetandGreet"
                                     + " ,a.DedicatedVNOCOperator as Dedicatedvnoc"
                                     + " ,a.OnSiteAVSupport as Avtech"
                                     + " ,* FROM dbo.Conf_Conference_D a " //left outer join dbo.Conf_Room_D b on a.confid = b.confid AND a.instanceid = b.instanceid "
                                     + " WHERE a.confid =" + ConfID;  // b.confid and PushedToExternal = 0 AND status IN(0,5)"; //Do we want push pending conference"

                if (InstanceID.Trim() != "")
                    query += " And a.instanceid = " + InstanceID;


                ret = false;
                ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Exception(100, "SQL error");
                    return (false);
                }

                #region Build Conferences object
                for (int i = 0; i < ds.Tables[dsTable].Rows.Count; i++)
                {
                    conf = new NS_YorktelScheduling.YTSConference();
                    int.TryParse(ds.Tables[dsTable].Rows[i]["orgId"].ToString(), out conf.iOrgID);
                    int.TryParse(ds.Tables[dsTable].Rows[i]["ConfId"].ToString(), out conf.iDbID);
                    int.TryParse(ds.Tables[dsTable].Rows[i]["InstanceId"].ToString(), out conf.iInstanceID);
                    int.TryParse(ds.Tables[dsTable].Rows[i]["ConfNumName"].ToString(), out conf.iDbNumName);
                    conf.sExternalName = ds.Tables[dsTable].Rows[i]["ExternalName"].ToString();
                    int.TryParse(ds.Tables[dsTable].Rows[i]["owner"].ToString(), out conf.iHostUserID);
                    conf.sExternalSchedulingID = ds.Tables[dsTable].Rows[i]["ESId"].ToString();
                    conf.sExternalSchedulingType = ds.Tables[dsTable].Rows[i]["ESType"].ToString();
                    conf.sMcuName = ds.Tables[dsTable].Rows[i]["Bridgename"].ToString();

                    conf.sMeetandGreet = ds.Tables[dsTable].Rows[i]["MeetandGreet"].ToString();
                    conf.sDedicatedvnoc = ds.Tables[dsTable].Rows[i]["Dedicatedvnoc"].ToString();
                    conf.sAvtech = ds.Tables[dsTable].Rows[i]["Avtech"].ToString();

                    if (conf.sExternalSchedulingType == "" && conf.sExternalSchedulingID == "")
                        conf.sExternalSchedulingID = conf.iDbNumName.ToString();

                    conf.isVIP = false;
                    if (ds.Tables[dsTable].Rows[i]["isVIP"].ToString() == "1")
                        conf.isVIP = true;

                    try
                    {
                        conf.sPwd = ds.Tables[dsTable].Rows[i]["password"].ToString().Trim();
                        int.TryParse(conf.sPwd, out conf.iPwd);
                    }
                    catch (Exception)
                    {
                        conf.sPwd = "";
                        conf.iPwd = 0;
                    }

                    try
                    {
                        int.TryParse(ds.Tables[dsTable].Rows[i]["deleted"].ToString().Trim(), out conf.iDeleted);
                        int.TryParse(ds.Tables[dsTable].Rows[i]["PushedToExternal"].ToString().Trim(), out conf.iPushed);
                    }
                    catch (Exception)
                    {
                        conf.iDeleted = 0;
                        conf.iPushed = 0;
                    }

                    #region Fetch the host email
                    DataSet ds2 = new DataSet();
                    string dsTable2 = "ConfHost";
                    string query2 = "Select Email,FirstName+ ' '+LastName as HostName from Usr_List_D where userid = " + conf.iHostUserID.ToString();
                    bool ret3 = SelectCommand(query2, ref ds2, dsTable2);
                    if (!ret3)
                    {
                        logger.Exception(100, "SQL error");
                        return (false);
                    }

                    if (ds2.Tables[dsTable2].Rows.Count < 1)
                    {
                        // no record 
                        logger.Trace("No host email found.");
                        return false;
                    }
                    conf.sHostEmail = ds2.Tables[dsTable2].Rows[0]["Email"].ToString();
                    conf.sHostName = ds2.Tables[dsTable2].Rows[0]["HostName"].ToString();
                    #endregion

                    conf.sRoomString = ds.Tables[dsTable].Rows[i]["Rooms In Conf"].ToString();
                    conf.sUserString = ds.Tables[dsTable].Rows[i]["Users in Conf"].ToString() + "," + ds.Tables[dsTable].Rows[i]["Guests in Conf"].ToString();

                    ret = false;
                    ret = GenerateConfName(conf, ref conf.sDbName);
                    if (!ret)
                    {
                        logger.Trace("Failure in generating conf name for bridges.");
                        return false;
                    }

                    //lastrundatetime
                    conf.dtModifiedDateTime = DateTime.Parse(ds.Tables[dsTable].Rows[i]["LastRunDateTime"].ToString());

					//ZD 100085 Starts
                    int.TryParse(ds.Tables[dsTable].Rows[i]["Duration"].ToString(), out conf.iDuration);
                    logger.Trace("Conf Duration : " + conf.iDuration.ToString());

                    // start date & time 
                    conf.dtSetUpDateTime = DateTime.Parse(ds.Tables[dsTable].Rows[i]["SetupTime"].ToString()); //start time of conf
                    
                    //ZD 104332 starts
                    if (conf.iImmediate == 1)
                    conf.dtStartDateTime = DateTime.Parse(ds.Tables[dsTable].Rows[i]["confdate"].ToString()); //FB 2440
                    else
                        conf.dtStartDateTime = DateTime.Parse(ds.Tables[dsTable].Rows[i]["confdate"].ToString()).AddSeconds(-40); //FB 2440 //5 secs buffer
                    //ZD 104332 ends

                    conf.dtEndDateTime = conf.dtStartDateTime.AddMinutes(conf.iDuration);
                    //conf.dtEndDateTime = DateTime.Parse(ds.Tables[dsTable].Rows[i]["TearDownTime"].ToString());
                    logger.Trace("Start Time : " + conf.dtStartDateTime.ToString("T"));
					//ZD 100085 End
                    // conf timezone
                    int.TryParse(ds.Tables[dsTable].Rows[i]["Timezone"].ToString(), out conf.iTimezoneID);
                    logger.Trace("TimezoneID : " + conf.iTimezoneID.ToString());

                    // timezone 
                    DataSet dsTZ = new DataSet();
                    string dsTableTZ = "TZName";
                    string queryTZ = "select timezone,timezonediff from gen_timezone_s where timezoneID = " + conf.iTimezoneID.ToString();
                    bool retTZ = SelectCommand(queryTZ, ref dsTZ, dsTableTZ);
                    if (!retTZ)
                    {
                        logger.Exception(100, "SQL error");
                        return (false);
                    }
                    conf.sName_StartDateTimeInLocalTZ = dsTZ.Tables[dsTableTZ].Rows[0]["timezonediff"].ToString() + dsTZ.Tables[dsTableTZ].Rows[0]["timezone"].ToString();

                    // start date & time in UTC
                    conf.dtStartDateTimeInUTC = conf.dtStartDateTime;

                    // Convert to local TZ
                    DataSet dsTZ1 = new DataSet();
                    string dsTableTZ1 = "LocalTZ";
                    string queryTZ1 = "select dbo.changeTime (" + conf.iTimezoneID.ToString() + ",'" + conf.dtStartDateTimeInUTC + "')";
                    bool retTZ1 = SelectCommand(queryTZ1, ref dsTZ1, dsTableTZ1);
                    if (!retTZ1)
                    {
                        logger.Exception(100, "SQL error");
                        return (false);
                    }
                    conf.dtStartDateTimeInLocalTZ = DateTime.Parse(dsTZ1.Tables[dsTableTZ1].Rows[0][0].ToString());
					
                    //ZD 100085 Starts
                    // duration
                    //int.TryParse(ds.Tables[dsTable].Rows[i]["Duration"].ToString(), out conf.iDuration);
                    //logger.Trace("Conf Duration : " + conf.iDuration.ToString());
					//ZD 100085 End
                    
                    //check conf type
                    int confType = 0;
                    int.TryParse(ds.Tables[dsTable].Rows[i]["ConfType"].ToString(), out confType); //dbo.Conference.ConfType				
                    logger.Trace("Conf Type : " + confType.ToString());
                    switch (confType)
                    {
                        case 2:
                            {
                                // audio-video conference
                                conf.etMediaType = NS_MESSENGER.Conference.eMediaType.AUDIO_VIDEO;
                                conf.etType = NS_MESSENGER.Conference.eType.MULTI_POINT;
                                break;
                            }
                        case 6:
                            {
                                // audio conference
                                conf.etMediaType = NS_MESSENGER.Conference.eMediaType.AUDIO;
                                conf.etType = NS_MESSENGER.Conference.eType.MULTI_POINT;
                                break;
                            }
                        case 4:
                            {
                                // p2p conference
                                conf.etMediaType = NS_MESSENGER.Conference.eMediaType.AUDIO_VIDEO;
                                conf.etType = NS_MESSENGER.Conference.eType.POINT_TO_POINT;
                                break;
                            }
                        default:
                            {   // 7 = room conference
                                this.errMsg = "This is not an audio/video conference.";
                                return false;
                            }
                    }

                    conf.bAutoTerminate = true;

                    // lecture mode
                    int lecMode = 0;
                    int.TryParse(ds.Tables[dsTable].Rows[i]["LectureMode"].ToString().Trim(), out lecMode);
                    if (lecMode == 0)
                    {
                        conf.bLectureMode = false;
                        logger.Trace("Lecture Mode : Disabled ");
                    }
                    else
                    {
                        conf.bLectureMode = true;
                        logger.Trace("Lecture Mode : Enabled ");
                    }
                    //FB 2486 Start
                    int closedcaption = 0;
                    int.TryParse(ds.Tables[dsTable].Rows[0]["isTextMsg"].ToString().Trim(), out closedcaption);
                    if (closedcaption == 0)
                    {
                        conf.bMessageOverlay = false;
                        logger.Trace("MessageOverlay : Disabled ");
                    }
                    else
                    {
                        conf.bMessageOverlay = true;
                        logger.Trace("MessageOverlay : Enabled ");
                    }
                    //Fb 2486 End
                    #region Fetch the conf advanced audio/video params
                    DataSet ds1 = new DataSet();
                    string dsTable1 = "Conference";
                    string query1 = "Select * from Conf_AdvAVParams_D where confid = '" + conf.iDbID.ToString() + "' and instanceid = '" + conf.iInstanceID.ToString() + "'";
                    bool ret1 = SelectCommand(query1, ref ds1, dsTable1);
                    if (!ret1)
                    {
                        logger.Exception(100, "SQL error");
                        return (false);
                    }

                    if (ds1.Tables[dsTable1].Rows.Count > 0)
                    {
                        //line rate
                        bool ret2 = ConvertToMcuLineRate((Int32.Parse(ds1.Tables[dsTable1].Rows[0]["LineRateID"].ToString())), ref conf.stLineRate.etLineRate);
                        if (!ret2)
                        {
                            logger.Exception(100, "Error in LineRateID");
                            return (false);
                        }


                        // audio codec
                        ret2 = false;
                        ret2 = ConvertToMcuAudioCodec((Int32.Parse(ds1.Tables[dsTable1].Rows[0]["AudioAlgorithmID"].ToString())), ref conf.etAudioCodec);
                        if (!ret2)
                        {
                            logger.Exception(100, "Error in AudioAlgorithmID");
                            return (false);
                        }


                        // network
                        ret2 = false;
                        ret2 = ConvertToMcuNetwork((Int32.Parse(ds1.Tables[dsTable1].Rows[0]["VideoProtocolID"].ToString())), ref conf.etNetwork);
                        if (!ret2)
                        {
                            logger.Exception(100, "Error in VideoProtocolID");
                            return (false);
                        }


                        try
                        {

                            // video layouts 
                            if (ds1.Tables[dsTable1].Rows[0]["videoLayoutID"].ToString() != null)
                            {
                                int.TryParse(ds1.Tables[dsTable1].Rows[0]["videoLayoutID"].ToString(), out conf.iVideoLayout);
                            }

                            // dual stream mode - H.239
                            if (ds1.Tables[dsTable1].Rows[0]["dualStreamModeID"].ToString() != null)
                            {
                                if (Int32.Parse(ds1.Tables[dsTable1].Rows[0]["dualStreamModeID"].ToString()) == 1)
                                {
                                    conf.bH239 = true;
                                }
                            }

                            // conference on port (polycom-specific feature)
                            if (ds1.Tables[dsTable1].Rows[0]["conferenceOnPort"].ToString() != null)
                            {
                                if (Int32.Parse(ds1.Tables[dsTable1].Rows[0]["conferenceOnPort"].ToString()) == 1)
                                {
                                    conf.bConferenceOnPort = true;
                                }
                            }

                            // encryption (polycom-specific feature)
                            if (ds1.Tables[dsTable1].Rows[0]["encryption"].ToString() != null)
                            {
                                if (Int32.Parse(ds1.Tables[dsTable1].Rows[0]["encryption"].ToString()) == 1)
                                {
                                    conf.bEncryption = true;
                                }
                            }

                            // max audio ports
                            if (ds1.Tables[dsTable1].Rows[0]["maxAudioParticipants"].ToString() != null)
                            {
                                int.TryParse(ds1.Tables[dsTable1].Rows[0]["maxAudioParticipants"].ToString(), out conf.iMaxAudioPorts);
                            }

                            // max video ports
                            if (ds1.Tables[dsTable1].Rows[0]["maxVideoParticipants"].ToString() != null)
                            {
                                int.TryParse(ds1.Tables[dsTable1].Rows[0]["maxVideoParticipants"].ToString(), out conf.iMaxVideoPorts);
                            }

                            // video codecs
                            if (ds1.Tables[dsTable1].Rows[0]["videoSession"].ToString() != null)
                            {
                                // Note : this is a confusing name matchup 
                                // In VRM DB this is equated to VideoSession (which is actually incorrect but needed to keep it that way to support legacy deployments)
                                // Its actually a VideoCodec (hence, in RTC the name is now corrected internally).
                                int VideoSession = 0;
                                int.TryParse(ds1.Tables[dsTable1].Rows[0]["videoSession"].ToString(), out VideoSession);
                                switch (VideoSession)
                                {
                                    case 0:
                                        {
                                            conf.etVideoCodec = NS_MESSENGER.Conference.eVideoCodec.AUTO;
                                            break;
                                        }
                                    case 1:
                                        {
                                            conf.etVideoCodec = NS_MESSENGER.Conference.eVideoCodec.H261;
                                            break;
                                        }
                                    case 2:
                                        {
                                            conf.etVideoCodec = NS_MESSENGER.Conference.eVideoCodec.H263;
                                            break;
                                        }
                                    case 3:
                                        {
                                            conf.etVideoCodec = NS_MESSENGER.Conference.eVideoCodec.H264;
                                            break;
                                        }
                                }
                            }

                            try
                            {
                                // video modes
                                switch (Int32.Parse(ds1.Tables[dsTable1].Rows[0]["videoMode"].ToString()))
                                {
                                    case 1:
                                        {
                                            conf.etVideoSession = NS_MESSENGER.Conference.eVideoSession.SWITCHING;
                                            break;
                                        }
                                    case 2:
                                        {
                                            conf.etVideoSession = NS_MESSENGER.Conference.eVideoSession.TRANSCODING;
                                            break;
                                        }
                                    default:
                                        {
                                            conf.etVideoSession = NS_MESSENGER.Conference.eVideoSession.CONTINOUS_PRESENCE;
                                            break;
                                        }
                                }
                            }
                            catch (Exception)
                            {
                                logger.Trace("Error in video mode.");
                            }

                            try
                            {
                                // single dial-in
                                if (ds1.Tables[dsTable1].Rows[0]["singleDialIn"].ToString() != null)
                                {
                                    if (Int32.Parse(ds1.Tables[dsTable1].Rows[0]["singleDialIn"].ToString()) == 1)
                                    {
                                        conf.isSingleDialIn = true;
                                    }
                                }
                            }
                            catch (Exception)
                            {
                                logger.Trace("Error in video mode.");
                            }
                        }
                        catch (Exception e)
                        {
                            logger.Trace("Advanced Audio/Video Params: " + e.Message);
                        }
                    }
                    #endregion

                    confS.Add(conf);
                }
                #endregion
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }

            return true;
        }
        #endregion

        #region FetchSystemESSettings
        /// <summary>
        /// FetchSystemESSettings
        /// </summary>
        /// <param name="SysESSet"></param>
        /// <param name="Estype"></param>
        /// <returns></returns>
        internal bool FetchSystemESSettings(ref NS_MESSENGER.ESSettings SysESSet, string Estype, string orgID)
        {
            cryptography.Crypto crypto = null;
            try
            {
                bool ret = false;
                DataSet ds = new DataSet();
                string dsTable = "ESSettings";

                string query = "select * ";
                if (orgID != "")
                    query += ", (select isnull(CustomerID,'') +','+ isnull(CustomerName,'') from Org_Settings_D where orgid = " + orgID + ") as custId";

                query += " from Sys_ESSettings_D";

                ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Exception(100, "SQL error");
                    return false;
                }

                if (orgID != "")
                {
                    string customerID = ds.Tables[dsTable].Rows[0]["custId"].ToString();
                    SysESSet.sCustomerID = customerID.Split(',')[0];
                    SysESSet.sCustomerName = customerID.Split(',')[1];
                }

                SysESSet.sPartnerName = ds.Tables[dsTable].Rows[0]["PartnerName"].ToString();
                SysESSet.sPartnerEmail = ds.Tables[dsTable].Rows[0]["PartnerEmail"].ToString();
                SysESSet.sPartnerURL = ds.Tables[dsTable].Rows[0]["PartnerURL"].ToString();
                SysESSet.sUserName = ds.Tables[dsTable].Rows[0]["UserName"].ToString();
                crypto = new Crypto();
                SysESSet.sPassword = crypto.decrypt(ds.Tables[dsTable].Rows[0]["Password"].ToString());
                SysESSet.dModifiedDate = DateTime.Parse(ds.Tables[dsTable].Rows[0]["ModifiedDate"].ToString());
                SysESSet.sESType = Estype;
                int.TryParse(ds.Tables[dsTable].Rows[0]["TimeoutValue"].ToString(), out SysESSet.sTimeout);//FB 2363

            }
            catch (Exception ex)
            {
                logger.Trace("FetchSystemESSettings:" + ex.StackTrace);
                return false;
            }
            return true;

        }
        #endregion

        #region InsertExternalEvent
        /// <summary>
        /// InsertExternalEvent
        /// </summary>
        /// <param name="evnt"></param>
        /// <returns></returns>
        internal bool InsertExternalEvent(ref NS_MESSENGER.sysExtSchedEvent evnt)
        {
            try
            {
                bool ret = false;
                string query = "";

                query = "Insert into ES_Event_D(RequestID,TypeID,CustomerID,EventTrigger,EventTime,[Status],StatusDate,StatusMessage,RequestCall,ResponseCall,ConfNumName, RetryCount, orgId) values ('"
                      + evnt.sRequestID.ToString() + "','"
                      + evnt.sTypeID + "','"
                      + evnt.sCustomerID + "','"
                      + evnt.sEventTrigger + "', cast('"
                      + evnt.dEventTime.ToString() + "' as datetime),'"
                      + evnt.sStatus + "',cast('"
                      + evnt.dStatusDate.ToString() + "' as datetime),'"
                      + evnt.sStatusMessage + "','"
                      + evnt.sRequestCall + "','"
                      + evnt.sResponseCall + "','"
                      + evnt.sConfnumname + "',"
                      + "0,"
                      + evnt.sOrgID
                      + ")";

                ret = NonSelectCommand(query);
                if (!ret)
                {
                    logger.Exception(100, "query failed. query =" + query);
                    return false;
                }
            }
            catch (Exception ex)
            {
                logger.Trace("InsertExternalEvent :" + ex.Message);
                return false;
            }
            return true;
        }
        #endregion

        #region FetchExternalEventforRequestID
        /// <summary>
        /// FetchExternalEventforRequestID
        /// </summary>
        /// <param name="evnt"></param>
        /// <returns></returns>
        internal bool FetchExternalEventforRequestID(ref NS_MESSENGER.sysExtSchedEvent evnt)
        {
            bool ret = false;
            DataSet ds = null;
            string dsTable = "";
            string query = "";
            DateTime statusTime = DateTime.MinValue;
            DateTime eventTime = DateTime.MinValue;

            try
            {
                if (evnt != null)
                {

                    ds = new DataSet();
                    dsTable = "ExternalEvent";
                    query = "select * from ES_Event_D where RequestID = '" + evnt.sRequestID.Trim() + "'";

                    ret = SelectCommand(query, ref ds, dsTable);
                    if (!ret)
                    {
                        logger.Exception(100, "SQL error");
                        return false;
                    }
                    if (ds.Tables[dsTable] != null && ds.Tables[dsTable].Rows.Count > 0)
                    {

                        evnt.sCustomerID = ds.Tables[dsTable].Rows[0]["CustomerID"].ToString();
                        evnt.sTypeID = ds.Tables[dsTable].Rows[0]["TypeID"].ToString();
                        evnt.sRequestID = ds.Tables[dsTable].Rows[0]["RequestID"].ToString();
                        evnt.sStatus = ds.Tables[dsTable].Rows[0]["Status"].ToString();
                        evnt.sStatusMessage = ds.Tables[dsTable].Rows[0]["StatusMessage"].ToString();
                        evnt.sRequestCall = ds.Tables[dsTable].Rows[0]["RequestCall"].ToString();
                        evnt.sResponseCall = ds.Tables[dsTable].Rows[0]["ResponseCall"].ToString();
                        evnt.sConfnumname = ds.Tables[dsTable].Rows[0]["ConfNumName"].ToString();
                        evnt.sEventTrigger = ds.Tables[dsTable].Rows[0]["EventTrigger"].ToString();
                        DateTime.TryParse(ds.Tables[dsTable].Rows[0]["EventTime"].ToString(), out eventTime);
                        DateTime.TryParse(ds.Tables[dsTable].Rows[0]["StatusDate"].ToString(), out statusTime);
                        evnt.dStatusDate = statusTime;
                        evnt.dEventTime = eventTime;
                        Int32.TryParse(ds.Tables[dsTable].Rows[0]["RetryCount"].ToString(), out evnt.sRetryCount);
                    }
                }

            }
            catch (Exception ex)
            {
                logger.Trace("FetchSystemESSettings:" + ex.StackTrace);
                return false;
            }
            return true;

        }
        #endregion

        #region FetchExternalNewEventList
        /// <summary>
        /// FetchExternalNewEventList
        /// </summary>
        /// <param name="evntList"></param>
        /// <returns></returns>
        internal bool FetchExternalNewEventList(ref List<NS_MESSENGER.sysExtSchedEvent> evntList)
        {
            bool ret = false;
            DataSet ds = null;
            string dsTable = "";
            string query = "";
            DateTime statusTime = DateTime.MinValue;
            DateTime eventTime = DateTime.MinValue;
            int tbleCount = 0;
            NS_MESSENGER.sysExtSchedEvent evnt = null;

            try
            {

                ds = new DataSet();
                dsTable = "ExternalEvent";
                query = "select * from ES_Event_D where status = 'N'";

                ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Exception(100, "SQL error");
                    return false;
                }
                if (ds.Tables[dsTable] != null)
                {
                    for (tbleCount = 0; tbleCount < ds.Tables[dsTable].Rows.Count; tbleCount++)
                    {
                        if (evntList == null)
                            evntList = new List<NS_MESSENGER.sysExtSchedEvent>();

                        evnt = new NS_MESSENGER.sysExtSchedEvent();
                        evnt.sCustomerID = ds.Tables[dsTable].Rows[tbleCount]["CustomerID"].ToString();
                        evnt.sTypeID = ds.Tables[dsTable].Rows[tbleCount]["TypeID"].ToString();
                        evnt.sRequestID = ds.Tables[dsTable].Rows[tbleCount]["RequestID"].ToString();
                        evnt.sStatus = ds.Tables[dsTable].Rows[tbleCount]["Status"].ToString();
                        evnt.sStatusMessage = ds.Tables[dsTable].Rows[tbleCount]["StatusMessage"].ToString();
                        evnt.sRequestCall = ds.Tables[dsTable].Rows[tbleCount]["RequestCall"].ToString();
                        evnt.sResponseCall = ds.Tables[dsTable].Rows[tbleCount]["ResponseCall"].ToString();
                        evnt.sConfnumname = ds.Tables[dsTable].Rows[tbleCount]["ConfNumName"].ToString();
                        evnt.sEventTrigger = ds.Tables[dsTable].Rows[tbleCount]["EventTrigger"].ToString();
                        DateTime.TryParse(ds.Tables[dsTable].Rows[tbleCount]["EventTime"].ToString(), out eventTime);
                        DateTime.TryParse(ds.Tables[dsTable].Rows[tbleCount]["StatusDate"].ToString(), out statusTime);
                        evnt.dStatusDate = statusTime;
                        evnt.dEventTime = eventTime;
                        Int32.TryParse(ds.Tables[dsTable].Rows[tbleCount]["RetryCount"].ToString(), out evnt.sRetryCount);
                        Int32.TryParse(ds.Tables[dsTable].Rows[tbleCount]["orgId"].ToString(), out evnt.sOrgID);                        
                        evntList.Add(evnt);
                    }
                }

            }
            catch (Exception ex)
            {
                logger.Trace("FetchSystemESSettings:" + ex.StackTrace);
                return false;
            }
            return true;

        }
        #endregion

        #region UpdateExternalEvent
        /// <summary>
        /// UpdateExternalEvent
        /// </summary>
        /// <param name="evnt"></param>
        /// <returns></returns>
        internal bool UpdateExternalEvent(ref NS_MESSENGER.sysExtSchedEvent evnt)
        {
            try
            {
                string completionUpdateStatus = "X";
                string completionUpdateMsg = "Completed sucessfully by event";
                bool ret = false;
                string query = "";

                query = "Update ES_Event_D set [Status] = '" + evnt.sStatus + "',StatusDate = cast('" + evnt.dStatusDate.ToString() + "' as datetime),StatusMessage = N'" + evnt.sStatusMessage.Replace("'", " ") + "',ResponseCall = '" + evnt.sResponseCall.Replace("'", " ") + "',EventTrigger = '" + evnt.sEventTrigger + "', RetryCount = " + evnt.sRetryCount + " Where RequestID = '" + evnt.sRequestID + "'"; //FB 2272

                ret = NonSelectCommand(query);
                if (!ret)
                {
                    logger.Exception(100, "query failed. query =" + query);
                    return false;
                }

                if (evnt.sStatus == "C")
                {
                    completionUpdateStatus = "X";
                    completionUpdateMsg += " " + evnt.sRequestID;
                    
                    query = "Select RequestID from ES_Event_D where [Status] = 'E' and ConfNumName = " + evnt.sConfnumname;

                    DataSet ds = new DataSet();
                    string dsTable = "AllEvents";
                    ret = false;

                    ret = SelectCommand(query, ref ds, dsTable);
                    if (!ret)
                    {
                        logger.Exception(100, "SQL error");
                        return (false);
                    }

                    for (int i = 0; i < ds.Tables[dsTable].Rows.Count; i++)
                    {
                        string requestID = "";
                        ret = false;

                        requestID = ds.Tables[dsTable].Rows[i]["RequestID"].ToString().Trim();

                        query = "Update ES_Event_D set [Status] = '" + completionUpdateStatus + "', StatusMessage = '" + completionUpdateMsg + "' Where RequestID = '" + requestID + "'";
                        ret = NonSelectCommand(query);
                        if (!ret)
                        {
                            logger.Exception(100, "query failed. query =" + query);
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Trace("UpdateExternalEvent :" + ex.Message);
                return false;
            }
            return true;
        }

        internal bool UpdateExternalEvent(ref NS_MESSENGER.sysExtSchedEvent evnt, string oldRequestID)
        {
            try
            {
                string completionUpdateStatus = "X";
                string completionUpdateMsg = "Completed sucessfully by event";
                bool ret = false;
                string query = "";
                string requetID = oldRequestID.Trim();

                if (requetID == "")
                    requetID = evnt.sRequestID.Trim();


                query = "Update ES_Event_D set [Status] = '" + evnt.sStatus + "',StatusDate = cast('" + evnt.dStatusDate.ToString() + "' as datetime),StatusMessage = N'" + evnt.sStatusMessage.Replace("'", " ") + "',ResponseCall = '" + evnt.sResponseCall.Replace("'", " ") + "',RequestCall = '" + evnt.sRequestCall.Replace("'", " ") + "',EventTrigger = '" + evnt.sEventTrigger + "', RetryCount = " + evnt.sRetryCount + " , RequestID = '" + evnt.sRequestID + "' Where RequestID = '" + requetID + "'"; //FB 2272

                ret = NonSelectCommand(query);
                if (!ret)
                {
                    logger.Exception(100, "query failed. query =" + query);
                    return false;
                }

                if (evnt.sStatus == "C")
                {
                    completionUpdateStatus = "X";
                    completionUpdateMsg += " " + evnt.sRequestID;

                    query = "Select RequestID from ES_Event_D where [Status] = 'E' and ConfNumName = " + evnt.sConfnumname;

                    DataSet ds = new DataSet();
                    string dsTable = "AllEvents";
                    ret = false;

                    ret = SelectCommand(query, ref ds, dsTable);
                    if (!ret)
                    {
                        logger.Exception(100, "SQL error");
                        return (false);
                    }

                    for (int i = 0; i < ds.Tables[dsTable].Rows.Count; i++)
                    {
                        string requestID = "";
                        ret = false;

                        requestID = ds.Tables[dsTable].Rows[i]["RequestID"].ToString().Trim();

                        query = "Update ES_Event_D set [Status] = '" + completionUpdateStatus + "', StatusMessage = '" + completionUpdateMsg + "' Where RequestID = '" + requestID + "'";
                        ret = NonSelectCommand(query);
                        if (!ret)
                        {
                            logger.Exception(100, "query failed. query =" + query);
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Trace("UpdateExternalEvent :" + ex.Message);
                return false;
            }
            return true;
        }

        #endregion

        #region UpdateConferenceforEvent
        /// <summary>
        /// UpdateConferenceforEvent
        /// </summary>
        /// <param name="evnt"></param>
        /// <param name="EStype"></param>
        /// <returns></returns>
        internal bool UpdateConferenceforEvent(ref NS_MESSENGER.sysExtSchedEvent evnt, string EStype)
        {
            try
            {
                bool ret = false;
                string query = "";
                string pushed = "1";

                if (evnt.sStatus == "E")
                    pushed = "2";


                query = "Update conf_conference_d set ESId = '" + evnt.sConfnumname + "',ESType = '" + EStype + "',PushedToExternal = " + pushed + " Where ConfNumName = " + evnt.sConfnumname.Trim() + "";

                ret = NonSelectCommand(query);
                if (!ret)
                {
                    logger.Exception(100, "query failed. query =" + query);
                    return false;
                }
            }
            catch (Exception ex)
            {
                logger.Trace("UpdateConferenceforEvent :" + ex.Message);
                return false;
            }
            return true;
        }
        #endregion
        //FB 2363 end

        //Polycom CMA
        public bool UpdateConferenceExternalID(int confid, int instanceid, string esID)
        {
            try
            {
                string query = " Update conf_conference_d set EsId = '" + esID + "', PushedToExternal=1 where confid = " + confid.ToString() + " and instanceid = " + instanceid.ToString(); //FB 2441
                bool ret = NonSelectCommand(query);
                if (!ret)
                {
                    logger.Exception(100, "Error executing query.");
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        //FB 2556 Start
        public bool UpdateConferenceExternalID(int confid, int instanceid, string esID,string esType,  string dialNumber) 
        {
            string query = "update Conf_Conference_D set EsId = '" + esID + "' , ESType = '" + esType + "' , PushedToExternal = 1 ";
            query += " , DialString = '" + dialNumber + "' where confid = '" + confid.ToString() + "'";
            query += " and instanceid = '" + instanceid.ToString() + "'";

            bool ret = NonSelectCommand(query);
            if (!ret)
            {
                logger.Exception(100, "Error executing query.");
                return false;
            }
            return true;
        }
        //FB 2556 End
        #region Get MCU preset and pre end
        /// <summary>
        /// FB 2440
        /// To set MCU pre start and pre end
        /// </summary>
        /// <param name="conf"></param>
        /// <returns>bool</returns>
        public bool GetandSetMCUStartandEnd(ref NS_MESSENGER.Conference conf)
        {
            DataSet ds = new DataSet();
            string query = "";
            string dsTable = "Times";
            bool ret = false;
            int iMCUSetup = 0, iMCUTearDown = 0, iDuration = 0;
            try
            {
                if (conf.etType != NS_MESSENGER.Conference.eType.POINT_TO_POINT && conf.iImmediate <= 0)
                {
                    //FB 2998 - start
                    /*
                    query = "Select McuSetupTime,MCUTeardonwnTime  from org_settings_d where orgID = " + conf.iOrgID.ToString();

                    ret = SelectCommand(query, ref ds, dsTable);

                    if (!ret)
                    {
                        logger.Trace("SQL error");
                        return (false);
                    }

                    for (int i = 0; i < ds.Tables[dsTable].Rows.Count; i++)
                    {

                        Int32.TryParse(ds.Tables[dsTable].Rows[i]["McuSetupTime"].ToString().Trim(), out iMCUSetup);
                        Int32.TryParse(ds.Tables[dsTable].Rows[i]["MCUTeardonwnTime"].ToString().Trim(), out iMCUTearDown);
                    }
                    */

                    iMCUSetup = conf.iMcuSetupTime;
                    iMCUTearDown = conf.iMCUTeardonwnTime; //FB 2998 - End
                    iDuration = conf.iDuration;//ZD 100085
                    //iDuration = (int)conf.dtEndDateTime.Subtract(conf.dtStartDateTime).TotalMinutes;

                    conf.dtStartDateTime = conf.dtStartDateTime.Subtract(new TimeSpan(0, iMCUSetup, 0));
                    conf.dtStartDateTimeInLocalTZ = conf.dtStartDateTimeInLocalTZ.Subtract(new TimeSpan(0, iMCUSetup, 0));
                    conf.dtStartDateTimeInUTC = conf.dtStartDateTimeInUTC.Subtract(new TimeSpan(0, iMCUSetup, 0));
                    conf.iDuration = iDuration - iMCUTearDown + iMCUSetup;
                }
                ret = true;

            }
            catch (Exception ex)
            {

                logger.Exception(100, ex.Message);
            }

            return ret;

        }
        #endregion

        #region Set party properties
        /// <summary>
        /// FB 2400
        /// </summary>
        internal void SetPartyProperties(ref NS_MESSENGER.Party partyMulti, ref NS_MESSENGER.Party party)
        {

            partyMulti.iAPIPortNo = party.iAPIPortNo;
            partyMulti.bIsLecturer = party.bIsLecturer;
            partyMulti.sPwd = party.sPwd;
            partyMulti.iAPIPortNo = party.iAPIPortNo;
            partyMulti.isRoom = party.isRoom;
            partyMulti.iconfnumname = party.iconfnumname;
            partyMulti.iDbId = party.iDbId;
            partyMulti.iMcuId = party.iMcuId;
            partyMulti.sMcuAddress = party.sMcuAddress;
            partyMulti.etCallType = party.etCallType;
            partyMulti.bIsOutsideNetwork = party.bIsOutsideNetwork;
            partyMulti.sMcuServiceName = party.sMcuServiceName;
            partyMulti.etAddressType = party.etAddressType;
            partyMulti.etType = party.etType;
            partyMulti.etModelType = party.etModelType;
            partyMulti.stLineRate.etLineRate = party.stLineRate.etLineRate;
            partyMulti.etConnType = party.etConnType;
            partyMulti.cMcu = party.cMcu;
            partyMulti.Emailaddress = party.Emailaddress; //ZD 103080

        }
        #endregion

        /**  FB 2447 **/
        /// <summary>
        /// FB 2447
        /// </summary>
        /// <param name="addr">Address to validate</param>
        /// <returns>boolean</returns>
        public bool IsContainsIP(string addr)
        {
            string pattern = @"\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b";
            Regex check = new Regex(pattern);
            bool valid = false;
            try
            {
                if (addr == "")
                    valid = false;
                else
                    valid = check.Match(addr).Success;

            }
            catch (Exception)
            {

                valid = false;
            }

            return valid;
        }
        /**  FB 2447 **/

        //FB 2448 start
        #region FetchConfMcus
        /// <summary>
        /// FetchConfMcus
        /// </summary>
        /// <param name="confId"></param>
        /// <param name="instanceId"></param>
        /// <param name="mcus"></param>
        /// <returns></returns>
        internal bool FetchConfMcus(int confId, int instanceId, ref List<NS_MESSENGER.MCU> mcus)
        {
            try
            {
                mcus = new List<NS_MESSENGER.MCU>();
                DataSet ds = new DataSet();
                string dsTable = "AllRooms";
                int bridgeId = 0;

                // Fetch which bridge the room endpoint is on.
                string query = " select cr.bridgeid from Conf_Conference_D c, Conf_Room_D cr where"
                            + " ( c.confid = cr.ConfID and c.instanceid = cr.instanceID )"
                            + " and cr.confid = " + confId.ToString()
                            + " and cr.instanceid = " + instanceId.ToString()
                            + " union"
                            + " select cu.bridgeid from Conf_Conference_D c, Conf_User_D cu where"
                            + " ( c.confid = cu.ConfID and c.instanceid = cu.instanceID )"
                            + " and cu.confid = " + confId.ToString()
                            + " and cu.instanceid = " + instanceId.ToString(); 

                bool ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Exception(100, "SQL error");
                    return (false);
                }

                NS_MESSENGER.MCU mcu = null;
                for (int i = 0; i < ds.Tables[dsTable].Rows.Count; i++)
                {
                    mcu = new NS_MESSENGER.MCU();
                    // BridgeID which is assigned to that endpoint
                    if(ds.Tables[dsTable].Rows[i]["bridgeid"] != null)
                        Int32.TryParse(ds.Tables[dsTable].Rows[i]["bridgeid"].ToString(), out bridgeId);

                    // Fetch the Bridge info  
                    if (bridgeId < 1)
                        continue;

                    bool ret2 = FetchMcu(bridgeId, ref mcu);
                    if (!ret2)
                    {
                        logger.Trace("Error in fetching MCU info");
                        return (false);
                    }
                    mcus.Add(mcu);
                }
                return (true);
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return (false);
            }

        }
        #endregion

        #region UpdateMcuCurrentTime
        /// <summary>
        /// UpdateMcuCurrentTime
        /// </summary>
        /// <param name="mcuDbId"></param>
        /// <param name="currentTime"></param>
        /// <returns></returns>
        internal bool UpdateMcuCurrentTime(int mcuDbId,DateTime currentTime)
        {
            try
            {
                string query = "update Mcu_List_D set CurrentDateTime = '" + currentTime.ToString() + "' where BridgeID = " + mcuDbId;
                bool ret = NonSelectCommand(query);
                if (!ret)
                {
                    logger.Trace("SQL error");
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion
        //FB 2448 end

        //FB 2543 Starts
        #region GetOrgTechInfo
        /// <summary>
        /// GetOrgTechInfo
        /// </summary>
        /// <param name="orgid"></param>
        /// <param name="emailBody"></param>
        internal void GetOrgTechInfo(int orgid, ref string contact, ref string emailid, ref string phone)
        {
            try
            {
                DataSet ds = new DataSet();
                string dsTable = "SysTechInfo";
                string query = "";
                contact = ""; emailid = ""; phone = "";
                query = "select [Name], email, phone from Sys_TechSupport_D where orgid=" + orgid;

                bool ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Trace("SQL error");
                }
                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            if (ds.Tables[0].Rows[0]["Name"] != null)
                            {
                                contact = ds.Tables[0].Rows[0]["Name"].ToString().Trim();
                                contact = contact.Replace("||", "\"").Replace("!!", "'");
                            }
                            if (ds.Tables[0].Rows[0]["email"] != null)
                            {
                                emailid = ds.Tables[0].Rows[0]["email"].ToString().Trim();
                            }
                            if (ds.Tables[0].Rows[0]["phone"] != null)
                            {
                                phone = ds.Tables[0].Rows[0]["phone"].ToString().Trim();
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                //return "";
            }
        }
        #endregion
        //FB 2543 Ends

        //FB 2501 Call Monitoring Start

        #region UpdateConference
        /// <summary>
        /// UpdateConferenceStatus
        /// </summary>
        /// <param name="confid"></param>
        /// <param name="instanceid"></param>
        /// <param name="confStatus"></param>
        /// <returns></returns>
        public bool UpdateConference(int confid, int instanceid, string confGUID, NS_MESSENGER.Conference.eStatus confStatus, String ConfURI)//ZD 100085 //ZD 101217
        {
            //ZD 100608 Starts
            int iConfStatus = 0;
            if (confStatus == NS_MESSENGER.Conference.eStatus.SCHEDULED)
                iConfStatus = 0;
            if (confStatus == NS_MESSENGER.Conference.eStatus.PENDING)
                iConfStatus = 1;
            if (confStatus == NS_MESSENGER.Conference.eStatus.TERMINATED)
                iConfStatus = 3;
            if (confStatus == NS_MESSENGER.Conference.eStatus.ONGOING)
                iConfStatus = 5;
            if (confStatus == NS_MESSENGER.Conference.eStatus.OnMCU)
                iConfStatus = 6;
            if (confStatus == NS_MESSENGER.Conference.eStatus.COMPLETED)
                iConfStatus = 7;
            if (confStatus == NS_MESSENGER.Conference.eStatus.DELETED)
                iConfStatus = 9;
            //ZD 100608 Ends
            string query = "update Conf_Conference_D set GUID = '" + confGUID + "', Status = " + iConfStatus + ""; //ZD 100085 //ZD 100608
            if (ConfURI != "") //ZD 101217
                query += " , ConfURI = '" + ConfURI + "'";
            query += " where confid = " + confid.ToString();
            query += " and instanceid = " + instanceid.ToString();

            bool ret = NonSelectCommand(query);
            if (!ret)
            {
                logger.Exception(100, "Error executing query.");
                return false;
            }
            return true;
        }

		//ZD 100152 Starts
        #region UpdateConference
        /// <summary>
        /// UpdateConferenceStatus
        /// </summary>
        /// <param name="confid"></param>
        /// <param name="instanceid"></param>
        /// <param name="confStatus"></param>
        /// <returns></returns>
        public bool UpdateConferenceGoogleid(int _confid, int _instanceid, string _confGUID, int _GoogleSequence, string _UpdatedDateTime)
        {
           
            try
            {
                if (_confGUID.Contains('_'))
                    _confGUID = _confGUID.Split('_')[0];
                string query = "update Conf_Conference_D set GoogleGUID = '" + _confGUID + "',GoogleSequence=" + _GoogleSequence + ",GoogleConfLastUpdated='" + _UpdatedDateTime + "'";
                query += " where confid = " + _confid.ToString();

                bool ret = NonSelectCommand(query);
                if (!ret)
                {
                    logger.Exception(100, "Error executing query.");
                    return false;
                }
            }
            catch (Exception ex)
            {
                logger.Trace("Error in UpdateConferenceGoogleid");
                return false;
            }
            return true;
        }
        #endregion

        #region GetGoogleConference
        /// <summary>
        /// GetGoogleConference
        /// </summary>
        /// <param name="_confGUID"></param>
        /// <param name="_userID"></param>
        /// <param name="ds"></param>
        /// <returns></returns>
        public bool GetGoogleConference(string _confGUID, string _userID, DateTime Lastrundate, ref DataSet ds) //ALLDEV-856
        {
            string query = "";
            string[] _ConfGuidArr = null;

            try
            {
                string dsTable = "Conference";

                if (!string.IsNullOrEmpty(_confGUID)) //ALLDEV-856
                {
                    if (_confGUID.Contains('_')) // for instance edit from the google  Google GUID comes with _ with Date
                    {
                        _ConfGuidArr = _confGUID.Split('_');
                        query = "select confid as confid,instanceid as instanceid,googleconflastupdated as GoogleConfLastUpdated,recuring as recur, GoogleGuid, orgId from conf_conference_d "; //ALLDEV-856
                        query += " where GoogleGuid = '" + _ConfGuidArr[0] + "' and confdate='" + DateTime.Parse(_ConfGuidArr[1].Substring(0, 4) + "-" + _ConfGuidArr[1].Substring(4, 2) + "-" + _ConfGuidArr[1].Substring(6, 2) + " " + _ConfGuidArr[1].Substring(8, 1) + " " + _ConfGuidArr[1].Substring(9, 2) + ":" + _ConfGuidArr[1].Substring(11, 2) + ":45").ToString("yyyy-MM-dd HH:mm:ss") + "' and deleted=0";
                    }
                    else
                    {
                        query = "select confid as confid,instanceid as instanceid,GoogleConfLastUpdated,recuring as recur, GoogleGuid, orgId from conf_conference_d "; //ALLDEV-856
                        query += " where GoogleGuid = '" + _confGUID.Trim() + "' and deleted=0";
                    }
                }
                //ALLDEV-856 Start
                else
                {
                    query = "select confid as confid,instanceid as instanceid,GoogleConfLastUpdated,recuring as recur, GoogleGuid, orgId from conf_conference_d ";
                    query += " where LastRunDateTime >= '" + Lastrundate + "' and deleted=0";
                }
                //ALLDEV-856 End
                bool ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Exception(100, "Error executing query.");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                logger.Exception(100, ex.Message);
                return false;
            }
        }
        #endregion

        #region CheckGooglePoll
        /// <summary>
        /// CheckGooglePoll
        /// </summary>
        /// <param name="userid"></param>
        /// <param name="pollstatus"></param>
        /// <returns></returns>
        public bool CheckGooglePoll(int userid, ref int pollstatus)
        {
            SqlConnection con = null;
            DataSet ds = null;
            bool ret = false;
            try
            {
                con=new SqlConnection(conStr);
                con.Open();
                string stmt = "select PollStatus from temp_GooglePoll_d where userid=" + userid;
                ds = new DataSet();

                ret = SelectCommand(stmt, ref ds, "GooglePoll");
                if (!ret)
                {
                    logger.Exception(100, "Error executing query.");
                    return false;
                }

                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        if (ds.Tables[0].Rows[0]["PollStatus"] == "0")
                        {
                            stmt = "delete temp_GooglePoll_d where userid=" + userid + "; insert into temp_GooglePoll_d (userid,PollStatus) values (" + userid + ",1)";
                             ret = NonSelectCommand(stmt);
                            pollstatus = 0;
                        }
                        else
                            pollstatus = 1;
                    }
                    else
                    {
                        stmt = "insert into temp_GooglePoll_d (userid,PollStatus) values (" + userid + ",1)";
                        ret = NonSelectCommand(stmt);
                        pollstatus = 0;
                    }

                }
                else
                    pollstatus = 1;

                con.Close();
                return true;
            }
            catch (Exception ex)
            {
                
                con.Close();
                logger.Trace("Exception  CheckGooglePoll" + ex.Message);
                return false;
            }
        }
        #endregion

        #region DeleteCheckGooglePoll
        /// <summary>
        /// DeleteCheckGooglePoll
        /// </summary>
        /// <param name="userid"></param>
        /// <returns></returns>
        public bool DeleteCheckGooglePoll(int userid)
        {
            string query = "";
            try
            {
                query = "delete temp_GooglePoll_d where userid=" + userid;
                bool ret = NonSelectCommand(query);
                return true;
            }
            catch (Exception ex)
            {
                logger.Trace(" DeleteCheckGooglePoll Block" + ex.Message);
                return false;
            }
        }
        #endregion
        //ZD 100152 Ends


		//FB 2659 - Starts
        public bool UpdateConference(int confid, int instanceid, string confGUID, string dialNumber)
        {
            string query = "update Conf_Conference_D set GUID = '" + confGUID + "' , PushedToExternal = 1 ";
            query += " where confid = '" + confid.ToString() + "'";
            query += " and instanceid = '" + instanceid.ToString() + "'";


            query += "  update conf_bridge_d set e164dialnumber = '" + dialNumber + "'";
            query += " where confid = '" + confid.ToString() + "'";
            query += " and instanceid = '" + instanceid.ToString() + "'";


            bool ret = NonSelectCommand(query);
            if (!ret)
            {
                logger.Exception(100, "Error executing query.");
                return false;
            }
            return true;
        }
		//FB 2659 - End
        #endregion

        #region Update conferenceEndpointGUID
        /// <summary>
        /// UpdateConferenceEndpointStatus
        /// </summary>
        /// <param name="confid"></param>
        /// <param name="instanceid"></param>
        /// <param name="address"></param>
        /// <param name="eID"></param>
        /// <returns></returns>
        public bool UpdateConferenceEndpointStatus(int confid, int instanceid,string address, string eID)
        {
            string query = ""; string queryupdt = "";
            string tempQuery = ""; string tempqueryupdt = "";
            DataSet ds = new DataSet();
            string dsTable = "endpoint";
            bool ret = false;
            string eptID = "";
            string tblName = "";
            try
            {
                query = "Select Uid from {0} where {1} confid = " + confid.ToString() + " and instanceid = " + instanceid.ToString() + " and ipisdnaddress = '" + address.Trim() + "'";
                queryupdt = "update {0} set GUID = '" + eID + "' where UID = {1}";
                
                tblName = "conf_room_d";
                tempQuery = string.Format(query, tblName, "");
                ret = SelectCommand(tempQuery, ref ds, dsTable);
                if (!ret)
                {
                    logger.Trace("SQL error");
                    return (false);
                }

                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[dsTable].Rows.Count > 0)
                    {
                        if (ds.Tables[dsTable].Rows[0]["Uid"] != null && ds.Tables[dsTable].Rows[0]["Uid"].ToString() != "")
                        {
                            eptID = ds.Tables[dsTable].Rows[0]["Uid"].ToString();
                            tempqueryupdt = string.Format(queryupdt, tblName, eptID);
                            ret = NonSelectCommand(tempqueryupdt);
                            if (!ret)
                            {
                                logger.Exception(100, "Error executing query.");
                                return false;
                            }

                            return true;
 
                        }
                    }
                }

                tblName = "conf_user_d";
                tempQuery = string.Format(query, tblName, " invitee = 1 and ");
                ret = SelectCommand(tempQuery, ref ds, dsTable);
                if (!ret)
                {
                    logger.Trace("SQL error");
                    return (false);
                }

                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[dsTable].Rows.Count > 0)
                    {
                        if (ds.Tables[dsTable].Rows[0]["Uid"] != null && ds.Tables[dsTable].Rows[0]["Uid"].ToString() != "")
                        {
                            eptID = ds.Tables[dsTable].Rows[0]["Uid"].ToString();
                            tempqueryupdt = string.Format(queryupdt, tblName, eptID);
                            ret = NonSelectCommand(tempqueryupdt);
                            if (!ret)
                            {
                                logger.Exception(100, "Error executing query.");
                                return false;
                            }

                            return true;

                        }
                    }
                }
                
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
            return true;
        }
        #endregion

        #region UpdateparticipantStatus
        /// <summary>
        /// participantDiagnosticsResponse
        /// </summary>
        /// <param name="confid"></param>
        /// <param name="instanceid"></param>
        /// <param name="party"></param>
        /// <returns></returns>
        public bool UpdateparticipantStatus(NS_MESSENGER.Party party,int confid, int instanceid ) //ZD 101056
        {
            string query = "";

            try
            {
                if (party.etType == NS_MESSENGER.Party.eType.GUEST || party.etType == NS_MESSENGER.Party.eType.USER)
                {
                    query = " Update conf_user_d set RxAudioPacketsReceived = '" + party.sRxAudioPacketsReceived + "', TxAudioPacketsSent = '" + party.sTxAudioPacketsSent + "', TxVideoPacketsSent = '" + party.sTxVideoPacketsSent + "', ";//ZD 100632
                    query += " RxAudioPacketErrors = '" + party.sRxAudioPacketErrors + "', stream = '" + party.sStream + "', RxAudioPacketsMissing = '" + party.sRxAudioPacketsMissing + "', "; //FB 2640
                    query += " RxVideoPacketsReceived = '" + party.sRxVideoPacketsReceived + "', RxVideoPacketErrors = '" + party.sRxVideoPacketErrors + "', RxVideoPacketsMissing = '" + party.sRxVideoPacketsMissing + "', ";
                    query += " mute = '" + Convert.ToInt32(party.bMute) + "', MuteRxaudio = '" + Convert.ToInt32(party.bMuteRxaudio) + "', MuteRxvideo = '" + Convert.ToInt32(party.bMuteRxvideo) + "', activeSpeaker = '" + Convert.ToInt32(party.bactiveSpeaker) + "',Partynameonmcu='" + party.sMcuName + "'";//FB 2971 //ZD 100174 //ZD 101056
                    query += " where IPISDNAddress = '" + party.sAddress + "' and confid=" + confid + " and instanceid=" + instanceid;//ZD 101056
                    if (party.sGUID != "")
                        query += " and GUID = '" + party.sGUID + "'";

                }
                else if (party.etType == NS_MESSENGER.Party.eType.ROOM)
                {
                    query = " Update conf_room_d set RxAudioPacketsReceived = '" + party.sRxAudioPacketsReceived + "', TxAudioPacketsSent = '" + party.sTxAudioPacketsSent + "', TxVideoPacketsSent = '" + party.sTxVideoPacketsSent + "', ";//ZD 100632
                    query += " RxAudioPacketErrors = '" + party.sRxAudioPacketErrors + "', stream = '" + party.sStream + "', RxAudioPacketsMissing = '" + party.sRxAudioPacketsMissing + "', "; //FB 2640
                    query += " RxVideoPacketsReceived = '" + party.sRxVideoPacketsReceived + "', RxVideoPacketErrors = '" + party.sRxVideoPacketErrors + "', RxVideoPacketsMissing = '" + party.sRxVideoPacketsMissing + "', ";
                    query += " mute = '" + Convert.ToInt32(party.bMute) + "', MuteRxaudio = '" + Convert.ToInt32(party.bMuteRxaudio) + "', MuteRxvideo = '" + Convert.ToInt32(party.bMuteRxvideo) + "', activeSpeaker = '" + Convert.ToInt32(party.bactiveSpeaker) + "',Partynameonmcu='" + party.sMcuName + "'";//FB 2971 //ZD 100174 // ZD 101056
                    query += " where IPISDNAddress = '" + party.sAddress + "' and confid=" + confid + " and instanceid=" + instanceid;//ZD 101056
                    if (party.sGUID != "")
                        query += " and GUID = '" + party.sGUID + "'";
                }
                else if (party.etType == NS_MESSENGER.Party.eType.CASCADE_LINK)
                {
                    query = " Update Conf_Cascade_D set RxAudioPacketsReceived = '" + party.sRxAudioPacketsReceived + "', TxAudioPacketsSent = '" + party.sTxAudioPacketsSent + "', TxVideoPacketsSent = '" + party.sTxVideoPacketsSent + "', ";//ZD 100632
                    query += " RxAudioPacketErrors = '" + party.sRxAudioPacketErrors + "', RxAudioPacketsMissing = '" + party.sRxAudioPacketsMissing + "', "; //FB 2640 commented for 100174 stream = '" + party.sStream + "',
                    query += " RxVideoPacketsReceived = '" + party.sRxVideoPacketsReceived + "', RxVideoPacketErrors = '" + party.sRxVideoPacketErrors + "', RxVideoPacketsMissing = '" + party.sRxVideoPacketsMissing + "', ";
                    query += " mute = '" + Convert.ToInt32(party.bMute) + "'";//FB 2971 //commented for ZD 100174 , MuteRxaudio = '" + Convert.ToInt32(party.bMuteRxaudio) + "', MuteRxvideo = '" + Convert.ToInt32(party.bMuteRxvideo) + "', activeSpeaker = '" + Convert.ToInt32(party.bactiveSpeaker) + "'// ZD 101056
                    query += " where IPISDNAddress = '" + party.sAddress + "' and confid=" + confid + " and instanceid=" + instanceid; //ZD 101056
                    if (party.sGUID != "")
                        query += " and GUID = '" + party.sGUID + "'";
                }
                bool ret = NonSelectCommand(query);
                if (!ret)
                {
                    logger.Exception(100, "Error executing query." + query);
                    return false;
                }
            }
            catch (Exception ex)
            {
                logger.Trace("UpdateConfPartyDetails :" + ex.Message);
            }
            return true;
        }

        #endregion

        #region FetchPartyTerminalTypeUsingGUID
        /// <summary>
        /// FetchPartyTerminalTypeUsingGUID
        /// </summary>
        /// <param name="confid"></param>
        /// <param name="instanceid"></param>
        /// <param name="party"></param>
        /// <returns></returns>
        public bool FetchPartyTerminalTypeUsingGUID(NS_MESSENGER.Party party)
        {
            string query = "";

            try
            {
                DataSet ds = new DataSet();
                string dsTable = "Users";

                query = "select terminaltype from Conf_User_D ";
                query += " where GUID = " + party.sGUID;
                bool ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Trace("SQL error");
                }
                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {

                        if (ds.Tables.Count > 0)
                        {
                            party.iTerminalType = Int32.Parse(ds.Tables[dsTable].Rows[0]["terminaltype"].ToString().Trim());
                        }
                    }
                }

                ds = new DataSet();
                dsTable = "Rooms";

                query = "select terminaltype from Conf_Room_D ";
                query += " where GUID = " + party.sGUID;
                ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Trace("SQL error");
                }
                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {

                        if (ds.Tables.Count > 0)
                        {
                            party.iTerminalType = Int32.Parse(ds.Tables[dsTable].Rows[0]["terminaltype"].ToString().Trim());
                        }
                    }
                }
                ds = new DataSet();
                dsTable = "Cascade";

                query = "select terminaltype from Conf_Cascade_D ";
                query += " where GUID = " + party.sGUID;
                ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Trace("SQL error");
                }
                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {

                        if (ds.Tables.Count > 0)
                        {
                            party.iTerminalType = Int32.Parse(ds.Tables[dsTable].Rows[0]["terminaltype"].ToString().Trim());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Trace("UpdateConfPartyDetails :" + ex.Message);
            }
            return true;
        }

        #endregion

        #region FetchPartyStatus
        /// <summary>
        /// UpdateConfPartyDetails
        /// </summary>
        /// <param name="party"></param>
        /// <returns></returns>
        bool FetchPartyStatus(int onlineStatus,ref NS_MESSENGER.Party party)
        {
            try
            {
                if (onlineStatus == 2)
                {
                    party.etStatus = NS_MESSENGER.Party.eOngoingStatus.FULL_CONNECT;//Connected - Blue Status
                }
                else
                {
                    if (onlineStatus == 0)
                    {
                        party.etStatus = NS_MESSENGER.Party.eOngoingStatus.DIS_CONNECT; //Disconnected
                    }
                    else if (onlineStatus == 1)
                    {
                        party.etStatus = NS_MESSENGER.Party.eOngoingStatus.PARTIAL_CONNECT; //Connecting
                    }
                    else if (onlineStatus == 3)
                    {
                        party.etStatus = NS_MESSENGER.Party.eOngoingStatus.ONLINE; //Online
                    }
                    else
                    {
                        party.etStatus = NS_MESSENGER.Party.eOngoingStatus.UNREACHABLE; //Unreachable 
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Trace("FetchPartyStatus :" + ex.Message);
            }
            return true;
        }
        #endregion

        //FB 2501 Dec6
        #region UpdateMcuInfo
        /// <summary>
        /// UpdateMcuInfo
        /// </summary>
        /// <param name="mcuDbId"></param>
        /// <param name="currentTime"></param>
        /// <returns></returns>
        internal bool UpdateMcuInfo(int mcuDbId, int portsVideoTotal, int portsVideoFree, int portsAudioTotal, int portsAudioFree,int status)
        {
            try
            {
                string query = "update Mcu_List_D set portsVideoTotal = " + portsVideoTotal + ",portsVideoFree = " + portsVideoFree + ", ";
                query += "portsAudioTotal = " + portsAudioTotal + ", portsAudioFree = " + portsAudioFree + ", status = " + status + " where BridgeID = " + mcuDbId;
                bool ret = NonSelectCommand(query);
                if (!ret)
                {
                    logger.Trace("SQL error");
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        //ZD 100369_MCU Start
        internal bool UpdateMcuStatus(NS_MESSENGER.MCU mcu, int status)
        {
            NS_EMAIL.Email Email = new NS_EMAIL.Email(configParams);
            List<string> Emailaddress = new List<string>();
            string query = "";
            try
            {
                int count = mcu.iPollCount + status;
                //Code corrected to during ZD 104852 starts to fix the issue that email not send to last person
                string[] delimiter = { ";" };
                string[] MultipleAddress = mcu.sMultipleAssistant.Trim().Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
                //Code corrected to during ZD 104852 ends

                if (count == -2 || count == -1 || count == 0)
                {
                    query = "update Mcu_List_D set PollCount = " + count + " where BridgeID = " + mcu.iDbId;
                    bool ret = NonSelectCommand(query);
                    if (!ret)
                    {
                        logger.Trace("SQL error");
                        return false;
                    }

                    if (count == -2 || count == 0)
                    {
                        Emailaddress = new List<string>();


                        Emailaddress.Add(mcu.sAdminEmail);

                        for (int i = 0; i < MultipleAddress.Length; i++) //Corrected during ticket ZD 104852
                        {
                            Emailaddress.Add(MultipleAddress[i]);
                        }

                        if (count == -2)
                        {
                            ret = false;
                            ret = Email.SendFailureEmailtoMCUAdmin(mcu.sName, mcu.iOrgId, Emailaddress);
                            if (!ret)
                            {
                                logger.Trace("Error in Sending Email");
                                return false;
                            }
                        }
                        else if (count == 0)
                        {
                            ret = false;
                            ret = Email.SendSuccessEmailtoMCUAdmin(mcu.sName, mcu.iOrgId, Emailaddress);
                            if (!ret)
                            {
                                logger.Trace("Error in Sending Email");
                                return false;
                            }
                        }
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        //ZD 100369_MCU End
        #endregion

        //FB 2501 Call Monitoring End

        
        #region UpdateConfPartyDetails
        /// <summary>
        /// UpdateConfPartyDetails
        /// </summary>
        /// <param name="party"></param>
        /// <returns></returns>
        public bool UpdateConfPartyDetails(NS_MESSENGER.Party party)
        {
            string query = "", confID = "", instanceID = "", terminalTpye = "";
            int onlineStatus = -1, calltype = 0;
            bool ret = false;
            try
            {
                GetConferenceIDfromParty(ref confID, ref instanceID, ref terminalTpye, party.sGUID);

                if (party.etStatus == NS_MESSENGER.Party.eOngoingStatus.FULL_CONNECT)
                {
                    onlineStatus = 2; //Connected - Blue Status
                }
                else
                {
                    if (party.etStatus == NS_MESSENGER.Party.eOngoingStatus.DIS_CONNECT)
                    {
                        onlineStatus = 0; //Disconnected
                    }
                    else if (party.etStatus == NS_MESSENGER.Party.eOngoingStatus.PARTIAL_CONNECT)
                    {
                        onlineStatus = 1; //Connecting
                    }
                    else if (party.etStatus == NS_MESSENGER.Party.eOngoingStatus.ONLINE)
                    {
                        onlineStatus = 3; //Online
                    }
                    else
                    {
                        onlineStatus = -1; //Unreachable 
                    }
                }

                if (party.etCallType == NS_MESSENGER.Party.eCallType.AUDIO)
                {
                    calltype = 1;
                }
                else if (party.etCallType == NS_MESSENGER.Party.eCallType.VIDEO)
                {
                    calltype = 2;
                }

                if (terminalTpye == "User")
                {
                    query = " Update conf_user_d set onlinestatus = " + onlineStatus + ", ";
                    query += " AudioOrVideo = " + calltype + ", mute = '" + Convert.ToInt32(party.bMute) + "', ";
                    query += " MuteRxaudio = '" + Convert.ToInt32(party.bMuteRxaudio) + "', MuteRxvideo = '" + Convert.ToInt32(party.bMuteRxvideo) + "', MuteTxvideo = '" + Convert.ToInt32(party.bMuteTxvideo) + "', ";
                    query += " stream = '" + party.sStream + "', setfocus = '" + Convert.ToInt32(party.bSetFocus) + "'";
                    query += " where confid = '" + confID.ToString() + "' and Instanceid = '" + instanceID + "' and IPISDNAddress = '" + party.sAddress + "'";
                    if (party.sGUID != "")
                        query += " and GUID = '" + party.sGUID + "'";
                }
                else if (terminalTpye == "Room")
                {
                    query = " Update conf_room_d set onlinestatus = " + onlineStatus + ", ";
                    query += " AudioOrVideo = " + calltype + ", mute = '" + Convert.ToInt32(party.bMute) + "', ";
                    query += " MuteRxaudio = '" + Convert.ToInt32(party.bMuteRxaudio) + "', MuteRxvideo = '" + Convert.ToInt32(party.bMuteRxvideo) + "', MuteTxvideo = '" + Convert.ToInt32(party.bMuteTxvideo) + "',";
                    query += " stream = '" + party.sStream + "', setfocus = '" + Convert.ToInt32(party.bSetFocus) + "'";
                    query += " where confid = '" + confID.ToString() + "' and Instanceid = '" + instanceID + "' and IPISDNAddress = '" + party.sAddress + "'";
                    if (party.sGUID != "")
                        query += " and GUID = '" + party.sGUID + "'";
                }

                /*if (party.etType == NS_MESSENGER.Party.eType.CASCADE_LINK)
                    query = " Update Conf_Cascade_D set onlinestatus = " + party.etStatus + ", ";
                    query += " AudioOrVideo = " + party.etCallType + ", mute = " + party.bMute + ", ";
                    query += " MuteRxaudio = " + party.bMuteRxaudio + ", MuteRxvideo = " + party.bMuteRxvideo + ", MuteTxvideo = " + party.bMuteTxvideo + "";
                    query += " MuteRxaudio = " + party.bMuteRxaudio + ", stream = " + party.sStream + ", setfocus = " + party.bSetFocus + "";
                    query += " where confid = '" + confID.ToString() + "' and Instanceid = '" + instanceID + "' and IPISDNAddress = '" + party.sAddress + "'";
                    if(party.sGUID != "")
                       query += " and GUID = '" + party.sGUID + "'";*/
                if (query.Trim() != "")//ZD 101205
                    ret = NonSelectCommand(query);
                if (!ret)
                {
                    logger.Exception(100, "Error executing query." + query);
                    return false;
                }

            }
            catch (Exception ex)
            {
                logger.Trace("UpdateConfPartyDetails :" + ex.Message);
            }
            return true;
        }
        #endregion

        //FB 2501 P2P Call Monitoring Start

        #region InsertConfP2PAlert
        /// <summary>
        /// InsertConfP2PAlert
        /// </summary>
        /// <param name="alert"></param>
        /// <returns></returns>
        public bool InsertConfP2PAlert(NS_MESSENGER.Alert alert)
        {
            try
            {
                if (alert.typeID == 2)
                {
                    // do not insert "add party" alert
                    return false;
                }
                if (alert.message == null)
                {
                    logger.Trace("Invalid alert message.Message text is null.");
                    return false;
                }

                if (alert.message.Trim().Length < 2 || alert.confID == 0 || alert.instanceID == 0 || alert.typeID == 0)
                {
                    logger.Trace("Invalid alert message.");
                    return false;
                }

                string query = "DECLARE @typeid int";
                query += " DECLARE @confid int";
                query += " DECLARE @instanceid int";
                query += " SET @confid =" + alert.confID.ToString();
                query += " SET @instanceid =" + alert.instanceID.ToString();
                query += " SET @typeid =" + alert.typeID.ToString();
                query += " insert into Conf_P2PAlert_D values (@confid,@instanceid,@typeid,getutcdate(),'" + alert.message.Trim() + "')";

                bool ret = NonSelectCommand(query);
                if (!ret)
                {
                    logger.Exception(10, "Error executing query.");
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion
        
        #region InsertConfPolycomVideoURL
        /// <summary>
        /// InsertConfPolycomVideoURL
        /// </summary>
        /// <param name="conf"></param>
        /// <returns></returns>
        public bool InsertConfPolycomVideoURL(NS_MESSENGER.Conference conf)
        {
            try
            {
                string query = "";

                #region Fetch the endpoints

                bool ret = false;
                ret = FetchRooms(conf.iDbID, conf.iInstanceID, ref conf.qParties, 0);
                if (!ret)
                {
                    logger.Exception(100, "Error in fetching rooms from db.");
                    return false;
                }
                ret = false;
                ret = FetchUsers(conf.iDbID, conf.iInstanceID, ref conf.qParties, 0);
                if (!ret)
                {
                    logger.Exception(100, "Error in fetching users from db.");
                    return false;
                }
                ret = false;
                ret = FetchGuests(conf.iDbID, conf.iInstanceID, ref conf.qParties, 0);
                if (!ret)
                {
                    logger.Exception(100, "Error in fetching guests from db.");
                    return false;
                }
                ret = false;
                Queue cascadeLinks = new Queue();
                ret = FetchCascadeLinks(conf.iDbID, conf.iInstanceID, ref cascadeLinks, 0);
                if (!ret)
                {
                    logger.Exception(100, "Error in fetching cascade links from db.");
                    return false;
                }

                #endregion

                logger.Trace(" Entering Polycom Streaming Image ...");

                // Fetch all the bridge ids on which this conf is running.
                System.Collections.IEnumerator partyEnumerator;
                partyEnumerator = conf.qParties.GetEnumerator();
                int partyCount = conf.qParties.Count;
                logger.Trace("Party Count = " + partyCount.ToString());

                Queue mcuIdList = new Queue();
                bool ret3 = false;
                for (int i = 0; i < partyCount; i++)
                {
                    ret3 = partyEnumerator.MoveNext();
                    if (!ret3) break;

                    NS_MESSENGER.Party party = new NS_MESSENGER.Party();
                    party = (NS_MESSENGER.Party)partyEnumerator.Current;

                    query = "";

                    if (party.iTerminalType == 2 && (party.etModelType == NS_MESSENGER.Party.eModelType.POLYCOM_HDX || party.etModelType == NS_MESSENGER.Party.eModelType.POLYCOM_VSX))
                    {
                        query = "update Conf_Room_D set stream = 'http://" + party.sAddress.Trim() + "/a_tvmon.htm' where confid = " + conf.iDbID + " and instanceid = " + conf.iInstanceID + " and roomid = " + party.iDbId + " and terminaltype = " + party.iTerminalType + ")";
                    }
                    else if (party.etModelType == NS_MESSENGER.Party.eModelType.POLYCOM_HDX || party.etModelType == NS_MESSENGER.Party.eModelType.POLYCOM_VSX)
                    {
                        query = "update Conf_User_D set stream = 'http://" + party.sAddress.Trim() + "/a_tvmon.htm' where confid = " + conf.iDbID + " and instanceid = " + conf.iInstanceID + " and userid = " + party.iDbId + " and terminaltype = " + party.iTerminalType + ")";
                    }
                    ret = NonSelectCommand(query);
                    if (!ret)
                    {
                        logger.Exception(10, "Error executing query.");
                        return false;
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion

        //FB 2501 P2P Call Monitoring End

        //FB 2566 24 Dec,2012

        public bool FetchConfMcu(NS_MESSENGER.Conference conf, ref List<int> mcuq)
        {
            // Retreive  the  bridges
            DataSet ds = new DataSet();
            string dsTable = "Conference";
            string query = "Select c.BridgeID from Conf_Bridge_D c,Mcu_list_D m where confid = '" + conf.iDbID.ToString() + "' and instanceid = '" + conf.iInstanceID.ToString() + "' and m.BridgeID = c.BridgeID and m.Status = 1";  // ZD 100040 Virtual has been removed as it is now used for a different purpose under samrt MCU and m.VirtualBridge = 0";//ZD 103944
            bool ret = SelectCommand(query, ref ds, dsTable);
            if (!ret)
            {
                logger.Exception(10, "SQL error");
                return (false);
            }
            int mcu = 0;
            for (int i = 0; i < ds.Tables[dsTable].Rows.Count; i++)
            {
                try
                {
                    if (ds.Tables[dsTable].Rows[i]["BridgeID"] != null)
                    {
                        if (ds.Tables[dsTable].Rows[i]["BridgeID"].ToString() != "")
                        {
                            int.TryParse(ds.Tables[dsTable].Rows[i]["BridgeID"].ToString(), out mcu); // ZD 100221 change for try parse
                            mcuq.Add(mcu);
                        }
                    }
                }
                catch (Exception e)
                {
                    logger.Exception(100, e.Message);
                }
            }

            if (mcuq.Count == 0 && conf.iBJNConf == 1 && conf.iBJNMeetingType == (int)NS_MESSENGER.BJNMeetingType.OneTimeMeeting)
            {
                DataSet ds1 = new DataSet();
                string dsTable1 = "Access";
                string query1 = "select TOP(1) BridgeID from Mcu_List_D where BridgeTypeId = 20 and orgId = " + conf.iOrgID + " and deleted = 0 ";
                bool ret1 = SelectCommand(query1, ref ds1, dsTable1);
                if (!ret1)
                {
                    logger.Exception(100, "SQL error");
                    return (false);
                }
                else
                {
                    if (ds1.Tables[dsTable1].Rows.Count > 0)
                    {
                        for (int tbleCount = 0; tbleCount < ds1.Tables[dsTable1].Rows.Count; tbleCount++)
                        {
                            if (!string.IsNullOrEmpty(ds1.Tables[dsTable1].Rows[tbleCount]["BridgeID"].ToString()))
                                int.TryParse(ds1.Tables[dsTable1].Rows[tbleCount]["BridgeID"].ToString(), out mcu);

                            mcuq.Add(mcu);
                        }
                    }
                    else
                    {
                        logger.Trace("BJN is not configure Properly.");
                        return false;
                    }
                }
            }
            return true;
        }

        public bool FetchConfSychronousMcu(NS_MESSENGER.Conference conf, ref List<int> mcuq)//FB 2441
        {
            // Retreive  the  bridges
            DataSet ds = new DataSet();
            string dsTable = "Conference";
            string query = "Select BridgeID from Conf_Bridge_D where Synchronous = 1 and  confid = '" + conf.iDbID.ToString() + "' and instanceid = '" + conf.iInstanceID.ToString() + "'";//FB 2584
            bool ret = SelectCommand(query, ref ds, dsTable);
            if (!ret)
            {
                logger.Exception(10, "SQL error");
                return (false);
            }
            int mcu = 0;
            for (int i = 0; i < ds.Tables[dsTable].Rows.Count; i++)
            {
                try
                {
                    if (ds.Tables[dsTable].Rows[i]["BridgeID"] != null)
                    {
                        if (ds.Tables[dsTable].Rows[i]["BridgeID"].ToString() != "")
                        {
                            int.TryParse(ds.Tables[dsTable].Rows[i]["BridgeID"].ToString(), out mcu); // ZD 100221 change for try parse
                            mcuq.Add(mcu);
                        }
                    }
                }
                catch (Exception e)
                {
                    logger.Exception(100, e.Message);
                }
            }
            return true;
        }

        //FB 2591
        #region UpdateMcuProfile
        /// <summary>
        /// UpdateMcuProfile
        /// </summary>
        /// <param name="mcuDbId"></param>
        /// <param name="currentTime"></param>
        /// <returns></returns>
        internal bool UpdateMcuProfile(int mcuDbId,List<NS_MESSENGER.MCUProfile> MCUProfilelist)
        {
            string query = "";
            bool ret=false;
            NS_MESSENGER.MCUProfile MCUProfile = new NS_MESSENGER.MCUProfile();
            try
            {
                query = " delete from MCU_Profiles_D where MCUId = " + mcuDbId.ToString();
                ret = NonSelectCommand(query);
                if (!ret)
                {
                    logger.Exception(10, "Error executing query.");
                    return false;
                }
                for (int i = 0; i < MCUProfilelist.Count; i++)
                {
                    MCUProfile = MCUProfilelist[i];
                    query = " insert into MCU_Profiles_D (MCUId,ProfileId,ProfileName) values (" + mcuDbId.ToString() + "," + MCUProfile.iId.ToString() + ",'" + MCUProfile.sDisplayname + "')";
                    ret = NonSelectCommand(query);
                    if (!ret)
                    {
                        logger.Exception(10, "Error executing query.");
                        return false;
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion

        /**  FB 2599 - Start **/
        /**  FB 2262 **/

        #region FetchVidyoSetting
        /// <summary>
        /// FB 2262
        /// </summary>
        /// <param name="orgID"></param>
        /// <param name="vidyoSettings"></param>
        /// <returns>boolean</returns>
        internal bool FetchVidyoSettings(ref NS_MESSENGER.VidyoSettings vidyoSettings, string orgID)
        {
            cryptography.Crypto crypto = null;
            bool ret = false;
            DataSet ds = new DataSet();
            string dsTable = "VidyoSettings";
            string query = "";

            try
            {
                query = "select * from Vidyo_Settings_D where orgid = " + orgID;

                ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Exception(100, "SQL error");
                    return false;
                }

                if (ds.Tables[dsTable] != null && ds.Tables[dsTable].Rows.Count > 0)
                {
                    vidyoSettings = new NS_MESSENGER.VidyoSettings();

                    vidyoSettings.sPartnerURL = ds.Tables[dsTable].Rows[0]["VidyoURL"].ToString();
                    vidyoSettings.sUserName = ds.Tables[dsTable].Rows[0]["AdminLogin"].ToString();
                    crypto = new Crypto();
                    vidyoSettings.sPassword = crypto.decrypt(ds.Tables[dsTable].Rows[0]["Password"].ToString());
                    vidyoSettings.dModifiedDate = DateTime.Parse(ds.Tables[dsTable].Rows[0]["PollTime"].ToString());
                }
            }
            catch (Exception ex)
            {
                logger.Trace("FetchVidyoSettings:" + ex.StackTrace);
                return false;
            }
            return true;

        }
        #endregion

        #region SaveUpdateVidyoRooms
        /// <summary>
        /// SaveUpdateVidyoRooms
        /// </summary>
        /// <param name="room"></param>
        /// <returns></returns>
        internal bool SaveUpdateVidyoRooms(ref List<NS_Vidyo.VidyoUserRoom> rooms, ref string errMsg)
        {
            string query = "";
            bool ret = false;
            int myVRMLocationID = 0;
            int myVRMUserID = 0;
            int orgTopTierID = 0, OrgMiddleTierID = 0;
            int iEptID = -1;
            DateTime lastModified = DateTime.UtcNow;
            string resourceType = "roomID", resourceTable = "Loc_Department_D";
            int[] depts = null;
            try
            {
                FetchOrgRoomTiersName(ref orgTopTierID, ref OrgMiddleTierID);

                for (int i = 0; i < rooms.Count; i++)
                {
                    try
                    {
                        if (rooms[i] == null)
                            continue;

                        rooms[i] = (NS_Vidyo.VidyoUserRoom)SetFieldValues((object)rooms[i]);

                        myVRMLocationID = 0;
                        myVRMLocationID = GetmyVRMRoomID(rooms[i].iMemberID);

                        myVRMUserID = 0;
                        myVRMUserID = GetmyVRMUserID(rooms[i].iRoomID, rooms[i].iOwnerID);

                        if (myVRMLocationID > 0)
                        {
                            rooms[i].sDisplayName = Regex.Replace(rooms[i].sDisplayName, @"[\\<>^+?|!`\[\]{}\=@$%&~',]", " "); //ZD 103424
                            query = " Update Loc_Room_D Set Name = '" + rooms[i].sDisplayName
                                 + "', notifyemails = '" + rooms[i].sEmail
                                 + "', lastModifieddate = '" + lastModified
                                 + "', Extension = '" + rooms[i].sExtension
                                 + "', VidyoURL  = '" + rooms[i].sURL
                                 + "', Pin  = '" + rooms[i].sPin
                                 + "', isLocked  = '" + rooms[i].iLocked
                                 + "', allowCallDirect  = '" + rooms[i].iAllowDIrectCalls
                                 + "', allowPersonalMeeting  = '" + rooms[i].iPersonalMeeting
                                 + "', EntityID   = '" + rooms[i].iRoomID
                                 + "', MemberID  = '" + rooms[i].iMemberID
                                 + "', [Type] = '" + rooms[i].sRole
                                 + "', Country = 224"
                                 + ", IsVMR = '" + rooms[i].iVMR //FB 2262T
                                 + "', InternalNumber = '" + rooms[i].sExtension //FB 2262T
                                 + "', OwnerID = " + myVRMUserID //FB 2262T
                                 + " where RoomId =" + myVRMLocationID + " and MemberID  = " + rooms[i].iMemberID + " and RoomCategory = 5";//FB 2717
                        }
                        else
                        {

                            query = " INSERT INTO Loc_Room_D (Name,Capacity,TimezoneID,Assistant,videoAvailable,L3LocationId,L2LocationId,City,Caterer,ProjectorAvailable,DynamicRoomLayout,Servicetype,Extroom,DedicatedVideo,DedicatedCodec,setuptime,teardowntime,isVIP,AdminID,notifyemails,AVOnsiteSupportEmail,"
                                    + " lastModifieddate,MaxPhoneCall,Extension,VidyoURL,Pin,isLocked,allowCallDirect,allowPersonalMeeting,EntityID,MemberID,[Type],OrgID,OwnerID,IsVMR,InternalNumber, RoomCategory,roomicontypeid)" //FB 2262T //FB 2717 TCK #100141
                                    + " values ('" + rooms[i].sDisplayName + "','5'"
                                    + " ,26,'11',2," + orgTopTierID.ToString() + ","
                                    + OrgMiddleTierID.ToString() + ",'',0,0,0,-1,0,0,0,0,0,0,11,'"
                                    + rooms[i].sEmail + "','','" + lastModified + "',0,'" + rooms[i].sExtension + "','" + rooms[i].sURL + "','"
                                    + rooms[i].sPin + "','" + rooms[i].iLocked + "','" + rooms[i].iAllowDIrectCalls + "','" + rooms[i].iPersonalMeeting + "',"
                                    + " " + rooms[i].iRoomID + "," + rooms[i].iMemberID + ",'" + rooms[i].sRole + "'," + defaultOrgId + "," + myVRMUserID + ","
                                    + " " + rooms[i].iVMR + ",'" + rooms[i].sExtension + "',5,26)"; //loc_room values FB 2262T //FB 2717 TCK #100141

                        }


                        ret = NonSelectCommand(query);
                        if (!ret)
                        {
                            logger.Exception(100, "query failed. query =" + query);
                            return false;
                        }

                        if (myVRMLocationID <= 0)
                            myVRMLocationID = GetmyVRMRoomID(rooms[i].iMemberID);

                        iEptID = SaveUpdateVidyoRoomEndpoints(rooms[i], ref errMsg, myVRMLocationID);

                        query = " Update Loc_Room_D Set endpointid =" + iEptID + " where roomID =" + myVRMLocationID;
                        ret = NonSelectCommand(query);
                        if (!ret)
                        {
                            logger.Exception(100, "query failed. query =" + query);
                        }

                        depts = new int[] { deptList[rooms[i].sGroup] };

                        if (!SetDepartmentsforResource(ref depts, resourceTable, resourceType, myVRMLocationID))
                            logger.Exception(100, "Saving Department Failed");
                    }
                    catch (Exception ex)
                    {
                        logger.Trace("SaveUpdateVidyoRooms :" + ex.StackTrace);
                        errMsg = ex.Message;
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Trace("SaveUpdateVidyoRooms :" + ex.StackTrace);
                errMsg = ex.Message;
                return false;
            }
            return true;
        }
        #endregion

        #region SaveUpdateVidyoUsers
        /// <summary>
        /// SaveUpdateVidyoUsers
        /// </summary>
        /// <param name="room"></param>
        /// <returns></returns>
        internal bool SaveUpdateVidyoUsers(ref List<NS_Vidyo.VidyoUserRoom> users, ref string errMsg)
        {
            string query = "";
            bool ret = false;
            int myVRMUserID = 0;
            DateTime accountexpiry = DateTime.UtcNow.AddDays(180);
            string resourceType = "userID", resourceTable = "Usr_Dept_D";
            string firstName = "", lastName = "";
            int[] depts = null;
            string stmt = "";
            try
            {

                for (int i = 0; i < users.Count; i++)
                {
                    try
                    {
                        if (users[i] == null)
                            continue;

                        users[i] = (NS_Vidyo.VidyoUserRoom)SetFieldValues((object)users[i]);
                        firstName = "";
                        lastName = "";
                        firstName = users[i].sDisplayName.Split(' ')[0];
                        if (users[i].sDisplayName.Split(' ').Length > 1)
                            lastName = users[i].sDisplayName.Split(' ')[1];

                        if (lastName == "")//FB 2717
                            lastName = firstName;

                        if (users[i].sRole == "Public")
                            lastName += "(Public)";

                        if (users[i].sEmail.Trim() == "")
                            users[i].sEmail = users[i].sExtension + "@vidyo.com";

                        if (!users[i].sEmail.Contains("@"))//FB 2262T
                            users[i].sEmail = users[i].sEmail + "@vidyo.com";

                        myVRMUserID = 0;
                        myVRMUserID = GetmyVRMUserID(users[i].iRoomID, users[i].iMemberID);

                        if (myVRMUserID <= 0)
                            myVRMUserID = GetmyVRMUserID(users[i].sEmail);

                        if (myVRMUserID <= 0)
                        {

                            myVRMUserID = GetMaxUserID();

                            stmt = " insert into ";
                            stmt += "Usr_List_D";
                            stmt += " (userid,firstname,lastname,[password],timezone,PreferedRoom,PreferedGroup,CCGroup, ";
                            stmt += " email,WorkPhone,CellPhone,AlternativeEmail,DoubleEmail,DefLineRate,DefVideoProtocol,ConferenceCode,LeaderPin,admin,deleted,lockCntTrns,"; //FB 1642-Audio add on
                            stmt += " login, roleID ,EmailClient, newuser,ipisdnaddress, bridgeID, ";
                            stmt += " defaultEquipmentID,connectionType, language,EmailLangId, outsidenetwork, emailmask, addresstype, accountexpiry, searchId, endpointid";
                            stmt += " ,dateFormat";
                            stmt += " ,enableAV,enableAVWO,enableCateringWO,enableFacilityWO";//ZD 101122
                            stmt += " ,enableParticipants,timeFormat,timezoneDisplay";
                            stmt += " ,enableExchange,enableDomino";
                            stmt += " ,tickerStatus,tickerPosition,tickerSpeed,tickerBackground,tickerDisplay,rssFeedLink";
                            stmt += " ,tickerStatus1,tickerPosition1,tickerSpeed1,tickerBackground1,tickerDisplay1,rssFeedLink1,Audioaddon";
                            stmt += ", lockStatus";
                            stmt += ", companyid, enableMobile ,PluginConfirmations,InternalVideoNumber,ExternalVideoNumber,HelpReqEmailID,HelpReqPhone,SendSurveyEmail,InitialTime";
                            stmt += ", PasswordTime,PerCalStartTime,PerCalEndTime,RoomCalStartTime,RoomCalEndime,PerCalShowHours,RoomCalShowHours,lastmodifieddate)";
                            stmt += " values (" + myVRMUserID + ",'" + firstName + "','" + lastName + "','" + users[i].sPassword + "',26,'-1',0,0,";
                            stmt += " '" + users[i].sEmail + "','','','', 0,384,1";
                            stmt += ",'',''";
                            stmt += "," + users[i].iAdminID + ",0 ,0,'" + users[i].sName + "'," + users[i].iRoleID + ",2,1,'2',-1";
                            stmt += ",-1,1,1,'',";
                            stmt += "0,1,1, '" + accountexpiry + "',-1,0";
                            stmt += ", 'MM/dd/yyyy'";
                            stmt += ", '0', '0', '0', '0'";//ZD 101122
                            stmt += ", '1', '1', '1', '0', '0'";
                            stmt += ", '1', '0', '3','#99ff99', '0', '0'";
                            stmt += ", '1', '0', '3','#99ff99', '0', '0'";
                            stmt += ", '0'";
                            stmt += ", 0";
                            stmt += ",'" + defaultOrgId + "','0','0','','','','','0','150000'";
                            stmt += ",'" + DateTime.UtcNow + "','" + DateTime.UtcNow + "','" + DateTime.UtcNow + "','" + DateTime.UtcNow + "','" + DateTime.UtcNow + "',0,0,'" + DateTime.UtcNow + "')";

                            ret = NonSelectCommand(stmt);
                            if (!ret)
                            {
                                logger.Exception(100, "query failed. query =" + query);
                                return false;
                            }
                            //ZD 102043
                            stmt = " insert into Acc_Balance_D ( userid,Totaltime,TimeRemaining,UsedMinutes,ScheduledMinutes,Inittime,Expirationtime,orgID) ";
                            stmt += " values(" + myVRMUserID;
                            stmt += " ,150000,150000,0,0"; //ZD 102043
                            stmt += " , '" + DateTime.UtcNow + "','" + accountexpiry + "'," + defaultOrgId + ")";

                            stmt += "  UPDATE Usr_List_D SET menumask = (SELECT rolemenumask FROM ";
                            stmt += " Usr_Roles_D WHERE roleid = " + users[i].iRoleID + ") WHERE userid = " + myVRMUserID;

                            ret = NonSelectCommand(stmt);
                            if (!ret)
                            {
                                logger.Exception(100, "query failed. query =" + query);
                                return false;
                            }
                        }

                        query = " Update Usr_List_D Set FirstName = '" + firstName
                                 + "', Email = '" + users[i].sEmail
                                 + "', LastName = '" + lastName
                                  + "', Login = '" + users[i].sName
                                 + "', Extension = '" + users[i].sExtension
                                 + "', VidyoURL  = '" + users[i].sURL
                                 + "', Pin  = '" + users[i].sPin
                                 + "', isLocked  = '" + users[i].iLocked
                                 + "', allowCallDirect  = '" + users[i].iAllowDIrectCalls
                                 + "', allowPersonalMeeting  = '" + users[i].iPersonalMeeting
                                 + "', EntityID   = '" + users[i].iRoomID
                                 + "', MemberID  = '" + users[i].iMemberID
                                 + "', [Type] = '" + users[i].sRole
                                 + "' where userId =" + myVRMUserID;


                        ret = NonSelectCommand(query);
                        if (!ret)
                        {
                            logger.Exception(100, "query failed. query =" + query);
                            return false;
                        }

                        if (myVRMUserID <= 0)
                            myVRMUserID = GetmyVRMUserID(users[i].iRoomID, users[i].iMemberID);

                        depts = deptList.Values.ToArray();

                        if (users[i].iAdminID == 1)
                            depts = new int[] { deptList[users[i].sGroup] };

                        if (!SetDepartmentsforResource(ref depts, resourceTable, resourceType, myVRMUserID))
                            logger.Exception(100, "Saving Department Failed");

                    }
                    catch (Exception ex)
                    {
                        logger.Trace("SaveUpdateVidyoUsers :" + ex.StackTrace);
                        errMsg = ex.Message;
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Trace("SaveUpdateVidyoUsers :" + ex.StackTrace);
                errMsg = ex.Message;
                return false;
            }
            return true;
        }
        #endregion

        #region SaveUpdateVidyoRoomEndpoints
        /// <summary>
        /// SaveUpdateVidyoRoomEndpoints
        /// </summary>
        /// <param name="room"></param>
        /// <returns></returns>
        internal int SaveUpdateVidyoRoomEndpoints(NS_Vidyo.VidyoUserRoom room, ref string errMsg, int myVRMLocationID)
        {
            string query = "", epAddress = "", epAddressType = "", epConnType = "2", epLineRate = "";
            bool ret = false, refInsert = true;
            int iEptID = -1, iProfileID = 1, iIsDefault = 1, iRet = 0, epUID = -1;
            DataSet ds = new DataSet();
            string dsTable = "Ept_List_d";
            try
            {
                epAddress = "";
                epConnType = "2";
                iEptID = -1;
                epAddressType = "3";
                epLineRate = "1024";

                query = " Select UID,EndpointID,addresstype,address,linerateID from Ept_List_D"
                          + " where endpointId = ( select endpointid from loc_room_d where RoomId =" + myVRMLocationID + " )";

                ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Exception(10, "SQL error");
                    return (-1);
                }
                if (ds.Tables[dsTable] != null)
                {

                    if (ds.Tables[dsTable].Rows.Count > 0)
                    {
                        for (int rwcnt = 0; rwcnt < ds.Tables[dsTable].Rows.Count; rwcnt++)
                        {
                            if (iEptID <= 0)
                            {
                                if (ds.Tables[dsTable].Rows[rwcnt]["EndpointID"] != null) //FB 2717
                                    if (ds.Tables[dsTable].Rows[rwcnt]["EndpointID"].ToString().Trim() != "")
                                        Int32.TryParse(ds.Tables[dsTable].Rows[rwcnt]["EndpointID"].ToString().Trim(), out iEptID);
                            }

                            if (iEptID > 0)
                            {
                                if (ds.Tables[dsTable].Rows[rwcnt]["UID"] != null && ds.Tables[dsTable].Rows[rwcnt]["addresstype"] != null && ds.Tables[dsTable].Rows[rwcnt]["address"] != null && ds.Tables[dsTable].Rows[rwcnt]["linerateID"] != null)
                                {
                                    epAddress = ds.Tables[dsTable].Rows[rwcnt]["address"].ToString().Trim();
                                    epLineRate = ds.Tables[dsTable].Rows[rwcnt]["linerateID"].ToString().Trim();
                                    Int32.TryParse(ds.Tables[dsTable].Rows[rwcnt]["UID"].ToString().Trim(), out epUID);
                                    epAddressType = ds.Tables[dsTable].Rows[rwcnt]["addresstype"].ToString();

                                }
                            }
                        }
                    }
                }

                if (IsContainsIP(room.sExtension))
                    epConnType = "2";

                iRet = SaveUpdatePublicRoomEndpoints(ref epUID, ref iEptID, ref refInsert, epAddress, epLineRate, room.sDisplayName, room.sExtension, epAddressType, epLineRate, epConnType, ref iProfileID, ref iIsDefault, 0);//FB 2594

                if (iRet > 0)
                    iRet = iEptID;
            }
            catch (Exception ex)
            {
                logger.Trace("SaveUpdateVidyoRoomEndpoints :" + ex.StackTrace);
                errMsg = ex.Message;
                return -1;
            }
            return iRet;
        }
        #endregion

        #region GetPublicRoomID
        /// <summary>
        /// GetPublicRoomID
        /// </summary>
        /// <param name="myVRMLocationID"></param>
        /// <returns></returns>
        internal int GetPublicRoomID(int myVRMLocationID)
        {
            int bRet = -1;
            DataSet ds = new DataSet();
            string dsTable = "ES_PublicRoom_d";
            try
            {
                if (myVRMLocationID > 0)
                {
                    string query = "select whygoRoomID from ES_PublicRoom_d where RoomID = " + myVRMLocationID.ToString();

                    bool ret = SelectCommand(query, ref ds, dsTable);
                    if (!ret)
                    {
                        logger.Exception(10, "SQL error");
                        return (-1);
                    }

                    if (ds.Tables[dsTable].Rows.Count > 0)
                    {
                        if (ds.Tables[dsTable].Rows[0]["whygoRoomID"] != null)
                            if (ds.Tables[dsTable].Rows[0]["whygoRoomID"].ToString().Trim() != "")
                                Int32.TryParse(ds.Tables[dsTable].Rows[0]["whygoRoomID"].ToString().Trim(), out bRet);
                    }
                }

            }
            catch (Exception ex)
            {
                logger.Trace("GetPublicRoomID" + ex.Message);
                return -1;
            }
            return bRet;
        }
        #endregion

        #region GetmyVRMRoomID
        /// <summary>
        /// GetmyVRMRoomID
        /// </summary>
        /// <param name="publicRoomID"></param>
        /// <returns></returns>
        internal int GetmyVRMRoomID(int vidyoRoomID)
        {
            int bRet = -1;
            DataSet ds = new DataSet();
            string dsTable = "Loc_Room_d";
            try
            {
                if (vidyoRoomID > 0)
                {
                    string query = " select RoomID from Loc_Room_d where MemberID = " + vidyoRoomID.ToString();

                    bool ret = SelectCommand(query, ref ds, dsTable);
                    if (!ret)
                    {
                        logger.Exception(10, "SQL error");
                        return (-1);
                    }

                    if (ds.Tables[dsTable].Rows.Count > 0)
                    {
                        if (ds.Tables[dsTable].Rows[0]["RoomID"] != null)
                            if (ds.Tables[dsTable].Rows[0]["RoomID"].ToString().Trim() != "")
                                Int32.TryParse(ds.Tables[dsTable].Rows[0]["RoomID"].ToString().Trim(), out bRet);
                    }
                }

            }
            catch (Exception ex)
            {
                logger.Trace("GetmyVRMRoomID" + ex.Message);
                return -1;
            }
            return bRet;
        }
        #endregion

        #region GetmyVRMUserID
        /// <summary>
        /// GetmyVRMUserID
        /// </summary>
        /// <param name="vidyoRoomID"></param>
        /// <param name="vidyoMemberID"></param>
        /// <returns></returns>
        internal int GetmyVRMUserID(int vidyoRoomID, int vidyoMemberID)
        {
            int bRet = -1;
            DataSet ds = new DataSet();
            string dsTable = "Usr_List_d";
            try
            {
                if (vidyoRoomID > 0)
                {
                    string query = " select userID from Usr_List_d where MemberID = " + vidyoMemberID;

                    bool ret = SelectCommand(query, ref ds, dsTable);
                    if (!ret)
                    {
                        logger.Exception(10, "SQL error");
                        return (-1);
                    }

                    if (ds.Tables[dsTable].Rows.Count > 0)
                    {
                        if (ds.Tables[dsTable].Rows[0]["userID"] != null)
                            if (ds.Tables[dsTable].Rows[0]["userID"].ToString().Trim() != "")
                                Int32.TryParse(ds.Tables[dsTable].Rows[0]["userID"].ToString().Trim(), out bRet);
                    }
                }

            }
            catch (Exception ex)
            {
                logger.Trace("GetmyVRMUserID" + ex.Message);
                return -1;
            }
            return bRet;
        }

        internal int GetmyVRMUserID(string email)
        {
            int bRet = -1;
            DataSet ds = new DataSet();
            string query = "", dsTable = "Usr_List_d";
            bool ret = false;
            try
            {
                if (email != "")
                {
                    query = "select userID from Usr_List_d where UPPER(Email) = '" + email.Trim().ToUpper() + "'";

                    ret = SelectCommand(query, ref ds, dsTable);
                    if (!ret)
                    {
                        logger.Exception(10, "SQL error");
                        return (-1);
                    }

                    if (ds.Tables[dsTable].Rows.Count > 0)
                    {
                        if (ds.Tables[dsTable].Rows[0]["userID"] != null)
                            if (ds.Tables[dsTable].Rows[0]["userID"].ToString().Trim() != "")
                                Int32.TryParse(ds.Tables[dsTable].Rows[0]["userID"].ToString().Trim(), out bRet);
                    }
                }

            }
            catch (Exception ex)
            {
                logger.Trace("GetmyVRMUserID" + ex.Message);
                return -1;
            }
            return bRet;
        }
        #endregion

        #region GetMaxEndpointID
        /// <summary>
        /// GetMaxEndpointID
        /// </summary>
        /// <param></param>
        /// <returns></returns>
        internal int GetMaxEndpointID()
        {
            int bRet = -1;
            DataSet ds = new DataSet();
            string query = "", dsTable = "Ept_Detials_d";
            try
            {

                query = "select (MAX(endpointId)+1) as EndpointID from Ept_List_D";

                bool ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Exception(10, "SQL error");
                    return (-1);
                }

                if (ds.Tables[dsTable].Rows.Count > 0)
                {
                    if (ds.Tables[dsTable].Rows[0]["EndpointID"] != null)
                        if (ds.Tables[dsTable].Rows[0]["EndpointID"].ToString().Trim() != "")
                            Int32.TryParse(ds.Tables[dsTable].Rows[0]["EndpointID"].ToString().Trim(), out bRet);
                }

            }
            catch (Exception ex)
            {
                logger.Trace("GetMaxEndpointID" + ex.Message);
                return -1;
            }
            return bRet;
        }
        #endregion

        #region GetMaxUserID
        /// <summary>
        /// GetMaxUserID
        /// </summary>
        /// <param></param>
        /// <returns></returns>
        internal int GetMaxUserID()
        {
            int bRet = -1;
            DataSet ds = new DataSet();
            string query = "", dsTable = "usr_list_d";
            bool ret = false;
            try
            {

                query = "select (MAX(userID)+1) as userID from Usr_List_D";

                ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Exception(10, "SQL error");
                    return (-1);
                }

                if (ds.Tables[dsTable].Rows.Count > 0)
                {
                    if (ds.Tables[dsTable].Rows[0]["userID"] != null)
                        if (ds.Tables[dsTable].Rows[0]["userID"].ToString().Trim() != "")
                            Int32.TryParse(ds.Tables[dsTable].Rows[0]["userID"].ToString().Trim(), out bRet);
                }

            }
            catch (Exception ex)
            {
                logger.Trace("GetMaxUserID" + ex.Message);
                return -1;
            }
            return bRet;
        }
        #endregion

        #region GetRoomEndpointID
        /// <summary>
        /// GetRoomEndpointID
        /// </summary>
        /// <param></param>
        /// <returns></returns>
        internal int GetRoomEndpointID(int myVRMRoomID)
        {
            int bRet = -1;
            DataSet ds = new DataSet();
            string query =  "", dsTable = "Loc_Room_d";
            bool ret = false;
            try
            {
                if (myVRMRoomID > 0)
                {

                    query = "select EndpointID from Loc_Room_d where RoomID = " + myVRMRoomID.ToString();

                    ret = SelectCommand(query, ref ds, dsTable);
                    if (!ret)
                    {
                        logger.Exception(10, "SQL error");
                        return (-1);
                    }

                    if (ds.Tables[dsTable].Rows.Count > 0)
                    {
                        if (ds.Tables[dsTable].Rows[0]["EndpointID"] != null)
                            if (ds.Tables[dsTable].Rows[0]["EndpointID"].ToString().Trim() != "")
                                Int32.TryParse(ds.Tables[dsTable].Rows[0]["EndpointID"].ToString().Trim(), out bRet);
                    }
                }

            }
            catch (Exception ex)
            {
                logger.Trace("GetRoomEndpointID" + ex.Message);
                return -1;
            }
            return bRet;
        }
        #endregion

        #region Check object for nulls and fill
        /// <summary>
        /// Check object for nulls and fill
        /// </summary>
        /// <param name="objtoCheck"></param>
        /// <returns></returns>
        public object SetFieldValues(object objtoCheck)
        {
            Type objType = null;
            object objValue = null;
            FieldInfo[] variousFields = null;
            try
            {
                if (objtoCheck == null)
                {
                    return null;
                }
                objType = objtoCheck.GetType();
                variousFields = objType.GetFields(BindingFlags.Instance | BindingFlags.Static | BindingFlags.NonPublic);

                foreach (FieldInfo propertyInfo in variousFields)
                {
                    try
                    {

                        if (propertyInfo.FieldType == typeof(string))
                        {
                            objValue = propertyInfo.GetValue(objtoCheck);

                            if (objValue == null)
                            {
                                if (propertyInfo.FieldType == typeof(string))
                                    objValue = "";
                            }
                            else
                            {
                                if (objValue.GetType() == typeof(string))
                                    objValue = objValue.ToString().Trim().Replace("'", "").Replace("<", "").Replace(">", "").Replace("-", "").Replace("&", "&amp;");

                            }

                            propertyInfo.SetValue(objtoCheck, objValue);
                        }
                    }

                    catch (Exception ex1)
                    {
                        logger.Trace("SetFieldValues" + ex1.StackTrace);

                    }
                }

            }
            catch (Exception ex)
            {

            }
            return objtoCheck;
        }
        public object SetPropertyValues(object objtoCheck)
        {
            Type objType = null;
            object objValue = null;
            PropertyInfo[] variousProperties = null;
            try
            {
                if (objtoCheck == null)
                {
                    return null;
                }
                objType = objtoCheck.GetType();
                variousProperties = objType.GetProperties();
                foreach (PropertyInfo propertyInfo in variousProperties)
                {
                    try
                    {
                        if (propertyInfo.CanRead)
                        {
                            if (propertyInfo.PropertyType == typeof(string))
                            {
                                objValue = propertyInfo.GetValue(objtoCheck, null);

                                if (objValue == null)
                                {
                                    if (propertyInfo.PropertyType == typeof(string))
                                        objValue = "";
                                }
                                else
                                {
                                    if (objValue.GetType() == typeof(string))
                                        objValue = objValue.ToString().Trim().Replace("'", "").Replace("<", "").Replace(">", "").Replace("-", "").Replace("&", " and ");

                                }

                                propertyInfo.SetValue(objtoCheck, objValue, null);
                            }
                        }
                    }

                    catch (Exception ex1)
                    {
                        logger.Trace("SetPropertyValues" + ex1.StackTrace);

                    }
                }

            }
            catch (Exception ex)
            {

            }
            return objtoCheck;
        }
        #endregion

        #region FetchOrgRoomTiersName
        /// <summary>
        /// FetchOrgRoomTiersName
        /// </summary>
        /// <param name="TopTier"></param>
        /// <param name="MiddleTier"></param>
        /// <returns></returns>
        internal bool FetchOrgRoomTiersName(ref int TopTierID, ref int MiddleTierID)
        {
            try
            {
                string Query = "";
                DataSet ds = new DataSet();
                string dsTable = "Org_Settings_D";

                Query = "select top 1 OnflyTopTierID,OnflyMiddleTierID from Org_Settings_D where OrgId = " + defaultOrgId;
                bool ret = SelectCommand(Query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Trace("SQL error");
                    return (false);
                }

                if (ds.Tables[dsTable].Rows.Count > 0)
                {
                    if (ds.Tables[dsTable].Rows[0]["OnflyTopTierID"] != null)
                        Int32.TryParse(ds.Tables[dsTable].Rows[0]["OnflyTopTierID"].ToString(), out TopTierID);

                    if (ds.Tables[dsTable].Rows[0]["OnflyMiddleTierID"] != null)
                        Int32.TryParse(ds.Tables[dsTable].Rows[0]["OnflyMiddleTierID"].ToString(), out MiddleTierID);
                }
            }
            catch (Exception ex)
            {
                logger.Trace("" + ex.StackTrace);
                return false;
            }
            return true;
        }
        #endregion

        #region SetDepartments
        /// <summary>
        /// SetDepartments
        /// </summary>
        /// <param></param>
        /// <returns></returns>
        internal bool SetDepartments(ref Dictionary<string, int> deptList)
        {
            int iCnt = 0;
            int iDeptID = 0;
            DataSet ds = new DataSet();
            string dsTable = "Dept_List_d";
            string query = "";
            try
            {
                if (deptList == null)
                    return false;

                for (iCnt = 0; iCnt < deptList.Count; iCnt++)
                {
                    iDeptID = 0;

                    query = " Declare @iDeptID int;"
                            + " Declare @iOrgID int;"
                            + " Declare @sDeptName varchar(250);"

                            + " set @sDeptName = '" + deptList.ElementAt(iCnt).Key.ToString().Trim() + "'"
                            + " set @iOrgID = " + defaultOrgId

                            + " set @iDeptID = (select DepartmentID from Dept_list_d where RTRIM(LTRIM(UPPER(DepartmentName)))= UPPER(@sDeptName) and OrgID = @iOrgID)"


                            + " IF @iDeptID is null"
                            + " BEGIN"

                                + " Insert into Dept_list_d (DepartmentName,Deleted,ResponseTime,orgID) Values (@sDeptName,0,0,@iOrgID)"

                                + " set @iDeptID = (SELECT SCOPE_IDENTITY())"
                            + " END"

                            + " select @iDeptID as DeptID;";

                    ds = new DataSet();
                    bool ret = SelectCommand(query, ref ds, dsTable);
                    if (!ret)
                    {
                        logger.Exception(10, "SQL error");
                        return (false);
                    }

                    if (ds.Tables[dsTable].Rows.Count > 0)
                    {
                        if (ds.Tables[dsTable].Rows[0]["DeptID"] != null)
                            if (ds.Tables[dsTable].Rows[0]["DeptID"].ToString().Trim() != "")
                                Int32.TryParse(ds.Tables[dsTable].Rows[0]["DeptID"].ToString().Trim(), out iDeptID);
                    }

                    if (iDeptID > 0)
                        deptList[deptList.ElementAt(iCnt).Key] = iDeptID;


                }

            }
            catch (Exception ex)
            {
                logger.Trace("SetDepartments" + ex.Message);
                return false;
            }
            return true;
        }
        #endregion

        #region SetDepartmentsforResource
        /// <summary>
        /// SetDepartmentsforResource
        /// </summary>
        /// <param></param>
        /// <returns></returns>
        internal bool SetDepartmentsforResource(ref int[] deptList, string resourceTableName, string resourceType, int resourceID)
        {
            int iCnt = 0;
            DataSet ds = new DataSet();
            string query = "";
            bool ret = false;
            try
            {
                if (deptList == null || resourceTableName == "" || resourceID <= 0)
                    return false;

                query = " Delete " + resourceTableName + " where " + resourceType + " =" + resourceID;
                ret = NonSelectCommand(query);
                if (!ret)
                {
                    logger.Exception(100, "query failed. query =" + query);
                }

                for (iCnt = 0; iCnt < deptList.Length; iCnt++)
                    query = " Insert into " + resourceTableName + " (" + resourceType + ",[departmentId]) Values (" + resourceID + "," + deptList[iCnt] + ")";

                ret = NonSelectCommand(query);
                if (!ret)
                {
                    logger.Exception(100, "query failed. query =" + query);
                }

            }
            catch (Exception ex)
            {
                logger.Trace("SetDepartmentsforResource" + ex.Message);
                return false;
            }
            return true;
        }
        #endregion

        #region Fetch Conf for Vidyo
        /// <summary>
        /// Fetch Conf for Vidyo
        /// </summary>
        /// <param name="confS"></param>
        /// <param name="ConfID"></param>
        /// <param name="InstanceID"></param>
        /// <returns></returns>
        internal bool FetchConfforVidyo(int ConfID, int InstanceID, ref NS_Vidyo.VidyoParticipant hostParty, ref List<NS_Vidyo.VidyoParticipant> userParties, ref List<NS_Vidyo.VidyoParticipant> roomParties)
        {

            int iMemberID = 0, iEntityID = 0, imyVRMID = 0;
            string sPassword = "", sLogin = "", sPin = "", sAddress = "", sType = "";
            cryptography.Crypto crypt = null;
            NS_Vidyo.VidyoParticipant userParty = null;
            NS_Vidyo.VidyoParticipant roomParty = null;
            try
            {
                // Retreive conf details
                DataSet ds = new DataSet();
                string dsTable = "Conference_Party";
                crypt = new Crypto();
                string query = null; bool ret = false;

                //query = "select MemberID, EntityID,UserID,Password,Login,Pin from Usr_List_D where userID = ("
                //    + "select owner from conf_conference_D where confid = "+ ConfID +" and instanceid = "+ InstanceID +")";
				//FB 2717	
                query = "select R.MemberID, u.EntityID,u.UserID,u.Password,u.Login,u.Pin from Usr_List_D U,Loc_Room_D R "
                    + " where U.UserID = (select owner from conf_conference_D where confid = " + ConfID + " and instanceid = " + InstanceID + ")"
                    + " and R.RoomID in (select RoomID from Conf_Room_D where confid = " + ConfID + " and instanceid = " + InstanceID + ") "; //and R.isVMR = 1";//FB 2717


                ret = false;
                ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Exception(100, "SQL error");
                    return (false);
                }
                if (ds.Tables[dsTable] != null)
                {
                    for (int i = 0; i < ds.Tables[dsTable].Rows.Count; i++)
                    {
                        iMemberID = -1; iEntityID = -1; imyVRMID = -1;
                        sPassword = ""; sLogin = ""; sPin = "";
						//FB 2717 Starts
                        if (ds.Tables[dsTable].Rows[i]["MemberID"] != null)
                            if (ds.Tables[dsTable].Rows[i]["MemberID"].ToString().Trim() != "")
                                Int32.TryParse(ds.Tables[dsTable].Rows[i]["MemberID"].ToString().Trim(), out iMemberID);

                        if (ds.Tables[dsTable].Rows[i]["EntityID"] != null)
                            if (ds.Tables[dsTable].Rows[i]["EntityID"].ToString().Trim() != "")
                                Int32.TryParse(ds.Tables[dsTable].Rows[i]["EntityID"].ToString().Trim(), out iEntityID);

                        if (ds.Tables[dsTable].Rows[i]["UserID"] != null)
                            if (ds.Tables[dsTable].Rows[i]["UserID"].ToString().Trim() != "")
                                Int32.TryParse(ds.Tables[dsTable].Rows[i]["UserID"].ToString().Trim(), out imyVRMID);

                        if (ds.Tables[dsTable].Rows[i]["Login"] != null)
                            if (ds.Tables[dsTable].Rows[i]["Login"].ToString().Trim() != "")
                                sLogin = ds.Tables[dsTable].Rows[i]["Login"].ToString().Trim();

                        if (ds.Tables[dsTable].Rows[i]["Password"] != null)
                            if (ds.Tables[dsTable].Rows[i]["Password"].ToString().Trim() != "")
                                sPassword = crypt.decrypt(ds.Tables[dsTable].Rows[i]["Password"].ToString().Trim());

                        if (ds.Tables[dsTable].Rows[i]["Pin"] != null)
                            if (ds.Tables[dsTable].Rows[i]["Pin"].ToString().Trim() != "")
                                sPin = ds.Tables[dsTable].Rows[i]["Pin"].ToString().Trim();
						//FB 2717 End
                    }

                    hostParty = new NS_Vidyo.VidyoParticipant();
                    hostParty.iMemberID = iMemberID;
                    hostParty.imyVRMID = imyVRMID;
                    hostParty.iRoomID = iEntityID;
                    hostParty.sLogin = sLogin;
                    hostParty.sPassword = sPassword;
                    hostParty.sPin = sPin;

                }

                query = "select MemberID, EntityID,u.userID,u.Login,u.Password,u.Pin from Usr_List_D U , conf_user_D c "
                    + "where c.UserID = U.UserID and confid = " + ConfID + " and instanceid = " + InstanceID + " and  Invitee = 4";

                ret = false;
                ds = new DataSet();
                ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Exception(100, "SQL error");
                    return (false);
                }

                if (ds.Tables[dsTable] != null)
                {
                    for (int i = 0; i < ds.Tables[dsTable].Rows.Count; i++)
                    {

                        iMemberID = -1; iEntityID = -1; imyVRMID = -1;
                        sPassword = ""; sLogin = ""; sPin = "";
						//FB 2717 Starts
                        if (ds.Tables[dsTable].Rows[i]["MemberID"] != null)
                            if (ds.Tables[dsTable].Rows[i]["MemberID"].ToString().Trim() != "")
                                Int32.TryParse(ds.Tables[dsTable].Rows[i]["MemberID"].ToString().Trim(), out iMemberID);

                        if (ds.Tables[dsTable].Rows[i]["EntityID"] != null)
                            if (ds.Tables[dsTable].Rows[i]["EntityID"].ToString().Trim() != "")
                                Int32.TryParse(ds.Tables[dsTable].Rows[i]["EntityID"].ToString().Trim(), out iEntityID);

                        if (ds.Tables[dsTable].Rows[i]["UserID"] != null)
                            if (ds.Tables[dsTable].Rows[i]["UserID"].ToString().Trim() != "")
                                Int32.TryParse(ds.Tables[dsTable].Rows[i]["UserID"].ToString().Trim(), out imyVRMID);

                        if (ds.Tables[dsTable].Rows[i]["Login"] != null)
                            if (ds.Tables[dsTable].Rows[i]["Login"].ToString().Trim() != "")
                                sLogin = ds.Tables[dsTable].Rows[i]["Login"].ToString().Trim();

                        if (ds.Tables[dsTable].Rows[i]["Password"] != null)
                            if (ds.Tables[dsTable].Rows[i]["Password"].ToString().Trim() != "")
                                sPassword = crypt.decrypt(ds.Tables[dsTable].Rows[i]["Password"].ToString().Trim());

                        if (ds.Tables[dsTable].Rows[i]["Pin"] != null)
                            if (ds.Tables[dsTable].Rows[i]["Pin"].ToString().Trim() != "")
                                sPin = ds.Tables[dsTable].Rows[i]["Pin"].ToString().Trim();
						//FB 2717 End

                        userParty = new NS_Vidyo.VidyoParticipant();
                        userParty.iMemberID = iMemberID;
                        userParty.imyVRMID = imyVRMID;
                        userParty.iRoomID = iEntityID;
                        userParty.sLogin = sLogin;
                        userParty.sPassword = sPassword;
                        userParty.sPin = sPin;

                        if (userParties == null)
                            userParties = new List<NS_Vidyo.VidyoParticipant>();

                        userParties.Add(userParty);
                    }
                }

                query = " select  cr.RoomID, c.MemberID, c.EntityID, cr.ipisdnaddress,C.Pin,c.Type "
                    + " from Conf_Room_D cr , Loc_Room_D c "
                    + " where cr.confid = " + ConfID
                    + " and cr.instanceid = " + InstanceID
                    + " and cr.Roomid = c.RoomID;";

                ret = false;
                ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Exception(100, "SQL error");
                    return (false);
                }

                if (ds.Tables[dsTable] != null)
                {
                    for (int i = 0; i < ds.Tables[dsTable].Rows.Count; i++)
                    {

                        iMemberID = -1; iEntityID = -1; imyVRMID = -1;
                        sPin = ""; sAddress = ""; sType = "";
						//FB 2717 Starts
                        if (ds.Tables[dsTable].Rows[i]["MemberID"] != null)
                            if (ds.Tables[dsTable].Rows[i]["MemberID"].ToString().Trim() != "")
                                Int32.TryParse(ds.Tables[dsTable].Rows[i]["MemberID"].ToString().Trim(), out iMemberID);

                        if (ds.Tables[dsTable].Rows[i]["EntityID"] != null)
                            if (ds.Tables[dsTable].Rows[i]["EntityID"].ToString().Trim() != "")
                                Int32.TryParse(ds.Tables[dsTable].Rows[i]["EntityID"].ToString().Trim(), out iEntityID);

                        if (ds.Tables[dsTable].Rows[i]["RoomID"] != null)
                            if (ds.Tables[dsTable].Rows[i]["RoomID"].ToString().Trim() != "")
                                Int32.TryParse(ds.Tables[dsTable].Rows[i]["RoomID"].ToString().Trim(), out imyVRMID);


                        if (ds.Tables[dsTable].Rows[i]["Pin"] != null)
                            if (ds.Tables[dsTable].Rows[i]["Pin"].ToString().Trim() != "")
                                sPin = ds.Tables[dsTable].Rows[i]["Pin"].ToString().Trim();

                        if (ds.Tables[dsTable].Rows[i]["ipisdnaddress"] != null)
                            if (ds.Tables[dsTable].Rows[i]["ipisdnaddress"].ToString().Trim() != "")
                                sAddress = ds.Tables[dsTable].Rows[i]["ipisdnaddress"].ToString().Trim();

                        if (ds.Tables[dsTable].Rows[i]["Type"] != null)
                            if (ds.Tables[dsTable].Rows[i]["Type"].ToString().Trim() != "")
                                sType = ds.Tables[dsTable].Rows[i]["Type"].ToString().Trim();
						//FB 2717 End

                        roomParty = new NS_Vidyo.VidyoParticipant();
                        roomParty.iMemberID = iMemberID;
                        roomParty.imyVRMID = imyVRMID;
                        roomParty.iRoomID = iEntityID;
                        roomParty.sAddress = sAddress;
                        roomParty.sType = sType;
                        roomParty.sPin = sPin;

                        if (roomParties == null)
                            roomParties = new List<NS_Vidyo.VidyoParticipant>();

                        roomParties.Add(roomParty);


                    }
                }

            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }

            return true;
        }
        #endregion

        #region FetchVidyoStatus
        /// <summary>
        /// FetchVidyoStatus
        /// </summary>
        /// <param name="isEnableVQ"></param>
        /// <returns></returns>
        public bool FetchVidyoStatus(ref int isEnableVidyo, ref int isCloudConference, int confID, int instanceID, ref int orgID)//FB 2717
        {

            DataSet ds = new DataSet();
            string dsTable = "OrgSettings";
			//FB 2717
            //string query = " Select Enablecloud,orgID from org_settings_d where orgID = ( select orgID from conf_conference_d where  confID = " + confID + " and instanceID = " + instanceID + ")";
            string query = " Select a.CloudConferencing, b.Enablecloud, b.OrgId from conf_conference_d a, org_settings_d b where a.deleted = 0 and a.orgID = b.orgID and confID = " + confID + " and instanceID = " + instanceID + "";
                                

            try
            {
                bool ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Exception(10, "SQL error");
                    return (false);
                }

                if (ds.Tables[dsTable] != null && ds.Tables[dsTable].Rows.Count > 0)
                {
                    Int32.TryParse(ds.Tables[dsTable].Rows[0]["CloudConferencing"].ToString().Trim(), out isCloudConference);//FB 2717
                    Int32.TryParse(ds.Tables[dsTable].Rows[0]["Enablecloud"].ToString().Trim(), out isEnableVidyo);
                    Int32.TryParse(ds.Tables[dsTable].Rows[0]["orgID"].ToString().Trim(), out orgID);
                }

                logger.Trace(query + " isEnableVidyo = " + isEnableVidyo);

            }
            catch (Exception ex)
            {
                logger.Trace("FetchVidyoStatus failed" + ex.Message);
                return false;
            }

            return true;
        }
        #endregion

        #region SendWelcomeUserMail
        /// <summary>
        /// SendWelcomeUserMail
        /// </summary>
        /// <param name="vidyoUser"></param>
        /// <param name="orgID"></param>
        /// <param name="fNname"></param>
        /// <param name="lName"></param>
        private void SendWelcomeUserMail(ref NS_Vidyo.VidyoUserRoom vidyoUser, int orgID, string fNname, string lName)
        {
            NS_MESSENGER.Email email = new NS_MESSENGER.Email();
            NS_EMAIL.Email sendEmail = new NS_EMAIL.Email(configParams);
            NS_MESSENGER.Smtp smtpServerInfo = new NS_MESSENGER.Smtp();
            string emailBody = "", emailSubject = "";
            string emailContent = "";
            cryptography.Crypto crypto = new cryptography.Crypto();
            try
            {

                logger.Trace("SendEsConfFailMail.....");
                FetchEmailString(orgID, ref emailBody, ref emailSubject, 30);
                FetchSmtpServerInfo(ref smtpServerInfo);

                if (emailBody.Trim() != "" && vidyoUser.sEmail.Trim() != "")
                {
                    emailContent = emailBody.Replace("{2}", fNname.Replace("||", "\"").Replace("!!", "'") + " " + lName.Replace("||", "\"").Replace("!!", "'"));
                    emailContent = emailBody.Replace("{36}", smtpServerInfo.WebsiteURL);
                    emailContent = emailBody.Replace("{41}", vidyoUser.sEmail);
                    emailContent = emailBody.Replace("{42}", crypto.decrypt(vidyoUser.sPassword));

                    email.orgID = orgID;
                    email.To = vidyoUser.sEmail.Trim();
                    email.From = "";
                    email.Body = emailContent;
                    email.Subject = emailSubject;
                    bool ret = sendEmail.SendWelcomeEmail(email, smtpServerInfo);
                    if (!ret)
                    {
                        //errMsg += sendEmail.errMsg;
                        logger.Trace("SMTP connection failed.");
                    }
                }

            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                errMsg = "Send Email" + e.Message;
            }
        }

        #endregion

        //FB 2392 start

        #region SaveUpdatePublicRooms
        /// <summary>
        /// SaveUpdatePublicRooms
        /// </summary>
        /// <param name="room"></param>
        /// <returns></returns>
        internal bool SaveUpdatePublicRooms(ref List<NS_MESSENGER_CUSTOMIZATIONS.Room> rooms, ref string errMsg, bool updateConfs)
        {
            string query = "";
            bool ret = false;
            int myVRMLocationID = 0;
            int orgTopTierID = 0, OrgMiddleTierID = 0;
            int topTierID = 0, middleTierID = 0;
            int iEptID = -1;
            List<NS_MESSENGER_CUSTOMIZATIONS.tierInfo> tierList = null;
            NS_MESSENGER_CUSTOMIZATIONS.tierInfo tier = null;
            String orgTopTierName = "", orgMiddleTierName = "";
            try
            {
                FetchOrgRoomTiersName(ref orgTopTierID, ref OrgMiddleTierID);

                for (int i = 0; i < rooms.Count; i++)
                {
                    myVRMLocationID = 0;
                    myVRMLocationID = GetRoomIDbyPublicRoomID(rooms[i].WhygoRoomId);
                    rooms[i].timezoneID = GetTimeZoneID(rooms[i].timezoneOffset, rooms[i].isDST);


                    if (myVRMLocationID > 0)
                    {
                        query = " Update Loc_Room_D Set Name = '" + rooms[i].name  //need to modify in update query
                             + "', Capacity = '" + rooms[i].Capacity
                             + "', RoomPhone = '" + rooms[i].RoomPhone
                             + "', Zipcode = '" + rooms[i].Zipcode
                             + "', timeZoneID = " + rooms[i].timezoneID
                             + ", latitude = '" + rooms[i].latitude
                             + "', longitude = '" + rooms[i].longitude
                             + "', City = '" + rooms[i].City
                             + "', isPublic = 1"
                             + ", Country = '" + rooms[i].myVRMID //ZD 100729 //ZD 100888
                             + "', videoAvailable = " + rooms[i].videoAvailable //,Assistant hardcorded
                             + ", Lastmodifieddate = '" + rooms[i].PublicRoomLastModified + "'" // FB 2532
                             + " FROM Loc_Room_D "
                             + " where RoomId =" + myVRMLocationID
                             + " Update ES_PublicRoom_d Set [address] ='" + rooms[i].Address1 + "'"
                             + ",RoomDescription='" + rooms[i].RoomDescription + "'"
                             + ",ExtraNotes='" + rooms[i].ExtraNotes + "'"
                             + ",AUXEquipment='" + rooms[i].AUXEquipment + "'"
                             + ",CurrencyType='" + rooms[i].CurrencyType + "'"
                             + ",genericSellPrice= '" + rooms[i].genericSellPrice + "'"
                             + ",geoCodeAddress= '" + rooms[i].geoCodeAddress + "'"
                             + ",internetBiller = '" + rooms[i].internetBiller + "'"
                             + ",internetPriceCurrency = '" + rooms[i].internetPriceCurrency + "'"
                             + ",isAutomatic = " + rooms[i].isAutomatic
                             + ",isHDCapable = " + rooms[i].isHDCapable
                             + ",isInternetCapable =" + rooms[i].isInternetCapable
                             + ",isInternetFree = " + rooms[i].isInternetFree
                             + ",isIPCapable = " + rooms[i].isIPCapable
                             + ",isISDNCapable = " + rooms[i].isISDNCapable
                             + ",isIPCConnectionCapable = " + rooms[i].isIPCConnectionCapable
                             + ",isIPDedicated = " + rooms[i].isIPDedicated
                             + ",isTP = " + rooms[i].isTP
                             + ",isVCCapable = " + rooms[i].isVCCapable
                             + ",MapLink = '" + rooms[i].Maplink + "'"
                             + ",IsEarlyHoursEnabled=" + rooms[i].IsEarlyHoursEnabled
                             + ",EHStartTime='" + rooms[i].EHStartTime + "'"
                             + ",EHEndTime='" + rooms[i].EHEndTime + "'"
                             + ",EHCost=" + rooms[i].EHCost
                             + ",IsAfterHourEnabled=" + rooms[i].IsAfterHourEnabled
                             + ",AHStartTime='" + rooms[i].AHStartTime + "'"
                             + ",AHEndTime='" + rooms[i].AHEndTime + "'"
                             + ",AHCost=" + rooms[i].AHCost
                             + ",openHours ='" + rooms[i].openHours
                             + "',OHStartTime='" + rooms[i].OHStartTime + "'"
                             + ",OHEndTime='" + rooms[i].OHEndTime + "'"
                             + ",OHCost=" + rooms[i].OHCost
                             + ",Is24HoursEnabled=" + rooms[i].Is24HoursEnabled
                             + ",PublicRoomLastModified='" + rooms[i].PublicRoomLastModified + "'"
                             + ",[Type]='" + rooms[i].Type + "'"
                             + ",ISDNNumber='" + rooms[i].isdnAddress + "'"
                             + ",IPAddress='" + rooms[i].ipAddress + "'"
                             + ",IPSpeed ='" + rooms[i].IPSpeed + "'"
                             + ",ISDNSpeed='" + rooms[i].ISDNSpeed + "'"
                             + ",StateName='" + rooms[i].State + "'"
                             + ",Country='" + rooms[i].Country + "'"
                             + ",City='" + rooms[i].City + "'"
                             + ",AHFullyAuto='" + rooms[i].AHFullyAuto + "'" //FB 2543 Starts
                             + ",CHFullyAuto='" + rooms[i].CHFullyAuto + "'"
                             + ",EHFullyAuto='" + rooms[i].EHFullyAuto + "'" //FB 2543 Ends
                             + " Where WhygoRoomId =" + rooms[i].WhygoRoomId;



                    }
                    else
                    {

                        updateConfs = false;

                        topTierID = orgTopTierID;
                        middleTierID = OrgMiddleTierID;
                        orgTopTierName = rooms[i].CountryName.Trim();
                        orgMiddleTierName = rooms[i].StateName.Trim();

                        if (tierList == null)
                            tierList = new List<NS_MESSENGER_CUSTOMIZATIONS.tierInfo>();
                        tier = tierList.Where(tInfo => tInfo.l3LocationName.ToLower().Trim() == orgTopTierName.Trim().ToLower() && tInfo.l2LocationName.ToLower().Trim() == orgMiddleTierName.ToLower().Trim()).FirstOrDefault();
                        if (tier == null)
                        {
                            tier = GetRoomTierInfo(ref orgTopTierName, ref orgMiddleTierName, ref tierList);

                            if (tier != null)
                            {
                                topTierID = tier.l3LocationId;
                                middleTierID = tier.l2LocationId;

                            }

                        }


                        query = " INSERT INTO Loc_Room_D (Name,Capacity,RoomPhone,Zipcode,TimezoneID,Assistant,videoAvailable,Lastmodifieddate,latitude,longitude,isPublic,L3LocationId,L2LocationId,City,Caterer,ProjectorAvailable,DynamicRoomLayout,Servicetype,Extroom,DedicatedVideo,DedicatedCodec,setuptime,teardowntime,isVIP,AdminID,notifyemails,AVOnsiteSupportEmail,MaxPhoneCall,roomicontypeid,Country)" //FB 2532 TCK #100141   //ZD 100888
                             + " OUTPUT Inserted.RoomID," + "'" + rooms[i].Address1 + "','" + rooms[i].RoomDescription + "','" + rooms[i].AUXEquipment
                             + "','" + rooms[i].ExtraNotes + "','" + rooms[i].CurrencyType + "'," + rooms[i].genericSellPrice + ",'" + rooms[i].geoCodeAddress + "','" + rooms[i].internetBiller
                             + "','" + rooms[i].internetPriceCurrency + "'," + rooms[i].isAutomatic + "," + rooms[i].isHDCapable + "," + rooms[i].isInternetCapable + "," + rooms[i].isInternetFree + "," + rooms[i].isIPCapable
                             + "," + rooms[i].isIPCConnectionCapable + "," + rooms[i].isIPDedicated + "," + rooms[i].isTP + "," + rooms[i].isVCCapable
                             + "," + rooms[i].IsEarlyHoursEnabled + ",'" + rooms[i].EHStartTime + "','" + rooms[i].EHEndTime + "'," + rooms[i].EHCost + "," + rooms[i].IsAfterHourEnabled + ",'" + rooms[i].AHStartTime + "','" + rooms[i].AHEndTime
                             + "'," + rooms[i].AHCost + ",'" + rooms[i].openHours + "','" + rooms[i].OHStartTime + "','" + rooms[i].OHEndTime + "'," + rooms[i].OHCost + "," + rooms[i].Is24HoursEnabled
                             + "," + rooms[i].WhygoRoomId + ",'" + rooms[i].PublicRoomLastModified + "','" + rooms[i].Type + "','" + rooms[i].isdnAddress + "','" + rooms[i].ipAddress + "','" + rooms[i].IPSpeed + "','" + rooms[i].ISDNSpeed + "','" + rooms[i].State + "','" + rooms[i].City + "','" + rooms[i].Country + "'," + rooms[i].isISDNCapable + ",'" + rooms[i].Maplink + "'"
                              + "," + rooms[i].EHFullyAuto + "," + rooms[i].CHFullyAuto + "," + rooms[i].AHFullyAuto //FB 2543
                             + " into ES_PublicRoom_d "
                             + " (RoomID,[address],RoomDescription,AUXEquipment,ExtraNotes,"
                             + " CurrencyType,genericSellPrice,geoCodeAddress,internetBiller,"
                             + " internetPriceCurrency,isAutomatic,isHDCapable,isInternetCapable,isInternetFree,isIPCapable,"
                             + " isIPCConnectionCapable,isIPDedicated,isTP,isVCCapable,IsEarlyHoursEnabled,"
                             + " EHStartTime ,EHEndTime,EHCost,IsAfterHourEnabled,AHStartTime,AHEndTime,AHCost,openHours,OHStartTime,"
                             + " OHEndTime,OHCost,Is24HoursEnabled,WhygoRoomId,PublicRoomLastModified,[Type],ISDNNumber,IPAddress,IPSpeed,ISDNSpeed,StateName,City,Country,isISDNCapable,MapLink,EHFullyAuto,CHFullyAuto,AHFullyAuto )"  //FB 2543
                             + " values ('" + rooms[i].name + "','" + rooms[i].Capacity + "','" + rooms[i].RoomPhone + "','" + rooms[i].Zipcode + "'"
                             + "," + rooms[i].timezoneID + ",'11'," + rooms[i].videoAvailable + ",'" + rooms[i].PublicRoomLastModified + "','" + rooms[i].latitude + "','" + rooms[i].longitude + "',1," + topTierID.ToString() + "," + middleTierID.ToString() + ",'" + rooms[i].City + "',0,0,0,-1,0,0,0,0,0,0,11,'','',0,26," + rooms[i].myVRMID + ")"; //loc_room values //FB 2532 TCK #100141

                    }


                    ret = NonSelectCommand(query);
                    if (!ret)
                    {
                        logger.Exception(100, "query failed. query =" + query);
                        return false;
                    }

                    if (myVRMLocationID <= 0)
                        myVRMLocationID = GetRoomIDbyPublicRoomID(rooms[i].WhygoRoomId);

                    iEptID = SaveUpdatePublicRoomEndpoints(rooms[i], ref errMsg, myVRMLocationID);

                    query = " Update Loc_Room_D Set endpointid =" + iEptID + " where roomID =" + myVRMLocationID;

                    ret = NonSelectCommand(query);
                    if (!ret)
                    {
                        logger.Exception(100, "query failed. query =" + query);
                    }

                    if (updateConfs)
                        SaveUpdatePublicRoomEndpointsinConferences(rooms[i], ref errMsg, myVRMLocationID);
                }
            }
            catch (Exception ex)
            {
                logger.Trace("SaveUpdatePublicRooms :" + ex.StackTrace);
                errMsg = ex.Message;
                return false;
            }
            return true;
        }
        #endregion

        #region SaveUpdatePublicRoomEndpoints
        /// <summary>
        /// SaveUpdatePublicRoomEndpoints
        /// </summary>
        /// <param name="room"></param>
        /// <param name="errMsg"></param>
        /// <param name="myVRMLocationID"></param>
        /// <returns></returns>
        internal int SaveUpdatePublicRoomEndpoints(NS_MESSENGER_CUSTOMIZATIONS.Room room, ref string errMsg, int myVRMLocationID)
        {
            string query = "";
            string ipAddress = "", isdnAddress = "", ipLineRate = "", isdnLineRate = "";
            bool ret = false, refInsert = true;
            int iEptID = -1, iProfileID = 1, iIsDefault = 1, iRet = 0, isdnUID = -1, ipUID = -1;
            DataSet ds = new DataSet();
            string dsTable = "Ept_List_d";
            try
            {
                iEptID = -1;

                query = " Select UID,EndpointID,addresstype,address,linerateID from Ept_List_D where endpointId=(select endpointid from loc_room_d where RoomId =" + myVRMLocationID + " )";
                          
                ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Exception(10, "SQL error");
                    return (-1);
                }
                if (ds.Tables[dsTable] != null)
                {

                    if (ds.Tables[dsTable].Rows.Count > 0)
                    {
                        for (int rwcnt = 0; rwcnt < ds.Tables[dsTable].Rows.Count; rwcnt++)
                        {
                            if (iEptID <= 0)
                            {
                                if (ds.Tables[dsTable].Rows[0]["EndpointID"] != null)
                                    if (ds.Tables[dsTable].Rows[0]["EndpointID"].ToString().Trim() != "")
                                        Int32.TryParse(ds.Tables[dsTable].Rows[0]["EndpointID"].ToString().Trim(), out iEptID);
                            }

                            if (iEptID > 0)
                            {

                                if (ds.Tables[dsTable].Rows[rwcnt]["UID"] != null && ds.Tables[dsTable].Rows[rwcnt]["addresstype"] != null && ds.Tables[dsTable].Rows[rwcnt]["address"] != null && ds.Tables[dsTable].Rows[rwcnt]["linerateID"] != null)
                                {
                                    if (ds.Tables[dsTable].Rows[rwcnt]["addresstype"].ToString().Trim() == "4")
                                    {
                                        isdnAddress = ds.Tables[dsTable].Rows[rwcnt]["address"].ToString().Trim();
                                        isdnLineRate = ds.Tables[dsTable].Rows[rwcnt]["linerateID"].ToString().Trim();
                                        Int32.TryParse(ds.Tables[dsTable].Rows[rwcnt]["UID"].ToString().Trim(), out isdnUID);
                                    }
                                    else if (ds.Tables[dsTable].Rows[rwcnt]["addresstype"].ToString().Trim() == "1")
                                    {
                                        ipAddress = ds.Tables[dsTable].Rows[rwcnt]["address"].ToString().Trim();
                                        ipLineRate = ds.Tables[dsTable].Rows[rwcnt]["linerateID"].ToString().Trim();
                                        Int32.TryParse(ds.Tables[dsTable].Rows[rwcnt]["UID"].ToString().Trim(), out ipUID);
                                    }
                                }
                            }
                        }
                    }
                }

                iRet = SaveUpdatePublicRoomEndpoints(ref ipUID, ref iEptID, ref refInsert, ipAddress, ipLineRate, room.name, room.ipAddress, "1", room.IPSpeed, "2", ref iProfileID, ref iIsDefault, 1);
                iRet += SaveUpdatePublicRoomEndpoints(ref isdnUID, ref iEptID, ref refInsert, isdnAddress, isdnLineRate, room.name, room.isdnAddress, "4", room.ISDNSpeed, "2", ref iProfileID, ref iIsDefault, 1);

                if (iRet > 0)
                    iRet = iEptID;
            }
            catch (Exception ex)
            {
                logger.Trace("SaveUpdatePublicRoomEndpoints :" + ex.StackTrace);
                errMsg = ex.Message;
                return -1;
            }
            return iRet;
        }
        #endregion

        #region GetPublicRoomLastModified
        /// <summary>
        /// GetPublicRoomLastModified
        /// </summary>
        /// <param name="PublicRoomLastModified"></param>
        /// <returns></returns>
        internal bool GetPublicRoomLastModified(ref DateTime PublicRoomLastModified)
        {
            try
            {
                DateTime.TryParse("2000-01-01 01:00:00.000", out PublicRoomLastModified);
                DataSet ds = new DataSet();
                string dsTable = "ES_PublicRoom_d";
                string query = " select max(PublicRoomLastModified) as PublicRoomLastModified from ES_PublicRoom_d";

                bool ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Exception(10, "SQL error");
                    return (false);
                }

                if (ds.Tables[dsTable].Rows.Count > 0)
                {
                    if (ds.Tables[dsTable].Rows[0]["PublicRoomLastModified"] != null)
                        if (ds.Tables[dsTable].Rows[0]["PublicRoomLastModified"].ToString().Trim() != "")
                            DateTime.TryParse(ds.Tables[dsTable].Rows[0]["PublicRoomLastModified"].ToString().Trim(), out PublicRoomLastModified);

                }
            }
            catch (Exception ex)
            {
                logger.Trace("GetPublicRoomLastModified" + ex.Message);
                return false;
            }
            return true;
        }
        #endregion

        #region GetPublicRoom
        /// <summary>
        /// GetPublicRoom
        /// </summary>
        /// <param name="myVRMLocationID"></param>
        /// <returns></returns>
        internal NS_MESSENGER_CUSTOMIZATIONS.Room GetPublicRoom(int myVRMLocationID)
        {
            NS_MESSENGER_CUSTOMIZATIONS.Room roomRet = new NS_MESSENGER_CUSTOMIZATIONS.Room();
            DataSet ds = new DataSet();
            DataSet dsEP = new DataSet();
            string query = "", dsTable = "ES_PublicRoom_d", dsEPTable = "Ept_List_D";
            int whygoRoomID = 0, addressType = 1;
            string address = "", linerate = "";
            bool ret = false;
            try
            {
                if (myVRMLocationID > 0)
                {
                    query = "select L.Name,L.Capacity,L.[State],L.City,L.videoAvailable,L.RoomPhone,L.Zipcode,L.Maplink,L.Latitude,L.Longitude,"
                                 + "P.* from ES_PublicRoom_d P,Loc_Room_D L where L.RoomID = P.RoomID and L.RoomID =" + myVRMLocationID.ToString();

                    ret = SelectCommand(query, ref ds, dsTable);
                    if (!ret)
                    {
                        logger.Exception(10, "SQL error");
                        return roomRet;
                    }

                    if (ds.Tables[dsTable].Rows.Count > 0)
                    {
                        #region Fetching EnpPoint
                        query = "select e.[address],e.linerateid,e.addresstype,e.connectiontype"
                              + "from Loc_Room_D L,Ept_List_D e where e.endpointId = l.endpointid and l.RoomID =" + myVRMLocationID.ToString();

                        ret = SelectCommand(query, ref dsEP, dsEPTable);
                        if (!ret)
                        {
                            logger.Exception(10, "SQL error");
                        }

                        for (int i = 0; i < dsEP.Tables[dsEPTable].Rows.Count; i++)
                        {
                            if (ds.Tables[dsTable].Rows[i]["addresstype"] != null)
                                if (ds.Tables[dsTable].Rows[i]["addresstype"].ToString().Trim() != "")
                                    Int32.TryParse(ds.Tables[dsTable].Rows[i]["addresstype"].ToString().Trim(), out addressType);

                            if (ds.Tables[dsTable].Rows[i]["address"] != null)
                                address = ds.Tables[dsTable].Rows[i]["address"].ToString().Trim();

                            if (ds.Tables[dsTable].Rows[0]["linerateid"] != null)
                                linerate = ds.Tables[dsTable].Rows[0]["linerateid"].ToString().Trim();

                            if (addressType == 1)
                            {
                                roomRet.ipAddress = address;
                                roomRet.IPSpeed = linerate;
                            }
                            else if (addressType == 4)
                            {
                                roomRet.isdnAddress = address;
                                roomRet.ISDNSpeed = linerate;
                            }
                        }
                        #endregion

                        #region WhyGo Public Room Details

                        if (ds.Tables[dsTable].Rows[0]["whygoRoomID"] != null)
                            if (ds.Tables[dsTable].Rows[0]["whygoRoomID"].ToString().Trim() != "")
                                Int32.TryParse(ds.Tables[dsTable].Rows[0]["whygoRoomID"].ToString().Trim(), out whygoRoomID);

                        roomRet.WhygoRoomId = whygoRoomID;

                        if (ds.Tables[dsTable].Rows[0]["Name"] != null)
                            roomRet.name = ds.Tables[dsTable].Rows[0]["Name"].ToString().Trim();

                        if (ds.Tables[dsTable].Rows[0]["Latitude"] != null)
                            roomRet.latitude = ds.Tables[dsTable].Rows[0]["Latitude"].ToString().Trim();

                        if (ds.Tables[dsTable].Rows[0]["Longitude"] != null)
                            roomRet.longitude = ds.Tables[dsTable].Rows[0]["Longitude"].ToString().Trim();

                        roomRet.Capacity = 0;
                        if (ds.Tables[dsTable].Rows[0]["Capacity"] != null)
                            if (ds.Tables[dsTable].Rows[0]["Capacity"].ToString().Trim() != "")
                                Int32.TryParse(ds.Tables[dsTable].Rows[0]["Capacity"].ToString().Trim(), out roomRet.Capacity);

                        if (ds.Tables[dsTable].Rows[0]["State"] != null)
                            roomRet.State = ds.Tables[dsTable].Rows[0]["State"].ToString().Trim();

                        if (ds.Tables[dsTable].Rows[0]["City"] != null)
                            roomRet.City = ds.Tables[dsTable].Rows[0]["City"].ToString().Trim();

                        if (ds.Tables[dsTable].Rows[0]["videoAvailable"] != null)
                            if (ds.Tables[dsTable].Rows[0]["videoAvailable"].ToString().Trim() != "")
                                Int32.TryParse(ds.Tables[dsTable].Rows[0]["videoAvailable"].ToString().Trim(), out roomRet.videoAvailable);

                        if (ds.Tables[dsTable].Rows[0]["RoomPhone"] != null)
                            roomRet.RoomPhone = ds.Tables[dsTable].Rows[0]["RoomPhone"].ToString().Trim();

                        if (ds.Tables[dsTable].Rows[0]["Zipcode"] != null)
                            roomRet.Zipcode = ds.Tables[dsTable].Rows[0]["Zipcode"].ToString().Trim();

                        if (ds.Tables[dsTable].Rows[0]["Maplink"] != null)
                            roomRet.Maplink = ds.Tables[dsTable].Rows[0]["Maplink"].ToString().Trim();

                        if (ds.Tables[dsTable].Rows[0]["Type"] != null)
                            roomRet.Type = ds.Tables[dsTable].Rows[0]["Type"].ToString().Trim();

                        if (ds.Tables[dsTable].Rows[0]["RoomDescription"] != null)
                            roomRet.RoomDescription = ds.Tables[dsTable].Rows[0]["RoomDescription"].ToString().Trim();

                        if (ds.Tables[dsTable].Rows[0]["ExtraNotes"] != null)
                            roomRet.ExtraNotes = ds.Tables[dsTable].Rows[0]["ExtraNotes"].ToString().Trim();

                        if (ds.Tables[dsTable].Rows[0]["AUXEquipment"] != null)
                            roomRet.AUXEquipment = ds.Tables[dsTable].Rows[0]["AUXEquipment"].ToString().Trim();

                        roomRet.genericSellPrice = 0.0m;
                        if (ds.Tables[dsTable].Rows[0]["genericSellPrice"] != null)
                            if (ds.Tables[dsTable].Rows[0]["genericSellPrice"].ToString().Trim() != "")
                                Decimal.TryParse(ds.Tables[dsTable].Rows[0]["genericSellPrice"].ToString().Trim() + "m", out roomRet.genericSellPrice);

                        if (ds.Tables[dsTable].Rows[0]["geoCodeAddress"] != null)
                            roomRet.geoCodeAddress = ds.Tables[dsTable].Rows[0]["geoCodeAddress"].ToString().Trim();

                        if (ds.Tables[dsTable].Rows[0]["internetBiller"] != null)
                            roomRet.internetBiller = ds.Tables[dsTable].Rows[0]["internetBiller"].ToString().Trim();

                        if (ds.Tables[dsTable].Rows[0]["internetPriceCurrency"] != null)
                            roomRet.internetPriceCurrency = ds.Tables[dsTable].Rows[0]["internetPriceCurrency"].ToString().Trim();

                        roomRet.isAutomatic = 0;
                        if (ds.Tables[dsTable].Rows[0]["isAutomatic"] != null)
                            if (ds.Tables[dsTable].Rows[0]["isAutomatic"].ToString().Trim() != "")
                                Int32.TryParse(ds.Tables[dsTable].Rows[0]["isAutomatic"].ToString().Trim(), out roomRet.isAutomatic);

                        roomRet.isHDCapable = 0;
                        if (ds.Tables[dsTable].Rows[0]["isHDCapable"] != null)
                            if (ds.Tables[dsTable].Rows[0]["isHDCapable"].ToString().Trim() != "")
                                Int32.TryParse(ds.Tables[dsTable].Rows[0]["isHDCapable"].ToString().Trim(), out roomRet.isHDCapable);


                        roomRet.isInternetCapable = 0;
                        if (ds.Tables[dsTable].Rows[0]["isInternetCapable"] != null)
                            if (ds.Tables[dsTable].Rows[0]["isInternetCapable"].ToString().Trim() != "")
                                Int32.TryParse(ds.Tables[dsTable].Rows[0]["isInternetCapable"].ToString().Trim(), out roomRet.isInternetCapable);

                        roomRet.isInternetFree = 0;
                        if (ds.Tables[dsTable].Rows[0]["isInternetFree"] != null)
                            if (ds.Tables[dsTable].Rows[0]["isInternetFree"].ToString().Trim() != "")
                                Int32.TryParse(ds.Tables[dsTable].Rows[0]["isInternetFree"].ToString().Trim(), out roomRet.isInternetFree);

                        roomRet.isIPCapable = 0;
                        if (ds.Tables[dsTable].Rows[0]["isIPCapable"] != null)
                            if (ds.Tables[dsTable].Rows[0]["isIPCapable"].ToString().Trim() != "")
                                Int32.TryParse(ds.Tables[dsTable].Rows[0]["isIPCapable"].ToString().Trim(), out roomRet.isIPCapable);

                        roomRet.isIPCConnectionCapable = 0;
                        if (ds.Tables[dsTable].Rows[0]["isIPCConnectionCapable"] != null)
                            if (ds.Tables[dsTable].Rows[0]["isIPCConnectionCapable"].ToString().Trim() != "")
                                Int32.TryParse(ds.Tables[dsTable].Rows[0]["isIPCConnectionCapable"].ToString().Trim(), out roomRet.isIPCConnectionCapable);

                        roomRet.isIPDedicated = 0;
                        if (ds.Tables[dsTable].Rows[0]["isIPDedicated"] != null)
                            if (ds.Tables[dsTable].Rows[0]["isIPDedicated"].ToString().Trim() != "")
                                Int32.TryParse(ds.Tables[dsTable].Rows[0]["isIPDedicated"].ToString().Trim(), out roomRet.isIPDedicated);

                        roomRet.isTP = 0;
                        if (ds.Tables[dsTable].Rows[0]["isTP"] != null)
                            if (ds.Tables[dsTable].Rows[0]["isTP"].ToString().Trim() != "")
                                Int32.TryParse(ds.Tables[dsTable].Rows[0]["isTP"].ToString().Trim(), out roomRet.isTP);

                        roomRet.isVCCapable = 0;
                        if (ds.Tables[dsTable].Rows[0]["isVCCapable"] != null)
                            if (ds.Tables[dsTable].Rows[0]["isVCCapable"].ToString().Trim() != "")
                                Int32.TryParse(ds.Tables[dsTable].Rows[0]["isVCCapable"].ToString().Trim(), out roomRet.isVCCapable);

                        if (ds.Tables[dsTable].Rows[0]["CurrencyType"] != null)
                            roomRet.CurrencyType = ds.Tables[dsTable].Rows[0]["CurrencyType"].ToString().Trim();

                        //Early Hour start
                        if (ds.Tables[dsTable].Rows[0]["IsEarlyHoursEnabled"] != null)
                            if (ds.Tables[dsTable].Rows[0]["IsEarlyHoursEnabled"].ToString().Trim() != "")
                                roomRet.IsEarlyHoursEnabled = Convert.ToInt32(ds.Tables[dsTable].Rows[0]["IsEarlyHoursEnabled"].ToString().Trim());

                        if (ds.Tables[dsTable].Rows[0]["earlyHoursStart"] != null)
                            roomRet.EHStartTime = ds.Tables[dsTable].Rows[0]["earlyHoursStart"].ToString();

                        if (ds.Tables[dsTable].Rows[0]["EHEndTime"] != null)
                            roomRet.EHEndTime = ds.Tables[dsTable].Rows[0]["EHEndTime"].ToString();

                        if (ds.Tables[dsTable].Rows[0]["EHCost"] != null)
                            roomRet.EHCost = decimal.Parse(ds.Tables[dsTable].Rows[0]["EHCost"].ToString().Trim());
                        //Early Hour end
                        //Open Hour start
                        if (ds.Tables[dsTable].Rows[0]["openHours"] != null)
                            roomRet.openHours = ds.Tables[dsTable].Rows[0]["openHours"].ToString().Trim();

                        if (ds.Tables[dsTable].Rows[0]["OHStartTime"] != null)
                            roomRet.OHStartTime = ds.Tables[dsTable].Rows[0]["OHStartTime"].ToString();

                        if (ds.Tables[dsTable].Rows[0]["OHEndTime"] != null)
                            roomRet.OHEndTime = ds.Tables[dsTable].Rows[0]["OHEndTime"].ToString();

                        if (ds.Tables[dsTable].Rows[0]["OHCost"] != null)
                            roomRet.OHCost = decimal.Parse(ds.Tables[dsTable].Rows[0]["OHCost"].ToString().Trim());
                        //Open Hour end
                        //After Hour start
                        if (ds.Tables[dsTable].Rows[0]["IsAfterHourEnabled"] != null)
                            if (ds.Tables[dsTable].Rows[0]["IsAfterHourEnabled"].ToString().Trim() != "")
                                roomRet.IsEarlyHoursEnabled = Convert.ToInt32(ds.Tables[dsTable].Rows[0]["IsAfterHourEnabled"].ToString().Trim());

                        if (ds.Tables[dsTable].Rows[0]["AHStartTime"] != null)
                            roomRet.AHStartTime = ds.Tables[dsTable].Rows[0]["AHStartTime"].ToString();

                        if (ds.Tables[dsTable].Rows[0]["AHEndTime"] != null)
                            roomRet.AHEndTime = ds.Tables[dsTable].Rows[0]["AHEndTime"].ToString();

                        if (ds.Tables[dsTable].Rows[0]["AHCost"] != null)
                            roomRet.AHCost = decimal.Parse(ds.Tables[dsTable].Rows[0]["AHCost"].ToString().Trim());
                        //After Hour end
                        //Crazy Hour start
                        if (ds.Tables[dsTable].Rows[0]["isCrazyHoursSupported"] != null)
                            if (ds.Tables[dsTable].Rows[0]["isCrazyHoursSupported"].ToString().Trim() != "")
                                roomRet.isCrazyHoursSupported = Convert.ToInt32(ds.Tables[dsTable].Rows[0]["isCrazyHoursSupported"].ToString().Trim());

                        if (ds.Tables[dsTable].Rows[0]["CHtartTime"] != null)
                            roomRet.CHtartTime = ds.Tables[dsTable].Rows[0]["CHtartTime"].ToString();

                        if (ds.Tables[dsTable].Rows[0]["CHEndTime"] != null)
                            roomRet.CHEndTime = ds.Tables[dsTable].Rows[0]["CHEndTime"].ToString();

                        if (ds.Tables[dsTable].Rows[0]["CHCost"] != null)
                            roomRet.CHCost = decimal.Parse(ds.Tables[dsTable].Rows[0]["CHCost"].ToString().Trim());
                        //Crazy Hour end

                        if (ds.Tables[dsTable].Rows[0]["Is24HoursEnabled"] != null)
                            if (ds.Tables[dsTable].Rows[0]["Is24HoursEnabled"].ToString().Trim() != "")
                                roomRet.Is24HoursEnabled = Convert.ToInt32(ds.Tables[dsTable].Rows[0]["Is24HoursEnabled"].ToString().Trim());

                        if (ds.Tables[dsTable].Rows[0]["PublicRoomLastModified"] != null)
                            if (ds.Tables[dsTable].Rows[0]["PublicRoomLastModified"].ToString().Trim() != "")
                                DateTime.TryParse(ds.Tables[dsTable].Rows[0]["PublicRoomLastModified "].ToString().Trim(), out roomRet.PublicRoomLastModified);

                        //FB 2543 Starts
                        roomRet.AHFullyAuto = 0;
                        if (ds.Tables[dsTable].Rows[0]["afterHoursFullyAuto"] != null)
                            if (ds.Tables[dsTable].Rows[0]["afterHoursFullyAuto"].ToString().Trim() != "")
                                Int32.TryParse(ds.Tables[dsTable].Rows[0]["afterHoursFullyAuto"].ToString().Trim(), out roomRet.AHFullyAuto);

                        roomRet.CHFullyAuto = 0;
                        if (ds.Tables[dsTable].Rows[0]["crazyHoursFullyAuto"] != null)
                            if (ds.Tables[dsTable].Rows[0]["crazyHoursFullyAuto"].ToString().Trim() != "")
                                Int32.TryParse(ds.Tables[dsTable].Rows[0]["crazyHoursFullyAuto"].ToString().Trim(), out roomRet.CHFullyAuto);

                        roomRet.EHFullyAuto = 0;
                        if (ds.Tables[dsTable].Rows[0]["earlyHoursFullyAuto"] != null)
                            if (ds.Tables[dsTable].Rows[0]["earlyHoursFullyAuto"].ToString().Trim() != "")
                                Int32.TryParse(ds.Tables[dsTable].Rows[0]["earlyHoursFullyAuto"].ToString().Trim(), out roomRet.EHFullyAuto);
                        //FB 2543 Ends

                        #endregion
                    }
                }

            }
            catch (Exception ex)
            {
                logger.Trace("GetPublicRoom" + ex.Message);
                return roomRet;
            }
            return roomRet;
        }

        internal NS_MESSENGER_CUSTOMIZATIONS.Room GetPublicRoomLimited(int myVRMLocationID)
        {
            NS_MESSENGER_CUSTOMIZATIONS.Room roomRet = null;
            DataSet ds = new DataSet();
            DataSet dsEP = new DataSet();
            string query = "", dsTable = "ES_PublicRoom_d", dsEPTable = "Ept_List_D";
            int whygoRoomID = 0, addressType = 1;
            string address = "", linerate = "";
            bool ret = false;
            try
            {
                if (myVRMLocationID > 0)
                {
                    query = "select L.Name,"
                                 + "P.whygoRoomID from ES_PublicRoom_d P,Loc_Room_D L where L.RoomID = P.RoomID and L.RoomID =" + myVRMLocationID.ToString();

                    ret = SelectCommand(query, ref ds, dsTable);
                    if (!ret)
                    {
                        logger.Exception(10, "SQL error");
                        return roomRet;
                    }

                    if (ds.Tables[dsTable].Rows.Count > 0)
                    {


                        #region WhyGo Public Room Details

                        if (ds.Tables[dsTable].Rows[0]["whygoRoomID"] != null)
                            if (ds.Tables[dsTable].Rows[0]["whygoRoomID"].ToString().Trim() != "")
                                Int32.TryParse(ds.Tables[dsTable].Rows[0]["whygoRoomID"].ToString().Trim(), out whygoRoomID);

                        roomRet.WhygoRoomId = whygoRoomID;

                        if (ds.Tables[dsTable].Rows[0]["Name"] != null)
                            roomRet.name = ds.Tables[dsTable].Rows[0]["Name"].ToString().Trim();



                        #endregion
                    }
                }

            }
            catch (Exception ex)
            {
                logger.Trace("GetPublicRoom" + ex.Message);
                return roomRet;
            }
            return roomRet;
        }
        #endregion

        #region GetRoomIDbyPublicRoomID
        /// <summary>
        /// GetRoomIDbyPublicRoomID
        /// </summary>
        /// <param name="publicRoomID"></param>
        /// <returns></returns>
        internal int GetRoomIDbyPublicRoomID(int publicRoomID)
        {
            int bRet = -1;
            DataSet ds = new DataSet();
            string query = "", dsTable = "ES_PublicRoom_d";
            bool ret = false;
            try
            {
                if (publicRoomID > 0)
                {
                    query = "select RoomID from ES_PublicRoom_d where whygoRoomID = " + publicRoomID.ToString();

                    ret = SelectCommand(query, ref ds, dsTable);
                    if (!ret)
                    {
                        logger.Exception(10, "SQL error");
                        return (-1);
                    }

                    if (ds.Tables[dsTable].Rows.Count > 0)
                    {
                        if (ds.Tables[dsTable].Rows[0]["RoomID"] != null)
                            if (ds.Tables[dsTable].Rows[0]["RoomID"].ToString().Trim() != "")
                                Int32.TryParse(ds.Tables[dsTable].Rows[0]["RoomID"].ToString().Trim(), out bRet);
                    }
                }

            }
            catch (Exception ex)
            {
                logger.Trace("GetRoomIDbyPublicRoomID" + ex.Message);
                return -1;
            }
            return bRet;
        }
        #endregion

        #region GetRoomTierInfo
        /// <summary>
        /// GetRoomTierInfo
        /// </summary>
        /// <returns></returns>
        internal NS_MESSENGER_CUSTOMIZATIONS.tierInfo GetRoomTierInfo(ref String CountryName, ref String StateName, ref  List<NS_MESSENGER_CUSTOMIZATIONS.tierInfo> tierList)
        {
            int l3ID = -1, l2ID = -1;
            DataSet ds = new DataSet();
            string query = "", dsTable = "ToporMiddle_Tier";
            bool ret = false;
            NS_MESSENGER_CUSTOMIZATIONS.tierInfo tier = null;
            try
            {
                if (CountryName != "" && StateName != "")
                {
                    query = "SELECT Id from Loc_Tier3_D where LOWER(Name) = '" + CountryName.ToLower() + "'";

                    ret = SelectCommand(query, ref ds, dsTable);
                    if (!ret)
                    {
                        logger.Exception(10, "SQL error");
                        return (null);
                    }

                    if (ds.Tables[dsTable].Rows.Count > 0)
                    {
                        if (ds.Tables[dsTable].Rows[0]["Id"] != null)
                            if (ds.Tables[dsTable].Rows[0]["Id"].ToString().Trim() != "")
                                Int32.TryParse(ds.Tables[dsTable].Rows[0]["Id"].ToString().Trim(), out l3ID);
                    }

                    if (l3ID > 0)
                    {
                        query = "SELECT Id from Loc_Tier2_D where LOWER(Name) = '" + StateName.ToLower() + "' and L3LocationID = " + l3ID;
                        ds = new DataSet();
                        ret = SelectCommand(query, ref ds, dsTable);
                        if (!ret)
                        {
                            logger.Exception(10, "SQL error");
                            return (null);
                        }

                        if (ds.Tables[dsTable].Rows.Count > 0)
                        {
                            if (ds.Tables[dsTable].Rows[0]["Id"] != null)
                                if (ds.Tables[dsTable].Rows[0]["Id"].ToString().Trim() != "")
                                    Int32.TryParse(ds.Tables[dsTable].Rows[0]["Id"].ToString().Trim(), out l2ID);
                        }

                    }

                    if (l3ID <= 0)
                    {

                        query = "INSERT Loc_Tier3_D (Name,disabled,orgID) OUTPUT Inserted.Id values ('" + CountryName + "',0," + defaultOrgId + ")";
                        ds = new DataSet();
                        ret = SelectCommand(query, ref ds, dsTable);
                        if (!ret)
                        {
                            logger.Exception(10, "SQL error");
                            return (null);
                        }

                        if (ds.Tables[dsTable].Rows.Count > 0)
                        {
                            if (ds.Tables[dsTable].Rows[0]["Id"] != null)
                                if (ds.Tables[dsTable].Rows[0]["Id"].ToString().Trim() != "")
                                    Int32.TryParse(ds.Tables[dsTable].Rows[0]["Id"].ToString().Trim(), out l3ID);
                        }


                    }

                    if (l3ID > 0 && l2ID <= 0)
                    {

                        query = "INSERT Loc_Tier2_D (Name,L3locationID,disabled,orgID) OUTPUT Inserted.Id values ('" + StateName + "'," + l3ID + ",0," + defaultOrgId + ")";
                        ds = new DataSet();
                        ret = SelectCommand(query, ref ds, dsTable);
                        if (!ret)
                        {
                            logger.Exception(10, "SQL error");
                            return (null);
                        }

                        if (ds.Tables[dsTable].Rows.Count > 0)
                        {
                            if (ds.Tables[dsTable].Rows[0]["Id"] != null)
                                if (ds.Tables[dsTable].Rows[0]["Id"].ToString().Trim() != "")
                                    Int32.TryParse(ds.Tables[dsTable].Rows[0]["Id"].ToString().Trim(), out l2ID);
                        }

                    }

                    if (l2ID > 0 && l3ID > 0)
                    {
                        if (tierList == null)
                            tierList = new List<NS_MESSENGER_CUSTOMIZATIONS.tierInfo>();

                        tier = new NS_MESSENGER_CUSTOMIZATIONS.tierInfo();
                        tier.l2LocationId = l2ID;
                        tier.l2LocationName = StateName;
                        tier.l3LocationId = l3ID;
                        tier.l3LocationName = CountryName;
                        tierList.Add(tier);
                        return (tier);

                    }


                }

            }
            catch (Exception ex)
            {
                logger.Trace("GetRoomIDbyPublicRoomID" + ex.Message);
                return null;
            }
            return null;
        }
        #endregion

        #region GetWhygoTimeZoneID
        /// <summary>
        /// GetWhygoTimeZoneID
        /// </summary>
        /// <param name="myVRMTimezoneID"></param>
        /// <returns></returns>
        internal int GetWhygoTimeZoneID(int myVRMTimezoneID)
        {
            int bRet = 56;//Similar to EST in myVRM
            DataSet ds = new DataSet();
            string dsTable = "whyGoTimeZoneID";
            string query = "";
            bool ret = false;
            try
            {
                if (myVRMTimezoneID > 0)
                {
                    query = "DECLARE @whygoTimeZoneID int  EXEC GetWhygoTimezone " + myVRMTimezoneID.ToString() + " ,@whygoTimeZoneID OUTPUT  select @whygoTimeZoneID as whygoTimeZone";

                    ret = SelectCommand(query, ref ds, dsTable);
                    if (!ret)
                    {
                        logger.Exception(10, "SQL error");
                        return (-1);
                    }

                    if (ds.Tables[dsTable].Rows.Count > 0)
                    {
                        if (ds.Tables[dsTable].Rows[0]["whygoTimeZone"] != null)
                            if (ds.Tables[dsTable].Rows[0]["whygoTimeZone"].ToString().Trim() != "")
                                Int32.TryParse(ds.Tables[dsTable].Rows[0]["whygoTimeZone"].ToString().Trim(), out bRet);
                    }

                    if (bRet <= 0)
                        bRet = 56;
                }

            }
            catch (Exception ex)
            {
                logger.Trace("GetWhygoTimeZoneID" + ex.Message);
                return bRet;
            }
            return bRet;
        }
        #endregion

        #region SaveUpdatePublicRoomEndpoints
        /// <summary>
        /// SaveUpdatePublicRoomEndpoints
        /// </summary>
        /// <param name="iUID"></param>
        /// <param name="iEptID"></param>
        /// <param name="refInsert"></param>
        /// <param name="oldEpAddress"></param>
        /// <param name="oldEpLinerate"></param>
        /// <param name="epName"></param>
        /// <param name="epAddress"></param>
        /// <param name="epAddressType"></param>
        /// <param name="lineRateID"></param>
        /// <param name="epConnType"></param>
        /// <param name="iProfileID"></param>
        /// <param name="iIsDefault"></param>
        /// <param name="PublicEndPoint"></param>
        /// <returns></returns>
        internal int SaveUpdatePublicRoomEndpoints(ref int iUID, ref int iEptID, ref bool refInsert, string oldEpAddress, string oldEpLinerate, string epName, string epAddress, string epAddressType, string lineRateID, string epConnType, ref int iProfileID, ref int iIsDefault, int PublicEndPoint)
        {

            bool ret = false, bChange = false; ;
            int iRet = 1;
            string query = "";
            string profileName = "", eProtocol = "1", eMCUaddType = "1"; //FB 2594
            try 
            {
                if (iUID <= 0 && epAddress != "" && oldEpAddress == "")
                {
                    profileName = epName + "_IP";

                    if (epAddressType == "4") //FB 2594 Starts
                    {
                        profileName = epName + "_ISDN";
                        eProtocol = "2";
                        eMCUaddType = "4";
                    }
                    //FB 2594 Ends

                    //FB 2599 - Starts
                    if (epAddressType == "3")
                    {
                        profileName = epName + "_E164";
                    }
                    //FB 2599 - End

                    if (iEptID <= 0)
                        iEptID = GetMaxEndpointID();

                    if (iEptID <= 0)
                    {
                        logger.Trace("SaveUpdatePublicRooms : Endpoint ID is empty");
                        return -1;

                    }

                    query = " insert into Ept_List_D (endpointId,name,[address],addresstype,linerateid,"
                              + " videoequipmentid,connectiontype,deleted,isDefault,encrypted,isTelePresence,profileId,orgID,extendpoint,profileName,PublicEndPoint,bridgeid,MCUAddressType,protocol)" //FB 2594
                              + " values(" + iEptID.ToString() + ",'" + epName + "','" + epAddress + "'," + epAddressType + "," + lineRateID
                              + ",1," + epConnType + ",0," + iIsDefault.ToString() + ",0,0," + iProfileID.ToString() + "," + defaultOrgId + ",0,'" + profileName + "'," + PublicEndPoint + ",-1," + eMCUaddType + "," + eProtocol + ")"; //FB 2594

                    ret = NonSelectCommand(query);
                    if (!ret)
                    {
                        logger.Exception(100, "query failed. query =" + query);
                    }
                    bChange = true;

                }
                else if (iEptID > 0 && epAddress != "" && (oldEpAddress.Trim() != epAddress.Trim() || oldEpLinerate.Trim() != lineRateID.Trim()))
                {
                    query = " Update Ept_List_D Set name =  '" + epName + "',[address]=  '" + epAddress + "',linerateid=  '" + lineRateID + "',isDefault=  '" + iIsDefault.ToString() + "',profileId=  '" + iProfileID.ToString() + "', profileName= '" + profileName.ToString() + "'"
                              + " Where EndpointID = " + iEptID.ToString() + " and [address]= '" + oldEpAddress + "'";

                    ret = NonSelectCommand(query);
                    if (!ret)
                    {
                        logger.Exception(100, "query failed. query =" + query);
                    }
                    bChange = true;

                }
                else if (iEptID > 0 && epAddress == "" && oldEpAddress != "")
                {
                    query = " Delete From Ept_List_D Set "
                              + " Where EndpointID = " + iEptID.ToString() + " and [address]= '" + oldEpAddress + "'";

                    ret = NonSelectCommand(query);
                    if (!ret)
                    {
                        logger.Exception(100, "query failed. query =" + query);
                    }
                    iRet = 0;
                }

                if (bChange || (iUID > 0 && !bChange))
                {
                    iProfileID++;
                    iIsDefault--;
                }
            }
            catch (Exception ex)
            {
                logger.Trace("SaveUpdatePublicRoomEndpoints" + ex.Message);
                return -1;
            }
            return iRet;
        }

        internal void SaveUpdatePublicRoomEndpointsinConferences(NS_MESSENGER_CUSTOMIZATIONS.Room room, ref string errMsg, int myVRMLocationID)
        {
            string query = "";
            string sAddress = "", sLineRate = "";
            bool ret = false;
            int iConfID = -1, iAddressType = 1, iUID = -1;
            DataSet ds = new DataSet();
            string dsTable = "Ept_List_d";
            try
            {


                query = " Select confUID,addresstype,UID from Conf_Room_D where  RoomId =" + myVRMLocationID + " and endpointID is not null and StartDate > GETUTCDATE ( )";

                ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Exception(10, "SQL error");

                }
                if (ds.Tables[dsTable] != null)
                {

                    if (ds.Tables[dsTable].Rows.Count > 0)
                    {
                        for (int rwcnt = 0; rwcnt < ds.Tables[dsTable].Rows.Count; rwcnt++)
                        {
                            iConfID = -1; sAddress = ""; iAddressType = -1;

                            if (ds.Tables[dsTable].Rows[0]["confUID"] != null)
                                if (ds.Tables[dsTable].Rows[0]["confUID"].ToString().Trim() != "")
                                    Int32.TryParse(ds.Tables[dsTable].Rows[0]["confUID"].ToString().Trim(), out iConfID);

                            if (ds.Tables[dsTable].Rows[0]["addresstype"] != null)
                                if (ds.Tables[dsTable].Rows[0]["addresstype"].ToString().Trim() != "")
                                    Int32.TryParse(ds.Tables[dsTable].Rows[0]["addresstype"].ToString().Trim(), out iAddressType);

                            if (ds.Tables[dsTable].Rows[0]["UID"] != null)
                                if (ds.Tables[dsTable].Rows[0]["UID"].ToString().Trim() != "")
                                    Int32.TryParse(ds.Tables[dsTable].Rows[0]["UID"].ToString().Trim(), out iUID);

                            sAddress = room.ipAddress;
                            sLineRate = room.IPSpeed;
                            if (iAddressType == 4)
                            {
                                sAddress = room.isdnAddress;
                                sLineRate = room.ISDNSpeed;
                            }

                            query += " Update Conf_room_d set ipisdnaddress = '" + sAddress + "' and DefLineRate = '" + sLineRate + "' where UID = " + iUID;

                        }

                        ret = NonSelectCommand(query);
                        if (!ret)
                        {
                            logger.Exception(10, "SQL error");

                        }
                    }
                }


            }
            catch (Exception ex)
            {
                logger.Trace("SaveUpdatePublicRoomEndpoints :" + ex.StackTrace);
                errMsg = ex.Message;

            }

        }
        #endregion

        #region ConverttoGMT
        /// <summary>
        /// ConverttoGMT
        /// </summary>
        /// <param name="toConvertDate"></param>
        /// <param name="timeZone"></param>
        /// <returns></returns>
        internal DateTime ConverttoGMT(DateTime toConvertDate, int timeZone)
        {
            DateTime dRet = DateTime.MinValue;
            DataSet ds = new DataSet();
            string dsTable = "ConverttoGMT";
            string query = "";
            bool ret = false;
            try
            {
                if (timeZone > 0)
                {
                    query = "select dbo.changeToGMTTime(" + timeZone + ",cast('" + toConvertDate.ToString() + "' as datetime)) as ConvertedTime";

                    ret = SelectCommand(query, ref ds, dsTable);
                    if (!ret)
                    {
                        logger.Trace("ConverttoGMT query failed: " + query);
                        return dRet;
                    }

                    if (ds.Tables[dsTable].Rows.Count > 0)
                    {
                        if (ds.Tables[dsTable].Rows[0]["ConvertedTime"] != null)
                            if (ds.Tables[dsTable].Rows[0]["ConvertedTime"].ToString().Trim() != "")
                                DateTime.TryParse(ds.Tables[dsTable].Rows[0]["ConvertedTime"].ToString().Trim(), out dRet);
                    }
                }

            }
            catch (Exception ex)
            {
                logger.Trace("ConverttoGMT" + ex.Message);
                return dRet;
            }
            return dRet;
        }
        #endregion

        #region GetTimeZoneID
        /// <summary>
        /// GetTimeZoneID
        /// </summary>
        /// <param name="offset"></param>
        /// <param name="DST"></param>
        /// <returns></returns>
        internal int GetTimeZoneID(double offset, int DST)
        {
            int timezoneID = -1, baseDST = -1, bRet = -1;
            double baseOffset = 0;
            DataSet ds = new DataSet();
            string query = "", dsTable = "Gen_TimeZone_d";
            NS_MESSENGER.timeZoneDetails tDetails = null;
            bool ret = false;
            try
            {
                if (timeZoneArray == null)
                {

                    query = "select TimeZoneID,Offset,DST from Gen_TimeZone_S ";

                    ret = SelectCommand(query, ref ds, dsTable);
                    if (!ret)
                    {
                        logger.Trace("GetTimeZoneID query failed: " + query);
                        return (-1);
                    }

                    if (ds.Tables[dsTable].Rows.Count > 0)
                    {

                        if (timeZoneArray == null)
                            timeZoneArray = new List<NS_MESSENGER.timeZoneDetails>();

                        for (int i = 0; i < ds.Tables[dsTable].Rows.Count; i++)
                        {
                            tDetails = new NS_MESSENGER.timeZoneDetails();
                            timezoneID = -1; baseOffset = -24; baseDST = -1;

                            tDetails.iTimeZoneID = timezoneID;
                            tDetails.iOffset = baseOffset;
                            tDetails.iDST = baseDST;

                            if (ds.Tables[dsTable].Rows[i]["TimeZoneID"] != null)
                                if (ds.Tables[dsTable].Rows[i]["TimeZoneID"].ToString().Trim() != "")
                                    Int32.TryParse(ds.Tables[dsTable].Rows[i]["TimeZoneID"].ToString().Trim(), out timezoneID);
                            if (ds.Tables[dsTable].Rows[i]["Offset"] != null)
                                if (ds.Tables[dsTable].Rows[i]["Offset"].ToString().Trim() != "")
                                    Double.TryParse(ds.Tables[dsTable].Rows[i]["Offset"].ToString().Trim(), out baseOffset);
                            if (ds.Tables[dsTable].Rows[i]["DST"] != null)
                                if (ds.Tables[dsTable].Rows[i]["DST"].ToString().Trim() != "")
                                    Int32.TryParse(ds.Tables[dsTable].Rows[i]["DST"].ToString().Trim(), out baseDST);

                            if (timezoneID > 0 && baseDST >= 0)
                            {
                                tDetails.iTimeZoneID = timezoneID;
                                tDetails.iOffset = baseOffset;
                                tDetails.iDST = baseDST;
                            }

                            timeZoneArray.Add(tDetails);
                        }

                    }
                }

                bRet = timeZoneArray.Where(tzone => (tzone.iOffset == offset && tzone.iDST == DST)).FirstOrDefault().iTimeZoneID;

            }
            catch (Exception ex)
            {
                logger.Trace("GetTimeZoneID" + ex.Message);
                return -1;
            }
            return bRet;
        }
        #endregion

        #region FetchSysSettingsDetails
        /// <summary>
        /// FetchSysSettingsDetails
        /// </summary>
        /// <param name="SysSettings"></param>
        /// <returns></returns>
        internal bool FetchSysSettingsDetails(ref NS_MESSENGER.SysSettings SysSettings)
        {
            try
            {
                string Query = "";
                DataSet ds = new DataSet();
                string dsTable = "Sys_Settings_D";

                Query = " Select * from Sys_WhygoSettings_D";
                bool ret = SelectCommand(Query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Trace("FetchSysSettingsDetails query failed: " + Query);
                    return (false);
                }

                if (ds.Tables[dsTable].Rows.Count > 0)
                {
                    if (SysSettings == null)
                        SysSettings = new NS_MESSENGER.SysSettings();

                    if (ds.Tables[dsTable].Rows[0]["WhyGoURL"] != null)
                        SysSettings.WhyGoURL = ds.Tables[dsTable].Rows[0]["WhyGoURL"].ToString();

                    if (ds.Tables[dsTable].Rows[0]["WhyGoUserName"] != null)
                        SysSettings.WhyGoUserName = ds.Tables[dsTable].Rows[0]["WhyGoUserName"].ToString();

                    if (ds.Tables[dsTable].Rows[0]["WhyGoPassword"] != null)
                    {
                        cryptography.Crypto crypto = new Crypto(); //FB 3054
                        SysSettings.WhyGoPassword = crypto.decrypt(ds.Tables[dsTable].Rows[0]["WhyGoPassword"].ToString());
                    }

                    //ZD 100694 Starts
                    if (ds.Tables[dsTable].Rows[0]["WhyGoAdminEmail"] != null)
                        SysSettings.WhygoAdminEmail = ds.Tables[dsTable].Rows[0]["WhyGoAdminEmail"].ToString();
                    //ZD 100694 Ends


                }
            }
            catch (Exception ex)
            {
                logger.Trace("" + ex.StackTrace);
                return false;
            }
            return true;
        }
        #endregion

        #region Fetch room for WHYGO
        /*
        /// <summary>
        /// Fetch room for WHYGO
        /// </summary>
        /// <param name="confid"></param>
        /// <param name="instanceid"></param>
        /// <param name="publicRooms"></param>
        /// <param name="privateRooms"></param>
        /// <returns></returns>
        public bool FetchRoomsforWhygo(int confid, int instanceid, ref List<inBookingLocation> publicRooms, ref List<inBookingLocationPrivate> privateRooms)
        {
            DataSet ds = new DataSet();
            string dsTable = "Rooms";
            StringBuilder query = new StringBuilder();
            bool ret = false;
            inBookingLocation publicRoom = null;
            inBookingLocationPrivate privateRoom = null;
            int myVRMLocationID = -1;
            int whygoLocationID = -1;
            NS_MESSENGER_CUSTOMIZATIONS.GenericRoom genericRoom = null;
            try
            {
                query.Append(" Select e.[name] as endpointName,c.ipisdnaddress,c.DefLineRate");
                query.Append(" ,c.AddressType,c.roomid,c.bridgeid,c.confuId,r.timezoneid,r.RoomPhone,u.firstName +' '+ u.LastName  as AdminName,u.email");
                query.Append(" from Loc_Room_D r,Conf_Room_D c,Ept_List_D e,Usr_List_D u");
                query.Append(" where c.confid = " + confid.ToString());
                query.Append(" and c.instanceid = " + instanceid.ToString());
                query.Append(" and c.roomid = r.roomid");
                query.Append(" and r.endpointid = e.endpointid");
                query.Append(" and c.profileid = e.profileid");
                query.Append(" and u.userid = r.assistant");

                ret = SelectCommand(query.ToString(), ref ds, dsTable);
                if (!ret)
                {
                    logger.Trace("FetchRoomsforWhygo query failed: " + query.ToString());
                    return (false);
                }

                for (int i = 0; i < ds.Tables[dsTable].Rows.Count; i++)
                {
                    try
                    {
                        myVRMLocationID = int.Parse(ds.Tables[dsTable].Rows[i]["roomid"].ToString());
                        whygoLocationID = GetPublicRoomID(myVRMLocationID);
                        genericRoom = new NS_MESSENGER_CUSTOMIZATIONS.GenericRoom();
                        //if (ds.Tables[dsTable].Rows[i]["endpointName"] != null)
                        //    genericRoom.nam = ds.Tables[dsTable].Rows[i]["endpointName"].ToString();
                        if (ds.Tables[dsTable].Rows[i]["AddressType"] != null)
                        {
                            if (ds.Tables[dsTable].Rows[i]["AddressType"].ToString() == "1") //ZD 100729
                                genericRoom.transmissionType = "IP";
                            else
                                genericRoom.transmissionType = "ISDN";
                        }

                        if (ds.Tables[dsTable].Rows[i]["bridgeid"] != null)
                            if (ds.Tables[dsTable].Rows[i]["bridgeid"].ToString().Trim() != "" && Int32.Parse(ds.Tables[dsTable].Rows[i]["bridgeid"].ToString().Trim()) > 0)
                                genericRoom.bridgeDetail = "bridge";
                        if (ds.Tables[dsTable].Rows[i]["RoomPhone"] != null)
                            genericRoom.contactPhone = ds.Tables[dsTable].Rows[i]["RoomPhone"].ToString();
                        if (ds.Tables[dsTable].Rows[i]["AdminName"] != null)
                            genericRoom.contactName = ds.Tables[dsTable].Rows[i]["AdminName"].ToString();
                        if (ds.Tables[dsTable].Rows[i]["email"] != null)
                            genericRoom.contactEmail = ds.Tables[dsTable].Rows[i]["email"].ToString();
                        if (ds.Tables[dsTable].Rows[i]["DefLineRate"] != null)
                            genericRoom.transmissionSpeed = ds.Tables[dsTable].Rows[i]["DefLineRate"].ToString();

                        if (ds.Tables[dsTable].Rows[i]["timezoneid"] != null)
                            if (ds.Tables[dsTable].Rows[i]["timezoneid"].ToString().Trim() != "")
                                genericRoom.timeZoneId = Int32.Parse(ds.Tables[dsTable].Rows[i]["timezoneid"].ToString().Trim());

                        if (whygoLocationID > 0)
                        {
                            publicRoom = new inBookingLocation();
                            publicRoom.locationID = whygoLocationID;
                            publicRoom.ipOrISDN = genericRoom.transmissionType;
                            publicRoom.speed = genericRoom.transmissionSpeed;
                            publicRoom.numberOfAttendees = 0;
                            if (publicRooms == null)
                                publicRooms = new List<inBookingLocation>();
                            publicRoom = (inBookingLocation)SetPropertyValues((object)publicRoom);
                            publicRoom.equipment = new int[0];
                            publicRoom.catering = new int[0];
                            publicRooms.Add(publicRoom);
                        }
                        else
                        {
                            privateRoom = new inBookingLocationPrivate();
                            privateRoom.transmissionNumber = genericRoom.transmissionNumber;
                            privateRoom.transmissionSpeed = genericRoom.transmissionSpeed;
                            privateRoom.transmissionType = genericRoom.transmissionType;
                            privateRoom.timeZoneId = GetWhygoTimeZoneID(genericRoom.timeZoneId);
                            privateRoom.contactEmail = genericRoom.contactEmail;
                            privateRoom.contactName = genericRoom.contactName;
                            privateRoom.contactPhone = genericRoom.contactPhone;

                            if (privateRooms == null)
                                privateRooms = new List<inBookingLocationPrivate>();

                            privateRoom = (inBookingLocationPrivate)SetPropertyValues((object)privateRoom);

                            privateRooms.Add(privateRoom);

                        }
                    }
                    catch (Exception e)
                    {
                        logger.Exception(100, e.Message);
                        return false;
                    }
                }
                return (true);
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }

        }
        #endregion

        #region Fetch Conf for Whygo
        /// <summary>
        /// Fetch Conf for Whygo
        /// </summary>
        /// <param name="ConfID"></param>
        /// <param name="InstanceID"></param>
        /// <param name="confs"></param>
        /// <returns></returns>
        internal bool FetchConfforWhygo(int ConfID, int InstanceID, ref List<NS_WhyGO.whyGOConference> confs)
        {
            NS_WhyGO.whyGOConference conf = null;
            int iNoofAttendee = 0;
            DataSet ds2 = new DataSet();
            string dsTable2 = "ConfHost";
            string query2 = "";
            try
            {
                // Retreive conf details
                DataSet ds = new DataSet();
                string dsTable = "Conference";

                string query = null; bool ret = false;

                query = "select c.*,(select COUNT(userid) from Conf_User_D U where C.confid = u.ConfID and c.instanceid = u.InstanceId ) as attendeeCount"
                        + " from Conf_Conference_D C where c.confid = " + ConfID.ToString();

                if (InstanceID > 0)
                    query += " and c.instanceid =  " + InstanceID.ToString();


                ret = false;
                ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Exception(100, "SQL error");
                    return (false);
                }

                #region Build Conferences object
                for (int i = 0; i < ds.Tables[dsTable].Rows.Count; i++)
                {
                    iNoofAttendee = 0;
                    conf = new NS_WhyGO.whyGOConference();
                    conf.iOrgID = Int32.Parse(ds.Tables[dsTable].Rows[i]["orgId"].ToString());
                    conf.iDbID = Int32.Parse(ds.Tables[dsTable].Rows[i]["ConfId"].ToString());
                    conf.iInstanceID = Int32.Parse(ds.Tables[dsTable].Rows[i]["InstanceId"].ToString());
                    conf.iDbNumName = Int32.Parse(ds.Tables[dsTable].Rows[i]["ConfNumName"].ToString());
                    conf.sExternalName = ds.Tables[dsTable].Rows[i]["ExternalName"].ToString();
                    conf.iHostUserID = Int32.Parse(ds.Tables[dsTable].Rows[i]["owner"].ToString());
                    conf.sExternalSchedulingID = ds.Tables[dsTable].Rows[i]["ESId"].ToString();
                    conf.sExternalSchedulingType = ds.Tables[dsTable].Rows[i]["ESType"].ToString();

                    conf.isVIP = false;
                    if (ds.Tables[dsTable].Rows[i]["isVIP"].ToString() == "1")
                        conf.isVIP = true;

                    try
                    {

                        conf.sPwd = ds.Tables[dsTable].Rows[i]["password"].ToString().Trim();
                        conf.iPwd = Int32.Parse(conf.sPwd);
                    }
                    catch (Exception)
                    {
                        conf.sPwd = "";
                        conf.iPwd = 0;
                    }

                    try
                    {
                        conf.iNoofAttendee = Int32.Parse(ds.Tables[dsTable].Rows[i]["attendeeCount"].ToString().Trim());
                    }
                    catch (Exception)
                    {
                        conf.iNoofAttendee = 0;
                    }

                    try
                    {
                        conf.iDeleted = Int32.Parse(ds.Tables[dsTable].Rows[i]["deleted"].ToString().Trim());
                        conf.iPushed = Int32.Parse(ds.Tables[dsTable].Rows[i]["PushedToExternal"].ToString().Trim());
                    }
                    catch (Exception)
                    {
                        conf.iDeleted = 0;
                        conf.iPushed = 0;
                    }
					//ZD 100085
                    conf.iDuration = Int32.Parse(ds.Tables[dsTable].Rows[i]["Duration"].ToString());
                    logger.Trace("Conf Duration : " + conf.iDuration.ToString());

                    //lastrundatetime
                    conf.dtModifiedDateTime = DateTime.Parse(ds.Tables[dsTable].Rows[i]["LastRunDateTime"].ToString());

                    // start date & time 
                    conf.dtSetUpDateTime = DateTime.Parse(ds.Tables[dsTable].Rows[i]["SetupTime"].ToString());
                    //conf.dtEndDateTime = DateTime.Parse(ds.Tables[dsTable].Rows[i]["TearDownTime"].ToString());//ZD 100085
                    //ZD 104332 starts
                    if (conf.iImmediate == 1)
                        conf.dtStartDateTime = DateTime.Parse(ds.Tables[dsTable].Rows[i]["confdate"].ToString());
                    else
                        conf.dtStartDateTime = DateTime.Parse(ds.Tables[dsTable].Rows[i]["confdate"].ToString()).AddSeconds(-40); //FB 2440 //5 secs buffer
                    //ZD 104332 ends

                    conf.dtEndDateTime = conf.dtStartDateTime.AddMinutes(conf.iDuration);
                    conf.iTimezoneID = Int32.Parse(ds.Tables[dsTable].Rows[i]["Timezone"].ToString());
                    //ZD 104332 commented
                    //conf.dtStartDateTime = conf.dtStartDateTime.AddSeconds(-20); //FB 3043 WhyGo - Modify 45 second buffer for network delays down to 25 seconds
					//ZD 100085 Starts
                    //conf.iDuration = Int32.Parse(ds.Tables[dsTable].Rows[i]["Duration"].ToString());
                    //logger.Trace("Conf Duration : " + conf.iDuration.ToString());
					//ZD 100085 End
                    conf.sZStartTime = conf.dtStartDateTime.ToString("s").Trim() + "Z";
                    conf.sZEndTime = conf.dtStartDateTime.AddMinutes(conf.iDuration).ToString("s").Trim() + "Z";
                    
                    conf.iconfMode = Int32.Parse(ds.Tables[dsTable].Rows[i]["confMode"].ToString()); //ZD 100694

                    #region Fetch the host email
                    query2 = "Select Email,FirstName+ ' '+LastName as HostName,Password from Usr_List_D where userid = " + conf.iHostUserID.ToString(); //ZD 100729
                    bool ret3 = SelectCommand(query2, ref ds2, dsTable2);
                    if (!ret3)
                    {
                        logger.Exception(100, "SQL error");
                        return (false);
                    }

                    if (ds2.Tables[dsTable2].Rows.Count < 1)
                    {
                        // no record 
                        logger.Trace("No host email found.");
                        return false;
                    }
                    conf.sHostEmail = ds2.Tables[dsTable2].Rows[0]["Email"].ToString();
                    conf.sHostName = ds2.Tables[dsTable2].Rows[0]["HostName"].ToString();
                    //ZD 100729 Starts
                    if (ds2.Tables[dsTable2].Rows[0]["Password"] != null && ds2.Tables[dsTable2].Rows[0]["Password"].ToString().Length > 0)
                    {
                        cryptography.Crypto crypto = new Crypto();
                        conf.sHostPassword = crypto.decrypt(ds2.Tables[dsTable2].Rows[0]["Password"].ToString());
                    }
                    //ZD 100729 Starts
                    #endregion

                    //check conf type
                    int confType = 0;
                    confType = Int32.Parse(ds.Tables[dsTable].Rows[i]["ConfType"].ToString()); //dbo.Conference.ConfType				
                    logger.Trace("Conf Type : " + confType.ToString());
                    switch (confType)
                    {
                        case 2:
                            {
                                // audio-video conference
                                conf.etMediaType = NS_MESSENGER.Conference.eMediaType.AUDIO_VIDEO;
                                conf.etType = NS_MESSENGER.Conference.eType.MULTI_POINT;
                                break;
                            }
                        case 6:
                            {
                                // audio conference
                                conf.etMediaType = NS_MESSENGER.Conference.eMediaType.AUDIO;
                                conf.etType = NS_MESSENGER.Conference.eType.MULTI_POINT;
                                break;
                            }
                        case 4:
                            {
                                // p2p conference
                                conf.etMediaType = NS_MESSENGER.Conference.eMediaType.AUDIO_VIDEO;
                                conf.etType = NS_MESSENGER.Conference.eType.POINT_TO_POINT;
                                break;
                            }
                        default:
                            {   // 7 = room conference
                                conf.etMediaType = NS_MESSENGER.Conference.eMediaType.AUDIO;
                                conf.etType = NS_MESSENGER.Conference.eType.ROOM_CONFERENCE;
                                break;
                            }
                    }

                    if (confs == null)
                        confs = new List<NS_WhyGO.whyGOConference>();

                    confs.Add(conf);
                }
                #endregion
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }

            return true;
        }*/
        #endregion

        #region UpdateConferenceforWhygo
        /// <summary>
        /// UpdateConferenceforEvent
        /// </summary>
        /// <param name="evnt"></param>
        /// <param name="EStype"></param>
        /// <returns></returns>
        internal bool UpdateConferenceforWhygo(int confid, int instanceid, string esID, string EStype)
        {
            try
            {
                bool ret = false;
                string query = "";
                string pushed = "1";

                query = "update conf_conference_d set EsId = '" + esID + "',ESType = '" + EStype + "',PushedToExternal = " + pushed + "  where confid = " + confid.ToString() + " and instanceid = " + instanceid.ToString();
                ret = NonSelectCommand(query);
                if (!ret)
                {
                    logger.Trace("UpdateConferenceforWhygo failed: " + query);
                    return false;
                }
            }
            catch (Exception ex)
            {
                logger.Trace("UpdateConferenceforWhygo :" + ex.Message);
                return false;
            }
            return true;
        }
        #endregion

        #region GetWhygoUserDetails
        /*
        /// <summary>
        /// GetWhygoUserDetials
        /// </summary>
        /// <param name="myVRMUserID"></param>
        /// <param name="whygoUserID"></param>
        /// <param name="whygoCurrencyType"></param>
        /// <returns></returns>
        internal int GetWhygoUserDetails(int myVRMUserID, ref int whygoUserID, ref string whygoCurrencyType)
        {
            int bRet = 1;
            DataSet ds = new DataSet();
            string query = "", dsTable = "ES_PublicRoom_d";
            int iwhygoCurrencyType = 1;
            whygoUserID = 21;
            whygoCurrencyType = "AUD";
            bool ret = false;
            try
            {
                if (myVRMUserID > 0)
                {
                    query = "select ESUserID,Region from Usr_List_d where UserID = " + myVRMUserID.ToString();

                    ret = SelectCommand(query, ref ds, dsTable);
                    if (!ret)
                    {
                        logger.Exception(10, "SQL error");
                        return (-1);
                    }

                    if (ds.Tables[dsTable].Rows.Count > 0)
                    {
                        if (ds.Tables[dsTable].Rows[0]["ESUserID"] != null)
                            if (ds.Tables[dsTable].Rows[0]["ESUserID"].ToString().Trim() != "")
                                Int32.TryParse(ds.Tables[dsTable].Rows[0]["ESUserID"].ToString().Trim(), out whygoUserID);

                        if (ds.Tables[dsTable].Rows[0]["Region"] != null)
                            if (ds.Tables[dsTable].Rows[0]["Region"].ToString().Trim() != "")
                                Int32.TryParse(ds.Tables[dsTable].Rows[0]["Region"].ToString().Trim(), out iwhygoCurrencyType);

                        if (iwhygoCurrencyType > 0)
                            whygoCurrencyType = Enum.GetName(typeof(NS_WhyGO.WhyGo.CurrencyType), iwhygoCurrencyType);

                    }
                }

            }
            catch (Exception ex)
            {
                logger.Trace("GetWhygoUserDetails" + ex.Message);
                return -1;
            }
            return bRet;
        }*/
        #endregion

        //FB 2392 end

        //FB 2636 
        public bool FetchBridgeDetails(int mcu, ref NS_MESSENGER.Conference conf)
        {
            int Dialnumber = 0;
            DataSet ds = new DataSet();
            string dsTable = "Conference";
            string query = "Select * from Conf_Bridge_D where confid = '" + conf.iDbID.ToString() + "' and instanceid = '" + conf.iInstanceID.ToString() + "' and BridgeID = " + mcu.ToString();//FB 2584
            bool ret = SelectCommand(query, ref ds, dsTable);
            if (!ret)
            {
                logger.Exception(10, "SQL error");
                return (false);
            }
            for (int i = 0; i < ds.Tables[dsTable].Rows.Count; i++)
            {
                try
                {
                    if (ds.Tables[dsTable].Rows[i]["E164Dialnumber"] != null)
                    {
                        if (ds.Tables[dsTable].Rows[i]["E164Dialnumber"].ToString() != "")
                        {
                            //FB 2659
                            conf.sDialinNumber = ds.Tables[dsTable].Rows[i]["E164Dialnumber"].ToString();
                        }
                    }
					 //FB 2839 Start
                    if (ds.Tables[dsTable].Rows[i]["ConfRMXServiceID"] != null)
                    {
                        if (ds.Tables[dsTable].Rows[i]["ConfRMXServiceID"].ToString() != "")
                        {
                            Dialnumber = Int32.Parse(ds.Tables[dsTable].Rows[i]["ConfRMXServiceID"].ToString());
                            conf.iConfRMXServiceID = Dialnumber;
                        }
                    }
                    //FB 2839 End

                    //ZD 104256 Starts
                    Dialnumber = 0;
                    if (ds.Tables[dsTable].Rows[i]["PoolOrderID"] != null)
                    {
                        if (ds.Tables[dsTable].Rows[i]["PoolOrderID"].ToString() != "")
                        {
                            Dialnumber = Int32.Parse(ds.Tables[dsTable].Rows[i]["PoolOrderID"].ToString());
                            conf.iConfPoolOrderID = Dialnumber;
                        }
                    }
                    //ZD 104256 Ends
                }
                catch (Exception e)
                {
                    logger.Exception(100, e.Message);
                }
            }
            return true;
        }
        //FB 2595 Start

        #region UpdateConferenceSwitching
        /// <summary>
        /// UpdateConferenceSwitching
        /// </summary>
        /// <param name="confid"></param>
        /// <param name="instanceid"></param>
        /// <param name="Switching"></param>
        /// <returns></returns>
        public bool UpdateConferenceSwitching(int confid, int instanceid, int Switching)
        {
            string query = "update Conf_Conference_D set NetworkSwitch = " + Switching.ToString();
            query += " where confid = " + confid.ToString();
            query += " and instanceid = " + instanceid.ToString();

            bool ret = NonSelectCommand(query);
            if (!ret)
            {
                logger.Exception(100, "Error executing query.");
                return false;
            }
            return true;
        }
        #endregion

        #region CheckMailLogo
        /// <summary>
        /// CheckMailLogo
        /// </summary>
        /// <param name="orgId"></param>
        /// <returns></returns>
        public bool CheckMailLogo(int orgId)
        {
            try
            {
                DataSet ds = new DataSet();
                string dsTable = "OrgSettings";
                string Query = " Select MailLogo from Org_settings_d where OrgId = '" + orgId + "' ";
                bool ret = SelectCommand(Query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Trace("Query Failed" + Query);
                    return false;
                }
                int mailLogo = 0;
                for (int i = 0; i < ds.Tables[dsTable].Rows.Count; i++)
                {
                    if (ds.Tables[dsTable].Rows[i]["MailLogo"] != null)
                    {
                        if (ds.Tables[dsTable].Rows[i]["MailLogo"].ToString() != "")
                        {
                            int.TryParse(ds.Tables[dsTable].Rows[i]["MailLogo"].ToString(), out mailLogo);
                        }
                    }
                }
                if (mailLogo > 0)
                    return true;
                else
                    return false;
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion

        #region FetchHardwareAdminEmail
        /// <summary>
        /// FetchHardwareAdminEmail
        /// </summary>
        /// <param name="OrgId"></param>
        /// <param name="hadrwareAdminEmail"></param>
        /// <returns></returns>
        public bool FetchHardwareAdminEmail(String OrgId, ref String hadrwareAdminEmail)
        {

            DataSet ds = new DataSet();
            string dsTable = "Organization";
            string query = "Select HardwareAdminEmail from Org_Settings_D where OrgId = '" + OrgId + "'";
            bool ret = SelectCommand(query, ref ds, dsTable);
            if (!ret)
            {
                logger.Exception(10, "SQL error");
                return (false);
            }
            for (int i = 0; i < ds.Tables[dsTable].Rows.Count; i++)
            {
                try
                {
                    if (ds.Tables[dsTable].Rows[i]["HardwareAdminEmail"] != null)
                    {
                        if (ds.Tables[dsTable].Rows[i]["HardwareAdminEmail"].ToString() != "")
                        {
                            hadrwareAdminEmail = ds.Tables[dsTable].Rows[i]["HardwareAdminEmail"].ToString();
                        }
                    }
                }
                catch (Exception e)
                {
                    logger.Exception(100, e.Message);
                }
            }
            return true;
        }
        #endregion

        //FB 2595 End
        //FB 2441 Starts
        #region UpdateConfDailString
        /// <summary>
        /// UpdateConfDailString
        /// </summary>
        /// <param name="conf"></param>
        /// <returns></returns>
        public bool UpdateConfDailString(NS_MESSENGER.Conference conf)
        {

            try
            {
                StringBuilder query = new StringBuilder();
                query.Append(" Update Conf_Conference_D set DialString = '" + conf.sDialString + "', Etag='" + conf.sEtag + "', PushedToExternal=1");
                query.Append(" where confid = " + conf.iDbID);
                query.Append(" and instanceid = " + conf.iInstanceID);

                bool ret = NonSelectCommand(query.ToString());
                if (!ret)
                {
                    logger.Exception(100, "UpdateConfDailString query failed: " + query);
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {

                logger.Exception(100, e.Message);
                return false;

            }
        }
        #endregion
        #region UpdateParticipantGUID
        /// <summary>
        /// UpdateParticipantGUID
        /// </summary>
        /// <param name="conf"></param>
        /// <returns></returns>
        public bool UpdateParticipantGUID(int confid, int instanceid, string address, string eID,int status) // ZD 101157 Changes in whole method
        {
            String query = ""; String queryupdt = "";
            String tempQuery = ""; String tempqueryupdt = "";
            DataSet ds = new DataSet();
            string dsTable = "endpoint";
            bool ret = false;
            string eptID = "";
            string tblName = "";
            try
            {
                //ZD 102901 Domain strip

                //query = "Select Uid from {0} where {1} confid = " + confid.ToString() + " and instanceid = " + instanceid.ToString() + " and ipisdnaddress = '" + address.Trim() + "'";


                query = "Select Uid from {0} where {1} confid = " + confid.ToString() + " and instanceid = " + instanceid.ToString() +
                    " and (Rtrim(Ltrim(ipisdnaddress)) = '" + address.Trim() + "'"+
                    " or Rtrim(Ltrim(SUBSTRING(ipisdnaddress,1,ISNULL(NULLIF(CHARINDEX('@',ipisdnaddress,1)-1,-1),0))))='" + address.Trim() + "')";

                
                queryupdt = "update {0} set GUID = '" + eID + "',onlinestatus =" + status +" where UID = {1}";

                tblName = "conf_room_d";
                tempQuery = string.Format(query, tblName, "");
                ret = SelectCommand(tempQuery, ref ds, dsTable);
                if (!ret)
                {
                    logger.Trace("SQL error");
                    return (false);
                }

                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[dsTable].Rows.Count > 0 && ds.Tables[dsTable].Rows.Count == 1) //ZD 104214
                    {
                        if (ds.Tables[dsTable].Rows[0]["Uid"] != null && ds.Tables[dsTable].Rows[0]["Uid"].ToString() != "")
                        {
                            eptID = ds.Tables[dsTable].Rows[0]["Uid"].ToString();
                            tempqueryupdt = string.Format(queryupdt, tblName, eptID);
                            ret = NonSelectCommand(tempqueryupdt);
                            if (!ret)
                            {
                                logger.Exception(100, "Error executing query.");
                                return false;
                            }

                            return true;

                        }
                    }
                }

                tblName = "conf_user_d";
                tempQuery = string.Format(query, tblName, " invitee = 1 and ");
                ret = SelectCommand(tempQuery, ref ds, dsTable);
                if (!ret)
                {
                    logger.Trace("SQL error");
                    return (false);
                }

                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[dsTable].Rows.Count > 0 && ds.Tables[dsTable].Rows.Count == 1) //ZD 104214
                    {
                        if (ds.Tables[dsTable].Rows[0]["Uid"] != null && ds.Tables[dsTable].Rows[0]["Uid"].ToString() != "")
                        {
                            eptID = ds.Tables[dsTable].Rows[0]["Uid"].ToString();
                            tempqueryupdt = string.Format(queryupdt, tblName, eptID);
                            ret = NonSelectCommand(tempqueryupdt);
                            if (!ret)
                            {
                                logger.Exception(100, "Error executing query.");
                                return false;
                            }

                            return true;

                        }
                    }
                }
                return false;

            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        
        #endregion

        #region FetchConfbridID
        internal bool FetchConfbridID(NS_MESSENGER.Conference conf,ref List<int> mcuid)
        {
            string query = "";
            bool ret = false;
            DataSet ds = new DataSet();
            string dsTable = "Bridge";
            int BridgeID = 0;
            mcuid = new List<int>();
            try
            {
                query = "select bridgeid from conf_bridge_d as BridgeID where confid=" + conf.iDbID + " and instanceid=" + conf.iInstanceID;
                
                ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Trace("SQL error");
                    return (false);
                }

                for (int i = 0; i < ds.Tables[dsTable].Rows.Count; i++)
                {
                    try
                    {
                        if (ds.Tables[dsTable].Rows[i]["BridgeID"] != null)
                        {
                            if (ds.Tables[dsTable].Rows[i]["BridgeID"].ToString() != "")
                            {
                                int.TryParse(ds.Tables[dsTable].Rows[i]["BridgeID"].ToString(),out BridgeID);
                                mcuid.Add(BridgeID);                            
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        logger.Exception(100, e.Message);
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion

        #region UpdatePartyMuteStatus
        /// <summary>
        /// UpdatePartyMuteStatus
        /// </summary>
        /// <param name="party"></param>
        /// <returns></returns>
        public bool UpdatePartyMuteStatus(NS_MESSENGER.Conference conf, List<NS_MESSENGER.Party> PartyList, int mute)
        {
            string query = "";
            try
            {
                query = " Update conf_user_d set mute = " + mute + "";
                query += " where confid = " + conf.iDbID + " and Instanceid = '" + conf.iInstanceID + "'";
                query += " Update conf_room_d set mute = '" + mute + "' ";
                query += " where confid = '" + conf.iDbID + "' and Instanceid = '" + conf.iInstanceID + "'";

                bool ret = NonSelectCommand(query);
                if (!ret)
                {
                    logger.Exception(100, "Error executing query." + query);
                    return false;
                }

                mute = 0;
                for (int i = 0; i < PartyList.Count; i++)
                {
                    query = " Update conf_user_d set mute = " + mute + "";
                    query += " where confid = '" + conf.iDbID + "' and Instanceid = '" + conf.iInstanceID + "'";
                    query += " and IPISDNAddress = '" + PartyList[i].sAddress + "'"; //ZD 102817
                    if (PartyList[i].sGUID != "")
                        query += " and GUID = '" + PartyList[i].sGUID + "'"; //ZD 102817

                    query += " Update conf_room_d set mute = '" + mute + "' ";
                    query += " where confid = '" + conf.iDbID + "' and Instanceid = '" + conf.iInstanceID + "'";
                    query += " and IPISDNAddress = '" + PartyList[i].sAddress + "'"; //ZD 102817
                    if (PartyList[i].sGUID != "")
                        query += " and GUID = '" + PartyList[i].sGUID + "'"; //ZD 102817
                }

                ret = NonSelectCommand(query);
                if (!ret)
                {
                    logger.Exception(100, "Error executing query." + query);
                    return false;
                }
            }
            catch (Exception ex)
            {
                logger.Trace("UpdatePartyMuteStatus :" + ex.Message);
            }
            return true;
        }
        #endregion

        //FB 2441 Ends

        //FB 2660 Starts

        #region FetchCDRMcu
        /// <summary>
        /// FetchCDRMcu
        /// </summary>
        /// <param name="cMcuList"></param>
        /// <returns></returns>
        internal bool FetchCDRMcu(ref List<NS_MESSENGER.MCU> cMcuList)
        {
            NS_MESSENGER.MCU cMcu = null;
            int iMcuType = 0;
            string dsTable = "", sQuery = "";
            bool ret = false;
            try
            {
                DataSet ds = new DataSet();
                dsTable = "MCU";

                sQuery = "Select * from Mcu_List_D where  virtualbridge = 0 and status =1 and deleted = 0 and EnableCDR = 1";
                ret = SelectCommand(sQuery, ref ds, dsTable);
                if (!ret)
                {
                    logger.Exception(100, "SQL error");
                    return (false);
                }

                if (ds.Tables[dsTable].Rows.Count < 1)
                {
                    logger.Trace("No bridge found...");
                    return false;
                }

                for (int i = 0; i < ds.Tables[dsTable].Rows.Count; i++)
                {
                    cMcu = new NS_MESSENGER.MCU();

                    cMcu.iDbId = int.Parse(ds.Tables[dsTable].Rows[i]["bridgeid"].ToString().Trim());
                    //FB 2593 Starts
                    cMcu.sName = ds.Tables[dsTable].Rows[i]["BridgeName"].ToString();
                    cMcu.sIp = ds.Tables[dsTable].Rows[i]["BridgeAddress"].ToString().Trim();
                    cMcu.sPwd = ds.Tables[dsTable].Rows[i]["BridgePassword"].ToString().Trim();
                    if (cMcu.sPwd != null && cMcu.sPwd.Length > 0)
                    {
                        cryptography.Crypto crypto = new Crypto();
                        cMcu.sPwd = crypto.decrypt(cMcu.sPwd);
                    }
                    //FB 2593 End
                    cMcu.sLogin = ds.Tables[dsTable].Rows[i]["BridgeLogin"].ToString().Trim();
                    cMcu.iTimezoneID = int.Parse(ds.Tables[dsTable].Rows[i]["timezone"].ToString().Trim());
                    cMcu.iDeleteCDRDays = int.Parse(ds.Tables[dsTable].Rows[i]["DeleteCDRDays"].ToString().Trim());
                    cMcu.iEnableCDR = int.Parse(ds.Tables[dsTable].Rows[i]["EnableCDR"].ToString().Trim());
                    cMcu.iOrgId = int.Parse(ds.Tables[dsTable].Rows[i]["orgId"].ToString().Trim()); //FB 2593
                    iMcuType = 0;
                    iMcuType = int.Parse(ds.Tables[dsTable].Rows[i]["BridgeTypeId"].ToString().Trim());
                    cMcu.sMcuType = iMcuType;

                    //FB 2683 Starts
                    cMcu.sDMAip = ds.Tables[dsTable].Rows[i]["DMAURL"].ToString().Trim();
                    cMcu.sDMALogin = ds.Tables[dsTable].Rows[i]["DMALogin"].ToString().Trim();
                    cMcu.sDMAPassword = ds.Tables[dsTable].Rows[i]["DMAPassword"].ToString().Trim();
                    if (cMcu.sDMAPassword != null && cMcu.sDMAPassword.Length > 0)
                    {
                        cryptography.Crypto crypto = new Crypto();
                        cMcu.sDMAPassword = crypto.decrypt(cMcu.sDMAPassword);
                    }
                    int port = 0;
                    int.TryParse(ds.Tables[dsTable].Rows[i]["DMAPort"].ToString().Trim(), out port);
                    cMcu.iDMAHttpport = port;

                    //FB 2683 Ends

                    //ZD 100768
                    cMcu.sBridgeExtNo = "";
                    if (ds.Tables[dsTable].Columns.Contains("BridgeExtNo") && ds.Tables[dsTable].Rows[i]["BridgeExtNo"] != null)
                        cMcu.sBridgeExtNo = ds.Tables[dsTable].Rows[i]["BridgeExtNo"].ToString();

                    switch (iMcuType)
                    {
                        case 1:
                        case 2:
                        case 3:
                            {
                                cMcu.dblSoftwareVer = Double.Parse(cMcu.sSoftwareVer);
                                if (cMcu.dblSoftwareVer < 7)
                                    cMcu.etType = NS_MESSENGER.MCU.eType.ACCORDv6;
                                else
                                    cMcu.etType = NS_MESSENGER.MCU.eType.ACCORDv7;
                                break;
                            }
                        case 4:
                        case 5:
                        case 6:
                            {
                                cMcu.etType = NS_MESSENGER.MCU.eType.CODIAN;
                                break;
                            }
                        case 7:
                            {
                                cMcu.dblSoftwareVer = 0.0;
                                cMcu.etType = NS_MESSENGER.MCU.eType.TANDBERG;
                                break;
                            }
                        //ZD 103787- It is for polycom RMX MCU  
                        case 17:
                        case 18:
                        case 19:
                        case 8:
                            {
                                cMcu.dblSoftwareVer = 0.0;
                                cMcu.etType = NS_MESSENGER.MCU.eType.RMX;
                                break;
                            }
                        
                        case 9:
                            {
                                cMcu.dblSoftwareVer = 0.0;
                                cMcu.etType = NS_MESSENGER.MCU.eType.RADVISION;
                                break;
                            }
                        case 10:
                            {
                                cMcu.dblSoftwareVer = 0.0;
                                cMcu.etType = NS_MESSENGER.MCU.eType.CMA;
                                break;
                            }
                        case 11:
                            {
                                cMcu.dblSoftwareVer = 0.0;
                                cMcu.etType = NS_MESSENGER.MCU.eType.Lifesize;
                                break;
                            }
                        case 12:
                            {
                                cMcu.dblSoftwareVer = 0.0;
                                cMcu.etType = NS_MESSENGER.MCU.eType.CISCOTP;
                                break;
                            }
                        case 13: //FB 2683
                            {
                                cMcu.dblSoftwareVer = 0.0;
                                cMcu.etType = NS_MESSENGER.MCU.eType.RPRM;
                                break;
                            }
                        case 20: //ZD 104021
                            {
                                cMcu.dblSoftwareVer = 0.0;
                                cMcu.etType = NS_MESSENGER.MCU.eType.BLUEJEANS;
                                break;
                            }
                        default:
                            {
                                logger.Trace("Unknown MCU type.");
                                return false;
                            }
                    }

                    if (cMcuList == null)
                        cMcuList = new List<NS_MESSENGER.MCU>();
                    if (cMcuList != null)
                        cMcuList.Add(cMcu);
                }

                return (true);
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion

        #region DeleteCDRRecord
        /// <summary>
        /// DeleteCDRRecord
        /// </summary>
        /// <param name="bridgeID"></param>
        /// <param name="DeleteCDRDays"></param>
        /// <param name="MCUCDRTable"></param>
        /// <returns></returns>
        internal bool DeleteCDRRecord(int bridgeID,int DeleteCDRDays, string MCUCDRTable,int bridgetypeid)//FB 2683
        {
            try
            {
                string query = ""; //string dsTable = "";
                bool ret = false; 
                //DataSet ds = new DataSet();
                DateTime DeleteCDRDate = DateTime.Now;
				//FB 2944
                /*int i = 0;
                query = "SELECT SYSUTCDATETIME()";
                dsTable = "SysSettings";
                ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Exception(10, "SQL error");
                    return (false);
                }
                for (i = 0; i < ds.Tables[dsTable].Rows.Count; i++)
                {
                    if (ds.Tables[dsTable].Rows[i][0] != null)
                        DateTime.TryParse(ds.Tables[dsTable].Rows[i][0].ToString().Trim(), out DeleteCDRDate);
                }
                */

                //DeleteCDRDate = DeleteCDRDate.AddDays(-DeleteCDRDays);
                DeleteCDRDate = DateTime.UtcNow.AddDays(-DeleteCDRDays); //FB 2593

                if (bridgeID > 0)
                {
                    //dsTable = "ConfMCUCDR";

                    query = "Delete from Conf_CDR_logs_D where McuId = " + bridgeID + " and EventDate < '" + DeleteCDRDate + "'";
                    ret = NonSelectCommand(query);
                    if (!ret)
                    {
                        logger.Trace("SQL error");
                        return false;
                    }
                }
                //FB 2683 Starts
                if (bridgetypeid == 13)
                {
                    query = "Delete from cdr_polycomRPRMconference_d where  starttime < '" + DeleteCDRDate + "';Delete from cdr_polycomRPRMcalls_d where  starttime < '" + DeleteCDRDate + "'";
                    ret = NonSelectCommand(query);
                    if (!ret)
                    {
                        logger.Trace("SQL error");
                        return false;
                    }
                }
                //FB 2683 Ends
                else
                if (MCUCDRTable != "")
                {
                    //dsTable = "MCUCDR";
                    query = "Delete from " + MCUCDRTable + " where  time < '" + DeleteCDRDate + "'";
                    ret = NonSelectCommand(query);
                    if (!ret)
                    {
                        logger.Trace("SQL error");
                        return false;
                    }
                }
                return true;

            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion
        //FB 2660 Ends

		//FB 2709
        #region InsertRPRMUserList
        /// <summary>
        /// InsertRPRMUserList
        /// </summary>
        /// <param name="mcuid"></param>
        /// <param name="interfaceid"></param>
        /// <param name="Login"></param>
        /// <param name="password"></param>
        /// <param name="Domain"></param>
        /// <returns></returns>
        public bool InsertRPRMUserList(int mcuid, int interfaceid, string Login, string password, string Domain)
        {
            cryptography.Crypto crypto = null;
            string encryptpass = "";
            try
            {
                crypto = new Crypto();
                encryptpass = crypto.encrypt(password);
                string query = "INSERT into MCU_RPRMLogin_D (bridgeId,BridgeInterfaceId,Login,Password,Domain,Deleted) VALUES  (" + mcuid.ToString() + "," + interfaceid.ToString() + ",'" + Login + "','" + encryptpass + "','" + Domain + "',0)";

                bool ret = NonSelectCommand(query);
                if (!ret)
                {
                    logger.Exception(100, "Error executing query.");
                    return false;
                }
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
            }
            return true;
        }
        #endregion
		// FB 2556 Starts
        #region UpdateConferenceEndpointStatus
        public bool UpdateConferenceEndpointStatus(int confid, int instanceid, string address, string eID,int onlineStatus)
        {
            String query = ""; String queryupdt = "";
            String tempQuery = ""; String tempqueryupdt = "";
            DataSet ds = new DataSet();
            string dsTable = "endpoint";
            bool ret = false;
            string eptID = "";
            string tblName = "";
            try
            {
                query = "Select Uid from {0} where {1} confid = " + confid.ToString() + " and instanceid = " + instanceid.ToString() + " and ipisdnaddress = '" + address.Trim() + "'";
                queryupdt = "update {0} set GUID = '" + eID + "' , onlinestatus = '"+ onlineStatus +"' where UID = {1}";

                tblName = "conf_room_d";
                tempQuery = string.Format(query, tblName, "");
                ret = SelectCommand(tempQuery, ref ds, dsTable);
                if (!ret)
                {
                    logger.Trace("SQL error");
                    return (false);
                }

                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[dsTable].Rows.Count > 0)
                    {
                        if (ds.Tables[dsTable].Rows[0]["Uid"] != null && ds.Tables[dsTable].Rows[0]["Uid"].ToString() != "")
                        {
                            eptID = ds.Tables[dsTable].Rows[0]["Uid"].ToString();
                            tempqueryupdt = string.Format(queryupdt, tblName, eptID);
                            ret = NonSelectCommand(tempqueryupdt);
                            if (!ret)
                            {
                                logger.Exception(100, "Error executing query.");
                                return false;
                            }

                            return true;

                        }
                    }
                }

                tblName = "conf_user_d";
                tempQuery = string.Format(query, tblName, " invitee = 1 and ");
                ret = SelectCommand(tempQuery, ref ds, dsTable);
                if (!ret)
                {
                    logger.Trace("SQL error");
                    return (false);
                }

                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[dsTable].Rows.Count > 0)
                    {
                        if (ds.Tables[dsTable].Rows[0]["Uid"] != null && ds.Tables[dsTable].Rows[0]["Uid"].ToString() != "")
                        {
                            eptID = ds.Tables[dsTable].Rows[0]["Uid"].ToString();
                            tempqueryupdt = string.Format(queryupdt, tblName, eptID);
                            ret = NonSelectCommand(tempqueryupdt);
                            if (!ret)
                            {
                                logger.Exception(100, "Error executing query.");
                                return false;
                            }

                            return true;

                        }
                    }
                }

            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
            return true;
        }
        #endregion
        // FB 2556 Ends

        //FB 2659 Starts
        #region InsertMCUOrgDetails
        /// <summary>
        /// InsertMCUOrgDetails
        /// </summary>
        /// <param name="MCUOrgList"></param>
        /// <returns></returns>
        internal bool InsertMCUOrgDetails(List<NS_MESSENGER.ExtMCUSilo> MCUOrgList)
        {
            try
            {
                StringBuilder query = new StringBuilder();
                bool ret = false;
                query.Append(" delete from Ext_MCUSilo_D where bridgeId = " + MCUOrgList[0].iBridgeId);
                ret = NonSelectCommand(query.ToString());
                if (!ret)
                {
                    logger.Exception(10, "Error executing query.");
                    return false;
                }
                ret = false;
                for (int i = 0; i < MCUOrgList.Count; i++)
                {
                    query.Append(" Insert into Ext_MCUSilo_D (BridgeId, BridgeTypeId, MemberId, Name, Alias, BillingCode) ");
                    query.Append(" values (" + MCUOrgList[i].iBridgeId + ", " + MCUOrgList[i].iBridgeTypeId + ", " + MCUOrgList[i].iOrgID + " , '" + MCUOrgList[i].sOrgName + "' , '" + MCUOrgList[i].sAlias + "' ,'" + MCUOrgList[i].sBillingCode + "' ) ");
                }
                ret = NonSelectCommand(query.ToString());
                if (!ret)
                {
                    logger.Exception(100, "query failed. query =" + query);
                }
            }
            catch (Exception ex)
            {
                logger.Trace("InsertMCUOrgDetails failed" + ex.Message);
                return false;
            }
            return true;
        }

        #endregion

        #region InsertMCUServiceDetails
        /// <summary>
        /// InsertMCUServiceDetails
        /// </summary>
        /// <param name="MCUServiceList"></param>
        /// <returns></returns>
        internal bool InsertMCUServiceDetails(List<NS_MESSENGER.ExtMCUService> MCUServiceList)
        {
            try
            {
                bool ret = false;

                StringBuilder query = new StringBuilder();
                query.Append(" delete from Ext_MCUService_D where bridgeId = " + MCUServiceList[0].iBridgeId + " and MemberID = " + MCUServiceList[0].iMemberId);//For each org/memberid, services need to deleted.
                ret = NonSelectCommand(query.ToString());
                if (!ret)
                {
                    logger.Exception(10, "Error executing query.");
                    return false;
                }
                ret = false;
                query = new StringBuilder();
                for (int i = 0; i < MCUServiceList.Count; i++)
                {
                    query.Append(" Insert into Ext_MCUService_D (bridgeId, BridgeTypeId, MemberId, ServiceId, ServiceName, Prefix, Description) ");
                    query.Append(" values ( " + MCUServiceList[i].iBridgeId + ", " + MCUServiceList[i].iBridgeTypeId + ", " + MCUServiceList[i].iMemberId + " , '" + MCUServiceList[i].iServiceId + "' ,");
                    query.Append(" '" + MCUServiceList[i].sServiceName + "' ,'" + MCUServiceList[i].sPrefix + "' ,'" + MCUServiceList[i].sDescription + "' ) ");
                }
                ret = NonSelectCommand(query.ToString());
                if (!ret)
                {
                    logger.Exception(100, "query failed. query =" + query.ToString());
                }
            }
            catch (Exception ex)
            {
                logger.Trace("InsertMCUServiceDetails failed" + ex.Message);
                return false;
            }
            return true;
        }

        #endregion

        #region FetchSysSettingDetails
        /// <summary>
        /// FetchSysSettingDetails
        /// </summary>
        /// <param name="EnableCloudInstallation"></param>
        /// <returns></returns>
        internal bool FetchSysSettingDetails(ref int EnableCloudInstallation)
        {
            try
            {
                string query = "", dsTable = "SysDetails";
                DataSet ds = new DataSet();

                query = " Select EnableCloudInstallation, Admin from Sys_Settings_D ";

                bool ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Trace("SQL error: Query is " + query);
                    return (false);
                }

                for (int i = 0; i < ds.Tables[dsTable].Rows.Count; i++)
                {
                    int.TryParse(ds.Tables[dsTable].Rows[i]["EnableCloudInstallation"].ToString(), out EnableCloudInstallation);
                }
            }
            catch (Exception ex)
            {
                logger.Trace("FetchSysSettingDetails Error: " + ex.Message);
                return false;

            }
            return true;
        }
        #endregion
        //FB 2659 End
        // ZD 104151 Start
        #region FetchEmptyconfOrgDetails
        /// <summary>
        /// FetchEmptyconfOrgDetails
        /// </summary>
        /// <param name="EmptyConfPush"></param>
        /// <returns></returns>
        internal bool FetchEmptyConfOrgDetails(ref int EmptyConfPush, ref int orgID)
        {
            try
            {
                string query = "", dsTable = "OrgDetails";
                DataSet ds = new DataSet();
                query = " Select Emptyconferencepush from Org_Settings_D where OrgID = " + orgID;
                bool ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Trace("SQL error: Query is " + query);
                    return (false);
                }

                for (int i = 0; i < ds.Tables[dsTable].Rows.Count; i++)
                {
                    int.TryParse(ds.Tables[dsTable].Rows[i]["Emptyconferencepush"].ToString(), out EmptyConfPush);
                }
            }
            catch (Exception ex)
            {
                logger.Trace("FetchEmptyconfOrgDetails Error: " + ex.Message);
                return false;

            }
            return true;
        }
        #endregion
        // ZD 104151 End
    

        public bool FetchRPRMConfUser(int RPRMUserid, ref NS_MESSENGER.Conference conf)
        {
            DataSet ds = new DataSet();
            string dsTable = "Conference";
            string query = "Select * from MCU_RPRMLogin_D where id = '" + RPRMUserid.ToString() + "'";
            bool ret = SelectCommand(query, ref ds, dsTable);
            if (!ret)
            {
                logger.Exception(10, "SQL error");
                return (false);
            }
            for (int i = 0; i < ds.Tables[dsTable].Rows.Count; i++)
            {
                try
                {
                    if (ds.Tables[dsTable].Rows[i]["Login"] != null)
                    {
                        if (ds.Tables[dsTable].Rows[i]["Login"].ToString() != "")
                        {
                            conf.sRPRMLogin = ds.Tables[dsTable].Rows[i]["Login"].ToString();
                        }
                        if (ds.Tables[dsTable].Rows[i]["Domain"].ToString() != "")
                        {
                            conf.sRPRMDomain = ds.Tables[dsTable].Rows[i]["Domain"].ToString();
                        }
                    }
                }
                catch (Exception e)
                {
                    logger.Exception(100, e.Message);
                }
            }
            return true;
        }
        //FB 2441 II Starts
        #region UpdateSyncStatus
        /// <summary>
        /// 
        /// </summary>
        /// <param name="conf"></param>
        /// <returns></returns>
        public bool UpdateSyncStatus(NS_MESSENGER.Conference conf) //FB 2441 II
        {
            string query = "";
            try
            {
                //query = "Delete Conf_SyncMCUAdj_D where confid=" + conf.iDbID + " and instanceid=" + conf.iInstanceID + " and Esid='" + conf.ESId + "'";
                query = "Delete Conf_SyncMCUAdj_D where confid=" + conf.iDbID + " and instanceid=" + conf.iInstanceID; //ZD 100221
                bool ret = NonSelectCommand(query);
                if (!ret)
                {
                    logger.Exception(100, "Error executing query." + query);
                    return false;
                }
                query = "Update Conf_conference_D set esid=NULL,etag=NULL,BJNUniqueid='' where confid=" + conf.iDbID + " and instanceid=" + conf.iInstanceID; //ZD 103263
                ret = NonSelectCommand(query);
                if (!ret)
                {
                    logger.Exception(100, "Error executing query." + query);
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
               logger.Trace("UpdateSyncStatus :" + ex.Message);
                return false;
            }
        }
        #endregion

        #region CheckRPRMLogin
        /// <summary>
        /// 
        /// </summary>
        /// <param name="mcuid"></param>
        /// <param name="RPRMLogin"></param>
        /// <param name="existcount"></param>
        /// <returns></returns>
        public bool CheckRPRMLogin(int mcuid,string RPRMLogin,ref int existcount)
        {
            string query = "";
            DataSet ds = new DataSet();
            try
            {
                query = "select Login as Login from MCU_RPRMLogin_D where bridgeid=" + mcuid +" and deleted=0" ;
                bool ret = SelectCommand(query, ref ds, "mcu_RPRMLogin_d");
                if (!ret)
                {
                    logger.Exception(10, "SQL error");
                    return (false);
                }
                if(ds!=null)
                {
                    if(ds.Tables[0].Rows.Count>0)
                    {
                        string Login = ds.Tables[0].Rows[0]["Login"].ToString().ToLower();
                        int count = Login.LastIndexOf('_');
                        Login= Login.Substring(0, count);
                        if (Login == RPRMLogin.ToLower())
                            existcount = ds.Tables[0].Rows.Count;
                        else
                        {
                            existcount = 0;
                            query = "update MCU_RPRMLogin_D set deleted=1 where bridgeid=" + mcuid;
                            ret = NonSelectCommand(query);
                            if (!ret)
                            {
                                logger.Exception(100, "Error executing query." + query);
                                return false;
                            }

                        }
                    }
                }

                return true;
            }
            catch (Exception ex)
            {

                return false;
            }
        }
        #endregion

        //FB 2441 II Ends

        //FB 2593 Start

        #region FetchMaxCDRIndex
        /// <summary>
        /// FetchMaxCDRIndex
        /// </summary>
        /// <param name="MaxCDRIndex"></param>
        /// <param name="table"></param>
        /// <returns></returns>
        internal Int32 FetchMaxCDRIndex(Int32 MaxCDRIndex, string table)
        {
            bool ret = false;
            string query = "";
            string dsTable = "CDRCisco";
            DataSet ds = new DataSet();
            try
            {
                query = " Select Max(CDRIndex) from " + table.Trim();

                ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Exception(100, "query failed. query =" + query);
                }

                for (int i = 0; i < ds.Tables[dsTable].Rows.Count; i++)
                {
                    Int32.TryParse(ds.Tables[dsTable].Rows[i][0].ToString(), out MaxCDRIndex);
                }
                logger.Trace("MaxCDRIndex in Table" + MaxCDRIndex);

            }
            catch (Exception ex)
            {
                logger.Trace("FetchMaxCDRIndex :" + ex.Message);
            }
            return MaxCDRIndex;
        }
        #endregion

        #region FetchConfforCallDetailRecord
        internal bool FetchConfforCallDetailRecord(ref NS_MESSENGER.Conference conf, string partyGUID, NS_MESSENGER.MCU MCU)
        {

            // Retreive conf details
            DataSet ds = new DataSet();
            string dsTable = "Conference";
            string confID = ""; string instanceID = "", terminalTpye = "";
            string query = null; bool ret = false;
            try
            {

                //FB 2797 Start

                if (MCU.etType == NS_MESSENGER.MCU.eType.CISCOTP)
                    query = "Select * from Conf_Conference_D where GUID = '" + conf.sGUID.ToString() + "'";
                else if (MCU.etType == NS_MESSENGER.MCU.eType.CODIAN)
                    query = "Select * from Conf_Conference_D where confnumname  = " + conf.iDbNumName.ToString(); //ZD 101348
                //query = "Select * from Conf_Conference_D where confnumname  = " + conf.iDbNumName.ToString() + " and isVMR = 0";

                //FB 2797 End

                if (partyGUID.Trim() != "" && conf.sGUID.Trim() == "")
                {
                    GetConferenceIDfromParty(ref confID, ref instanceID, ref terminalTpye, partyGUID);
                    query = "Select * from Conf_Conference_D where confid = '" + confID.ToString() + "' and instanceid = '" + instanceID.ToString() + "'";
                }

                ret = false;
                ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Exception(100, "SQL error");
                    return (false);
                }

                if (ds.Tables[dsTable].Rows.Count < 1)
                {
                    // no confs 
                    logger.Trace("No conf found.");
                    return false;
                }
                //ZD 100085 Starts
                conf.iDuration = Int32.Parse(ds.Tables[dsTable].Rows[0]["Duration"].ToString());
                //conf.iDuration = conf.iDuration + timeDiff.Minutes;
                logger.Trace("Conf Duration : " + conf.iDuration.ToString());

                //FB 2593 Starts
                // start date & time
                //ZD 104332 starts
                if (conf.iImmediate == 1)
                    conf.dtStartDateTime = DateTime.Parse(ds.Tables[dsTable].Rows[0]["ConfDate"].ToString()); //FB 2440
                else
                    conf.dtStartDateTime = DateTime.Parse(ds.Tables[dsTable].Rows[0]["ConfDate"].ToString()).AddSeconds(-40); //FB 2440   //5 secs buffer

                //ZD 104332 ends
                logger.Trace("Start Time : " + conf.dtStartDateTime.ToString("T"));
                
                //conf.dtEndDateTime = DateTime.Parse(ds.Tables[dsTable].Rows[0]["TearDownTime"].ToString());//FB 2440
                conf.dtEndDateTime = conf.dtStartDateTime.AddMinutes(conf.iDuration);
                //ZD 100085 End
                if (DateTime.UtcNow < conf.dtEndDateTime)
                {
                    logger.Trace("Conferecne is Ongoing.");
                    return false;
                }
                //FB 2593 End
                conf.iOrgID = Int32.Parse(ds.Tables[dsTable].Rows[0]["orgId"].ToString()); //FB 2335 
                conf.iDbID = Int32.Parse(ds.Tables[dsTable].Rows[0]["ConfId"].ToString());
                conf.iInstanceID = Int32.Parse(ds.Tables[dsTable].Rows[0]["InstanceId"].ToString());
                conf.iDbNumName = Int32.Parse(ds.Tables[dsTable].Rows[0]["ConfNumName"].ToString());
                conf.sExternalName = ds.Tables[dsTable].Rows[0]["ExternalName"].ToString();
                conf.iHostUserID = Int32.Parse(ds.Tables[dsTable].Rows[0]["owner"].ToString());
                conf.ESId = ds.Tables[dsTable].Rows[0]["ESId"].ToString(); //Polycom CMA
                conf.iImmediate = Int32.Parse(ds.Tables[dsTable].Rows[0]["immediate"].ToString());//FB 2440
                if (ds.Tables[dsTable].Rows[0]["isvmr"].ToString().Trim() != "")//FB 2447
                    conf.iIsVMR = Int32.Parse(ds.Tables[dsTable].Rows[0]["isvmr"].ToString());//FB 2447
                try
                {
                    conf.sPwd = ds.Tables[dsTable].Rows[0]["password"].ToString().Trim();
                    conf.iPwd = Int32.Parse(conf.sPwd);


                }
                catch (Exception)
                {
                    conf.sPwd = "";
                    conf.iPwd = 0;
                }

                #region Fetch the host email
                DataSet ds2 = new DataSet();
                string dsTable2 = "ConfHost"; //FB 2593
                string query2 = "Select Email,FirstName + ' ' + LastName as HostName from Usr_List_D where userid = " + conf.iHostUserID.ToString();
                bool ret3 = SelectCommand(query2, ref ds2, dsTable2);
                if (!ret3)
                {
                    logger.Exception(100, "SQL error");
                    return (false);
                }

                if (ds2.Tables[dsTable2].Rows.Count < 1)
                {
                    // no record 
                    logger.Trace("No host email found.");
                    return false;
                }
                conf.sHostEmail = ds2.Tables[dsTable2].Rows[0]["Email"].ToString();
                conf.sHostName = ds2.Tables[dsTable2].Rows[0]["HostName"].ToString();//FB 2593
                #endregion

                ret = false;
                ret = GenerateConfName(conf, ref conf.sDbName);
                if (!ret)
                {
                    logger.Trace("Failure in generating conf name for bridges.");
                    return false;
                }



                // conf timezone
                conf.iTimezoneID = Int32.Parse(ds.Tables[dsTable].Rows[0]["Timezone"].ToString());
                logger.Trace("TimezoneID : " + conf.iTimezoneID.ToString());

                // timezone 
                DataSet dsTZ = new DataSet();
                string dsTableTZ = "TZName";
                string queryTZ = "select timezone,timezonediff from gen_timezone_s where timezoneID = " + conf.iTimezoneID.ToString();
                bool retTZ = SelectCommand(queryTZ, ref dsTZ, dsTableTZ);
                if (!retTZ)
                {
                    logger.Exception(100, "SQL error");
                    return (false);
                }
                conf.sName_StartDateTimeInLocalTZ = dsTZ.Tables[dsTableTZ].Rows[0]["timezonediff"].ToString() + dsTZ.Tables[dsTableTZ].Rows[0]["timezone"].ToString();

                // start date & time in UTC
                conf.dtStartDateTimeInUTC = conf.dtStartDateTime;

                // Time difference 
                /*TimeSpan timeDiff = conf.dtStartDateTime.Subtract(DateTime.UtcNow);
                logger.Trace ("DateTime.UtcNow: " + DateTime.UtcNow.ToString("T"));
                logger.Trace ("Time Diff : " + timeDiff.Minutes.ToString());
                */

                // Convert to local TZ
                DataSet dsTZ1 = new DataSet();
                string dsTableTZ1 = "LocalTZ";
                string queryTZ1 = "select dbo.changeTime (" + conf.iTimezoneID.ToString() + ",'" + conf.dtStartDateTimeInUTC + "')";
                bool retTZ1 = SelectCommand(queryTZ1, ref dsTZ1, dsTableTZ1);
                if (!retTZ1)
                {
                    logger.Exception(100, "SQL error");
                    return (false);
                }
                conf.dtStartDateTimeInLocalTZ = DateTime.Parse(dsTZ1.Tables[dsTableTZ1].Rows[0][0].ToString());
				//ZD 100085 Starts
                // duration
                //conf.iDuration = Int32.Parse(ds.Tables[dsTable].Rows[0]["Duration"].ToString());
                //conf.iDuration = conf.iDuration + timeDiff.Minutes;
                //logger.Trace("Conf Duration : " + conf.iDuration.ToString());
				//ZD 100085 End
                //check conf type
                int confType = 0;
                confType = Int32.Parse(ds.Tables[dsTable].Rows[0]["ConfType"].ToString()); //dbo.Conference.ConfType				
                logger.Trace("Conf Type : " + confType.ToString());
                switch (confType)
                {
                    case 2:
                        {
                            // audio-video conference
                            conf.etMediaType = NS_MESSENGER.Conference.eMediaType.AUDIO_VIDEO;
                            conf.etType = NS_MESSENGER.Conference.eType.MULTI_POINT;
                            break;
                        }
                    case 6:
                        {
                            // audio conference
                            conf.etMediaType = NS_MESSENGER.Conference.eMediaType.AUDIO;
                            conf.etType = NS_MESSENGER.Conference.eType.MULTI_POINT;
                            break;
                        }
                    case 4:
                        {
                            // p2p conference
                            conf.etMediaType = NS_MESSENGER.Conference.eMediaType.AUDIO_VIDEO;
                            conf.etType = NS_MESSENGER.Conference.eType.POINT_TO_POINT;
                            break;
                        }
                    default:
                        {   // 7 = room conference
                            this.errMsg = "This is not an audio/video conference.";
                            return false;
                        }
                }

                //conf.bAutoTerminate = NS_CONFIG.Config.CONF_AUTO_TERMINATE; 
                conf.bAutoTerminate = true;

                // lecture mode
                int lecMode = Int32.Parse(ds.Tables[dsTable].Rows[0]["LectureMode"].ToString().Trim());
                if (lecMode == 0)
                {
                    conf.bLectureMode = false;
                    logger.Trace("Lecture Mode : Disabled ");
                }
                else
                {
                    conf.bLectureMode = true;
                    logger.Trace("Lecture Mode : Enabled ");
                }
                //FB 2486 Start
                int MessageOverlay = Int32.Parse(ds.Tables[dsTable].Rows[0]["isTextMsg"].ToString().Trim());
                if (MessageOverlay == 0)
                {
                    conf.bMessageOverlay = false;
                    logger.Trace("Message Overlay : Disabled ");
                }
                else
                {
                    conf.bMessageOverlay = true;
                    logger.Trace("Message Overlay : Enabled ");
                }
                //FB 2486 End

                //FB 2501 Call Monitoring Start
                conf.sGUID = ds.Tables[dsTable].Rows[0]["GUID"].ToString();
                //FB 2501 Call Monitoring End
                
                //ZD 101348 Starts
                int isPermanent = 0;
                if (ds.Tables[dsTable].Rows[0]["Permanent"] != null)
                    int.TryParse(ds.Tables[dsTable].Rows[0]["Permanent"].ToString(), out isPermanent);
                conf.iPermanent = isPermanent;
                logger.Trace("conf.iPermanent:" + conf.iPermanent);
                //ZD 101348 End

            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }

            return true;
        }
        #endregion

        #region Insert Call Detail record
        /// <summary>
        /// Insert Call Detail record
        /// </summary>
        /// <param name="cdrEventLists"></param>
        /// <param name="evntMCU"></param>
        /// <returns></returns>
        internal bool InsertCDRinMyvrm(ref List<NS_MESSENGER.CDREvent> cdrEventLists, NS_MESSENGER.MCU evntMCU,string responseXML) //FB 2683
        {
            bool ret = false;
            string query = "";
            DataSet ds = new DataSet();
            try
            {
                if (evntMCU.etType == NS_MESSENGER.MCU.eType.CISCOTP)
                {
                    for (int i = 0; i < cdrEventLists.Count; i++)
                    {	//FB 2593 Starts
                        query += " Insert into CDR_Cisco_D ([ConferenceGUID],[ParticipantGUID],[EventType],[BridgeId],[BridgeType],[CDRIndex],[time],";
                        query += " [ConfName],[NumericId],[CallID],[Uri],[PinProtected],[CallDirection],[CallProtocol],[EndpointIPAddress],[EndpointDisplayName],";
                        query += " [EndpointURI],[EndpointConfiguredName],[TimeInConference],[DisconnectReason],[MaxSimultaneousAudioVideoParticipants],";
                        query += " [MaxSimultaneousAudioOnlyParticipants],[TotalAudioVideoParticipants],[TotalAudioOnlyParticipants],[SessionDuration],[Name],";
                        query += " [ActiveTime],[EncryptedTime],[Width],[Height],[MaxBandwidth],[Bandwidth],[PacketsReceived],[PacketsLost],[duration],[ScheduledTime])";
                        query += " values ( '" + cdrEventLists[i].sConferenceGUID + "', '" + cdrEventLists[i].sParticipantGUID + "', '" + cdrEventLists[i].sEventType + "', '" + cdrEventLists[i].iBridgeId + "','" + cdrEventLists[i].iBridgeType + "',";
                        query += " '" + cdrEventLists[i].iIndex + "', '" + cdrEventLists[i].dTime + "', '" + cdrEventLists[i].sConfName + "', '" + cdrEventLists[i].iNumericId + "',";
                        query += " '" + cdrEventLists[i].sCallID + "','" + cdrEventLists[i].sUri + "', '" + cdrEventLists[i].sPinProtected + "',  '" + cdrEventLists[i].sCallDirection + "',";
                        query += " '" + cdrEventLists[i].sCallProtocol + "', '" + cdrEventLists[i].sEndpointIPAddress + "', '" + cdrEventLists[i].sEndpointDisplayName + "', '" + cdrEventLists[i].sEndpointURI + "',";
                        query += " '" + cdrEventLists[i].sEndpointConfiguredName + "', '" + cdrEventLists[i].sTimeInConference + "', '" + cdrEventLists[i].sDisconnectReason + "', '" + cdrEventLists[i].sMaxSimultaneousAVParticipants + "',";
                        query += " '" + cdrEventLists[i].sMaxSimultaneousAudioOnlyParticipants + "', '" + cdrEventLists[i].iTotalAudioVideoParticipants + "',  '" + cdrEventLists[i].iTotalAudioOnlyParticipants + "',  '" + cdrEventLists[i].iSessionDuration + "', ";
                        query += " '" + cdrEventLists[i].sName + "', '" + cdrEventLists[i].sActiveTime + "', '" + cdrEventLists[i].sEncryptedTime + "', '" + cdrEventLists[i].iWidth + "', '" + cdrEventLists[i].iHeight + "',";
                        query += " '" + cdrEventLists[i].iMaxBandwidth + "', '" + cdrEventLists[i].iBandwidth + "', '" + cdrEventLists[i].iPacketsReceived + "', '" + cdrEventLists[i].iPacketsLost + "', '" + cdrEventLists[i].iDuration + "', '" + cdrEventLists[i].sScheduledTime + "') ";
                        //FB 2593 End
                    }
                }
                //FB 2797 Start
				//ZD 101348 Starts
                else if (evntMCU.etType == NS_MESSENGER.MCU.eType.CODIAN)
                {
                    for (int i = 0; i < cdrEventLists.Count; i++)
                    {
                        query += " Insert into CDR_Codian_D ";
                        query += "([uniqueId],[PinProtected],[EventType],[billingCode],[OwnerName],[CDRIndex],[time],[ConfName],[NumericId],";
                        query += "[scheduleddate],[scheduledTime],[scheduleddurationinminutes],[EndpointId],[direction],[EndpointIPAddress],";
                        query += "[EndpointDN],[Endpointh323Alias],[EndpointConfiguredName],[TimeInConference],[TimeInConferenceInMinutes],";
                        query += "[DisconnectReason],[EndpointDirection],[protocol],[mediaEncryptionStatus],[MediaFromResolution],";
                        query += "[MediaFromVideoCodec],[MediaFromaudioCodec],[MediaFrombandwidth],[MediaToresolution],[MediaTovideoCodec],";
                        query += "[MediaToaudioCodec],[MediaTobandwidth],[AudioVideoParticipants],[AudioOnlyParticipants],";
                        query += "[streamingParticipantsAllowed],[MaxSimultaneousAudioVideoParticipants],[MaxSimultaneousAudioOnlyParticipants],";
                        query += "[maxSimultaneousStreaming],[TotalAudioVideoParticipants],[TotalAudioOnlyParticipants],";
                        query += "[TotalstreamingParticipantsAllowed],[registeredWithGatekeeper],[duration],[durationInMinutes],";
                        query += "[Confnumname],[LastModifiedDateTime])";
                        query += " values ( " + cdrEventLists[i].iuniqueId + ", '" + cdrEventLists[i].sPinProtected + "', '" + cdrEventLists[i].sEventType + "', '" + cdrEventLists[i].sbillingCode + "','" + cdrEventLists[i].sownername + "',";
                        query += "" + cdrEventLists[i].iIndex + ", '" + cdrEventLists[i].dTime + "', '" + cdrEventLists[i].sName + "', " + cdrEventLists[i].iNumericId + ", '" + cdrEventLists[i].sScheduleddate + "','" + cdrEventLists[i].sScheduledTime + "', ";
                        query += "'" + cdrEventLists[i].sScheduleddurationinminutes + "', " + cdrEventLists[i].iparticipantId + ",";
                        query += "'" + cdrEventLists[i].sDirection + "', '" + cdrEventLists[i].sEndpointIPAddress + "', '" + cdrEventLists[i].sdn + "', '" + cdrEventLists[i].sh323Alias + "',";
                        query += "'" + cdrEventLists[i].sEndpointConfiguredName + "', " + cdrEventLists[i].iTimeInConference + ", " + cdrEventLists[i].itimeInConferenceInMinutes + ",'" + cdrEventLists[i].sDisconnectReason + "','" + cdrEventLists[i].sEndpointDirection + "','" + cdrEventLists[i].sCallProtocol + "',";
                        query += "'" + cdrEventLists[i].sMediaEncryptionStatus + "', '" + cdrEventLists[i].sMediaFromResolution + "',  '" + cdrEventLists[i].sMediaFromVideoCodec + "',  '" + cdrEventLists[i].sMediaFromaudioCodec + "', ";
                        query += "" + cdrEventLists[i].iMediaFrombandwidth + ", '" + cdrEventLists[i].sMediaToresolution + "', '" + cdrEventLists[i].sMediaTovideoCodec + "', '" + cdrEventLists[i].sMediaToaudioCodec + "', " + cdrEventLists[i].iMediaTobandwidth + ",";
                        query += "" + cdrEventLists[i].iAudioVideoParticipants + ", " + cdrEventLists[i].iAudioOnlyParticipants + ", " + cdrEventLists[i].istreamingParticipantsAllowed + ", " + cdrEventLists[i].iMaxSimultaneousAudioVideoParticipants + ",";
                        query += "" + cdrEventLists[i].iMaxSimultaneousAudioOnlyParticipants + ", " + cdrEventLists[i].imaxSimultaneousStreaming + ", " + cdrEventLists[i].iTotalAudioVideoParticipants + ", " + cdrEventLists[i].iTotalAudioOnlyParticipants + ",";
                        query += "" + cdrEventLists[i].iTotalstreamingParticipantsAllowed + ", '" + cdrEventLists[i].sregisteredWithGatekeeper + "', " + cdrEventLists[i].iduration + ", " + cdrEventLists[i].idurationInMinutes + ", " + cdrEventLists[i].iConfnumname + ",'" + DateTime.Now + "') ";
					//ZD 101348 End
                    }
                }
                //FB 2797 End
                //FB 2683 Start
                else if (evntMCU.etType == NS_MESSENGER.MCU.eType.RMX)
                {
                    query = "insert into CDR_PolycomRMX_D (ResponseXML,time,LastModified) values('" + responseXML + "','" + cdrEventLists [0].sTime+ "','" + DateTime.Now + "')";
                }
                //FB 2683 End
                ret = NonSelectCommand(query);
                if (!ret)
                {
                    logger.Exception(100, "query failed. query =" + query);
                    return false;
                }

            }
            catch (Exception ex)
            {
                logger.Exception(100, "InsertCDRinMyvrm query failed. query =" + query);
                logger.Trace("InsertCDRinMyvrm :" + ex.Message);
                return false;
            }
            return true;
        }
        #endregion      

        #region GetConferenceIDfromParty
        public bool GetConferenceIDfromParty(ref string confid, ref string instanceid, ref string terminalTpye, string partyGUID)
        {
            string query = "", tempquery = "";
            DataSet ds = new DataSet();
            string dsTable = "endpoint";
            bool ret = false;
            string tblName = "";
            try
            {
                query = "Select confID, InstanceID from {0} where GUID = '" + partyGUID + "'";

                tblName = "Conf_Room_D";
                tempquery = string.Format(query, tblName);
                ret = SelectCommand(tempquery, ref ds, dsTable);
                if (!ret)
                {
                    logger.Trace("SQL error");
                    return (false);
                }

                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[dsTable].Rows.Count > 0)
                    {
                        if (ds.Tables[dsTable].Rows[0]["confID"] != null && ds.Tables[dsTable].Rows[0]["confID"].ToString() != "")
                        {
                            confid = ds.Tables[dsTable].Rows[0]["confID"].ToString();
                            instanceid = ds.Tables[dsTable].Rows[0]["InstanceID"].ToString();
                            terminalTpye = "Room";
                            return true;

                        }
                    }
                }

                tblName = "Conf_User_D";
                dsTable = "ConfUser";
                tempquery = string.Format(query, tblName);
                ret = SelectCommand(tempquery, ref ds, dsTable);
                if (!ret)
                {
                    logger.Trace("SQL error");
                    return (false);
                }

                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[dsTable].Rows.Count > 0)
                    {
                        if (ds.Tables[dsTable].Rows[0]["confID"] != null && ds.Tables[dsTable].Rows[0]["confID"].ToString() != "")
                        {
                            confid = ds.Tables[dsTable].Rows[0]["confID"].ToString();
                            instanceid = ds.Tables[dsTable].Rows[0]["InstanceID"].ToString();
                            terminalTpye = "User";
                            return true;

                        }
                    }
                }

                tblName = "Conf_Cascade_D";
                dsTable = "ConfCascade";
                tempquery = string.Format(query, tblName);
                ret = SelectCommand(tempquery, ref ds, dsTable);
                if (!ret)
                {
                    logger.Trace("SQL error");
                    return (false);
                }

                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[dsTable].Rows.Count > 0)
                    {
                        if (ds.Tables[dsTable].Rows[0]["confID"] != null && ds.Tables[dsTable].Rows[0]["confID"].ToString() != "")
                        {
                            confid = ds.Tables[dsTable].Rows[0]["confID"].ToString();
                            instanceid = ds.Tables[dsTable].Rows[0]["InstanceID"].ToString();
                            terminalTpye = "Cascade";
                            return true;

                        }
                    }
                }


            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
            return false;
        }

        #endregion

        #region InsertCDRConference
        /// <summary>
        /// InsertCDRConference
        /// </summary>
        /// <param name="Conf"></param>
        /// <returns></returns>
        internal bool InsertCDRConference(NS_MESSENGER.MCU.CDRConference Conf)
        {
            string query = "", subquery = "";//ZD 101348
            bool ret = false;
            int confDuration = 0;//ZD 101348

            try
            {
				//ZD 101348 Starts
                if (Conf.IsVMRConf == "1" || Conf.IsPermanentConf == "1")
                {
                    query += " IF NOT EXISTS(SELECT confNumname FROM Conf_VMRCDR_logs_D WHERE Confid =" + Conf.Confid + " and instanceId = " + Conf.Instanceid + ")";
                    query += " insert into Conf_VMRCDR_logs_D (Confid,Instanceid,ConfNumName,Orgid,EventDate,McuID,McuName,McuTypeID,McuType,McuAddress,ConfTimeZone,Host,MCUConfID,Title,ScheduledStart,ScheduledEnd,Duration,LastModifiedDateTime,EventId,ConfGUID)";
                    query += " values(" + Conf.Confid + "," + Conf.Instanceid + "," + Conf.ConfNumName + "," + Conf.Orgid + ",'" + Conf.EventDate + "', " + Conf.McuID + ",'" + Conf.McuName + "'," + Conf.McuTypeID + ",'" + Conf.McuType + "','" + Conf.McuAddress + "'," + Conf.ConfTimeZone + ",'" + Conf.Host + "','" + Conf.McuConfGuid + "','" + Conf.Title + "','" + Conf.ScheduledStart + "','" + Conf.ScheduledEnd + "'," + Conf.Duration + ",'" + DateTime.Now + "','" + Conf.EventId + "','" + Conf.ConfGUID + "')";
					//ZD 101348 Starts
                    if (!Conf.Duration.Equals("0"))
                    {
                        int.TryParse(Conf.Duration, out confDuration);
                        if (confDuration > 0)
                        {
                            subquery = " Update Conf_Conference_D Set Duration = " + confDuration + " where confid = " + Conf.Confid + " and instanceId = " + Conf.Instanceid + "";
                            ret = NonSelectCommand(subquery);
                            if (!ret)
                            {
                                logger.Exception(100, "Error: Update duration in conf table for Permanent VMR:" + subquery);
                            }
                        }
                    }
					//ZD 101348 End
                }
                else
                {
                    query = " insert into Conf_CDR_logs_D (Confid,Instanceid,ConfNumName,Orgid,EventDate,McuID,McuName,McuTypeID,McuType,McuAddress,ConfTimeZone,Host,MCUConfID,Title,ScheduledStart,ScheduledEnd,Duration,LastModifiedDateTime,EventId,ConfGUID)";
                    query += " values(" + Conf.Confid + "," + Conf.Instanceid + "," + Conf.ConfNumName + "," + Conf.Orgid + ",'" + Conf.EventDate + "', " + Conf.McuID + ",'" + Conf.McuName + "'," + Conf.McuTypeID + ",'" + Conf.McuType + "','" + Conf.McuAddress + "'," + Conf.ConfTimeZone + ",'" + Conf.Host + "','" + Conf.McuConfGuid + "','" + Conf.Title + "','" + Conf.ScheduledStart + "','" + Conf.ScheduledEnd + "'," + Conf.Duration + ",'" + DateTime.Now + "','" + Conf.EventId + "','" + Conf.ConfGUID + "')";
                }
				//ZD 101348 End
                ret = NonSelectCommand(query);
                if (!ret)
                {
                    logger.Exception(100, "Error executing query." + query);
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                logger.Trace("Error in InsertCDRConference" + ex.Message);
                return false;
            }
        }

        #endregion
		//ZD 101348 Starts
        #region GetPartyDetails
        /// <summary>
        /// GetPartyDetails
        /// </summary>
        /// <param name="confid"></param>
        /// <param name="instanceid"></param>
        /// <param name="partyGUID"></param>
        /// <param name="ds"></param>
        /// <returns></returns>
        public bool GetPartyDetails(int confid, int instanceid, int Numname, ref DataSet ds, string confGUID, int isPermanentConf)//,List<string> partyGuid
        {
            string query = "", tempquery = "";
            ds = new DataSet();
            DataSet confroomds = new DataSet();
            DataSet confuserds = new DataSet();
            DataSet confTable = new DataSet();
            DataSet mergedPartyTable = new DataSet();
            string dsTable = "endpoint";
            string confdsTable = "confdsTable";
            bool ret = false;
            string tblName = "";
            try
            {

                if (isPermanentConf == 1)
                {
                    query = " select * from Conf_Conference_D  where GUID = '" + confGUID + "' ";
                    ret = SelectCommand(query, ref confTable, confdsTable);
                    if (!ret)
                    {
                        logger.Trace("SQL error");
                        //return (false);
                    }
                    for (int i = 0; i < confTable.Tables[confdsTable].Rows.Count; i++)
                    {
                        confroomds = new DataSet();
                        confuserds = new DataSet();
                        dsTable = "endpoint";
                        if (confTable.Tables[confdsTable].Rows[i]["confid"] != null)
                            int.TryParse(confTable.Tables[confdsTable].Rows[i]["confid"].ToString(), out confid);
                        if (confTable.Tables[confdsTable].Rows[i]["instanceID"] != null)
                            int.TryParse(confTable.Tables[confdsTable].Rows[i]["instanceID"].ToString(), out instanceid);
                        if (confTable.Tables[confdsTable].Rows[i]["confnumname"] != null)
                            int.TryParse(confTable.Tables[confdsTable].Rows[i]["confnumname"].ToString(), out Numname);

                        query = " Select Partyname, TerminalType, CAST(ConnectionType as int) ConnectionType, ipisdnaddress, GUID from Conf_Room_D where ";
                        query += " EndpointId > 0 and ConfID = '" + confid + "' and instanceID = '" + instanceid + "' and confuid = '" + Numname + "'"; //GUID in ('" + ExistcommaSeperated + "')

                        ret = SelectCommand(query, ref confroomds, dsTable);
                        if (!ret)
                        {
                            logger.Trace("SQL error");
                            return (false);
                        }
						//ZD 101348
                        dsTable = "ConfUser";
                        tempquery = " Select Partyname, TerminalType, CAST(ConnectionType as int) ConnectionType, ipisdnaddress, GUID from Conf_User_D where ";
                        tempquery += " Invitee = 1 and ConfID = '" + confid + "' and instanceID = '" + instanceid + "' and confuid = '" + Numname + "'"; //GUID in ('" + ExistcommaSeperated + "')

                        ret = SelectCommand(tempquery, ref confuserds, dsTable);
                        if (!ret)
                        {
                            logger.Trace("SQL error");
                            return (false);
                        }
                        mergedPartyTable = confroomds.Copy();
                        mergedPartyTable.Merge(confuserds, true, MissingSchemaAction.Add);
                        ds.Merge(mergedPartyTable, true, MissingSchemaAction.Add);
                    }
                }
                else
                {
                    query = " Select Partyname, TerminalType, ConnectionType, ipisdnaddress, GUID from {0} where ";
                    query += " ConfID = '" + confid + "' and instanceID = '" + instanceid + "' and confuid = '" + Numname + "'"; //GUID in ('" + ExistcommaSeperated + "')

                    tblName = "Conf_Room_D";
                    tempquery = string.Format(query, tblName);
                    ret = SelectCommand(tempquery, ref confroomds, dsTable);
                    if (!ret)
                    {
                        logger.Trace("SQL error");
                        return (false);
                    }

                    tblName = "Conf_User_D";
                    dsTable = "ConfUser";
                    tempquery = string.Format(query, tblName);
                    ret = SelectCommand(tempquery, ref confuserds, dsTable);
                    if (!ret)
                    {
                        logger.Trace("SQL error");
                        return (false);
                    }
                    ds = confroomds.Copy();
                    ds.Merge(confuserds);
                }
                //ZD 101348 End                

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion

        #region InsertCDRParty
        /// <summary>
        /// InsertCDRParty
        /// </summary>
        /// <param name="Party"></param>
        /// <returns></returns>
        internal bool InsertCDRParty(NS_MESSENGER.MCU.CDRParty Party)
        {
            bool ret = false;
            string query = "";
            try
            {
				//ZD 101348 Starts
                if (Party.IsVMRConf == "1" || Party.IsPermanentConf == "1")
                {
                    query = " Insert into Conf_VMRCDREndpoints_D (ConfId,InstanceId,ConfNumName,OrgID,ConfGUID,PartyGUID,ConfTimeZone,EndpointName,EndpointAddress,EndpointType,ConnectionType,EndpointDateTime,EndpointConnectStatus,DisconnectReason,LastModifiedDateTime,EventId)";
                    query += " Values(" + Party.confid + "," + Party.InstanceId + "," + Party.ConfNumName + "," + Party.OrgID + ",'" + Party.ConfGUID + "','" + Party.EndpointAddress + "','" + Party.ConfTimeZone + "','" + Party.EndpointName + "','" + Party.EndpointAddress + "','" + Party.EndpointType + "','" + Party.ConnectionType + "','" + DateTime.Parse(Party.EndpointDateTime).ToString() + "'," + Party.EndpointConnectStatus + ",'" + Party.DisconnectReason + "','" + DateTime.Now + "','" + Party.EventId + "')";
                }
                else
                {
                    query = " Insert into Conf_CDREndpoints_D (ConfId,InstanceId,ConfNumName,OrgID,ConfGUID,PartyGUID,ConfTimeZone,EndpointName,EndpointAddress,EndpointType,ConnectionType,EndpointDateTime,EndpointConnectStatus,DisconnectReason,LastModifiedDateTime,EventId)";
                    query += " Values(" + Party.confid + "," + Party.InstanceId + "," + Party.ConfNumName + "," + Party.OrgID + ",'" + Party.ConfGUID + "','" + Party.partyGuid + "','" + Party.ConfTimeZone + "','" + Party.EndpointName + "','" + Party.EndpointAddress + "','" + Party.EndpointType + "','" + Party.ConnectionType + "','" + DateTime.Parse(Party.EndpointDateTime).ToString() + "'," + Party.EndpointConnectStatus + ",'" + Party.DisconnectReason + "','" + DateTime.Now + "','" + Party.EventId + "')";
                }
				//ZD 101348 End
                ret = NonSelectCommand(query);
                if (!ret)
                {
                    logger.Exception(100, "Error executing query." + query);
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                logger.Trace("Error in InsertCDRParty" + ex.Message);
                return false;
            }
        }
        #endregion

        #region FetchConferenceCDREvent
        /// <summary>
        /// FetchConferenceCDREvent
        /// </summary>
        /// <param name="cMcuList"></param>
        /// <returns></returns>
        internal bool FetchConferenceCDREvent(string uniqueId, ref NS_MESSENGER.CDREvent.VMRCDR VMRCDR, NS_MESSENGER.MCU MCU, ref int TotalPortMinActual, int AccessType, ref bool AlreadyExist, ref DateTime confStartDate)//ZD 101348
        {
            string dsTable = "", sQuery = "", EventType = "", dsTable1 = "";
            bool ret = false;
            DataTable table = new DataTable();
            DataTable table1 = new DataTable();
            int Porttimeinsecond = 0;
            try
            {
                DataSet ds = new DataSet();
                DataSet ds1 = new DataSet();
                dsTable = "CDR";
                dsTable1 = "VMRCDR";

                switch (MCU.etType)
                {
                    case NS_MESSENGER.MCU.eType.CODIAN:
                        {
                            #region CODIAN
							//ZD 101348
                            //if (AccessType == 1)
                            sQuery = "Select * from CDR_Codian_D where Confnumname = " + uniqueId.ToString();
                            //else
                            //    sQuery = "Select * from CDR_Codian_D where uniqueId = " + uniqueId.ToString();

                            ret = SelectCommand(sQuery, ref ds, dsTable);
                            if (!ret)
                            {
                                logger.Exception(100, "SQL error");
                                return (false);
                            }

                            if (ds.Tables[dsTable].Rows.Count < 1)
                            {
                                logger.Trace("No bridge found...");
                                return false;
                            }
                            //ZD 101348 Starts
                            DataRow[] foundRows = null;
                            string expression;
                            if (AccessType == 0)
                            {
                                expression = "conferenceFinished";
                                table = ds.Tables[dsTable];
                                foundRows = table.Select("EventType = '" + expression + "'");
                            }
                            //ZD 101348 End
                            if (AccessType == 1 || (foundRows != null && foundRows.Length > 0))//ZD 101348
                            {
                                for (int i = 0; i < ds.Tables[dsTable].Rows.Count; i++)
                                {
                                    if (ds.Tables[dsTable].Rows[i]["EventType"] != null)
                                    {
                                        if (ds.Tables[dsTable].Rows[i]["EventType"].ToString() != "")
                                        {
                                            EventType = ds.Tables[dsTable].Rows[i]["EventType"].ToString();
                                        }
                                    }

                                    if (ds.Tables[dsTable].Rows[i]["time"] != null)
                                    {
                                        if (ds.Tables[dsTable].Rows[i]["time"].ToString() != "")
                                        {
                                            VMRCDR.StartDate = ds.Tables[dsTable].Rows[i]["time"].ToString();
                                        }
                                    }

                                    if (ds.Tables[dsTable].Rows[i]["CDRIndex"] != null)
                                    {
                                        if (ds.Tables[dsTable].Rows[i]["CDRIndex"].ToString() != "")
                                        {
                                            VMRCDR.EventId = ds.Tables[dsTable].Rows[i]["CDRIndex"].ToString();
                                        }
                                    }

                                    if (EventType == "scheduledConferenceStarted")
                                    {
                                        if (ds.Tables[dsTable].Rows[i]["uniqueId"] != null)
                                        {
                                            if (ds.Tables[dsTable].Rows[i]["uniqueId"].ToString() != "")
                                            {
                                                VMRCDR.MCUConfGUID = ds.Tables[dsTable].Rows[i]["uniqueId"].ToString();
                                            }
                                            logger.Trace("VMRCDR.MCUConfGUIDuniqueId" + VMRCDR.MCUConfGUID);
                                        }
                                        if (ds.Tables[dsTable].Rows[i]["NumericId"] != null)
                                        {
                                            if (ds.Tables[dsTable].Rows[i]["NumericId"].ToString() != "")
                                            {
                                                VMRCDR.VMRAddress = ds.Tables[dsTable].Rows[i]["NumericId"].ToString(); //VMR ID
                                            }
                                            logger.Trace("VMRCDR.VMRAddressNumericId" + VMRCDR.VMRAddress);
                                        }
                                        if (ds.Tables[dsTable].Rows[i]["time"] != null)
                                        {
                                            if (ds.Tables[dsTable].Rows[i]["time"].ToString() != "")
                                            {
                                                DateTime.TryParse(ds.Tables[dsTable].Rows[i]["time"].ToString(), out confStartDate);
                                            }
                                        }
                                      
                                    }

                                    if (EventType == "participantLeft")
                                    {
                                        if (ds.Tables[dsTable].Rows[i]["timeInConference"] != null)
                                        {
                                            if (ds.Tables[dsTable].Rows[i]["timeInConference"].ToString() != "")
                                            {
                                                Int32.TryParse(ds.Tables[dsTable].Rows[i]["timeInConference"].ToString(), out Porttimeinsecond);
                                                TotalPortMinActual += Porttimeinsecond;
                                            }
                                        }
                                    }

                                    if (EventType == "conferenceFinished")
                                    {
                                        if (ds.Tables[dsTable].Rows[i]["durationInMinutes"] != null)
                                        {
                                            if (ds.Tables[dsTable].Rows[i]["durationInMinutes"].ToString() != "")
                                            {
                                                VMRCDR.TotalMinActual = ds.Tables[dsTable].Rows[i]["durationInMinutes"].ToString();
                                            }
                                        }
                                    }
                                }
                            }
                            return (true);

                            #endregion
                        }
                    case NS_MESSENGER.MCU.eType.CISCOTP: //FB 2593
                        {
                            #region CISCOTP

                            sQuery = " Select * from CDR_Cisco_D where  ConferenceGUID = '" + uniqueId.ToString() + "'";
                            ret = SelectCommand(sQuery, ref ds, dsTable);
                            if (!ret)
                            {
                                logger.Exception(100, "SQL error");
                                return (false);
                            }

                            if (ds.Tables[dsTable].Rows.Count < 1)
                            {
                                logger.Trace("No bridge found...");
                                return false;
                            }

                            //ZD 101348 Starts
                            DataRow[] foundRows = null;
                            string expression;
                            if (AccessType == 0)
                            {
                                expression = "conferenceFinished";
                                table = ds.Tables[dsTable];
                                foundRows = table.Select("EventType = '" + expression + "'");
                            }
                            //ZD 101348 End

                            /*if (AccessType == 2)
                            {
                                sQuery = " Select * from VMR_CDR_Logs_D ";
                                ret = SelectCommand(sQuery, ref ds1, dsTable1);
                                if (!ret)
                                {
                                    logger.Exception(100, "SQL error");
                                    return (false);
                                }

                                table1 = ds1.Tables[dsTable1];

                                DataRow[] foundRows1;
                                foundRows1 = table1.Select("MCUConfGUID = '" + uniqueId.ToString() + "'");
                                if (foundRows1.Length > 0)
                                    AlreadyExist = true;
                            }
                            */
                            if (AccessType == 1 || (foundRows != null && foundRows.Length > 0))//ZD 101348
                            {
                                for (int i = 0; i < ds.Tables[dsTable].Rows.Count; i++)
                                {
                                    if (ds.Tables[dsTable].Rows[i]["EventType"] != null)
                                    {
                                        if (ds.Tables[dsTable].Rows[i]["EventType"].ToString() != "")
                                        {
                                            EventType = ds.Tables[dsTable].Rows[i]["EventType"].ToString();
                                        }
                                    }

                                    if (ds.Tables[dsTable].Rows[i]["CDRIndex"] != null)
                                    {
                                        if (ds.Tables[dsTable].Rows[i]["CDRIndex"].ToString() != "")
                                        {
                                            VMRCDR.EventId = ds.Tables[dsTable].Rows[i]["CDRIndex"].ToString();
                                        }
                                    }

                                    if (ds.Tables[dsTable].Rows[i]["time"] != null)
                                    {
                                        if (ds.Tables[dsTable].Rows[i]["time"].ToString() != "")
                                        {
                                            VMRCDR.StartDate = ds.Tables[dsTable].Rows[i]["time"].ToString();
                                        }
                                    }

                                    if (EventType == "conferenceStarted")
                                    {
                                        if (ds.Tables[dsTable].Rows[i]["ConferenceGUID"] != null)
                                        {
                                            if (ds.Tables[dsTable].Rows[i]["ConferenceGUID"].ToString() != "")
                                            {
                                                VMRCDR.MCUConfGUID = ds.Tables[dsTable].Rows[i]["ConferenceGUID"].ToString();
                                            }
                                        }
                                        if (ds.Tables[dsTable].Rows[i]["NumericId"] != null)
                                        {
                                            if (ds.Tables[dsTable].Rows[i]["NumericId"].ToString() != "")
                                            {
                                                VMRCDR.VMRAddress = ds.Tables[dsTable].Rows[i]["NumericId"].ToString();
                                            }
                                        }
                                        if (ds.Tables[dsTable].Rows[i]["time"] != null)
                                        {
                                            if (ds.Tables[dsTable].Rows[i]["time"].ToString() != "")
                                            {
                                                DateTime.TryParse(ds.Tables[dsTable].Rows[i]["time"].ToString(), out confStartDate);
                                            }
                                        }
                                       
                                    }

                                    if (EventType == "participantLeft")
                                    {
                                        if (ds.Tables[dsTable].Rows[i]["timeInConference"] != null)
                                        {
                                            if (ds.Tables[dsTable].Rows[i]["timeInConference"].ToString() != "")
                                            {
                                                Int32.TryParse(ds.Tables[dsTable].Rows[i]["timeInConference"].ToString(), out Porttimeinsecond);
                                                TotalPortMinActual += Porttimeinsecond;
                                            }
                                        }
                                    }

                                    //if (EventType == "conferenceInactive") //FB 2593
                                    //{
                                    //    if (ds.Tables[dsTable].Rows[i]["sessionDuration"] != null)
                                    //    {
                                    //        if (ds.Tables[dsTable].Rows[i]["sessionDuration"].ToString() != "")
                                    //        {
                                    //            VMRCDR.TotalMinActual = ds.Tables[dsTable].Rows[i]["sessionDuration"].ToString();
                                    //        }
                                    //    }
                                    //}
                                    if (EventType == "conferenceFinished") //FB 2593
                                    {
                                        if (ds.Tables[dsTable].Rows[i]["duration"] != null)
                                        {
                                            if (ds.Tables[dsTable].Rows[i]["duration"].ToString() != "")
                                            {
                                                VMRCDR.TotalMinActual = ds.Tables[dsTable].Rows[i]["duration"].ToString();
                                            }
                                        }
                                    }
                                    
                                }
                            }
                            return (true);

                            #endregion
                        }
                    default:
                        {
                            this.errMsg = "Unknown MCU etType.";
                            return false;
                        }
                }

            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion

        #region FetchPartyCDREvent
        /// <summary>
        /// FetchPartyCDREvent
        /// </summary>
        /// <param name="cMcuList"></param>
        /// <returns></returns>
        internal bool FetchPartyCDREvent(string uniqueId,int partyid, ref DataSet ds , NS_MESSENGER.MCU MCU)
        {
            string dsTable = "", sQuery = "";
            bool ret = false;
            DataTable table = new DataTable();
            try
            {
                ds = new DataSet();
                dsTable = "CDR";

                switch (MCU.etType)
                {
                    case NS_MESSENGER.MCU.eType.CISCOTP: //FB 2593
                        {
                            #region CISCOTP

                            sQuery = "Select * from CDR_Cisco_D where  ParticipantGUID = '" + uniqueId.ToString()+"'";
                            ret = SelectCommand(sQuery, ref ds, dsTable);
                            if (!ret)
                            {
                                logger.Exception(100, "SQL error");
                                return (false);
                            }

                            if (ds.Tables[dsTable].Rows.Count < 1)
                            {
                                logger.Trace("No bridge found...");
                                return false;
                            }

                            return (true);

                            #endregion
                        }
                    case NS_MESSENGER.MCU.eType.CODIAN: //FB 2593
                        {
                            #region CODIAN

                            sQuery = "Select * from CDR_Codian_D where  uniqueId = '" + uniqueId.ToString() + "' and EndpointId = " + partyid.ToString();
                            ret = SelectCommand(sQuery, ref ds, dsTable);
                            if (!ret)
                            {
                                logger.Exception(100, "SQL error");
                                return (false);
                            }

                            if (ds.Tables[dsTable].Rows.Count < 1)
                            {
                                logger.Trace("No bridge found...");
                                return false;
                            }

                            return (true);

                            #endregion
                        }
                    default:
                        {
                            this.errMsg = "Unknown MCU etType.";
                            return false;
                        }
                }

            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion

        #region FetchVMRConferenceUniqueid
        /// <summary>
        /// FetchVMRConferenceUniqueid
        /// </summary>
        /// <param name="cMcuList"></param>
        /// <returns></returns>
        internal bool FetchVMRConferenceUniqueid(ref List<string> uniqueId, NS_MESSENGER.MCU MCU)
        {
            string iUniqueid = "";
            string dsTable = "", sQuery = "";
            bool ret = false;
            try
            {
                DataSet ds = new DataSet();
                dsTable = "CDR";

                switch (MCU.etType)
                {
                    case NS_MESSENGER.MCU.eType.CODIAN:
                        {
                            #region CODIAN

                            sQuery = "Select uniqueId from CDR_Codian_D where  scheduledTime = 'permanent' ";

                            ret = SelectCommand(sQuery, ref ds, dsTable);
                            if (!ret)
                            {
                                logger.Exception(100, "SQL error");
                                return (false);
                            }

                            if (ds.Tables[dsTable].Rows.Count < 1)
                            {
                                logger.Trace("No bridge found...");
                                return false;
                            }

                            for (int i = 0; i < ds.Tables[dsTable].Rows.Count; i++)
                            {
                                if (ds.Tables[dsTable].Rows[i]["uniqueId"] != null)
                                {
                                    if (ds.Tables[dsTable].Rows[i]["uniqueId"].ToString() != "")
                                    {
                                        iUniqueid = ds.Tables[dsTable].Rows[i]["uniqueId"].ToString();
                                    }
                                }
                                if (uniqueId == null)
                                    uniqueId = new List<string>();
                                if (uniqueId != null && iUniqueid != "")
                                    uniqueId.Add(iUniqueid);
                            }
                            return (true);

                            #endregion
                        }
                    case NS_MESSENGER.MCU.eType.CISCOTP: //FB 2593
                        {
                            #region CISCOTP
                            sQuery = "Select ConferenceGUID from CDR_Cisco_D where  ScheduledTime = '0' and EventType='conferenceStarted' ";
                            ret = SelectCommand(sQuery, ref ds, dsTable);
                            if (!ret)
                            {
                                logger.Exception(100, "SQL error");
                                return (false);
                            }

                            if (ds.Tables[dsTable].Rows.Count < 1)
                            {
                                logger.Trace("No bridge found...");
                                return false;
                            }

                            for (int i = 0; i < ds.Tables[dsTable].Rows.Count; i++)
                            {
                                if (ds.Tables[dsTable].Rows[i]["ConferenceGUID"] != null)
                                {
                                    if (ds.Tables[dsTable].Rows[i]["ConferenceGUID"].ToString() != "")
                                    {
                                        iUniqueid = ds.Tables[dsTable].Rows[i]["ConferenceGUID"].ToString();
                                    }
                                }
                                if (uniqueId == null)
                                    uniqueId = new List<string>();
                                if (uniqueId != null && iUniqueid != "")
                                    uniqueId.Add(iUniqueid);
                            }
                            return (true);
                            #endregion
                        }

                    default:
                        {
                            this.errMsg = "Unknown MCU etType.";
                            return false;
                        }
                }
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion

        #region InsertVMRConference
        /// <summary>
        /// InsertVMRConference
        /// </summary>
        /// <param name="evnt"></param>
        /// <returns></returns>
        internal bool InsertVMRConference(NS_MESSENGER.CDREvent.VMRCDR VMRCDR)
        {
            bool ret = false;
            string query = "";

            try
            {
                //ret = true;
                //RPRM VMR Starts
                if (VMRCDR.BridgeTypeID == "13")
                    query += "IF NOT EXISTS(SELECT 1 FROM VMR_CDR_Logs_D WHERE mcuconfguid ='" + VMRCDR.MCUConfGUID + "')";
                //RPRM VMR Ends
                
                query += " Insert into VMR_CDR_Logs_D ([MCUConfGUID],[VMRAddress],[StartDate],[TotalMinActual],[TotalPortMinActual],[LastModifiedDateTime],[OrgId],[MCUid],[MCUName],[MCUType],[MCUAddress]) ";
                query += " values ( '" + VMRCDR.MCUConfGUID + "', '" + VMRCDR.VMRAddress + "', '" + VMRCDR.StartDate + "','" + VMRCDR.TotalMinActual + "',";
                query += "'" + VMRCDR.TotalPortMinActual + "', '" + DateTime.Now + "', '" + VMRCDR.OrgId + "', '" + VMRCDR.MCUid + "', '" + VMRCDR.MCUname + "','" + VMRCDR.MCUtype + "', '" + VMRCDR.MCUAddress + "')";

                ret = NonSelectCommand(query);
                if (!ret)
                {
                    logger.Exception(100, "query failed. query =" + query);
                    return false;
                }
            }
            catch (Exception ex)
            {
                logger.Trace("InsertVMRConference :" + ex.Message);
                return false;
            }
            return true;
        }
        #endregion

        #region UpdateVMRConference
        /// <summary>
        /// UpdateVMRConference
        /// </summary>
        /// <param name="evnt"></param>
        /// <returns></returns>
        internal bool UpdateVMRConference(NS_MESSENGER.CDREvent.VMRCDR VMRCDR)
        {
            bool ret = false;
            string query = "";

            try
            {
                ret = true;
                query += " Update VMR_CDR_Logs_D SET [VMRAddress] = '" + VMRCDR.VMRAddress + "',[StartDate] = '" + VMRCDR.StartDate + "',[TotalMinActual] = '" + VMRCDR.TotalMinActual + "',";
                query += " [TotalPortMinActual] = '" + VMRCDR.TotalPortMinActual + "',[LastModifiedDateTime] = '" + DateTime.Now + "',[OrgId] = '" + VMRCDR.OrgId + "',[MCUid] = '" + VMRCDR.MCUid + "',[MCUName] = '" + VMRCDR.MCUname + "',";
                query += " [MCUType] = '" + VMRCDR.MCUtype + "',[MCUAddress] = '" + VMRCDR.MCUAddress + "' WHERE [MCUConfGUID] = '" + VMRCDR.MCUConfGUID + "'";

                ret = NonSelectCommand(query);
                if (!ret)
                {
                    logger.Exception(100, "query failed. query =" + query);
                    return false;
                }
            }
            catch (Exception ex)
            {
                logger.Trace("InsertVMRConference :" + ex.Message);
                return false;
            }
            return true;
        }
        #endregion

        //FB 2593 End

        //FB 2797 Start

        #region UpdateConferenceUniqueid
        /// <summary>
        /// UpdateConferenceUniqueid
        /// </summary>
        /// <param name="confid"></param>
        /// <param name="instanceid"></param>
        /// <param name="confStatus"></param>
        /// <returns></returns>
        public bool UpdateConferenceUniqueid(int confid, int instanceid, string Uniqueid, string locked, int confStatus) //FB 2971 //ZD 100085
        {
            //ZD 100608 Starts
            string query = "";
            query = "update Conf_Conference_D set ";
            
            if (Uniqueid != "") 
                query += "Uniqueid = " + Uniqueid + ", ";
            //ZD 100608 Ends

            query += "Status = " + confStatus + "";//ZD 100085
            query += " where confid = " + confid.ToString();
            query += " and instanceid = " + instanceid.ToString();
            //FB 2971 Start
            if (locked != "") //ZD 100608 Starts
            {
                query += "update Conf_AdvAVParams_D set ConfLockUnlock = " + locked + "";
                query += " where confid = " + confid.ToString();
                query += " and instanceid = " + instanceid.ToString();
            }//ZD 100608 Ends
            //FB 2971 End

            bool ret = NonSelectCommand(query);
            if (!ret)
            {
                logger.Exception(100, "Error executing query.");
                return false;
            }
            return true;
        }

        #endregion

        #region FetchPartyIdCDREvent
        /// <summary>
        /// FetchPartyIdCDREvent
        /// </summary>
        /// <param name="confid"></param>
        /// <param name="instanceid"></param>
        /// <param name="confStatus"></param>
        /// <returns></returns>
        public bool FetchPartyIdCDREvent(string ipaddress, string confuid,ref List<int> partyid)
        {
            DataSet ds = new DataSet();
            string dsTable = "CDR";
            int eptid = 0;
			//ZD 101348
            string query = "select EndpointId from CDR_Codian_D Where EndpointIPAddress = '" + ipaddress + "' or EndpointDN = '" + ipaddress + "' or Endpointh323Alias = '" + ipaddress + "' ";
            query += " and uniqueId = '" + confuid + "'";

            bool ret = false;
            ret = SelectCommand(query, ref ds, dsTable);
            if (!ret)
            {
                logger.Exception(100, "SQL error");
                return (false);
            }

            if (ds.Tables[dsTable].Rows.Count < 1)
            {
                logger.Trace("No Party found.");
                return false;
            }

            for (int i = 0; i < ds.Tables[dsTable].Rows.Count; i++)
            {
                int.TryParse(ds.Tables[dsTable].Rows[i]["EndpointId"].ToString().Trim(), out eptid);
                if (eptid > 0)
                {
                    if (partyid == null)
                        partyid = new List<int>();

                    if (partyid != null)
                        if(!partyid.Contains(eptid))
                            partyid.Add(eptid);
                }
            }
       
            
            return true;
        }

        #endregion

        //FB 2797 End

        //FB 2683 Starts

        //RMX Starts

        #region UpdateConfUIDonMCU
        /// <summary>
        /// Code to update the  GUID in conf_bridge_d
        /// </summary>
        /// <param name="confUIDonMCU"></param>
        /// <param name="conf"></param>
        /// <returns></returns>
        internal bool UpdateConfUIDonMCU(string confUIDonMCU, NS_MESSENGER.Conference conf)
        {
            string query = "";
            bool ret = false;
            try
            {
                query = "update conf_bridge_d set ConfUIDonMCU= '" + confUIDonMCU + "' where confid=" + conf.iDbID + " and instanceid=" + conf.iInstanceID;
                ret = NonSelectCommand(query);
                if (!ret)
                {
                    logger.Exception(100, "query failed. query =" + query);
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {

                logger.Trace("UpdateConfUIDonMCU :" + ex.Message);
                return false;
            }
        }
        #endregion

        #region selectMCUGUID
        /// <summary>
        /// select the MCU conference GUID for myvrm 
        /// </summary>
        /// <param name="ds"></param>
        /// <param name="MCU"></param>
        /// <returns></returns>
        internal bool selectMCUGUID(ref DataSet ds, NS_MESSENGER.MCU MCU)
        {
            string query = "", dsTable = "PolycomRMXGUID";
            bool ret = false;

            try
            {
                query = "select B.confuidonmcu as confuidonmcu,";
                query = query + "B.confuid as confnumname,";
                query = query + "B.confid as confid,";
                query = query + "C.orgid as orgid,";
                query = query + "B.instanceid as instanceid,";
                query = query + "B.bridgeid as bridgeid,";
                query = query + "B.bridgename as bridgename,";
                query = query + "B.bridgetypeid as bridgetypeid,";
                query = query + "B.bridgeipisdnaddress as bridgeipisdnaddress,";
                query = query + "B.CDRFetchStatus as CDRFetchStatus,";
                query = query + "M.name as BridgeType,";
                query = query + "U.FirstName +'  '+ U.LastName as Host,";
                query = query + "c.timezone as TimeZone,";
                query = query + "c.externalname as Title,";
                query = query + "c.guid as GUID,";
                query = query + " C.ConfDate as scheduledstart,";
                query = query + "DATEADD (mi , c.duration , C.ConfDate ) as Scheduledend";
                query = query + " from conf_bridge_d B,";
                query = query + " conf_conference_d C,";
                query = query + " mcu_vendor_s M,";
                query = query + " usr_list_d U,";
                query = query + " gen_timezone_s T";
                query = query + " where";
                query = query + " M.id=B.bridgetypeid and";
                query = query + " B.instanceid=c.instanceid and";
                query = query + " B.confuid=c.confnumname and ";
                query = query + " C.owner=U.userid and";
                query = query + " c.TimeZone=T.TimeZoneID and";
                query = query + " Getutcdate()>DATEADD(MI,c.duration,c.conftime) and ";

                if (MCU.sMcuType == 8)
                {
                    query = query + " B.CDRFetchStatus = 0 and ";
                    query = query + " B.confuidonmcu is not null and";
                }
                if (MCU.sMcuType == 13)
                {
                    query = query + " B.CDRFetchStatus = 0 and ";
                    query = query + " c.guid<>'' and ";
                }
                query = query + " B.bridgeid=" + MCU.iDbId;


                ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Trace("Error in Fetch Command");
                }
                return true;
            }
            catch (Exception ex)
            {
                logger.Trace("selectPolycomRMXGUID :" + ex.Message);
                return false;
            }
        }
        #endregion

        #region GetConferenceIDfromParty
        /// <summary>
        /// GetConferenceIDfromParty
        /// </summary>
        /// <param name="confid"></param>
        /// <param name="instanceid"></param>
        /// <param name="partyGUID"></param>
        /// <param name="ds"></param>
        /// <returns></returns>
        public bool GetConferenceIDfromParty(List<string> partyGUID, ref DataSet ds)
        {
            string query = "", tempquery = "";
            ds = new DataSet();
            string dsTable = "endpoint";
            bool ret = false;
            string tblName = "";
            try
            {
                var ExistinglistArray = partyGUID.Select(r => r.ToString()).ToArray();
                var ExistcommaSeperated = string.Join("','", ExistinglistArray);
                query = "Select confID, InstanceID,PartyName,GUID,TerminalType from {0} where GUID  in ('" + ExistcommaSeperated + "')";

                tblName = "Conf_Room_D";
                tempquery = string.Format(query, tblName);
                ret = SelectCommand(tempquery, ref ds, dsTable);
                if (!ret)
                {
                    logger.Trace("SQL error");
                    return (false);
                }

                tblName = "Conf_User_D";
                dsTable = "ConfUser";
                tempquery = string.Format(query, tblName);
                ret = SelectCommand(tempquery, ref ds, dsTable);
                if (!ret)
                {
                    logger.Trace("SQL error");
                    return (false);
                }

                ds.Tables[0].Merge(ds.Tables[1]); //FB 2593


                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }

        }

        #endregion

        #region UpdateCDRFetchStatus
        /// <summary>
        /// Update RMX CDR Fetch status in conf_bridge_d
        /// </summary>
        /// <param name="MyvrmCDRRMX"></param>
        /// <returns></returns>
        internal bool UpdateCDRFetchStatus(string Confnumname)
        {
            logger.Trace("Entering into UpdateCDRFetchStatus");
            bool ret = false;
            string query = "";
            try
            {

                query = " Update Conf_Bridge_D Set CDRFetchStatus = 1 Where Confuid = " + Confnumname ;
                
                ret = NonSelectCommand(query);
                if (!ret)
                {
                    logger.Exception(100, " query failed. query =" + query);
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                logger.Trace("selectPolycomRMXCDR :" + ex.Message);
                return false;
            }
        }
        #endregion

        //RMX Ends

        //RPRM Starts

        #region selectLastModifiedDateTime
        /// <summary>
        /// selectLastModifiedDateTime
        /// </summary>
        /// <param name="MCU"></param>
        /// <param name="StartDate"></param>
        /// <returns></returns>
        internal bool selectLastModifiedDateTime(NS_MESSENGER.MCU MCU, ref string StartDate)
        {
            string query = "";
            bool ret = false;
            DataSet ds = new DataSet();
            DateTime dttemp = new DateTime();
            try
            {
                query = "select LastModifiedDateTime as LastModified from conf_cdr_logs_d where mcutypeid=" + MCU.sMcuType;
                ret = SelectCommand(query, ref ds, "conf_cdr_logs_d");
                if (!ret)
                {
                    logger.Trace("Error in Fetch Command");
                }
                if (ds.Tables[0].Rows.Count > 0)
                    StartDate = ds.Tables[0].Rows[0]["LastModified"].ToString();
                else
                    StartDate = "";
                if (StartDate == "")
                {
                    dttemp = DateTime.Now;
                    query = "select ConfDate as LastModified from conf_conference_d where confid=12";
                    ds = new DataSet();
                    ret = SelectCommand(query, ref ds, "Conf_confernece_d");
                    if (!ret)
                    {
                        logger.Trace("Error in Fetch Command");
                    }
                    if (ds.Tables[0].Rows.Count > 0)
                        StartDate = ds.Tables[0].Rows[0]["LastModified"].ToString();
                    else
                        StartDate = "";

                }

                return true;

            }
            catch (Exception ex)
            {
                logger.Trace("Error in Fetching selectLastModifiedDateTime" + ex.Message);
                return false;
            }
        }
        #endregion

        #region InsertRPRMCDRinMyvrm
        /// <summary>
        /// InsertRPRMCDRinMyvrm
        /// </summary>
        /// <param name="CSVTable"></param>
        /// <param name="excelFiles"></param>
        /// <returns></returns>
        internal bool InsertRPRMCDRinMyvrm(DataTable CSVTable, string excelFiles)
        {
            logger.Trace("Entering into InsertRPRMCDRinMyvrm");
            bool ret = false;
            string query = "";
            DataSet ExistingCDR = new DataSet();
            DataTable FilteredCDR = new DataTable();
            string sqlQuery = "";
            try
            {

                using (System.Data.SqlClient.SqlBulkCopy sbc = new System.Data.SqlClient.SqlBulkCopy(conStr))
                {
                    if (excelFiles.Contains("calls"))
                    {
                        var Existinglist = (CSVTable.Rows.OfType<DataRow>().Select(dr => new NS_MESSENGER.MCU.PolycomRPRMCDR
                        {
                            Calluuid = dr["CallUuid"].ToString(),
                        }).ToList());

                        if (Existinglist.Count > 0)
                        {
                            var ExistinglistArray = Existinglist.Select(r => r.Calluuid.ToString()).ToArray();
                            var ExistcommaSeperated = string.Join("','", ExistinglistArray);

                            query = "select * from CDR_PolycomRPRMCalls_d where Calluuid in ('" + ExistcommaSeperated + "')";

                            ret = SelectCommand(query, ref ExistingCDR, "CDR_PolycomRPRMCalls_d");
                            if (!ret)
                            {
                                logger.Trace("Error in Fetching CDR_PolycomRPRMCalls_d");
                                return false;
                            }
                            Existinglist = (ExistingCDR.Tables[0].Rows.OfType<DataRow>().Select(dr => new NS_MESSENGER.MCU.PolycomRPRMCDR
                            {
                                Calluuid = dr["CallUuid"].ToString(),
                            }).ToList());
                            if (Existinglist.Count > 0)
                            {
                                ExistinglistArray = Existinglist.Select(r => r.Calluuid.ToString()).ToArray();
                                ExistcommaSeperated = string.Join("','", ExistinglistArray);

                                CSVTable.DefaultView.RowFilter = ("callUuid NOT IN('" + ExistcommaSeperated + "')");
                                FilteredCDR = CSVTable.DefaultView.ToTable();
                            }
                            else
                                FilteredCDR = CSVTable;

                            for(int i=0;i<FilteredCDR.Rows.Count;i++)
                            {
                                sqlQuery = "Insert into CDR_PolycomRPRMCalls_d (version,type,calltype,calluuid,dialin,starttime,endtime,origendpoint,dialstring,destEndpoint,origsignaltype,destsignaltype,refconfuuid,lastforwardendpoint,cause,causesource,bitrate,classofservice,ingresscluster,egresscluster,vmrcluster,veqcluster,userrole,userdataa,userdatab,userdatac,userdatad,userdatae) values";
                                sqlQuery = sqlQuery + " ('" + FilteredCDR.Rows[i]["version"].ToString() + "','" + FilteredCDR.Rows[i]["type"].ToString() + "','" + FilteredCDR.Rows[i]["calltype"].ToString() + "','" + FilteredCDR.Rows[i]["calluuid"].ToString() + "',";
                                sqlQuery = sqlQuery + "'" + FilteredCDR.Rows[i]["dialin"].ToString() + "','" + DateTime.Parse(FilteredCDR.Rows[i]["starttime"].ToString()) + "','" + DateTime.Parse(FilteredCDR.Rows[i]["endtime"].ToString()) + "','" + FilteredCDR.Rows[i]["origendpoint"].ToString() + "',";
                                sqlQuery = sqlQuery + "'" + FilteredCDR.Rows[i]["dialstring"].ToString() + "','" + FilteredCDR.Rows[i]["destEndpoint"].ToString() + "','" + FilteredCDR.Rows[i]["origsignaltype"].ToString() + "','" + FilteredCDR.Rows[i]["destsignaltype"].ToString() + "','" + FilteredCDR.Rows[i]["refconfuuid"].ToString() + "',";
                                sqlQuery = sqlQuery + "'" + FilteredCDR.Rows[i]["lastforwardendpoint"].ToString() + "','" + FilteredCDR.Rows[i]["cause"].ToString().Replace("'", "") + "','" + FilteredCDR.Rows[i]["causesource"].ToString().Replace("'", "") + "','" + FilteredCDR.Rows[i]["bitrate"].ToString() + "','" + FilteredCDR.Rows[i]["classofservice"].ToString() + "',";
                                sqlQuery = sqlQuery + "'" + FilteredCDR.Rows[i]["ingresscluster"].ToString() + "','" + FilteredCDR.Rows[i]["egresscluster"].ToString() + "','" + FilteredCDR.Rows[i]["vmrcluster"].ToString() + "','" + FilteredCDR.Rows[i]["veqcluster"].ToString() + "','" + FilteredCDR.Rows[i]["userrole"].ToString() + "',";
                                sqlQuery = sqlQuery + "'" + FilteredCDR.Rows[i]["userdataa"].ToString() + "','" + FilteredCDR.Rows[i]["userdatab"].ToString() + "','" + FilteredCDR.Rows[i]["userdatac"].ToString() + "','" + FilteredCDR.Rows[i]["userdatad"].ToString() + "','" + FilteredCDR.Rows[i]["userdatae"].ToString() + "')";

                                ret = NonSelectCommand(sqlQuery);
                                if (!ret)
                                     logger.Exception(100, "Error executing query." + query);
                                   
                               

                            }
                        }

                                               
                    }
                    else
                    {
                        var Existinglist = (CSVTable.Rows.OfType<DataRow>().Select(dr => new NS_MESSENGER.MCU.PolycomRPRMCDR
                        {
                            confuuid = dr["confuuid"].ToString(),
                        }).ToList());
                        if (Existinglist.Count > 0)
                        {
                            var ExistinglistArray = Existinglist.Select(r => r.confuuid.ToString()).ToArray();
                            var ExistcommaSeperated = string.Join("','", ExistinglistArray); ;

                            query = "select * from CDR_PolycomRPRMConference_d where confuuid in ('" + ExistcommaSeperated + "')";

                            ret = SelectCommand(query, ref ExistingCDR, "CDR_PolycomRPRMConference_d");
                            if (!ret)
                            {
                                logger.Trace("Error in Fetching CDR_PolycomRPRMConference_d");
                                return false;
                            }
                            Existinglist = (ExistingCDR.Tables[0].Rows.OfType<DataRow>().Select(dr => new NS_MESSENGER.MCU.PolycomRPRMCDR
                            {
                                confuuid = dr["confuuid"].ToString(),
                            }).ToList());

                            if (Existinglist.Count > 0)
                            {
                                ExistinglistArray = Existinglist.Select(r => r.confuuid.ToString()).ToArray();
                                ExistcommaSeperated = string.Join("','", ExistinglistArray); ;

                                CSVTable.DefaultView.RowFilter = ("confuuid NOT IN('" + ExistcommaSeperated + "')");
                                FilteredCDR = CSVTable.DefaultView.ToTable();
                            }
                            else
                                FilteredCDR = CSVTable;

                            for (int i = 0; i < FilteredCDR.Rows.Count; i++)
                            {
                                sqlQuery = " insert into cdr_polycomRPRMconference_d (version,type,conftype,cluster,confuuid,starttime,endtime,userid,roomid,partcount,classofservice,userdataa,userdatab,userdatac)";
                                sqlQuery = sqlQuery + " values('" + FilteredCDR.Rows[i]["version"].ToString() + "','" + FilteredCDR.Rows[i]["type"].ToString() + "','" + FilteredCDR.Rows[i]["conftype"].ToString() + "',";
                                sqlQuery = sqlQuery + "'" + FilteredCDR.Rows[i]["cluster"].ToString() + "','" + FilteredCDR.Rows[i]["confuuid"].ToString() + "','" + DateTime.Parse(FilteredCDR.Rows[i]["starttime"].ToString()) + "',";
                                sqlQuery = sqlQuery + "'" + DateTime.Parse(FilteredCDR.Rows[i]["endtime"].ToString()) + "','" + FilteredCDR.Rows[i]["userid"].ToString() + "',";
                                sqlQuery = sqlQuery + "'" + FilteredCDR.Rows[i]["roomid"].ToString() + "','" + FilteredCDR.Rows[i]["partcount"].ToString() + "','" + FilteredCDR.Rows[i]["classofservice"].ToString() + "',";
                                sqlQuery = sqlQuery + "'" + FilteredCDR.Rows[i]["userdataa"].ToString() + "','" + FilteredCDR.Rows[i]["userdatab"].ToString() + "','" + FilteredCDR.Rows[i]["userdatac"].ToString() + "')";
                                ret = NonSelectCommand(sqlQuery);
                                if (!ret)
                                   logger.Exception(100, "Error executing query." + query);
                            }
                            
                        }
                    }
                 
                }
                return true;
            }
            catch (Exception ex)
            {
                logger.Trace("selectPolycomRMXCDR :" + ex.Message);
                return false;
            }
        }
        #endregion

        #region UpdateLastModifiedDateTime
        /// <summary>
        /// UpdateLastModifiedDateTime
        /// </summary>
        /// <param name="mcu"></param>
        /// <returns></returns>
        internal bool UpdateLastModifiedDateTime(NS_MESSENGER.MCU mcu)
        {
            bool ret = false;
            string query = "";
            try
            {
                query = "update conf_cdr_logs_d set LastModifiedDateTime='" + DateTime.Now + "' where mcutypeid=" + mcu.sMcuType;
                ret = NonSelectCommand(query);
                if (!ret)
                {
                    logger.Exception(100, "Error executing query." + query);
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {

                logger.Trace("Error in updating LastDateTime" + ex.Message);
                return false;
            }
        }
        #endregion

        //RPRM Ends

        //FB 2683 Ends


        #region UpdateLastDateTimeCDRPoll
        /// <summary>
        /// UpdateLastDateTimeCDRPoll
        /// </summary>
        /// <param name="mcu"></param>
        /// <returns></returns>
        internal bool UpdateLastDateTimeCDRPoll(int BridgeId)
        {
            bool ret = false;
            string query = "";
            try
            {
                //Valli-table
                query = " Update Mcu_List_D Set LastDateTimeCDRPoll='" + DateTime.Now + "' where BridgeID = " + BridgeId + "";
                ret = NonSelectCommand(query);
                if (!ret)
                {
                    logger.Exception(100, "Error executing query." + query);
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {

                logger.Trace("Error in updating LastDateTime" + ex.Message);
                return false;
            }
        }
        #endregion

        //FB 2569 Start

        public bool FetchP2PConfAlerts(NS_MESSENGER.Conference conf, ref Queue alertQ)
        {
            // Retreive  the  alerts
            DataSet ds = new DataSet();
            string dsTable = "ConfP2PAlerts";
            string query = "Select C.AlertTypeID,C.[Message],C.[Timestamp],G.Description from Conf_P2PAlert_D C, Gen_AlertType_S G where C.ConfID = " + conf.iDbID.ToString() + " and C.InstanceID = " + conf.iInstanceID.ToString();
            query += " and C.AlertTypeID = G.AlertTypeID order by C.[Timestamp] desc";
            bool ret = SelectCommand(query, ref ds, dsTable);
            if (!ret)
            {
                logger.Exception(10, "SQL error");
                return (false);
            }

            for (int i = 0; i < ds.Tables[dsTable].Rows.Count; i++)
            {
                NS_MESSENGER.Alert alert = new NS_MESSENGER.Alert();
                try
                {
                    alert.confID = conf.iDbID;
                    alert.instanceID = conf.iInstanceID;
                    alert.typeID = Int32.Parse(ds.Tables[dsTable].Rows[i]["AlertTypeID"].ToString().Trim());
                    alert.message = ds.Tables[dsTable].Rows[i]["Message"].ToString().Trim();
                    alert.timestamp = DateTime.Parse(ds.Tables[dsTable].Rows[i]["Timestamp"].ToString().Trim());

                    alertQ.Enqueue(alert);
                }
                catch (Exception e)
                {
                    logger.Trace("Alert not in correct format : " + e.Message);
                    continue;
                }
            }
            return true;
        }

        //FB 2569 End

        //ZD 100190 - Starts
        #region UpdatePushToExternal
        /// <summary>
        /// UpdatePushToExternal
        /// </summary>
        /// <param name="confid"></param>
        /// <param name="instanceid"></param>
        /// <param name="IsPushToExternal"></param>
        /// <returns></returns>
        public bool UpdatePushToExternal(int confid, int instanceid, int IsPushToExternal)
        {
            StringBuilder query = new StringBuilder();
            query.Append(" update Conf_Conference_D set PushedToExternal = '" + IsPushToExternal + "' ");
            query.Append(" where confid = '" + confid + "'");
            query.Append(" and instanceid = '" + instanceid + "'");

            bool ret = NonSelectCommand(query.ToString());
            if (!ret)
            {
                logger.Exception(100, "Error executing query.");
                return false;
            }
            return true;
        }
        #endregion
        //ZD 100190 - End

        //ZD 100518 Starts
        #region FetchMaxParticipants
        /// <summary>
        /// FetchMaxParticipants
        /// </summary>
        /// <param name="OrgId"></param>
        /// <param name="OrgMaxParticipants"></param>
        /// <returns></returns>
        public bool FetchMaxParticipants(String OrgId, ref int OrgMaxParticipants)
        {

            DataSet ds = new DataSet();
            string dsTable = "Organization";
            string query = "Select MaxParticipants from Org_Settings_D where OrgId = '" + OrgId + "'";
            bool ret = SelectCommand(query, ref ds, dsTable);
            if (!ret)
            {
                logger.Exception(10, "SQL error");
                return (false);
            }
            for (int i = 0; i < ds.Tables[dsTable].Rows.Count; i++)
            {
                try
                {
                    if (ds.Tables[dsTable].Rows[i]["MaxParticipants"] != null)
                    {
                        if (ds.Tables[dsTable].Rows[i]["MaxParticipants"].ToString() != "")
                        {
                            int.TryParse(ds.Tables[dsTable].Rows[i]["MaxParticipants"].ToString(), out OrgMaxParticipants);
                        }
                    }
                }
                catch (Exception e)
                {
                    logger.Exception(100, e.Message);
                }
            }
            return true;
        }
        #endregion

        #region FetchMaxConcurrentCall
       /// <summary>
        /// FetchMaxConcurrentCall
       /// </summary>
       /// <param name="OrgId"></param>
       /// <param name="OrgMaxConCrntCall"></param>
       /// <returns></returns>
        public bool FetchMaxConcurrentCall(String OrgId, ref int OrgMaxConCrntCall)
        {

            DataSet ds = new DataSet();
            string dsTable = "Organization";
            string query = "Select MaxConcurrentCall from Org_Settings_D where OrgId = '" + OrgId + "'";
            bool ret = SelectCommand(query, ref ds, dsTable);
            if (!ret)
            {
                logger.Exception(10, "SQL error");
                return (false);
            }
            for (int i = 0; i < ds.Tables[dsTable].Rows.Count; i++)
            {
                try
                {
                    if (ds.Tables[dsTable].Rows[i]["MaxConcurrentCall"] != null)
                    {
                        if (ds.Tables[dsTable].Rows[i]["MaxConcurrentCall"].ToString() != "")
                        {
                            int.TryParse(ds.Tables[dsTable].Rows[i]["MaxConcurrentCall"].ToString(), out OrgMaxConCrntCall);
                        }
                    }
                }
                catch (Exception e)
                {
                    logger.Exception(100, e.Message);
                }
            }
            return true;
        }
        #endregion
        #region FecthScheduledCalls
        /// <summary>
        /// FetchHardwareAdminEmail
        /// </summary>
        /// <param name="OrgId"></param>
        /// <param name="hadrwareAdminEmail"></param>
        /// <returns></returns>
        public bool FecthScheduledCalls(string startDate, string endDate, int editConfId, int orgId, ref List<int>iViewConfs, ref int totalConfCnt)
        {

            DataSet ds = new DataSet();
            string dsTable = "Organization", query = "";
            StringBuilder stmt = new StringBuilder();
            int ConfGUID = 0, scheduledConfcnt=0;

            stmt.Append(" Select externalname, GUID, confdate from Conf_Conference_D ");
            stmt.Append(" where dateAdd(second,-45,confdate) < '" + endDate + "' and dateAdd(second,-45,dateAdd(minute, duration, confdate)) > '" + startDate + "' ");
            stmt.Append(" and  Deleted = 0 and OrgId = " + orgId + " ");
            if (editConfId > 0)
                stmt.Append(" and confid != " + editConfId + "");
            
            query = stmt.ToString();

            bool ret = SelectCommand(query, ref ds, dsTable);
            if (!ret)
            {
                logger.Exception(10, "SQL error");
                return (false);
            }
            for (int i = 0; i < ds.Tables[dsTable].Rows.Count; i++)
            {
                try
                {
                    if (ds.Tables[dsTable].Rows[i]["GUID"] != null)
                    {
                        if (ds.Tables[dsTable].Rows[i]["GUID"].ToString() != "")
                        {
                            int.TryParse(ds.Tables[dsTable].Rows[i]["GUID"].ToString(), out ConfGUID);
                            if (ConfGUID > 0)
                            {
                                if (!iViewConfs.Contains(ConfGUID))
                                    iViewConfs.Add(ConfGUID);
                            }
                            else // Scheduled in myVRM but not in iView MCU
                                scheduledConfcnt = scheduledConfcnt + 1;
                        }
                    }
                }
                catch (Exception e)
                {
                    logger.Exception(100, e.Message);
                }
            }
            totalConfCnt = iViewConfs.Count + scheduledConfcnt;

            return true;
        }
        #endregion

        #region fetchEditConfGUID
        /// <summary>
        /// FetchHardwareAdminEmail
        /// </summary>
        /// <param name="OrgId"></param>
        /// <param name="hadrwareAdminEmail"></param>
        /// <returns></returns>
        public bool fetchEditConfGUID(int editConfId, ref int editConfGUID)
        {

            DataSet ds = new DataSet();
            string dsTable = "Organization", query = "";
            StringBuilder stmt = new StringBuilder();

            stmt.Append("Select GUID from Conf_Conference_D where confid = " + editConfId + "");

            query = stmt.ToString();

            bool ret = SelectCommand(query, ref ds, dsTable);
            if (!ret)
            {
                logger.Exception(10, "SQL error");
                return (false);
            }
            for (int i = 0; i < ds.Tables[dsTable].Rows.Count; i++)
            {
                try
                {
                    if (ds.Tables[dsTable].Rows[i]["GUID"] != null)
                        if (ds.Tables[dsTable].Rows[i]["GUID"].ToString() != "")
                            int.TryParse(ds.Tables[dsTable].Rows[i]["GUID"].ToString(), out editConfGUID);
                           
                }
                catch (Exception e)
                {
                    logger.Exception(100, e.Message);
                }
            }

            return true;
        }
        #endregion

        //ZD 100518 Ends
       
       //ZD 100694 Starts
       #region FetchWhygoStatus
       /// <summary>
       /// FetchWhygoStatus
       /// </summary>
       /// <param name="FetchWhygoStatus"></param>
       /// <returns></returns>
        internal bool FetchWhygoStatus(ref int FetchWhygoStatus)
        {
            try
            {
                string query = "", dsTable = "SysDetails";
                DataSet ds = new DataSet();

                query = " Select EnablePublicRoomService from Org_Settings_D where OrgId=11 ";

                bool ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Trace("SQL error: Query is " + query);
                    return (false);
                }

                for (int i = 0; i < ds.Tables[dsTable].Rows.Count; i++)
                {
                    int.TryParse(ds.Tables[dsTable].Rows[i]["EnablePublicRoomService"].ToString(), out FetchWhygoStatus);
                }
            }
            catch (Exception ex)
            {
                logger.Trace("FetchSysSettingDetails Error: " + ex.Message);
                return false;

            }
            return true;
        }
        #endregion
       //ZD 100694 Ends


		//ZD 100221 Starts
        #region UpdateConfWebEXMeetingKey
        /// <summary>
        /// UpdateConfWebEXMeetingKey
        /// </summary>
        /// <returns></returns>
        public bool UpdateConfWebEXMeetingKey(string confid, string instanceid,string Recur, string meetingkey,string HostURL,string AttendeeURL)
        {
            try
            {
                string query = "update Conf_Conference_D set WebEXMeetingKey = '" + meetingkey + "', WebEXHostURL='"+HostURL+"'";
                query += " where confid = " + confid.ToString();
                if(Recur!="1")
                    query += " and instanceid = " + instanceid.ToString();

                query += ";update Conf_user_D set WebEXAttendeeURL='" + AttendeeURL + "'";
                query += " where confid = " + confid.ToString();
                if (Recur != "1")
                    query += " and instanceid = " + instanceid.ToString() + ";";

                query += ";delete Conf_SyncMCUAdj_D";
                query += " where confid = " + confid.ToString();
                if (Recur != "1")
                    query += " and instanceid = " + instanceid.ToString() + ";";

                bool ret = NonSelectCommand(query.ToString());
                if (!ret)
                {
                    logger.Exception(100, "Error executing query.");
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                logger.Trace("Error in UpdateConfWebEXMeetingKey:" + ex.Message);
                return false;
            }
        }
        #endregion

        #region FetchRecurrence
        /// <summary>
        /// FetchRecurrence
        /// </summary>
        /// <param name="confuid"></param>
        /// <param name="queue"></param>
        /// <returns></returns>
        public bool FetchRecurrence(int confid, ref Queue queue)
        {
            int temp = 0;
            DataSet ds = new DataSet();
            DataSet confds = null;
            NS_MESSENGER.Recurrence rec = null;
            string dsTable = "Recurrence";

            string query = "select * from conf_recurInfo_d";
            query += " where  confid=" + confid;
          
            bool ret = SelectCommand(query, ref ds, dsTable);
            if (!ret)
            {
                logger.Exception(10, "SQL error");
                return (false);
            }

            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                try
                {
                    rec = new NS_MESSENGER.Recurrence();

                    temp = 0;
                    int.TryParse(ds.Tables[dsTable].Rows[i]["duration"].ToString(), out temp);
                    rec.iDuration = temp;

                    temp = 0;
                    int.TryParse(ds.Tables[dsTable].Rows[i]["recurType"].ToString(), out temp);
                    rec.iRecurType = temp;

                    temp = 0;
                    int.TryParse(ds.Tables[dsTable].Rows[i]["subType"].ToString(), out temp);
                    rec.iSubType = temp;

                    temp = 0;
                    int.TryParse(ds.Tables[dsTable].Rows[i]["yearMonth"].ToString(), out temp);
                    rec.iyearMonth = temp;

                    if (ds.Tables[dsTable].Rows[i]["days"].ToString() != null)
                        rec.sDays = ds.Tables[dsTable].Rows[i]["days"].ToString();

                    rec.dtstartTime = DateTime.Parse(ds.Tables[dsTable].Rows[i]["starttime"].ToString());
                    rec.dtendTime  = DateTime.Parse(ds.Tables[dsTable].Rows[i]["endtime"].ToString());

                    temp = 0;
                    int.TryParse(ds.Tables[dsTable].Rows[i]["endType"].ToString(), out temp);
                    rec.iendType = temp;

                    temp = 0;
                    int.TryParse(ds.Tables[dsTable].Rows[i]["dayNO"].ToString(), out temp);
                    rec.idayno = temp;

                    temp = 0;
                    int.TryParse(ds.Tables[dsTable].Rows[i]["gap"].ToString(), out temp);
                    rec.igap = temp;

                    temp = 0;
                    int.TryParse(ds.Tables[dsTable].Rows[i]["occurrence"].ToString(), out temp);

                    if (temp == 1)
                    {
                        query = "select * from conf_conference_d";
                        query += " where  confid=" + confid;
                        dsTable = "Confernecetable";
                        confds = new DataSet();
                        ret = SelectCommand(query, ref confds, dsTable);
                       
                        if (!ret)
                        {
                            logger.Exception(10, "SQL error");
                            return (false);
                        }
                        if (confds.Tables[dsTable].Rows.Count > 0)
                            temp = confds.Tables[dsTable].Rows.Count;
                    }


                    rec.ioccurrence = temp;

                    queue.Enqueue(rec);

                }
                catch (Exception e)
                {
                    logger.Exception(200, e.Message);
                }
            }

            return true;
        }
        #endregion

        #region FetchWebExInfo
        /// <summary>
        /// FetchWebExInfo
        /// </summary>
        /// <returns></returns>
        public bool FetchWebExInfo(string confid, string instanceid, string Recur, ref string meetingkey,ref string HostURL,ref string AttendeeURL)
        {
            try
            {
                string query = "", dsTable = "SysDetails";
                DataSet ds = new DataSet();

                query = "Select WebExMeetingKey,WebExHostURL,WebEXAttendeeURL from Conf_SyncMCUAdj_D where confid =" + confid.ToString();
                if (Recur != "1")
                    query += " and instanceid = " + instanceid.ToString();

                bool ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Trace("SQL error: Query is " + query);
                    return (false);
                }

                for (int i = 0; i < ds.Tables[dsTable].Rows.Count; i++)
                {
                    meetingkey = ds.Tables[dsTable].Rows[i]["WebExMeetingKey"].ToString();
                    HostURL = ds.Tables[dsTable].Rows[i]["WebExHostURL"].ToString();
                    AttendeeURL = ds.Tables[dsTable].Rows[i]["WebEXAttendeeURL"].ToString();
                }

                return true;
            }
            catch (Exception ex)
            {
                logger.Trace("Error in FetchWebExInfo:" + ex.Message);
                return false;
            }
        }
        #endregion
        //ZD 100221 Ends

        //ZD 100890 Start

        #region FetchPartyInfo
        /// <summary>
        /// FetchPartyInfo
        /// </summary>
        /// <param name="Partyid"></param>
        /// <param name="Party"></param>
        /// <returns></returns>
        internal bool FetchPartyInfo(int Partyid, ref NS_MESSENGER.Party Party)
        {
            string IsStaticIDEnabled = "";
            try
            {
                // Retreive  the  bridge record
                DataSet ds = new DataSet();
                string dsTable = "User";
                string sQuery = "select * from Usr_List_D where UserID = " + Partyid;
                bool ret = SelectCommand(sQuery, ref ds, dsTable);
                if (!ret)
                {
                    logger.Exception(100, "SQL error");
                    return (false);
                }

                if (ds.Tables[dsTable].Rows.Count < 1)
                {
                    logger.Trace("No User found...");
                    return false;
                }

                if (ds.Tables[dsTable].Rows[0]["UserID"] != null)
                    int.TryParse(ds.Tables[dsTable].Rows[0]["UserID"].ToString(), out Party.iDbId);

                if (ds.Tables[dsTable].Rows[0]["FirstName"] != null)
                    Party.sFirstName = ds.Tables[dsTable].Rows[0]["FirstName"].ToString();

                if (ds.Tables[dsTable].Rows[0]["LastName"] != null)
                    Party.sLastName = ds.Tables[dsTable].Rows[0]["LastName"].ToString();

                if (ds.Tables[dsTable].Rows[0]["Password"] != null)
                {
                    Party.sPwd = ds.Tables[dsTable].Rows[0]["Password"].ToString();
                    if (Party.sPwd != null && Party.sPwd.Length > 0)
                    {
                        cryptography.Crypto crypto = new Crypto();
                        Party.sPwd = crypto.decrypt(Party.sPwd);
                        var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(Party.sPwd);
                        Party.sPwd = System.Convert.ToBase64String(plainTextBytes);
                    }
                }

                if (ds.Tables[dsTable].Rows[0]["StaticID"] != null)
                    Party.sStaticID = ds.Tables[dsTable].Rows[0]["StaticID"].ToString();

                if (ds.Tables[dsTable].Rows[0]["Email"] != null)
                    Party.Emailaddress = ds.Tables[dsTable].Rows[0]["Email"].ToString();

                if (ds.Tables[dsTable].Rows[0]["WorkPhone"] != null)
                    Party.sWorkPhone = ds.Tables[dsTable].Rows[0]["WorkPhone"].ToString();

                if (ds.Tables[dsTable].Rows[0]["CellPhone"] != null)
                    Party.sCellPhone = ds.Tables[dsTable].Rows[0]["CellPhone"].ToString();

                if (ds.Tables[dsTable].Rows[0]["ExternalUserId"] != null)
                    Party.siViewUserID = ds.Tables[dsTable].Rows[0]["ExternalUserId"].ToString().Trim();

                if (ds.Tables[dsTable].Rows[0]["IsStaticIDEnabled"] != null)
                    IsStaticIDEnabled = ds.Tables[dsTable].Rows[0]["IsStaticIDEnabled"].ToString().Trim();

                if (IsStaticIDEnabled == "1")
                    Party.IsStaticIDEnabled = true;
                else
                    Party.IsStaticIDEnabled = false;

                if (ds.Tables[dsTable].Rows[0]["VirtualRoomId"] != null)
                    Party.sVirtualRoomId = ds.Tables[dsTable].Rows[0]["VirtualRoomId"].ToString().Trim();

                return (true);
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion

        #region UpdateParticipantExternalID
        /// <summary>
        /// UpdateParticipantExternalID
        /// </summary>
        /// <param name="Party"></param>
        /// <returns></returns>
        internal bool UpdateParticipantExternalID(NS_MESSENGER.Party Party)
        {
            string query = "";
            try
            {
                if(Party.siViewUserID != "")
                    query = " Update Usr_List_D set ExternalUserId = '" + Party.siViewUserID + "' where UserID = " + Party.iDbId;

                if (Party.sVirtualRoomId != "")
                    query += " Update Usr_List_D set VirtualRoomId = '" + Party.sVirtualRoomId + "' where UserID = " + Party.iDbId; //ZD 100969

                if (Party.sStaticID != "")
                    query += " Update Usr_List_D set StaticID = '" + Party.sStaticID + "' where UserID = " + Party.iDbId; //ZD 100969

                bool ret = NonSelectCommand(query);
                if (!ret)
                {
                    logger.Exception(100, "Error executing query." + query);
                    return false;
                }
                return (true);
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion

        #region DeleteParticipantExternalID
        /// <summary>
        /// DeleteParticipantExternalID
        /// </summary>
        /// <param name="Party"></param>
        /// <returns></returns>
        internal bool DeleteParticipantExternalID(string iViewUserID, string VirtualRoomId)
        {
            string query = "";
            try
            {
                if (iViewUserID != "")
                    query = " Update Usr_Inactive_D set ExternalUserId = '' where ExternalUserId = '" + iViewUserID + "'";

                if (VirtualRoomId != "")
                    query += " Update Usr_Inactive_D set VirtualRoomId = '' where UserID = '" + VirtualRoomId + "'";

                bool ret = NonSelectCommand(query);
                if (!ret)
                {
                    logger.Exception(100, "Error executing query." + query);
                    return false;
                }
                return (true);
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion

        //ZD 100890 End

        //ZD 100522 Start
        #region FetchVMRRoom
        /// <summary>
        /// FetchVMRRoom
        /// </summary>
        /// <param name="roomid"></param>
        /// <param name="Confid"></param>
        /// <returns></returns>
        internal bool FetchVMRRoom(int roomid, ref string Confid)
        {
            try
            {
                DataSet ds = new DataSet();
                string dsTable = "Room";
                string sQuery = "select PermanentConfId from Loc_Room_D where RoomCategory = 2 and IsCreateOnMCU = 1 and RoomID = " + roomid;
                bool ret = SelectCommand(sQuery, ref ds, dsTable);
                if (!ret)
                {
                    logger.Exception(100, "SQL error");
                    return false;
                }

                if (ds.Tables[dsTable].Rows.Count < 1)
                {
                    logger.Trace("No Room found...");
                    return false;
                }

                if (ds.Tables[dsTable].Rows[0]["PermanentConfId"] != null)
                    Confid = ds.Tables[dsTable].Rows[0]["PermanentConfId"].ToString();

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion

        #region UpdateVMRRoomDetails
        /// <summary>
        /// UpdateVMRRoomDetails
        /// </summary>
        /// <param name="Party"></param>
        /// <returns></returns>
        internal bool UpdateVMRRoomDetails(int roomId, string GUID, string PermanentconfName, string VMRID, string confid, string intnumber, string extnumber)
        {
            string query = "";
            try
            {
                query = " Update Loc_Room_D set PermanentConfId = '" + confid + "' , PermanentconfName = '" + PermanentconfName + "'  , RoomVMRID = '" + VMRID + "' ,VMRConfId = '" + GUID + "', InternalNumber = '" + intnumber + "', ExternalNumber = '" + extnumber + "'  where RoomID = " + roomId;

                bool ret = NonSelectCommand(query);
                if (!ret)
                {
                    logger.Exception(100, "Error executing query." + query);
                    return false;
                }
                return (true);
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion
        //ZD 100522 End


        //ZD 101348 Starts
        internal bool UpdateConferenceDuration(int confid, int instanceId, int duration)
        {
            try
            {
                string query = "Update Conf_conference_D set duration = " + duration + " where Confid = " + confid + " and instanceId = " + instanceId + "";
                bool ret = NonSelectCommand(query);
                if (!ret)
                {
                    logger.Trace("Failed to UpdateConferenceDuration: " + query);
                }		

            }
            catch (Exception ex)
            {
                
            }
            return true;

        }
        //ZD 101348 End

        //ZD 101217 Starts
        internal bool UpdateSystemLocation(int mcuDbId, List<NS_MESSENGER.SystemLocation> SystemLocationlist)
        {
            string query = "";
            bool ret = false;
            NS_MESSENGER.SystemLocation SystemLocation = new NS_MESSENGER.SystemLocation();
            try
            {
                query = " delete from Gen_SystemLocation_D where MCUId = " + mcuDbId.ToString();
                ret = NonSelectCommand(query);
                if (!ret)
                {
                    logger.Exception(10, "Error executing query.");
                    return false;
                }
                for (int i = 0; i < SystemLocationlist.Count; i++)
                {
                    SystemLocation = SystemLocationlist[i];
                    query = " insert into Gen_SystemLocation_D (MCUId,SysLocId,Name) values (" + mcuDbId.ToString() + ",'" + SystemLocation.id + "','" + SystemLocation.name + "')";
                    ret = NonSelectCommand(query);
                    if (!ret)
                    {
                        logger.Exception(10, "Error executing query.");
                        return false;
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        //ZD 101217 End

        //ZD 101522 Start

        #region FetchSystemLocation
        /// <summary>
        /// FetchSystemLocation
        /// </summary>
        /// <param name="roomid"></param>
        /// <param name="Confid"></param>
        /// <returns></returns>
        internal bool FetchSystemLocation(int mcuID, int LocationID, ref string LocationName)
        {
            try
            {
                if (LocationID > 0)
                {
                    DataSet ds = new DataSet();
                    string dsTable = "sysLoc";
                    string sQuery = "Select Name from Gen_SystemLocation_D where SysLocId = " + LocationID.ToString() + " and MCUId = " + mcuID;
                    bool ret = SelectCommand(sQuery, ref ds, dsTable);
                    if (!ret)
                    {
                        logger.Exception(100, "SQL error");
                        return false;
                    }

                    if (ds.Tables[dsTable].Rows.Count < 1)
                    {
                        logger.Trace("No Room found...");
                        return false;
                    }

                    if (ds.Tables[dsTable].Rows[0]["Name"] != null)
                        LocationName = ds.Tables[dsTable].Rows[0]["Name"].ToString();
                }
                else
                    LocationName = "";

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion

        //ZD 101522 End

        //ZD 101363
        public bool FetchRoomPassword(int eptid,int profid, ref string pwd)
        {
            try
            {
                if (eptid == 0 || profid == 0)
                    pwd = "";
              
                DataSet ds = new DataSet();
                string dsTable = "Room";

                // find the endpoint and then the first room associated with it.
                string query = "select password from Ept_List_D where endpointId = " + eptid + " and profileId = " + profid + "";
               
                logger.Trace(query);

                bool ret = false;
                ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Exception(100, "SQL error");
                    return (false);
                }

                if (ds.Tables[dsTable].Rows.Count > 0)
                {
                    pwd = ds.Tables[dsTable].Rows[0][0].ToString();
                    return true;
                }
                else
                    return false;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
		//ZD 101522 End
        
        //ZD 101527 Starts
        #region SaveRPRMEndpointList
        /// <summary>
        /// SaveUpdateRPRMEndpoints
        /// </summary>
        /// <param name="room"></param>
        /// <param name="errMsg"></param>
        /// <param name="myVRMLocationID"></param>
        /// <returns></returns>
        internal bool SaveRPRMEndpointList(List<NS_MESSENGER.RPRMEndpoints> ept, int orgID, int userID)
        {
            bool isPendingEndpoints = false, isPendingRooms = false;
            string emailBody = "", emailSubject = "";
            NS_MESSENGER.Email email = new NS_MESSENGER.Email();
            bool ret = true; List<int> pendingorgID = new List<int>();
            List<string> emails = new List<string>();
            List<string> Name = new List<string>();
            string logoId = "", logoName = "", attchmnt = "", techEmail = "";
            try
            {
                for (int profilecnt = 0; profilecnt < ept.Count; profilecnt++)
                {
                    if (!SaveUpdateRPRMEndpoints(ept[profilecnt], ept[profilecnt].orgID, ref isPendingEndpoints, ref isPendingRooms, ref pendingorgID))
                        return false;
                }


                //EMails to Site/Org Admins

                for (int cnt = 0; cnt < pendingorgID.Count(); cnt++)
                {
                    FetachSiteOrgAdmin(pendingorgID[cnt], ref emails, ref Name);

                    for (int i = 0; i < emails.Count; i++)
                    {
                        emailBody = "";
                        FetchEmailString(pendingorgID[cnt], ref emailBody, ref emailSubject, 71);
                        if (CheckMailLogo(pendingorgID[cnt]))
                        {
                            logoId = "LogoImg";
                            logoName = "Org_" + pendingorgID[cnt] + "mail_logo.gif";
                            attchmnt = "<table width=\"800px\"><tr><td align=\"center\" ><img id=\"" + logoId + "\" src=\"" + logoName + "\" alt /></td></tr></table>";
                        }
                        emailBody = emailBody.Replace("{1}", attchmnt);

                        email.To = emails[i];
                        email.isHTML = true;
                        email.Subject = emailSubject;
                        FetchTechEmail(pendingorgID[cnt], ref techEmail);

                        email.From = techEmail;
                        FetchTechInfo(pendingorgID[cnt], ref emailBody); //FB 3069
                        email.Body = emailBody;
                        //email.Body = emailBody.Replace("{2}", Name[cnt]);
                        ret = InsertEmailInQueue(email);
                        if (!ret)
                        {
                            logger.Trace("Pending conference deletion notification email not sent out.");
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                logger.Trace("SaveRPRMEndpointList :" + ex.StackTrace);
                errMsg = ex.Message;
                return false;
            }
            return true;
        }
        #endregion

        #region SaveUpdateRPRMEndpoints
        /// <summary>
        /// SaveUpdateRPRMEndpoints
        /// </summary>
        /// <param name="room"></param>
        /// <param name="errMsg"></param>
        /// <param name="myVRMLocationID"></param>
        /// <returns></returns>
        internal bool SaveUpdateRPRMEndpoints(NS_MESSENGER.RPRMEndpoints ept, int orgID, ref bool isPendingEndpoints, ref bool isPendingRooms, ref List<int> pendingorgID)
        {
            string query = "";
            bool ret = false, isSaveRoom = false;
            int iEptID = -1, iProfileID = 1, iIsDefault = 1, eProtocol = 1, addressType = 1, userID = 11, catogry = 0;
            DataSet ds = new DataSet(), dsept = new DataSet();
            string dsTable = "Ept_List_d", dsTable2 = "Loc_Room_D";
            int EptCount = 0, orgEptLicenseCnt = 0, ManufacturerID = 0, LocCnt = 0, SyncLocCnt = 0;
            NS_MESSENGER.SyncRoom SyncEptRoom = new NS_MESSENGER.SyncRoom();
            int EPTID = -1, IsSyncEP = -1;
            DateTime Lastmodifieddate = DateTime.Now;
            string tablename = "Ept_List_D", EPProfileName = ""; //ZD 102200
            List<int> eptRooms = new List<int>();
            try
            {
                iEptID = -1;
                //Check whether Endpoint is already impotred
                dsTable = "1Ept_Sync_D";

                query = " Select endpointId,Lastmodifieddate from Ept_Sync_D where  identifierValue=" + ept.identifier;
                ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Exception(10, "SQL error");
                    return false;
                }
                if (ds.Tables[dsTable].Rows.Count > 0)
                {
                    IsSyncEP = 0;
                    int.TryParse(ds.Tables[dsTable].Rows[0][0].ToString(), out EPTID);
                    Lastmodifieddate = Convert.ToDateTime(ds.Tables[dsTable].Rows[0][1].ToString());
                }

                if (EPTID <= 0)
                {
                   
                    dsTable = "2Ept_List_D";
                    query = " Select endpointId,Lastmodifieddate from Ept_List_D where  isDefault =1 and deleted = 0  and identifierValue=" + ept.identifier;

                    ret = SelectCommand(query, ref ds, dsTable);
                    if (!ret)
                    {
                        logger.Exception(10, "SQL error");
                        return false;
                    }
                    if (ds.Tables[dsTable].Rows.Count > 0)
                    {
                        IsSyncEP = 1;
                        int.TryParse(ds.Tables[dsTable].Rows[0][0].ToString(), out EPTID);
                        Lastmodifieddate = Convert.ToDateTime(ds.Tables[dsTable].Rows[0][1].ToString());
                    }
                }


                if (EPTID > 0) //Update Ept Details
                {
                    if (IsSyncEP == 0)
                        tablename = "EPt_Sync_D";
                    //ZD 102200 Starts
                    EPProfileName = ept.EndpointName;
                    if (string.IsNullOrEmpty(ept.EndpointName))
                        EPProfileName = "RPRM " + ept.address;
                    //ZD 102200 Ends
                    iProfileID = 1;
                    iIsDefault = 0;
                    query = "Update " + tablename + " set name = '" + EPProfileName + "',protocol ='1',addresstype='1',address='" + ept.address + "' ,profileName='" + EPProfileName + "',Lastmodifieddate='" + DateTime.UtcNow + "' where profileId = " + iProfileID + " and identifierValue = " + ept.identifier;
                    ret = NonSelectCommand(query);
                    if (!ret)
                    {
                        logger.Exception(10, "SQL error");
                        return false;
                    }
                    if (ept.EptProfile != null)
                    {
                        for (int prfct = 0; prfct < ept.EptProfile.Count; prfct++)
                        {
                            iIsDefault = 0;
                            eProtocol = 1; addressType = ept.EptProfile[prfct].aliastype;  iProfileID++;
                            if (ept.EptProfile[prfct].aliastype == 4)
                            {
                                eProtocol = 4;
                                addressType = 4;
                            }
                            else if (ept.EptProfile[prfct].aliastype == 3)
                            {
                                eProtocol = 4;
                                addressType = 4;
                            }
                            else if (ept.EptProfile[prfct].aliastype == 6)
                            {

                                eProtocol = 1;
                                addressType = 6;

                                //if (ept.ModelType.ToLower() == "rp-mobile" || ept.ModelType.ToLower() == "rp-desktop")
                                //{
                                //    //iIsDefault = 1;
                                //    query = "Update Ept_Sync_D set isDefault = 0 where deleted = 0 and isDefault = 1 and identifierValue = " + ept.identifier;

                                //    ret = NonSelectCommand(query);
                                //    if (!ret)
                                //    {
                                //        logger.Exception(10, "SQL error");
                                //        return false;
                                //    }
                                //}

                            }
                            dsTable = "2Ept_List_D" + prfct;
                            query = " Select endpointId,profileId from Ept_List_D where deleted = 0  and identifierValue=" + ept.identifier + " and profileId = " + iProfileID;

                            ret = SelectCommand(query, ref ds, dsTable);
                            if (!ret)
                            {
                                logger.Exception(10, "SQL error");
                                return false;
                            }
                            if (ds.Tables[dsTable].Rows.Count > 0)
                                query = "Update " + tablename + " set name = '" + ept.EndpointName + "',protocol ='" + eProtocol + "',addresstype='" + addressType + "',address='" + ept.EptProfile[prfct].address + "' ,profileName='" + ept.EndpointProfileName + "',Lastmodifieddate='" + DateTime.UtcNow + "' where profileId = " + iProfileID + " and identifierValue = " + ept.identifier;
                            else
                            {
                                //int.TryParse(ds.Tables[dsTable].Rows[0][0].ToString(), out EPTID);
                                IdentifyModelType(ref ManufacturerID, ept.ModelType);
                                query = " INSERT INTO [Ept_List_D]([endpointId],[name],[password],[protocol],[connectiontype],[addresstype],[address],[deleted],[outsidenetwork],[videoequipmentid]";
                                query += ",[linerateid],[bridgeid],[endptURL],[profileId],[profileName],[isDefault],[encrypted],[MCUAddress],[MCUAddressType],[TelnetAPI],[orgId],[ExchangeID],[CalendarInvite]";
                                query += ",[ApiPortNo],[ConferenceCode],[LeaderPin],[isTelePresence],[MultiCodecAddress],[RearSecCameraAddress],[Extendpoint],[EptOnlineStatus],[PublicEndPoint],[NetworkURL]";
                                query += ",[Secureport],[Secured],[EptCurrentStatus],[GateKeeeperAddress],[UserName],[ManufacturerID],[LastModifiedUser],[Lastmodifieddate],[ProfileType],[IsP2PDefault]";
                                query += ",[IsTestEquipment],[CreateCategory],[identifierValue],[SSHSupport])";
                                query += "    VALUES";
                                query += "(" + EPTID + " ,'" + ept.EndpointName + "' ,''," + eProtocol + " ,2 ," + addressType + " ,'" + ept.EptProfile[prfct].address + "' ,0,0," + ManufacturerID;
                                query += ",768," + ept.mcuID + ",''," + iProfileID + ",'" + ept.EndpointProfileName + "'," + iIsDefault + ",0,'',0,0," + ept.orgID + ",'',0";
                                query += ",23,'','',0,'','',0,0,0,''";
                                query += ",0,0,0,'','',3, " + userID + ",'" + DateTime.UtcNow + "' ,1,0,0,2," + ept.identifier + ",0)";

                            }
                            ret = NonSelectCommand(query);
                            if (!ret)
                            {
                                logger.Exception(10, "SQL error");
                                return false;
                            }

                        }
                    }
                    // Check in Location Table for the endpoints
                    dsTable = "Ept_List_d5";
                    query = "Select  Count(*) from Loc_Room_D where CreateType =2 and orgId =" + orgID + " and endpointid = " + EPTID;

                    ret = SelectCommand(query, ref ds, dsTable);
                    if (!ret)
                    {
                        logger.Exception(10, "SQL error");
                        return false;
                    }
                    if (ds.Tables[dsTable].Rows.Count > 0)
                    {
                        int.TryParse(ds.Tables[dsTable].Rows[0][0].ToString(), out LocCnt);
                    }

                    dsTable = "Ept_List_d6";
                    query = "Select  Count(*) from Loc_Sync_D where orgId =" + orgID + " and identifierValue = " + ept.identifier;

                    ret = SelectCommand(query, ref ds, dsTable);
                    if (!ret)
                    {
                        logger.Exception(10, "SQL error");
                        return false;
                    }
                    if (ds.Tables[dsTable].Rows.Count > 0)
                    {
                        int.TryParse(ds.Tables[dsTable].Rows[0][0].ToString(), out SyncLocCnt);
                    }
                    if (SyncLocCnt == 0 && LocCnt == 0)
                    {
                        SyncEptRoom = new NS_MESSENGER.SyncRoom();
                        SyncEptRoom.EndpointId = EPTID;
                        SyncEptRoom.RoomName = ept.EndpointName;
                        SyncEptRoom.EndpointName = ept.EndpointName;
                        SyncEptRoom.OrgId = ept.orgID;
                        if (IsSyncEP == 0)
                        {
                            SyncEptRoom.EndpointCategory = 0; // Endpoint in Sync Table
                            catogry = 2;
                        }
                        else
                        {
                            SyncEptRoom.EndpointCategory = 1; // Endpoint in Ept_List Table
                            catogry = 1;
                        }
                        SyncEptRoom.identifierValue = ept.identifier;
                        SaveUpdateRPRMRoom(SyncEptRoom, ref isPendingRooms, catogry, ref pendingorgID);
                    }
                }
                else  //Insert
                {
                    //Endpoint Used Count
                    dsTable = "Ept_List_d";
                    query = "Select  COUNT(distinct(endpointId)) from Ept_List_D where ( isDefault = 1 or IsP2PDefault =1) and deleted = 0 and Extendpoint =0 and PublicEndPoint =0 and orgId =" + orgID;

                    ret = SelectCommand(query, ref ds, dsTable);
                    if (!ret)
                    {
                        logger.Exception(10, "SQL error");
                        return false;
                    }
                    if (ds.Tables[dsTable].Rows.Count > 0)
                    {
                        int.TryParse(ds.Tables[dsTable].Rows[0][0].ToString(), out EptCount);
                    }

                    //Org Endpoint License
                    dsTable = "Org_settings_d";
                    query = " Select MaxEndpoints from Org_settings_d where OrgId = '" + orgID + "' ";
                    ret = SelectCommand(query, ref ds, dsTable);
                    if (!ret)
                    {
                        logger.Exception(10, "SQL error");
                        return false;
                    }
                    if (ds.Tables[dsTable].Rows.Count > 0)
                    {
                        int.TryParse(ds.Tables[dsTable].Rows[0][0].ToString(), out orgEptLicenseCnt);
                    }

                    //License Check
                    isPendingEndpoints = false;
                    if (EptCount >= orgEptLicenseCnt)
                        isPendingEndpoints = true;

                    //Insert in Database
                    iIsDefault = 1; iProfileID = 1; eProtocol = 1;
                    if (isPendingEndpoints)
                    {
                        //if (!CheckAddressAvailabllity(ept.address, ept.orgID, 1))
                        //    return false;
                        if (!pendingorgID.Contains(ept.orgID))
                            pendingorgID.Add(ept.orgID);

                        if (iEptID <= 0)
                            iEptID = GetSyncMaxEndpointID();

                        if (iEptID <= 0)
                        {
                            logger.Trace("SaveUpdatePendingEndpoints : Endpoint ID is empty");
                            return false;
                        }
                        //Inserting Default Profile, Always IP
                        IdentifyModelType(ref ManufacturerID, ept.ModelType);
                        query = " INSERT INTO Ept_Sync_D ([endpointId],[name],[protocol],[addresstype],[address],[profileId],[profileName],[isDefault],[videoequipmentid],[orgId],[Lastmodifieddate],[identifierValue],[mcuID],[ManufacturerID])";
                        query += "VALUES";
                        query += "( " + iEptID + ",'" + ept.EndpointName + "' ," + eProtocol + "," + addressType + ",'" + ept.address + "'," + iProfileID + ",'" + ept.EndpointProfileName + "'," + iIsDefault + ",0," + ept.orgID + ",'" + DateTime.UtcNow + "'," + ept.identifier + "," + ept.mcuID + "," + ManufacturerID + ")";

                        ret = NonSelectCommand(query);
                        if (!ret)
                        {
                            logger.Exception(100, "query failed. query =" + query);
                            return false;
                        }

                        SyncEptRoom = new NS_MESSENGER.SyncRoom();
                        SyncEptRoom.EndpointId = iEptID;
                        SyncEptRoom.RoomName = ept.EndpointName;
                        SyncEptRoom.EndpointName = ept.EndpointName;
                        SyncEptRoom.OrgId = ept.orgID;
                        SyncEptRoom.EndpointCategory = 1; // Endpoint in Sync Table
                        SyncEptRoom.identifierValue = ept.identifier;
                        //Inserting other Profiles
                        if (ept.EptProfile != null)
                        {
                            for (int pfct = 0; pfct < ept.EptProfile.Count; pfct++)
                            {
                                iProfileID++;
                                iIsDefault = 0;
                                if (ept.EptProfile[pfct].aliastype == 4)
                                {
                                    eProtocol = 4;
                                    addressType = 4;
                                }
                                else if (ept.EptProfile[pfct].aliastype == 3)
                                {
                                    eProtocol = 4;
                                    addressType = 4;
                                }
                                else if (ept.EptProfile[pfct].aliastype == 2)
                                {
                                    eProtocol = 1;
                                    addressType = 2;
                                }
                                else if (ept.EptProfile[pfct].aliastype == 6)
                                {
                                    
                                    eProtocol = 1;
                                    addressType = 6;

                                    if (ept.ModelType.ToLower() == "rp-mobile" || ept.ModelType.ToLower() == "rp-desktop")
                                    {
                                        iIsDefault = 1;
                                        query = "Update Ept_Sync_D set isDefault = 0 where deleted = 0 and isDefault = 1 and identifierValue = " + ept.identifier;

                                        ret = NonSelectCommand(query);
                                        if (!ret)
                                        {
                                            logger.Exception(10, "SQL error");
                                            return false;
                                        }
                                    }
                                   
                                }

                                query = " INSERT INTO Ept_Sync_D ([endpointId],[name],[protocol],[addresstype],[address],[profileId],[profileName],[isDefault],[videoequipmentid],[orgId],[Lastmodifieddate],[identifierValue],[mcuID],[ManufacturerID])";
                                query += "VALUES";
                                query += "( " + iEptID + ",'" + ept.EndpointName + "' ," + eProtocol + "," + addressType + ",'" + ept.EptProfile[pfct].address + "'," + iProfileID + ",'" + ept.EndpointProfileName + "'," + iIsDefault + ",0," + ept.orgID + ",'" + DateTime.UtcNow + "'," + ept.identifier + "," + ept.mcuID + "," + ManufacturerID + ")";

                                ret = false;
                                ret = NonSelectCommand(query);
                                if (!ret)
                                {
                                    logger.Exception(100, "query failed. query =" + query);
                                }
                            }
                        }

                        // Check in Location Table for the endpoints
                        eptRooms = new List<int>();
                        dsTable = "2Ept_List_D";
                        query = " Select endpointId,Lastmodifieddate from Ept_List_D where  isDefault =1 and identifierValue=" + ept.identifier;

                        ret = SelectCommand(query, ref ds, dsTable);
                        if (!ret)
                        {
                            logger.Exception(10, "SQL error");
                            return false;
                        }
                        if (ds.Tables[dsTable].Rows.Count > 0)
                        {
                            for (int eptcnt = 0; eptcnt < ds.Tables[dsTable].Rows.Count; eptcnt++)
                            {
                                dsTable2 = "Ept_List_d5";
                                query = "Select  Count(*) from Loc_Room_D where CreateType =2 and orgId =" + orgID + " and endpointid = " + ds.Tables[dsTable].Rows[eptcnt][0];
                                ret = SelectCommand(query, ref dsept, dsTable2);
                                if (!ret)
                                {
                                    logger.Exception(10, "SQL error");
                                    return false;
                                }
                                if (dsept.Tables[dsTable2].Rows.Count > 0)
                                {
                                    int.TryParse(dsept.Tables[dsTable2].Rows[0][0].ToString(), out SyncLocCnt);
                                    if (SyncLocCnt != 0)
                                    {
                                        if (!eptRooms.Contains(SyncLocCnt))
                                            eptRooms.Add(SyncLocCnt);
                                    }

                                }
                            }
                        }

                        dsTable = "Ept_List_d6";
                        query = "Select  Count(*) from Loc_Sync_D where orgId =" + orgID + " and identifierValue = " + ept.identifier;

                        ret = SelectCommand(query, ref ds, dsTable);
                        if (!ret)
                        {
                            logger.Exception(10, "SQL error");
                            return false;
                        }
                        if (ds.Tables[dsTable].Rows.Count > 0)
                        {
                            int.TryParse(ds.Tables[dsTable].Rows[0][0].ToString(), out SyncLocCnt);
                        }
                        if (eptRooms.Count == 0 && LocCnt == 0)
                            SaveUpdateRPRMRoom(SyncEptRoom, ref isPendingRooms, 2, ref pendingorgID); // Endpoit save in Ept_Sync_D table
                    }
                    else
                    {

                        //if (!CheckAddressAvailabllity(ept.address, ept.orgID, 0))
                        //    return false;

                        if (iEptID <= 0)
                            iEptID = GetMaxEndpointID();

                        if (iEptID <= 0)
                        {
                            logger.Trace("SaveUpdatePendingEndpoints : Endpoint ID is empty");
                            return false;
                        }
                        EPProfileName = ept.EndpointName;
                        if (string.IsNullOrEmpty(ept.EndpointName))
                            EPProfileName = "RPRM " + ept.address;
                        //Inserting Default Profile, Always IP
                        IdentifyModelType(ref ManufacturerID, ept.ModelType);
                        query = " INSERT INTO [Ept_List_D]([endpointId],[name],[password],[protocol],[connectiontype],[addresstype],[address],[deleted],[outsidenetwork],[videoequipmentid]";
                        query += ",[linerateid],[bridgeid],[endptURL],[profileId],[profileName],[isDefault],[encrypted],[MCUAddress],[MCUAddressType],[TelnetAPI],[orgId],[ExchangeID],[CalendarInvite]";
                        query += ",[ApiPortNo],[ConferenceCode],[LeaderPin],[isTelePresence],[MultiCodecAddress],[RearSecCameraAddress],[Extendpoint],[EptOnlineStatus],[PublicEndPoint],[NetworkURL]";
                        query += ",[Secureport],[Secured],[EptCurrentStatus],[GateKeeeperAddress],[UserName],[ManufacturerID],[LastModifiedUser],[Lastmodifieddate],[ProfileType],[IsP2PDefault]";
                        query += ",[IsTestEquipment],[CreateCategory],[identifierValue],[SSHSupport])";
                        query += "    VALUES";
                        query += "(" + iEptID + " ,'" + EPProfileName + "' ,''," + eProtocol + " ,2 ," + addressType + " ,'" + ept.address + "' ,0,0," + ManufacturerID;
                        query += ",768," + ept.mcuID + ",''," + iProfileID + ",'" + EPProfileName + "'," + iIsDefault + ",0,'',0,0," + ept.orgID + ",'',0";
                        query += ",23,'','',0,'','',0,0,0,''";
                        query += ",0,0,0,'','',3, " + userID + ",'" + DateTime.UtcNow + "' ,1,0,0,2," + ept.identifier + ",0)";

                        ret = NonSelectCommand(query);
                        if (!ret)
                        {
                            logger.Exception(100, "query failed. query =" + query);
                            return false;

                        }


                        SyncEptRoom = new NS_MESSENGER.SyncRoom();
                        SyncEptRoom.EndpointId = iEptID;
                        SyncEptRoom.RoomName = ept.EndpointName;
                        SyncEptRoom.EndpointName = ept.EndpointName;
                        SyncEptRoom.OrgId = ept.orgID;
                        SyncEptRoom.EndpointCategory = 0; // Endpoint in Sync Table
                        SyncEptRoom.identifierValue = ept.identifier;
                        //Inserting other Profiles
                        if (ept.EptProfile != null)
                        {
                            for (int pfct = 0; pfct < ept.EptProfile.Count; pfct++)
                            {
                                iProfileID++;
                                iIsDefault = 0;
                                if (ept.EptProfile[pfct].aliastype == 4)
                                {
                                    eProtocol = 2;
                                    addressType = 4;
                                }
                                else if (ept.EptProfile[pfct].aliastype == 2)
                                {
                                    eProtocol = 1;
                                    addressType = 2;
                                }
                                else if (ept.EptProfile[pfct].aliastype == 6)
                                {
                                    eProtocol = 1;
                                    addressType = 6;
                                    
                                    if (ept.ModelType.ToLower() == "rp-mobile" || ept.ModelType.ToLower() == "rp-desktop")
                                    {
                                        iIsDefault = 1;
                                        query = "Update Ept_List_D set isDefault = 0 where deleted = 0 and isDefault = 1 and identifierValue = " + ept.identifier;

                                        ret = NonSelectCommand(query);
                                        if (!ret)
                                        {
                                            logger.Exception(10, "SQL error");
                                            return false;
                                        }
                                    }

                                }

                                query = " INSERT INTO [Ept_List_D]([endpointId],[name],[password],[protocol],[connectiontype],[addresstype],[address],[deleted],[outsidenetwork],[videoequipmentid]";
                                query += ",[linerateid],[bridgeid],[endptURL],[profileId],[profileName],[isDefault],[encrypted],[MCUAddress],[MCUAddressType],[TelnetAPI],[orgId],[ExchangeID],[CalendarInvite]";
                                query += ",[ApiPortNo],[ConferenceCode],[LeaderPin],[isTelePresence],[MultiCodecAddress],[RearSecCameraAddress],[Extendpoint],[EptOnlineStatus],[PublicEndPoint],[NetworkURL]";
                                query += ",[Secureport],[Secured],[EptCurrentStatus],[GateKeeeperAddress],[UserName],[ManufacturerID],[LastModifiedUser],[Lastmodifieddate],[ProfileType],[IsP2PDefault]";
                                query += ",[IsTestEquipment],[CreateCategory],[identifierValue],[SSHSupport])";
                                query += "    VALUES";
                                query += "(" + iEptID + " ,'" + EPProfileName + "' ,''," + eProtocol + " ,2 ," + addressType + " ,'" + ept.EptProfile[pfct].address + "' ,0,0," + ManufacturerID;
                                query += ",768," + ept.mcuID + ",''," + iProfileID + ",'" + EPProfileName + "'," + iIsDefault + ",0,'',0,0," + ept.orgID + ",'',0";
                                query += ",23,'','',0,'','',0,0,0,''";
                                query += ",0,0,0,'','',3, " + userID + ",'" + DateTime.UtcNow + "' ,1,0,0,2," + ept.identifier + ",0)";

                                ret = NonSelectCommand(query);
                                if (!ret)
                                {
                                    logger.Exception(100, "query failed. query =" + query);
                                }
                            }
                        }
                        // Check in Location Table for the endpoints
                        eptRooms = new List<int>();
                        dsTable = "2Ept_List_D";
                        query = " Select endpointId,Lastmodifieddate from Ept_List_D where isDefault =1 and identifierValue=" + ept.identifier;

                        ret = SelectCommand(query, ref ds, dsTable);
                        if (!ret)
                        {
                            logger.Exception(10, "SQL error");
                            return false;
                        }
                        if (ds.Tables[dsTable].Rows.Count > 0)
                        {
                            for (int eptcnt = 0; eptcnt < ds.Tables[dsTable].Rows.Count; eptcnt++)
                            {
                                dsTable2 = "Ept_List_d5";
                                query = "Select  Count(*) from Loc_Room_D where CreateType =2 and orgId =" + orgID + " and endpointid = " + ds.Tables[dsTable].Rows[eptcnt][0];
                                ret = SelectCommand(query, ref dsept, dsTable2);
                                if (!ret)
                                {
                                    logger.Exception(10, "SQL error");
                                    return false;
                                }
                                if (dsept.Tables[dsTable2].Rows.Count > 0)
                                {
                                    int.TryParse(dsept.Tables[dsTable2].Rows[0][0].ToString(), out SyncLocCnt);
                                    if (SyncLocCnt != 0)
                                    {
                                        if (!eptRooms.Contains(SyncLocCnt))
                                            eptRooms.Add(SyncLocCnt);
                                    }

                                }
                            }
                        }

                        dsTable = "Ept_List_d6";
                        query = "Select  Count(*) from Loc_Sync_D where orgId =" + orgID + " and identifierValue = " + ept.identifier;

                        ret = SelectCommand(query, ref ds, dsTable);
                        if (!ret)
                        {
                            logger.Exception(10, "SQL error");
                            return false;
                        }
                        if (ds.Tables[dsTable].Rows.Count > 0)
                        {
                            int.TryParse(ds.Tables[dsTable].Rows[0][0].ToString(), out SyncLocCnt);
                        }
                        if (eptRooms.Count == 0 && LocCnt == 0)
                            SaveUpdateRPRMRoom(SyncEptRoom, ref isPendingRooms, 1, ref pendingorgID); // Endpoit save in Ept_List_D table
                    }
                }

            }
            catch (Exception ex)
            {
                logger.Trace("SaveUpdateRPRMEndpoints :" + ex.StackTrace);
                errMsg = ex.Message;
                return false;
            }
            return true;
        }
        #endregion

        #region SaveUpdateRPRMRoom
        /// <summary>
        /// SaveUpdateRPRMRoom
        /// </summary>
        /// <param name="room"></param>
        /// <param name="errMsg"></param>
        /// <param name="myVRMLocationID"></param>
        /// <returns></returns>
        internal int SaveUpdateRPRMRoom(NS_MESSENGER.SyncRoom roomProfile,ref bool isPendingRooms, int EndpointCategory,ref List<int> pendingorgID)
        {
            string query = "";
            bool ret = false;
            int iRmID = -1, iRet = 0,toptierID=0,middleTierID=0,timezoneID=0;
            DataSet ds = new DataSet();
            string dsTable = "Loc_Room_d";
            int RoomCount = 0, orgRoomLicenseCnt = 0;
            string Toptier = "", MiddleTier = "", Email = "", UserName = "";
            try
            {
                iRmID = -1;

                //Room Used Count
                query = "Select Count(*) from Loc_Room_D where Disabled = 0  and videoAvailable = 2 and isPublic =0 and Extroom =0 and IsVMR =0 and Disabled = 0 and RoomCategory != 4 and orgId =  " + roomProfile.OrgId;
                
                ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Exception(10, "SQL error");
                    return (-1);
                }
                if (ds.Tables[dsTable].Rows.Count > 0)
                {
                    int.TryParse(ds.Tables[dsTable].Rows[0][0].ToString(), out RoomCount);
                }

                //Org Room License
                ret = true;
                dsTable = "Org_Settings_d2";
                query = " Select MaxVideoRooms from Org_Settings_d where OrgId =" + roomProfile.OrgId;
                ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Exception(10, "SQL error");
                    return (-1);
                }
                if (ds.Tables[dsTable].Rows.Count > 0)
                {
                    int.TryParse(ds.Tables[dsTable].Rows[0][0].ToString(), out orgRoomLicenseCnt);
                }

                //License Check
                isPendingRooms = false;
                if (RoomCount >= orgRoomLicenseCnt)
                    isPendingRooms = true;
                dsTable = "Org_Settings_d1";
                query = "Select MiddleTier,TopTier,OnflyMiddleTierID,OnflyTopTierID  from Org_Settings_D where OrgId = " + roomProfile.OrgId;
                ret = true;
                ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Exception(10, "SQL error");
                    return (-1);
                }
                if (ds.Tables[dsTable].Rows.Count > 0)
                {
                    MiddleTier = Convert.ToString(ds.Tables[dsTable].Rows[0][0]);
                    Toptier = Convert.ToString(ds.Tables[dsTable].Rows[0][1]);
                    int.TryParse(ds.Tables[dsTable].Rows[0][2].ToString(), out middleTierID);
                    int.TryParse(ds.Tables[dsTable].Rows[0][3].ToString(), out toptierID);
                }

                dsTable = "Sys_Settings_D";
                query = "Select TimeZone from Sys_Settings_D ";
                ret = true;
                ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Exception(10, "SQL error");
                    return (-1);
                }
                if (ds.Tables[dsTable].Rows.Count > 0)
                {
                    int.TryParse(ds.Tables[dsTable].Rows[0][0].ToString(), out timezoneID);
                }


                #region Fetch the Admin email
                DataSet ds2 = new DataSet();
                string dsTable2 = "AdminUser";
                string query2 = "Select Email, FirstName + ' ' + LastName as HostName from Usr_List_D where userid = 11";
                ret = SelectCommand(query2, ref ds2, dsTable2);
                if (!ret)
                {
                    logger.Exception(100, "SQL error");
                    return (-1);
                }

                if (ds2.Tables[dsTable2].Rows.Count < 1)
                {
                    // no record 
                    logger.Trace("No Admin email found.");
                    return (-1);
                }
                Email = ds2.Tables[dsTable2].Rows[0]["Email"].ToString();
                UserName = ds2.Tables[dsTable2].Rows[0]["HostName"].ToString();
                #endregion
                if (EndpointCategory == 2)
                    isPendingRooms = true; // Endpoint is asved in Sync table, so room in sync table

                if (isPendingRooms)
                {
                    if (!pendingorgID.Contains(roomProfile.OrgId))
                        pendingorgID.Add(roomProfile.OrgId);

                    query = "INSERT INTO [Loc_Sync_D]([Name],[endpointid],[orgId],[endPointName],[endPointCategory],[identifierValue])";
                    query += "VALUES('" + roomProfile.RoomName + "', " + roomProfile.EndpointId + ", " + roomProfile.OrgId + ", '" + roomProfile.EndpointName + "',2," + roomProfile.identifierValue + ")";

                    ret = NonSelectCommand(query);
                    if (!ret)
                    {
                        logger.Exception(100, "query failed. query =" + query);
                    }
                }
                else
                {
                    query = "   INSERT INTO Loc_Room_D ([Name],[RoomBuilding],[RoomFloor],[RoomNumber],[RoomPhone],[Capacity],[Assistant],[AssistantPhone],[ProjectorAvailable],[MaxPhoneCall],[AdminID]";
                    query += ",[videoAvailable],[DefaultEquipmentid],[DynamicRoomLayout],[Caterer],[L2LocationId],[L3LocationId],[Disabled],[responsetime],[responsemessage],[roomimage],[setuptime],[teardowntime]";
                    query += ",[auxattachments],[endpointid],[costcenterid],[notifyemails],[Address1],[Address2],[City],[State],[Zipcode]";
                    query += ",  [Country],[Maplink],[ParkingDirections],[AdditionalComments]";
                    query += ",[TimezoneID],[Longitude],[Latitude],[auxattachments2],[MapImage1],[MapImage2],[SecurityImage1],[SecurityImage2],[MiscImage1],[MiscImage2],[Custom1],[Custom2],[Custom3]";
                    query += ",[Custom4],[Custom5],[Custom6],[Custom7],[Custom8],[Custom9],[Custom10],[orgId],[HandiCappedAccess],[Lastmodifieddate],[MapImage1Id],[MapImage2Id],[SecurityImage1Id],";
                    query += "[SecurityImage2Id],[MiscImage1Id],[MiscImage2Id],[RoomImageId],[isVIP],[isTelepresence],[ServiceType],[DedicatedVideo],[RoomQueue],[DedicatedCodec],[AVOnsiteSupportEmail]";
                    query += ",[Extroom],[IsVMR],[InternalNumber],[ExternalNumber],[EntityID],[Type],[OwnerID],[Extension],[VidyoURL],[Pin],[isLocked],[allowCallDirect],[MemberID],[allowPersonalMeeting]";
                    query += ",[isPublic],[RoomCategory],[RoomToken],[LastModifiedUser],[VMRLink],[RoomIconTypeId],[RoomUID],[Password],[GuestContactName],[GuestContactEmail],[GuestContactPhone]";
                    query += ",[PermanentConfId],[VMRConfId],[RoomVMRID],[BridgeId],[IsCreateOnMCU],[IsCreateTemplate],[IsDialOutLoc],[DialOutLocationIds],[PermanentconfName],[ConfTemplateId]";
                    query += ",[iControl],[TopTier],[MiddleTier],[Secure],[Securityemail],[UsersSecurityemail],[CreateType],[Domain],[ExchangeURL])     VALUES";
                    query += " ('" + roomProfile.RoomName + "','','','','','0','11','',0,0,11,2,0,0,0," + middleTierID + "," + toptierID + ",0,60,'','',0,0";
                    query += ",''," + roomProfile.EndpointId + ",0,'','','','',0,'',0,'','',''," + timezoneID + ",'','','','','','','','','','','','','','','','','','',''," + roomProfile.OrgId + ",0,'" + DateTime.UtcNow + "',0,0,0,0";
                    query += " ,0,0,'0',0,0,0,'0','','','',0,0,'','',0,'',0,'','','',0,0,0,0,0,1,'',11,'',26,'','','" + UserName + "','" + Email + "','','','','',0,0,0,0,'','',0,0,'" + Toptier + "','" + MiddleTier + "',0,'','',2,'','')";


                    ret = NonSelectCommand(query);
                    if (!ret)
                    {
                        logger.Exception(100, "query failed. query =" + query);
                    }

                }

            }
            catch (Exception ex)
            {
                logger.Trace("SaveUpdateRPRMRoom :" + ex.StackTrace);
                errMsg = ex.Message;
                return -1;
            }
            return iRet;
        }
        #endregion

        #region GetSyncMaxEndpointID
        /// <summary>
        /// GetSyncMaxEndpointID
        /// </summary>
        /// <param></param>
        /// <returns></returns>
        internal int GetSyncMaxEndpointID()
        {
            int bRet = 1;
            DataSet ds = new DataSet();
            string query = "", dsTable = "Ept_Sync_D";
            try
            {

                query = "select (MAX(endpointId)+1) as EndpointID from Ept_Sync_D";

                bool ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Exception(10, "SQL error");
                    return (-1);
                }

                if (ds.Tables[dsTable].Rows.Count > 0)
                {
                    if (ds.Tables[dsTable].Rows[0][0] != null)
                        if (ds.Tables[dsTable].Rows[0][0].ToString().Trim() != "")
                            Int32.TryParse(ds.Tables[dsTable].Rows[0][0].ToString().Trim(), out bRet);
                }

            }
            catch (Exception ex)
            {
                logger.Trace("GetSyncMaxEndpointID" + ex.Message);
                return -1;
            }
            return bRet;
        }
        #endregion

        #region CheckAddressAvailabllity
       /// <summary>
        /// CheckAddressAvailabllity
       /// </summary>
       /// <param name="Address"></param>
       /// <param name="orgID"></param>
       /// <param name="isSyncEpt"></param>
       /// <returns></returns>
        internal bool CheckAddressAvailabllity(string Address, int orgID,int isSyncEpt)
        {
            DataSet ds = new DataSet();
            string query = "", dsTable = "Ept_Sync_D";
            try
            {

                if (isSyncEpt == 1)
                    query = "Select * from Ept_Sync_D where  isDefault =1 and orgId = " + orgID + "and address='" + Address + "'";
                else
                    query = "Select * from Ept_List_D where  isDefault =1 and profileId > 0 and Extendpoint = 0 and deleted =0 and orgId = " + orgID + "and address='" + Address + "'";

                bool ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Exception(10, "SQL error");
                    return false;
                }

                if (ds.Tables[dsTable].Rows.Count > 0)
                    return false;

                return true;

            }
            catch (Exception ex)
            {
                logger.Trace("CheckAddressAvailabllity" + ex.Message);
                return false;
            }
        }
        #endregion

        #region FetchRPRMMcu
        /// <summary>
        /// FetchRPRMMcu
        /// </summary>
        /// <param name="FetchWhygoStatus"></param>
        /// <returns></returns>
        internal bool FetchRPRMMcu(ref List<int> MCUIds, int OrgID)
        {
            string query = "", dsTable = "MCU";
            DataSet ds = new DataSet();
            int BridgeID = 0, RoomSyncPollTime = 0;
            DateTime LastUpdatedRoomSync;
            MCUIds = new List<int>();
            try
            {
                //Need to implement the logic that Org has RPRM mcu and within time interval
                if (OrgID > 0)
                    query = "  Select BridgeID ,orgId, * from Mcu_List_D where BridgeTypeId=13 and orgId = " + OrgID;
                else
                {
                    query = "  Select m.BridgeID, o.RoomSyncPollTime ,o.LastUpdatedRoomSync from Mcu_List_D m ,Org_Settings_D o ";
                    query += " where m.BridgeTypeId = 13 and m.orgId = o.OrgId and o.RoomSyncAuto = 1 and m.deleted =0";
                }

                bool ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Trace("SQL error: Query is " + query);
                    return (false);
                }

                for (int i = 0; i < ds.Tables[dsTable].Rows.Count; i++)
                {
                    int.TryParse(ds.Tables[dsTable].Rows[i]["BridgeID"].ToString(), out BridgeID);
                    if (OrgID == 0)
                    {
                        int.TryParse(ds.Tables[dsTable].Rows[i]["RoomSyncPollTime"].ToString(), out RoomSyncPollTime);
                        DateTime.TryParse(ds.Tables[dsTable].Rows[i]["LastUpdatedRoomSync"].ToString(), out LastUpdatedRoomSync);
                        if (LastUpdatedRoomSync.AddHours(RoomSyncPollTime) < DateTime.UtcNow)
                            MCUIds.Add(BridgeID);
                    }
                    else
                        MCUIds.Add(BridgeID);
                }
            }
            catch (Exception ex)
            {
                logger.Trace("FetchSysSettingDetails Error: " + ex.Message);
                return false;

            }
            return true;
        }
        #endregion

        internal bool UpdateLastSyncDate(int OrgID)
        {
            try
            {
                // update the timesnapshot
                string query = " update Org_Settings_D set LastUpdatedRoomSync = getutcdate() where OrgId = " + OrgID;
                bool ret = NonSelectCommand(query);
                if (!ret)
                {
                    logger.Trace("SQL error");
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }

        #region FetachSiteOrgAdmin
        /// <summary>
        /// FetachSiteOrgAdmin
        /// </summary>
        /// <param name="orgid"></param>
        /// <param name="emailbody"></param>
        /// <param name="emailsubject"></param>
        internal void FetachSiteOrgAdmin(int orgid, ref List<string> emails, ref List<string> Name)
        {
            try
            {
                emails = new List<string>();

                DataSet ds = new DataSet();
                string dsTable = "Usr_List";
                string query = "";

                if (orgid < 11)
                    orgid = 11; //default company

                query = "Select Email, FirstName + ' ' + LastName as HostName from Usr_List_D where Admin in (2) and companyId =" + orgid ;


                bool ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Trace("SQL error");
                    //return null;
                }
                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[dsTable].Rows.Count; i++)
                        {
                            if (ds.Tables[0].Rows[i]["Email"] != null)
                            {
                                if (!emails.Contains(ds.Tables[0].Rows[i]["Email"].ToString().Trim()))
                                    emails.Add(ds.Tables[0].Rows[i]["Email"].ToString().Trim());
                            }

                        }
                    }
                }
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                //return null;
            }
        }
        #endregion

        #region FetchTechEmail
        /// <summary>
        /// FetchTechEmail
        /// </summary>
        /// <param name="orgid"></param>
        /// <param name="email"></param>
        internal void FetchTechEmail(int orgid, ref string techEmail)
        {
            try
            {
                DataSet ds = new DataSet();
                string dsTable = "SysTechInfo";
                string query = "";
                string contact = "";
                string phone = "";

                query = "select [Name], email, phone from Sys_TechSupport_D where orgid=" + orgid;

                bool ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Trace("SQL error");
                    //return null;
                }
                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            if (ds.Tables[0].Rows[0]["Name"] != null)
                            {
                                contact = ds.Tables[0].Rows[0]["Name"].ToString().Trim();
                                contact = contact.Replace("||", "\"").Replace("!!", "'"); // FB 1888   
                            }
                            if (ds.Tables[0].Rows[0]["email"] != null)
                            {
                                techEmail = ds.Tables[0].Rows[0]["email"].ToString().Trim();
                            }
                            if (ds.Tables[0].Rows[0]["phone"] != null)
                            {
                                phone = ds.Tables[0].Rows[0]["phone"].ToString().Trim();
                            }
                        }
                    }
                }
                
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                //return "";
            }
        }
        #endregion

        #region IdentifyModelType
        /// <summary>
        /// IdentifyModelType
        /// </summary>
        /// <param name="orgid"></param>
        /// <param name="email"></param>
        internal void IdentifyModelType(ref int ManufacturerID, string ModelTypeName)
        {
            try
            {
                ManufacturerID = 26;
                if (ModelTypeName == "RP-Desktop")
                    ManufacturerID = 67;
                else if (ModelTypeName == "RealPresence Group 700")
                    ManufacturerID = 65;
                else if (ModelTypeName == "RP-Mobile")
                    ManufacturerID = 67;
                else if (ModelTypeName == "HDX")
                    ManufacturerID = 25;
                else if (ModelTypeName == "GroupSeries")
                    ManufacturerID = 63;

            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                //return "";
            }
        }
        #endregion
        //ZD 101527 Ends

        //ZD 102063 Starts
        internal bool FetchRadvionMCUDetails(NS_MESSENGER.MCU MCU,ref NS_MESSENGER.Conference conf)
        {
            try
            {
                bool ret = false;
                DataSet ds = new DataSet();
                string dsTable = "MCURad", query;
                int ServicePrefix = 0;

                conf.radParam = new NS_MESSENGER.RadVisionConfParam();
               
                ds = new DataSet();
                dsTable = "MCUSilo";
                query = " Select * from Ext_MCUService_D where ServiceId =" + conf.cMcu.ConfServiceID + "and bridgeid = '" + conf.cMcu.iDbId + "'";
                ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Trace("SQL error");

                }

                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            int.TryParse(ds.Tables[dsTable].Rows[0]["Prefix"].ToString().Trim(), out ServicePrefix);

                        }
                    }
                }
                conf.radParam.ServicePrefix = ServicePrefix;

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return (false);
            }
        }
        //ZD 102063 Ends

        //ZD 102359 Starts

        #region UpdateParticipantStatus
        /// <summary>
        /// UpdateParticipantStatus
        /// </summary>
        /// <param name="conf"></param>
        /// <returns></returns>
        public bool UpdateParticipantStatus(int confid, int instanceid,string eID, int status)
        {
            String query = ""; String queryupdt = "";
            String tempQuery = ""; String tempqueryupdt = "";
            DataSet ds = new DataSet();
            string dsTable = "endpoint";
            bool ret = false;
            string eptID = "";
            string tblName = "";
            try
            {
                
                queryupdt = "update {0} set onlinestatus =" + status + " where GUID = '" + eID + "'";

                tblName = "conf_room_d";
                tempqueryupdt = string.Format(queryupdt, tblName, eptID);
                ret = NonSelectCommand(tempqueryupdt);
                if (!ret)
                {
                    logger.Exception(100, "Error executing query.");
                    return false;
                }
               

                tblName = "conf_user_d";
                tempQuery = string.Format(query, tblName, " invitee = 1 and ");
                tempqueryupdt = string.Format(queryupdt, tblName, eptID);
                ret = NonSelectCommand(tempqueryupdt);
                if (!ret)
                {
                    logger.Exception(100, "Error executing query.");
                    return false;
                }

                return true;                

            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }

        #endregion

        //ZD 102359 Ends

        //ZD 102817 Starts
        #region UpdateAllPartyStatus
        /// <summary>
        /// UpdateAllPartyStatus  // Method changed for ZD 103184 Fetching the endpoint address from DB based on (IPadress,Dial String and GUID)
        /// </summary>
        /// <param name="confid"></param>
        /// <param name="instanceid"></param>
        /// <param name="UserData"></param>
        /// <param name="particpant"></param>
        /// <returns></returns>
        public bool UpdateAllPartyStatus(int confid, int instanceid,ref List<NS_MESSENGER.User> UserData, ref NS_MESSENGER.Party particpant)
        {
            string query = "", EPAddress = "", EPIdentifier = "", EPDialString = "";
            bool ret = false;
            string[] partyAddArray = null;
            string[] delimitedArr = { "h323:" }; //ZD 102817
            DataSet ds = null;
            List<string> IPAddress = new List<string>();

            try
            {


                //ZD 103184 Starts
                for (int i = 0; i < UserData.Count; i++)
                {
                    if (UserData[i].DialAddress.Contains("sip:") && particpant.sAddress.Contains(UserData[i].IPISDNAddress))
                    {
                        partyAddArray = particpant.sAddress.Split('@');
                        if (partyAddArray != null && partyAddArray.Length > 0)
                        {
                            if (partyAddArray[0] == UserData[i].IPISDNAddress)
                                UserData[i].IPISDNAddress = particpant.sAddress;

                        }
                    }
                    //ZD 102817 Starts
                    if (UserData[i].IPISDNAddress.Contains("h323:"))
                    {
                        partyAddArray = UserData[i].IPISDNAddress.Split(delimitedArr, StringSplitOptions.None);
                        if (partyAddArray != null && partyAddArray.Length > 0)
                            if (particpant.sAddress.Contains(partyAddArray[1]))
                                UserData[i].IPISDNAddress = partyAddArray[1];
                    }
                    //ZD 102817 Ends
                    if (!IPAddress.Contains(UserData[i].IPISDNAddress))
                        if (!string.IsNullOrEmpty(UserData[i].IPISDNAddress))
                            IPAddress.Add(UserData[i].IPISDNAddress);
                    
                    if (EPIdentifier == "")
                        EPIdentifier = "'" + UserData[i].ParticipantGUID + "'";
                    else
                        EPIdentifier += ",'" + UserData[i].ParticipantGUID + "'";

                    if (EPDialString == "")
                        EPDialString = "'" + UserData[i].DialAddress + "'";
                    else
                        EPDialString += ",'" + UserData[i].DialAddress + "'";
                }

                ds = new DataSet();
                string dsTable = "ConfRoom";
                // Party Identifier - IP Address
                query = " Select IPISDNAddress from conf_room_d where GUID in (" + EPIdentifier + ") and confid = " + confid + " and InstanceId = " + instanceid;
                ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                    logger.Trace("SQL error");

                for (int i = 0; i < ds.Tables[dsTable].Rows.Count; i++)
                {

                    if (!IPAddress.Contains(ds.Tables[dsTable].Rows[i]["IPISDNAddress"].ToString().Trim()))
                        IPAddress.Add(ds.Tables[dsTable].Rows[i]["IPISDNAddress"].ToString().Trim());
                }
                
                //Dial String  -IP Address
                ds = new DataSet();
                dsTable = "ConfRoomIPAddress";
                query = " Select IPISDNAddress from conf_room_d where IPISDNAddress in (" + EPDialString + ") and confid = " + confid + " and InstanceId = " + instanceid;
                ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                    logger.Trace("SQL error");

                for (int i = 0; i < ds.Tables[dsTable].Rows.Count; i++)
                {
                    if (!IPAddress.Contains(ds.Tables[dsTable].Rows[i]["IPISDNAddress"].ToString().Trim()))
                        IPAddress.Add(ds.Tables[dsTable].Rows[i]["IPISDNAddress"].ToString().Trim());
                }

                EPAddress = string.Join("','", IPAddress);

                query = "update conf_room_d set onlinestatus = 0 where confid = " + confid + " and InstanceId = " + instanceid + " and IPISDNAddress not in( '" + EPAddress + "')";

                ret = NonSelectCommand(query);
                if (!ret)
                {
                    logger.Exception(100, "Error executing query.");
                    return false;
                }


                // Cnnf User

                ds = new DataSet();
                dsTable = "ConfUser";
                // Party Identifier - IP Address
                query = " Select IPISDNAddress from conf_user_d where invitee = 1 and GUID in (" + EPIdentifier + ") and confid = " + confid + " and InstanceId = " + instanceid;
                ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                    logger.Trace("SQL error");

                for (int i = 0; i < ds.Tables[dsTable].Rows.Count; i++)
                {

                    if (!IPAddress.Contains(ds.Tables[dsTable].Rows[i]["IPISDNAddress"].ToString().Trim()))
                        IPAddress.Add(ds.Tables[dsTable].Rows[i]["IPISDNAddress"].ToString().Trim());
                }

                //Dial String  -IP Address
                ds = new DataSet();
                dsTable = "ConfUserIP";
                query = " Select IPISDNAddress from conf_user_d where invitee = 1 and IPISDNAddress in (" + EPDialString + ") and confid = " + confid + " and InstanceId = " + instanceid;
                ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                    logger.Trace("SQL error");

                for (int i = 0; i < ds.Tables[dsTable].Rows.Count; i++)
                {
                    if (!IPAddress.Contains(ds.Tables[dsTable].Rows[i]["IPISDNAddress"].ToString().Trim()))
                        IPAddress.Add(ds.Tables[dsTable].Rows[i]["IPISDNAddress"].ToString().Trim());
                }

                EPAddress = string.Join("','", IPAddress);

                query = "update conf_user_d set onlinestatus = 0 where invitee = 1 and confid = " + confid + " and InstanceId = " + instanceid + " and IPISDNAddress not in( '" + EPAddress + "')";

                //ZD 103184 Ends

                ret = NonSelectCommand(query);
                if (!ret)
                {
                    logger.Exception(100, "Error executing query.");
                    return false;
                }
                return true;

            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }

        #endregion
        //ZD 102817 Ends
		//ZD 103263 Start

        #region CheckBJNToken
        /// <summary>
        /// CheckBJNToken
        /// </summary>
        /// <param name="FetchWhygoStatus"></param>
		/// Method Changed for ZD 104021
        /// <returns></returns>
        internal bool CheckBJNToken(ref NS_MESSENGER.MCU mcu)
        {
            string query = "", dsTable = "BJN";
            DataSet ds = new DataSet();
            DateTime ExpiryDateTime = DateTime.Now;
            try
            {
                query = "  Select BJNAppToken, BJNExpiry, BJNExpiryDateTime  from MCU_List_D where BJNAppKey = '" + mcu.sBJNAPPkey + "' and BJNAppSecret = '" + mcu.sBJNAppsecret + "' and BridgeID = " + mcu.iDbId;

                bool ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Trace("SQL error: Query is " + query);
                    return (false);
                }

                if (ds.Tables[dsTable].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[dsTable].Rows.Count; i++)
                    {
                        if (ds.Tables[dsTable].Rows[i]["BJNAppToken"] != null)
                            mcu.sBJNAccessToken = ds.Tables[dsTable].Rows[i]["BJNAppToken"].ToString();

                        if (ds.Tables[dsTable].Rows[i]["BJNExpiry"] != null)
                            int.TryParse(ds.Tables[dsTable].Rows[i]["BJNExpiry"].ToString(), out mcu.iBJNExpiry);

                        if (ds.Tables[dsTable].Rows[i]["BJNExpiryDateTime"] != null)
                            DateTime.TryParse(ds.Tables[dsTable].Rows[i]["BJNExpiryDateTime"].ToString(), out mcu.dBJNExpiryDateTime);
                    }

                    if (mcu.sBJNAccessToken != "" && mcu.iBJNExpiry > 0 && (mcu.dBJNExpiryDateTime > DateTime.Now))
                        return true;
                    else
                        return false;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                logger.Trace("FetchSysSettingDetails Error: " + ex.Message);
                return false;

            }
        }
        #endregion

        #region UpdateBJNToken
        /// <summary>
        /// UpdateBJNToken
		/// Method Changed for ZD 104021
        /// </summary>
        /// <param name="FetchWhygoStatus"></param>
        /// <returns></returns>
        internal bool UpdateBJNToken(NS_MESSENGER.MCU mcu)
        {
            string query = "", dsTable = "BJN";
            DataSet ds = new DataSet();
            try
            {
                query = "UPDATE MCU_LIST_D SET BJNAppToken = '" + mcu.sBJNAccessToken + "', BJNExpiry = '" + mcu.iBJNExpiry + "', BJNExpiryDateTime = '" + mcu.dBJNExpiryDateTime + "', BJNEnterpriseID = '" + mcu.ienterpriseId + "' WHERE BridgeID = " + mcu.iDbId;

                bool ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Trace("SQL error: Delete Query is " + query);
                    return (false);
                }
            }
            catch (Exception ex)
            {
                logger.Trace("FetchSysSettingDetails Error: " + ex.Message);
                return false;

            }
            return true;
        }
        #endregion

        #region FetchBJNRoom
        internal bool FetchBJNRoom(int roomid, ref NS_MESSENGER.BJNRoomDetails BJNRoom)
        {

            try
            {
                DataSet ds1 = new DataSet();
                string dsTable1 = "BJNRoom";
                string query1 = "select RoomVMRID, BJNUserName, BJNUserId, Password from loc_room_d where EnableBJN = 1 and roomid = " + roomid.ToString();
                bool ret1 = SelectCommand(query1, ref ds1, dsTable1);
                if (!ret1)
                {
                    logger.Exception(100, "SQL error");
                    return (false);
                }
                else
                {
                    if (ds1.Tables[dsTable1].Rows.Count > 0)
                    {
                        for (int tbleCount = 0; tbleCount < ds1.Tables[dsTable1].Rows.Count; tbleCount++)
                        {
                            if (!string.IsNullOrEmpty(ds1.Tables[dsTable1].Rows[tbleCount]["RoomVMRID"].ToString()))
                                BJNRoom.numericId = ds1.Tables[dsTable1].Rows[tbleCount]["RoomVMRID"].ToString();
                            if (!string.IsNullOrEmpty(ds1.Tables[dsTable1].Rows[tbleCount]["BJNUserName"].ToString()))
                                BJNRoom.name = ds1.Tables[dsTable1].Rows[tbleCount]["BJNUserName"].ToString();
                            if (!string.IsNullOrEmpty(ds1.Tables[dsTable1].Rows[tbleCount]["BJNUserId"].ToString()))
                                BJNRoom.userID = ds1.Tables[dsTable1].Rows[tbleCount]["BJNUserId"].ToString();
                            if (!string.IsNullOrEmpty(ds1.Tables[dsTable1].Rows[tbleCount]["Password"].ToString()))
                                BJNRoom.moderatorPasscode = ds1.Tables[dsTable1].Rows[tbleCount]["Password"].ToString();
                        }
                    }
                    else
                    {
                        logger.Trace(" Room not found.");
                        return false;
                    }
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion

        #region FetchAccessKey
        /// <summary>
        /// FetchAccessKey
		/// Method Changed for ZD 104021
        /// </summary>
        /// <param name="mcu"></param>
        /// <returns></returns>
        internal bool FetchAccessKey(ref NS_MESSENGER.MCU mcu)
        {

            try
            {
                DataSet ds1 = new DataSet();
                string dsTable1 = "Access";
                string query1 = "select TOP(1) BJNAppKey, BJNAppSecret, BJNEnterpriseID, BridgeID from Mcu_List_D where BridgeTypeId = 20 and orgId = " + mcu.iOrgId + " and deleted = 0 ";
                bool ret1 = SelectCommand(query1, ref ds1, dsTable1);
                if (!ret1)
                {
                    logger.Exception(100, "SQL error");
                    return (false);
                }
                else
                {
                    if (ds1.Tables[dsTable1].Rows.Count > 0)
                    {
                        for (int tbleCount = 0; tbleCount < ds1.Tables[dsTable1].Rows.Count; tbleCount++)
                        {
                            if (!string.IsNullOrEmpty(ds1.Tables[dsTable1].Rows[tbleCount]["BJNAppKey"].ToString()))
                                mcu.sBJNAPPkey = ds1.Tables[dsTable1].Rows[tbleCount]["BJNAppKey"].ToString();
                            if (!string.IsNullOrEmpty(ds1.Tables[dsTable1].Rows[tbleCount]["BJNAppSecret"].ToString()))
                                mcu.sBJNAppsecret = ds1.Tables[dsTable1].Rows[tbleCount]["BJNAppSecret"].ToString();
                            if (!string.IsNullOrEmpty(ds1.Tables[dsTable1].Rows[tbleCount]["BridgeID"].ToString()))
                                int.TryParse(ds1.Tables[dsTable1].Rows[tbleCount]["BridgeID"].ToString(), out mcu.iDbId);
                            if (!string.IsNullOrEmpty(ds1.Tables[dsTable1].Rows[tbleCount]["BJNEnterpriseID"].ToString()))
                                int.TryParse(ds1.Tables[dsTable1].Rows[tbleCount]["BJNEnterpriseID"].ToString(), out mcu.ienterpriseId);
                        }
                    }
                    else
                    {
                        logger.Trace("BJN is not configure Properly.");
                        return false;
                    }
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion

        #region UpdateBJNUserDetails
        /// <summary>
        /// UpdateBJNUserDetails
        /// </summary>
        /// <param name="FetchWhygoStatus"></param>
        /// <returns></returns>
        internal bool UpdateBJNUserDetails(int Roomid, string BJNUserid, string BJNUsername, string BJNPartyPassCode)
        {
            string query = "", dsTable = "BJNUser";
            DataSet ds = new DataSet();
            String Username = "";
            try
            {
                Username = "https://bluejeans.com/" + BJNUsername;
                query = "Update Loc_Room_D SET BJNUserId='" + BJNUserid + "' , VMRLink = '" + Username + "', BJNUserName = '" + BJNUsername + "', BJNPartyPassCode = '" + BJNPartyPassCode + "' Where RoomID =" + Roomid;

                bool ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Trace("SQL error: Delete Query is " + query);
                    return (false);
                }

                ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Trace("SQL error: insert Query is " + query);
                    return (false);
                }

            }
            catch (Exception ex)
            {
                logger.Trace("FetchSysSettingDetails Error: " + ex.Message);
                return false;

            }
            return true;
        }
		//ZD 104021 Start
        internal bool UpdateBJNUserDetails(int userid, string BJNUserid, string BJNUsername, string BJNMeetingId, string BJNPassCode, string BJNPartyPassCode)
        {
            string query = "", dsTable = "BJNUser";
            DataSet ds = new DataSet();
            try
            {
                query = "Update Usr_List_D SET BJNUserId='" + BJNUserid + "' , BJNMeetingId = '" + BJNMeetingId + "', BJNUserName = '" + BJNUsername + "', BJNPassCode = '" + BJNPassCode + "', BJNPartyPassCode = '" + BJNPartyPassCode + "' Where UserID =" + userid;

                bool ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Trace("SQL error: Delete Query is " + query);
                    return (false);
                }

                ret = SelectCommand(query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Trace("SQL error: insert Query is " + query);
                    return (false);
                }

            }
            catch (Exception ex)
            {
                logger.Trace("FetchSysSettingDetails Error: " + ex.Message);
                return false;

            }
            return true;
        }
		//ZD 104021 End
        #endregion

        #region UpdateBJNCallId
        /// <summary>
        /// UpdateBJNCallId
        /// </summary>
        /// <param name="FetchWhygoStatus"></param>
        /// <returns></returns>
        internal bool UpdateBJNCallId(int confid, int instanceid, string BJNUniqueid, string Meetingnumber, string BJNPasscode)
        {
            string query = "";
            DataSet ds = new DataSet();
            try
            {
                query = "update Conf_Conference_D set BJNUniqueid = '" + BJNUniqueid + "' , BJNPasscode = '" + BJNPasscode + "', BJNMeetingid = '" + Meetingnumber + "'";
                query += " where confid = " + confid.ToString();
                query += " and instanceid = " + instanceid.ToString();

                bool ret = NonSelectCommand(query);
                if (!ret)
                {
                    logger.Exception(100, "Error executing query.");
                    return false;
                }
                return true;

            }
            catch (Exception ex)
            {
                logger.Trace("FetchSysSettingDetails Error: " + ex.Message);
                return false;

            }
        }
        #endregion

        #region UpdateBJNPartcipantId
        /// <summary>
        /// UpdateBJNPartcipantId
        /// </summary>
        /// <param name="FetchWhygoStatus"></param>
        /// <returns></returns>
        internal bool UpdateBJNPartcipantId(int confid, int instanceid, string Meetingnumber, string BJNPasscode)
        {
            string query = "", ipaddress = "";
            DataSet ds = new DataSet();
            try
            {
                DataSet ds1 = new DataSet();
                string dsTable1 = "BJNRoom";
                string query1 = "select ipisdnaddress from Conf_Room_D ";
                query1 += " where ConfID = " + confid.ToString();
                query1 += " and instanceID = " + instanceid.ToString();
                query1 += " and isBJNRoom = 1";
                bool ret1 = SelectCommand(query1, ref ds1, dsTable1);
                if (!ret1)
                {
                    logger.Exception(100, "SQL error");
                    return (false);
                }
                else
                {
                    if (ds1.Tables[dsTable1].Rows.Count > 0)
                    {
                        for (int tbleCount = 0; tbleCount < ds1.Tables[dsTable1].Rows.Count; tbleCount++)
                        {
                            if (!string.IsNullOrEmpty(ds1.Tables[dsTable1].Rows[tbleCount]["ipisdnaddress"].ToString()))
                                ipaddress = ds1.Tables[dsTable1].Rows[tbleCount]["ipisdnaddress"].ToString();
                        }
                    }
                    else
                    {
                        logger.Trace("No Conference BJN Room found.");
                        return false;
                    }
                }

                ipaddress = ipaddress + "B" + Meetingnumber + "+" + BJNPasscode;
                query = "update Conf_Room_D set ipisdnaddress = '" + ipaddress + "'";
                query += " where ConfID = " + confid.ToString();
                query += " and instanceID = " + instanceid.ToString();
                query += " and isBJNRoom = 1";

                bool ret = NonSelectCommand(query);
                if (!ret)
                {
                    logger.Exception(100, "Error executing query.");
                    return false;
                }
            }
            catch (Exception ex)
            {
                logger.Trace("FetchSysSettingDetails Error: " + ex.Message);
                return false;

            }
            return true;
        }
        #endregion
		//ZD 104021 Start
        public bool FetchBJNEndpoint(NS_MESSENGER.Conference conf, ref Queue partyq, List<int> confmcuList)
        {
            string MCUaddress = "";
            DataSet ds1 = new DataSet();
            string dsTable1 = "Access";
            string query1 = "";
            try
            {
                NS_MESSENGER.Party party = new NS_MESSENGER.Party();

                party.sName = conf.sDbName + "BJN Party";
                logger.Trace("BJN User = " + party.sName);

                if (conf.sBJNMeetingid != "" && conf.sBJNPasscode != "" && partyq.Count != 0) //ZD 104215
                    party.sAddress = "199.48.152.152" + "B" + conf.sBJNMeetingid + "+" + conf.sBJNPasscode;
                else
                    party.sAddress = "";

                party.bIsOutsideNetwork = false;

                if (party.sAddress.Trim() != "")
                {
                    party.etConnType = NS_MESSENGER.Party.eConnectionType.DIAL_OUT;
                    party.isRoom = true;
                    party.iDbId = conf.iDbID;

                    if (confmcuList.Count > 0)
                    {
                        party.iMcuId = confmcuList[0];

                        query1 = "select BridgeIPISDNAddress from Conf_Bridge_D where confuid = " + conf.iDbNumName + "";
                        bool ret1 = SelectCommand(query1, ref ds1, dsTable1);
                        if (!ret1)
                        {
                            logger.Exception(100, "SQL error");
                            return (false);
                        }
                        else
                        {
                            if (ds1.Tables[dsTable1].Rows.Count > 0)
                            {
                                if (!string.IsNullOrEmpty(ds1.Tables[dsTable1].Rows[0]["BridgeIPISDNAddress"].ToString()))
                                    MCUaddress = ds1.Tables[dsTable1].Rows[0]["BridgeIPISDNAddress"].ToString();
                            }
                            else
                            {
                                logger.Trace("BJN is not configure Properly.");
                                return false;
                            }
                        }
                        party.sMcuAddress = MCUaddress;
                        logger.Trace("MCU Address: " + party.sMcuAddress);

                        logger.Trace("Getting the mcu for the BJN Party...");
                        bool ret4 = FetchMcu(party.iMcuId, ref party.cMcu);
                        if (!ret4)
                        {
                            logger.Exception(100, "Error in fetching MCU info");
                        }
                    }
                    else
                        logger.Trace("Not a Valid MCU to add BJN Party");

                    party.etCallType = NS_MESSENGER.Party.eCallType.VIDEO;
                    party.sMcuServiceName = "";
                    party.iAPIPortNo = 23;
                    party.iconfnumname = conf.iDbNumName;
                    party.etAddressType = NS_MESSENGER.Party.eAddressType.H323_ID;
                    party.etProtocol = NS_MESSENGER.Party.eProtocol.IP;
                    party.etVideoProtocol = NS_MESSENGER.Party.eVideoProtocol.H263;
                    party.stLineRate = conf.stLineRate;
                    party.bIsLecturer = false;
                    party.etType = NS_MESSENGER.Party.eType.BJN;
                    partyq.Enqueue(party);

                    logger.Trace("Party added to queue. Name: " + party.sName + " , McuAddress = " + party.sMcuAddress);
                }
                else if (conf.iCascadeBJN == 0 || partyq.Count == 0)
                {
                    party.iMcuId = confmcuList[0];
                    logger.Trace("Getting the mcu for the BJN Party...");
                    bool ret4 = FetchMcu(party.iMcuId, ref party.cMcu);
                    if (!ret4)
                    {
                        logger.Exception(100, "Error in fetching MCU info");
                    }
                    partyq.Enqueue(party);
                }
            }
            catch (Exception e)
            {
                logger.Exception(200, e.Message);
            }

            return true;
        }
		//ZD 104021 End
        //ZD 103263 End

        //ZD 104256 Starts
        #region UpdateMcuPoolOrder
        /// <summary>
        /// UpdateMcuPoolOrder
        /// </summary>
        /// <param name="mcuDbId"></param>
        /// <param name="currentTime"></param>
        /// <returns></returns>
        internal bool UpdateMcuPoolOrder(int mcuDbId, List<NS_MESSENGER.MCUProfile> MCUProfilelist)
        {
            string query = "";
            bool ret = false;
            NS_MESSENGER.MCUProfile MCUProfile = new NS_MESSENGER.MCUProfile();
            try
            {
                query = " Delete from MCU_PoolOrders_D where MCUId = " + mcuDbId.ToString();
                ret = NonSelectCommand(query);
                if (!ret)
                {
                    logger.Exception(10, "Error executing query.");
                    return false;
                }
                for (int i = 0; i < MCUProfilelist.Count; i++)
                {
                    MCUProfile = MCUProfilelist[i];
                    query = " insert into MCU_PoolOrders_D (MCUId,PoolOrderId,PoolOrderName) values (" + mcuDbId.ToString() + "," + MCUProfile.iId.ToString() + ",'" + MCUProfile.sDisplayname + "')";
                    ret = NonSelectCommand(query);
                    if (!ret)
                    {
                        logger.Exception(10, "Error executing query.");
                        return false;
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion

        #region FetchPoolOrderName
       /// <summary>
        /// FetchPoolOrderName
       /// </summary>
       /// <param name="PoolOrderID"></param>
       /// <param name="PoolOrderName"></param>
       /// <returns></returns>
        internal bool FetchPoolOrderName(int PoolOrderID, int mcuId, ref string PoolOrderName)
        {
            try
            {
                string Query = ""; PoolOrderName = "";
                DataSet ds = new DataSet();
                string dsTable = "MCU_PoolOrders_D";

                Query = " Select MCUId, PoolOrderName from MCU_PoolOrders_D where MCUId = " + mcuId + " and PoolOrderId = " + PoolOrderID + "";
                bool ret = SelectCommand(Query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Exception(100, "SQL error");
                    return (false);
                }

                if (ds.Tables[dsTable].Rows.Count > 0)
                {
                    PoolOrderName = "";
                    PoolOrderName = ds.Tables[dsTable].Rows[0]["PoolOrderName"].ToString().Trim();
                    logger.Trace("Conf PoolOrder Name = " + PoolOrderName);
                }
            }
            catch (Exception ex)
            {
                logger.Trace("" + ex.StackTrace);
                return false;
            }
            return true;
        }
        #endregion
 		//ZD 104256 Ends

		//ZD 104465 Starts
        #region FetchConferenceProfile
        /// <summary>
        /// FetchConferenceProfile
        /// </summary>
        /// <param name="ConfProfileID"></param>
        /// <param name="mcuId"></param>
        /// <param name="ConfProfileName"></param>
        /// <returns></returns>
        internal bool FetchConferenceProfile(int ConfProfileID, int mcuId, ref string ConfProfileName)
        {
            try
            {
                string Query = ""; ConfProfileName = "";
                DataSet ds = new DataSet();
                string dsTable = "MCU_Profiles_D";

                Query = " Select MCUId, ProfileName from MCU_Profiles_D where MCUId = " + mcuId + " and ProfileId = " + ConfProfileID + "";
                bool ret = SelectCommand(Query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Exception(100, "SQL error");
                    return (false);
                }

                if (ds.Tables[dsTable].Rows.Count > 0)
                {
                    ConfProfileName = "";
                    ConfProfileName = ds.Tables[dsTable].Rows[0]["ProfileName"].ToString().Trim();
                    logger.Trace("Conf ProfileName Name = " + ConfProfileName);
                }
            }
            catch (Exception ex)
            {
                logger.Trace("" + ex.StackTrace);
                return false;
            }
            return true;
        }
        #endregion
        //ZD 104465 Ends

        //ALLDEV-842 Starts
        #region FetchEM7Details
        public bool FetchEM7Details(ref NS_MESSENGER.EM7Details EM7Details, string orgID)
        {
            try
            {
                string Query = "";
                DataSet ds = new DataSet();
                string dsTable = "EM7";

                Query = "Select o.OrgId, o.EM7OrgID, s.EM7URI, s.EM7Username, s.EM7Password, s.EM7Port from Org_Settings_D o,Sys_Settings_D s where o.OrgId = " + orgID;
                bool ret = SelectCommand(Query, ref ds, dsTable);
                if (!ret)
                {
                    logger.Trace("FetchEM7Details query failed: " + Query);
                    return (false);
                }

                if (ds.Tables[dsTable].Rows.Count > 0)
                {
                    if (EM7Details == null)
                        EM7Details = new NS_MESSENGER.EM7Details();

                    int OrgId = 0;
                    if (ds.Tables[dsTable].Rows[0]["OrgId"] != null)
                        Int32.TryParse(ds.Tables[dsTable].Rows[0]["OrgId"].ToString().Trim(), out OrgId);
                    EM7Details.OrgId = OrgId;

                    int EM7OrgID = 0;
                    if (ds.Tables[dsTable].Rows[0]["EM7OrgID"] != null)
                        Int32.TryParse(ds.Tables[dsTable].Rows[0]["EM7OrgID"].ToString().Trim(), out EM7OrgID);
                    EM7Details.EM7OrgID = EM7OrgID;

                    if (ds.Tables[dsTable].Rows[0]["EM7URI"] != null)
                        EM7Details.EM7URL = ds.Tables[dsTable].Rows[0]["EM7URI"].ToString();

                    if (ds.Tables[dsTable].Rows[0]["EM7Username"] != null)
                        EM7Details.EM7Username = ds.Tables[dsTable].Rows[0]["EM7Username"].ToString();

                    if (ds.Tables[0].Rows[0]["EM7Password"] != null)
                    {
                        cryptography.Crypto crypto = new Crypto();
                        EM7Details.EM7Password = crypto.decrypt(ds.Tables[0].Rows[0]["EM7Password"].ToString().Trim());
                    }

                    if (ds.Tables[dsTable].Rows[0]["EM7Port"] != null)
                        EM7Details.EM7Port = ds.Tables[0].Rows[0]["EM7Port"].ToString().Trim();
                    if (EM7Details.EM7Port == "443")
                        EM7Details.EM7URL = "https://" + EM7Details.EM7URL;
                    else
                        EM7Details.EM7URL = "http://" + EM7Details.EM7URL;

                    logger.Trace("EM7URL=" + EM7Details.EM7URL + "  EM7Username" + EM7Details.EM7Username + " EM7Password" + EM7Details.EM7Password + " EM7Port:" + EM7Details.EM7Port);
                }
            }
            catch (Exception ex)
            {
                logger.Trace("" + ex.StackTrace);
                return false;
            }
            return true;
        }
        #endregion

        #region UpdateEndpointStatus
        public bool UpdateEndpointStatus(Hashtable EndpointList)
        {
            string query = "",query1 = "", EM7OnlineStatus = "", dsTable = "EPTLIST";
            bool ret = false;
            DataSet ds;
            string Eptid = "", Profileid = "";
            try
            {
                query = "Update Ept_List_D Set EptOnlineStatus = 2,Eptcurrentstatus = 0 where deleted = 0 ";
                ret = NonSelectCommand(query);
                if (!ret)
                {
                    logger.Trace(query);
                    return false;
                }

                if (EndpointList.Count > 0)
                {
                    foreach (DictionaryEntry eptData in EndpointList)
                    {
                        ds = new DataSet();                        
                        if (eptData.Value.ToString() == "0")
                            EM7OnlineStatus = "1";
                        else
                            EM7OnlineStatus = "0";
                        query = "select * from Ept_List_D where [address] =  '" + eptData.Key.ToString() + "'";
                        ret = SelectCommand(query, ref ds, dsTable);
                        if (!ret)
                        {
                            logger.Trace(query);
                            return false;
                        }
                        if (ds.Tables.Count > 0)
                        {
                            for (int i = 0; i < ds.Tables[dsTable].Rows.Count; i++)
                            {
                                if (ds.Tables[dsTable].Rows[i]["endpointId"] != null)
                                    Eptid = ds.Tables[dsTable].Rows[i]["endpointId"].ToString().Trim();

                                if (ds.Tables[dsTable].Rows[i]["profileId"] != null)
                                    Profileid = ds.Tables[dsTable].Rows[i]["profileId"].ToString().Trim();

                                query1 += " Update Ept_List_D Set EptOnlineStatus = " + EM7OnlineStatus + ",Eptcurrentstatus = " + eptData.Value.ToString() + " where endpointId =  '" + Eptid + "' and profileId = '" + Profileid + "'";
                            }
                        }
                    }                    
                    ret = NonSelectCommand(query1);
                    if (!ret)
                    {
                        logger.Trace(query1);
                        return false;
                    }
                } 
            }
            catch (Exception e)
            {
                logger.Trace(e.Message);
                return false;
            }
            return true;
        }
        #endregion        
        //ALLDEV-842 Ends
    }
}

#if COMMENT
internal bool FetchP2PConferences(ref Queue confq)
		{
			try 
			{
				DataSet ds = new DataSet();
				string dsTable = "P2P";
				bool ret = SelectCommand(p2pSql,ref ds,dsTable);
				if (!ret)
				{
					
					logger.Exception(10,"SQL error");
					return (false);
				}
				
				for (int i=0;i<ds.Tables[dsTable].Rows.Count ; i++)
				{
					NS_MESSENGER.Conference conf = new NS_MESSENGER.Conference();

					// Retreive confid,instanceid
					conf.iDbID = Int32.Parse(ds.Tables[dsTable].Rows[i][0].ToString());
					conf.iInstanceID =  Int32.Parse(ds.Tables[dsTable].Rows[i][1].ToString());

					confq.Enqueue(conf);
				}
			}
			catch(Exception e)
			{	
				logger.Exception(100,e.Message );
				return false;
			}
			return true;
		}
		internal bool FetchConf(ref NS_PERSISTENCE.Conference conf)
		{
			try 
			{
				// Retreive conf details
				DataSet ds = new DataSet();
				string dsTable = "Conference";
				string query = "Select * from Conf_Conference_D where confid = " + conf.iConfID.ToString() + " and instanceid = " + conf.iInstanceID.ToString();
				bool ret = SelectCommand(query,ref ds,dsTable);
				if (!ret)
				{
					logger.Exception(100,"SQL error");
					return (false);
				}		

				if (ds.Tables[dsTable].Rows.Count < 1 )
				{
					// no confs 
					logger.Trace ("No conf found.");
					return false;
				}

				//conf details
				conf.sConfName = ds.Tables[dsTable].Rows[0]["ExternalName"].ToString().Trim();
				conf.iOwnerID = Int32.Parse(ds.Tables[dsTable].Rows[0]["owner"].ToString().Trim());  
				conf.sDescription = ds.Tables[dsTable].Rows[0]["description"].ToString().Trim();
				conf.sPassword = ds.Tables[dsTable].Rows[0]["password"].ToString().Trim();			

				// start date & time
				conf.dtStartDateTime = DateTime.Parse(ds.Tables[dsTable].Rows[0]["ConfDate"].ToString().Trim()); 
				logger.Trace ("Start Time : " + conf.dtStartDateTime.ToString("T"));

				// duration
				conf.iDurationMin = Int32.Parse(ds.Tables[dsTable].Rows[0]["Duration"].ToString().Trim());				

				//timezone
				conf.iTimezoneID = Int32.Parse(ds.Tables[dsTable].Rows[0]["Timezone"].ToString().Trim()); 				
		
				// convert the conf time from UTC to conf timezone (this is for latter comparison)
				ChangeTimeToAnotherTimeZone(conf.iTimezoneID,ref conf.dtStartDateTime);

				//type
				conf.iConfType = Int32.Parse(ds.Tables[dsTable].Rows[0]["ConfType"].ToString().Trim()); 			

				//internal
				int iPublic = Int32.Parse(ds.Tables[dsTable].Rows[0]["Public"].ToString().Trim()); 			
				if (iPublic==1)
					conf.isPublic = true;

				//recurring
				int iRecurring = Int32.Parse(ds.Tables[dsTable].Rows[0]["Recuring"].ToString().Trim()); 			
				if (iPublic==1)
					conf.isRecurring = true;

				if (conf.isRecurring)
				{
#region  recurring pattern details
					DataSet ds1 = new DataSet();
					string dsTable1 = "Recurring";
					string query1 = "Select * from Conf_RecurInfo_D where confid = " + conf.iConfID.ToString()  ;	
					bool ret1 = SelectCommand(query1,ref ds1,dsTable1);
					if (!ret1)
					{
						logger.Exception(100,"SQL error");
						return (false);
					}		

					if (ds1.Tables[dsTable1].Rows.Count > 0 )
					{
						// recur type
						conf.iRecur_RecurType = Int32.Parse(ds1.Tables[dsTable1].Rows[0]["RecurType"].ToString());

					}
#endregion
				}
				/*				
#region Fetch the conf advanced audio/video params 
								DataSet ds1 = new DataSet();
								string dsTable1 = "Conference";
								string query1 = "Select * from Conf_AdvAVParams_D where confid = " + conf.iConfID.ToString() + " and instanceid = " + conf.iInstanceID.ToString() ;	
								bool ret1 = SelectCommand(query1,ref ds1,dsTable1);
								if (!ret1)
								{
									logger.Exception(100,"SQL error");
									return (false);
								}		

								if (ds1.Tables[dsTable1].Rows.Count > 0 )
								{
									//line rate
									conf.iLineRateID = Int32.Parse(ds1.Tables[dsTable1].Rows[0]["LineRateID"].ToString());

									// audio codec
									conf.iAudioalgorithmID = Int32.Parse(ds1.Tables[dsTable1].Rows[0]["AudioAlgorithmID"].ToString());

									// network
									conf.iVideoProtocolID = Int32.Parse(ds1.Tables[dsTable1].Rows[0]["VideoProtocolID"].ToString());							
					
									// media
									conf.iMediaID = Int32.Parse(ds1.Tables[dsTable1].Rows[0]["mediaID"].ToString().Trim());
						
									// video layouts 
									conf.iVideoLayout = Int32.Parse(ds1.Tables[dsTable1].Rows[0]["videoLayoutID"].ToString());
				
									// dual stream mode - H.239
									if (ds1.Tables[dsTable1].Rows[0]["dualStreamModeID"].ToString()!= null)
										{
											if (Int32.Parse(ds1.Tables[dsTable1].Rows[0]["dualStreamModeID"].ToString())== 1)
											{
												conf.bH239 = true;
											}
										}
					
										// conference on port (polycom-specific feature)
										if (ds1.Tables[dsTable1].Rows[0]["conferenceOnPort"].ToString()!= null)
										{
											if (Int32.Parse(ds1.Tables[dsTable1].Rows[0]["conferenceOnPort"].ToString())== 1)
											{
												conf.bConferenceOnPort = true;
											}
										}
									
										// encryption (polycom-specific feature)
										if (ds1.Tables[dsTable1].Rows[0]["encryption"].ToString()!= null)
										{
											if (Int32.Parse(ds1.Tables[dsTable1].Rows[0]["encryption"].ToString())== 1)
											{
												conf.bEncryption = true;
											}
										}
						
										// max audio ports
										if (ds1.Tables[dsTable1].Rows[0]["maxAudioParticipants"].ToString()!= null)
										{
											conf.iMaxAudioPorts = Int32.Parse(ds1.Tables[dsTable1].Rows[0]["maxAudioParticipants"].ToString());							
										}
						
										// max video ports
										if (ds1.Tables[dsTable1].Rows[0]["maxVideoParticipants"].ToString()!= null)
										{
											conf.iMaxVideoPorts = Int32.Parse(ds1.Tables[dsTable1].Rows[0]["maxVideoParticipants"].ToString());							
										}

										// video codecs
										if (ds1.Tables[dsTable1].Rows[0]["videoSession"].ToString()!= null)
										{
											// Note : this is a confusing name matchup 
											// In VRM DB this is equated to VideoSession (which is actually incorrect but needed to keep it that way to support legacy deployments)
											// Its actually a VideoCodec (hence, in RTC the name is now corrected internally).
											switch (Int32.Parse(ds1.Tables[dsTable1].Rows[0]["videoSession"].ToString()))
											{
												case 0:
												{
													conf.etVideoCodec = NS_MESSENGER.Conference.eVideoCodec.AUTO;
													break;
												}
												case 1:
												{
													conf.etVideoCodec = NS_MESSENGER.Conference.eVideoCodec.H261;
													break;
												}
												case 2:
												{
													conf.etVideoCodec = NS_MESSENGER.Conference.eVideoCodec.H263;
													break;
												}
												case 3:
												{
													conf.etVideoCodec = NS_MESSENGER.Conference.eVideoCodec.H264;
													break;
												}
											}
										}
									}
									catch (Exception e)
									{
										logger.Trace ("Advanced Audio/Video Params: " + e.Message);
									}
								}
#endregion
				*/
				
				// retrieve all rooms
				ret = false;
				ret= FetchConfRooms(conf.iConfID,conf.iInstanceID,ref conf.roomList);
				if (!ret)
				{
					logger.Trace("Error retreiving rooms for conference =" + conf.sConfName);
				}
				
				// retrieve all users & guests
				//ret = false;
				//ret= FetchAllUsers(conf.iConfID,conf.iInstanceID,ref conf.partyList);
				//if (!ret)
				//{
				//	logger.Trace("Error retreiving users for conference =" + conf.sDbName);
				//}
				
								
			}
			catch(Exception e)
			{	
				logger.Exception(100,e.Message);
				return false;
			}

			return true;
		}

#endif

