
/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9216.3.2  Ends  (29th April 2016)        */
/*                              Features & Bugs for V2.9216.3.3  Starts(2nd May 2016)           */
/*                                                                                              */
/* ******************************************************************************************** */

/* ********************** ALLDEV-846 - 4th May 2016 Starts************ */

update gen_timezone_s set CurDSTstart = CurDSTstart4, CurDSTend = CurDSTend4,
CurDSTstart1 = CurDSTstart5, CurDSTend1 =CurDSTend5, CurDSTstart2 = CurDSTstart6, CurDSTend2 =CurDSTend6, 
CurDSTstart3 = CurDSTstart7, CurDSTend3 =CurDSTend7, CurDSTstart4 = CurDSTstart8, CurDSTend4 =CurDSTend8, 
CurDSTstart5 = CurDSTstart9, CurDSTend5 =CurDSTend9 

--NO DST changes (12) 
update gen_timezone_s set 
CurDSTstart6 = '1980-01-01 00:00:00.000', CurDSTend6 = '1980-01-01 00:00:00.000',
CurDSTstart7 = '1980-01-01 00:00:00.000', CurDSTend7 = '1980-01-01 00:00:00.000',
CurDSTstart8 = '1980-01-01 00:00:00.000', CurDSTend8 = '1980-01-01 00:00:00.000',
CurDSTstart9 = '1980-01-01 00:00:00.000', CurDSTend9 = '1980-01-01 00:00:00.000'
where TimeZoneID in (5,12,27,28,41,44,48,49,53,68,74,88)

-- Easern TZ (8)
update gen_timezone_s set 
CurDSTstart6 = '2018-03-11 02:00:00.000', CurDSTend6 = '2018-11-04 02:00:00.000',
CurDSTstart7 = '2019-03-10 02:00:00.000', CurDSTend7 = '2019-11-03 02:00:00.000',
CurDSTstart8 = '2020-03-08 02:00:00.000', CurDSTend8 = '2020-11-01 02:00:00.000',
CurDSTstart9 = '2021-03-14 02:00:00.000', CurDSTend9 = '2021-11-07 02:00:00.000'
where TimeZoneID in (19,26,42,47,51,91,2,6)

--1 Tehran
update gen_timezone_s set 
CurDSTstart6 = '2018-03-22 00:00:00.000', CurDSTend6 = '2018-09-22 00:00:00.000',
CurDSTstart7 = '2019-03-22 00:00:00.000', CurDSTend7 = '2019-09-22 00:00:00.000',
CurDSTstart8 = '2020-03-21 00:00:00.000', CurDSTend8 = '2020-09-21 00:00:00.000',
CurDSTstart9 = '2021-03-22 00:00:00.000', CurDSTend9 = '2021-09-22 00:00:00.000'
where TimeZoneID in (37)

--4 Bucharest
update gen_timezone_s set 
CurDSTstart6 = '2018-03-25 03:00:00.000', CurDSTend6 = '2018-10-28 04:00:00.000',
CurDSTstart7 = '2019-03-31 03:00:00.000', CurDSTend7 = '2019-10-27 04:00:00.000',
CurDSTstart8 = '2020-03-29 03:00:00.000', CurDSTend8 = '2020-10-25 04:00:00.000',
CurDSTstart9 = '2021-03-28 03:00:00.000', CurDSTend9 = '2021-10-31 04:00:00.000'
where TimeZoneID in (24,34,30,95)

--1 Dublin
update gen_timezone_s set 
CurDSTstart6 = '2018-03-25 01:00:00.000', CurDSTend6 = '2018-10-28 02:00:00.000',
CurDSTstart7 = '2019-03-31 01:00:00.000', CurDSTend7 = '2019-10-27 02:00:00.000',
CurDSTstart8 = '2020-03-29 01:00:00.000', CurDSTend8 = '2020-10-25 02:00:00.000',
CurDSTstart9 = '2021-03-28 01:00:00.000', CurDSTend9 = '2021-10-31 02:00:00.000'
where TimeZoneID in (31)

--1 Azores
update gen_timezone_s set 
CurDSTstart6 = '2018-03-25 00:00:00.000', CurDSTend6 = '2018-10-28 01:00:00.000',
CurDSTstart7 = '2019-03-31 00:00:00.000', CurDSTend7 = '2019-10-27 01:00:00.000',
CurDSTstart8 = '2020-03-29 00:00:00.000', CurDSTend8 = '2020-10-25 01:00:00.000',
CurDSTstart9 = '2021-03-28 00:00:00.000', CurDSTend9 = '2021-10-31 01:00:00.000'
where TimeZoneID in (9)


--5 Casablanca
update gen_timezone_s set 
CurDSTstart6 = '2018-03-25 02:00:00.000', CurDSTend6 = '2018-10-28 03:00:00.000',
CurDSTstart7 = '2019-03-31 02:00:00.000', CurDSTend7 = '2019-10-27 03:00:00.000',
CurDSTstart8 = '2020-03-29 02:00:00.000', CurDSTend8 = '2020-10-25 03:00:00.000',
CurDSTstart9 = '2021-03-28 02:00:00.000', CurDSTend9 = '2021-10-31 03:00:00.000'
where TimeZoneID in (16,17,52,71,102)

--1 Greenland
update gen_timezone_s set 
CurDSTstart6 = '2018-03-24 22:00:00.000', CurDSTend6 = '2018-10-27 23:00:00.000',
CurDSTstart7 = '2019-03-30 22:00:00.000', CurDSTend7 = '2019-10-26 23:00:00.000',
CurDSTstart8 = '2020-03-28 22:00:00.000', CurDSTend8 = '2020-10-24 23:00:00.000',
CurDSTstart9 = '2021-03-27 22:00:00.000', CurDSTend9 = '2021-10-30 23:00:00.000'
where TimeZoneID in (32)


--3 Sydney
update gen_timezone_s set 
CurDSTstart6 = '2018-04-01 03:00:00.000', CurDSTend6 = '2018-10-07 02:00:00.000',
CurDSTstart7 = '2019-04-07 03:00:00.000', CurDSTend7 = '2019-10-06 02:00:00.000',
CurDSTstart8 = '2020-04-05 03:00:00.000', CurDSTend8 = '2020-10-04 02:00:00.000',
CurDSTstart9 = '2021-04-04 03:00:00.000', CurDSTend9 = '2021-10-03 02:00:00.000'
where TimeZoneID in (63,8,13)


--1 Auckland
update gen_timezone_s set 
CurDSTstart6 = '2018-04-01 03:00:00.000', CurDSTend6 = '2018-09-30 02:00:00.000',
CurDSTstart7 = '2019-04-07 03:0:00.000',  CurDSTend7 = '2019-09-29 02:00:00.000',
CurDSTstart8 = '2020-04-05 03:00:00.000', CurDSTend8 = '2020-09-27 02:00:00.000',
CurDSTstart9 = '2021-04-04 03:00:00.000', CurDSTend9 = '2021-09-26 02:00:00.000'
where TimeZoneID in (46)



--1 Santiago
update gen_timezone_s set 
CurDSTstart6 = '2018-05-13 00:00:00.000', CurDSTend6 = '2018-08-12 00:00:00.000',
CurDSTstart7 = '2019-05-12 00:00:00.000', CurDSTend7 = '2019-08-11 00:00:00.000',
CurDSTstart8 = '2020-05-10 00:00:00.000', CurDSTend8 = '2020-08-09 00:00:00.000',
CurDSTstart9 = '2021-05-09 00:00:00.000', CurDSTend9 = '2021-08-15 00:00:00.000'
where TimeZoneID in (50)

--2 Brasilia
update gen_timezone_s set 
CurDSTstart6 = '2018-02-18 00:00:00.000', CurDSTend6 = '2018-10-21 00:00:00.000',
CurDSTstart7 = '2019-02-17 00:00:00.000', CurDSTend7 = '2019-10-20 00:00:00.000',
CurDSTstart8 = '2020-02-16 00:00:00.000', CurDSTend8 = '2020-10-18 00:00:00.000',
CurDSTstart9 = '2021-02-21 00:00:00.000', CurDSTend9 = '2021-10-17 00:00:00.000'
where TimeZoneID in (80,25)


-- 2 Guadalajara, Mexico City, Monterrey
update gen_timezone_s set 
CurDSTstart6 = '2018-04-01 02:00:00.000', CurDSTend6 = '2018-10-28 02:00:00.000',
CurDSTstart7 = '2019-04-07 02:00:00.000', CurDSTend7 = '2019-10-27 02:00:00.000',
CurDSTstart8 = '2020-04-05 02:00:00.000', CurDSTend8 = '2020-10-25 02:00:00.000',
CurDSTstart9 = '2021-04-04 02:00:00.000', CurDSTend9 = '2021-04-04 02:00:00.000'
where TimeZoneID in (81,89)

--1 Beirut
update gen_timezone_s set 
CurDSTstart6 = '2018-03-25 00:00:00.000', CurDSTend6 = '2018-10-28 00:00:00.000',
CurDSTstart7 = '2019-03-31 00:00:00.000', CurDSTend7 = '2019-10-27 00:00:00.000',
CurDSTstart8 = '2020-03-29 00:00:00.000', CurDSTend8 = '2020-10-25 00:00:00.000',
CurDSTstart9 = '2021-03-28 00:00:00.000', CurDSTend9 = '2021-10-31 00:00:00.000'
where TimeZoneID in (87)

--1 Windhoek

update gen_timezone_s set 
CurDSTstart6 = '2018-04-01 02:00:00.000', CurDSTend6 = '2018-09-02 02:00:00.000',
CurDSTstart7 = '2019-04-07 02:00:00.000', CurDSTend7 = '2019-09-01 02:00:00.000',
CurDSTstart8 = '2020-04-05 02:00:00.000', CurDSTend8 = '2020-09-06 02:00:00.000',
CurDSTstart9 = '2020-09-06 02:00:00.000', CurDSTend9 = '2021-09-05 02:00:00.000'
where TimeZoneID in (87)

--1 Samoa
update gen_timezone_s set 
CurDSTstart6 = '2018-04-01 04:00:00.000', CurDSTend6 = '2018-09-30 03:00:00.000',
CurDSTstart7 = '2019-04-07 04:00:00.000', CurDSTend7 = '2019-09-29 03:00:00.000',
CurDSTstart8 = '2020-04-05 04:00:00.000', CurDSTend8 = '2020-09-27 03:00:00.000',
CurDSTstart9 = '2021-04-04 04:00:00.000', CurDSTend9 = '2021-09-26 03:00:00.000'
where TimeZoneID in (57)

--1 Asuncion
update gen_timezone_s set 
CurDSTstart6 = '2018-03-25 00:00:00.000', CurDSTend6 = '2018-10-07 00:00:00.000',
CurDSTstart7 = '2019-03-24 00:00:00.000', CurDSTend7 = '2019-10-06 00:00:00.000',
CurDSTstart8 = '2020-03-22 00:00:00.000', CurDSTend8 = '2020-10-04 00:00:00.000',
CurDSTstart9 = '2021-03-28 00:00:00.000', CurDSTend9 = '2021-10-03 00:00:00.000'
where TimeZoneID in (93)

--1 Damascus
update gen_timezone_s set 
CurDSTstart6 = '2018-03-30 00:00:00.000', CurDSTend6 = '2018-10-26 00:00:00.000',
CurDSTstart7 = '2019-03-29 00:00:00.000', CurDSTend7 = '2019-10-25 00:00:00.000',
CurDSTstart8 = '2020-03-27 00:00:00.000', CurDSTend8 = '2020-10-30 00:00:00.000',
CurDSTstart9 = '2021-03-26 00:00:00.000', CurDSTend9 = '2021-10-29 00:00:00.000'
where TimeZoneID in (94)


Declare @tzone as int, @confid as int, @instanceid as int, @chDate as datetime
Declare @chTime as datetime, @setupTime as datetime, @tearTime as datetime

DECLARE getDSTCursor CURSOR for

select a.timezone, a.confid, a.instanceid
, dbo.changeToGMTTime(a.timezone,dateadd(minute, -t.Bias, a.confdate)) as changeddate
, dbo.changeToGMTTime(a.timezone,dateadd(minute, -t.Bias, a.conftime)) as changedtime
, dbo.changeToGMTTime(a.timezone,dateadd(minute, -t.Bias, a.SetupTime)) as changedSetupTime
, dbo.changeToGMTTime(a.timezone,dateadd(minute, -t.Bias, a.TearDownTime)) as changedTearDownTime
from Conf_Conference_D a , gen_timezone_s t where a.timezone = t.timezoneid and t.DST = 1
and status in (0,1)and --confdate >= getdate() 
--and settingtime < '2017-12-12 00:00:00.000'
confdate >= '2017-12-12 00:00:00.000'
order by a.confid , a.instanceid
OPEN getDSTCursor

FETCH NEXT FROM getDSTCursor INTO  @tzone, @confid, @instanceid, @chDate, @chTime, @setupTime, @tearTime
WHILE @@FETCH_STATUS = 0
BEGIN

update Conf_Conference_D set confdate = @chDate, conftime = @chTime, SetupTime = @setupTime, TearDownTime = @tearTime
where confid = @confid  and instanceid = @instanceid

update Conf_Room_D set StartDate = @chDate where ConfID = @confid and instanceID = @instanceid

update Conf_User_D set ConfStartDate = @chDate where ConfID = @confid and instanceID = @instanceid

if(@instanceid = 1)
Begin
	update Conf_RecurInfo_D set startTime = @chDate where confid = @confid 
End

FETCH NEXT FROM getDSTCursor INTO @tzone, @confid, @instanceid, @chDate, @chTime, @setupTime, @tearTime
END
CLOSE getDSTCursor
DEALLOCATE getDSTCursor



/* ********************** ALLDEV-846 - 4th May 2016 Ends************** */


/* **********************ALLDEV-839 - 5th May 2016 Starts************ */

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Sys_Settings_D ADD
	PollCount int NULL,
	AdminEmailaddress nvarchar(500) NULL,
	GoogleIntegration int NULL
GO
ALTER TABLE dbo.Sys_Settings_D SET (LOCK_ESCALATION = TABLE)
GO
COMMIT


update Sys_Settings_D set PollCount = 0 , AdminEmailaddress = '' , GoogleIntegration = 0



BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Google_Conf_Error_D
	(
	ID int NOT NULL IDENTITY (1, 1),
	confid int NOT NULL,
	instanceid int NOT NULL,
	confnumname nvarchar(250) NULL,
	GoogleGUID nvarchar(250) NULL,
	GoogleSequence int NOT NULL,
	GoogleConfLastUpdated [datetime] NULL,
	[LastModifiedDate] [datetime] NULL,
	ConfInxml nvarchar(Max) NULL,
	ConfOutxml nvarchar(Max) NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Google_Conf_Error_D SET (LOCK_ESCALATION = TABLE)
GO
COMMIT


/* **********************ALLDEV-839 - 5th May 2016 Ends************ */




